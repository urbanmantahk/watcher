﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Object_t;
// System.Runtime.InteropServices.IDispatchImplAttribute
struct IDispatchImplAttribute_t1_799;
// System.Runtime.InteropServices.ImportedFromTypeLibAttribute
struct ImportedFromTypeLibAttribute_t1_803;
// System.String
struct String_t;
// System.Runtime.InteropServices.InterfaceTypeAttribute
struct InterfaceTypeAttribute_t1_805;
// System.Runtime.InteropServices.InvalidComObjectException
struct InvalidComObjectException_t1_806;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Runtime.InteropServices.InvalidOleVariantTypeException
struct InvalidOleVariantTypeException_t1_807;
// System.Runtime.InteropServices.LCIDConversionAttribute
struct LCIDConversionAttribute_t1_808;
// System.Array
struct Array_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Int16[]
struct Int16U5BU5D_t1_1694;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Int64[]
struct Int64U5BU5D_t1_1665;
// System.Single[]
struct SingleU5BU5D_t1_1695;
// System.Double[]
struct DoubleU5BU5D_t1_1666;
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Module
struct Module_t1_495;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Threading.Thread
struct Thread_t1_901;
// System.Runtime.InteropServices.UCOMITypeInfo
struct UCOMITypeInfo_t1_1696;
// System.Runtime.InteropServices.ComTypes.ITypeInfo
struct ITypeInfo_t1_1697;
// System.Runtime.InteropServices.UCOMITypeLib
struct UCOMITypeLib_t1_1698;
// System.Runtime.InteropServices.ComTypes.ITypeLib
struct ITypeLib_t1_1699;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.SecureString
struct SecureString_t1_1197;
// System.Delegate
struct Delegate_t1_22;
// System.Runtime.InteropServices.MarshalDirectiveException
struct MarshalDirectiveException_t1_812;
// System.Runtime.InteropServices.PreserveSigAttribute
struct PreserveSigAttribute_t1_814;
// System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute
struct PrimaryInteropAssemblyAttribute_t1_815;
// System.Runtime.InteropServices.ProgIdAttribute
struct ProgIdAttribute_t1_816;
// System.Runtime.InteropServices.RegistrationServices
struct RegistrationServices_t1_819;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Runtime.InteropServices.RuntimeEnvironment
struct RuntimeEnvironment_t1_820;
// System.Runtime.InteropServices.SEHException
struct SEHException_t1_821;
// System.Runtime.InteropServices.SafeArrayRankMismatchException
struct SafeArrayRankMismatchException_t1_824;
// System.Runtime.InteropServices.SafeArrayTypeMismatchException
struct SafeArrayTypeMismatchException_t1_825;
// System.Runtime.InteropServices.SafeHandle
struct SafeHandle_t1_88;
// System.Runtime.InteropServices.SetWin32ContextInIDispatchAttribute
struct SetWin32ContextInIDispatchAttribute_t1_826;
// System.Runtime.InteropServices.TypeLibConverter
struct TypeLibConverter_t1_831;
// System.Runtime.InteropServices.ITypeLibExporterNotifySink
struct ITypeLibExporterNotifySink_t1_1700;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1_466;
// System.Runtime.InteropServices.ITypeLibImporterNotifySink
struct ITypeLibImporterNotifySink_t1_1701;
// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_t1_577;
// System.Version
struct Version_t1_578;
// System.Runtime.InteropServices.TypeLibFuncAttribute
struct TypeLibFuncAttribute_t1_833;
// System.Runtime.InteropServices.TypeLibImportClassAttribute
struct TypeLibImportClassAttribute_t1_835;
// System.Runtime.InteropServices.TypeLibTypeAttribute
struct TypeLibTypeAttribute_t1_837;
// System.Runtime.InteropServices.TypeLibVarAttribute
struct TypeLibVarAttribute_t1_839;
// System.Runtime.InteropServices.TypeLibVersionAttribute
struct TypeLibVersionAttribute_t1_841;
// System.Runtime.InteropServices.UnknownWrapper
struct UnknownWrapper_t1_842;
// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t1_843;
// System.Runtime.InteropServices.VariantWrapper
struct VariantWrapper_t1_849;
// System.Runtime.Remoting.Activation.ActivationServices
struct ActivationServices_t1_850;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1_851;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Proxies.RemotingProxy
struct RemotingProxy_t1_932;
// System.Runtime.Remoting.Messaging.ConstructionCall
struct ConstructionCall_t1_930;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
struct AppDomainLevelActivator_t1_853;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
struct ConstructionLevelActivator_t1_854;
// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t1_855;
// System.Runtime.Remoting.Activation.RemoteActivationAttribute
struct RemoteActivationAttribute_t1_856;
// System.Collections.IList
struct IList_t1_262;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Runtime.Remoting.Activation.RemoteActivator
struct RemoteActivator_t1_857;
// System.Runtime.Remoting.Activation.UrlAttribute
struct UrlAttribute_t1_858;
// System.Runtime.Remoting.Channels.AggregateDictionary
struct AggregateDictionary_t1_860;
// System.Collections.IDictionary[]
struct IDictionaryU5BU5D_t1_861;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Runtime.Remoting.Channels.AggregateEnumerator
struct AggregateEnumerator_t1_862;
// System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties
struct BaseChannelObjectWithProperties_t1_864;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Runtime.Remoting.Channels.BaseChannelSinkWithProperties
struct BaseChannelSinkWithProperties_t1_865;
// System.Runtime.Remoting.Channels.BaseChannelWithProperties
struct BaseChannelWithProperties_t1_866;
// System.Runtime.Remoting.Channels.ChannelDataStore
struct ChannelDataStore_t1_868;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t1_870;
// System.Runtime.Remoting.Channels.ChannelServices
struct ChannelServices_t1_871;
// System.Runtime.Remoting.Contexts.CrossContextChannel
struct CrossContextChannel_t1_872;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Channels.IChannelSender
struct IChannelSender_t1_1705;
// System.Runtime.Remoting.Channels.IChannel[]
struct IChannelU5BU5D_t1_1704;
// System.Runtime.Remoting.Channels.IServerChannelSink
struct IServerChannelSink_t1_1706;
// System.Runtime.Remoting.Channels.IServerChannelSinkProvider
struct IServerChannelSinkProvider_t1_1707;
// System.Runtime.Remoting.Channels.IChannelReceiver
struct IChannelReceiver_t1_1708;
// System.Runtime.Remoting.Channels.IServerChannelSinkStack
struct IServerChannelSinkStack_t1_1709;
// System.Runtime.Remoting.Channels.IChannel
struct IChannel_t1_1710;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.ChannelData
struct ChannelData_t1_1022;
// System.Runtime.Remoting.ProviderData
struct ProviderData_t1_1023;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.ReturnMessage
struct ReturnMessage_t1_960;
// System.Runtime.Remoting.Channels.ExceptionFilterSink
struct ExceptionFilterSink_t1_873;
// System.Runtime.Remoting.Channels.ChanelSinkStackEntry
struct ChanelSinkStackEntry_t1_876;
// System.Runtime.Remoting.Channels.IChannelSinkBase
struct IChannelSinkBase_t1_867;
// System.Runtime.Remoting.Channels.ClientChannelSinkStack
struct ClientChannelSinkStack_t1_877;
// System.Runtime.Remoting.Channels.ITransportHeaders
struct ITransportHeaders_t1_1711;
// System.IO.Stream
struct Stream_t1_405;
// System.Runtime.Remoting.Channels.IClientChannelSink
struct IClientChannelSink_t1_1712;
// System.Runtime.Remoting.Channels.CrossAppDomainData
struct CrossAppDomainData_t1_878;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
struct CrossAppDomainChannel_t1_879;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
struct CrossAppDomainSink_t1_882;
// System.Runtime.Remoting.Messaging.CADMethodCallMessage
struct CADMethodCallMessage_t1_925;
// System.Runtime.Remoting.Channels.CADSerializer
struct CADSerializer_t1_883;
// System.IO.MemoryStream
struct MemoryStream_t1_433;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Runtime.Remoting.Channels.AsyncRequest
struct AsyncRequest_t1_884;
// System.Runtime.Remoting.Channels.ServerChannelSinkStack
struct ServerChannelSinkStack_t1_885;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.Runtime.Remoting.Channels.ServerDispatchSink
struct ServerDispatchSink_t1_886;
// System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack
struct IServerResponseChannelSinkStack_t1_1713;
// System.Runtime.Remoting.Channels.ServerDispatchSinkProvider
struct ServerDispatchSinkProvider_t1_887;
// System.Runtime.Remoting.Channels.IChannelDataStore
struct IChannelDataStore_t1_1714;
// System.Runtime.Remoting.Channels.SinkProviderData
struct SinkProviderData_t1_889;
// System.Runtime.Remoting.Channels.TransportHeaders
struct TransportHeaders_t1_890;
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t1_1715;
// System.Runtime.Remoting.Contexts.IDynamicProperty
struct IDynamicProperty_t1_895;
// System.ContextBoundObject
struct ContextBoundObject_t1_897;
// System.Runtime.Remoting.Contexts.DynamicPropertyCollection
struct DynamicPropertyCollection_t1_892;
// System.Runtime.Remoting.Contexts.IContextProperty
struct IContextProperty_t1_1716;
// System.Runtime.Remoting.Contexts.CrossContextDelegate
struct CrossContextDelegate_t1_1622;
// System.LocalDataStoreSlot
struct LocalDataStoreSlot_t1_1564;
// System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg
struct DynamicPropertyReg_t1_894;
// System.Runtime.Remoting.Contexts.ContextCallbackObject
struct ContextCallbackObject_t1_893;
// System.Runtime.Remoting.Contexts.ContextAttribute
struct ContextAttribute_t1_859;
// System.Runtime.Remoting.Contexts.ContextProperty
struct ContextProperty_t1_898;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "mscorlib_System_Runtime_InteropServices_FILETIME.h"
#include "mscorlib_System_Runtime_InteropServices_FILETIMEMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCDESC.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCKIND.h"
#include "mscorlib_System_Runtime_InteropServices_INVOKEKIND.h"
#include "mscorlib_System_Runtime_InteropServices_CALLCONV.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ELEMDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_FUNCKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_LocaleMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRefMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_IDLDESC.h"
#include "mscorlib_System_Runtime_InteropServices_IDLDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_IDLFLAG.h"
#include "mscorlib_System_Runtime_InteropServices_IDLFLAGMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_IDispatchImplAttribu.h"
#include "mscorlib_System_Runtime_InteropServices_IDispatchImplAttribuMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_IDispatchImplType.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Int16.h"
#include "mscorlib_System_Runtime_InteropServices_IDispatchImplTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_IMPLTYPEFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_IMPLTYPEFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_INVOKEKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ImportedFromTypeLibA.h"
#include "mscorlib_System_Runtime_InteropServices_ImportedFromTypeLibAMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ImporterEventKind.h"
#include "mscorlib_System_Runtime_InteropServices_ImporterEventKindMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"
#include "mscorlib_System_Runtime_InteropServices_InvalidComObjectExce.h"
#include "mscorlib_System_Runtime_InteropServices_InvalidComObjectExceMethodDeclarations.h"
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_SystemException.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_InteropServices_InvalidOleVariantTyp.h"
#include "mscorlib_System_Runtime_InteropServices_InvalidOleVariantTypMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_LCIDConversionAttrib.h"
#include "mscorlib_System_Runtime_InteropServices_LCIDConversionAttribMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_LIBFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_LIBFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_LayoutKind.h"
#include "mscorlib_System_Runtime_InteropServices_LayoutKindMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
#include "mscorlib_System_OperatingSystem.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotImplementedException.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System___ComObjectMethodDeclarations.h"
#include "mscorlib_System_InvalidCastExceptionMethodDeclarations.h"
#include "mscorlib_Mono_Interop_ComInteropProxyMethodDeclarations.h"
#include "mscorlib_System___ComObject.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_InvalidCastException.h"
#include "mscorlib_Mono_Interop_ComInteropProxy.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_ModuleMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ComMemberType.h"
#include "mscorlib_System_VariantMethodDeclarations.h"
#include "mscorlib_System_Variant.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_Threading_Thread.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_Assembly.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Security_SecureString.h"
#include "mscorlib_System_Security_SecureStringMethodDeclarations.h"
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_COMExceptionMethodDeclarations.h"
#include "mscorlib_System_OutOfMemoryException.h"
#include "mscorlib_System_Runtime_InteropServices_COMException.h"
#include "mscorlib_System_Delegate.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExce.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExceMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMFLAG.h"
#include "mscorlib_System_Runtime_InteropServices_PARAMFLAGMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_PrimaryInteropAssemb.h"
#include "mscorlib_System_Runtime_InteropServices_PrimaryInteropAssembMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ProgIdAttribute.h"
#include "mscorlib_System_Runtime_InteropServices_ProgIdAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationClassCon.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationClassConMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationConnecti.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationConnectiMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationServices.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationServicesMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_AssemblyRegistration.h"
#include "mscorlib_System_Runtime_InteropServices_RuntimeEnvironment.h"
#include "mscorlib_System_Runtime_InteropServices_RuntimeEnvironmentMethodDeclarations.h"
#include "mscorlib_System_Security_SecurityManagerMethodDeclarations.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionMethodDeclarations.h"
#include "mscorlib_System_Security_Permissions_FileIOPermission.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"
#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_CodeAccessPermissionMethodDeclarations.h"
#include "mscorlib_System_Reflection_AssemblyMethodDeclarations.h"
#include "mscorlib_System_IO_PathMethodDeclarations.h"
#include "mscorlib_System_VersionMethodDeclarations.h"
#include "mscorlib_System_Version.h"
#include "mscorlib_System_Runtime_InteropServices_SEHException.h"
#include "mscorlib_System_Runtime_InteropServices_SEHExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalException.h"
#include "mscorlib_System_Runtime_InteropServices_STATSTG.h"
#include "mscorlib_System_Runtime_InteropServices_STATSTGMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_SYSKIND.h"
#include "mscorlib_System_Runtime_InteropServices_SYSKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_SafeArrayRankMismatc.h"
#include "mscorlib_System_Runtime_InteropServices_SafeArrayRankMismatcMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_SafeArrayTypeMismatc.h"
#include "mscorlib_System_Runtime_InteropServices_SafeArrayTypeMismatcMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinalizMethodDeclarations.h"
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException.h"
#include "mscorlib_System_GCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_SetWin32ContextInIDi.h"
#include "mscorlib_System_Runtime_InteropServices_SetWin32ContextInIDiMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEATTR.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEATTRMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEKIND.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEDESC.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEKINDMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TYPELIBATTR.h"
#include "mscorlib_System_Runtime_InteropServices_TYPELIBATTRMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibConverter.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibConverterMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibExporterFlags.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
#include "mscorlib_System_Reflection_StrongNameKeyPair.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImporterFlags.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibExporterFlagsMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncAttribute.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncFlags.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncFlagsMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImporterFlagsMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeAttribute.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeFlags.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeFlagsMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarAttribute.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarFlags.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVarFlagsMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_UnknownWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_UnknownWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedFunctionPoi.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedFunctionPoiMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTypeMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_VARDESC_DESCUNION.h"
#include "mscorlib_System_Runtime_InteropServices_VARDESC_DESCUNIONMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_VARDESC.h"
#include "mscorlib_System_Runtime_InteropServices_VARDESCMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_VarEnum.h"
#include "mscorlib_System_Runtime_InteropServices_VARFLAGS.h"
#include "mscorlib_System_Runtime_InteropServices_VARFLAGSMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_VarEnumMethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_VariantWrapper.h"
#include "mscorlib_System_Runtime_InteropServices_VariantWrapperMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
#include "mscorlib_System_Threading_ThreadMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Identity.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionRespoMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
#include "mscorlib_System_MarshalByRefObject.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionRespo.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTypeEntry.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivatorLevel.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivatorLevelMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivation.h"
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivationMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
#include "mscorlib_System_MarshalByRefObjectMethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseState.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
#include "mscorlib_System_Runtime_Remoting_Channels_AggregateDictionar.h"
#include "mscorlib_System_Runtime_Remoting_Channels_AggregateDictionarMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_AggregateEnumeratoMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_AggregateEnumerato.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelObjectW.h"
#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelObjectWMethodDeclarations.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable.h"
#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelSinkWit.h"
#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelSinkWitMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelWithPro.h"
#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelWithProMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelDataStore.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelDataStoreMethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne_0MethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne_0.h"
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerDispatchSink_0MethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerDispatchSink_0.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerProcessing.h"
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ChannelData.h"
#include "mscorlib_System_Runtime_Remoting_ChannelDataMethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_ProviderData.h"
#include "mscorlib_System_Reflection_ConstructorInfo.h"
#include "mscorlib_System_Reflection_TargetInvocationException.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ExceptionFilterSinMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ExceptionFilterSin.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodResponseMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodResponse.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChanelSinkStackEnt.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChanelSinkStackEntMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ClientChannelSinkS.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ClientChannelSinkSMethodDeclarations.h"
#include "mscorlib_System_IO_Stream.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink_0MethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink_0.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CADMethodCallMess.h"
#include "mscorlib_System_AppDomainMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ErrorMessageMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CADSerializerMethodDeclarations.h"
#include "mscorlib_System_AppDomain.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CADMethodReturnMe.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ErrorMessage.h"
#include "mscorlib_System_IO_MemoryStream.h"
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CADMethodCallMessMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_AsyncRequestMethodDeclarations.h"
#include "mscorlib_System_Threading_WaitCallbackMethodDeclarations.h"
#include "mscorlib_System_Threading_ThreadPoolMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_AsyncRequest.h"
#include "mscorlib_System_Threading_WaitCallback.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CADSerializer.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_2MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_2.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerChannelSinkS.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerChannelSinkSMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerDispatchSink.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerDispatchSinkMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerProcessingMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderData.h"
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderDataMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Channels_TransportHeaders.h"
#include "mscorlib_System_Runtime_Remoting_Channels_TransportHeadersMethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProviderMethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparerMethodDeclarations.h"
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProvider.h"
#include "mscorlib_System_Collections_CaseInsensitiveComparer.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPropertyCol_0MethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPropertyCol_0.h"
#include "mscorlib_System_ContextBoundObject.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientContextTerm.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerContextTermMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerContextTerm.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientContextTermMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_StackBuilderSinkMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerObjectTermiMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSinkMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_StackBuilderSink.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ServerObjectTermi.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSink.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextDelega.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextCallbackObjMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextCallbackObj.h"
#include "mscorlib_System_LocalDataStoreSlot.h"
#include "mscorlib_System_LocalDataStoreSlotMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPropertyCol.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPropertyColMethodDeclarations.h"
#include "mscorlib_System_ContextBoundObjectMethodDeclarations.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextProperty.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextPropertyMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: System.Runtime.InteropServices.FUNCDESC
extern "C" void FUNCDESC_t1_792_marshal(const FUNCDESC_t1_792& unmarshaled, FUNCDESC_t1_792_marshaled& marshaled)
{
	marshaled.___memid_0 = unmarshaled.___memid_0;
	marshaled.___lprgscode_1 = reinterpret_cast<intptr_t>((unmarshaled.___lprgscode_1).___m_value_0);
	marshaled.___lprgelemdescParam_2 = reinterpret_cast<intptr_t>((unmarshaled.___lprgelemdescParam_2).___m_value_0);
	marshaled.___funckind_3 = unmarshaled.___funckind_3;
	marshaled.___invkind_4 = unmarshaled.___invkind_4;
	marshaled.___callconv_5 = unmarshaled.___callconv_5;
	marshaled.___cParams_6 = unmarshaled.___cParams_6;
	marshaled.___cParamsOpt_7 = unmarshaled.___cParamsOpt_7;
	marshaled.___oVft_8 = unmarshaled.___oVft_8;
	marshaled.___cScodes_9 = unmarshaled.___cScodes_9;
	ELEMDESC_t1_785_marshal(unmarshaled.___elemdescFunc_10, marshaled.___elemdescFunc_10);
	marshaled.___wFuncFlags_11 = unmarshaled.___wFuncFlags_11;
}
extern "C" void FUNCDESC_t1_792_marshal_back(const FUNCDESC_t1_792_marshaled& marshaled, FUNCDESC_t1_792& unmarshaled)
{
	unmarshaled.___memid_0 = marshaled.___memid_0;
	(unmarshaled.___lprgscode_1).___m_value_0 = reinterpret_cast<void*>(marshaled.___lprgscode_1);
	(unmarshaled.___lprgelemdescParam_2).___m_value_0 = reinterpret_cast<void*>(marshaled.___lprgelemdescParam_2);
	unmarshaled.___funckind_3 = marshaled.___funckind_3;
	unmarshaled.___invkind_4 = marshaled.___invkind_4;
	unmarshaled.___callconv_5 = marshaled.___callconv_5;
	unmarshaled.___cParams_6 = marshaled.___cParams_6;
	unmarshaled.___cParamsOpt_7 = marshaled.___cParamsOpt_7;
	unmarshaled.___oVft_8 = marshaled.___oVft_8;
	unmarshaled.___cScodes_9 = marshaled.___cScodes_9;
	ELEMDESC_t1_785_marshal_back(marshaled.___elemdescFunc_10, unmarshaled.___elemdescFunc_10);
	unmarshaled.___wFuncFlags_11 = marshaled.___wFuncFlags_11;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.FUNCDESC
extern "C" void FUNCDESC_t1_792_marshal_cleanup(FUNCDESC_t1_792_marshaled& marshaled)
{
	ELEMDESC_t1_785_marshal_cleanup(marshaled.___elemdescFunc_10);
}
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.IntPtr)
extern "C" void GCHandle__ctor_m1_7649 (GCHandle_t1_795 * __this, IntPtr_t ___h, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___h;
		int32_t L_1 = IntPtr_op_Explicit_m1_845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___handle_0 = L_1;
		return;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object)
extern "C" void GCHandle__ctor_m1_7650 (GCHandle_t1_795 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		GCHandle__ctor_m1_7651(__this, L_0, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::.ctor(System.Object,System.Runtime.InteropServices.GCHandleType)
extern "C" void GCHandle__ctor_m1_7651 (GCHandle_t1_795 * __this, Object_t * ___value, int32_t ___type, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___type;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_0011;
		}
	}

IL_000e:
	{
		___type = 2;
	}

IL_0011:
	{
		Object_t * L_2 = ___value;
		int32_t L_3 = ___type;
		int32_t L_4 = GCHandle_GetTargetHandle_m1_7661(NULL /*static, unused*/, L_2, 0, L_3, /*hidden argument*/NULL);
		__this->___handle_0 = L_4;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.GCHandle::get_IsAllocated()
extern "C" bool GCHandle_get_IsAllocated_m1_7652 (GCHandle_t1_795 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___handle_0);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Object System.Runtime.InteropServices.GCHandle::get_Target()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2110;
extern "C" Object_t * GCHandle_get_Target_m1_7653 (GCHandle_t1_795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral2110 = il2cpp_codegen_string_literal_from_index(2110);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GCHandle_get_IsAllocated_m1_7652(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2110, /*hidden argument*/NULL);
		InvalidOperationException_t1_1559 * L_2 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		int32_t L_3 = (__this->___handle_0);
		Object_t * L_4 = GCHandle_GetTarget_m1_7660(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::set_Target(System.Object)
extern "C" void GCHandle_set_Target_m1_7654 (GCHandle_t1_795 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		int32_t L_1 = (__this->___handle_0);
		int32_t L_2 = GCHandle_GetTargetHandle_m1_7661(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		__this->___handle_0 = L_2;
		return;
	}
}
// System.IntPtr System.Runtime.InteropServices.GCHandle::AddrOfPinnedObject()
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2111;
extern Il2CppCodeGenString* _stringLiteral2112;
extern "C" IntPtr_t GCHandle_AddrOfPinnedObject_m1_7655 (GCHandle_t1_795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral2111 = il2cpp_codegen_string_literal_from_index(2111);
		_stringLiteral2112 = il2cpp_codegen_string_literal_from_index(2112);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		int32_t L_0 = (__this->___handle_0);
		IntPtr_t L_1 = GCHandle_GetAddrOfPinnedObject_m1_7663(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		IntPtr_t L_3 = IntPtr_op_Explicit_m1_842(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		bool L_4 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentException_t1_1425 * L_5 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_5, _stringLiteral2111, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0028:
	{
		IntPtr_t L_6 = V_0;
		IntPtr_t L_7 = IntPtr_op_Explicit_m1_842(NULL /*static, unused*/, ((int32_t)-2), /*hidden argument*/NULL);
		bool L_8 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_9 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_9, _stringLiteral2112, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0045:
	{
		IntPtr_t L_10 = V_0;
		return L_10;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object)
extern "C" GCHandle_t1_795  GCHandle_Alloc_m1_7656 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		GCHandle_t1_795  L_1 = {0};
		GCHandle__ctor_m1_7650(&L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::Alloc(System.Object,System.Runtime.InteropServices.GCHandleType)
extern "C" GCHandle_t1_795  GCHandle_Alloc_m1_7657 (Object_t * __this /* static, unused */, Object_t * ___value, int32_t ___type, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		int32_t L_1 = ___type;
		GCHandle_t1_795  L_2 = {0};
		GCHandle__ctor_m1_7651(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Runtime.InteropServices.GCHandle::Free()
extern "C" void GCHandle_Free_m1_7658 (GCHandle_t1_795 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___handle_0);
		GCHandle_FreeHandle_m1_7662(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___handle_0 = 0;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.GCHandle::CheckCurrentDomain(System.Int32)
extern "C" bool GCHandle_CheckCurrentDomain_m1_7659 (Object_t * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*GCHandle_CheckCurrentDomain_m1_7659_ftn) (int32_t);
	return  ((GCHandle_CheckCurrentDomain_m1_7659_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::CheckCurrentDomain) (___handle);
}
// System.Object System.Runtime.InteropServices.GCHandle::GetTarget(System.Int32)
extern "C" Object_t * GCHandle_GetTarget_m1_7660 (Object_t * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Object_t * (*GCHandle_GetTarget_m1_7660_ftn) (int32_t);
	return  ((GCHandle_GetTarget_m1_7660_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::GetTarget) (___handle);
}
// System.Int32 System.Runtime.InteropServices.GCHandle::GetTargetHandle(System.Object,System.Int32,System.Runtime.InteropServices.GCHandleType)
extern "C" int32_t GCHandle_GetTargetHandle_m1_7661 (Object_t * __this /* static, unused */, Object_t * ___obj, int32_t ___handle, int32_t ___type, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*GCHandle_GetTargetHandle_m1_7661_ftn) (Object_t *, int32_t, int32_t);
	return  ((GCHandle_GetTargetHandle_m1_7661_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::GetTargetHandle) (___obj, ___handle, ___type);
}
// System.Void System.Runtime.InteropServices.GCHandle::FreeHandle(System.Int32)
extern "C" void GCHandle_FreeHandle_m1_7662 (Object_t * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*GCHandle_FreeHandle_m1_7662_ftn) (int32_t);
	 ((GCHandle_FreeHandle_m1_7662_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::FreeHandle) (___handle);
}
// System.IntPtr System.Runtime.InteropServices.GCHandle::GetAddrOfPinnedObject(System.Int32)
extern "C" IntPtr_t GCHandle_GetAddrOfPinnedObject_m1_7663 (Object_t * __this /* static, unused */, int32_t ___handle, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*GCHandle_GetAddrOfPinnedObject_m1_7663_ftn) (int32_t);
	return  ((GCHandle_GetAddrOfPinnedObject_m1_7663_ftn)mscorlib::System::Runtime::InteropServices::GCHandle::GetAddrOfPinnedObject) (___handle);
}
// System.Boolean System.Runtime.InteropServices.GCHandle::Equals(System.Object)
extern TypeInfo* GCHandle_t1_795_il2cpp_TypeInfo_var;
extern "C" bool GCHandle_Equals_m1_7664 (GCHandle_t1_795 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GCHandle_t1_795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(594);
		s_Il2CppMethodIntialized = true;
	}
	GCHandle_t1_795  V_0 = {0};
	{
		Object_t * L_0 = ___o;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Object_t * L_1 = ___o;
		if (((Object_t *)IsInstSealed(L_1, GCHandle_t1_795_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return 0;
	}

IL_0013:
	{
		int32_t L_2 = (__this->___handle_0);
		Object_t * L_3 = ___o;
		V_0 = ((*(GCHandle_t1_795 *)((GCHandle_t1_795 *)UnBox (L_3, GCHandle_t1_795_il2cpp_TypeInfo_var))));
		int32_t L_4 = ((&V_0)->___handle_0);
		return ((((int32_t)L_2) == ((int32_t)L_4))? 1 : 0);
	}
}
// System.Int32 System.Runtime.InteropServices.GCHandle::GetHashCode()
extern "C" int32_t GCHandle_GetHashCode_m1_7665 (GCHandle_t1_795 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = &(__this->___handle_0);
		int32_t L_1 = Int32_GetHashCode_m1_81(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::FromIntPtr(System.IntPtr)
extern "C" GCHandle_t1_795  GCHandle_FromIntPtr_m1_7666 (Object_t * __this /* static, unused */, IntPtr_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___value;
		GCHandle_t1_795  L_1 = GCHandle_op_Explicit_m1_7669(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr System.Runtime.InteropServices.GCHandle::ToIntPtr(System.Runtime.InteropServices.GCHandle)
extern "C" IntPtr_t GCHandle_ToIntPtr_m1_7667 (Object_t * __this /* static, unused */, GCHandle_t1_795  ___value, const MethodInfo* method)
{
	{
		GCHandle_t1_795  L_0 = ___value;
		IntPtr_t L_1 = GCHandle_op_Explicit_m1_7668(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr System.Runtime.InteropServices.GCHandle::op_Explicit(System.Runtime.InteropServices.GCHandle)
extern "C" IntPtr_t GCHandle_op_Explicit_m1_7668 (Object_t * __this /* static, unused */, GCHandle_t1_795  ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___value)->___handle_0);
		IntPtr_t L_1 = IntPtr_op_Explicit_m1_842(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.InteropServices.GCHandle System.Runtime.InteropServices.GCHandle::op_Explicit(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2113;
extern Il2CppCodeGenString* _stringLiteral2114;
extern "C" GCHandle_t1_795  GCHandle_op_Explicit_m1_7669 (Object_t * __this /* static, unused */, IntPtr_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral2113 = il2cpp_codegen_string_literal_from_index(2113);
		_stringLiteral2114 = il2cpp_codegen_string_literal_from_index(2114);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___value;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, _stringLiteral2113, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001b:
	{
		IntPtr_t L_4 = ___value;
		int32_t L_5 = IntPtr_op_Explicit_m1_845(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		bool L_6 = GCHandle_CheckCurrentDomain_m1_7659(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0036;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_7, _stringLiteral2114, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0036:
	{
		IntPtr_t L_8 = ___value;
		GCHandle_t1_795  L_9 = {0};
		GCHandle__ctor_m1_7649(&L_9, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean System.Runtime.InteropServices.GCHandle::op_Equality(System.Runtime.InteropServices.GCHandle,System.Runtime.InteropServices.GCHandle)
extern TypeInfo* GCHandle_t1_795_il2cpp_TypeInfo_var;
extern "C" bool GCHandle_op_Equality_m1_7670 (Object_t * __this /* static, unused */, GCHandle_t1_795  ___a, GCHandle_t1_795  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GCHandle_t1_795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(594);
		s_Il2CppMethodIntialized = true;
	}
	{
		GCHandle_t1_795  L_0 = ___b;
		GCHandle_t1_795  L_1 = L_0;
		Object_t * L_2 = Box(GCHandle_t1_795_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = GCHandle_Equals_m1_7664((&___a), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean System.Runtime.InteropServices.GCHandle::op_Inequality(System.Runtime.InteropServices.GCHandle,System.Runtime.InteropServices.GCHandle)
extern TypeInfo* GCHandle_t1_795_il2cpp_TypeInfo_var;
extern "C" bool GCHandle_op_Inequality_m1_7671 (Object_t * __this /* static, unused */, GCHandle_t1_795  ___a, GCHandle_t1_795  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GCHandle_t1_795_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(594);
		s_Il2CppMethodIntialized = true;
	}
	{
		GCHandle_t1_795  L_0 = ___b;
		GCHandle_t1_795  L_1 = L_0;
		Object_t * L_2 = Box(GCHandle_t1_795_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = GCHandle_Equals_m1_7664((&___a), L_2, /*hidden argument*/NULL);
		return ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
extern "C" void HandleRef__ctor_m1_7672 (HandleRef_t1_797 * __this, Object_t * ___wrapper, IntPtr_t ___handle, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___wrapper;
		__this->___wrapper_0 = L_0;
		IntPtr_t L_1 = ___handle;
		__this->___handle_1 = L_1;
		return;
	}
}
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
extern "C" IntPtr_t HandleRef_get_Handle_m1_7673 (HandleRef_t1_797 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___handle_1);
		return L_0;
	}
}
// System.Object System.Runtime.InteropServices.HandleRef::get_Wrapper()
extern "C" Object_t * HandleRef_get_Wrapper_m1_7674 (HandleRef_t1_797 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___wrapper_0);
		return L_0;
	}
}
// System.IntPtr System.Runtime.InteropServices.HandleRef::ToIntPtr(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t HandleRef_ToIntPtr_m1_7675 (Object_t * __this /* static, unused */, HandleRef_t1_797  ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = HandleRef_get_Handle_m1_7673((&___value), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IntPtr System.Runtime.InteropServices.HandleRef::op_Explicit(System.Runtime.InteropServices.HandleRef)
extern "C" IntPtr_t HandleRef_op_Explicit_m1_7676 (Object_t * __this /* static, unused */, HandleRef_t1_797  ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = HandleRef_get_Handle_m1_7673((&___value), /*hidden argument*/NULL);
		return L_0;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.IDLDESC
extern "C" void IDLDESC_t1_783_marshal(const IDLDESC_t1_783& unmarshaled, IDLDESC_t1_783_marshaled& marshaled)
{
	marshaled.___dwReserved_0 = unmarshaled.___dwReserved_0;
	marshaled.___wIDLFlags_1 = unmarshaled.___wIDLFlags_1;
}
extern "C" void IDLDESC_t1_783_marshal_back(const IDLDESC_t1_783_marshaled& marshaled, IDLDESC_t1_783& unmarshaled)
{
	unmarshaled.___dwReserved_0 = marshaled.___dwReserved_0;
	unmarshaled.___wIDLFlags_1 = marshaled.___wIDLFlags_1;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.IDLDESC
extern "C" void IDLDESC_t1_783_marshal_cleanup(IDLDESC_t1_783_marshaled& marshaled)
{
}
// System.Void System.Runtime.InteropServices.IDispatchImplAttribute::.ctor(System.Runtime.InteropServices.IDispatchImplType)
extern "C" void IDispatchImplAttribute__ctor_m1_7677 (IDispatchImplAttribute_t1_799 * __this, int32_t ___implType, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___implType;
		__this->___Impl_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.IDispatchImplAttribute::.ctor(System.Int16)
extern "C" void IDispatchImplAttribute__ctor_m1_7678 (IDispatchImplAttribute_t1_799 * __this, int16_t ___implType, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int16_t L_0 = ___implType;
		__this->___Impl_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.IDispatchImplType System.Runtime.InteropServices.IDispatchImplAttribute::get_Value()
extern "C" int32_t IDispatchImplAttribute_get_Value_m1_7679 (IDispatchImplAttribute_t1_799 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___Impl_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ImportedFromTypeLibAttribute::.ctor(System.String)
extern "C" void ImportedFromTypeLibAttribute__ctor_m1_7680 (ImportedFromTypeLibAttribute_t1_803 * __this, String_t* ___tlbFile, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tlbFile;
		__this->___TlbFile_0 = L_0;
		return;
	}
}
// System.String System.Runtime.InteropServices.ImportedFromTypeLibAttribute::get_Value()
extern "C" String_t* ImportedFromTypeLibAttribute_get_Value_m1_7681 (ImportedFromTypeLibAttribute_t1_803 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___TlbFile_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern "C" void InterfaceTypeAttribute__ctor_m1_7682 (InterfaceTypeAttribute_t1_805 * __this, int32_t ___interfaceType, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___interfaceType;
		__this->___intType_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Int16)
extern "C" void InterfaceTypeAttribute__ctor_m1_7683 (InterfaceTypeAttribute_t1_805 * __this, int16_t ___interfaceType, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int16_t L_0 = ___interfaceType;
		__this->___intType_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.ComInterfaceType System.Runtime.InteropServices.InterfaceTypeAttribute::get_Value()
extern "C" int32_t InterfaceTypeAttribute_get_Value_m1_7684 (InterfaceTypeAttribute_t1_805 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___intType_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.InvalidComObjectException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2115;
extern "C" void InvalidComObjectException__ctor_m1_7685 (InvalidComObjectException_t1_806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2115 = il2cpp_codegen_string_literal_from_index(2115);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2115, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233049), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidComObjectException::.ctor(System.String)
extern "C" void InvalidComObjectException__ctor_m1_7686 (InvalidComObjectException_t1_806 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233049), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidComObjectException::.ctor(System.String,System.Exception)
extern "C" void InvalidComObjectException__ctor_m1_7687 (InvalidComObjectException_t1_806 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233049), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidComObjectException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void InvalidComObjectException__ctor_m1_7688 (InvalidComObjectException_t1_806 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2116;
extern "C" void InvalidOleVariantTypeException__ctor_m1_7689 (InvalidOleVariantTypeException_t1_807 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2116 = il2cpp_codegen_string_literal_from_index(2116);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2116, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233039), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor(System.String)
extern "C" void InvalidOleVariantTypeException__ctor_m1_7690 (InvalidOleVariantTypeException_t1_807 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233039), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor(System.String,System.Exception)
extern "C" void InvalidOleVariantTypeException__ctor_m1_7691 (InvalidOleVariantTypeException_t1_807 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233039), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void InvalidOleVariantTypeException__ctor_m1_7692 (InvalidOleVariantTypeException_t1_807 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.LCIDConversionAttribute::.ctor(System.Int32)
extern "C" void LCIDConversionAttribute__ctor_m1_7693 (LCIDConversionAttribute_t1_808 * __this, int32_t ___lcid, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___lcid;
		__this->___id_0 = L_0;
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.LCIDConversionAttribute::get_Value()
extern "C" int32_t LCIDConversionAttribute_get_Value_m1_7694 (LCIDConversionAttribute_t1_808 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___id_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::.cctor()
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal__cctor_m1_7695 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		((Marshal_t1_811_StaticFields*)Marshal_t1_811_il2cpp_TypeInfo_var->static_fields)->___SystemMaxDBCSCharSize_0 = 2;
		OperatingSystem_t1_1545 * L_0 = Environment_get_OSVersion_m1_14055(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = OperatingSystem_get_Platform_m1_14531(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_001c;
		}
	}
	{
		G_B3_0 = 2;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = 1;
	}

IL_001d:
	{
		((Marshal_t1_811_StaticFields*)Marshal_t1_811_il2cpp_TypeInfo_var->static_fields)->___SystemDefaultCharSize_1 = G_B3_0;
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::AddRefInternal(System.IntPtr)
extern "C" int32_t Marshal_AddRefInternal_m1_7696 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_AddRefInternal_m1_7696_ftn) (IntPtr_t);
	return  ((Marshal_AddRefInternal_m1_7696_ftn)mscorlib::System::Runtime::InteropServices::Marshal::AddRefInternal) (___pUnk);
}
// System.Int32 System.Runtime.InteropServices.Marshal::AddRef(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2117;
extern Il2CppCodeGenString* _stringLiteral2118;
extern "C" int32_t Marshal_AddRef_m1_7697 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral2117 = il2cpp_codegen_string_literal_from_index(2117);
		_stringLiteral2118 = il2cpp_codegen_string_literal_from_index(2118);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pUnk;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_3, _stringLiteral2117, _stringLiteral2118, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___pUnk;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_5 = Marshal_AddRefInternal_m1_7696(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocCoTaskMem(System.Int32)
extern "C" IntPtr_t Marshal_AllocCoTaskMem_m1_7698 (Object_t * __this /* static, unused */, int32_t ___cb, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_AllocCoTaskMem_m1_7698_ftn) (int32_t);
	return  ((Marshal_AllocCoTaskMem_m1_7698_ftn)mscorlib::System::Runtime::InteropServices::Marshal::AllocCoTaskMem) (___cb);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.IntPtr)
extern "C" IntPtr_t Marshal_AllocHGlobal_m1_7699 (Object_t * __this /* static, unused */, IntPtr_t ___cb, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_AllocHGlobal_m1_7699_ftn) (IntPtr_t);
	return  ((Marshal_AllocHGlobal_m1_7699_ftn)mscorlib::System::Runtime::InteropServices::Marshal::AllocHGlobal) (___cb);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_AllocHGlobal_m1_7700 (Object_t * __this /* static, unused */, int32_t ___cb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___cb;
		IntPtr_t L_1 = IntPtr_op_Explicit_m1_842(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = Marshal_AllocHGlobal_m1_7699(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object System.Runtime.InteropServices.Marshal::BindToMoniker(System.String)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Object_t * Marshal_BindToMoniker_m1_7701 (Object_t * __this /* static, unused */, String_t* ___monikerName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ChangeWrapperHandleStrength(System.Object,System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_ChangeWrapperHandleStrength_m1_7702 (Object_t * __this /* static, unused */, Object_t * ___otp, bool ___fIsWeak, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::copy_to_unmanaged(System.Array,System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_copy_to_unmanaged_m1_7703 (Object_t * __this /* static, unused */, Array_t * ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_copy_to_unmanaged_m1_7703_ftn) (Array_t *, int32_t, IntPtr_t, int32_t);
	 ((Marshal_copy_to_unmanaged_m1_7703_ftn)mscorlib::System::Runtime::InteropServices::Marshal::copy_to_unmanaged) (___source, ___startIndex, ___destination, ___length);
}
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
extern "C" void Marshal_copy_from_unmanaged_m1_7704 (Object_t * __this /* static, unused */, IntPtr_t ___source, int32_t ___startIndex, Array_t * ___destination, int32_t ___length, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_copy_from_unmanaged_m1_7704_ftn) (IntPtr_t, int32_t, Array_t *, int32_t);
	 ((Marshal_copy_from_unmanaged_m1_7704_ftn)mscorlib::System::Runtime::InteropServices::Marshal::copy_from_unmanaged) (___source, ___startIndex, ___destination, ___length);
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Byte[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7705 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t1_109* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Char[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7706 (Object_t * __this /* static, unused */, CharU5BU5D_t1_16* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t1_16* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Int16[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7707 (Object_t * __this /* static, unused */, Int16U5BU5D_t1_1694* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int16U5BU5D_t1_1694* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Int32[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7708 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_275* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t1_275* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Int64[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7709 (Object_t * __this /* static, unused */, Int64U5BU5D_t1_1665* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int64U5BU5D_t1_1665* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Single[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7710 (Object_t * __this /* static, unused */, SingleU5BU5D_t1_1695* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleU5BU5D_t1_1695* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Double[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7711 (Object_t * __this /* static, unused */, DoubleU5BU5D_t1_1666* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		DoubleU5BU5D_t1_1666* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr[],System.Int32,System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7712 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1_34* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtrU5BU5D_t1_34* L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtr_t L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7713 (Object_t * __this /* static, unused */, IntPtr_t ___source, ByteU5BU5D_t1_109* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		ByteU5BU5D_t1_109* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7714 (Object_t * __this /* static, unused */, IntPtr_t ___source, CharU5BU5D_t1_16* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		CharU5BU5D_t1_16* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Int16[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7715 (Object_t * __this /* static, unused */, IntPtr_t ___source, Int16U5BU5D_t1_1694* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		Int16U5BU5D_t1_1694* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Int32[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7716 (Object_t * __this /* static, unused */, IntPtr_t ___source, Int32U5BU5D_t1_275* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		Int32U5BU5D_t1_275* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Int64[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7717 (Object_t * __this /* static, unused */, IntPtr_t ___source, Int64U5BU5D_t1_1665* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		Int64U5BU5D_t1_1665* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7718 (Object_t * __this /* static, unused */, IntPtr_t ___source, SingleU5BU5D_t1_1695* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		SingleU5BU5D_t1_1695* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Double[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7719 (Object_t * __this /* static, unused */, IntPtr_t ___source, DoubleU5BU5D_t1_1666* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		DoubleU5BU5D_t1_1666* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.IntPtr[],System.Int32,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_Copy_m1_7720 (Object_t * __this /* static, unused */, IntPtr_t ___source, IntPtrU5BU5D_t1_34* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___source;
		int32_t L_1 = ___startIndex;
		IntPtrU5BU5D_t1_34* L_2 = ___destination;
		int32_t L_3 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_from_unmanaged_m1_7704(NULL /*static, unused*/, L_0, L_1, (Array_t *)(Array_t *)L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::CreateAggregatedObject(System.IntPtr,System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_CreateAggregatedObject_m1_7721 (Object_t * __this /* static, unused */, IntPtr_t ___pOuter, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Object System.Runtime.InteropServices.Marshal::CreateWrapperOfType(System.Object,System.Type)
extern TypeInfo* __ComObject_t1_131_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t1_1558_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2119;
extern Il2CppCodeGenString* _stringLiteral2120;
extern Il2CppCodeGenString* _stringLiteral1433;
extern "C" Object_t * Marshal_CreateWrapperOfType_m1_7722 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		__ComObject_t1_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		InvalidCastException_t1_1558_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		_stringLiteral2119 = il2cpp_codegen_string_literal_from_index(2119);
		_stringLiteral2120 = il2cpp_codegen_string_literal_from_index(2120);
		_stringLiteral1433 = il2cpp_codegen_string_literal_from_index(1433);
		s_Il2CppMethodIntialized = true;
	}
	__ComObject_t1_131 * V_0 = {0};
	TypeU5BU5D_t1_31* V_1 = {0};
	Type_t * V_2 = {0};
	TypeU5BU5D_t1_31* V_3 = {0};
	int32_t V_4 = 0;
	{
		Object_t * L_0 = ___o;
		V_0 = ((__ComObject_t1_131 *)IsInstClass(L_0, __ComObject_t1_131_il2cpp_TypeInfo_var));
		__ComObject_t1_131 * L_1 = V_0;
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t1_1425 * L_2 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_2, _stringLiteral2119, _stringLiteral2120, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001d:
	{
		Type_t * L_3 = ___t;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_4 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_4, _stringLiteral1433, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_002e:
	{
		Object_t * L_5 = ___o;
		NullCheck(L_5);
		Type_t * L_6 = Object_GetType_m1_5(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		TypeU5BU5D_t1_31* L_7 = (TypeU5BU5D_t1_31*)VirtFuncInvoker0< TypeU5BU5D_t1_31* >::Invoke(168 /* System.Type[] System.Type::GetInterfaces() */, L_6);
		V_1 = L_7;
		TypeU5BU5D_t1_31* L_8 = V_1;
		V_3 = L_8;
		V_4 = 0;
		goto IL_0076;
	}

IL_0044:
	{
		TypeU5BU5D_t1_31* L_9 = V_3;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_2 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_9, L_11, sizeof(Type_t *)));
		Type_t * L_12 = V_2;
		NullCheck(L_12);
		bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(129 /* System.Boolean System.Type::get_IsImport() */, L_12);
		if (!L_13)
		{
			goto IL_0070;
		}
	}
	{
		__ComObject_t1_131 * L_14 = V_0;
		Type_t * L_15 = V_2;
		NullCheck(L_14);
		IntPtr_t L_16 = __ComObject_GetInterface_m1_14770(L_14, L_15, /*hidden argument*/NULL);
		IntPtr_t L_17 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_18 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0070;
		}
	}
	{
		InvalidCastException_t1_1558 * L_19 = (InvalidCastException_t1_1558 *)il2cpp_codegen_object_new (InvalidCastException_t1_1558_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m1_14165(L_19, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_19);
	}

IL_0070:
	{
		int32_t L_20 = V_4;
		V_4 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_21 = V_4;
		TypeU5BU5D_t1_31* L_22 = V_3;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))))))
		{
			goto IL_0044;
		}
	}
	{
		__ComObject_t1_131 * L_23 = V_0;
		NullCheck(L_23);
		IntPtr_t L_24 = __ComObject_get_IUnknown_m1_14772(L_23, /*hidden argument*/NULL);
		Type_t * L_25 = ___t;
		ComInteropProxy_t1_129 * L_26 = ComInteropProxy_GetProxy_m1_1738(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Object_t * L_27 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(11 /* System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy() */, L_26);
		return L_27;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::DestroyStructure(System.IntPtr,System.Type)
extern "C" void Marshal_DestroyStructure_m1_7723 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___structuretype, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_DestroyStructure_m1_7723_ftn) (IntPtr_t, Type_t *);
	 ((Marshal_DestroyStructure_m1_7723_ftn)mscorlib::System::Runtime::InteropServices::Marshal::DestroyStructure) (___ptr, ___structuretype);
}
// System.Void System.Runtime.InteropServices.Marshal::FreeBSTR(System.IntPtr)
extern "C" void Marshal_FreeBSTR_m1_7724 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_FreeBSTR_m1_7724_ftn) (IntPtr_t);
	 ((Marshal_FreeBSTR_m1_7724_ftn)mscorlib::System::Runtime::InteropServices::Marshal::FreeBSTR) (___ptr);
}
// System.Void System.Runtime.InteropServices.Marshal::FreeCoTaskMem(System.IntPtr)
extern "C" void Marshal_FreeCoTaskMem_m1_7725 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_FreeCoTaskMem_m1_7725_ftn) (IntPtr_t);
	 ((Marshal_FreeCoTaskMem_m1_7725_ftn)mscorlib::System::Runtime::InteropServices::Marshal::FreeCoTaskMem) (___ptr);
}
// System.Void System.Runtime.InteropServices.Marshal::FreeHGlobal(System.IntPtr)
extern "C" void Marshal_FreeHGlobal_m1_7726 (Object_t * __this /* static, unused */, IntPtr_t ___hglobal, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_FreeHGlobal_m1_7726_ftn) (IntPtr_t);
	 ((Marshal_FreeHGlobal_m1_7726_ftn)mscorlib::System::Runtime::InteropServices::Marshal::FreeHGlobal) (___hglobal);
}
// System.Void System.Runtime.InteropServices.Marshal::ClearBSTR(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ClearBSTR_m1_7727 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IntPtr_t L_0 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_ReadInt32_m1_7806(NULL /*static, unused*/, L_0, ((int32_t)-4), /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		goto IL_001c;
	}

IL_0010:
	{
		IntPtr_t L_2 = ___ptr;
		int32_t L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteByte_m1_7841(NULL /*static, unused*/, L_2, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeBSTR(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ZeroFreeBSTR_m1_7728 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_ClearBSTR_m1_7727(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___s;
		Marshal_FreeBSTR_m1_7724(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ClearAnsi(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ClearAnsi_m1_7729 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0013;
	}

IL_0007:
	{
		IntPtr_t L_0 = ___ptr;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteByte_m1_7841(NULL /*static, unused*/, L_0, L_1, 0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0013:
	{
		IntPtr_t L_3 = ___ptr;
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		uint8_t L_5 = Marshal_ReadByte_m1_7800(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ClearUnicode(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ClearUnicode_m1_7730 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0013;
	}

IL_0007:
	{
		IntPtr_t L_0 = ___ptr;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteInt16_m1_7844(NULL /*static, unused*/, L_0, L_1, 0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)2));
	}

IL_0013:
	{
		IntPtr_t L_3 = ___ptr;
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int16_t L_5 = Marshal_ReadInt16_m1_7803(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeCoTaskMemAnsi(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ZeroFreeCoTaskMemAnsi_m1_7731 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_ClearAnsi_m1_7729(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___s;
		Marshal_FreeCoTaskMem_m1_7725(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeCoTaskMemUnicode(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ZeroFreeCoTaskMemUnicode_m1_7732 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_ClearUnicode_m1_7730(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___s;
		Marshal_FreeCoTaskMem_m1_7725(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeGlobalAllocAnsi(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ZeroFreeGlobalAllocAnsi_m1_7733 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_ClearAnsi_m1_7729(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___s;
		Marshal_FreeHGlobal_m1_7726(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeGlobalAllocUnicode(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ZeroFreeGlobalAllocUnicode_m1_7734 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_ClearUnicode_m1_7730(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___s;
		Marshal_FreeHGlobal_m1_7726(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid System.Runtime.InteropServices.Marshal::GenerateGuidForType(System.Type)
extern "C" Guid_t1_319  Marshal_GenerateGuidForType_m1_7735 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		Guid_t1_319  L_1 = (Guid_t1_319 )VirtFuncInvoker0< Guid_t1_319  >::Invoke(160 /* System.Guid System.Type::get_GUID() */, L_0);
		return L_1;
	}
}
// System.String System.Runtime.InteropServices.Marshal::GenerateProgIdForType(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_GenerateProgIdForType_m1_7736 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Object System.Runtime.InteropServices.Marshal::GetActiveObject(System.String)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Object_t * Marshal_GetActiveObject_m1_7737 (Object_t * __this /* static, unused */, String_t* ___progID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetCCW(System.Object,System.Type)
extern "C" IntPtr_t Marshal_GetCCW_m1_7738 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___T, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_GetCCW_m1_7738_ftn) (Object_t *, Type_t *);
	return  ((Marshal_GetCCW_m1_7738_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetCCW) (___o, ___T);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetComInterfaceForObjectInternal(System.Object,System.Type)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* __ComObject_t1_131_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetComInterfaceForObjectInternal_m1_7739 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___T, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		__ComObject_t1_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		bool L_1 = Marshal_IsComObject_m1_7781(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Object_t * L_2 = ___o;
		Type_t * L_3 = ___T;
		NullCheck(((__ComObject_t1_131 *)CastclassClass(L_2, __ComObject_t1_131_il2cpp_TypeInfo_var)));
		IntPtr_t L_4 = __ComObject_GetInterface_m1_14770(((__ComObject_t1_131 *)CastclassClass(L_2, __ComObject_t1_131_il2cpp_TypeInfo_var)), L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0018:
	{
		Object_t * L_5 = ___o;
		Type_t * L_6 = ___T;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_7 = Marshal_GetCCW_m1_7738(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetComInterfaceForObject(System.Object,System.Type)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetComInterfaceForObject_m1_7740 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___T, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Object_t * L_0 = ___o;
		Type_t * L_1 = ___T;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = Marshal_GetComInterfaceForObjectInternal_m1_7739(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		IntPtr_t L_3 = V_0;
		Marshal_AddRef_m1_7697(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetComInterfaceForObjectInContext(System.Object,System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetComInterfaceForObjectInContext_m1_7741 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Object System.Runtime.InteropServices.Marshal::GetComObjectData(System.Object,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2121;
extern "C" Object_t * Marshal_GetComObjectData_m1_7742 (Object_t * __this /* static, unused */, Object_t * ___obj, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2121 = il2cpp_codegen_string_literal_from_index(2121);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2121, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetComSlotForMethodInfoInternal(System.Reflection.MemberInfo)
extern "C" int32_t Marshal_GetComSlotForMethodInfoInternal_m1_7743 (Object_t * __this /* static, unused */, MemberInfo_t * ___m, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_GetComSlotForMethodInfoInternal_m1_7743_ftn) (MemberInfo_t *);
	return  ((Marshal_GetComSlotForMethodInfoInternal_m1_7743_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetComSlotForMethodInfoInternal) (___m);
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetComSlotForMethodInfo(System.Reflection.MemberInfo)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* MethodInfo_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1541;
extern Il2CppCodeGenString* _stringLiteral2122;
extern "C" int32_t Marshal_GetComSlotForMethodInfo_m1_7744 (Object_t * __this /* static, unused */, MemberInfo_t * ___m, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		MethodInfo_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(62);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral1541 = il2cpp_codegen_string_literal_from_index(1541);
		_stringLiteral2122 = il2cpp_codegen_string_literal_from_index(2122);
		s_Il2CppMethodIntialized = true;
	}
	{
		MemberInfo_t * L_0 = ___m;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1541, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		MemberInfo_t * L_2 = ___m;
		if (((MethodInfo_t *)IsInstClass(L_2, MethodInfo_t_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_3, _stringLiteral2122, _stringLiteral1541, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002c:
	{
		MemberInfo_t * L_4 = ___m;
		NullCheck(L_4);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(22 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_4);
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(130 /* System.Boolean System.Type::get_IsInterface() */, L_5);
		if (L_6)
		{
			goto IL_004c;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_7, _stringLiteral2122, _stringLiteral1541, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_004c:
	{
		MemberInfo_t * L_8 = ___m;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_9 = Marshal_GetComSlotForMethodInfoInternal_m1_7743(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetEndComSlot(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_GetEndComSlot_m1_7745 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetExceptionCode()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_GetExceptionCode_m1_7746 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetExceptionPointers()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetExceptionPointers_m1_7747 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetHINSTANCE(System.Reflection.Module)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1541;
extern "C" IntPtr_t Marshal_GetHINSTANCE_m1_7748 (Object_t * __this /* static, unused */, Module_t1_495 * ___m, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral1541 = il2cpp_codegen_string_literal_from_index(1541);
		s_Il2CppMethodIntialized = true;
	}
	{
		Module_t1_495 * L_0 = ___m;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1541, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Module_t1_495 * L_2 = ___m;
		NullCheck(L_2);
		IntPtr_t L_3 = Module_GetHINSTANCE_m1_7010(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetHRForException(System.Exception)
extern "C" int32_t Marshal_GetHRForException_m1_7749 (Object_t * __this /* static, unused */, Exception_t1_33 * ___e, const MethodInfo* method)
{
	{
		Exception_t1_33 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___hresult_8);
		return L_1;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetHRForLastWin32Error()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_GetHRForLastWin32Error_m1_7750 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIDispatchForObjectInternal(System.Object)
extern "C" IntPtr_t Marshal_GetIDispatchForObjectInternal_m1_7751 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_GetIDispatchForObjectInternal_m1_7751_ftn) (Object_t *);
	return  ((Marshal_GetIDispatchForObjectInternal_m1_7751_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetIDispatchForObjectInternal) (___o);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIDispatchForObject(System.Object)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetIDispatchForObject_m1_7752 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Object_t * L_0 = ___o;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_GetIDispatchForObjectInternal_m1_7751(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		Marshal_AddRef_m1_7697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIDispatchForObjectInContext(System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetIDispatchForObjectInContext_m1_7753 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetITypeInfoForType(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetITypeInfoForType_m1_7754 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIUnknownForObjectInternal(System.Object)
extern "C" IntPtr_t Marshal_GetIUnknownForObjectInternal_m1_7755 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_GetIUnknownForObjectInternal_m1_7755_ftn) (Object_t *);
	return  ((Marshal_GetIUnknownForObjectInternal_m1_7755_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetIUnknownForObjectInternal) (___o);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIUnknownForObject(System.Object)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetIUnknownForObject_m1_7756 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Object_t * L_0 = ___o;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_GetIUnknownForObjectInternal_m1_7755(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		Marshal_AddRef_m1_7697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		return L_3;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIUnknownForObjectInContext(System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetIUnknownForObjectInContext_m1_7757 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetManagedThunkForUnmanagedMethodPtr(System.IntPtr,System.IntPtr,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetManagedThunkForUnmanagedMethodPtr_m1_7758 (Object_t * __this /* static, unused */, IntPtr_t ___pfnMethodToWrap, IntPtr_t ___pbSignature, int32_t ___cbSignature, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Reflection.MemberInfo System.Runtime.InteropServices.Marshal::GetMethodInfoForComSlot(System.Type,System.Int32,System.Runtime.InteropServices.ComMemberType&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" MemberInfo_t * Marshal_GetMethodInfoForComSlot_m1_7759 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___slot, int32_t* ___memberType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::GetNativeVariantForObject(System.Object,System.IntPtr)
extern TypeInfo* Variant_t1_1617_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_GetNativeVariantForObject_m1_7760 (Object_t * __this /* static, unused */, Object_t * ___obj, IntPtr_t ___pDstNativeVariant, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Variant_t1_1617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	Variant_t1_1617  V_0 = {0};
	{
		Initobj (Variant_t1_1617_il2cpp_TypeInfo_var, (&V_0));
		Object_t * L_0 = ___obj;
		Variant_SetValue_m1_14719((&V_0), L_0, /*hidden argument*/NULL);
		Variant_t1_1617  L_1 = V_0;
		Variant_t1_1617  L_2 = L_1;
		Object_t * L_3 = Box(Variant_t1_1617_il2cpp_TypeInfo_var, &L_2);
		IntPtr_t L_4 = ___pDstNativeVariant;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_StructureToPtr_m1_7836(NULL /*static, unused*/, L_3, L_4, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Runtime.InteropServices.Marshal::GetObjectForCCW(System.IntPtr)
extern "C" Object_t * Marshal_GetObjectForCCW_m1_7761 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Object_t * (*Marshal_GetObjectForCCW_m1_7761_ftn) (IntPtr_t);
	return  ((Marshal_GetObjectForCCW_m1_7761_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetObjectForCCW) (___pUnk);
}
// System.Object System.Runtime.InteropServices.Marshal::GetObjectForIUnknown(System.IntPtr)
extern const Il2CppType* __ComObject_t1_131_0_0_0_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Object_t * Marshal_GetObjectForIUnknown_m1_7762 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		__ComObject_t1_131_0_0_0_var = il2cpp_codegen_type_from_index(175);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ComInteropProxy_t1_129 * V_1 = {0};
	{
		IntPtr_t L_0 = ___pUnk;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Object_t * L_1 = Marshal_GetObjectForCCW_m1_7761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		IntPtr_t L_3 = ___pUnk;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(__ComObject_t1_131_0_0_0_var), /*hidden argument*/NULL);
		ComInteropProxy_t1_129 * L_5 = ComInteropProxy_GetProxy_m1_1738(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		ComInteropProxy_t1_129 * L_6 = V_1;
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(11 /* System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy() */, L_6);
		V_0 = L_7;
	}

IL_0025:
	{
		Object_t * L_8 = V_0;
		return L_8;
	}
}
// System.Object System.Runtime.InteropServices.Marshal::GetObjectForNativeVariant(System.IntPtr)
extern const Il2CppType* Variant_t1_1617_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* Variant_t1_1617_il2cpp_TypeInfo_var;
extern "C" Object_t * Marshal_GetObjectForNativeVariant_m1_7763 (Object_t * __this /* static, unused */, IntPtr_t ___pSrcNativeVariant, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Variant_t1_1617_0_0_0_var = il2cpp_codegen_type_from_index(595);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		Variant_t1_1617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(595);
		s_Il2CppMethodIntialized = true;
	}
	Variant_t1_1617  V_0 = {0};
	{
		IntPtr_t L_0 = ___pSrcNativeVariant;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Variant_t1_1617_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Object_t * L_2 = Marshal_PtrToStructure_m1_7796(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((*(Variant_t1_1617 *)((Variant_t1_1617 *)UnBox (L_2, Variant_t1_1617_il2cpp_TypeInfo_var))));
		Object_t * L_3 = Variant_GetValue_m1_14720((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Object[] System.Runtime.InteropServices.Marshal::GetObjectsForNativeVariants(System.IntPtr,System.Int32)
extern const Il2CppType* Variant_t1_1617_0_0_0_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123;
extern Il2CppCodeGenString* _stringLiteral2124;
extern "C" ObjectU5BU5D_t1_272* Marshal_GetObjectsForNativeVariants_m1_7764 (Object_t * __this /* static, unused */, IntPtr_t ___aSrcNativeVariant, int32_t ___cVars, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Variant_t1_1617_0_0_0_var = il2cpp_codegen_type_from_index(595);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral2123 = il2cpp_codegen_string_literal_from_index(2123);
		_stringLiteral2124 = il2cpp_codegen_string_literal_from_index(2124);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_272* V_0 = {0};
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___cVars;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13275(L_1, _stringLiteral2123, _stringLiteral2124, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		int32_t L_2 = ___cVars;
		V_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, L_2));
		V_1 = 0;
		goto IL_0050;
	}

IL_0025:
	{
		ObjectU5BU5D_t1_272* L_3 = V_0;
		int32_t L_4 = V_1;
		int64_t L_5 = IntPtr_ToInt64_m1_836((&___aSrcNativeVariant), /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Variant_t1_1617_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_8 = Marshal_SizeOf_m1_7823(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_9 = IntPtr_op_Explicit_m1_843(NULL /*static, unused*/, ((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)L_6*(int32_t)L_8))))))), /*hidden argument*/NULL);
		Object_t * L_10 = Marshal_GetObjectForNativeVariant_m1_7763(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, L_4, sizeof(Object_t *))) = (Object_t *)L_10;
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = ___cVars;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0025;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_14 = V_0;
		return L_14;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetStartComSlot(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_GetStartComSlot_m1_7765 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Threading.Thread System.Runtime.InteropServices.Marshal::GetThreadFromFiberCookie(System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Thread_t1_901 * Marshal_GetThreadFromFiberCookie_m1_7766 (Object_t * __this /* static, unused */, int32_t ___cookie, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Object System.Runtime.InteropServices.Marshal::GetTypedObjectForIUnknown(System.IntPtr,System.Type)
extern TypeInfo* ComInteropProxy_t1_129_il2cpp_TypeInfo_var;
extern TypeInfo* __ComObject_t1_131_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" Object_t * Marshal_GetTypedObjectForIUnknown_m1_7767 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComInteropProxy_t1_129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		__ComObject_t1_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	ComInteropProxy_t1_129 * V_0 = {0};
	__ComObject_t1_131 * V_1 = {0};
	Type_t * V_2 = {0};
	TypeU5BU5D_t1_31* V_3 = {0};
	int32_t V_4 = 0;
	{
		IntPtr_t L_0 = ___pUnk;
		Type_t * L_1 = ___t;
		ComInteropProxy_t1_129 * L_2 = (ComInteropProxy_t1_129 *)il2cpp_codegen_object_new (ComInteropProxy_t1_129_il2cpp_TypeInfo_var);
		ComInteropProxy__ctor_m1_1734(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ComInteropProxy_t1_129 * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(11 /* System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy() */, L_3);
		V_1 = ((__ComObject_t1_131 *)CastclassClass(L_4, __ComObject_t1_131_il2cpp_TypeInfo_var));
		Type_t * L_5 = ___t;
		NullCheck(L_5);
		TypeU5BU5D_t1_31* L_6 = (TypeU5BU5D_t1_31*)VirtFuncInvoker0< TypeU5BU5D_t1_31* >::Invoke(168 /* System.Type[] System.Type::GetInterfaces() */, L_5);
		V_3 = L_6;
		V_4 = 0;
		goto IL_005c;
	}

IL_0023:
	{
		TypeU5BU5D_t1_31* L_7 = V_3;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_7, L_9, sizeof(Type_t *)));
		Type_t * L_10 = V_2;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(112 /* System.Reflection.TypeAttributes System.Type::get_Attributes() */, L_10);
		if ((!(((uint32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)4096)))) == ((uint32_t)((int32_t)4096)))))
		{
			goto IL_0056;
		}
	}
	{
		__ComObject_t1_131 * L_12 = V_1;
		Type_t * L_13 = V_2;
		NullCheck(L_12);
		IntPtr_t L_14 = __ComObject_GetInterface_m1_14770(L_12, L_13, /*hidden argument*/NULL);
		IntPtr_t L_15 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_16 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0056;
		}
	}
	{
		return NULL;
	}

IL_0056:
	{
		int32_t L_17 = V_4;
		V_4 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_18 = V_4;
		TypeU5BU5D_t1_31* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		__ComObject_t1_131 * L_20 = V_1;
		return L_20;
	}
}
// System.Type System.Runtime.InteropServices.Marshal::GetTypeForITypeInfo(System.IntPtr)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Type_t * Marshal_GetTypeForITypeInfo_m1_7768 (Object_t * __this /* static, unused */, IntPtr_t ___piTypeInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Runtime.InteropServices.Marshal::GetTypeInfoName(System.Runtime.InteropServices.UCOMITypeInfo)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_GetTypeInfoName_m1_7769 (Object_t * __this /* static, unused */, Object_t * ___pTI, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Runtime.InteropServices.Marshal::GetTypeInfoName(System.Runtime.InteropServices.ComTypes.ITypeInfo)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_GetTypeInfoName_m1_7770 (Object_t * __this /* static, unused */, Object_t * ___typeInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Guid System.Runtime.InteropServices.Marshal::GetTypeLibGuid(System.Runtime.InteropServices.UCOMITypeLib)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Guid_t1_319  Marshal_GetTypeLibGuid_m1_7771 (Object_t * __this /* static, unused */, Object_t * ___pTLB, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Guid System.Runtime.InteropServices.Marshal::GetTypeLibGuid(System.Runtime.InteropServices.ComTypes.ITypeLib)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Guid_t1_319  Marshal_GetTypeLibGuid_m1_7772 (Object_t * __this /* static, unused */, Object_t * ___typelib, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Guid System.Runtime.InteropServices.Marshal::GetTypeLibGuidForAssembly(System.Reflection.Assembly)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Guid_t1_319  Marshal_GetTypeLibGuidForAssembly_m1_7773 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___asm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetTypeLibLcid(System.Runtime.InteropServices.UCOMITypeLib)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_GetTypeLibLcid_m1_7774 (Object_t * __this /* static, unused */, Object_t * ___pTLB, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetTypeLibLcid(System.Runtime.InteropServices.ComTypes.ITypeLib)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_GetTypeLibLcid_m1_7775 (Object_t * __this /* static, unused */, Object_t * ___typelib, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Runtime.InteropServices.Marshal::GetTypeLibName(System.Runtime.InteropServices.UCOMITypeLib)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_GetTypeLibName_m1_7776 (Object_t * __this /* static, unused */, Object_t * ___pTLB, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Runtime.InteropServices.Marshal::GetTypeLibName(System.Runtime.InteropServices.ComTypes.ITypeLib)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_GetTypeLibName_m1_7777 (Object_t * __this /* static, unused */, Object_t * ___typelib, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::GetTypeLibVersionForAssembly(System.Reflection.Assembly,System.Int32&,System.Int32&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_GetTypeLibVersionForAssembly_m1_7778 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___inputAssembly, int32_t* ___majorVersion, int32_t* ___minorVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Object System.Runtime.InteropServices.Marshal::GetUniqueObjectForIUnknown(System.IntPtr)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Object_t * Marshal_GetUniqueObjectForIUnknown_m1_7779 (Object_t * __this /* static, unused */, IntPtr_t ___unknown, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetUnmanagedThunkForManagedMethodPtr(System.IntPtr,System.IntPtr,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_GetUnmanagedThunkForManagedMethodPtr_m1_7780 (Object_t * __this /* static, unused */, IntPtr_t ___pfnMethodToWrap, IntPtr_t ___pbSignature, int32_t ___cbSignature, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.Marshal::IsComObject(System.Object)
extern "C" bool Marshal_IsComObject_m1_7781 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef bool (*Marshal_IsComObject_m1_7781_ftn) (Object_t *);
	return  ((Marshal_IsComObject_m1_7781_ftn)mscorlib::System::Runtime::InteropServices::Marshal::IsComObject) (___o);
}
// System.Boolean System.Runtime.InteropServices.Marshal::IsTypeVisibleFromCom(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool Marshal_IsTypeVisibleFromCom_m1_7782 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::NumParamBytes(System.Reflection.MethodInfo)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_NumParamBytes_m1_7783 (Object_t * __this /* static, unused */, MethodInfo_t * ___m, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::GetLastWin32Error()
extern "C" int32_t Marshal_GetLastWin32Error_m1_7784 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_GetLastWin32Error_m1_7784_ftn) ();
	return  ((Marshal_GetLastWin32Error_m1_7784_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetLastWin32Error) ();
}
// System.IntPtr System.Runtime.InteropServices.Marshal::OffsetOf(System.Type,System.String)
extern "C" IntPtr_t Marshal_OffsetOf_m1_7785 (Object_t * __this /* static, unused */, Type_t * ___t, String_t* ___fieldName, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_OffsetOf_m1_7785_ftn) (Type_t *, String_t*);
	return  ((Marshal_OffsetOf_m1_7785_ftn)mscorlib::System::Runtime::InteropServices::Marshal::OffsetOf) (___t, ___fieldName);
}
// System.Void System.Runtime.InteropServices.Marshal::Prelink(System.Reflection.MethodInfo)
extern "C" void Marshal_Prelink_m1_7786 (Object_t * __this /* static, unused */, MethodInfo_t * ___m, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_Prelink_m1_7786_ftn) (MethodInfo_t *);
	 ((Marshal_Prelink_m1_7786_ftn)mscorlib::System::Runtime::InteropServices::Marshal::Prelink) (___m);
}
// System.Void System.Runtime.InteropServices.Marshal::PrelinkAll(System.Type)
extern "C" void Marshal_PrelinkAll_m1_7787 (Object_t * __this /* static, unused */, Type_t * ___c, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_PrelinkAll_m1_7787_ftn) (Type_t *);
	 ((Marshal_PrelinkAll_m1_7787_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PrelinkAll) (___c);
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAnsi(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringAnsi_m1_7788 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*Marshal_PtrToStringAnsi_m1_7788_ftn) (IntPtr_t);
	return  ((Marshal_PtrToStringAnsi_m1_7788_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStringAnsi_mscorlib_System_String_mscorlib_System_IntPtr) (___ptr);
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAnsi(System.IntPtr,System.Int32)
extern "C" String_t* Marshal_PtrToStringAnsi_m1_7789 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___len, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*Marshal_PtrToStringAnsi_m1_7789_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_PtrToStringAnsi_m1_7789_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStringAnsi_mscorlib_System_String_mscorlib_System_IntPtr_mscorlib_System_Int32) (___ptr, ___len);
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_PtrToStringAuto_m1_7790 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Marshal_t1_811_StaticFields*)Marshal_t1_811_il2cpp_TypeInfo_var->static_fields)->___SystemDefaultCharSize_1;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0016;
		}
	}
	{
		IntPtr_t L_1 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		String_t* L_2 = Marshal_PtrToStringUni_m1_7792(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_0016:
	{
		IntPtr_t L_3 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		String_t* L_4 = Marshal_PtrToStringAnsi_m1_7788(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" String_t* Marshal_PtrToStringAuto_m1_7791 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___len, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Marshal_t1_811_StaticFields*)Marshal_t1_811_il2cpp_TypeInfo_var->static_fields)->___SystemDefaultCharSize_1;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0017;
		}
	}
	{
		IntPtr_t L_1 = ___ptr;
		int32_t L_2 = ___len;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		String_t* L_3 = Marshal_PtrToStringUni_m1_7793(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_001e;
	}

IL_0017:
	{
		IntPtr_t L_4 = ___ptr;
		int32_t L_5 = ___len;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		String_t* L_6 = Marshal_PtrToStringAnsi_m1_7789(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringUni(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringUni_m1_7792 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*Marshal_PtrToStringUni_m1_7792_ftn) (IntPtr_t);
	return  ((Marshal_PtrToStringUni_m1_7792_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStringUni_mscorlib_System_String_mscorlib_System_IntPtr) (___ptr);
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringUni(System.IntPtr,System.Int32)
extern "C" String_t* Marshal_PtrToStringUni_m1_7793 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___len, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*Marshal_PtrToStringUni_m1_7793_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_PtrToStringUni_m1_7793_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStringUni_mscorlib_System_String_mscorlib_System_IntPtr_mscorlib_System_Int32) (___ptr, ___len);
}
// System.String System.Runtime.InteropServices.Marshal::PtrToStringBSTR(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringBSTR_m1_7794 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef String_t* (*Marshal_PtrToStringBSTR_m1_7794_ftn) (IntPtr_t);
	return  ((Marshal_PtrToStringBSTR_m1_7794_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStringBSTR) (___ptr);
}
// System.Void System.Runtime.InteropServices.Marshal::PtrToStructure(System.IntPtr,System.Object)
extern "C" void Marshal_PtrToStructure_m1_7795 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Object_t * ___structure, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_PtrToStructure_m1_7795_ftn) (IntPtr_t, Object_t *);
	 ((Marshal_PtrToStructure_m1_7795_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStructureObject) (___ptr, ___structure);
}
// System.Object System.Runtime.InteropServices.Marshal::PtrToStructure(System.IntPtr,System.Type)
extern "C" Object_t * Marshal_PtrToStructure_m1_7796 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___structureType, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Object_t * (*Marshal_PtrToStructure_m1_7796_ftn) (IntPtr_t, Type_t *);
	return  ((Marshal_PtrToStructure_m1_7796_ftn)mscorlib::System::Runtime::InteropServices::Marshal::PtrToStructure) (___ptr, ___structureType);
}
// System.Int32 System.Runtime.InteropServices.Marshal::QueryInterfaceInternal(System.IntPtr,System.Guid&,System.IntPtr&)
extern "C" int32_t Marshal_QueryInterfaceInternal_m1_7797 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, Guid_t1_319 * ___iid, IntPtr_t* ___ppv, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_QueryInterfaceInternal_m1_7797_ftn) (IntPtr_t, Guid_t1_319 *, IntPtr_t*);
	return  ((Marshal_QueryInterfaceInternal_m1_7797_ftn)mscorlib::System::Runtime::InteropServices::Marshal::QueryInterfaceInternal) (___pUnk, ___iid, ___ppv);
}
// System.Int32 System.Runtime.InteropServices.Marshal::QueryInterface(System.IntPtr,System.Guid&,System.IntPtr&)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2117;
extern Il2CppCodeGenString* _stringLiteral2118;
extern "C" int32_t Marshal_QueryInterface_m1_7798 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, Guid_t1_319 * ___iid, IntPtr_t* ___ppv, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral2117 = il2cpp_codegen_string_literal_from_index(2117);
		_stringLiteral2118 = il2cpp_codegen_string_literal_from_index(2118);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pUnk;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_3, _stringLiteral2117, _stringLiteral2118, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___pUnk;
		Guid_t1_319 * L_5 = ___iid;
		IntPtr_t* L_6 = ___ppv;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_QueryInterfaceInternal_m1_7797(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" uint8_t Marshal_ReadByte_m1_7799 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		uint8_t L_1 = Marshal_ReadByte_m1_7800(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.IntPtr,System.Int32)
extern "C" uint8_t Marshal_ReadByte_m1_7800 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef uint8_t (*Marshal_ReadByte_m1_7800_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReadByte_m1_7800_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReadByte) (___ptr, ___ofs);
}
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.Object,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" uint8_t Marshal_ReadByte_m1_7801 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int16 System.Runtime.InteropServices.Marshal::ReadInt16(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" int16_t Marshal_ReadInt16_m1_7802 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int16_t L_1 = Marshal_ReadInt16_m1_7803(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int16 System.Runtime.InteropServices.Marshal::ReadInt16(System.IntPtr,System.Int32)
extern "C" int16_t Marshal_ReadInt16_m1_7803 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int16_t (*Marshal_ReadInt16_m1_7803_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReadInt16_m1_7803_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReadInt16) (___ptr, ___ofs);
}
// System.Int16 System.Runtime.InteropServices.Marshal::ReadInt16(System.Object,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int16_t Marshal_ReadInt16_m1_7804 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_ReadInt32_m1_7805 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_ReadInt32_m1_7806(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr,System.Int32)
extern "C" int32_t Marshal_ReadInt32_m1_7806 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_ReadInt32_m1_7806_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReadInt32_m1_7806_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReadInt32) (___ptr, ___ofs);
}
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.Object,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_ReadInt32_m1_7807 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int64 System.Runtime.InteropServices.Marshal::ReadInt64(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" int64_t Marshal_ReadInt64_m1_7808 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int64_t L_1 = Marshal_ReadInt64_m1_7809(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int64 System.Runtime.InteropServices.Marshal::ReadInt64(System.IntPtr,System.Int32)
extern "C" int64_t Marshal_ReadInt64_m1_7809 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int64_t (*Marshal_ReadInt64_m1_7809_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReadInt64_m1_7809_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReadInt64) (___ptr, ___ofs);
}
// System.Int64 System.Runtime.InteropServices.Marshal::ReadInt64(System.Object,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int64_t Marshal_ReadInt64_m1_7810 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::ReadIntPtr(System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_ReadIntPtr_m1_7811 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_ReadIntPtr_m1_7812(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::ReadIntPtr(System.IntPtr,System.Int32)
extern "C" IntPtr_t Marshal_ReadIntPtr_m1_7812 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_ReadIntPtr_m1_7812_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReadIntPtr_m1_7812_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReadIntPtr) (___ptr, ___ofs);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::ReadIntPtr(System.Object,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_ReadIntPtr_m1_7813 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::ReAllocCoTaskMem(System.IntPtr,System.Int32)
extern "C" IntPtr_t Marshal_ReAllocCoTaskMem_m1_7814 (Object_t * __this /* static, unused */, IntPtr_t ___pv, int32_t ___cb, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_ReAllocCoTaskMem_m1_7814_ftn) (IntPtr_t, int32_t);
	return  ((Marshal_ReAllocCoTaskMem_m1_7814_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReAllocCoTaskMem) (___pv, ___cb);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::ReAllocHGlobal(System.IntPtr,System.IntPtr)
extern "C" IntPtr_t Marshal_ReAllocHGlobal_m1_7815 (Object_t * __this /* static, unused */, IntPtr_t ___pv, IntPtr_t ___cb, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_ReAllocHGlobal_m1_7815_ftn) (IntPtr_t, IntPtr_t);
	return  ((Marshal_ReAllocHGlobal_m1_7815_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReAllocHGlobal) (___pv, ___cb);
}
// System.Int32 System.Runtime.InteropServices.Marshal::ReleaseInternal(System.IntPtr)
extern "C" int32_t Marshal_ReleaseInternal_m1_7816 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_ReleaseInternal_m1_7816_ftn) (IntPtr_t);
	return  ((Marshal_ReleaseInternal_m1_7816_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReleaseInternal) (___pUnk);
}
// System.Int32 System.Runtime.InteropServices.Marshal::Release(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2117;
extern Il2CppCodeGenString* _stringLiteral2118;
extern "C" int32_t Marshal_Release_m1_7817 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral2117 = il2cpp_codegen_string_literal_from_index(2117);
		_stringLiteral2118 = il2cpp_codegen_string_literal_from_index(2118);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___pUnk;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_3, _stringLiteral2117, _stringLiteral2118, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___pUnk;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_5 = Marshal_ReleaseInternal_m1_7816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::ReleaseComObjectInternal(System.Object)
extern "C" int32_t Marshal_ReleaseComObjectInternal_m1_7818 (Object_t * __this /* static, unused */, Object_t * ___co, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_ReleaseComObjectInternal_m1_7818_ftn) (Object_t *);
	return  ((Marshal_ReleaseComObjectInternal_m1_7818_ftn)mscorlib::System::Runtime::InteropServices::Marshal::ReleaseComObjectInternal) (___co);
}
// System.Int32 System.Runtime.InteropServices.Marshal::ReleaseComObject(System.Object)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2117;
extern Il2CppCodeGenString* _stringLiteral2120;
extern Il2CppCodeGenString* _stringLiteral2125;
extern "C" int32_t Marshal_ReleaseComObject_m1_7819 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral2117 = il2cpp_codegen_string_literal_from_index(2117);
		_stringLiteral2120 = il2cpp_codegen_string_literal_from_index(2120);
		_stringLiteral2125 = il2cpp_codegen_string_literal_from_index(2125);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_1, _stringLiteral2117, _stringLiteral2120, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t * L_2 = ___o;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		bool L_3 = Marshal_IsComObject_m1_7781(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0031;
		}
	}
	{
		ArgumentException_t1_1425 * L_4 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_4, _stringLiteral2125, _stringLiteral2120, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0031:
	{
		Object_t * L_5 = ___o;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_6 = Marshal_ReleaseComObjectInternal_m1_7818(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ReleaseThreadCache()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_ReleaseThreadCache_m1_7820 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.Marshal::SetComObjectData(System.Object,System.Object,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2121;
extern "C" bool Marshal_SetComObjectData_m1_7821 (Object_t * __this /* static, unused */, Object_t * ___obj, Object_t * ___key, Object_t * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2121 = il2cpp_codegen_string_literal_from_index(2121);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2121, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Object)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_SizeOf_m1_7822 (Object_t * __this /* static, unused */, Object_t * ___structure, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___structure;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m1_5(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_2 = Marshal_SizeOf_m1_7823(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Type)
extern "C" int32_t Marshal_SizeOf_m1_7823 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef int32_t (*Marshal_SizeOf_m1_7823_ftn) (Type_t *);
	return  ((Marshal_SizeOf_m1_7823_ftn)mscorlib::System::Runtime::InteropServices::Marshal::SizeOf) (___t);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToBSTR(System.String)
extern "C" IntPtr_t Marshal_StringToBSTR_m1_7824 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_StringToBSTR_m1_7824_ftn) (String_t*);
	return  ((Marshal_StringToBSTR_m1_7824_ftn)mscorlib::System::Runtime::InteropServices::Marshal::StringToBSTR) (___s);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToCoTaskMemAnsi(System.String)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_StringToCoTaskMemAnsi_m1_7825 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___s;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = Marshal_AllocCoTaskMem_m1_7698(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_0;
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_4));
		V_3 = 0;
		goto IL_002d;
	}

IL_001e:
	{
		ByteU5BU5D_t1_109* L_5 = V_2;
		int32_t L_6 = V_3;
		String_t* L_7 = ___s;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_442(L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_6, sizeof(uint8_t))) = (uint8_t)(((int32_t)((uint8_t)L_9)));
		int32_t L_10 = V_3;
		V_3 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_11 = V_3;
		String_t* L_12 = ___s;
		NullCheck(L_12);
		int32_t L_13 = String_get_Length_m1_571(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001e;
		}
	}
	{
		ByteU5BU5D_t1_109* L_14 = V_2;
		String_t* L_15 = ___s;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m1_571(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_14, L_16, sizeof(uint8_t))) = (uint8_t)0;
		ByteU5BU5D_t1_109* L_17 = V_2;
		IntPtr_t L_18 = V_1;
		int32_t L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_17, 0, L_18, L_19, /*hidden argument*/NULL);
		IntPtr_t L_20 = V_1;
		return L_20;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToCoTaskMemAuto(System.String)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_StringToCoTaskMemAuto_m1_7826 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Marshal_t1_811_StaticFields*)Marshal_t1_811_il2cpp_TypeInfo_var->static_fields)->___SystemDefaultCharSize_1;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = Marshal_StringToCoTaskMemUni_m1_7827(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_0016:
	{
		String_t* L_3 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_4 = Marshal_StringToCoTaskMemAnsi_m1_7825(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToCoTaskMemUni(System.String)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_StringToCoTaskMemUni_m1_7827 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	CharU5BU5D_t1_16* V_2 = {0};
	{
		String_t* L_0 = ___s;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)1));
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = Marshal_AllocCoTaskMem_m1_7698(NULL /*static, unused*/, ((int32_t)((int32_t)L_2*(int32_t)2)), /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_0;
		V_2 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, L_4));
		String_t* L_5 = ___s;
		CharU5BU5D_t1_16* L_6 = V_2;
		String_t* L_7 = ___s;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1_571(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_CopyTo_m1_445(L_5, 0, L_6, 0, L_8, /*hidden argument*/NULL);
		CharU5BU5D_t1_16* L_9 = V_2;
		String_t* L_10 = ___s;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1_571(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_11);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_9, L_11, sizeof(uint16_t))) = (uint16_t)0;
		CharU5BU5D_t1_16* L_12 = V_2;
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_12, 0, L_13, L_14, /*hidden argument*/NULL);
		IntPtr_t L_15 = V_1;
		return L_15;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalAnsi(System.String)
extern "C" IntPtr_t Marshal_StringToHGlobalAnsi_m1_7828 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_StringToHGlobalAnsi_m1_7828_ftn) (String_t*);
	return  ((Marshal_StringToHGlobalAnsi_m1_7828_ftn)mscorlib::System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi) (___s);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalAuto(System.String)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" IntPtr_t Marshal_StringToHGlobalAuto_m1_7829 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Marshal_t1_811_StaticFields*)Marshal_t1_811_il2cpp_TypeInfo_var->static_fields)->___SystemDefaultCharSize_1;
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_1 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = Marshal_StringToHGlobalUni_m1_7830(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_0016:
	{
		String_t* L_3 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_4 = Marshal_StringToHGlobalAnsi_m1_7828(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalUni(System.String)
extern "C" IntPtr_t Marshal_StringToHGlobalUni_m1_7830 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_StringToHGlobalUni_m1_7830_ftn) (String_t*);
	return  ((Marshal_StringToHGlobalUni_m1_7830_ftn)mscorlib::System::Runtime::InteropServices::Marshal::StringToHGlobalUni) (___s);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToBSTR(System.Security.SecureString)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" IntPtr_t Marshal_SecureStringToBSTR_m1_7831 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SecureString_t1_1197 * L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SecureString_t1_1197 * L_2 = ___s;
		NullCheck(L_2);
		int32_t L_3 = SecureString_get_Length_m1_11997(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_5 = Marshal_AllocCoTaskMem_m1_7698(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4+(int32_t)1))*(int32_t)2))+(int32_t)4)), /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = (ByteU5BU5D_t1_109*)NULL;
		IntPtr_t L_6 = V_1;
		int32_t L_7 = V_0;
		Marshal_WriteInt32_m1_7850(NULL /*static, unused*/, L_6, 0, ((int32_t)((int32_t)L_7*(int32_t)2)), /*hidden argument*/NULL);
	}

IL_0031:
	try
	{ // begin try (depth: 1)
		{
			SecureString_t1_1197 * L_8 = ___s;
			NullCheck(L_8);
			ByteU5BU5D_t1_109* L_9 = SecureString_GetBuffer_m1_12010(L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			V_3 = 0;
			goto IL_005e;
		}

IL_003f:
		{
			IntPtr_t L_10 = V_1;
			int32_t L_11 = V_3;
			ByteU5BU5D_t1_109* L_12 = V_2;
			int32_t L_13 = V_3;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)L_13*(int32_t)2)));
			int32_t L_14 = ((int32_t)((int32_t)L_13*(int32_t)2));
			ByteU5BU5D_t1_109* L_15 = V_2;
			int32_t L_16 = V_3;
			NullCheck(L_15);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)((int32_t)((int32_t)L_16*(int32_t)2))+(int32_t)1)));
			int32_t L_17 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16*(int32_t)2))+(int32_t)1));
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
			Marshal_WriteInt16_m1_7844(NULL /*static, unused*/, L_10, ((int32_t)((int32_t)4+(int32_t)((int32_t)((int32_t)L_11*(int32_t)2)))), (((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_12, L_14, sizeof(uint8_t)))<<(int32_t)8))|(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_15, L_17, sizeof(uint8_t)))))))), /*hidden argument*/NULL);
			int32_t L_18 = V_3;
			V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
		}

IL_005e:
		{
			int32_t L_19 = V_3;
			int32_t L_20 = V_0;
			if ((((int32_t)L_19) < ((int32_t)L_20)))
			{
				goto IL_003f;
			}
		}

IL_0065:
		{
			IntPtr_t L_21 = V_1;
			ByteU5BU5D_t1_109* L_22 = V_2;
			NullCheck(L_22);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
			Marshal_WriteInt16_m1_7844(NULL /*static, unused*/, L_21, ((int32_t)((int32_t)4+(int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))))), 0, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x9A, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		{
			ByteU5BU5D_t1_109* L_23 = V_2;
			if (!L_23)
			{
				goto IL_0099;
			}
		}

IL_007c:
		{
			ByteU5BU5D_t1_109* L_24 = V_2;
			NullCheck(L_24);
			V_4 = (((int32_t)((int32_t)(((Array_t *)L_24)->max_length))));
			goto IL_0091;
		}

IL_0086:
		{
			int32_t L_25 = V_4;
			V_4 = ((int32_t)((int32_t)L_25-(int32_t)1));
			ByteU5BU5D_t1_109* L_26 = V_2;
			int32_t L_27 = V_4;
			NullCheck(L_26);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_26, L_27, sizeof(uint8_t))) = (uint8_t)0;
		}

IL_0091:
		{
			int32_t L_28 = V_4;
			if ((((int32_t)L_28) > ((int32_t)0)))
			{
				goto IL_0086;
			}
		}

IL_0099:
		{
			IL2CPP_END_FINALLY(118)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_009a:
	{
		IntPtr_t L_29 = V_1;
		int64_t L_30 = IntPtr_op_Explicit_m1_846(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		IntPtr_t L_31 = IntPtr_op_Explicit_m1_843(NULL /*static, unused*/, ((int64_t)((int64_t)L_30+(int64_t)(((int64_t)((int64_t)4))))), /*hidden argument*/NULL);
		return L_31;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToCoTaskMemAnsi(System.Security.SecureString)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" IntPtr_t Marshal_SecureStringToCoTaskMemAnsi_m1_7832 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SecureString_t1_1197 * L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SecureString_t1_1197 * L_2 = ___s;
		NullCheck(L_2);
		int32_t L_3 = SecureString_get_Length_m1_11997(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_5 = Marshal_AllocCoTaskMem_m1_7698(NULL /*static, unused*/, ((int32_t)((int32_t)L_4+(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_0;
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_6+(int32_t)1))));
	}

IL_002a:
	try
	{ // begin try (depth: 1)
		{
			SecureString_t1_1197 * L_7 = ___s;
			NullCheck(L_7);
			ByteU5BU5D_t1_109* L_8 = SecureString_GetBuffer_m1_12010(L_7, /*hidden argument*/NULL);
			V_3 = L_8;
			V_4 = 0;
			V_5 = 0;
			goto IL_005e;
		}

IL_003c:
		{
			ByteU5BU5D_t1_109* L_9 = V_2;
			int32_t L_10 = V_4;
			ByteU5BU5D_t1_109* L_11 = V_3;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
			int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10, sizeof(uint8_t))) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_11, L_13, sizeof(uint8_t)));
			ByteU5BU5D_t1_109* L_14 = V_3;
			int32_t L_15 = V_5;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_14, L_15, sizeof(uint8_t))) = (uint8_t)0;
			ByteU5BU5D_t1_109* L_16 = V_3;
			int32_t L_17 = V_5;
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)));
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), sizeof(uint8_t))) = (uint8_t)0;
			int32_t L_18 = V_4;
			V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
			int32_t L_19 = V_5;
			V_5 = ((int32_t)((int32_t)L_19+(int32_t)2));
		}

IL_005e:
		{
			int32_t L_20 = V_4;
			int32_t L_21 = V_0;
			if ((((int32_t)L_20) < ((int32_t)L_21)))
			{
				goto IL_003c;
			}
		}

IL_0066:
		{
			ByteU5BU5D_t1_109* L_22 = V_2;
			int32_t L_23 = V_4;
			NullCheck(L_22);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_22, L_23, sizeof(uint8_t))) = (uint8_t)0;
			ByteU5BU5D_t1_109* L_24 = V_2;
			IntPtr_t L_25 = V_1;
			int32_t L_26 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
			Marshal_copy_to_unmanaged_m1_7703(NULL /*static, unused*/, (Array_t *)(Array_t *)L_24, 0, L_25, ((int32_t)((int32_t)L_26+(int32_t)1)), /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x97, FINALLY_007b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		{
			int32_t L_27 = V_0;
			V_6 = L_27;
			goto IL_008e;
		}

IL_0083:
		{
			int32_t L_28 = V_6;
			V_6 = ((int32_t)((int32_t)L_28-(int32_t)1));
			ByteU5BU5D_t1_109* L_29 = V_2;
			int32_t L_30 = V_6;
			NullCheck(L_29);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_29, L_30, sizeof(uint8_t))) = (uint8_t)0;
		}

IL_008e:
		{
			int32_t L_31 = V_6;
			if ((((int32_t)L_31) > ((int32_t)0)))
			{
				goto IL_0083;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(123)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0097:
	{
		IntPtr_t L_32 = V_1;
		return L_32;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToCoTaskMemUnicode(System.Security.SecureString)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" IntPtr_t Marshal_SecureStringToCoTaskMemUnicode_m1_7833 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SecureString_t1_1197 * L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SecureString_t1_1197 * L_2 = ___s;
		NullCheck(L_2);
		int32_t L_3 = SecureString_get_Length_m1_11997(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_5 = Marshal_AllocCoTaskMem_m1_7698(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)2))+(int32_t)2)), /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = (ByteU5BU5D_t1_109*)NULL;
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			SecureString_t1_1197 * L_6 = ___s;
			NullCheck(L_6);
			ByteU5BU5D_t1_109* L_7 = SecureString_GetBuffer_m1_12010(L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			V_3 = 0;
			goto IL_0050;
		}

IL_0033:
		{
			IntPtr_t L_8 = V_1;
			int32_t L_9 = V_3;
			ByteU5BU5D_t1_109* L_10 = V_2;
			int32_t L_11 = V_3;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)((int32_t)L_11*(int32_t)2)));
			int32_t L_12 = ((int32_t)((int32_t)L_11*(int32_t)2));
			ByteU5BU5D_t1_109* L_13 = V_2;
			int32_t L_14 = V_3;
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)((int32_t)((int32_t)((int32_t)L_14*(int32_t)2))+(int32_t)1)));
			int32_t L_15 = ((int32_t)((int32_t)((int32_t)((int32_t)L_14*(int32_t)2))+(int32_t)1));
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
			Marshal_WriteInt16_m1_7844(NULL /*static, unused*/, L_8, ((int32_t)((int32_t)L_9*(int32_t)2)), (((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_12, sizeof(uint8_t)))<<(int32_t)8))|(int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_13, L_15, sizeof(uint8_t)))))))), /*hidden argument*/NULL);
			int32_t L_16 = V_3;
			V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
		}

IL_0050:
		{
			int32_t L_17 = V_3;
			int32_t L_18 = V_0;
			if ((((int32_t)L_17) < ((int32_t)L_18)))
			{
				goto IL_0033;
			}
		}

IL_0057:
		{
			IntPtr_t L_19 = V_1;
			ByteU5BU5D_t1_109* L_20 = V_2;
			NullCheck(L_20);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
			Marshal_WriteInt16_m1_7844(NULL /*static, unused*/, L_19, (((int32_t)((int32_t)(((Array_t *)L_20)->max_length)))), 0, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x8A, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		{
			ByteU5BU5D_t1_109* L_21 = V_2;
			if (!L_21)
			{
				goto IL_0089;
			}
		}

IL_006c:
		{
			ByteU5BU5D_t1_109* L_22 = V_2;
			NullCheck(L_22);
			V_4 = (((int32_t)((int32_t)(((Array_t *)L_22)->max_length))));
			goto IL_0081;
		}

IL_0076:
		{
			int32_t L_23 = V_4;
			V_4 = ((int32_t)((int32_t)L_23-(int32_t)1));
			ByteU5BU5D_t1_109* L_24 = V_2;
			int32_t L_25 = V_4;
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
			*((uint8_t*)(uint8_t*)SZArrayLdElema(L_24, L_25, sizeof(uint8_t))) = (uint8_t)0;
		}

IL_0081:
		{
			int32_t L_26 = V_4;
			if ((((int32_t)L_26) > ((int32_t)0)))
			{
				goto IL_0076;
			}
		}

IL_0089:
		{
			IL2CPP_END_FINALLY(102)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_008a:
	{
		IntPtr_t L_27 = V_1;
		return L_27;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToGlobalAllocAnsi(System.Security.SecureString)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" IntPtr_t Marshal_SecureStringToGlobalAllocAnsi_m1_7834 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		SecureString_t1_1197 * L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SecureString_t1_1197 * L_2 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = Marshal_SecureStringToCoTaskMemAnsi_m1_7832(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToGlobalAllocUnicode(System.Security.SecureString)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" IntPtr_t Marshal_SecureStringToGlobalAllocUnicode_m1_7835 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		SecureString_t1_1197 * L_0 = ___s;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SecureString_t1_1197 * L_2 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = Marshal_SecureStringToCoTaskMemUnicode_m1_7833(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::StructureToPtr(System.Object,System.IntPtr,System.Boolean)
extern "C" void Marshal_StructureToPtr_m1_7836 (Object_t * __this /* static, unused */, Object_t * ___structure, IntPtr_t ___ptr, bool ___fDeleteOld, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_StructureToPtr_m1_7836_ftn) (Object_t *, IntPtr_t, bool);
	 ((Marshal_StructureToPtr_m1_7836_ftn)mscorlib::System::Runtime::InteropServices::Marshal::StructureToPtr) (___structure, ___ptr, ___fDeleteOld);
}
// System.Void System.Runtime.InteropServices.Marshal::ThrowExceptionForHR(System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ThrowExceptionForHR_m1_7837 (Object_t * __this /* static, unused */, int32_t ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	{
		int32_t L_0 = ___errorCode;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Exception_t1_33 * L_1 = Marshal_GetExceptionForHR_m1_7858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Exception_t1_33 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		Exception_t1_33 * L_3 = V_0;
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_000f:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::ThrowExceptionForHR(System.Int32,System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_ThrowExceptionForHR_m1_7838 (Object_t * __this /* static, unused */, int32_t ___errorCode, IntPtr_t ___errorInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	{
		int32_t L_0 = ___errorCode;
		IntPtr_t L_1 = ___errorInfo;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Exception_t1_33 * L_2 = Marshal_GetExceptionForHR_m1_7859(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Exception_t1_33 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0010;
		}
	}
	{
		Exception_t1_33 * L_4 = V_0;
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0010:
	{
		return;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::UnsafeAddrOfPinnedArrayElement(System.Array,System.Int32)
extern "C" IntPtr_t Marshal_UnsafeAddrOfPinnedArrayElement_m1_7839 (Object_t * __this /* static, unused */, Array_t * ___arr, int32_t ___index, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_UnsafeAddrOfPinnedArrayElement_m1_7839_ftn) (Array_t *, int32_t);
	return  ((Marshal_UnsafeAddrOfPinnedArrayElement_m1_7839_ftn)mscorlib::System::Runtime::InteropServices::Marshal::UnsafeAddrOfPinnedArrayElement) (___arr, ___index);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.IntPtr,System.Byte)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteByte_m1_7840 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, uint8_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		uint8_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteByte_m1_7841(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.IntPtr,System.Int32,System.Byte)
extern "C" void Marshal_WriteByte_m1_7841 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, uint8_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteByte_m1_7841_ftn) (IntPtr_t, int32_t, uint8_t);
	 ((Marshal_WriteByte_m1_7841_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteByte) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.Object,System.Int32,System.Byte)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteByte_m1_7842 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, uint8_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Int16)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt16_m1_7843 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int16_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		int16_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteInt16_m1_7844(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Int32,System.Int16)
extern "C" void Marshal_WriteInt16_m1_7844 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, int16_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteInt16_m1_7844_ftn) (IntPtr_t, int32_t, int16_t);
	 ((Marshal_WriteInt16_m1_7844_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteInt16) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.Object,System.Int32,System.Int16)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt16_m1_7845 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, int16_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Char)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt16_m1_7846 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, uint16_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		uint16_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteInt16_m1_7847(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Int32,System.Char)
extern "C" void Marshal_WriteInt16_m1_7847 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, uint16_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteInt16_m1_7847_ftn) (IntPtr_t, int32_t, uint16_t);
	 ((Marshal_WriteInt16_m1_7847_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteInt16) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.Object,System.Int32,System.Char)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt16_m1_7848 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, uint16_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt32(System.IntPtr,System.Int32)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt32_m1_7849 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		int32_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteInt32_m1_7850(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt32(System.IntPtr,System.Int32,System.Int32)
extern "C" void Marshal_WriteInt32_m1_7850 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, int32_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteInt32_m1_7850_ftn) (IntPtr_t, int32_t, int32_t);
	 ((Marshal_WriteInt32_m1_7850_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteInt32) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt32(System.Object,System.Int32,System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt32_m1_7851 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt64(System.IntPtr,System.Int64)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt64_m1_7852 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int64_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		int64_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteInt64_m1_7853(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt64(System.IntPtr,System.Int32,System.Int64)
extern "C" void Marshal_WriteInt64_m1_7853 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, int64_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteInt64_m1_7853_ftn) (IntPtr_t, int32_t, int64_t);
	 ((Marshal_WriteInt64_m1_7853_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteInt64) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteInt64(System.Object,System.Int32,System.Int64)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteInt64_m1_7854 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, int64_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteIntPtr(System.IntPtr,System.IntPtr)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteIntPtr_m1_7855 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, IntPtr_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___ptr;
		IntPtr_t L_1 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Marshal_WriteIntPtr_m1_7856(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.Marshal::WriteIntPtr(System.IntPtr,System.Int32,System.IntPtr)
extern "C" void Marshal_WriteIntPtr_m1_7856 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, IntPtr_t ___val, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*Marshal_WriteIntPtr_m1_7856_ftn) (IntPtr_t, int32_t, IntPtr_t);
	 ((Marshal_WriteIntPtr_m1_7856_ftn)mscorlib::System::Runtime::InteropServices::Marshal::WriteIntPtr) (___ptr, ___ofs, ___val);
}
// System.Void System.Runtime.InteropServices.Marshal::WriteIntPtr(System.Object,System.Int32,System.IntPtr)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void Marshal_WriteIntPtr_m1_7857 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, IntPtr_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Exception System.Runtime.InteropServices.Marshal::GetExceptionForHR(System.Int32)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" Exception_t1_33 * Marshal_GetExceptionForHR_m1_7858 (Object_t * __this /* static, unused */, int32_t ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___errorCode;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Exception_t1_33 * L_2 = Marshal_GetExceptionForHR_m1_7859(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Exception System.Runtime.InteropServices.Marshal::GetExceptionForHR(System.Int32,System.IntPtr)
extern TypeInfo* OutOfMemoryException_t1_1557_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* COMException_t1_760_il2cpp_TypeInfo_var;
extern "C" Exception_t1_33 * Marshal_GetExceptionForHR_m1_7859 (Object_t * __this /* static, unused */, int32_t ___errorCode, IntPtr_t ___errorInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OutOfMemoryException_t1_1557_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(596);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		COMException_t1_760_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(597);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___errorCode;
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-2147024882))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = V_2;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)-2147024809))))
		{
			goto IL_0023;
		}
	}
	{
		goto IL_0029;
	}

IL_001d:
	{
		OutOfMemoryException_t1_1557 * L_3 = (OutOfMemoryException_t1_1557 *)il2cpp_codegen_object_new (OutOfMemoryException_t1_1557_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m1_14542(L_3, /*hidden argument*/NULL);
		return L_3;
	}

IL_0023:
	{
		ArgumentException_t1_1425 * L_4 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13259(L_4, /*hidden argument*/NULL);
		return L_4;
	}

IL_0029:
	{
		int32_t L_5 = ___errorCode;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		int32_t L_7 = ___errorCode;
		COMException_t1_760 * L_8 = (COMException_t1_760 *)il2cpp_codegen_object_new (COMException_t1_760_il2cpp_TypeInfo_var);
		COMException__ctor_m1_7593(L_8, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_003c:
	{
		return (Exception_t1_33 *)NULL;
	}
}
// System.Int32 System.Runtime.InteropServices.Marshal::FinalReleaseComObject(System.Object)
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern "C" int32_t Marshal_FinalReleaseComObject_m1_7860 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0005;
	}

IL_0005:
	{
		Object_t * L_0 = ___o;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_ReleaseComObject_m1_7819(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0005;
		}
	}
	{
		return 0;
	}
}
// System.Delegate System.Runtime.InteropServices.Marshal::GetDelegateForFunctionPointerInternal(System.IntPtr,System.Type)
extern "C" Delegate_t1_22 * Marshal_GetDelegateForFunctionPointerInternal_m1_7861 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___t, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Delegate_t1_22 * (*Marshal_GetDelegateForFunctionPointerInternal_m1_7861_ftn) (IntPtr_t, Type_t *);
	return  ((Marshal_GetDelegateForFunctionPointerInternal_m1_7861_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetDelegateForFunctionPointerInternal) (___ptr, ___t);
}
// System.Delegate System.Runtime.InteropServices.Marshal::GetDelegateForFunctionPointer(System.IntPtr,System.Type)
extern const Il2CppType* MulticastDelegate_t1_21_0_0_0_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1433;
extern Il2CppCodeGenString* _stringLiteral2126;
extern Il2CppCodeGenString* _stringLiteral88;
extern "C" Delegate_t1_22 * Marshal_GetDelegateForFunctionPointer_m1_7862 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MulticastDelegate_t1_21_0_0_0_var = il2cpp_codegen_type_from_index(57);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral1433 = il2cpp_codegen_string_literal_from_index(1433);
		_stringLiteral2126 = il2cpp_codegen_string_literal_from_index(2126);
		_stringLiteral88 = il2cpp_codegen_string_literal_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___t;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral1433, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Type_t * L_2 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MulticastDelegate_t1_21_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(164 /* System.Boolean System.Type::IsSubclassOf(System.Type) */, L_2, L_3);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Type_t * L_5 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MulticastDelegate_t1_21_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6))))
		{
			goto IL_0046;
		}
	}

IL_0036:
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_7, _stringLiteral2126, _stringLiteral1433, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0046:
	{
		IntPtr_t L_8 = ___ptr;
		IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_10 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0061;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_11 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_11, _stringLiteral88, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0061:
	{
		IntPtr_t L_12 = ___ptr;
		Type_t * L_13 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		Delegate_t1_22 * L_14 = Marshal_GetDelegateForFunctionPointerInternal_m1_7861(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetFunctionPointerForDelegateInternal(System.Delegate)
extern "C" IntPtr_t Marshal_GetFunctionPointerForDelegateInternal_m1_7863 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___d, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef IntPtr_t (*Marshal_GetFunctionPointerForDelegateInternal_m1_7863_ftn) (Delegate_t1_22 *);
	return  ((Marshal_GetFunctionPointerForDelegateInternal_m1_7863_ftn)mscorlib::System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegateInternal) (___d);
}
// System.IntPtr System.Runtime.InteropServices.Marshal::GetFunctionPointerForDelegate(System.Delegate)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t1_811_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral281;
extern "C" IntPtr_t Marshal_GetFunctionPointerForDelegate_m1_7864 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___d, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Marshal_t1_811_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral281 = il2cpp_codegen_string_literal_from_index(281);
		s_Il2CppMethodIntialized = true;
	}
	{
		Delegate_t1_22 * L_0 = ___d;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral281, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Delegate_t1_22 * L_2 = ___d;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t1_811_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = Marshal_GetFunctionPointerForDelegateInternal_m1_7863(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2127;
extern "C" void MarshalDirectiveException__ctor_m1_7865 (MarshalDirectiveException_t1_812 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2127 = il2cpp_codegen_string_literal_from_index(2127);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2127, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233035), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.String)
extern "C" void MarshalDirectiveException__ctor_m1_7866 (MarshalDirectiveException_t1_812 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233035), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.String,System.Exception)
extern "C" void MarshalDirectiveException__ctor_m1_7867 (MarshalDirectiveException_t1_812 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233035), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.MarshalDirectiveException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MarshalDirectiveException__ctor_m1_7868 (MarshalDirectiveException_t1_812 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.PARAMDESC
extern "C" void PARAMDESC_t1_784_marshal(const PARAMDESC_t1_784& unmarshaled, PARAMDESC_t1_784_marshaled& marshaled)
{
	marshaled.___lpVarValue_0 = reinterpret_cast<intptr_t>((unmarshaled.___lpVarValue_0).___m_value_0);
	marshaled.___wParamFlags_1 = unmarshaled.___wParamFlags_1;
}
extern "C" void PARAMDESC_t1_784_marshal_back(const PARAMDESC_t1_784_marshaled& marshaled, PARAMDESC_t1_784& unmarshaled)
{
	(unmarshaled.___lpVarValue_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___lpVarValue_0);
	unmarshaled.___wParamFlags_1 = marshaled.___wParamFlags_1;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.PARAMDESC
extern "C" void PARAMDESC_t1_784_marshal_cleanup(PARAMDESC_t1_784_marshaled& marshaled)
{
}
// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern "C" void PreserveSigAttribute__ctor_m1_7869 (PreserveSigAttribute_t1_814 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::.ctor(System.Int32,System.Int32)
extern "C" void PrimaryInteropAssemblyAttribute__ctor_m1_7870 (PrimaryInteropAssemblyAttribute_t1_815 * __this, int32_t ___major, int32_t ___minor, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		__this->___major_0 = L_0;
		int32_t L_1 = ___minor;
		__this->___minor_1 = L_1;
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::get_MajorVersion()
extern "C" int32_t PrimaryInteropAssemblyAttribute_get_MajorVersion_m1_7871 (PrimaryInteropAssemblyAttribute_t1_815 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___major_0);
		return L_0;
	}
}
// System.Int32 System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute::get_MinorVersion()
extern "C" int32_t PrimaryInteropAssemblyAttribute_get_MinorVersion_m1_7872 (PrimaryInteropAssemblyAttribute_t1_815 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___minor_1);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.ProgIdAttribute::.ctor(System.String)
extern "C" void ProgIdAttribute__ctor_m1_7873 (ProgIdAttribute_t1_816 * __this, String_t* ___progId, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___progId;
		__this->___pid_0 = L_0;
		return;
	}
}
// System.String System.Runtime.InteropServices.ProgIdAttribute::get_Value()
extern "C" String_t* ProgIdAttribute_get_Value_m1_7874 (ProgIdAttribute_t1_816 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___pid_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.RegistrationServices::.ctor()
extern "C" void RegistrationServices__ctor_m1_7875 (RegistrationServices_t1_819 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Guid System.Runtime.InteropServices.RegistrationServices::GetManagedCategoryGuid()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Guid_t1_319  RegistrationServices_GetManagedCategoryGuid_m1_7876 (RegistrationServices_t1_819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String System.Runtime.InteropServices.RegistrationServices::GetProgIdForType(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" String_t* RegistrationServices_GetProgIdForType_m1_7877 (RegistrationServices_t1_819 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Type[] System.Runtime.InteropServices.RegistrationServices::GetRegistrableTypesInAssembly(System.Reflection.Assembly)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" TypeU5BU5D_t1_31* RegistrationServices_GetRegistrableTypesInAssembly_m1_7878 (RegistrationServices_t1_819 * __this, Assembly_t1_467 * ___assembly, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.RegistrationServices::RegisterAssembly(System.Reflection.Assembly,System.Runtime.InteropServices.AssemblyRegistrationFlags)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool RegistrationServices_RegisterAssembly_m1_7879 (RegistrationServices_t1_819 * __this, Assembly_t1_467 * ___assembly, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.RegistrationServices::RegisterTypeForComClients(System.Type,System.Guid&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrationServices_RegisterTypeForComClients_m1_7880 (RegistrationServices_t1_819 * __this, Type_t * ___type, Guid_t1_319 * ___g, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.RegistrationServices::TypeRepresentsComType(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool RegistrationServices_TypeRepresentsComType_m1_7881 (RegistrationServices_t1_819 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.RegistrationServices::TypeRequiresRegistration(System.Type)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool RegistrationServices_TypeRequiresRegistration_m1_7882 (RegistrationServices_t1_819 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.RegistrationServices::UnregisterAssembly(System.Reflection.Assembly)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool RegistrationServices_UnregisterAssembly_m1_7883 (RegistrationServices_t1_819 * __this, Assembly_t1_467 * ___assembly, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Int32 System.Runtime.InteropServices.RegistrationServices::RegisterTypeForComClients(System.Type,System.Runtime.InteropServices.RegistrationClassContext,System.Runtime.InteropServices.RegistrationConnectionType)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" int32_t RegistrationServices_RegisterTypeForComClients_m1_7884 (RegistrationServices_t1_819 * __this, Type_t * ___type, int32_t ___classContext, int32_t ___flags, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.RegistrationServices::UnregisterTypeForComClients(System.Int32)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void RegistrationServices_UnregisterTypeForComClients_m1_7885 (RegistrationServices_t1_819 * __this, int32_t ___cookie, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.RuntimeEnvironment::.ctor()
extern "C" void RuntimeEnvironment__ctor_m1_7886 (RuntimeEnvironment_t1_820 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String System.Runtime.InteropServices.RuntimeEnvironment::get_SystemConfigurationFile()
extern TypeInfo* SecurityManager_t1_1406_il2cpp_TypeInfo_var;
extern TypeInfo* FileIOPermission_t1_1274_il2cpp_TypeInfo_var;
extern "C" String_t* RuntimeEnvironment_get_SystemConfigurationFile_m1_7887 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecurityManager_t1_1406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		FileIOPermission_t1_1274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(598);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = Environment_GetMachineConfigPath_m1_14086(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(SecurityManager_t1_1406_il2cpp_TypeInfo_var);
		bool L_1 = SecurityManager_get_SecurityEnabled_m1_12118(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = V_0;
		FileIOPermission_t1_1274 * L_3 = (FileIOPermission_t1_1274 *)il2cpp_codegen_object_new (FileIOPermission_t1_1274_il2cpp_TypeInfo_var);
		FileIOPermission__ctor_m1_10823(L_3, 8, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Security.CodeAccessPermission::Demand() */, L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Boolean System.Runtime.InteropServices.RuntimeEnvironment::FromGlobalAccessCache(System.Reflection.Assembly)
extern "C" bool RuntimeEnvironment_FromGlobalAccessCache_m1_7888 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method)
{
	{
		Assembly_t1_467 * L_0 = ___a;
		NullCheck(L_0);
		bool L_1 = Assembly_get_GlobalAssemblyCache_m1_6582(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String System.Runtime.InteropServices.RuntimeEnvironment::GetRuntimeDirectory()
extern const Il2CppType* Int32_t1_3_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern "C" String_t* RuntimeEnvironment_GetRuntimeDirectory_m1_7889 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_0_0_0_var = il2cpp_codegen_type_from_index(11);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int32_t1_3_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Assembly_t1_467 * L_1 = (Assembly_t1_467 *)VirtFuncInvoker0< Assembly_t1_467 * >::Invoke(156 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_0);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(54 /* System.String System.Reflection.Assembly::get_Location() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_3 = Path_GetDirectoryName_m1_5125(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String System.Runtime.InteropServices.RuntimeEnvironment::GetSystemVersion()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2128;
extern Il2CppCodeGenString* _stringLiteral56;
extern "C" String_t* RuntimeEnvironment_GetSystemVersion_m1_7890 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral2128 = il2cpp_codegen_string_literal_from_index(2128);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 6));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2128);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2128;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		Version_t1_578 * L_2 = Environment_get_Version_m1_14061(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Version_get_Major_m1_14729(L_2, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral56);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral56;
		ObjectU5BU5D_t1_272* L_7 = L_6;
		Version_t1_578 * L_8 = Environment_get_Version_m1_14061(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Version_get_Minor_m1_14730(L_8, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral56);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral56;
		ObjectU5BU5D_t1_272* L_13 = L_12;
		Version_t1_578 * L_14 = Environment_get_Version_m1_14061(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = Version_get_Build_m1_14728(L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5, sizeof(Object_t *))) = (Object_t *)L_17;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1_562(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Void System.Runtime.InteropServices.SEHException::.ctor()
extern "C" void SEHException__ctor_m1_7891 (SEHException_t1_821 * __this, const MethodInfo* method)
{
	{
		ExternalException__ctor_m1_7643(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SEHException::.ctor(System.String)
extern "C" void SEHException__ctor_m1_7892 (SEHException_t1_821 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		ExternalException__ctor_m1_7644(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SEHException::.ctor(System.String,System.Exception)
extern "C" void SEHException__ctor_m1_7893 (SEHException_t1_821 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		ExternalException__ctor_m1_7646(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SEHException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SEHException__ctor_m1_7894 (SEHException_t1_821 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		ExternalException__ctor_m1_7645(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.SEHException::CanResume()
extern "C" bool SEHException_CanResume_m1_7895 (SEHException_t1_821 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.STATSTG
extern "C" void STATSTG_t1_822_marshal(const STATSTG_t1_822& unmarshaled, STATSTG_t1_822_marshaled& marshaled)
{
	marshaled.___pwcsName_0 = il2cpp_codegen_marshal_string(unmarshaled.___pwcsName_0);
	marshaled.___type_1 = unmarshaled.___type_1;
	marshaled.___cbSize_2 = unmarshaled.___cbSize_2;
	marshaled.___mtime_3 = unmarshaled.___mtime_3;
	marshaled.___ctime_4 = unmarshaled.___ctime_4;
	marshaled.___atime_5 = unmarshaled.___atime_5;
	marshaled.___grfMode_6 = unmarshaled.___grfMode_6;
	marshaled.___grfLocksSupported_7 = unmarshaled.___grfLocksSupported_7;
	marshaled.___clsid_8 = unmarshaled.___clsid_8;
	marshaled.___grfStateBits_9 = unmarshaled.___grfStateBits_9;
	marshaled.___reserved_10 = unmarshaled.___reserved_10;
}
extern "C" void STATSTG_t1_822_marshal_back(const STATSTG_t1_822_marshaled& marshaled, STATSTG_t1_822& unmarshaled)
{
	unmarshaled.___pwcsName_0 = il2cpp_codegen_marshal_string_result(marshaled.___pwcsName_0);
	unmarshaled.___type_1 = marshaled.___type_1;
	unmarshaled.___cbSize_2 = marshaled.___cbSize_2;
	unmarshaled.___mtime_3 = marshaled.___mtime_3;
	unmarshaled.___ctime_4 = marshaled.___ctime_4;
	unmarshaled.___atime_5 = marshaled.___atime_5;
	unmarshaled.___grfMode_6 = marshaled.___grfMode_6;
	unmarshaled.___grfLocksSupported_7 = marshaled.___grfLocksSupported_7;
	unmarshaled.___clsid_8 = marshaled.___clsid_8;
	unmarshaled.___grfStateBits_9 = marshaled.___grfStateBits_9;
	unmarshaled.___reserved_10 = marshaled.___reserved_10;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.STATSTG
extern "C" void STATSTG_t1_822_marshal_cleanup(STATSTG_t1_822_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___pwcsName_0);
	marshaled.___pwcsName_0 = NULL;
}
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2129;
extern "C" void SafeArrayRankMismatchException__ctor_m1_7896 (SafeArrayRankMismatchException_t1_824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2129 = il2cpp_codegen_string_literal_from_index(2129);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2129, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233032), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor(System.String)
extern "C" void SafeArrayRankMismatchException__ctor_m1_7897 (SafeArrayRankMismatchException_t1_824 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233032), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor(System.String,System.Exception)
extern "C" void SafeArrayRankMismatchException__ctor_m1_7898 (SafeArrayRankMismatchException_t1_824 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233032), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayRankMismatchException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SafeArrayRankMismatchException__ctor_m1_7899 (SafeArrayRankMismatchException_t1_824 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayTypeMismatchException::.ctor()
extern Il2CppCodeGenString* _stringLiteral2130;
extern "C" void SafeArrayTypeMismatchException__ctor_m1_7900 (SafeArrayTypeMismatchException_t1_825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2130 = il2cpp_codegen_string_literal_from_index(2130);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Locale_GetText_m1_1404(NULL /*static, unused*/, _stringLiteral2130, /*hidden argument*/NULL);
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233037), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayTypeMismatchException::.ctor(System.String)
extern "C" void SafeArrayTypeMismatchException__ctor_m1_7901 (SafeArrayTypeMismatchException_t1_825 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		SystemException__ctor_m1_14605(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233037), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayTypeMismatchException::.ctor(System.String,System.Exception)
extern "C" void SafeArrayTypeMismatchException__ctor_m1_7902 (SafeArrayTypeMismatchException_t1_825 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___inner;
		SystemException__ctor_m1_14607(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2146233037), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeArrayTypeMismatchException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SafeArrayTypeMismatchException__ctor_m1_7903 (SafeArrayTypeMismatchException_t1_825 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		SystemException__ctor_m1_14606(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::.ctor()
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void SafeHandle__ctor_m1_7904 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		CriticalFinalizerObject__ctor_m1_7558(__this, /*hidden argument*/NULL);
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::.ctor(System.IntPtr,System.Boolean)
extern "C" void SafeHandle__ctor_m1_7905 (SafeHandle_t1_88 * __this, IntPtr_t ___invalidHandleValue, bool ___ownsHandle, const MethodInfo* method)
{
	{
		CriticalFinalizerObject__ctor_m1_7558(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___invalidHandleValue;
		__this->___invalid_handle_value_1 = L_0;
		bool L_1 = ___ownsHandle;
		__this->___owns_handle_3 = L_1;
		__this->___refcount_2 = 1;
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::Close()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern "C" void SafeHandle_Close_m1_7906 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->___refcount_2);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t1_1588 * L_3 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001c:
	{
		int32_t L_4 = (__this->___refcount_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_0 = ((int32_t)((int32_t)L_5-(int32_t)1));
		int32_t* L_6 = &(__this->___refcount_2);
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = Interlocked_CompareExchange_m1_12664(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_11 = V_0;
		if (L_11)
		{
			goto IL_0070;
		}
	}
	{
		bool L_12 = (__this->___owns_handle_3);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, __this);
		if (L_13)
		{
			goto IL_0070;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.Runtime.InteropServices.SafeHandle::ReleaseHandle() */, __this);
		IntPtr_t L_14 = (__this->___invalid_handle_value_1);
		__this->___handle_0 = L_14;
		__this->___refcount_2 = (-1);
	}

IL_0070:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousAddRef(System.Boolean&)
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern "C" void SafeHandle_DangerousAddRef_m1_7907 (SafeHandle_t1_88 * __this, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->___refcount_2);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t1_1588 * L_3 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001d:
	{
		int32_t L_4 = (__this->___refcount_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0040;
		}
	}
	{
		Type_t * L_7 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_7);
		ObjectDisposedException_t1_1588 * L_9 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0040:
	{
		int32_t* L_10 = &(__this->___refcount_2);
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = Interlocked_CompareExchange_m1_12664(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_001d;
		}
	}
	{
		bool* L_15 = ___success;
		*((int8_t*)(L_15)) = (int8_t)1;
		return;
	}
}
// System.IntPtr System.Runtime.InteropServices.SafeHandle::DangerousGetHandle()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern "C" IntPtr_t SafeHandle_DangerousGetHandle_m1_7908 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___refcount_2);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t1_1588 * L_3 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001d:
	{
		IntPtr_t L_4 = (__this->___handle_0);
		return L_4;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::DangerousRelease()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern "C" void SafeHandle_DangerousRelease_m1_7909 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->___refcount_2);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_1);
		ObjectDisposedException_t1_1588 * L_3 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_3, L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_001d:
	{
		int32_t L_4 = (__this->___refcount_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_0 = ((int32_t)((int32_t)L_5-(int32_t)1));
		int32_t* L_6 = &(__this->___refcount_2);
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = Interlocked_CompareExchange_m1_12664(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_11 = V_0;
		if (L_11)
		{
			goto IL_006a;
		}
	}
	{
		bool L_12 = (__this->___owns_handle_3);
		if (!L_12)
		{
			goto IL_006a;
		}
	}
	{
		bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, __this);
		if (L_13)
		{
			goto IL_006a;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.Runtime.InteropServices.SafeHandle::ReleaseHandle() */, __this);
		IntPtr_t L_14 = (__this->___invalid_handle_value_1);
		__this->___handle_0 = L_14;
	}

IL_006a:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose()
extern "C" void SafeHandle_Dispose_m1_7910 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void System.Runtime.InteropServices.SafeHandle::Dispose(System.Boolean) */, __this, 1);
		GC_SuppressFinalize_m1_14113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::SetHandleAsInvalid()
extern "C" void SafeHandle_SetHandleAsInvalid_m1_7911 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___invalid_handle_value_1);
		__this->___handle_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::Dispose(System.Boolean)
extern "C" void SafeHandle_Dispose_m1_7912 (SafeHandle_t1_88 * __this, bool ___disposing, const MethodInfo* method)
{
	{
		bool L_0 = ___disposing;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		SafeHandle_Close_m1_7906(__this, /*hidden argument*/NULL);
		goto IL_0011;
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::SetHandle(System.IntPtr)
extern "C" void SafeHandle_SetHandle_m1_7913 (SafeHandle_t1_88 * __this, IntPtr_t ___handle, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___handle;
		__this->___handle_0 = L_0;
		return;
	}
}
// System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsClosed()
extern "C" bool SafeHandle_get_IsClosed_m1_7914 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___refcount_2);
		return ((((int32_t)((((int32_t)L_0) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Runtime.InteropServices.SafeHandle::Finalize()
extern "C" void SafeHandle_Finalize_m1_7915 (SafeHandle_t1_88 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (__this->___owns_handle_3);
			if (!L_0)
			{
				goto IL_0029;
			}
		}

IL_000b:
		{
			bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.Runtime.InteropServices.SafeHandle::get_IsInvalid() */, __this);
			if (L_1)
			{
				goto IL_0029;
			}
		}

IL_0016:
		{
			VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean System.Runtime.InteropServices.SafeHandle::ReleaseHandle() */, __this);
			IntPtr_t L_2 = (__this->___invalid_handle_value_1);
			__this->___handle_0 = L_2;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		CriticalFinalizerObject_Finalize_m1_7559(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Runtime.InteropServices.SetWin32ContextInIDispatchAttribute::.ctor()
extern "C" void SetWin32ContextInIDispatchAttribute__ctor_m1_7916 (SetWin32ContextInIDispatchAttribute_t1_826 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.TYPEATTR
extern "C" void TYPEATTR_t1_827_marshal(const TYPEATTR_t1_827& unmarshaled, TYPEATTR_t1_827_marshaled& marshaled)
{
	marshaled.___guid_1 = unmarshaled.___guid_1;
	marshaled.___lcid_2 = unmarshaled.___lcid_2;
	marshaled.___dwReserved_3 = unmarshaled.___dwReserved_3;
	marshaled.___memidConstructor_4 = unmarshaled.___memidConstructor_4;
	marshaled.___memidDestructor_5 = unmarshaled.___memidDestructor_5;
	marshaled.___lpstrSchema_6 = reinterpret_cast<intptr_t>((unmarshaled.___lpstrSchema_6).___m_value_0);
	marshaled.___cbSizeInstance_7 = unmarshaled.___cbSizeInstance_7;
	marshaled.___typekind_8 = unmarshaled.___typekind_8;
	marshaled.___cFuncs_9 = unmarshaled.___cFuncs_9;
	marshaled.___cVars_10 = unmarshaled.___cVars_10;
	marshaled.___cImplTypes_11 = unmarshaled.___cImplTypes_11;
	marshaled.___cbSizeVft_12 = unmarshaled.___cbSizeVft_12;
	marshaled.___cbAlignment_13 = unmarshaled.___cbAlignment_13;
	marshaled.___wTypeFlags_14 = unmarshaled.___wTypeFlags_14;
	marshaled.___wMajorVerNum_15 = unmarshaled.___wMajorVerNum_15;
	marshaled.___wMinorVerNum_16 = unmarshaled.___wMinorVerNum_16;
	marshaled.___tdescAlias_17 = unmarshaled.___tdescAlias_17;
	IDLDESC_t1_783_marshal(unmarshaled.___idldescType_18, marshaled.___idldescType_18);
}
extern "C" void TYPEATTR_t1_827_marshal_back(const TYPEATTR_t1_827_marshaled& marshaled, TYPEATTR_t1_827& unmarshaled)
{
	unmarshaled.___guid_1 = marshaled.___guid_1;
	unmarshaled.___lcid_2 = marshaled.___lcid_2;
	unmarshaled.___dwReserved_3 = marshaled.___dwReserved_3;
	unmarshaled.___memidConstructor_4 = marshaled.___memidConstructor_4;
	unmarshaled.___memidDestructor_5 = marshaled.___memidDestructor_5;
	(unmarshaled.___lpstrSchema_6).___m_value_0 = reinterpret_cast<void*>(marshaled.___lpstrSchema_6);
	unmarshaled.___cbSizeInstance_7 = marshaled.___cbSizeInstance_7;
	unmarshaled.___typekind_8 = marshaled.___typekind_8;
	unmarshaled.___cFuncs_9 = marshaled.___cFuncs_9;
	unmarshaled.___cVars_10 = marshaled.___cVars_10;
	unmarshaled.___cImplTypes_11 = marshaled.___cImplTypes_11;
	unmarshaled.___cbSizeVft_12 = marshaled.___cbSizeVft_12;
	unmarshaled.___cbAlignment_13 = marshaled.___cbAlignment_13;
	unmarshaled.___wTypeFlags_14 = marshaled.___wTypeFlags_14;
	unmarshaled.___wMajorVerNum_15 = marshaled.___wMajorVerNum_15;
	unmarshaled.___wMinorVerNum_16 = marshaled.___wMinorVerNum_16;
	unmarshaled.___tdescAlias_17 = marshaled.___tdescAlias_17;
	IDLDESC_t1_783_marshal_back(marshaled.___idldescType_18, unmarshaled.___idldescType_18);
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.TYPEATTR
extern "C" void TYPEATTR_t1_827_marshal_cleanup(TYPEATTR_t1_827_marshaled& marshaled)
{
	IDLDESC_t1_783_marshal_cleanup(marshaled.___idldescType_18);
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.TYPELIBATTR
extern "C" void TYPELIBATTR_t1_830_marshal(const TYPELIBATTR_t1_830& unmarshaled, TYPELIBATTR_t1_830_marshaled& marshaled)
{
	marshaled.___guid_0 = unmarshaled.___guid_0;
	marshaled.___lcid_1 = unmarshaled.___lcid_1;
	marshaled.___syskind_2 = unmarshaled.___syskind_2;
	marshaled.___wMajorVerNum_3 = unmarshaled.___wMajorVerNum_3;
	marshaled.___wMinorVerNum_4 = unmarshaled.___wMinorVerNum_4;
	marshaled.___wLibFlags_5 = unmarshaled.___wLibFlags_5;
}
extern "C" void TYPELIBATTR_t1_830_marshal_back(const TYPELIBATTR_t1_830_marshaled& marshaled, TYPELIBATTR_t1_830& unmarshaled)
{
	unmarshaled.___guid_0 = marshaled.___guid_0;
	unmarshaled.___lcid_1 = marshaled.___lcid_1;
	unmarshaled.___syskind_2 = marshaled.___syskind_2;
	unmarshaled.___wMajorVerNum_3 = marshaled.___wMajorVerNum_3;
	unmarshaled.___wMinorVerNum_4 = marshaled.___wMinorVerNum_4;
	unmarshaled.___wLibFlags_5 = marshaled.___wLibFlags_5;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.TYPELIBATTR
extern "C" void TYPELIBATTR_t1_830_marshal_cleanup(TYPELIBATTR_t1_830_marshaled& marshaled)
{
}
// System.Void System.Runtime.InteropServices.TypeLibConverter::.ctor()
extern "C" void TypeLibConverter__ctor_m1_7917 (TypeLibConverter_t1_831 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Runtime.InteropServices.TypeLibConverter::ConvertAssemblyToTypeLib(System.Reflection.Assembly,System.String,System.Runtime.InteropServices.TypeLibExporterFlags,System.Runtime.InteropServices.ITypeLibExporterNotifySink)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Object_t * TypeLibConverter_ConvertAssemblyToTypeLib_m1_7918 (TypeLibConverter_t1_831 * __this, Assembly_t1_467 * ___assembly, String_t* ___strTypeLibName, int32_t ___flags, Object_t * ___notifySink, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Reflection.Emit.AssemblyBuilder System.Runtime.InteropServices.TypeLibConverter::ConvertTypeLibToAssembly(System.Object,System.String,System.Int32,System.Runtime.InteropServices.ITypeLibImporterNotifySink,System.Byte[],System.Reflection.StrongNameKeyPair,System.Boolean)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AssemblyBuilder_t1_466 * TypeLibConverter_ConvertTypeLibToAssembly_m1_7919 (TypeLibConverter_t1_831 * __this, Object_t * ___typeLib, String_t* ___asmFileName, int32_t ___flags, Object_t * ___notifySink, ByteU5BU5D_t1_109* ___publicKey, StrongNameKeyPair_t1_577 * ___keyPair, bool ___unsafeInterfaces, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Reflection.Emit.AssemblyBuilder System.Runtime.InteropServices.TypeLibConverter::ConvertTypeLibToAssembly(System.Object,System.String,System.Runtime.InteropServices.TypeLibImporterFlags,System.Runtime.InteropServices.ITypeLibImporterNotifySink,System.Byte[],System.Reflection.StrongNameKeyPair,System.String,System.Version)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" AssemblyBuilder_t1_466 * TypeLibConverter_ConvertTypeLibToAssembly_m1_7920 (TypeLibConverter_t1_831 * __this, Object_t * ___typeLib, String_t* ___asmFileName, int32_t ___flags, Object_t * ___notifySink, ByteU5BU5D_t1_109* ___publicKey, StrongNameKeyPair_t1_577 * ___keyPair, String_t* ___asmNamespace, Version_t1_578 * ___asmVersion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.InteropServices.TypeLibConverter::GetPrimaryInteropAssembly(System.Guid,System.Int32,System.Int32,System.Int32,System.String&,System.String&)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" bool TypeLibConverter_GetPrimaryInteropAssembly_m1_7921 (TypeLibConverter_t1_831 * __this, Guid_t1_319  ___g, int32_t ___major, int32_t ___minor, int32_t ___lcid, String_t** ___asmName, String_t** ___asmCodeBase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.InteropServices.TypeLibFuncAttribute::.ctor(System.Int16)
extern "C" void TypeLibFuncAttribute__ctor_m1_7922 (TypeLibFuncAttribute_t1_833 * __this, int16_t ___flags, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int16_t L_0 = ___flags;
		__this->___flags_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibFuncAttribute::.ctor(System.Runtime.InteropServices.TypeLibFuncFlags)
extern "C" void TypeLibFuncAttribute__ctor_m1_7923 (TypeLibFuncAttribute_t1_833 * __this, int32_t ___flags, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___flags;
		__this->___flags_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.TypeLibFuncFlags System.Runtime.InteropServices.TypeLibFuncAttribute::get_Value()
extern "C" int32_t TypeLibFuncAttribute_get_Value_m1_7924 (TypeLibFuncAttribute_t1_833 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___flags_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern "C" void TypeLibImportClassAttribute__ctor_m1_7925 (TypeLibImportClassAttribute_t1_835 * __this, Type_t * ___importClass, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___importClass;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_0);
		__this->____importClass_0 = L_1;
		return;
	}
}
// System.String System.Runtime.InteropServices.TypeLibImportClassAttribute::get_Value()
extern "C" String_t* TypeLibImportClassAttribute_get_Value_m1_7926 (TypeLibImportClassAttribute_t1_835 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____importClass_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibTypeAttribute::.ctor(System.Int16)
extern "C" void TypeLibTypeAttribute__ctor_m1_7927 (TypeLibTypeAttribute_t1_837 * __this, int16_t ___flags, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int16_t L_0 = ___flags;
		__this->___flags_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibTypeAttribute::.ctor(System.Runtime.InteropServices.TypeLibTypeFlags)
extern "C" void TypeLibTypeAttribute__ctor_m1_7928 (TypeLibTypeAttribute_t1_837 * __this, int32_t ___flags, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___flags;
		__this->___flags_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.TypeLibTypeFlags System.Runtime.InteropServices.TypeLibTypeAttribute::get_Value()
extern "C" int32_t TypeLibTypeAttribute_get_Value_m1_7929 (TypeLibTypeAttribute_t1_837 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___flags_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibVarAttribute::.ctor(System.Int16)
extern "C" void TypeLibVarAttribute__ctor_m1_7930 (TypeLibVarAttribute_t1_839 * __this, int16_t ___flags, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int16_t L_0 = ___flags;
		__this->___flags_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibVarAttribute::.ctor(System.Runtime.InteropServices.TypeLibVarFlags)
extern "C" void TypeLibVarAttribute__ctor_m1_7931 (TypeLibVarAttribute_t1_839 * __this, int32_t ___flags, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___flags;
		__this->___flags_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.TypeLibVarFlags System.Runtime.InteropServices.TypeLibVarAttribute::get_Value()
extern "C" int32_t TypeLibVarAttribute_get_Value_m1_7932 (TypeLibVarAttribute_t1_839 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___flags_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
extern "C" void TypeLibVersionAttribute__ctor_m1_7933 (TypeLibVersionAttribute_t1_841 * __this, int32_t ___major, int32_t ___minor, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___major;
		__this->___major_0 = L_0;
		int32_t L_1 = ___minor;
		__this->___minor_1 = L_1;
		return;
	}
}
// System.Int32 System.Runtime.InteropServices.TypeLibVersionAttribute::get_MajorVersion()
extern "C" int32_t TypeLibVersionAttribute_get_MajorVersion_m1_7934 (TypeLibVersionAttribute_t1_841 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___major_0);
		return L_0;
	}
}
// System.Int32 System.Runtime.InteropServices.TypeLibVersionAttribute::get_MinorVersion()
extern "C" int32_t TypeLibVersionAttribute_get_MinorVersion_m1_7935 (TypeLibVersionAttribute_t1_841 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___minor_1);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.UnknownWrapper::.ctor(System.Object)
extern "C" void UnknownWrapper__ctor_m1_7936 (UnknownWrapper_t1_842 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___obj;
		__this->___InternalObject_0 = L_0;
		return;
	}
}
// System.Object System.Runtime.InteropServices.UnknownWrapper::get_WrappedObject()
extern "C" Object_t * UnknownWrapper_get_WrappedObject_m1_7937 (UnknownWrapper_t1_842 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___InternalObject_0);
		return L_0;
	}
}
// System.Void System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::.ctor(System.Runtime.InteropServices.CallingConvention)
extern "C" void UnmanagedFunctionPointerAttribute__ctor_m1_7938 (UnmanagedFunctionPointerAttribute_t1_843 * __this, int32_t ___callingConvention, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___callingConvention;
		__this->___call_conv_0 = L_0;
		return;
	}
}
// System.Runtime.InteropServices.CallingConvention System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::get_CallingConvention()
extern "C" int32_t UnmanagedFunctionPointerAttribute_get_CallingConvention_m1_7939 (UnmanagedFunctionPointerAttribute_t1_843 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___call_conv_0);
		return L_0;
	}
}
// Conversion methods for marshalling of: System.Runtime.InteropServices.VARDESC
extern "C" void VARDESC_t1_846_marshal(const VARDESC_t1_846& unmarshaled, VARDESC_t1_846_marshaled& marshaled)
{
	marshaled.___memid_0 = unmarshaled.___memid_0;
	marshaled.___lpstrSchema_1 = il2cpp_codegen_marshal_string(unmarshaled.___lpstrSchema_1);
	ELEMDESC_t1_785_marshal(unmarshaled.___elemdescVar_2, marshaled.___elemdescVar_2);
	marshaled.___wVarFlags_3 = unmarshaled.___wVarFlags_3;
	marshaled.___varkind_4 = unmarshaled.___varkind_4;
}
extern "C" void VARDESC_t1_846_marshal_back(const VARDESC_t1_846_marshaled& marshaled, VARDESC_t1_846& unmarshaled)
{
	unmarshaled.___memid_0 = marshaled.___memid_0;
	unmarshaled.___lpstrSchema_1 = il2cpp_codegen_marshal_string_result(marshaled.___lpstrSchema_1);
	ELEMDESC_t1_785_marshal_back(marshaled.___elemdescVar_2, unmarshaled.___elemdescVar_2);
	unmarshaled.___wVarFlags_3 = marshaled.___wVarFlags_3;
	unmarshaled.___varkind_4 = marshaled.___varkind_4;
}
// Conversion method for clean up from marshalling of: System.Runtime.InteropServices.VARDESC
extern "C" void VARDESC_t1_846_marshal_cleanup(VARDESC_t1_846_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___lpstrSchema_1);
	marshaled.___lpstrSchema_1 = NULL;
	ELEMDESC_t1_785_marshal_cleanup(marshaled.___elemdescVar_2);
}
// System.Void System.Runtime.InteropServices.VariantWrapper::.ctor(System.Object)
extern "C" void VariantWrapper__ctor_m1_7940 (VariantWrapper_t1_849 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___obj;
		__this->____wrappedObject_0 = L_0;
		return;
	}
}
// System.Object System.Runtime.InteropServices.VariantWrapper::get_WrappedObject()
extern "C" Object_t * VariantWrapper_get_WrappedObject_m1_7941 (VariantWrapper_t1_849 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->____wrappedObject_0);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Activation.ActivationServices::.ctor()
extern "C" void ActivationServices__ctor_m1_7942 (ActivationServices_t1_850 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::get_ConstructionActivator()
extern TypeInfo* ActivationServices_t1_850_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructionLevelActivator_t1_854_il2cpp_TypeInfo_var;
extern "C" Object_t * ActivationServices_get_ConstructionActivator_m1_7943 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ActivationServices_t1_850_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(599);
		ConstructionLevelActivator_t1_854_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(600);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((ActivationServices_t1_850_StaticFields*)ActivationServices_t1_850_il2cpp_TypeInfo_var->static_fields)->____constructionActivator_0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ConstructionLevelActivator_t1_854 * L_1 = (ConstructionLevelActivator_t1_854 *)il2cpp_codegen_object_new (ConstructionLevelActivator_t1_854_il2cpp_TypeInfo_var);
		ConstructionLevelActivator__ctor_m1_7957(L_1, /*hidden argument*/NULL);
		((ActivationServices_t1_850_StaticFields*)ActivationServices_t1_850_il2cpp_TypeInfo_var->static_fields)->____constructionActivator_0 = L_1;
	}

IL_0014:
	{
		Object_t * L_2 = ((ActivationServices_t1_850_StaticFields*)ActivationServices_t1_850_il2cpp_TypeInfo_var->static_fields)->____constructionActivator_0;
		return L_2;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Activation.ActivationServices::Activate(System.Runtime.Remoting.Proxies.RemotingProxy,System.Runtime.Remoting.Messaging.ConstructionCall)
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern "C" Object_t * ActivationServices_Activate_m1_7944 (Object_t * __this /* static, unused */, RemotingProxy_t1_932 * ___proxy, ConstructionCall_t1_930 * ___ctorCall, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Identity_t1_943 * V_1 = {0};
	{
		ConstructionCall_t1_930 * L_0 = ___ctorCall;
		RemotingProxy_t1_932 * L_1 = ___proxy;
		NullCheck(L_0);
		ConstructionCall_set_SourceProxy_m1_8363(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_2 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = Context_get_HasExitSinks_m1_8143(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		ConstructionCall_t1_930 * L_4 = ___ctorCall;
		NullCheck(L_4);
		bool L_5 = ConstructionCall_get_IsContextOk_m1_8350(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_6 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Object_t * L_7 = Context_GetClientContextSinkChain_m1_8149(L_6, /*hidden argument*/NULL);
		ConstructionCall_t1_930 * L_8 = ___ctorCall;
		NullCheck(L_7);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.IMessageSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_7, L_8);
		V_0 = L_9;
		goto IL_003e;
	}

IL_0037:
	{
		ConstructionCall_t1_930 * L_10 = ___ctorCall;
		Object_t * L_11 = ActivationServices_RemoteActivate_m1_7945(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
	}

IL_003e:
	{
		Object_t * L_12 = V_0;
		if (!((Object_t *)IsInst(L_12, IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var)))
		{
			goto IL_0072;
		}
	}
	{
		Object_t * L_13 = V_0;
		NullCheck(((Object_t *)Castclass(L_13, IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var)));
		Exception_t1_33 * L_14 = (Exception_t1_33 *)InterfaceFuncInvoker0< Exception_t1_33 * >::Invoke(0 /* System.Exception System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_Exception() */, IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var, ((Object_t *)Castclass(L_13, IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var)));
		if (L_14)
		{
			goto IL_0072;
		}
	}
	{
		RemotingProxy_t1_932 * L_15 = ___proxy;
		NullCheck(L_15);
		Identity_t1_943 * L_16 = RealProxy_get_ObjectIdentity_m1_8997(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_0072;
		}
	}
	{
		ConstructionCall_t1_930 * L_17 = ___ctorCall;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Identity_t1_943 * L_18 = RemotingServices_GetMessageTargetIdentity_m1_9234(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		RemotingProxy_t1_932 * L_19 = ___proxy;
		Identity_t1_943 * L_20 = V_1;
		NullCheck(L_19);
		RemotingProxy_AttachIdentity_m1_9018(L_19, L_20, /*hidden argument*/NULL);
	}

IL_0072:
	{
		Object_t * L_21 = V_0;
		return L_21;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Activation.ActivationServices::RemoteActivate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* IActivator_t1_851_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* ReturnMessage_t1_960_il2cpp_TypeInfo_var;
extern "C" Object_t * ActivationServices_RemoteActivate_m1_7945 (Object_t * __this /* static, unused */, Object_t * ___ctorCall, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		IActivator_t1_851_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		ReturnMessage_t1_960_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___ctorCall;
			NullCheck(L_0);
			Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IConstructionCallMessage::get_Activator() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_0);
			Object_t * L_2 = ___ctorCall;
			NullCheck(L_1);
			Object_t * L_3 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(3 /* System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.IActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IActivator_t1_851_il2cpp_TypeInfo_var, L_1, L_2);
			V_1 = L_3;
			goto IL_002a;
		}

IL_0012:
		{
			; // IL_0012: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		{
			V_0 = ((Exception_t1_33 *)__exception_local);
			Exception_t1_33 * L_4 = V_0;
			Object_t * L_5 = ___ctorCall;
			ReturnMessage_t1_960 * L_6 = (ReturnMessage_t1_960 *)il2cpp_codegen_object_new (ReturnMessage_t1_960_il2cpp_TypeInfo_var);
			ReturnMessage__ctor_m1_8624(L_6, L_4, L_5, /*hidden argument*/NULL);
			V_1 = L_6;
			goto IL_002a;
		}

IL_0025:
		{
			; // IL_0025: leave IL_002a
		}
	} // end catch (depth: 1)

IL_002a:
	{
		Object_t * L_7 = V_1;
		return L_7;
	}
}
// System.Object System.Runtime.Remoting.Activation.ActivationServices::CreateProxyFromAttributes(System.Type,System.Object[])
extern TypeInfo* IContextAttribute_t1_1764_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* UrlAttribute_t1_858_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2131;
extern "C" Object_t * ActivationServices_CreateProxyFromAttributes_m1_7946 (Object_t * __this /* static, unused */, Type_t * ___type, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IContextAttribute_t1_1764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		UrlAttribute_t1_858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		_stringLiteral2131 = il2cpp_codegen_string_literal_from_index(2131);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Object_t * V_1 = {0};
	ObjectU5BU5D_t1_272* V_2 = {0};
	int32_t V_3 = 0;
	ActivatedClientTypeEntry_t1_1008 * V_4 = {0};
	{
		V_0 = (String_t*)NULL;
		ObjectU5BU5D_t1_272* L_0 = ___activationAttributes;
		V_2 = L_0;
		V_3 = 0;
		goto IL_0040;
	}

IL_000b:
	{
		ObjectU5BU5D_t1_272* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_3, sizeof(Object_t *)));
		Object_t * L_4 = V_1;
		if (((Object_t *)IsInst(L_4, IContextAttribute_t1_1764_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		RemotingException_t1_1025 * L_5 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_5, _stringLiteral2131, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0025:
	{
		Object_t * L_6 = V_1;
		if (!((UrlAttribute_t1_858 *)IsInstSealed(L_6, UrlAttribute_t1_858_il2cpp_TypeInfo_var)))
		{
			goto IL_003c;
		}
	}
	{
		Object_t * L_7 = V_1;
		NullCheck(((UrlAttribute_t1_858 *)CastclassSealed(L_7, UrlAttribute_t1_858_il2cpp_TypeInfo_var)));
		String_t* L_8 = UrlAttribute_get_UrlValue_m1_7978(((UrlAttribute_t1_858 *)CastclassSealed(L_7, UrlAttribute_t1_858_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_003c:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_10 = V_3;
		ObjectU5BU5D_t1_272* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_000b;
		}
	}
	{
		String_t* L_12 = V_0;
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		Type_t * L_13 = ___type;
		String_t* L_14 = V_0;
		ObjectU5BU5D_t1_272* L_15 = ___activationAttributes;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_16 = RemotingServices_CreateClientProxy_m1_9213(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_0058:
	{
		Type_t * L_17 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		ActivatedClientTypeEntry_t1_1008 * L_18 = RemotingConfiguration_IsRemotelyActivatedClientType_m1_9122(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		ActivatedClientTypeEntry_t1_1008 * L_19 = V_4;
		if (!L_19)
		{
			goto IL_0070;
		}
	}
	{
		ActivatedClientTypeEntry_t1_1008 * L_20 = V_4;
		ObjectU5BU5D_t1_272* L_21 = ___activationAttributes;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_22 = RemotingServices_CreateClientProxy_m1_9212(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_0070:
	{
		Type_t * L_23 = ___type;
		NullCheck(L_23);
		bool L_24 = (bool)VirtFuncInvoker0< bool >::Invoke(126 /* System.Boolean System.Type::get_IsContextful() */, L_23);
		if (!L_24)
		{
			goto IL_0083;
		}
	}
	{
		Type_t * L_25 = ___type;
		ObjectU5BU5D_t1_272* L_26 = ___activationAttributes;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_27 = RemotingServices_CreateClientProxyForContextBound_m1_9215(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		return L_27;
	}

IL_0083:
	{
		return NULL;
	}
}
// System.Runtime.Remoting.Messaging.ConstructionCall System.Runtime.Remoting.Activation.ActivationServices::CreateConstructionCall(System.Type,System.String,System.Object[])
extern TypeInfo* ConstructionCall_t1_930_il2cpp_TypeInfo_var;
extern TypeInfo* AppDomainLevelActivator_t1_853_il2cpp_TypeInfo_var;
extern TypeInfo* ContextLevelActivator_t1_855_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IContextAttribute_t1_1764_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" ConstructionCall_t1_930 * ActivationServices_CreateConstructionCall_m1_7947 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___activationUrl, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructionCall_t1_930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		AppDomainLevelActivator_t1_853_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(611);
		ContextLevelActivator_t1_855_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(612);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IContextAttribute_t1_1764_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	ConstructionCall_t1_930 * V_0 = {0};
	Object_t * V_1 = {0};
	ArrayList_t1_170 * V_2 = {0};
	bool V_3 = false;
	Context_t1_891 * V_4 = {0};
	Object_t * V_5 = {0};
	Object_t * V_6 = {0};
	ObjectU5BU5D_t1_272* V_7 = {0};
	Object_t * V_8 = {0};
	ObjectU5BU5D_t1_272* V_9 = {0};
	int32_t V_10 = 0;
	Object_t * V_11 = {0};
	Object_t * V_12 = {0};
	Object_t * V_13 = {0};
	Object_t * V_14 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B19_0 = 0;
	{
		Type_t * L_0 = ___type;
		ConstructionCall_t1_930 * L_1 = (ConstructionCall_t1_930 *)il2cpp_codegen_object_new (ConstructionCall_t1_930_il2cpp_TypeInfo_var);
		ConstructionCall__ctor_m1_8346(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = ___type;
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(126 /* System.Boolean System.Type::get_IsContextful() */, L_2);
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		ConstructionCall_t1_930 * L_4 = V_0;
		String_t* L_5 = ___activationUrl;
		Object_t * L_6 = ActivationServices_get_ConstructionActivator_m1_7943(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppDomainLevelActivator_t1_853 * L_7 = (AppDomainLevelActivator_t1_853 *)il2cpp_codegen_object_new (AppDomainLevelActivator_t1_853_il2cpp_TypeInfo_var);
		AppDomainLevelActivator__ctor_m1_7952(L_7, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< Object_t * >::Invoke(36 /* System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_Activator(System.Runtime.Remoting.Activation.IActivator) */, L_4, L_7);
		ConstructionCall_t1_930 * L_8 = V_0;
		NullCheck(L_8);
		ConstructionCall_set_IsContextOk_m1_8351(L_8, 0, /*hidden argument*/NULL);
		ConstructionCall_t1_930 * L_9 = V_0;
		return L_9;
	}

IL_002c:
	{
		Object_t * L_10 = ActivationServices_get_ConstructionActivator_m1_7943(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_10;
		Object_t * L_11 = V_1;
		ContextLevelActivator_t1_855 * L_12 = (ContextLevelActivator_t1_855 *)il2cpp_codegen_object_new (ContextLevelActivator_t1_855_il2cpp_TypeInfo_var);
		ContextLevelActivator__ctor_m1_7962(L_12, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		ArrayList_t1_170 * L_13 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_13, /*hidden argument*/NULL);
		V_2 = L_13;
		ObjectU5BU5D_t1_272* L_14 = ___activationAttributes;
		if (!L_14)
		{
			goto IL_004c;
		}
	}
	{
		ArrayList_t1_170 * L_15 = V_2;
		ObjectU5BU5D_t1_272* L_16 = ___activationAttributes;
		NullCheck(L_15);
		VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_15, (Object_t *)(Object_t *)L_16);
	}

IL_004c:
	{
		String_t* L_17 = ___activationUrl;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		String_t* L_18 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___CrossContextUrl_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1_601(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_20 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_20;
		bool L_21 = V_3;
		if (!L_21)
		{
			goto IL_00bd;
		}
	}
	{
		ArrayList_t1_170 * L_22 = V_2;
		NullCheck(L_22);
		Object_t * L_23 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_22);
		V_6 = L_23;
	}

IL_006d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0096;
		}

IL_0072:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			Object_t * L_25 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_24);
			V_5 = ((Object_t *)Castclass(L_25, IContextAttribute_t1_1764_il2cpp_TypeInfo_var));
			Object_t * L_26 = V_5;
			Context_t1_891 * L_27 = V_4;
			ConstructionCall_t1_930 * L_28 = V_0;
			NullCheck(L_26);
			bool L_29 = (bool)InterfaceFuncInvoker2< bool, Context_t1_891 *, Object_t * >::Invoke(1 /* System.Boolean System.Runtime.Remoting.Contexts.IContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IContextAttribute_t1_1764_il2cpp_TypeInfo_var, L_26, L_27, L_28);
			if (L_29)
			{
				goto IL_0096;
			}
		}

IL_008f:
		{
			V_3 = 0;
			goto IL_00a2;
		}

IL_0096:
		{
			Object_t * L_30 = V_6;
			NullCheck(L_30);
			bool L_31 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_30);
			if (L_31)
			{
				goto IL_0072;
			}
		}

IL_00a2:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00a7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00a7;
	}

FINALLY_00a7:
	{ // begin finally (depth: 1)
		{
			Object_t * L_32 = V_6;
			V_13 = ((Object_t *)IsInst(L_32, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_33 = V_13;
			if (L_33)
			{
				goto IL_00b5;
			}
		}

IL_00b4:
		{
			IL2CPP_END_FINALLY(167)
		}

IL_00b5:
		{
			Object_t * L_34 = V_13;
			NullCheck(L_34);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_34);
			IL2CPP_END_FINALLY(167)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(167)
	{
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00bd:
	{
		Type_t * L_35 = ___type;
		NullCheck(L_35);
		ObjectU5BU5D_t1_272* L_36 = (ObjectU5BU5D_t1_272*)VirtFuncInvoker1< ObjectU5BU5D_t1_272*, bool >::Invoke(28 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_35, 1);
		V_7 = L_36;
		ObjectU5BU5D_t1_272* L_37 = V_7;
		V_9 = L_37;
		V_10 = 0;
		goto IL_010d;
	}

IL_00d2:
	{
		ObjectU5BU5D_t1_272* L_38 = V_9;
		int32_t L_39 = V_10;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = L_39;
		V_8 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_38, L_40, sizeof(Object_t *)));
		Object_t * L_41 = V_8;
		if (!((Object_t *)IsInst(L_41, IContextAttribute_t1_1764_il2cpp_TypeInfo_var)))
		{
			goto IL_0107;
		}
	}
	{
		bool L_42 = V_3;
		if (!L_42)
		{
			goto IL_00fc;
		}
	}
	{
		Object_t * L_43 = V_8;
		Context_t1_891 * L_44 = V_4;
		ConstructionCall_t1_930 * L_45 = V_0;
		NullCheck(((Object_t *)Castclass(L_43, IContextAttribute_t1_1764_il2cpp_TypeInfo_var)));
		bool L_46 = (bool)InterfaceFuncInvoker2< bool, Context_t1_891 *, Object_t * >::Invoke(1 /* System.Boolean System.Runtime.Remoting.Contexts.IContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IContextAttribute_t1_1764_il2cpp_TypeInfo_var, ((Object_t *)Castclass(L_43, IContextAttribute_t1_1764_il2cpp_TypeInfo_var)), L_44, L_45);
		G_B19_0 = ((int32_t)(L_46));
		goto IL_00fd;
	}

IL_00fc:
	{
		G_B19_0 = 0;
	}

IL_00fd:
	{
		V_3 = G_B19_0;
		ArrayList_t1_170 * L_47 = V_2;
		Object_t * L_48 = V_8;
		NullCheck(L_47);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_47, L_48);
	}

IL_0107:
	{
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_010d:
	{
		int32_t L_50 = V_10;
		ObjectU5BU5D_t1_272* L_51 = V_9;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_51)->max_length)))))))
		{
			goto IL_00d2;
		}
	}
	{
		bool L_52 = V_3;
		if (L_52)
		{
			goto IL_0174;
		}
	}
	{
		ConstructionCall_t1_930 * L_53 = V_0;
		ArrayList_t1_170 * L_54 = V_2;
		NullCheck(L_54);
		ObjectU5BU5D_t1_272* L_55 = (ObjectU5BU5D_t1_272*)VirtFuncInvoker0< ObjectU5BU5D_t1_272* >::Invoke(62 /* System.Object[] System.Collections.ArrayList::ToArray() */, L_54);
		NullCheck(L_53);
		ConstructionCall_SetActivationAttributes_m1_8357(L_53, L_55, /*hidden argument*/NULL);
		ArrayList_t1_170 * L_56 = V_2;
		NullCheck(L_56);
		Object_t * L_57 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_56);
		V_12 = L_57;
	}

IL_0132:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014d;
		}

IL_0137:
		{
			Object_t * L_58 = V_12;
			NullCheck(L_58);
			Object_t * L_59 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_58);
			V_11 = ((Object_t *)Castclass(L_59, IContextAttribute_t1_1764_il2cpp_TypeInfo_var));
			Object_t * L_60 = V_11;
			ConstructionCall_t1_930 * L_61 = V_0;
			NullCheck(L_60);
			InterfaceActionInvoker1< Object_t * >::Invoke(0 /* System.Void System.Runtime.Remoting.Contexts.IContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IContextAttribute_t1_1764_il2cpp_TypeInfo_var, L_60, L_61);
		}

IL_014d:
		{
			Object_t * L_62 = V_12;
			NullCheck(L_62);
			bool L_63 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_62);
			if (L_63)
			{
				goto IL_0137;
			}
		}

IL_0159:
		{
			IL2CPP_LEAVE(0x174, FINALLY_015e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_015e;
	}

FINALLY_015e:
	{ // begin finally (depth: 1)
		{
			Object_t * L_64 = V_12;
			V_14 = ((Object_t *)IsInst(L_64, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_65 = V_14;
			if (L_65)
			{
				goto IL_016c;
			}
		}

IL_016b:
		{
			IL2CPP_END_FINALLY(350)
		}

IL_016c:
		{
			Object_t * L_66 = V_14;
			NullCheck(L_66);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_66);
			IL2CPP_END_FINALLY(350)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(350)
	{
		IL2CPP_JUMP_TBL(0x174, IL_0174)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0174:
	{
		String_t* L_67 = ___activationUrl;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		String_t* L_68 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___CrossContextUrl_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_69 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_018c;
		}
	}
	{
		String_t* L_70 = ___activationUrl;
		Object_t * L_71 = V_1;
		AppDomainLevelActivator_t1_853 * L_72 = (AppDomainLevelActivator_t1_853 *)il2cpp_codegen_object_new (AppDomainLevelActivator_t1_853_il2cpp_TypeInfo_var);
		AppDomainLevelActivator__ctor_m1_7952(L_72, L_70, L_71, /*hidden argument*/NULL);
		V_1 = L_72;
	}

IL_018c:
	{
		ConstructionCall_t1_930 * L_73 = V_0;
		Object_t * L_74 = V_1;
		NullCheck(L_73);
		VirtActionInvoker1< Object_t * >::Invoke(36 /* System.Void System.Runtime.Remoting.Messaging.ConstructionCall::set_Activator(System.Runtime.Remoting.Activation.IActivator) */, L_73, L_74);
		ConstructionCall_t1_930 * L_75 = V_0;
		bool L_76 = V_3;
		NullCheck(L_75);
		ConstructionCall_set_IsContextOk_m1_8351(L_75, L_76, /*hidden argument*/NULL);
		ConstructionCall_t1_930 * L_77 = V_0;
		return L_77;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Activation.ActivationServices::CreateInstanceFromMessage(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* ServerIdentity_t1_70_il2cpp_TypeInfo_var;
extern TypeInfo* MarshalByRefObject_t1_69_il2cpp_TypeInfo_var;
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructionCall_t1_930_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodMessage_t1_948_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructionResponse_t1_935_il2cpp_TypeInfo_var;
extern "C" Object_t * ActivationServices_CreateInstanceFromMessage_m1_7948 (Object_t * __this /* static, unused */, Object_t * ___ctorCall, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		ServerIdentity_t1_70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		MarshalByRefObject_t1_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		ConstructionCall_t1_930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		IMethodMessage_t1_948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		ConstructionResponse_t1_935_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(616);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ServerIdentity_t1_70 * V_1 = {0};
	ConstructionCall_t1_930 * V_2 = {0};
	MarshalByRefObject_t1_69 * V_3 = {0};
	{
		Object_t * L_0 = ___ctorCall;
		NullCheck(L_0);
		Type_t * L_1 = (Type_t *)InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_0);
		Object_t * L_2 = ActivationServices_AllocateUninitializedClassInstance_m1_7950(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Object_t * L_3 = ___ctorCall;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Identity_t1_943 * L_4 = RemotingServices_GetMessageTargetIdentity_m1_9234(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = ((ServerIdentity_t1_70 *)CastclassClass(L_4, ServerIdentity_t1_70_il2cpp_TypeInfo_var));
		ServerIdentity_t1_70 * L_5 = V_1;
		Object_t * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_7 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ServerIdentity_AttachServerObject_m1_9251(L_5, ((MarshalByRefObject_t1_69 *)CastclassClass(L_6, MarshalByRefObject_t1_69_il2cpp_TypeInfo_var)), L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ___ctorCall;
		V_2 = ((ConstructionCall_t1_930 *)IsInstClass(L_8, ConstructionCall_t1_930_il2cpp_TypeInfo_var));
		Object_t * L_9 = ___ctorCall;
		NullCheck(L_9);
		Type_t * L_10 = (Type_t *)InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_10);
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(126 /* System.Boolean System.Type::get_IsContextful() */, L_10);
		if (!L_11)
		{
			goto IL_007b;
		}
	}
	{
		ConstructionCall_t1_930 * L_12 = V_2;
		if (!L_12)
		{
			goto IL_007b;
		}
	}
	{
		ConstructionCall_t1_930 * L_13 = V_2;
		NullCheck(L_13);
		RemotingProxy_t1_932 * L_14 = ConstructionCall_get_SourceProxy_m1_8362(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007b;
		}
	}
	{
		ConstructionCall_t1_930 * L_15 = V_2;
		NullCheck(L_15);
		RemotingProxy_t1_932 * L_16 = ConstructionCall_get_SourceProxy_m1_8362(L_15, /*hidden argument*/NULL);
		ServerIdentity_t1_70 * L_17 = V_1;
		NullCheck(L_16);
		RemotingProxy_AttachIdentity_m1_9018(L_16, L_17, /*hidden argument*/NULL);
		ConstructionCall_t1_930 * L_18 = V_2;
		NullCheck(L_18);
		RemotingProxy_t1_932 * L_19 = ConstructionCall_get_SourceProxy_m1_8362(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Object_t * L_20 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(11 /* System.Object System.Runtime.Remoting.Proxies.RealProxy::GetTransparentProxy() */, L_19);
		V_3 = ((MarshalByRefObject_t1_69 *)CastclassClass(L_20, MarshalByRefObject_t1_69_il2cpp_TypeInfo_var));
		MarshalByRefObject_t1_69 * L_21 = V_3;
		Object_t * L_22 = ___ctorCall;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		RemotingServices_InternalExecuteMessage_m1_9183(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		goto IL_008e;
	}

IL_007b:
	{
		Object_t * L_23 = ___ctorCall;
		NullCheck(L_23);
		MethodBase_t1_335 * L_24 = (MethodBase_t1_335 *)InterfaceFuncInvoker0< MethodBase_t1_335 * >::Invoke(4 /* System.Reflection.MethodBase System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodBase() */, IMethodMessage_t1_948_il2cpp_TypeInfo_var, L_23);
		Object_t * L_25 = V_0;
		Object_t * L_26 = ___ctorCall;
		NullCheck(L_26);
		ObjectU5BU5D_t1_272* L_27 = (ObjectU5BU5D_t1_272*)InterfaceFuncInvoker0< ObjectU5BU5D_t1_272* >::Invoke(1 /* System.Object[] System.Runtime.Remoting.Messaging.IMethodMessage::get_Args() */, IMethodMessage_t1_948_il2cpp_TypeInfo_var, L_26);
		NullCheck(L_24);
		VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t1_272* >::Invoke(43 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_24, L_25, L_27);
	}

IL_008e:
	{
		Object_t * L_28 = V_0;
		Object_t * L_29 = ___ctorCall;
		ConstructionResponse_t1_935 * L_30 = (ConstructionResponse_t1_935 *)il2cpp_codegen_object_new (ConstructionResponse_t1_935_il2cpp_TypeInfo_var);
		ConstructionResponse__ctor_m1_8369(L_30, L_28, (LogicalCallContext_t1_941 *)NULL, L_29, /*hidden argument*/NULL);
		return L_30;
	}
}
// System.Object System.Runtime.Remoting.Activation.ActivationServices::CreateProxyForType(System.Type)
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern "C" Object_t * ActivationServices_CreateProxyForType_m1_7949 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		s_Il2CppMethodIntialized = true;
	}
	ActivatedClientTypeEntry_t1_1008 * V_0 = {0};
	WellKnownClientTypeEntry_t1_1039 * V_1 = {0};
	{
		Type_t * L_0 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		ActivatedClientTypeEntry_t1_1008 * L_1 = RemotingConfiguration_IsRemotelyActivatedClientType_m1_9122(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ActivatedClientTypeEntry_t1_1008 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		ActivatedClientTypeEntry_t1_1008 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_4 = RemotingServices_CreateClientProxy_m1_9212(NULL /*static, unused*/, L_3, (ObjectU5BU5D_t1_272*)(ObjectU5BU5D_t1_272*)NULL, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		Type_t * L_5 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		WellKnownClientTypeEntry_t1_1039 * L_6 = RemotingConfiguration_IsWellKnownClientType_m1_9124(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		WellKnownClientTypeEntry_t1_1039 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0029;
		}
	}
	{
		WellKnownClientTypeEntry_t1_1039 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_9 = RemotingServices_CreateClientProxy_m1_9214(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0029:
	{
		Type_t * L_10 = ___type;
		NullCheck(L_10);
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(126 /* System.Boolean System.Type::get_IsContextful() */, L_10);
		if (!L_11)
		{
			goto IL_003c;
		}
	}
	{
		Type_t * L_12 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_13 = RemotingServices_CreateClientProxyForContextBound_m1_9215(NULL /*static, unused*/, L_12, (ObjectU5BU5D_t1_272*)(ObjectU5BU5D_t1_272*)NULL, /*hidden argument*/NULL);
		return L_13;
	}

IL_003c:
	{
		return NULL;
	}
}
// System.Object System.Runtime.Remoting.Activation.ActivationServices::AllocateUninitializedClassInstance(System.Type)
extern "C" Object_t * ActivationServices_AllocateUninitializedClassInstance_m1_7950 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef Object_t * (*ActivationServices_AllocateUninitializedClassInstance_m1_7950_ftn) (Type_t *);
	return  ((ActivationServices_AllocateUninitializedClassInstance_m1_7950_ftn)mscorlib::System::Runtime::Remoting::Activation::ActivationServices::AllocateUninitializedClassInstance) (___type);
}
// System.Void System.Runtime.Remoting.Activation.ActivationServices::EnableProxyActivation(System.Type,System.Boolean)
extern "C" void ActivationServices_EnableProxyActivation_m1_7951 (Object_t * __this /* static, unused */, Type_t * ___type, bool ___enable, const MethodInfo* method)
{
	using namespace il2cpp::icalls;
	typedef void (*ActivationServices_EnableProxyActivation_m1_7951_ftn) (Type_t *, bool);
	 ((ActivationServices_EnableProxyActivation_m1_7951_ftn)mscorlib::System::Runtime::Remoting::Activation::ActivationServices::EnableProxyActivation) (___type, ___enable);
}
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
extern "C" void AppDomainLevelActivator__ctor_m1_7952 (AppDomainLevelActivator_t1_853 * __this, String_t* ___activationUrl, Object_t * ___next, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___activationUrl;
		__this->____activationUrl_0 = L_0;
		Object_t * L_1 = ___next;
		__this->____next_1 = L_1;
		return;
	}
}
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.AppDomainLevelActivator::get_Level()
extern "C" int32_t AppDomainLevelActivator_get_Level_m1_7953 (AppDomainLevelActivator_t1_853 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.AppDomainLevelActivator::get_NextActivator()
extern "C" Object_t * AppDomainLevelActivator_get_NextActivator_m1_7954 (AppDomainLevelActivator_t1_853 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->____next_1);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void AppDomainLevelActivator_set_NextActivator_m1_7955 (AppDomainLevelActivator_t1_853 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->____next_1 = L_0;
		return;
	}
}
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.AppDomainLevelActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern const Il2CppType* IActivator_t1_851_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* IActivator_t1_851_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructionResponse_t1_935_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var;
extern TypeInfo* ObjRef_t1_923_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2132;
extern "C" Object_t * AppDomainLevelActivator_Activate_m1_7956 (AppDomainLevelActivator_t1_853 * __this, Object_t * ___ctorCall, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IActivator_t1_851_0_0_0_var = il2cpp_codegen_type_from_index(605);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		IActivator_t1_851_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		ConstructionResponse_t1_935_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(616);
		IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		ObjRef_t1_923_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(617);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral2132 = il2cpp_codegen_string_literal_from_index(2132);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Exception_t1_33 * V_2 = {0};
	ObjRef_t1_923 * V_3 = {0};
	Object_t * V_4 = {0};
	Identity_t1_943 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IActivator_t1_851_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_1 = (__this->____activationUrl_0);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Object_t * L_2 = RemotingServices_Connect_m1_9185(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_1 = ((Object_t *)Castclass(L_2, IActivator_t1_851_il2cpp_TypeInfo_var));
		Object_t * L_3 = ___ctorCall;
		Object_t * L_4 = ___ctorCall;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IConstructionCallMessage::get_Activator() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(1 /* System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.IActivator::get_NextActivator() */, IActivator_t1_851_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_3);
		InterfaceActionInvoker1< Object_t * >::Invoke(3 /* System.Void System.Runtime.Remoting.Activation.IConstructionCallMessage::set_Activator(System.Runtime.Remoting.Activation.IActivator) */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_3, L_6);
	}

IL_002c:
	try
	{ // begin try (depth: 1)
		Object_t * L_7 = V_1;
		Object_t * L_8 = ___ctorCall;
		NullCheck(L_7);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(3 /* System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.IActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IActivator_t1_851_il2cpp_TypeInfo_var, L_7, L_8);
		V_0 = L_9;
		goto IL_004d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0039;
		throw e;
	}

CATCH_0039:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1_33 *)__exception_local);
			Exception_t1_33 * L_10 = V_2;
			Object_t * L_11 = ___ctorCall;
			ConstructionResponse_t1_935 * L_12 = (ConstructionResponse_t1_935 *)il2cpp_codegen_object_new (ConstructionResponse_t1_935_il2cpp_TypeInfo_var);
			ConstructionResponse__ctor_m1_8370(L_12, L_10, L_11, /*hidden argument*/NULL);
			V_6 = L_12;
			goto IL_0089;
		}

IL_0048:
		{
			; // IL_0048: leave IL_004d
		}
	} // end catch (depth: 1)

IL_004d:
	{
		Object_t * L_13 = V_0;
		NullCheck(L_13);
		Object_t * L_14 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(3 /* System.Object System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_ReturnValue() */, IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var, L_13);
		V_3 = ((ObjRef_t1_923 *)CastclassClass(L_14, ObjRef_t1_923_il2cpp_TypeInfo_var));
		ObjRef_t1_923 * L_15 = V_3;
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Runtime.Remoting.ObjRef::get_URI() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Identity_t1_943 * L_17 = RemotingServices_GetIdentityForUri_m1_9216(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0074;
		}
	}
	{
		RemotingException_t1_1025 * L_18 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_18, _stringLiteral2132, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_0074:
	{
		ObjRef_t1_923 * L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		ClientIdentity_t1_1013 * L_20 = RemotingServices_GetOrCreateClientIdentity_m1_9219(NULL /*static, unused*/, L_19, (Type_t *)NULL, (&V_4), /*hidden argument*/NULL);
		V_5 = L_20;
		Object_t * L_21 = ___ctorCall;
		Identity_t1_943 * L_22 = V_5;
		RemotingServices_SetMessageTargetIdentity_m1_9235(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Object_t * L_23 = V_0;
		return L_23;
	}

IL_0089:
	{
		Object_t * L_24 = V_6;
		return L_24;
	}
}
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern "C" void ConstructionLevelActivator__ctor_m1_7957 (ConstructionLevelActivator_t1_854 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.ConstructionLevelActivator::get_Level()
extern "C" int32_t ConstructionLevelActivator_get_Level_m1_7958 (ConstructionLevelActivator_t1_854 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(4);
	}
}
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ConstructionLevelActivator::get_NextActivator()
extern "C" Object_t * ConstructionLevelActivator_get_NextActivator_m1_7959 (ConstructionLevelActivator_t1_854 * __this, const MethodInfo* method)
{
	{
		return (Object_t *)NULL;
	}
}
// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ConstructionLevelActivator_set_NextActivator_m1_7960 (ConstructionLevelActivator_t1_854 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.ConstructionLevelActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var;
extern "C" Object_t * ConstructionLevelActivator_Activate_m1_7961 (ConstructionLevelActivator_t1_854 * __this, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(602);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_0 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_1 = Context_GetServerContextSinkChain_m1_8148(L_0, /*hidden argument*/NULL);
		Object_t * L_2 = ___msg;
		NullCheck(L_1);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.IMessageSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_1, L_2);
		return ((Object_t *)Castclass(L_3, IConstructionReturnMessage_t1_1703_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator__ctor_m1_7962 (ContextLevelActivator_t1_855 * __this, Object_t * ___next, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___next;
		__this->___m_NextActivator_0 = L_0;
		return;
	}
}
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.ContextLevelActivator::get_Level()
extern "C" int32_t ContextLevelActivator_get_Level_m1_7963 (ContextLevelActivator_t1_855 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(8);
	}
}
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ContextLevelActivator::get_NextActivator()
extern "C" Object_t * ContextLevelActivator_get_NextActivator_m1_7964 (ContextLevelActivator_t1_855 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___m_NextActivator_0);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void ContextLevelActivator_set_NextActivator_m1_7965 (ContextLevelActivator_t1_855 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___m_NextActivator_0 = L_0;
		return;
	}
}
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.ContextLevelActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructionCall_t1_930_il2cpp_TypeInfo_var;
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* IActivator_t1_851_il2cpp_TypeInfo_var;
extern "C" Object_t * ContextLevelActivator_Activate_m1_7966 (ContextLevelActivator_t1_855 * __this, Object_t * ___ctorCall, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		ConstructionCall_t1_930_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		IActivator_t1_851_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		s_Il2CppMethodIntialized = true;
	}
	ServerIdentity_t1_70 * V_0 = {0};
	ConstructionCall_t1_930 * V_1 = {0};
	Context_t1_891 * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___ctorCall;
		NullCheck(L_0);
		Type_t * L_1 = (Type_t *)InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		ClientActivatedIdentity_t1_1031 * L_2 = RemotingServices_CreateContextBoundObjectIdentity_m1_9221(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Object_t * L_3 = ___ctorCall;
		ServerIdentity_t1_70 * L_4 = V_0;
		RemotingServices_SetMessageTargetIdentity_m1_9235(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Object_t * L_5 = ___ctorCall;
		V_1 = ((ConstructionCall_t1_930 *)IsInstClass(L_5, ConstructionCall_t1_930_il2cpp_TypeInfo_var));
		ConstructionCall_t1_930 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		ConstructionCall_t1_930 * L_7 = V_1;
		NullCheck(L_7);
		bool L_8 = ConstructionCall_get_IsContextOk_m1_8350(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0062;
		}
	}

IL_002b:
	{
		ServerIdentity_t1_70 * L_9 = V_0;
		Object_t * L_10 = ___ctorCall;
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Context_t1_891 * L_11 = Context_CreateNewContext_m1_8153(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		ServerIdentity_set_Context_m1_9254(L_9, L_11, /*hidden argument*/NULL);
		ServerIdentity_t1_70 * L_12 = V_0;
		NullCheck(L_12);
		Context_t1_891 * L_13 = ServerIdentity_get_Context_m1_9253(L_12, /*hidden argument*/NULL);
		Context_t1_891 * L_14 = Context_SwitchToContext_m1_8152(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_15 = (__this->___m_NextActivator_0);
			Object_t * L_16 = ___ctorCall;
			NullCheck(L_15);
			Object_t * L_17 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(3 /* System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.IActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IActivator_t1_851_il2cpp_TypeInfo_var, L_15, L_16);
			V_3 = L_17;
			IL2CPP_LEAVE(0x6F, FINALLY_005a);
		}

IL_0055:
		{
			; // IL_0055: leave IL_0062
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_005a;
	}

FINALLY_005a:
	{ // begin finally (depth: 1)
		Context_t1_891 * L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Context_SwitchToContext_m1_8152(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(90)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(90)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0062:
	{
		Object_t * L_19 = (__this->___m_NextActivator_0);
		Object_t * L_20 = ___ctorCall;
		NullCheck(L_19);
		Object_t * L_21 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(3 /* System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.IActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage) */, IActivator_t1_851_il2cpp_TypeInfo_var, L_19, L_20);
		return L_21;
	}

IL_006f:
	{
		Object_t * L_22 = V_3;
		return L_22;
	}
}
// System.Void System.Runtime.Remoting.Activation.RemoteActivationAttribute::.ctor()
extern "C" void RemoteActivationAttribute__ctor_m1_7967 (RemoteActivationAttribute_t1_856 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Activation.RemoteActivationAttribute::.ctor(System.Collections.IList)
extern "C" void RemoteActivationAttribute__ctor_m1_7968 (RemoteActivationAttribute_t1_856 * __this, Object_t * ___contextProperties, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___contextProperties;
		__this->____contextProperties_0 = L_0;
		return;
	}
}
// System.Boolean System.Runtime.Remoting.Activation.RemoteActivationAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" bool RemoteActivationAttribute_IsContextOK_m1_7969 (RemoteActivationAttribute_t1_856 * __this, Context_t1_891 * ___ctx, Object_t * ___ctor, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Runtime.Remoting.Activation.RemoteActivationAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void RemoteActivationAttribute_GetPropertiesForNewContext_m1_7970 (RemoteActivationAttribute_t1_856 * __this, Object_t * ___ctor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->____contextProperties_0);
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		Object_t * L_1 = (__this->____contextProperties_0);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_001c:
		{
			Object_t * L_3 = V_1;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_3);
			V_0 = L_4;
			Object_t * L_5 = ___ctor;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(5 /* System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_5);
			Object_t * L_7 = V_0;
			NullCheck(L_6);
			InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1_262_il2cpp_TypeInfo_var, L_6, L_7);
		}

IL_0030:
		{
			Object_t * L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001c;
			}
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x52, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			Object_t * L_10 = V_1;
			V_2 = ((Object_t *)IsInst(L_10, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_11 = V_2;
			if (L_11)
			{
				goto IL_004b;
			}
		}

IL_004a:
		{
			IL2CPP_END_FINALLY(64)
		}

IL_004b:
		{
			Object_t * L_12 = V_2;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0052:
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Activation.RemoteActivator::.ctor()
extern "C" void RemoteActivator__ctor_m1_7971 (RemoteActivator_t1_857 * __this, const MethodInfo* method)
{
	{
		MarshalByRefObject__ctor_m1_1370(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.RemoteActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* RemoteActivationAttribute_t1_856_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodMessage_t1_948_il2cpp_TypeInfo_var;
extern TypeInfo* MarshalByRefObject_t1_69_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructionResponse_t1_935_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2133;
extern Il2CppCodeGenString* _stringLiteral2134;
extern "C" Object_t * RemoteActivator_Activate_m1_7972 (RemoteActivator_t1_857 * __this, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		RemoteActivationAttribute_t1_856_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(619);
		IMethodMessage_t1_948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		MarshalByRefObject_t1_69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		ConstructionResponse_t1_935_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(616);
		_stringLiteral2133 = il2cpp_codegen_string_literal_from_index(2133);
		_stringLiteral2134 = il2cpp_codegen_string_literal_from_index(2134);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_272* V_0 = {0};
	MarshalByRefObject_t1_69 * V_1 = {0};
	ObjRef_t1_923 * V_2 = {0};
	{
		Object_t * L_0 = ___msg;
		NullCheck(L_0);
		Type_t * L_1 = (Type_t *)InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		bool L_2 = RemotingConfiguration_IsActivationAllowed_m1_9121(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		Object_t * L_3 = ___msg;
		NullCheck(L_3);
		String_t* L_4 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationTypeName() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral2133, L_4, _stringLiteral2134, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_6 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_6, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_002b:
	{
		ObjectU5BU5D_t1_272* L_7 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		Object_t * L_8 = ___msg;
		NullCheck(L_8);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(5 /* System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_8);
		RemoteActivationAttribute_t1_856 * L_10 = (RemoteActivationAttribute_t1_856 *)il2cpp_codegen_object_new (RemoteActivationAttribute_t1_856_il2cpp_TypeInfo_var);
		RemoteActivationAttribute__ctor_m1_7968(L_10, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 0, sizeof(Object_t *))) = (Object_t *)L_10;
		V_0 = L_7;
		Object_t * L_11 = ___msg;
		NullCheck(L_11);
		Type_t * L_12 = (Type_t *)InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_11);
		Object_t * L_13 = ___msg;
		NullCheck(L_13);
		ObjectU5BU5D_t1_272* L_14 = (ObjectU5BU5D_t1_272*)InterfaceFuncInvoker0< ObjectU5BU5D_t1_272* >::Invoke(1 /* System.Object[] System.Runtime.Remoting.Messaging.IMethodMessage::get_Args() */, IMethodMessage_t1_948_il2cpp_TypeInfo_var, L_13);
		ObjectU5BU5D_t1_272* L_15 = V_0;
		Object_t * L_16 = Activator_CreateInstance_m1_13036(NULL /*static, unused*/, L_12, L_14, L_15, /*hidden argument*/NULL);
		V_1 = ((MarshalByRefObject_t1_69 *)CastclassClass(L_16, MarshalByRefObject_t1_69_il2cpp_TypeInfo_var));
		MarshalByRefObject_t1_69 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		ObjRef_t1_923 * L_18 = RemotingServices_Marshal_m1_9192(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		ObjRef_t1_923 * L_19 = V_2;
		Object_t * L_20 = ___msg;
		ConstructionResponse_t1_935 * L_21 = (ConstructionResponse_t1_935 *)il2cpp_codegen_object_new (ConstructionResponse_t1_935_il2cpp_TypeInfo_var);
		ConstructionResponse__ctor_m1_8369(L_21, L_19, (LogicalCallContext_t1_941 *)NULL, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Object System.Runtime.Remoting.Activation.RemoteActivator::InitializeLifetimeService()
extern TypeInfo* ILease_t1_907_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" Object_t * RemoteActivator_InitializeLifetimeService_m1_7973 (RemoteActivator_t1_857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILease_t1_907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(620);
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = MarshalByRefObject_InitializeLifetimeService_m1_1376(__this, /*hidden argument*/NULL);
		V_0 = ((Object_t *)Castclass(L_0, ILease_t1_907_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Runtime.Remoting.Lifetime.LeaseState System.Runtime.Remoting.Lifetime.ILease::get_CurrentState() */, ILease_t1_907_il2cpp_TypeInfo_var, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0054;
		}
	}
	{
		Object_t * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_4 = TimeSpan_FromMinutes_m1_14645(NULL /*static, unused*/, (30.0), /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceActionInvoker1< TimeSpan_t1_368  >::Invoke(3 /* System.Void System.Runtime.Remoting.Lifetime.ILease::set_InitialLeaseTime(System.TimeSpan) */, ILease_t1_907_il2cpp_TypeInfo_var, L_3, L_4);
		Object_t * L_5 = V_0;
		TimeSpan_t1_368  L_6 = TimeSpan_FromMinutes_m1_14645(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceActionInvoker1< TimeSpan_t1_368  >::Invoke(7 /* System.Void System.Runtime.Remoting.Lifetime.ILease::set_SponsorshipTimeout(System.TimeSpan) */, ILease_t1_907_il2cpp_TypeInfo_var, L_5, L_6);
		Object_t * L_7 = V_0;
		TimeSpan_t1_368  L_8 = TimeSpan_FromMinutes_m1_14645(NULL /*static, unused*/, (10.0), /*hidden argument*/NULL);
		NullCheck(L_7);
		InterfaceActionInvoker1< TimeSpan_t1_368  >::Invoke(5 /* System.Void System.Runtime.Remoting.Lifetime.ILease::set_RenewOnCallTime(System.TimeSpan) */, ILease_t1_907_il2cpp_TypeInfo_var, L_7, L_8);
	}

IL_0054:
	{
		Object_t * L_9 = V_0;
		return L_9;
	}
}
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.RemoteActivator::get_Level()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" int32_t RemoteActivator_get_Level_m1_7974 (RemoteActivator_t1_857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.RemoteActivator::get_NextActivator()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" Object_t * RemoteActivator_get_NextActivator_m1_7975 (RemoteActivator_t1_857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Activation.RemoteActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void RemoteActivator_set_NextActivator_m1_7976 (RemoteActivator_t1_857 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Activation.UrlAttribute::.ctor(System.String)
extern "C" void UrlAttribute__ctor_m1_7977 (UrlAttribute_t1_858 * __this, String_t* ___callsiteURL, const MethodInfo* method)
{
	{
		String_t* L_0 = ___callsiteURL;
		ContextAttribute__ctor_m1_8170(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___callsiteURL;
		__this->___url_1 = L_1;
		return;
	}
}
// System.String System.Runtime.Remoting.Activation.UrlAttribute::get_UrlValue()
extern "C" String_t* UrlAttribute_get_UrlValue_m1_7978 (UrlAttribute_t1_858 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___url_1);
		return L_0;
	}
}
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::Equals(System.Object)
extern TypeInfo* UrlAttribute_t1_858_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool UrlAttribute_Equals_m1_7979 (UrlAttribute_t1_858 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UrlAttribute_t1_858_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		if (((UrlAttribute_t1_858 *)IsInstSealed(L_0, UrlAttribute_t1_858_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___o;
		NullCheck(((UrlAttribute_t1_858 *)CastclassSealed(L_1, UrlAttribute_t1_858_il2cpp_TypeInfo_var)));
		String_t* L_2 = UrlAttribute_get_UrlValue_m1_7978(((UrlAttribute_t1_858 *)CastclassSealed(L_1, UrlAttribute_t1_858_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		String_t* L_3 = (__this->___url_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 System.Runtime.Remoting.Activation.UrlAttribute::GetHashCode()
extern "C" int32_t UrlAttribute_GetHashCode_m1_7980 (UrlAttribute_t1_858 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___url_1);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m1_577(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Runtime.Remoting.Activation.UrlAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" void UrlAttribute_GetPropertiesForNewContext_m1_7981 (UrlAttribute_t1_858 * __this, Object_t * ___ctorMsg, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean System.Runtime.Remoting.Activation.UrlAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" bool UrlAttribute_IsContextOK_m1_7982 (UrlAttribute_t1_858 * __this, Context_t1_891 * ___ctx, Object_t * ___msg, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::.ctor(System.Collections.IDictionary[])
extern "C" void AggregateDictionary__ctor_m1_7983 (AggregateDictionary_t1_860 * __this, IDictionaryU5BU5D_t1_861* ___dics, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IDictionaryU5BU5D_t1_861* L_0 = ___dics;
		__this->___dictionaries_0 = L_0;
		return;
	}
}
// System.Collections.IEnumerator System.Runtime.Remoting.Channels.AggregateDictionary::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* AggregateEnumerator_t1_862_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateDictionary_System_Collections_IEnumerable_GetEnumerator_m1_7984 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AggregateEnumerator_t1_862_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	{
		IDictionaryU5BU5D_t1_861* L_0 = (__this->___dictionaries_0);
		AggregateEnumerator_t1_862 * L_1 = (AggregateEnumerator_t1_862 *)il2cpp_codegen_object_new (AggregateEnumerator_t1_862_il2cpp_TypeInfo_var);
		AggregateEnumerator__ctor_m1_8000(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::get_IsFixedSize()
extern "C" bool AggregateDictionary_get_IsFixedSize_m1_7985 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::get_IsReadOnly()
extern "C" bool AggregateDictionary_get_IsReadOnly_m1_7986 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Object System.Runtime.Remoting.Channels.AggregateDictionary::get_Item(System.Object)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateDictionary_get_Item_m1_7987 (AggregateDictionary_t1_860 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	IDictionaryU5BU5D_t1_861* V_1 = {0};
	int32_t V_2 = 0;
	{
		IDictionaryU5BU5D_t1_861* L_0 = (__this->___dictionaries_0);
		V_1 = L_0;
		V_2 = 0;
		goto IL_002a;
	}

IL_000e:
	{
		IDictionaryU5BU5D_t1_861* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_3, sizeof(Object_t *)));
		Object_t * L_4 = V_0;
		Object_t * L_5 = ___key;
		NullCheck(L_4);
		bool L_6 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, L_5);
		if (!L_6)
		{
			goto IL_0026;
		}
	}
	{
		Object_t * L_7 = V_0;
		Object_t * L_8 = ___key;
		NullCheck(L_7);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_7, L_8);
		return L_9;
	}

IL_0026:
	{
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_11 = V_2;
		IDictionaryU5BU5D_t1_861* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::set_Item(System.Object,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void AggregateDictionary_set_Item_m1_7988 (AggregateDictionary_t1_860 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.ICollection System.Runtime.Remoting.Channels.AggregateDictionary::get_Keys()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateDictionary_get_Keys_m1_7989 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	IDictionaryU5BU5D_t1_861* V_1 = {0};
	int32_t V_2 = 0;
	{
		ArrayList_t1_170 * L_0 = (__this->____keys_2);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArrayList_t1_170 * L_1 = (__this->____keys_2);
		return L_1;
	}

IL_0012:
	{
		ArrayList_t1_170 * L_2 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_2, /*hidden argument*/NULL);
		__this->____keys_2 = L_2;
		IDictionaryU5BU5D_t1_861* L_3 = (__this->___dictionaries_0);
		V_1 = L_3;
		V_2 = 0;
		goto IL_0044;
	}

IL_002b:
	{
		IDictionaryU5BU5D_t1_861* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_0 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_4, L_6, sizeof(Object_t *)));
		ArrayList_t1_170 * L_7 = (__this->____keys_2);
		Object_t * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_7);
		VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_7, L_9);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_11 = V_2;
		IDictionaryU5BU5D_t1_861* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		ArrayList_t1_170 * L_13 = (__this->____keys_2);
		return L_13;
	}
}
// System.Collections.ICollection System.Runtime.Remoting.Channels.AggregateDictionary::get_Values()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateDictionary_get_Values_m1_7990 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	IDictionaryU5BU5D_t1_861* V_1 = {0};
	int32_t V_2 = 0;
	{
		ArrayList_t1_170 * L_0 = (__this->____values_1);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArrayList_t1_170 * L_1 = (__this->____values_1);
		return L_1;
	}

IL_0012:
	{
		ArrayList_t1_170 * L_2 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_2, /*hidden argument*/NULL);
		__this->____values_1 = L_2;
		IDictionaryU5BU5D_t1_861* L_3 = (__this->___dictionaries_0);
		V_1 = L_3;
		V_2 = 0;
		goto IL_0044;
	}

IL_002b:
	{
		IDictionaryU5BU5D_t1_861* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_0 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_4, L_6, sizeof(Object_t *)));
		ArrayList_t1_170 * L_7 = (__this->____values_1);
		Object_t * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(5 /* System.Collections.ICollection System.Collections.IDictionary::get_Values() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_7);
		VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_7, L_9);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_11 = V_2;
		IDictionaryU5BU5D_t1_861* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		ArrayList_t1_170 * L_13 = (__this->____values_1);
		return L_13;
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::Add(System.Object,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void AggregateDictionary_Add_m1_7991 (AggregateDictionary_t1_860 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void AggregateDictionary_Clear_m1_7992 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::Contains(System.Object)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" bool AggregateDictionary_Contains_m1_7993 (AggregateDictionary_t1_860 * __this, Object_t * ___ob, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	IDictionaryU5BU5D_t1_861* V_1 = {0};
	int32_t V_2 = 0;
	{
		IDictionaryU5BU5D_t1_861* L_0 = (__this->___dictionaries_0);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0024;
	}

IL_000e:
	{
		IDictionaryU5BU5D_t1_861* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_3, sizeof(Object_t *)));
		Object_t * L_4 = V_0;
		Object_t * L_5 = ___ob;
		NullCheck(L_4);
		bool L_6 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_4, L_5);
		if (!L_6)
		{
			goto IL_0020;
		}
	}
	{
		return 1;
	}

IL_0020:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_8 = V_2;
		IDictionaryU5BU5D_t1_861* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}
}
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Channels.AggregateDictionary::GetEnumerator()
extern TypeInfo* AggregateEnumerator_t1_862_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateDictionary_GetEnumerator_m1_7994 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AggregateEnumerator_t1_862_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		s_Il2CppMethodIntialized = true;
	}
	{
		IDictionaryU5BU5D_t1_861* L_0 = (__this->___dictionaries_0);
		AggregateEnumerator_t1_862 * L_1 = (AggregateEnumerator_t1_862 *)il2cpp_codegen_object_new (AggregateEnumerator_t1_862_il2cpp_TypeInfo_var);
		AggregateEnumerator__ctor_m1_8000(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::Remove(System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void AggregateDictionary_Remove_m1_7995 (AggregateDictionary_t1_860 * __this, Object_t * ___ob, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateDictionary::CopyTo(System.Array,System.Int32)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void AggregateDictionary_CopyTo_m1_7996 (AggregateDictionary_t1_860 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(18 /* System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Channels.AggregateDictionary::GetEnumerator() */, __this);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_000c:
		{
			Object_t * L_1 = V_1;
			NullCheck(L_1);
			Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_1);
			V_0 = L_2;
			Array_t * L_3 = ___array;
			Object_t * L_4 = V_0;
			int32_t L_5 = ___index;
			int32_t L_6 = L_5;
			___index = ((int32_t)((int32_t)L_6+(int32_t)1));
			NullCheck(L_3);
			Array_SetValue_m1_1020(L_3, L_4, L_6, /*hidden argument*/NULL);
		}

IL_0020:
		{
			Object_t * L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_000c;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x42, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		{
			Object_t * L_9 = V_1;
			V_2 = ((Object_t *)IsInst(L_9, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_10 = V_2;
			if (L_10)
			{
				goto IL_003b;
			}
		}

IL_003a:
		{
			IL2CPP_END_FINALLY(48)
		}

IL_003b:
		{
			Object_t * L_11 = V_2;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_11);
			IL2CPP_END_FINALLY(48)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Int32 System.Runtime.Remoting.Channels.AggregateDictionary::get_Count()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" int32_t AggregateDictionary_get_Count_m1_7997 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	IDictionaryU5BU5D_t1_861* V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = 0;
		IDictionaryU5BU5D_t1_861* L_0 = (__this->___dictionaries_0);
		V_2 = L_0;
		V_3 = 0;
		goto IL_0021;
	}

IL_0010:
	{
		IDictionaryU5BU5D_t1_861* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_3, sizeof(Object_t *)));
		int32_t L_4 = V_0;
		Object_t * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_5);
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)L_6));
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_8 = V_3;
		IDictionaryU5BU5D_t1_861* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.AggregateDictionary::get_IsSynchronized()
extern "C" bool AggregateDictionary_get_IsSynchronized_m1_7998 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Runtime.Remoting.Channels.AggregateDictionary::get_SyncRoot()
extern "C" Object_t * AggregateDictionary_get_SyncRoot_m1_7999 (AggregateDictionary_t1_860 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateEnumerator::.ctor(System.Collections.IDictionary[])
extern "C" void AggregateEnumerator__ctor_m1_8000 (AggregateEnumerator_t1_862 * __this, IDictionaryU5BU5D_t1_861* ___dics, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IDictionaryU5BU5D_t1_861* L_0 = ___dics;
		__this->___dictionaries_0 = L_0;
		VirtActionInvoker0::Invoke(6 /* System.Void System.Runtime.Remoting.Channels.AggregateEnumerator::Reset() */, __this);
		return;
	}
}
// System.Collections.DictionaryEntry System.Runtime.Remoting.Channels.AggregateEnumerator::get_Entry()
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t1_284  AggregateEnumerator_get_Entry_m1_8001 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___currente_2);
		NullCheck(L_0);
		DictionaryEntry_t1_284  L_1 = (DictionaryEntry_t1_284 )InterfaceFuncInvoker0< DictionaryEntry_t1_284  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object System.Runtime.Remoting.Channels.AggregateEnumerator::get_Key()
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateEnumerator_get_Key_m1_8002 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___currente_2);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object System.Runtime.Remoting.Channels.AggregateEnumerator::get_Value()
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateEnumerator_get_Value_m1_8003 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___currente_2);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object System.Runtime.Remoting.Channels.AggregateEnumerator::get_Current()
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern "C" Object_t * AggregateEnumerator_get_Current_m1_8004 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___currente_2);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.AggregateEnumerator::MoveNext()
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" bool AggregateEnumerator_MoveNext_m1_8005 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___pos_1);
		IDictionaryU5BU5D_t1_861* L_1 = (__this->___dictionaries_0);
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}

IL_0015:
	{
		Object_t * L_2 = (__this->___currente_2);
		NullCheck(L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_2);
		if (L_3)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_4 = (__this->___pos_1);
		__this->___pos_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = (__this->___pos_1);
		IDictionaryU5BU5D_t1_861* L_6 = (__this->___dictionaries_0);
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length)))))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		IDictionaryU5BU5D_t1_861* L_7 = (__this->___dictionaries_0);
		int32_t L_8 = (__this->___pos_1);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck((*(Object_t **)(Object_t **)SZArrayLdElema(L_7, L_9, sizeof(Object_t *))));
		Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(9 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t1_35_il2cpp_TypeInfo_var, (*(Object_t **)(Object_t **)SZArrayLdElema(L_7, L_9, sizeof(Object_t *))));
		__this->___currente_2 = L_10;
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.Runtime.Remoting.Channels.AggregateEnumerator::MoveNext() */, __this);
		return L_11;
	}

IL_0067:
	{
		return 1;
	}
}
// System.Void System.Runtime.Remoting.Channels.AggregateEnumerator::Reset()
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void AggregateEnumerator_Reset_m1_8006 (AggregateEnumerator_t1_862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___pos_1 = 0;
		IDictionaryU5BU5D_t1_861* L_0 = (__this->___dictionaries_0);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		IDictionaryU5BU5D_t1_861* L_1 = (__this->___dictionaries_0);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		NullCheck((*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_2, sizeof(Object_t *))));
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(9 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t1_35_il2cpp_TypeInfo_var, (*(Object_t **)(Object_t **)SZArrayLdElema(L_1, L_2, sizeof(Object_t *))));
		__this->___currente_2 = L_3;
	}

IL_0028:
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::.ctor()
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void BaseChannelObjectWithProperties__ctor_m1_8007 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		__this->___table_0 = L_0;
		return;
	}
}
// System.Collections.IEnumerator System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * BaseChannelObjectWithProperties_System_Collections_IEnumerable_GetEnumerator_m1_8008 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___table_0);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(36 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Int32 System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Count()
extern "C" int32_t BaseChannelObjectWithProperties_get_Count_m1_8009 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___table_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.Hashtable::get_Count() */, L_0);
		return L_1;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_IsFixedSize()
extern "C" bool BaseChannelObjectWithProperties_get_IsFixedSize_m1_8010 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_IsReadOnly()
extern "C" bool BaseChannelObjectWithProperties_get_IsReadOnly_m1_8011 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_IsSynchronized()
extern "C" bool BaseChannelObjectWithProperties_get_IsSynchronized_m1_8012 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Item(System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" Object_t * BaseChannelObjectWithProperties_get_Item_m1_8013 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::set_Item(System.Object,System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void BaseChannelObjectWithProperties_set_Item_m1_8014 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.ICollection System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Keys()
extern "C" Object_t * BaseChannelObjectWithProperties_get_Keys_m1_8015 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___table_0);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(28 /* System.Collections.ICollection System.Collections.Hashtable::get_Keys() */, L_0);
		return L_1;
	}
}
// System.Collections.IDictionary System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Properties()
extern "C" Object_t * BaseChannelObjectWithProperties_get_Properties_m1_8016 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_SyncRoot()
extern "C" Object_t * BaseChannelObjectWithProperties_get_SyncRoot_m1_8017 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Collections.ICollection System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::get_Values()
extern "C" Object_t * BaseChannelObjectWithProperties_get_Values_m1_8018 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___table_0);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Collections.ICollection System.Collections.Hashtable::get_Values() */, L_0);
		return L_1;
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Add(System.Object,System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void BaseChannelObjectWithProperties_Add_m1_8019 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void BaseChannelObjectWithProperties_Clear_m1_8020 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Contains(System.Object)
extern "C" bool BaseChannelObjectWithProperties_Contains_m1_8021 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___table_0);
		Object_t * L_1 = ___key;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(35 /* System.Boolean System.Collections.Hashtable::Contains(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::CopyTo(System.Array,System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void BaseChannelObjectWithProperties_CopyTo_m1_8022 (BaseChannelObjectWithProperties_t1_864 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::GetEnumerator()
extern "C" Object_t * BaseChannelObjectWithProperties_GetEnumerator_m1_8023 (BaseChannelObjectWithProperties_t1_864 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___table_0);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(36 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::Remove(System.Object)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void BaseChannelObjectWithProperties_Remove_m1_8024 (BaseChannelObjectWithProperties_t1_864 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelSinkWithProperties::.ctor()
extern "C" void BaseChannelSinkWithProperties__ctor_m1_8025 (BaseChannelSinkWithProperties_t1_865 * __this, const MethodInfo* method)
{
	{
		BaseChannelObjectWithProperties__ctor_m1_8007(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.BaseChannelWithProperties::.ctor()
extern "C" void BaseChannelWithProperties__ctor_m1_8026 (BaseChannelWithProperties_t1_866 * __this, const MethodInfo* method)
{
	{
		BaseChannelObjectWithProperties__ctor_m1_8007(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IDictionary System.Runtime.Remoting.Channels.BaseChannelWithProperties::get_Properties()
extern TypeInfo* IChannelSinkBase_t1_867_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionaryU5BU5D_t1_861_il2cpp_TypeInfo_var;
extern TypeInfo* AggregateDictionary_t1_860_il2cpp_TypeInfo_var;
extern "C" Object_t * BaseChannelWithProperties_get_Properties_m1_8027 (BaseChannelWithProperties_t1_866 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChannelSinkBase_t1_867_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		IDictionaryU5BU5D_t1_861_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		AggregateDictionary_t1_860_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		s_Il2CppMethodIntialized = true;
	}
	IDictionaryU5BU5D_t1_861* V_0 = {0};
	{
		Object_t * L_0 = (__this->___SinksWithProperties_1);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		Object_t * L_1 = (__this->___SinksWithProperties_1);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IDictionary System.Runtime.Remoting.Channels.IChannelSinkBase::get_Properties() */, IChannelSinkBase_t1_867_il2cpp_TypeInfo_var, L_1);
		if (L_2)
		{
			goto IL_0022;
		}
	}

IL_001b:
	{
		Object_t * L_3 = BaseChannelObjectWithProperties_get_Properties_m1_8016(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0022:
	{
		IDictionaryU5BU5D_t1_861* L_4 = ((IDictionaryU5BU5D_t1_861*)SZArrayNew(IDictionaryU5BU5D_t1_861_il2cpp_TypeInfo_var, 2));
		Object_t * L_5 = BaseChannelObjectWithProperties_get_Properties_m1_8016(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		IDictionaryU5BU5D_t1_861* L_6 = L_4;
		Object_t * L_7 = (__this->___SinksWithProperties_1);
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IDictionary System.Runtime.Remoting.Channels.IChannelSinkBase::get_Properties() */, IChannelSinkBase_t1_867_il2cpp_TypeInfo_var, L_7);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_8;
		V_0 = L_6;
		IDictionaryU5BU5D_t1_861* L_9 = V_0;
		AggregateDictionary_t1_860 * L_10 = (AggregateDictionary_t1_860 *)il2cpp_codegen_object_new (AggregateDictionary_t1_860_il2cpp_TypeInfo_var);
		AggregateDictionary__ctor_m1_7983(L_10, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelDataStore::.ctor(System.String[])
extern "C" void ChannelDataStore__ctor_m1_8028 (ChannelDataStore_t1_868 * __this, StringU5BU5D_t1_238* ___channelURIs, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_0 = ___channelURIs;
		__this->____channelURIs_0 = L_0;
		return;
	}
}
// System.String[] System.Runtime.Remoting.Channels.ChannelDataStore::get_ChannelUris()
extern "C" StringU5BU5D_t1_238* ChannelDataStore_get_ChannelUris_m1_8029 (ChannelDataStore_t1_868 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->____channelURIs_0);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelDataStore::set_ChannelUris(System.String[])
extern "C" void ChannelDataStore_set_ChannelUris_m1_8030 (ChannelDataStore_t1_868 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = ___value;
		__this->____channelURIs_0 = L_0;
		return;
	}
}
// System.Object System.Runtime.Remoting.Channels.ChannelDataStore::get_Item(System.Object)
extern "C" Object_t * ChannelDataStore_get_Item_m1_8031 (ChannelDataStore_t1_868 * __this, Object_t * ___key, const MethodInfo* method)
{
	DictionaryEntry_t1_284  V_0 = {0};
	DictionaryEntryU5BU5D_t1_869* V_1 = {0};
	int32_t V_2 = 0;
	{
		DictionaryEntryU5BU5D_t1_869* L_0 = (__this->____extraData_1);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return NULL;
	}

IL_000d:
	{
		DictionaryEntryU5BU5D_t1_869* L_1 = (__this->____extraData_1);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0046;
	}

IL_001b:
	{
		DictionaryEntryU5BU5D_t1_869* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		V_0 = (*(DictionaryEntry_t1_284 *)((DictionaryEntry_t1_284 *)(DictionaryEntry_t1_284 *)SZArrayLdElema(L_2, L_3, sizeof(DictionaryEntry_t1_284 ))));
		Object_t * L_4 = DictionaryEntry_get_Key_m1_3229((&V_0), /*hidden argument*/NULL);
		Object_t * L_5 = ___key;
		NullCheck(L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Object_t * L_7 = DictionaryEntry_get_Value_m1_3231((&V_0), /*hidden argument*/NULL);
		return L_7;
	}

IL_0042:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_9 = V_2;
		DictionaryEntryU5BU5D_t1_869* L_10 = V_1;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		return NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelDataStore::set_Item(System.Object,System.Object)
extern TypeInfo* DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var;
extern "C" void ChannelDataStore_set_Item_m1_8032 (ChannelDataStore_t1_868 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(625);
		s_Il2CppMethodIntialized = true;
	}
	DictionaryEntryU5BU5D_t1_869* V_0 = {0};
	{
		DictionaryEntryU5BU5D_t1_869* L_0 = (__this->____extraData_1);
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		DictionaryEntryU5BU5D_t1_869* L_1 = ((DictionaryEntryU5BU5D_t1_869*)SZArrayNew(DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var, 1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		Object_t * L_2 = ___key;
		Object_t * L_3 = ___value;
		DictionaryEntry_t1_284  L_4 = {0};
		DictionaryEntry__ctor_m1_3228(&L_4, L_2, L_3, /*hidden argument*/NULL);
		(*(DictionaryEntry_t1_284 *)((DictionaryEntry_t1_284 *)(DictionaryEntry_t1_284 *)SZArrayLdElema(L_1, 0, sizeof(DictionaryEntry_t1_284 )))) = L_4;
		__this->____extraData_1 = L_1;
		goto IL_006d;
	}

IL_002f:
	{
		DictionaryEntryU5BU5D_t1_869* L_5 = (__this->____extraData_1);
		NullCheck(L_5);
		V_0 = ((DictionaryEntryU5BU5D_t1_869*)SZArrayNew(DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))))+(int32_t)1))));
		DictionaryEntryU5BU5D_t1_869* L_6 = (__this->____extraData_1);
		DictionaryEntryU5BU5D_t1_869* L_7 = V_0;
		NullCheck(L_6);
		VirtActionInvoker2< Array_t *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, L_6, (Array_t *)(Array_t *)L_7, 0);
		DictionaryEntryU5BU5D_t1_869* L_8 = V_0;
		DictionaryEntryU5BU5D_t1_869* L_9 = (__this->____extraData_1);
		NullCheck(L_9);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, (((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))));
		Object_t * L_10 = ___key;
		Object_t * L_11 = ___value;
		DictionaryEntry_t1_284  L_12 = {0};
		DictionaryEntry__ctor_m1_3228(&L_12, L_10, L_11, /*hidden argument*/NULL);
		(*(DictionaryEntry_t1_284 *)((DictionaryEntry_t1_284 *)(DictionaryEntry_t1_284 *)SZArrayLdElema(L_8, (((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))), sizeof(DictionaryEntry_t1_284 )))) = L_12;
		DictionaryEntryU5BU5D_t1_869* L_13 = V_0;
		__this->____extraData_1 = L_13;
	}

IL_006d:
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern "C" void ChannelInfo__ctor_m1_8033 (ChannelInfo_t1_870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1_272* L_0 = ChannelServices_GetCurrentChannelInfo_m1_8058(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___channelData_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor(System.Object)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern "C" void ChannelInfo__ctor_m1_8034 (ChannelInfo_t1_870 * __this, Object_t * ___remoteChannelData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		Object_t * L_1 = ___remoteChannelData;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		__this->___channelData_0 = L_0;
		return;
	}
}
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern "C" ObjectU5BU5D_t1_272* ChannelInfo_get_ChannelData_m1_8035 (ChannelInfo_t1_870 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1_272* L_0 = (__this->___channelData_0);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.ChannelInfo::set_ChannelData(System.Object[])
extern "C" void ChannelInfo_set_ChannelData_m1_8036 (ChannelInfo_t1_870 * __this, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1_272* L_0 = ___value;
		__this->___channelData_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.ctor()
extern "C" void ChannelServices__ctor_m1_8037 (ChannelServices_t1_871 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.cctor()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* CrossContextChannel_t1_872_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2135;
extern Il2CppCodeGenString* _stringLiteral2136;
extern Il2CppCodeGenString* _stringLiteral2137;
extern "C" void ChannelServices__cctor_m1_8038 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		CrossContextChannel_t1_872_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(626);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral2135 = il2cpp_codegen_string_literal_from_index(2135);
		_stringLiteral2136 = il2cpp_codegen_string_literal_from_index(2136);
		_stringLiteral2137 = il2cpp_codegen_string_literal_from_index(2137);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_0, /*hidden argument*/NULL);
		((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0 = L_0;
		ArrayList_t1_170 * L_1 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_1, /*hidden argument*/NULL);
		((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___delayedClientChannels_1 = L_1;
		CrossContextChannel_t1_872 * L_2 = (CrossContextChannel_t1_872 *)il2cpp_codegen_object_new (CrossContextChannel_t1_872_il2cpp_TypeInfo_var);
		CrossContextChannel__ctor_m1_8185(L_2, /*hidden argument*/NULL);
		((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->____crossContextSink_2 = L_2;
		((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___CrossContextUrl_3 = _stringLiteral2135;
		StringU5BU5D_t1_238* L_3 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 2));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral2136);
		*((String_t**)(String_t**)SZArrayLdElema(L_3, 0, sizeof(String_t*))) = (String_t*)_stringLiteral2136;
		StringU5BU5D_t1_238* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, _stringLiteral2137);
		*((String_t**)(String_t**)SZArrayLdElema(L_4, 1, sizeof(String_t*))) = (String_t*)_stringLiteral2137;
		((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___oldStartModeTypes_4 = (Object_t *)L_4;
		return;
	}
}
// System.Runtime.Remoting.Contexts.CrossContextChannel System.Runtime.Remoting.Channels.ChannelServices::get_CrossContextChannel()
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern "C" CrossContextChannel_t1_872 * ChannelServices_get_CrossContextChannel_m1_8039 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		CrossContextChannel_t1_872 * L_0 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->____crossContextSink_2;
		return L_0;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ChannelServices::CreateClientChannelSinkChain(System.String,System.Object,System.String&)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IChannel_t1_1710_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelSender_t1_1705_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern "C" Object_t * ChannelServices_CreateClientChannelSinkChain_m1_8040 (Object_t * __this /* static, unused */, String_t* ___url, Object_t * ___remoteChannelData, String_t** ___objectUri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IChannel_t1_1710_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(627);
		IChannelSender_t1_1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(628);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_272* V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	Object_t * V_6 = {0};
	Object_t * V_7 = {0};
	Object_t * V_8 = {0};
	Object_t * V_9 = {0};
	Object_t * V_10 = {0};
	Object_t * V_11 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___remoteChannelData;
		V_0 = ((ObjectU5BU5D_t1_272*)Castclass(L_0, ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_1 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_1);
		V_1 = L_2;
		Object_t * L_3 = V_1;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_4 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_4);
			V_3 = L_5;
		}

IL_0023:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0064;
			}

IL_0028:
			{
				Object_t * L_6 = V_3;
				NullCheck(L_6);
				Object_t * L_7 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_6);
				V_2 = ((Object_t *)Castclass(L_7, IChannel_t1_1710_il2cpp_TypeInfo_var));
				Object_t * L_8 = V_2;
				V_4 = ((Object_t *)IsInst(L_8, IChannelSender_t1_1705_il2cpp_TypeInfo_var));
				Object_t * L_9 = V_4;
				if (L_9)
				{
					goto IL_0048;
				}
			}

IL_0043:
			{
				goto IL_0064;
			}

IL_0048:
			{
				Object_t * L_10 = V_4;
				String_t* L_11 = ___url;
				ObjectU5BU5D_t1_272* L_12 = V_0;
				String_t** L_13 = ___objectUri;
				IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
				Object_t * L_14 = ChannelServices_CreateClientChannelSinkChain_m1_8041(NULL /*static, unused*/, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
				V_5 = L_14;
				Object_t * L_15 = V_5;
				if (!L_15)
				{
					goto IL_0064;
				}
			}

IL_005b:
			{
				Object_t * L_16 = V_5;
				V_9 = L_16;
				IL2CPP_LEAVE(0x114, FINALLY_0074);
			}

IL_0064:
			{
				Object_t * L_17 = V_3;
				NullCheck(L_17);
				bool L_18 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_17);
				if (L_18)
				{
					goto IL_0028;
				}
			}

IL_006f:
			{
				IL2CPP_LEAVE(0x89, FINALLY_0074);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_0074;
		}

FINALLY_0074:
		{ // begin finally (depth: 2)
			{
				Object_t * L_19 = V_3;
				V_10 = ((Object_t *)IsInst(L_19, IDisposable_t1_1035_il2cpp_TypeInfo_var));
				Object_t * L_20 = V_10;
				if (L_20)
				{
					goto IL_0081;
				}
			}

IL_0080:
			{
				IL2CPP_END_FINALLY(116)
			}

IL_0081:
			{
				Object_t * L_21 = V_10;
				NullCheck(L_21);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_21);
				IL2CPP_END_FINALLY(116)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(116)
		{
			IL2CPP_END_CLEANUP(0x114, FINALLY_0108);
			IL2CPP_JUMP_TBL(0x89, IL_0089)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_0089:
		{
			IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
			RemotingConfiguration_LoadDefaultDelayedChannels_m1_9116(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_22 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___delayedClientChannels_1;
			NullCheck(L_22);
			Object_t * L_23 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_22);
			V_7 = L_23;
		}

IL_009a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00dc;
			}

IL_009f:
			{
				Object_t * L_24 = V_7;
				NullCheck(L_24);
				Object_t * L_25 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_24);
				V_6 = ((Object_t *)Castclass(L_25, IChannelSender_t1_1705_il2cpp_TypeInfo_var));
				Object_t * L_26 = V_6;
				String_t* L_27 = ___url;
				ObjectU5BU5D_t1_272* L_28 = V_0;
				String_t** L_29 = ___objectUri;
				IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
				Object_t * L_30 = ChannelServices_CreateClientChannelSinkChain_m1_8041(NULL /*static, unused*/, L_26, L_27, L_28, L_29, /*hidden argument*/NULL);
				V_8 = L_30;
				Object_t * L_31 = V_8;
				if (!L_31)
				{
					goto IL_00dc;
				}
			}

IL_00c0:
			{
				IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
				ArrayList_t1_170 * L_32 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___delayedClientChannels_1;
				Object_t * L_33 = V_6;
				NullCheck(L_32);
				VirtActionInvoker1< Object_t * >::Invoke(42 /* System.Void System.Collections.ArrayList::Remove(System.Object) */, L_32, L_33);
				Object_t * L_34 = V_6;
				ChannelServices_RegisterChannel_m1_8048(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
				Object_t * L_35 = V_8;
				V_9 = L_35;
				IL2CPP_LEAVE(0x114, FINALLY_00ed);
			}

IL_00dc:
			{
				Object_t * L_36 = V_7;
				NullCheck(L_36);
				bool L_37 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_36);
				if (L_37)
				{
					goto IL_009f;
				}
			}

IL_00e8:
			{
				IL2CPP_LEAVE(0x103, FINALLY_00ed);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_00ed;
		}

FINALLY_00ed:
		{ // begin finally (depth: 2)
			{
				Object_t * L_38 = V_7;
				V_11 = ((Object_t *)IsInst(L_38, IDisposable_t1_1035_il2cpp_TypeInfo_var));
				Object_t * L_39 = V_11;
				if (L_39)
				{
					goto IL_00fb;
				}
			}

IL_00fa:
			{
				IL2CPP_END_FINALLY(237)
			}

IL_00fb:
			{
				Object_t * L_40 = V_11;
				NullCheck(L_40);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_40);
				IL2CPP_END_FINALLY(237)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(237)
		{
			IL2CPP_END_CLEANUP(0x114, FINALLY_0108);
			IL2CPP_JUMP_TBL(0x103, IL_0103)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_0103:
		{
			IL2CPP_LEAVE(0x10F, FINALLY_0108);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0108;
	}

FINALLY_0108:
	{ // begin finally (depth: 1)
		Object_t * L_41 = V_1;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(264)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(264)
	{
		IL2CPP_JUMP_TBL(0x114, IL_0114)
		IL2CPP_JUMP_TBL(0x10F, IL_010f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_010f:
	{
		String_t** L_42 = ___objectUri;
		*((Object_t **)(L_42)) = (Object_t *)NULL;
		return (Object_t *)NULL;
	}

IL_0114:
	{
		Object_t * L_43 = V_9;
		return L_43;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ChannelServices::CreateClientChannelSinkChain(System.Runtime.Remoting.Channels.IChannelSender,System.String,System.Object[],System.String&)
extern TypeInfo* IChannelSender_t1_1705_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelDataStore_t1_1714_il2cpp_TypeInfo_var;
extern "C" Object_t * ChannelServices_CreateClientChannelSinkChain_m1_8041 (Object_t * __this /* static, unused */, Object_t * ___sender, String_t* ___url, ObjectU5BU5D_t1_272* ___channelDataArray, String_t** ___objectUri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChannelSender_t1_1705_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(628);
		IChannelDataStore_t1_1714_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ObjectU5BU5D_t1_272* V_1 = {0};
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	{
		String_t** L_0 = ___objectUri;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		ObjectU5BU5D_t1_272* L_1 = ___channelDataArray;
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		Object_t * L_2 = ___sender;
		String_t* L_3 = ___url;
		String_t** L_4 = ___objectUri;
		NullCheck(L_2);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker3< Object_t *, String_t*, Object_t *, String_t** >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.IChannelSender::CreateMessageSink(System.String,System.Object,System.String&) */, IChannelSender_t1_1705_il2cpp_TypeInfo_var, L_2, L_3, NULL, L_4);
		return L_5;
	}

IL_0013:
	{
		ObjectU5BU5D_t1_272* L_6 = ___channelDataArray;
		V_1 = L_6;
		V_2 = 0;
		goto IL_0050;
	}

IL_001c:
	{
		ObjectU5BU5D_t1_272* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_0 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_7, L_9, sizeof(Object_t *)));
		Object_t * L_10 = V_0;
		if (!((Object_t *)IsInst(L_10, IChannelDataStore_t1_1714_il2cpp_TypeInfo_var)))
		{
			goto IL_003a;
		}
	}
	{
		Object_t * L_11 = ___sender;
		Object_t * L_12 = V_0;
		String_t** L_13 = ___objectUri;
		NullCheck(L_11);
		Object_t * L_14 = (Object_t *)InterfaceFuncInvoker3< Object_t *, String_t*, Object_t *, String_t** >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.IChannelSender::CreateMessageSink(System.String,System.Object,System.String&) */, IChannelSender_t1_1705_il2cpp_TypeInfo_var, L_11, (String_t*)NULL, L_12, L_13);
		V_3 = L_14;
		goto IL_0044;
	}

IL_003a:
	{
		Object_t * L_15 = ___sender;
		String_t* L_16 = ___url;
		Object_t * L_17 = V_0;
		String_t** L_18 = ___objectUri;
		NullCheck(L_15);
		Object_t * L_19 = (Object_t *)InterfaceFuncInvoker3< Object_t *, String_t*, Object_t *, String_t** >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.IChannelSender::CreateMessageSink(System.String,System.Object,System.String&) */, IChannelSender_t1_1705_il2cpp_TypeInfo_var, L_15, L_16, L_17, L_18);
		V_3 = L_19;
	}

IL_0044:
	{
		Object_t * L_20 = V_3;
		if (!L_20)
		{
			goto IL_004c;
		}
	}
	{
		Object_t * L_21 = V_3;
		return L_21;
	}

IL_004c:
	{
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_23 = V_2;
		ObjectU5BU5D_t1_272* L_24 = V_1;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_24)->max_length)))))))
		{
			goto IL_001c;
		}
	}
	{
		return (Object_t *)NULL;
	}
}
// System.Runtime.Remoting.Channels.IChannel[] System.Runtime.Remoting.Channels.ChannelServices::get_RegisteredChannels()
extern const Il2CppType* IChannel_t1_1710_0_0_0_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* IChannel_t1_1710_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelU5BU5D_t1_1704_il2cpp_TypeInfo_var;
extern "C" IChannelU5BU5D_t1_1704* ChannelServices_get_RegisteredChannels_m1_8042 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChannel_t1_1710_0_0_0_var = il2cpp_codegen_type_from_index(627);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		IChannel_t1_1710_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(627);
		CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IChannelU5BU5D_t1_1704_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(631);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ArrayList_t1_170 * V_1 = {0};
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	IChannelU5BU5D_t1_1704* V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_0 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			ArrayList_t1_170 * L_3 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
			ArrayList__ctor_m1_3049(L_3, /*hidden argument*/NULL);
			V_1 = L_3;
			V_2 = 0;
			goto IL_004b;
		}

IL_001e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_4 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			int32_t L_5 = V_2;
			NullCheck(L_4);
			Object_t * L_6 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, L_5);
			V_3 = ((Object_t *)Castclass(L_6, IChannel_t1_1710_il2cpp_TypeInfo_var));
			Object_t * L_7 = V_3;
			if (!((CrossAppDomainChannel_t1_879 *)IsInstClass(L_7, CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var)))
			{
				goto IL_003f;
			}
		}

IL_003a:
		{
			goto IL_0047;
		}

IL_003f:
		{
			ArrayList_t1_170 * L_8 = V_1;
			Object_t * L_9 = V_3;
			NullCheck(L_8);
			VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_8, L_9);
		}

IL_0047:
		{
			int32_t L_10 = V_2;
			V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_004b:
		{
			int32_t L_11 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_12 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_12);
			int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_12);
			if ((((int32_t)L_11) < ((int32_t)L_13)))
			{
				goto IL_001e;
			}
		}

IL_005b:
		{
			ArrayList_t1_170 * L_14 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_15 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IChannel_t1_1710_0_0_0_var), /*hidden argument*/NULL);
			NullCheck(L_14);
			Array_t * L_16 = (Array_t *)VirtFuncInvoker1< Array_t *, Type_t * >::Invoke(63 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_14, L_15);
			V_4 = ((IChannelU5BU5D_t1_1704*)Castclass(L_16, IChannelU5BU5D_t1_1704_il2cpp_TypeInfo_var));
			IL2CPP_LEAVE(0x83, FINALLY_007c);
		}

IL_0077:
		{
			; // IL_0077: leave IL_0083
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		Object_t * L_17 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(124)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0083:
	{
		IChannelU5BU5D_t1_1704* L_18 = V_4;
		return L_18;
	}
}
// System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.ChannelServices::CreateServerChannelSinkChain(System.Runtime.Remoting.Channels.IServerChannelSinkProvider,System.Runtime.Remoting.Channels.IChannelReceiver)
extern TypeInfo* IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var;
extern TypeInfo* ServerDispatchSinkProvider_t1_887_il2cpp_TypeInfo_var;
extern "C" Object_t * ChannelServices_CreateServerChannelSinkChain_m1_8043 (Object_t * __this /* static, unused */, Object_t * ___provider, Object_t * ___channel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(632);
		ServerDispatchSinkProvider_t1_887_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(633);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___provider;
		V_0 = L_0;
		goto IL_000e;
	}

IL_0007:
	{
		Object_t * L_1 = V_0;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Runtime.Remoting.Channels.IServerChannelSinkProvider System.Runtime.Remoting.Channels.IServerChannelSinkProvider::get_Next() */, IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
	}

IL_000e:
	{
		Object_t * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Runtime.Remoting.Channels.IServerChannelSinkProvider System.Runtime.Remoting.Channels.IServerChannelSinkProvider::get_Next() */, IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var, L_3);
		if (L_4)
		{
			goto IL_0007;
		}
	}
	{
		Object_t * L_5 = V_0;
		ServerDispatchSinkProvider_t1_887 * L_6 = (ServerDispatchSinkProvider_t1_887 *)il2cpp_codegen_object_new (ServerDispatchSinkProvider_t1_887_il2cpp_TypeInfo_var);
		ServerDispatchSinkProvider__ctor_m1_8114(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceActionInvoker1< Object_t * >::Invoke(1 /* System.Void System.Runtime.Remoting.Channels.IServerChannelSinkProvider::set_Next(System.Runtime.Remoting.Channels.IServerChannelSinkProvider) */, IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var, L_5, L_6);
		Object_t * L_7 = ___provider;
		Object_t * L_8 = ___channel;
		NullCheck(L_7);
		Object_t * L_9 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.IServerChannelSinkProvider::CreateSink(System.Runtime.Remoting.Channels.IChannelReceiver) */, IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var, L_7, L_8);
		return L_9;
	}
}
// System.Runtime.Remoting.Channels.ServerProcessing System.Runtime.Remoting.Channels.ChannelServices::DispatchMessage(System.Runtime.Remoting.Channels.IServerChannelSinkStack,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessage&)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodMessage_t1_948_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2138;
extern "C" int32_t ChannelServices_DispatchMessage_m1_8044 (Object_t * __this /* static, unused */, Object_t * ___sinkStack, Object_t * ___msg, Object_t ** ___replyMsg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IMethodMessage_t1_948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		_stringLiteral2138 = il2cpp_codegen_string_literal_from_index(2138);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___msg;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2138, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t ** L_2 = ___replyMsg;
		Object_t * L_3 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		Object_t * L_4 = ChannelServices_SyncDispatchMessage_m1_8052(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		*((Object_t **)(L_2)) = (Object_t *)L_4;
		Object_t * L_5 = ___msg;
		NullCheck(((Object_t *)Castclass(L_5, IMethodMessage_t1_948_il2cpp_TypeInfo_var)));
		MethodBase_t1_335 * L_6 = (MethodBase_t1_335 *)InterfaceFuncInvoker0< MethodBase_t1_335 * >::Invoke(4 /* System.Reflection.MethodBase System.Runtime.Remoting.Messaging.IMethodMessage::get_MethodBase() */, IMethodMessage_t1_948_il2cpp_TypeInfo_var, ((Object_t *)Castclass(L_5, IMethodMessage_t1_948_il2cpp_TypeInfo_var)));
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		bool L_7 = RemotingServices_IsOneWay_m1_9209(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0030;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0030:
	{
		return (int32_t)(0);
	}
}
// System.Runtime.Remoting.Channels.IChannel System.Runtime.Remoting.Channels.ChannelServices::GetChannel(System.String)
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IChannel_t1_1710_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" Object_t * ChannelServices_GetChannel_m1_8045 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IChannel_t1_1710_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(627);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_0 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_3 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_3);
			V_2 = L_4;
		}

IL_001c:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0050;
			}

IL_0021:
			{
				Object_t * L_5 = V_2;
				NullCheck(L_5);
				Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_5);
				V_1 = ((Object_t *)Castclass(L_6, IChannel_t1_1710_il2cpp_TypeInfo_var));
				Object_t * L_7 = V_1;
				NullCheck(L_7);
				String_t* L_8 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_7);
				String_t* L_9 = ___name;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				bool L_10 = String_op_Equality_m1_601(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
				if (!L_10)
				{
					goto IL_0050;
				}
			}

IL_003e:
			{
				Object_t * L_11 = V_1;
				if (((CrossAppDomainChannel_t1_879 *)IsInstClass(L_11, CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var)))
				{
					goto IL_0050;
				}
			}

IL_0049:
			{
				Object_t * L_12 = V_1;
				V_3 = L_12;
				IL2CPP_LEAVE(0x88, FINALLY_0060);
			}

IL_0050:
			{
				Object_t * L_13 = V_2;
				NullCheck(L_13);
				bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_13);
				if (L_14)
				{
					goto IL_0021;
				}
			}

IL_005b:
			{
				IL2CPP_LEAVE(0x75, FINALLY_0060);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_0060;
		}

FINALLY_0060:
		{ // begin finally (depth: 2)
			{
				Object_t * L_15 = V_2;
				V_4 = ((Object_t *)IsInst(L_15, IDisposable_t1_1035_il2cpp_TypeInfo_var));
				Object_t * L_16 = V_4;
				if (L_16)
				{
					goto IL_006d;
				}
			}

IL_006c:
			{
				IL2CPP_END_FINALLY(96)
			}

IL_006d:
			{
				Object_t * L_17 = V_4;
				NullCheck(L_17);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_17);
				IL2CPP_END_FINALLY(96)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(96)
		{
			IL2CPP_END_CLEANUP(0x88, FINALLY_0081);
			IL2CPP_JUMP_TBL(0x75, IL_0075)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_0075:
		{
			V_3 = (Object_t *)NULL;
			IL2CPP_LEAVE(0x88, FINALLY_0081);
		}

IL_007c:
		{
			; // IL_007c: leave IL_0088
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0081;
	}

FINALLY_0081:
	{ // begin finally (depth: 1)
		Object_t * L_18 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(129)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(129)
	{
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0088:
	{
		Object_t * L_19 = V_3;
		return L_19;
	}
}
// System.Collections.IDictionary System.Runtime.Remoting.Channels.ChannelServices::GetChannelSinkProperties(System.Object)
extern const Il2CppType* IDictionaryU5BU5D_t1_861_0_0_0_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ClientIdentity_t1_1013_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern TypeInfo* IClientChannelSink_t1_1712_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelSinkBase_t1_867_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionaryU5BU5D_t1_861_il2cpp_TypeInfo_var;
extern TypeInfo* AggregateDictionary_t1_860_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2139;
extern Il2CppCodeGenString* _stringLiteral854;
extern "C" Object_t * ChannelServices_GetChannelSinkProperties_m1_8046 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryU5BU5D_t1_861_0_0_0_var = il2cpp_codegen_type_from_index(623);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ClientIdentity_t1_1013_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(634);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		IClientChannelSink_t1_1712_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		IChannelSinkBase_t1_867_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		IDictionaryU5BU5D_t1_861_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		AggregateDictionary_t1_860_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		_stringLiteral2139 = il2cpp_codegen_string_literal_from_index(2139);
		_stringLiteral854 = il2cpp_codegen_string_literal_from_index(854);
		s_Il2CppMethodIntialized = true;
	}
	ClientIdentity_t1_1013 * V_0 = {0};
	Object_t * V_1 = {0};
	ArrayList_t1_170 * V_2 = {0};
	Object_t * V_3 = {0};
	IDictionaryU5BU5D_t1_861* V_4 = {0};
	{
		Object_t * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		bool L_1 = RemotingServices_IsTransparentProxy_m1_9182(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentException_t1_1425 * L_2 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_2, _stringLiteral2139, _stringLiteral854, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_001b:
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		RealProxy_t1_130 * L_4 = RemotingServices_GetRealProxy_m1_9196(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Identity_t1_943 * L_5 = RealProxy_get_ObjectIdentity_m1_8997(L_4, /*hidden argument*/NULL);
		V_0 = ((ClientIdentity_t1_1013 *)CastclassClass(L_5, ClientIdentity_t1_1013_il2cpp_TypeInfo_var));
		ClientIdentity_t1_1013 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = Identity_get_ChannelSink_m1_9054(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		ArrayList_t1_170 * L_8 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_8, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_0045;
	}

IL_003e:
	{
		Object_t * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.IMessageSink::get_NextSink() */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_9);
		V_1 = L_10;
	}

IL_0045:
	{
		Object_t * L_11 = V_1;
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		Object_t * L_12 = V_1;
		if (!((Object_t *)IsInst(L_12, IClientChannelSink_t1_1712_il2cpp_TypeInfo_var)))
		{
			goto IL_003e;
		}
	}

IL_0056:
	{
		Object_t * L_13 = V_1;
		if (L_13)
		{
			goto IL_0062;
		}
	}
	{
		Hashtable_t1_100 * L_14 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_14, /*hidden argument*/NULL);
		return L_14;
	}

IL_0062:
	{
		Object_t * L_15 = V_1;
		V_3 = ((Object_t *)IsInst(L_15, IClientChannelSink_t1_1712_il2cpp_TypeInfo_var));
		goto IL_0082;
	}

IL_006e:
	{
		ArrayList_t1_170 * L_16 = V_2;
		Object_t * L_17 = V_3;
		NullCheck(L_17);
		Object_t * L_18 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IDictionary System.Runtime.Remoting.Channels.IChannelSinkBase::get_Properties() */, IChannelSinkBase_t1_867_il2cpp_TypeInfo_var, L_17);
		NullCheck(L_16);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_16, L_18);
		Object_t * L_19 = V_3;
		NullCheck(L_19);
		Object_t * L_20 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Runtime.Remoting.Channels.IClientChannelSink System.Runtime.Remoting.Channels.IClientChannelSink::get_NextChannelSink() */, IClientChannelSink_t1_1712_il2cpp_TypeInfo_var, L_19);
		V_3 = L_20;
	}

IL_0082:
	{
		Object_t * L_21 = V_3;
		if (L_21)
		{
			goto IL_006e;
		}
	}
	{
		ArrayList_t1_170 * L_22 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IDictionaryU5BU5D_t1_861_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_22);
		Array_t * L_24 = (Array_t *)VirtFuncInvoker1< Array_t *, Type_t * >::Invoke(63 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_22, L_23);
		V_4 = ((IDictionaryU5BU5D_t1_861*)Castclass(L_24, IDictionaryU5BU5D_t1_861_il2cpp_TypeInfo_var));
		IDictionaryU5BU5D_t1_861* L_25 = V_4;
		AggregateDictionary_t1_860 * L_26 = (AggregateDictionary_t1_860 *)il2cpp_codegen_object_new (AggregateDictionary_t1_860_il2cpp_TypeInfo_var);
		AggregateDictionary__ctor_m1_7983(L_26, L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.String[] System.Runtime.Remoting.Channels.ChannelServices::GetUrlsForObject(System.MarshalByRefObject)
extern const Il2CppType* String_t_0_0_0_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelReceiver_t1_1708_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" StringU5BU5D_t1_238* ChannelServices_GetUrlsForObject_m1_8047 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		IChannelReceiver_t1_1708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	ArrayList_t1_170 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MarshalByRefObject_t1_69 * L_0 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		String_t* L_1 = RemotingServices_GetObjectUri_m1_9189(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0));
	}

IL_0014:
	{
		ArrayList_t1_170 * L_3 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_4 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_4);
		V_2 = L_5;
		Object_t * L_6 = V_2;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_002b:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_7 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_7);
			Object_t * L_8 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_7);
			V_4 = L_8;
		}

IL_0037:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0071;
			}

IL_003c:
			{
				Object_t * L_9 = V_4;
				NullCheck(L_9);
				Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_9);
				V_3 = L_10;
				Object_t * L_11 = V_3;
				if (!((CrossAppDomainChannel_t1_879 *)IsInstClass(L_11, CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var)))
				{
					goto IL_0054;
				}
			}

IL_004f:
			{
				goto IL_0071;
			}

IL_0054:
			{
				Object_t * L_12 = V_3;
				V_5 = ((Object_t *)IsInst(L_12, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var));
				Object_t * L_13 = V_5;
				if (!L_13)
				{
					goto IL_0071;
				}
			}

IL_0063:
			{
				ArrayList_t1_170 * L_14 = V_1;
				Object_t * L_15 = V_5;
				String_t* L_16 = V_0;
				NullCheck(L_15);
				StringU5BU5D_t1_238* L_17 = (StringU5BU5D_t1_238*)InterfaceFuncInvoker1< StringU5BU5D_t1_238*, String_t* >::Invoke(1 /* System.String[] System.Runtime.Remoting.Channels.IChannelReceiver::GetUrlsForUri(System.String) */, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var, L_15, L_16);
				NullCheck(L_14);
				VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection) */, L_14, (Object_t *)(Object_t *)L_17);
			}

IL_0071:
			{
				Object_t * L_18 = V_4;
				NullCheck(L_18);
				bool L_19 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_18);
				if (L_19)
				{
					goto IL_003c;
				}
			}

IL_007d:
			{
				IL2CPP_LEAVE(0x98, FINALLY_0082);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_0082;
		}

FINALLY_0082:
		{ // begin finally (depth: 2)
			{
				Object_t * L_20 = V_4;
				V_6 = ((Object_t *)IsInst(L_20, IDisposable_t1_1035_il2cpp_TypeInfo_var));
				Object_t * L_21 = V_6;
				if (L_21)
				{
					goto IL_0090;
				}
			}

IL_008f:
			{
				IL2CPP_END_FINALLY(130)
			}

IL_0090:
			{
				Object_t * L_22 = V_6;
				NullCheck(L_22);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_22);
				IL2CPP_END_FINALLY(130)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(130)
		{
			IL2CPP_JUMP_TBL(0x98, IL_0098)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_0098:
		{
			IL2CPP_LEAVE(0xA4, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		Object_t * L_23 = V_2;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(157)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xA4, IL_00a4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00a4:
	{
		ArrayList_t1_170 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		Array_t * L_26 = (Array_t *)VirtFuncInvoker1< Array_t *, Type_t * >::Invoke(63 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_24, L_25);
		return ((StringU5BU5D_t1_238*)Castclass(L_26, StringU5BU5D_t1_238_il2cpp_TypeInfo_var));
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern "C" void ChannelServices_RegisterChannel_m1_8048 (Object_t * __this /* static, unused */, Object_t * ___chnl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___chnl;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ChannelServices_RegisterChannel_m1_8049(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel,System.Boolean)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ISecurableChannel_t1_1765_il2cpp_TypeInfo_var;
extern TypeInfo* IChannel_t1_1710_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelReceiver_t1_1708_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2140;
extern Il2CppCodeGenString* _stringLiteral2141;
extern Il2CppCodeGenString* _stringLiteral2142;
extern Il2CppCodeGenString* _stringLiteral2143;
extern "C" void ChannelServices_RegisterChannel_m1_8049 (Object_t * __this /* static, unused */, Object_t * ___chnl, bool ___ensureSecurity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ISecurableChannel_t1_1765_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		IChannel_t1_1710_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(627);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IChannelReceiver_t1_1708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		_stringLiteral2140 = il2cpp_codegen_string_literal_from_index(2140);
		_stringLiteral2141 = il2cpp_codegen_string_literal_from_index(2141);
		_stringLiteral2142 = il2cpp_codegen_string_literal_from_index(2142);
		_stringLiteral2143 = il2cpp_codegen_string_literal_from_index(2143);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___chnl;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2140, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		bool L_2 = ___ensureSecurity;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		Object_t * L_3 = ___chnl;
		V_1 = ((Object_t *)IsInst(L_3, ISecurableChannel_t1_1765_il2cpp_TypeInfo_var));
		Object_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		Object_t * L_5 = ___chnl;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral2141, L_6, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_8 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_8, L_7, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_003a:
	{
		Object_t * L_9 = V_1;
		NullCheck(L_9);
		InterfaceActionInvoker1< bool >::Invoke(1 /* System.Void System.Runtime.Remoting.Channels.ISecurableChannel::set_IsSecured(System.Boolean) */, ISecurableChannel_t1_1765_il2cpp_TypeInfo_var, L_9, 1);
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_10 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_10);
		Object_t * L_11 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_10);
		V_0 = L_11;
		Object_t * L_12 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		{
			V_2 = (-1);
			V_3 = 0;
			goto IL_00d4;
		}

IL_005b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_13 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			int32_t L_14 = V_3;
			NullCheck(L_13);
			Object_t * L_15 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_13, L_14);
			V_4 = ((Object_t *)Castclass(L_15, IChannel_t1_1710_il2cpp_TypeInfo_var));
			Object_t * L_16 = V_4;
			NullCheck(L_16);
			String_t* L_17 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_16);
			Object_t * L_18 = ___chnl;
			NullCheck(L_18);
			String_t* L_19 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_18);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_20 = String_op_Equality_m1_601(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
			if (!L_20)
			{
				goto IL_00b5;
			}
		}

IL_0084:
		{
			Object_t * L_21 = ___chnl;
			NullCheck(L_21);
			String_t* L_22 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_21);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_23 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
			bool L_24 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
			if (!L_24)
			{
				goto IL_00b5;
			}
		}

IL_0099:
		{
			Object_t * L_25 = V_4;
			NullCheck(L_25);
			String_t* L_26 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Channels.IChannel::get_ChannelName() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_25);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_27 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral2142, L_26, _stringLiteral2143, /*hidden argument*/NULL);
			RemotingException_t1_1025 * L_28 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
			RemotingException__ctor_m1_9174(L_28, L_27, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_28);
		}

IL_00b5:
		{
			Object_t * L_29 = V_4;
			NullCheck(L_29);
			int32_t L_30 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Runtime.Remoting.Channels.IChannel::get_ChannelPriority() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_29);
			Object_t * L_31 = ___chnl;
			NullCheck(L_31);
			int32_t L_32 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Runtime.Remoting.Channels.IChannel::get_ChannelPriority() */, IChannel_t1_1710_il2cpp_TypeInfo_var, L_31);
			if ((((int32_t)L_30) >= ((int32_t)L_32)))
			{
				goto IL_00d0;
			}
		}

IL_00c7:
		{
			int32_t L_33 = V_2;
			if ((!(((uint32_t)L_33) == ((uint32_t)(-1)))))
			{
				goto IL_00d0;
			}
		}

IL_00ce:
		{
			int32_t L_34 = V_3;
			V_2 = L_34;
		}

IL_00d0:
		{
			int32_t L_35 = V_3;
			V_3 = ((int32_t)((int32_t)L_35+(int32_t)1));
		}

IL_00d4:
		{
			int32_t L_36 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_37 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_37);
			int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_37);
			if ((((int32_t)L_36) < ((int32_t)L_38)))
			{
				goto IL_005b;
			}
		}

IL_00e4:
		{
			int32_t L_39 = V_2;
			if ((((int32_t)L_39) == ((int32_t)(-1))))
			{
				goto IL_00fc;
			}
		}

IL_00eb:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_40 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			int32_t L_41 = V_2;
			Object_t * L_42 = ___chnl;
			NullCheck(L_40);
			VirtActionInvoker2< int32_t, Object_t * >::Invoke(40 /* System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object) */, L_40, L_41, L_42);
			goto IL_0108;
		}

IL_00fc:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_43 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			Object_t * L_44 = ___chnl;
			NullCheck(L_43);
			VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_43, L_44);
		}

IL_0108:
		{
			Object_t * L_45 = ___chnl;
			V_5 = ((Object_t *)IsInst(L_45, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var));
			Object_t * L_46 = V_5;
			if (!L_46)
			{
				goto IL_0139;
			}
		}

IL_0117:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			Object_t * L_47 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___oldStartModeTypes_4;
			Object_t * L_48 = ___chnl;
			NullCheck(L_48);
			Type_t * L_49 = Object_GetType_m1_5(L_48, /*hidden argument*/NULL);
			NullCheck(L_49);
			String_t* L_50 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_49);
			NullCheck(L_47);
			bool L_51 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t1_262_il2cpp_TypeInfo_var, L_47, L_50);
			if (!L_51)
			{
				goto IL_0139;
			}
		}

IL_0131:
		{
			Object_t * L_52 = V_5;
			NullCheck(L_52);
			InterfaceActionInvoker1< Object_t * >::Invoke(2 /* System.Void System.Runtime.Remoting.Channels.IChannelReceiver::StartListening(System.Object) */, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var, L_52, NULL);
		}

IL_0139:
		{
			IL2CPP_LEAVE(0x145, FINALLY_013e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_013e;
	}

FINALLY_013e:
	{ // begin finally (depth: 1)
		Object_t * L_53 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(318)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(318)
	{
		IL2CPP_JUMP_TBL(0x145, IL_0145)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0145:
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannelConfig(System.Runtime.Remoting.ChannelData)
extern const Il2CppType* IChannelSender_t1_1705_0_0_0_var;
extern const Il2CppType* IChannelReceiver_t1_1708_0_0_0_var;
extern const Il2CppType* IDictionary_t1_35_0_0_0_var;
extern const Il2CppType* IClientChannelSinkProvider_t1_1766_0_0_0_var;
extern const Il2CppType* IServerChannelSinkProvider_t1_1707_0_0_0_var;
extern TypeInfo* ProviderData_t1_1023_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var;
extern TypeInfo* IClientChannelSinkProvider_t1_1766_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* IChannel_t1_1710_il2cpp_TypeInfo_var;
extern TypeInfo* TargetInvocationException_t1_638_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelReceiver_t1_1708_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1602;
extern Il2CppCodeGenString* _stringLiteral2144;
extern Il2CppCodeGenString* _stringLiteral2145;
extern Il2CppCodeGenString* _stringLiteral2146;
extern Il2CppCodeGenString* _stringLiteral2147;
extern "C" void ChannelServices_RegisterChannelConfig_m1_8050 (Object_t * __this /* static, unused */, ChannelData_t1_1022 * ___channel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IChannelSender_t1_1705_0_0_0_var = il2cpp_codegen_type_from_index(628);
		IChannelReceiver_t1_1708_0_0_0_var = il2cpp_codegen_type_from_index(636);
		IDictionary_t1_35_0_0_0_var = il2cpp_codegen_type_from_index(116);
		IClientChannelSinkProvider_t1_1766_0_0_0_var = il2cpp_codegen_type_from_index(638);
		IServerChannelSinkProvider_t1_1707_0_0_0_var = il2cpp_codegen_type_from_index(632);
		ProviderData_t1_1023_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(632);
		IClientChannelSinkProvider_t1_1766_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(638);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		IChannel_t1_1710_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(627);
		TargetInvocationException_t1_638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(450);
		IChannelReceiver_t1_1708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		_stringLiteral1602 = il2cpp_codegen_string_literal_from_index(1602);
		_stringLiteral2144 = il2cpp_codegen_string_literal_from_index(2144);
		_stringLiteral2145 = il2cpp_codegen_string_literal_from_index(2145);
		_stringLiteral2146 = il2cpp_codegen_string_literal_from_index(2146);
		_stringLiteral2147 = il2cpp_codegen_string_literal_from_index(2147);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	int32_t V_2 = 0;
	ProviderData_t1_1023 * V_3 = {0};
	Object_t * V_4 = {0};
	int32_t V_5 = 0;
	ProviderData_t1_1023 * V_6 = {0};
	Object_t * V_7 = {0};
	Type_t * V_8 = {0};
	ObjectU5BU5D_t1_272* V_9 = {0};
	TypeU5BU5D_t1_31* V_10 = {0};
	bool V_11 = false;
	bool V_12 = false;
	ConstructorInfo_t1_478 * V_13 = {0};
	Object_t * V_14 = {0};
	Object_t * V_15 = {0};
	TargetInvocationException_t1_638 * V_16 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Object_t *)NULL;
		V_1 = (Object_t *)NULL;
		ChannelData_t1_1022 * L_0 = ___channel;
		NullCheck(L_0);
		ArrayList_t1_170 * L_1 = ChannelData_get_ServerProviders_m1_9166(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_1);
		V_2 = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_0045;
	}

IL_0017:
	{
		ChannelData_t1_1022 * L_3 = ___channel;
		NullCheck(L_3);
		ArrayList_t1_170 * L_4 = ChannelData_get_ServerProviders_m1_9166(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		NullCheck(L_4);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_4, L_5);
		V_3 = ((ProviderData_t1_1023 *)IsInstClass(L_6, ProviderData_t1_1023_il2cpp_TypeInfo_var));
		ProviderData_t1_1023 * L_7 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		Object_t * L_8 = ChannelServices_CreateProvider_m1_8051(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_4 = ((Object_t *)Castclass(L_8, IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var));
		Object_t * L_9 = V_4;
		Object_t * L_10 = V_0;
		NullCheck(L_9);
		InterfaceActionInvoker1< Object_t * >::Invoke(1 /* System.Void System.Runtime.Remoting.Channels.IServerChannelSinkProvider::set_Next(System.Runtime.Remoting.Channels.IServerChannelSinkProvider) */, IServerChannelSinkProvider_t1_1707_il2cpp_TypeInfo_var, L_9, L_10);
		Object_t * L_11 = V_4;
		V_0 = L_11;
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0045:
	{
		int32_t L_13 = V_2;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		ChannelData_t1_1022 * L_14 = ___channel;
		NullCheck(L_14);
		ArrayList_t1_170 * L_15 = ChannelData_get_ClientProviders_m1_9167(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_15);
		V_5 = ((int32_t)((int32_t)L_16-(int32_t)1));
		goto IL_0093;
	}

IL_0060:
	{
		ChannelData_t1_1022 * L_17 = ___channel;
		NullCheck(L_17);
		ArrayList_t1_170 * L_18 = ChannelData_get_ClientProviders_m1_9167(L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_5;
		NullCheck(L_18);
		Object_t * L_20 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_18, L_19);
		V_6 = ((ProviderData_t1_1023 *)IsInstClass(L_20, ProviderData_t1_1023_il2cpp_TypeInfo_var));
		ProviderData_t1_1023 * L_21 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		Object_t * L_22 = ChannelServices_CreateProvider_m1_8051(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_7 = ((Object_t *)Castclass(L_22, IClientChannelSinkProvider_t1_1766_il2cpp_TypeInfo_var));
		Object_t * L_23 = V_7;
		Object_t * L_24 = V_1;
		NullCheck(L_23);
		InterfaceActionInvoker1< Object_t * >::Invoke(1 /* System.Void System.Runtime.Remoting.Channels.IClientChannelSinkProvider::set_Next(System.Runtime.Remoting.Channels.IClientChannelSinkProvider) */, IClientChannelSinkProvider_t1_1766_il2cpp_TypeInfo_var, L_23, L_24);
		Object_t * L_25 = V_7;
		V_1 = L_25;
		int32_t L_26 = V_5;
		V_5 = ((int32_t)((int32_t)L_26-(int32_t)1));
	}

IL_0093:
	{
		int32_t L_27 = V_5;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0060;
		}
	}
	{
		ChannelData_t1_1022 * L_28 = ___channel;
		NullCheck(L_28);
		String_t* L_29 = (L_28->___Type_1);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m1_1128, L_29, "mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e");
		V_8 = L_30;
		Type_t * L_31 = V_8;
		if (L_31)
		{
			goto IL_00ca;
		}
	}
	{
		ChannelData_t1_1022 * L_32 = ___channel;
		NullCheck(L_32);
		String_t* L_33 = (L_32->___Type_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral1602, L_33, _stringLiteral2144, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_35 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_35, L_34, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_35);
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IChannelSender_t1_1705_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_37 = V_8;
		NullCheck(L_36);
		bool L_38 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_36, L_37);
		V_11 = L_38;
		Type_t * L_39 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IChannelReceiver_t1_1708_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_40 = V_8;
		NullCheck(L_39);
		bool L_41 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_39, L_40);
		V_12 = L_41;
		bool L_42 = V_11;
		if (!L_42)
		{
			goto IL_014b;
		}
	}
	{
		bool L_43 = V_12;
		if (!L_43)
		{
			goto IL_014b;
		}
	}
	{
		TypeU5BU5D_t1_31* L_44 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IDictionary_t1_35_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 0);
		ArrayElementTypeCheck (L_44, L_45);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_44, 0, sizeof(Type_t *))) = (Type_t *)L_45;
		TypeU5BU5D_t1_31* L_46 = L_44;
		Type_t * L_47 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IClientChannelSinkProvider_t1_1766_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 1);
		ArrayElementTypeCheck (L_46, L_47);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_46, 1, sizeof(Type_t *))) = (Type_t *)L_47;
		TypeU5BU5D_t1_31* L_48 = L_46;
		Type_t * L_49 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IServerChannelSinkProvider_t1_1707_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 2);
		ArrayElementTypeCheck (L_48, L_49);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_48, 2, sizeof(Type_t *))) = (Type_t *)L_49;
		V_10 = L_48;
		ObjectU5BU5D_t1_272* L_50 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		ChannelData_t1_1022 * L_51 = ___channel;
		NullCheck(L_51);
		Hashtable_t1_100 * L_52 = ChannelData_get_CustomProperties_m1_9168(L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 0);
		ArrayElementTypeCheck (L_50, L_52);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 0, sizeof(Object_t *))) = (Object_t *)L_52;
		ObjectU5BU5D_t1_272* L_53 = L_50;
		Object_t * L_54 = V_1;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 1);
		ArrayElementTypeCheck (L_53, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, 1, sizeof(Object_t *))) = (Object_t *)L_54;
		ObjectU5BU5D_t1_272* L_55 = L_53;
		Object_t * L_56 = V_0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 2);
		ArrayElementTypeCheck (L_55, L_56);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, 2, sizeof(Object_t *))) = (Object_t *)L_56;
		V_9 = L_55;
		goto IL_01e3;
	}

IL_014b:
	{
		bool L_57 = V_11;
		if (!L_57)
		{
			goto IL_018e;
		}
	}
	{
		TypeU5BU5D_t1_31* L_58 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_59 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IDictionary_t1_35_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 0);
		ArrayElementTypeCheck (L_58, L_59);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_58, 0, sizeof(Type_t *))) = (Type_t *)L_59;
		TypeU5BU5D_t1_31* L_60 = L_58;
		Type_t * L_61 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IClientChannelSinkProvider_t1_1766_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 1);
		ArrayElementTypeCheck (L_60, L_61);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_60, 1, sizeof(Type_t *))) = (Type_t *)L_61;
		V_10 = L_60;
		ObjectU5BU5D_t1_272* L_62 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		ChannelData_t1_1022 * L_63 = ___channel;
		NullCheck(L_63);
		Hashtable_t1_100 * L_64 = ChannelData_get_CustomProperties_m1_9168(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 0);
		ArrayElementTypeCheck (L_62, L_64);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_62, 0, sizeof(Object_t *))) = (Object_t *)L_64;
		ObjectU5BU5D_t1_272* L_65 = L_62;
		Object_t * L_66 = V_1;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 1);
		ArrayElementTypeCheck (L_65, L_66);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_65, 1, sizeof(Object_t *))) = (Object_t *)L_66;
		V_9 = L_65;
		goto IL_01e3;
	}

IL_018e:
	{
		bool L_67 = V_12;
		if (!L_67)
		{
			goto IL_01d1;
		}
	}
	{
		TypeU5BU5D_t1_31* L_68 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_69 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IDictionary_t1_35_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 0);
		ArrayElementTypeCheck (L_68, L_69);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_68, 0, sizeof(Type_t *))) = (Type_t *)L_69;
		TypeU5BU5D_t1_31* L_70 = L_68;
		Type_t * L_71 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IServerChannelSinkProvider_t1_1707_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		ArrayElementTypeCheck (L_70, L_71);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_70, 1, sizeof(Type_t *))) = (Type_t *)L_71;
		V_10 = L_70;
		ObjectU5BU5D_t1_272* L_72 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		ChannelData_t1_1022 * L_73 = ___channel;
		NullCheck(L_73);
		Hashtable_t1_100 * L_74 = ChannelData_get_CustomProperties_m1_9168(L_73, /*hidden argument*/NULL);
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 0);
		ArrayElementTypeCheck (L_72, L_74);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_72, 0, sizeof(Object_t *))) = (Object_t *)L_74;
		ObjectU5BU5D_t1_272* L_75 = L_72;
		Object_t * L_76 = V_0;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, 1);
		ArrayElementTypeCheck (L_75, L_76);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_75, 1, sizeof(Object_t *))) = (Object_t *)L_76;
		V_9 = L_75;
		goto IL_01e3;
	}

IL_01d1:
	{
		Type_t * L_77 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = String_Concat_m1_556(NULL /*static, unused*/, L_77, _stringLiteral2145, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_79 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_79, L_78, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_79);
	}

IL_01e3:
	{
		Type_t * L_80 = V_8;
		TypeU5BU5D_t1_31* L_81 = V_10;
		NullCheck(L_80);
		ConstructorInfo_t1_478 * L_82 = (ConstructorInfo_t1_478 *)VirtFuncInvoker1< ConstructorInfo_t1_478 *, TypeU5BU5D_t1_31* >::Invoke(49 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[]) */, L_80, L_81);
		V_13 = L_82;
		ConstructorInfo_t1_478 * L_83 = V_13;
		if (L_83)
		{
			goto IL_0207;
		}
	}
	{
		Type_t * L_84 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_85 = String_Concat_m1_556(NULL /*static, unused*/, L_84, _stringLiteral2146, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_86 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_86, L_85, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_86);
	}

IL_0207:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t1_478 * L_87 = V_13;
		ObjectU5BU5D_t1_272* L_88 = V_9;
		NullCheck(L_87);
		Object_t * L_89 = ConstructorInfo_Invoke_m1_6777(L_87, L_88, /*hidden argument*/NULL);
		V_14 = ((Object_t *)Castclass(L_89, IChannel_t1_1710_il2cpp_TypeInfo_var));
		goto IL_022b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t1_638_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_021c;
		throw e;
	}

CATCH_021c:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_16 = ((TargetInvocationException_t1_638 *)__exception_local);
			TargetInvocationException_t1_638 * L_90 = V_16;
			NullCheck(L_90);
			Exception_t1_33 * L_91 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, L_90);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_91);
		}

IL_0226:
		{
			goto IL_022b;
		}
	} // end catch (depth: 1)

IL_022b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_92 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_92);
		Object_t * L_93 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_92);
		V_15 = L_93;
		Object_t * L_94 = V_15;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_94, /*hidden argument*/NULL);
	}

IL_023e:
	try
	{ // begin try (depth: 1)
		{
			ChannelData_t1_1022 * L_95 = ___channel;
			NullCheck(L_95);
			String_t* L_96 = (L_95->___DelayLoadAsClientChannel_3);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_97 = String_op_Equality_m1_601(NULL /*static, unused*/, L_96, _stringLiteral2147, /*hidden argument*/NULL);
			if (!L_97)
			{
				goto IL_0271;
			}
		}

IL_0253:
		{
			Object_t * L_98 = V_14;
			if (((Object_t *)IsInst(L_98, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var)))
			{
				goto IL_0271;
			}
		}

IL_025f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_99 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___delayedClientChannels_1;
			Object_t * L_100 = V_14;
			NullCheck(L_99);
			VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_99, L_100);
			goto IL_0278;
		}

IL_0271:
		{
			Object_t * L_101 = V_14;
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ChannelServices_RegisterChannel_m1_8048(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		}

IL_0278:
		{
			IL2CPP_LEAVE(0x285, FINALLY_027d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_027d;
	}

FINALLY_027d:
	{ // begin finally (depth: 1)
		Object_t * L_102 = V_15;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_102, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(637)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(637)
	{
		IL2CPP_JUMP_TBL(0x285, IL_0285)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0285:
	{
		return;
	}
}
// System.Object System.Runtime.Remoting.Channels.ChannelServices::CreateProvider(System.Runtime.Remoting.ProviderData)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* TargetInvocationException_t1_638_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1602;
extern Il2CppCodeGenString* _stringLiteral2144;
extern Il2CppCodeGenString* _stringLiteral2148;
extern Il2CppCodeGenString* _stringLiteral2149;
extern "C" Object_t * ChannelServices_CreateProvider_m1_8051 (Object_t * __this /* static, unused */, ProviderData_t1_1023 * ___prov, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		TargetInvocationException_t1_638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(450);
		_stringLiteral1602 = il2cpp_codegen_string_literal_from_index(1602);
		_stringLiteral2144 = il2cpp_codegen_string_literal_from_index(2144);
		_stringLiteral2148 = il2cpp_codegen_string_literal_from_index(2148);
		_stringLiteral2149 = il2cpp_codegen_string_literal_from_index(2149);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	ObjectU5BU5D_t1_272* V_1 = {0};
	Exception_t1_33 * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ProviderData_t1_1023 * L_0 = ___prov;
		NullCheck(L_0);
		String_t* L_1 = (L_0->___Type_1);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m1_1128, L_1, "mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e");
		V_0 = L_2;
		Type_t * L_3 = V_0;
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		ProviderData_t1_1023 * L_4 = ___prov;
		NullCheck(L_4);
		String_t* L_5 = (L_4->___Type_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral1602, L_5, _stringLiteral2144, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_7 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_002d:
	{
		ObjectU5BU5D_t1_272* L_8 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		ProviderData_t1_1023 * L_9 = ___prov;
		NullCheck(L_9);
		Hashtable_t1_100 * L_10 = (L_9->___CustomProperties_3);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_272* L_11 = L_8;
		ProviderData_t1_1023 * L_12 = ___prov;
		NullCheck(L_12);
		Object_t * L_13 = (L_12->___CustomData_4);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 1, sizeof(Object_t *))) = (Object_t *)L_13;
		V_1 = L_11;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_14 = V_0;
			ObjectU5BU5D_t1_272* L_15 = V_1;
			Object_t * L_16 = Activator_CreateInstance_m1_13035(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
			V_3 = L_16;
			goto IL_00a3;
		}

IL_0053:
		{
			; // IL_0053: leave IL_00a3
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0058;
		throw e;
	}

CATCH_0058:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1_33 *)__exception_local);
			Exception_t1_33 * L_17 = V_2;
			if (!((TargetInvocationException_t1_638 *)IsInstSealed(L_17, TargetInvocationException_t1_638_il2cpp_TypeInfo_var)))
			{
				goto IL_0070;
			}
		}

IL_0064:
		{
			Exception_t1_33 * L_18 = V_2;
			NullCheck(((TargetInvocationException_t1_638 *)CastclassSealed(L_18, TargetInvocationException_t1_638_il2cpp_TypeInfo_var)));
			Exception_t1_33 * L_19 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, ((TargetInvocationException_t1_638 *)CastclassSealed(L_18, TargetInvocationException_t1_638_il2cpp_TypeInfo_var)));
			V_2 = L_19;
		}

IL_0070:
		{
			ObjectU5BU5D_t1_272* L_20 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
			NullCheck(L_20);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
			ArrayElementTypeCheck (L_20, _stringLiteral2148);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral2148;
			ObjectU5BU5D_t1_272* L_21 = L_20;
			Type_t * L_22 = V_0;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
			ArrayElementTypeCheck (L_21, L_22);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 1, sizeof(Object_t *))) = (Object_t *)L_22;
			ObjectU5BU5D_t1_272* L_23 = L_21;
			NullCheck(L_23);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
			ArrayElementTypeCheck (L_23, _stringLiteral2149);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral2149;
			ObjectU5BU5D_t1_272* L_24 = L_23;
			Exception_t1_33 * L_25 = V_2;
			NullCheck(L_25);
			String_t* L_26 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_25);
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
			ArrayElementTypeCheck (L_24, L_26);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 3, sizeof(Object_t *))) = (Object_t *)L_26;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_27 = String_Concat_m1_562(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
			RemotingException_t1_1025 * L_28 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
			RemotingException__ctor_m1_9174(L_28, L_27, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_28);
		}

IL_009e:
		{
			goto IL_00a3;
		}
	} // end catch (depth: 1)

IL_00a3:
	{
		Object_t * L_29 = V_3;
		return L_29;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.ChannelServices::SyncDispatchMessage(System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern "C" Object_t * ChannelServices_SyncDispatchMessage_m1_8052 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ReturnMessage_t1_960 * L_1 = ChannelServices_CheckIncomingMessage_m1_8054(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___msg;
		Object_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		Object_t * L_5 = ChannelServices_CheckReturnMessage_m1_8055(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		CrossContextChannel_t1_872 * L_6 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->____crossContextSink_2;
		Object_t * L_7 = ___msg;
		NullCheck(L_6);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(4 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Contexts.CrossContextChannel::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, L_6, L_7);
		V_0 = L_8;
		Object_t * L_9 = ___msg;
		Object_t * L_10 = V_0;
		Object_t * L_11 = ChannelServices_CheckReturnMessage_m1_8055(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Channels.ChannelServices::AsyncDispatchMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern TypeInfo* ExceptionFilterSink_t1_873_il2cpp_TypeInfo_var;
extern "C" Object_t * ChannelServices_AsyncDispatchMessage_m1_8053 (Object_t * __this /* static, unused */, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		ExceptionFilterSink_t1_873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ReturnMessage_t1_960 * L_1 = ChannelServices_CheckIncomingMessage_m1_8054(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Object_t * L_3 = ___replySink;
		Object_t * L_4 = ___msg;
		Object_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		Object_t * L_6 = ChannelServices_CheckReturnMessage_m1_8055(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.IMessageSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_3, L_6);
		return (Object_t *)NULL;
	}

IL_001d:
	{
		Object_t * L_7 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		bool L_8 = ChannelServices_IsLocalCall_m1_8056(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		bool L_9 = RemotingConfiguration_CustomErrorsEnabled_m1_9139(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0036;
		}
	}
	{
		Object_t * L_10 = ___msg;
		Object_t * L_11 = ___replySink;
		ExceptionFilterSink_t1_873 * L_12 = (ExceptionFilterSink_t1_873 *)il2cpp_codegen_object_new (ExceptionFilterSink_t1_873_il2cpp_TypeInfo_var);
		ExceptionFilterSink__ctor_m1_8059(L_12, L_10, L_11, /*hidden argument*/NULL);
		___replySink = L_12;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		CrossContextChannel_t1_872 * L_13 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->____crossContextSink_2;
		Object_t * L_14 = ___msg;
		Object_t * L_15 = ___replySink;
		NullCheck(L_13);
		Object_t * L_16 = (Object_t *)VirtFuncInvoker2< Object_t *, Object_t *, Object_t * >::Invoke(5 /* System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Contexts.CrossContextChannel::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink) */, L_13, L_14, L_15);
		return L_16;
	}
}
// System.Runtime.Remoting.Messaging.ReturnMessage System.Runtime.Remoting.Channels.ChannelServices::CheckIncomingMessage(System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* IMethodMessage_t1_948_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* ServerIdentity_t1_70_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodCallMessage_t1_949_il2cpp_TypeInfo_var;
extern TypeInfo* ReturnMessage_t1_960_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2150;
extern "C" ReturnMessage_t1_960 * ChannelServices_CheckIncomingMessage_m1_8054 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IMethodMessage_t1_948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		ServerIdentity_t1_70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(614);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		IMethodCallMessage_t1_949_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		ReturnMessage_t1_960_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		_stringLiteral2150 = il2cpp_codegen_string_literal_from_index(2150);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ServerIdentity_t1_70 * V_1 = {0};
	{
		Object_t * L_0 = ___msg;
		V_0 = ((Object_t *)Castclass(L_0, IMethodMessage_t1_948_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_Uri() */, IMethodMessage_t1_948_il2cpp_TypeInfo_var, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		Identity_t1_943 * L_3 = RemotingServices_GetIdentityForUri_m1_9216(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = ((ServerIdentity_t1_70 *)IsInstClass(L_3, ServerIdentity_t1_70_il2cpp_TypeInfo_var));
		ServerIdentity_t1_70 * L_4 = V_1;
		if (L_4)
		{
			goto IL_003f;
		}
	}
	{
		Object_t * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Runtime.Remoting.Messaging.IMethodMessage::get_Uri() */, IMethodMessage_t1_948_il2cpp_TypeInfo_var, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral2150, L_6, /*hidden argument*/NULL);
		RemotingException_t1_1025 * L_8 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_8, L_7, /*hidden argument*/NULL);
		Object_t * L_9 = ___msg;
		ReturnMessage_t1_960 * L_10 = (ReturnMessage_t1_960 *)il2cpp_codegen_object_new (ReturnMessage_t1_960_il2cpp_TypeInfo_var);
		ReturnMessage__ctor_m1_8624(L_10, L_8, ((Object_t *)Castclass(L_9, IMethodCallMessage_t1_949_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_10;
	}

IL_003f:
	{
		Object_t * L_11 = ___msg;
		ServerIdentity_t1_70 * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		RemotingServices_SetMessageTargetIdentity_m1_9235(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return (ReturnMessage_t1_960 *)NULL;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.ChannelServices::CheckReturnMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodCallMessage_t1_949_il2cpp_TypeInfo_var;
extern TypeInfo* MethodResponse_t1_936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2151;
extern "C" Object_t * ChannelServices_CheckReturnMessage_m1_8055 (Object_t * __this /* static, unused */, Object_t * ___callMsg, Object_t * ___retMsg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		IMethodCallMessage_t1_949_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		MethodResponse_t1_936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		_stringLiteral2151 = il2cpp_codegen_string_literal_from_index(2151);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t1_33 * V_1 = {0};
	{
		Object_t * L_0 = ___retMsg;
		V_0 = ((Object_t *)IsInst(L_0, IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		Object_t * L_2 = V_0;
		NullCheck(L_2);
		Exception_t1_33 * L_3 = (Exception_t1_33 *)InterfaceFuncInvoker0< Exception_t1_33 * >::Invoke(0 /* System.Exception System.Runtime.Remoting.Messaging.IMethodReturnMessage::get_Exception() */, IMethodReturnMessage_t1_1718_il2cpp_TypeInfo_var, L_2);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		Object_t * L_4 = ___callMsg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		bool L_5 = ChannelServices_IsLocalCall_m1_8056(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		bool L_6 = RemotingConfiguration_CustomErrorsEnabled_m1_9139(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		Exception_t1_33 * L_7 = (Exception_t1_33 *)il2cpp_codegen_object_new (Exception_t1_33_il2cpp_TypeInfo_var);
		Exception__ctor_m1_1238(L_7, _stringLiteral2151, /*hidden argument*/NULL);
		V_1 = L_7;
		Exception_t1_33 * L_8 = V_1;
		Object_t * L_9 = ___callMsg;
		MethodResponse_t1_936 * L_10 = (MethodResponse_t1_936 *)il2cpp_codegen_object_new (MethodResponse_t1_936_il2cpp_TypeInfo_var);
		MethodResponse__ctor_m1_8512(L_10, L_8, ((Object_t *)Castclass(L_9, IMethodCallMessage_t1_949_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		___retMsg = L_10;
	}

IL_0041:
	{
		Object_t * L_11 = ___retMsg;
		return L_11;
	}
}
// System.Boolean System.Runtime.Remoting.Channels.ChannelServices::IsLocalCall(System.Runtime.Remoting.Messaging.IMessage)
extern "C" bool ChannelServices_IsLocalCall_m1_8056 (Object_t * __this /* static, unused */, Object_t * ___callMsg, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChannelServices::UnregisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelReceiver_t1_1708_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2152;
extern "C" void ChannelServices_UnregisterChannel_m1_8057 (Object_t * __this /* static, unused */, Object_t * ___chnl, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IChannelReceiver_t1_1708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral2152 = il2cpp_codegen_string_literal_from_index(2152);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___chnl;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13268(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_2 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_2);
		V_0 = L_3;
		Object_t * L_4 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_005d;
		}

IL_0024:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_5 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			int32_t L_6 = V_1;
			NullCheck(L_5);
			Object_t * L_7 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_5, L_6);
			Object_t * L_8 = ___chnl;
			if ((!(((Object_t*)(Object_t *)L_7) == ((Object_t*)(Object_t *)L_8))))
			{
				goto IL_0059;
			}
		}

IL_0035:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_9 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			int32_t L_10 = V_1;
			NullCheck(L_9);
			VirtActionInvoker1< int32_t >::Invoke(43 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_9, L_10);
			Object_t * L_11 = ___chnl;
			V_2 = ((Object_t *)IsInst(L_11, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var));
			Object_t * L_12 = V_2;
			if (!L_12)
			{
				goto IL_0054;
			}
		}

IL_004d:
		{
			Object_t * L_13 = V_2;
			NullCheck(L_13);
			InterfaceActionInvoker1< Object_t * >::Invoke(3 /* System.Void System.Runtime.Remoting.Channels.IChannelReceiver::StopListening(System.Object) */, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var, L_13, NULL);
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x84, FINALLY_007d);
		}

IL_0059:
		{
			int32_t L_14 = V_1;
			V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_005d:
		{
			int32_t L_15 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_16 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_16);
			int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_16);
			if ((((int32_t)L_15) < ((int32_t)L_17)))
			{
				goto IL_0024;
			}
		}

IL_006d:
		{
			RemotingException_t1_1025 * L_18 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
			RemotingException__ctor_m1_9174(L_18, _stringLiteral2152, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x84, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		Object_t * L_19 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(125)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0084:
	{
		return;
	}
}
// System.Object[] System.Runtime.Remoting.Channels.ChannelServices::GetCurrentChannelInfo()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IChannelReceiver_t1_1708_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" ObjectU5BU5D_t1_272* ChannelServices_GetCurrentChannelInfo_m1_8058 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IChannelReceiver_t1_1708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t1_170 * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t1_170 * L_0 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ArrayList_t1_170 * L_1 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(29 /* System.Object System.Collections.ArrayList::get_SyncRoot() */, L_1);
		V_1 = L_2;
		Object_t * L_3 = V_1;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
			ArrayList_t1_170 * L_4 = ((ChannelServices_t1_871_StaticFields*)ChannelServices_t1_871_il2cpp_TypeInfo_var->static_fields)->___registeredChannels_0;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_4);
			V_3 = L_5;
		}

IL_0022:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0056;
			}

IL_0027:
			{
				Object_t * L_6 = V_3;
				NullCheck(L_6);
				Object_t * L_7 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_6);
				V_2 = L_7;
				Object_t * L_8 = V_2;
				V_4 = ((Object_t *)IsInst(L_8, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var));
				Object_t * L_9 = V_4;
				if (!L_9)
				{
					goto IL_0056;
				}
			}

IL_003d:
			{
				Object_t * L_10 = V_4;
				NullCheck(L_10);
				Object_t * L_11 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Runtime.Remoting.Channels.IChannelReceiver::get_ChannelData() */, IChannelReceiver_t1_1708_il2cpp_TypeInfo_var, L_10);
				V_5 = L_11;
				Object_t * L_12 = V_5;
				if (!L_12)
				{
					goto IL_0056;
				}
			}

IL_004d:
			{
				ArrayList_t1_170 * L_13 = V_0;
				Object_t * L_14 = V_5;
				NullCheck(L_13);
				VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_13, L_14);
			}

IL_0056:
			{
				Object_t * L_15 = V_3;
				NullCheck(L_15);
				bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_15);
				if (L_16)
				{
					goto IL_0027;
				}
			}

IL_0061:
			{
				IL2CPP_LEAVE(0x7B, FINALLY_0066);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_0066;
		}

FINALLY_0066:
		{ // begin finally (depth: 2)
			{
				Object_t * L_17 = V_3;
				V_6 = ((Object_t *)IsInst(L_17, IDisposable_t1_1035_il2cpp_TypeInfo_var));
				Object_t * L_18 = V_6;
				if (L_18)
				{
					goto IL_0073;
				}
			}

IL_0072:
			{
				IL2CPP_END_FINALLY(102)
			}

IL_0073:
			{
				Object_t * L_19 = V_6;
				NullCheck(L_19);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_19);
				IL2CPP_END_FINALLY(102)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(102)
		{
			IL2CPP_JUMP_TBL(0x7B, IL_007b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		Object_t * L_20 = V_1;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(128)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0087:
	{
		ArrayList_t1_170 * L_21 = V_0;
		NullCheck(L_21);
		ObjectU5BU5D_t1_272* L_22 = (ObjectU5BU5D_t1_272*)VirtFuncInvoker0< ObjectU5BU5D_t1_272* >::Invoke(62 /* System.Object[] System.Collections.ArrayList::ToArray() */, L_21);
		return L_22;
	}
}
// System.Void System.Runtime.Remoting.Channels.ExceptionFilterSink::.ctor(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void ExceptionFilterSink__ctor_m1_8059 (ExceptionFilterSink_t1_873 * __this, Object_t * ___call, Object_t * ___next, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___call;
		__this->____call_1 = L_0;
		Object_t * L_1 = ___next;
		__this->____next_0 = L_1;
		return;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.ExceptionFilterSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern "C" Object_t * ExceptionFilterSink_SyncProcessMessage_m1_8060 (ExceptionFilterSink_t1_873 * __this, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->____next_0);
		Object_t * L_1 = (__this->____call_1);
		Object_t * L_2 = ___msg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		Object_t * L_3 = ChannelServices_CheckReturnMessage_m1_8055(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.IMessageSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_0, L_3);
		return L_4;
	}
}
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Channels.ExceptionFilterSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" Object_t * ExceptionFilterSink_AsyncProcessMessage_m1_8061 (ExceptionFilterSink_t1_873 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		InvalidOperationException_t1_1559 * L_0 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14170(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ExceptionFilterSink::get_NextSink()
extern "C" Object_t * ExceptionFilterSink_get_NextSink_m1_8062 (ExceptionFilterSink_t1_873 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->____next_0);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Channels.ChanelSinkStackEntry::.ctor(System.Runtime.Remoting.Channels.IChannelSinkBase,System.Object,System.Runtime.Remoting.Channels.ChanelSinkStackEntry)
extern "C" void ChanelSinkStackEntry__ctor_m1_8063 (ChanelSinkStackEntry_t1_876 * __this, Object_t * ___sink, Object_t * ___state, ChanelSinkStackEntry_t1_876 * ___next, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___sink;
		__this->___Sink_0 = L_0;
		Object_t * L_1 = ___state;
		__this->___State_1 = L_1;
		ChanelSinkStackEntry_t1_876 * L_2 = ___next;
		__this->___Next_2 = L_2;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::.ctor()
extern "C" void ClientChannelSinkStack__ctor_m1_8064 (ClientChannelSinkStack_t1_877 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::.ctor(System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void ClientChannelSinkStack__ctor_m1_8065 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___replySink, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___replySink;
		__this->____replySink_0 = L_0;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::AsyncProcessResponse(System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream)
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* IClientChannelSink_t1_1712_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2153;
extern "C" void ClientChannelSinkStack_AsyncProcessResponse_m1_8066 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___headers, Stream_t1_405 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		IClientChannelSink_t1_1712_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		_stringLiteral2153 = il2cpp_codegen_string_literal_from_index(2153);
		s_Il2CppMethodIntialized = true;
	}
	ChanelSinkStackEntry_t1_876 * V_0 = {0};
	{
		ChanelSinkStackEntry_t1_876 * L_0 = (__this->____sinkStack_1);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RemotingException_t1_1025 * L_1 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_1, _stringLiteral2153, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ChanelSinkStackEntry_t1_876 * L_2 = (__this->____sinkStack_1);
		V_0 = L_2;
		ChanelSinkStackEntry_t1_876 * L_3 = (__this->____sinkStack_1);
		NullCheck(L_3);
		ChanelSinkStackEntry_t1_876 * L_4 = (L_3->___Next_2);
		__this->____sinkStack_1 = L_4;
		ChanelSinkStackEntry_t1_876 * L_5 = V_0;
		NullCheck(L_5);
		Object_t * L_6 = (L_5->___Sink_0);
		ChanelSinkStackEntry_t1_876 * L_7 = V_0;
		NullCheck(L_7);
		Object_t * L_8 = (L_7->___State_1);
		Object_t * L_9 = ___headers;
		Stream_t1_405 * L_10 = ___stream;
		NullCheck(((Object_t *)Castclass(L_6, IClientChannelSink_t1_1712_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker4< Object_t *, Object_t *, Object_t *, Stream_t1_405 * >::Invoke(2 /* System.Void System.Runtime.Remoting.Channels.IClientChannelSink::AsyncProcessResponse(System.Runtime.Remoting.Channels.IClientResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream) */, IClientChannelSink_t1_1712_il2cpp_TypeInfo_var, ((Object_t *)Castclass(L_6, IClientChannelSink_t1_1712_il2cpp_TypeInfo_var)), __this, L_8, L_9, L_10);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::DispatchException(System.Exception)
extern TypeInfo* ReturnMessage_t1_960_il2cpp_TypeInfo_var;
extern "C" void ClientChannelSinkStack_DispatchException_m1_8067 (ClientChannelSinkStack_t1_877 * __this, Exception_t1_33 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReturnMessage_t1_960_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1_33 * L_0 = ___e;
		ReturnMessage_t1_960 * L_1 = (ReturnMessage_t1_960 *)il2cpp_codegen_object_new (ReturnMessage_t1_960_il2cpp_TypeInfo_var);
		ReturnMessage__ctor_m1_8624(L_1, L_0, (Object_t *)NULL, /*hidden argument*/NULL);
		VirtActionInvoker1< Object_t * >::Invoke(8 /* System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::DispatchReplyMessage(System.Runtime.Remoting.Messaging.IMessage) */, __this, L_1);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::DispatchReplyMessage(System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern "C" void ClientChannelSinkStack_DispatchReplyMessage_m1_8068 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->____replySink_0);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Object_t * L_1 = (__this->____replySink_0);
		Object_t * L_2 = ___msg;
		NullCheck(L_1);
		InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.IMessageSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_1, L_2);
	}

IL_0018:
	{
		return;
	}
}
// System.Object System.Runtime.Remoting.Channels.ClientChannelSinkStack::Pop(System.Runtime.Remoting.Channels.IClientChannelSink)
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2154;
extern "C" Object_t * ClientChannelSinkStack_Pop_m1_8069 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___sink, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral2154 = il2cpp_codegen_string_literal_from_index(2154);
		s_Il2CppMethodIntialized = true;
	}
	ChanelSinkStackEntry_t1_876 * V_0 = {0};
	{
		goto IL_0030;
	}

IL_0005:
	{
		ChanelSinkStackEntry_t1_876 * L_0 = (__this->____sinkStack_1);
		V_0 = L_0;
		ChanelSinkStackEntry_t1_876 * L_1 = (__this->____sinkStack_1);
		NullCheck(L_1);
		ChanelSinkStackEntry_t1_876 * L_2 = (L_1->___Next_2);
		__this->____sinkStack_1 = L_2;
		ChanelSinkStackEntry_t1_876 * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = (L_3->___Sink_0);
		Object_t * L_5 = ___sink;
		if ((!(((Object_t*)(Object_t *)L_4) == ((Object_t*)(Object_t *)L_5))))
		{
			goto IL_0030;
		}
	}
	{
		ChanelSinkStackEntry_t1_876 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = (L_6->___State_1);
		return L_7;
	}

IL_0030:
	{
		ChanelSinkStackEntry_t1_876 * L_8 = (__this->____sinkStack_1);
		if (L_8)
		{
			goto IL_0005;
		}
	}
	{
		RemotingException_t1_1025 * L_9 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_9, _stringLiteral2154, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}
}
// System.Void System.Runtime.Remoting.Channels.ClientChannelSinkStack::Push(System.Runtime.Remoting.Channels.IClientChannelSink,System.Object)
extern TypeInfo* ChanelSinkStackEntry_t1_876_il2cpp_TypeInfo_var;
extern "C" void ClientChannelSinkStack_Push_m1_8070 (ClientChannelSinkStack_t1_877 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChanelSinkStackEntry_t1_876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___sink;
		Object_t * L_1 = ___state;
		ChanelSinkStackEntry_t1_876 * L_2 = (__this->____sinkStack_1);
		ChanelSinkStackEntry_t1_876 * L_3 = (ChanelSinkStackEntry_t1_876 *)il2cpp_codegen_object_new (ChanelSinkStackEntry_t1_876_il2cpp_TypeInfo_var);
		ChanelSinkStackEntry__ctor_m1_8063(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->____sinkStack_1 = L_3;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern "C" void CrossAppDomainData__ctor_m1_8071 (CrossAppDomainData_t1_878 * __this, int32_t ___domainId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = 0;
		Object_t * L_1 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_0);
		__this->____ContextID_0 = L_1;
		int32_t L_2 = ___domainId;
		__this->____DomainID_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		String_t* L_3 = RemotingConfiguration_get_ProcessId_m1_9112(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->____processGuid_2 = L_3;
		return;
	}
}
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainData::get_DomainID()
extern "C" int32_t CrossAppDomainData_get_DomainID_m1_8072 (CrossAppDomainData_t1_878 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____DomainID_1);
		return L_0;
	}
}
// System.String System.Runtime.Remoting.Channels.CrossAppDomainData::get_ProcessID()
extern "C" String_t* CrossAppDomainData_get_ProcessID_m1_8073 (CrossAppDomainData_t1_878 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____processGuid_2);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.ctor()
extern "C" void CrossAppDomainChannel__ctor_m1_8074 (CrossAppDomainChannel_t1_879 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.cctor()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var;
extern "C" void CrossAppDomainChannel__cctor_m1_8075 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1_0(L_0, /*hidden argument*/NULL);
		((CrossAppDomainChannel_t1_879_StaticFields*)CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var->static_fields)->___s_lock_2 = L_0;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::RegisterCrossAppDomainChannel()
extern TypeInfo* CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var;
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m1_8076 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	CrossAppDomainChannel_t1_879 * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var);
		Object_t * L_0 = ((CrossAppDomainChannel_t1_879_StaticFields*)CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var->static_fields)->___s_lock_2;
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		CrossAppDomainChannel_t1_879 * L_2 = (CrossAppDomainChannel_t1_879 *)il2cpp_codegen_object_new (CrossAppDomainChannel_t1_879_il2cpp_TypeInfo_var);
		CrossAppDomainChannel__ctor_m1_8074(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		CrossAppDomainChannel_t1_879 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		ChannelServices_RegisterChannel_m1_8048(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x24, FINALLY_001d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_001d;
	}

FINALLY_001d:
	{ // begin finally (depth: 1)
		Object_t * L_4 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(29)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(29)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0024:
	{
		return;
	}
}
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelName()
extern Il2CppCodeGenString* _stringLiteral2155;
extern "C" String_t* CrossAppDomainChannel_get_ChannelName_m1_8077 (CrossAppDomainChannel_t1_879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2155 = il2cpp_codegen_string_literal_from_index(2155);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral2155;
	}
}
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelPriority()
extern "C" int32_t CrossAppDomainChannel_get_ChannelPriority_m1_8078 (CrossAppDomainChannel_t1_879 * __this, const MethodInfo* method)
{
	{
		return ((int32_t)100);
	}
}
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::Parse(System.String,System.String&)
extern "C" String_t* CrossAppDomainChannel_Parse_m1_8079 (CrossAppDomainChannel_t1_879 * __this, String_t* ___url, String_t** ___objectURI, const MethodInfo* method)
{
	{
		String_t** L_0 = ___objectURI;
		String_t* L_1 = ___url;
		*((Object_t **)(L_0)) = (Object_t *)L_1;
		return (String_t*)NULL;
	}
}
// System.Object System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelData()
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainData_t1_878_il2cpp_TypeInfo_var;
extern "C" Object_t * CrossAppDomainChannel_get_ChannelData_m1_8080 (CrossAppDomainChannel_t1_879 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		CrossAppDomainData_t1_878_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		int32_t L_0 = Thread_GetDomainID_m1_12814(NULL /*static, unused*/, /*hidden argument*/NULL);
		CrossAppDomainData_t1_878 * L_1 = (CrossAppDomainData_t1_878 *)il2cpp_codegen_object_new (CrossAppDomainData_t1_878_il2cpp_TypeInfo_var);
		CrossAppDomainData__ctor_m1_8071(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String[] System.Runtime.Remoting.Channels.CrossAppDomainChannel::GetUrlsForUri(System.String)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2156;
extern "C" StringU5BU5D_t1_238* CrossAppDomainChannel_GetUrlsForUri_m1_8081 (CrossAppDomainChannel_t1_879 * __this, String_t* ___objectURI, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2156 = il2cpp_codegen_string_literal_from_index(2156);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, _stringLiteral2156, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StartListening(System.Object)
extern "C" void CrossAppDomainChannel_StartListening_m1_8082 (CrossAppDomainChannel_t1_879 * __this, Object_t * ___data, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StopListening(System.Object)
extern "C" void CrossAppDomainChannel_StopListening_m1_8083 (CrossAppDomainChannel_t1_879 * __this, Object_t * ___data, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.CrossAppDomainChannel::CreateMessageSink(System.String,System.Object,System.String&)
extern TypeInfo* CrossAppDomainData_t1_878_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2155;
extern Il2CppCodeGenString* _stringLiteral2157;
extern "C" Object_t * CrossAppDomainChannel_CreateMessageSink_m1_8084 (CrossAppDomainChannel_t1_879 * __this, String_t* ___url, Object_t * ___data, String_t** ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossAppDomainData_t1_878_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(609);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral2155 = il2cpp_codegen_string_literal_from_index(2155);
		_stringLiteral2157 = il2cpp_codegen_string_literal_from_index(2157);
		s_Il2CppMethodIntialized = true;
	}
	CrossAppDomainData_t1_878 * V_0 = {0};
	{
		String_t** L_0 = ___uri;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		Object_t * L_1 = ___data;
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		Object_t * L_2 = ___data;
		V_0 = ((CrossAppDomainData_t1_878 *)IsInstClass(L_2, CrossAppDomainData_t1_878_il2cpp_TypeInfo_var));
		CrossAppDomainData_t1_878 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		CrossAppDomainData_t1_878 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = CrossAppDomainData_get_ProcessID_m1_8073(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RemotingConfiguration_t1_1020_il2cpp_TypeInfo_var);
		String_t* L_6 = RemotingConfiguration_get_ProcessId_m1_9112(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1_601(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0037;
		}
	}
	{
		CrossAppDomainData_t1_878 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = CrossAppDomainData_get_DomainID_m1_8072(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
		CrossAppDomainSink_t1_882 * L_10 = CrossAppDomainSink_GetSink_m1_8087(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0037:
	{
		String_t* L_11 = ___url;
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		String_t* L_12 = ___url;
		NullCheck(L_12);
		bool L_13 = String_StartsWith_m1_531(L_12, _stringLiteral2155, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0058;
		}
	}
	{
		NotSupportedException_t1_1583 * L_14 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_14, _stringLiteral2157, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
	}

IL_0058:
	{
		return (Object_t *)NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::.ctor(System.Int32)
extern "C" void CrossAppDomainSink__ctor_m1_8085 (CrossAppDomainSink_t1_882 * __this, int32_t ___domainID, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___domainID;
		__this->____domainID_2 = L_0;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::.cctor()
extern const Il2CppType* CrossAppDomainSink_t1_882_0_0_0_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2158;
extern "C" void CrossAppDomainSink__cctor_m1_8086 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossAppDomainSink_t1_882_0_0_0_var = il2cpp_codegen_type_from_index(645);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral2158 = il2cpp_codegen_string_literal_from_index(2158);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___s_sinks_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(CrossAppDomainSink_t1_882_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		MethodInfo_t * L_2 = (MethodInfo_t *)VirtFuncInvoker2< MethodInfo_t *, String_t*, int32_t >::Invoke(81 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags) */, L_1, _stringLiteral2158, ((int32_t)40));
		((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___processMessageMethod_1 = L_2;
		return;
	}
}
// System.Runtime.Remoting.Channels.CrossAppDomainSink System.Runtime.Remoting.Channels.CrossAppDomainSink::GetSink(System.Int32)
extern TypeInfo* CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" CrossAppDomainSink_t1_882 * CrossAppDomainSink_GetSink_m1_8087 (Object_t * __this /* static, unused */, int32_t ___domainID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	CrossAppDomainSink_t1_882 * V_1 = {0};
	CrossAppDomainSink_t1_882 * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_0 = ((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___s_sinks_0;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(25 /* System.Object System.Collections.Hashtable::get_SyncRoot() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_3 = ((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___s_sinks_0;
			int32_t L_4 = ___domainID;
			int32_t L_5 = L_4;
			Object_t * L_6 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_5);
			NullCheck(L_3);
			bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(38 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_3, L_6);
			if (!L_7)
			{
				goto IL_0041;
			}
		}

IL_0026:
		{
			IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_8 = ((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___s_sinks_0;
			int32_t L_9 = ___domainID;
			int32_t L_10 = L_9;
			Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
			NullCheck(L_8);
			Object_t * L_12 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_8, L_11);
			V_2 = ((CrossAppDomainSink_t1_882 *)CastclassClass(L_12, CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var));
			IL2CPP_LEAVE(0x6C, FINALLY_0065);
		}

IL_0041:
		{
			int32_t L_13 = ___domainID;
			CrossAppDomainSink_t1_882 * L_14 = (CrossAppDomainSink_t1_882 *)il2cpp_codegen_object_new (CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
			CrossAppDomainSink__ctor_m1_8085(L_14, L_13, /*hidden argument*/NULL);
			V_1 = L_14;
			IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_15 = ((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___s_sinks_0;
			int32_t L_16 = ___domainID;
			int32_t L_17 = L_16;
			Object_t * L_18 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_17);
			CrossAppDomainSink_t1_882 * L_19 = V_1;
			NullCheck(L_15);
			VirtActionInvoker2< Object_t *, Object_t * >::Invoke(31 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_15, L_18, L_19);
			CrossAppDomainSink_t1_882 * L_20 = V_1;
			V_2 = L_20;
			IL2CPP_LEAVE(0x6C, FINALLY_0065);
		}

IL_0060:
		{
			; // IL_0060: leave IL_006c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		Object_t * L_21 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_006c:
	{
		CrossAppDomainSink_t1_882 * L_22 = V_2;
		return L_22;
	}
}
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainSink::get_TargetDomainId()
extern "C" int32_t CrossAppDomainSink_get_TargetDomainId_m1_8088 (CrossAppDomainSink_t1_882 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->____domainID_2);
		return L_0;
	}
}
// System.Runtime.Remoting.Channels.CrossAppDomainSink/ProcessMessageRes System.Runtime.Remoting.Channels.CrossAppDomainSink::ProcessMessageInDomain(System.Byte[],System.Runtime.Remoting.Messaging.CADMethodCallMessage)
extern TypeInfo* ProcessMessageRes_t1_880_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* ErrorMessage_t1_938_il2cpp_TypeInfo_var;
extern TypeInfo* MethodResponse_t1_936_il2cpp_TypeInfo_var;
extern "C" ProcessMessageRes_t1_880  CrossAppDomainSink_ProcessMessageInDomain_m1_8089 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___arrRequest, CADMethodCallMessage_t1_925 * ___cadMsg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProcessMessageRes_t1_880_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(646);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		ErrorMessage_t1_938_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		MethodResponse_t1_936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		s_Il2CppMethodIntialized = true;
	}
	ProcessMessageRes_t1_880  V_0 = {0};
	Exception_t1_33 * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Initobj (ProcessMessageRes_t1_880_il2cpp_TypeInfo_var, (&V_0));
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		AppDomain_t1_1403 * L_0 = AppDomain_get_CurrentDomain_m1_13075(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_1 = ___arrRequest;
		CADMethodCallMessage_t1_925 * L_2 = ___cadMsg;
		ByteU5BU5D_t1_109** L_3 = &((&V_0)->___arrResponse_0);
		CADMethodReturnMessage_t1_881 ** L_4 = &((&V_0)->___cadMrm_1);
		NullCheck(L_0);
		AppDomain_ProcessMessageInDomain_m1_13167(L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		goto IL_004b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0027;
		throw e;
	}

CATCH_0027:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1_33 *)__exception_local);
		Exception_t1_33 * L_5 = V_1;
		ErrorMessage_t1_938 * L_6 = (ErrorMessage_t1_938 *)il2cpp_codegen_object_new (ErrorMessage_t1_938_il2cpp_TypeInfo_var);
		ErrorMessage__ctor_m1_8378(L_6, /*hidden argument*/NULL);
		MethodResponse_t1_936 * L_7 = (MethodResponse_t1_936 *)il2cpp_codegen_object_new (MethodResponse_t1_936_il2cpp_TypeInfo_var);
		MethodResponse__ctor_m1_8512(L_7, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Object_t * L_8 = V_2;
		MemoryStream_t1_433 * L_9 = CADSerializer_SerializeMessage_m1_8096(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(32 /* System.Byte[] System.IO.MemoryStream::GetBuffer() */, L_9);
		(&V_0)->___arrResponse_0 = L_10;
		goto IL_004b;
	} // end catch (depth: 1)

IL_004b:
	{
		ProcessMessageRes_t1_880  L_11 = V_0;
		return L_11;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.CrossAppDomainSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* ProcessMessageRes_t1_880_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* IMethodCallMessage_t1_949_il2cpp_TypeInfo_var;
extern TypeInfo* MethodResponse_t1_936_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* ReturnMessage_t1_960_il2cpp_TypeInfo_var;
extern "C" Object_t * CrossAppDomainSink_SyncProcessMessage_m1_8090 (CrossAppDomainSink_t1_882 * __this, Object_t * ___msgRequest, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		ProcessMessageRes_t1_880_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(646);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		IMethodCallMessage_t1_949_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		MethodResponse_t1_936_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		ReturnMessage_t1_960_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(606);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	CADMethodReturnMessage_t1_881 * V_3 = {0};
	CADMethodCallMessage_t1_925 * V_4 = {0};
	MemoryStream_t1_433 * V_5 = {0};
	Context_t1_891 * V_6 = {0};
	ProcessMessageRes_t1_880  V_7 = {0};
	MemoryStream_t1_433 * V_8 = {0};
	Exception_t1_33 * V_9 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Object_t *)NULL;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (ByteU5BU5D_t1_109*)NULL;
			V_2 = (ByteU5BU5D_t1_109*)NULL;
			V_3 = (CADMethodReturnMessage_t1_881 *)NULL;
			Object_t * L_0 = ___msgRequest;
			CADMethodCallMessage_t1_925 * L_1 = CADMethodCallMessage_Create_m1_8309(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			V_4 = L_1;
			CADMethodCallMessage_t1_925 * L_2 = V_4;
			if (L_2)
			{
				goto IL_0027;
			}
		}

IL_0017:
		{
			Object_t * L_3 = ___msgRequest;
			MemoryStream_t1_433 * L_4 = CADSerializer_SerializeMessage_m1_8096(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
			V_5 = L_4;
			MemoryStream_t1_433 * L_5 = V_5;
			NullCheck(L_5);
			ByteU5BU5D_t1_109* L_6 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(32 /* System.Byte[] System.IO.MemoryStream::GetBuffer() */, L_5);
			V_2 = L_6;
		}

IL_0027:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
			Context_t1_891 * L_7 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_6 = L_7;
		}

IL_002e:
		try
		{ // begin try (depth: 2)
			int32_t L_8 = (__this->____domainID_2);
			IL2CPP_RUNTIME_CLASS_INIT(CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var);
			MethodInfo_t * L_9 = ((CrossAppDomainSink_t1_882_StaticFields*)CrossAppDomainSink_t1_882_il2cpp_TypeInfo_var->static_fields)->___processMessageMethod_1;
			ObjectU5BU5D_t1_272* L_10 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
			ByteU5BU5D_t1_109* L_11 = V_2;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
			ArrayElementTypeCheck (L_10, L_11);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 0, sizeof(Object_t *))) = (Object_t *)L_11;
			ObjectU5BU5D_t1_272* L_12 = L_10;
			CADMethodCallMessage_t1_925 * L_13 = V_4;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
			ArrayElementTypeCheck (L_12, L_13);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 1, sizeof(Object_t *))) = (Object_t *)L_13;
			Object_t * L_14 = AppDomain_InvokeInDomainByID_m1_13145(NULL /*static, unused*/, L_8, L_9, NULL, L_12, /*hidden argument*/NULL);
			V_7 = ((*(ProcessMessageRes_t1_880 *)((ProcessMessageRes_t1_880 *)UnBox (L_14, ProcessMessageRes_t1_880_il2cpp_TypeInfo_var))));
			ByteU5BU5D_t1_109* L_15 = ((&V_7)->___arrResponse_0);
			V_1 = L_15;
			CADMethodReturnMessage_t1_881 * L_16 = ((&V_7)->___cadMrm_1);
			V_3 = L_16;
			IL2CPP_LEAVE(0x73, FINALLY_006a);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_006a;
		}

FINALLY_006a:
		{ // begin finally (depth: 2)
			Context_t1_891 * L_17 = V_6;
			AppDomain_InternalSetContext_m1_13140(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(106)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(106)
		{
			IL2CPP_JUMP_TBL(0x73, IL_0073)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_0073:
		{
			ByteU5BU5D_t1_109* L_18 = V_1;
			if (!L_18)
			{
				goto IL_0094;
			}
		}

IL_0079:
		{
			ByteU5BU5D_t1_109* L_19 = V_1;
			MemoryStream_t1_433 * L_20 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
			MemoryStream__ctor_m1_5046(L_20, L_19, /*hidden argument*/NULL);
			V_8 = L_20;
			MemoryStream_t1_433 * L_21 = V_8;
			Object_t * L_22 = ___msgRequest;
			Object_t * L_23 = CADSerializer_DeserializeMessage_m1_8095(NULL /*static, unused*/, L_21, ((Object_t *)IsInst(L_22, IMethodCallMessage_t1_949_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			V_0 = L_23;
			goto IL_00a1;
		}

IL_0094:
		{
			Object_t * L_24 = ___msgRequest;
			CADMethodReturnMessage_t1_881 * L_25 = V_3;
			MethodResponse_t1_936 * L_26 = (MethodResponse_t1_936 *)il2cpp_codegen_object_new (MethodResponse_t1_936_il2cpp_TypeInfo_var);
			MethodResponse__ctor_m1_8514(L_26, ((Object_t *)IsInst(L_24, IMethodCallMessage_t1_949_il2cpp_TypeInfo_var)), L_25, /*hidden argument*/NULL);
			V_0 = L_26;
		}

IL_00a1:
		{
			goto IL_00c6;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a6;
		throw e;
	}

CATCH_00a6:
	{ // begin catch(System.Exception)
		{
			V_9 = ((Exception_t1_33 *)__exception_local);
		}

IL_00a8:
		try
		{ // begin try (depth: 2)
			Exception_t1_33 * L_27 = V_9;
			Object_t * L_28 = ___msgRequest;
			ReturnMessage_t1_960 * L_29 = (ReturnMessage_t1_960 *)il2cpp_codegen_object_new (ReturnMessage_t1_960_il2cpp_TypeInfo_var);
			ReturnMessage__ctor_m1_8624(L_29, L_27, ((Object_t *)IsInst(L_28, IMethodCallMessage_t1_949_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			V_0 = L_29;
			goto IL_00c1;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1_33 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00bb;
			throw e;
		}

CATCH_00bb:
		{ // begin catch(System.Exception)
			goto IL_00c1;
		} // end catch (depth: 2)

IL_00c1:
		{
			goto IL_00c6;
		}
	} // end catch (depth: 1)

IL_00c6:
	{
		Object_t * L_30 = V_0;
		return L_30;
	}
}
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Channels.CrossAppDomainSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern TypeInfo* AsyncRequest_t1_884_il2cpp_TypeInfo_var;
extern TypeInfo* WaitCallback_t1_1629_il2cpp_TypeInfo_var;
extern const MethodInfo* CrossAppDomainSink_SendAsyncMessage_m1_8092_MethodInfo_var;
extern "C" Object_t * CrossAppDomainSink_AsyncProcessMessage_m1_8091 (CrossAppDomainSink_t1_882 * __this, Object_t * ___reqMsg, Object_t * ___replySink, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AsyncRequest_t1_884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		WaitCallback_t1_1629_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		CrossAppDomainSink_SendAsyncMessage_m1_8092_MethodInfo_var = il2cpp_codegen_method_info_from_index(36);
		s_Il2CppMethodIntialized = true;
	}
	AsyncRequest_t1_884 * V_0 = {0};
	{
		Object_t * L_0 = ___reqMsg;
		Object_t * L_1 = ___replySink;
		AsyncRequest_t1_884 * L_2 = (AsyncRequest_t1_884 *)il2cpp_codegen_object_new (AsyncRequest_t1_884_il2cpp_TypeInfo_var);
		AsyncRequest__ctor_m1_8099(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		IntPtr_t L_3 = { (void*)CrossAppDomainSink_SendAsyncMessage_m1_8092_MethodInfo_var };
		WaitCallback_t1_1629 * L_4 = (WaitCallback_t1_1629 *)il2cpp_codegen_object_new (WaitCallback_t1_1629_il2cpp_TypeInfo_var);
		WaitCallback__ctor_m1_14839(L_4, __this, L_3, /*hidden argument*/NULL);
		AsyncRequest_t1_884 * L_5 = V_0;
		ThreadPool_QueueUserWorkItem_m1_12926(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainSink::SendAsyncMessage(System.Object)
extern TypeInfo* AsyncRequest_t1_884_il2cpp_TypeInfo_var;
extern TypeInfo* IMessageSink_t1_874_il2cpp_TypeInfo_var;
extern "C" void CrossAppDomainSink_SendAsyncMessage_m1_8092 (CrossAppDomainSink_t1_882 * __this, Object_t * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AsyncRequest_t1_884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		IMessageSink_t1_874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(601);
		s_Il2CppMethodIntialized = true;
	}
	AsyncRequest_t1_884 * V_0 = {0};
	Object_t * V_1 = {0};
	{
		Object_t * L_0 = ___data;
		V_0 = ((AsyncRequest_t1_884 *)CastclassClass(L_0, AsyncRequest_t1_884_il2cpp_TypeInfo_var));
		AsyncRequest_t1_884 * L_1 = V_0;
		NullCheck(L_1);
		Object_t * L_2 = (L_1->___MsgRequest_1);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(7 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.CrossAppDomainSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, __this, L_2);
		V_1 = L_3;
		AsyncRequest_t1_884 * L_4 = V_0;
		NullCheck(L_4);
		Object_t * L_5 = (L_4->___ReplySink_0);
		Object_t * L_6 = V_1;
		NullCheck(L_5);
		InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.IMessageSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage) */, IMessageSink_t1_874_il2cpp_TypeInfo_var, L_5, L_6);
		return;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.CrossAppDomainSink::get_NextSink()
extern "C" Object_t * CrossAppDomainSink_get_NextSink_m1_8093 (CrossAppDomainSink_t1_882 * __this, const MethodInfo* method)
{
	{
		return (Object_t *)NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.CADSerializer::.ctor()
extern "C" void CADSerializer__ctor_m1_8094 (CADSerializer_t1_883 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.CADSerializer::DeserializeMessage(System.IO.MemoryStream,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern TypeInfo* BinaryFormatter_t1_1028_il2cpp_TypeInfo_var;
extern TypeInfo* IMessage_t1_875_il2cpp_TypeInfo_var;
extern "C" Object_t * CADSerializer_DeserializeMessage_m1_8095 (Object_t * __this /* static, unused */, MemoryStream_t1_433 * ___mem, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BinaryFormatter_t1_1028_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		IMessage_t1_875_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(650);
		s_Il2CppMethodIntialized = true;
	}
	BinaryFormatter_t1_1028 * V_0 = {0};
	{
		BinaryFormatter_t1_1028 * L_0 = (BinaryFormatter_t1_1028 *)il2cpp_codegen_object_new (BinaryFormatter_t1_1028_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1_9338(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		BinaryFormatter_t1_1028 * L_1 = V_0;
		NullCheck(L_1);
		BinaryFormatter_set_SurrogateSelector_m1_9349(L_1, (Object_t *)NULL, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_2 = ___mem;
		NullCheck(L_2);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_2, (((int64_t)((int64_t)0))));
		Object_t * L_3 = ___msg;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		BinaryFormatter_t1_1028 * L_4 = V_0;
		MemoryStream_t1_433 * L_5 = ___mem;
		NullCheck(L_4);
		Object_t * L_6 = BinaryFormatter_Deserialize_m1_9355(L_4, L_5, (HeaderHandler_t1_1623 *)NULL, /*hidden argument*/NULL);
		return ((Object_t *)Castclass(L_6, IMessage_t1_875_il2cpp_TypeInfo_var));
	}

IL_0029:
	{
		BinaryFormatter_t1_1028 * L_7 = V_0;
		MemoryStream_t1_433 * L_8 = ___mem;
		Object_t * L_9 = ___msg;
		NullCheck(L_7);
		Object_t * L_10 = BinaryFormatter_DeserializeMethodResponse_m1_9357(L_7, L_8, (HeaderHandler_t1_1623 *)NULL, L_9, /*hidden argument*/NULL);
		return ((Object_t *)Castclass(L_10, IMessage_t1_875_il2cpp_TypeInfo_var));
	}
}
// System.IO.MemoryStream System.Runtime.Remoting.Channels.CADSerializer::SerializeMessage(System.Runtime.Remoting.Messaging.IMessage)
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryFormatter_t1_1028_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingSurrogateSelector_t1_957_il2cpp_TypeInfo_var;
extern "C" MemoryStream_t1_433 * CADSerializer_SerializeMessage_m1_8096 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		BinaryFormatter_t1_1028_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		RemotingSurrogateSelector_t1_957_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t1_433 * V_0 = {0};
	BinaryFormatter_t1_1028 * V_1 = {0};
	{
		MemoryStream_t1_433 * L_0 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5044(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		BinaryFormatter_t1_1028 * L_1 = (BinaryFormatter_t1_1028 *)il2cpp_codegen_object_new (BinaryFormatter_t1_1028_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1_9338(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		BinaryFormatter_t1_1028 * L_2 = V_1;
		RemotingSurrogateSelector_t1_957 * L_3 = (RemotingSurrogateSelector_t1_957 *)il2cpp_codegen_object_new (RemotingSurrogateSelector_t1_957_il2cpp_TypeInfo_var);
		RemotingSurrogateSelector__ctor_m1_8613(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		BinaryFormatter_set_SurrogateSelector_m1_9349(L_2, L_3, /*hidden argument*/NULL);
		BinaryFormatter_t1_1028 * L_4 = V_1;
		MemoryStream_t1_433 * L_5 = V_0;
		Object_t * L_6 = ___msg;
		NullCheck(L_4);
		BinaryFormatter_Serialize_m1_9359(L_4, L_5, L_6, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_7, (((int64_t)((int64_t)0))));
		MemoryStream_t1_433 * L_8 = V_0;
		return L_8;
	}
}
// System.IO.MemoryStream System.Runtime.Remoting.Channels.CADSerializer::SerializeObject(System.Object)
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* BinaryFormatter_t1_1028_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingSurrogateSelector_t1_957_il2cpp_TypeInfo_var;
extern "C" MemoryStream_t1_433 * CADSerializer_SerializeObject_m1_8097 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		BinaryFormatter_t1_1028_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		RemotingSurrogateSelector_t1_957_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t1_433 * V_0 = {0};
	BinaryFormatter_t1_1028 * V_1 = {0};
	{
		MemoryStream_t1_433 * L_0 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1_5044(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		BinaryFormatter_t1_1028 * L_1 = (BinaryFormatter_t1_1028 *)il2cpp_codegen_object_new (BinaryFormatter_t1_1028_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1_9338(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		BinaryFormatter_t1_1028 * L_2 = V_1;
		RemotingSurrogateSelector_t1_957 * L_3 = (RemotingSurrogateSelector_t1_957 *)il2cpp_codegen_object_new (RemotingSurrogateSelector_t1_957_il2cpp_TypeInfo_var);
		RemotingSurrogateSelector__ctor_m1_8613(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		BinaryFormatter_set_SurrogateSelector_m1_9349(L_2, L_3, /*hidden argument*/NULL);
		BinaryFormatter_t1_1028 * L_4 = V_1;
		MemoryStream_t1_433 * L_5 = V_0;
		Object_t * L_6 = ___obj;
		NullCheck(L_4);
		BinaryFormatter_Serialize_m1_9359(L_4, L_5, L_6, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_7 = V_0;
		NullCheck(L_7);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_7, (((int64_t)((int64_t)0))));
		MemoryStream_t1_433 * L_8 = V_0;
		return L_8;
	}
}
// System.Object System.Runtime.Remoting.Channels.CADSerializer::DeserializeObject(System.IO.MemoryStream)
extern TypeInfo* BinaryFormatter_t1_1028_il2cpp_TypeInfo_var;
extern "C" Object_t * CADSerializer_DeserializeObject_m1_8098 (Object_t * __this /* static, unused */, MemoryStream_t1_433 * ___mem, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BinaryFormatter_t1_1028_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		s_Il2CppMethodIntialized = true;
	}
	BinaryFormatter_t1_1028 * V_0 = {0};
	{
		BinaryFormatter_t1_1028 * L_0 = (BinaryFormatter_t1_1028 *)il2cpp_codegen_object_new (BinaryFormatter_t1_1028_il2cpp_TypeInfo_var);
		BinaryFormatter__ctor_m1_9338(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		BinaryFormatter_t1_1028 * L_1 = V_0;
		NullCheck(L_1);
		BinaryFormatter_set_SurrogateSelector_m1_9349(L_1, (Object_t *)NULL, /*hidden argument*/NULL);
		MemoryStream_t1_433 * L_2 = ___mem;
		NullCheck(L_2);
		VirtActionInvoker1< int64_t >::Invoke(11 /* System.Void System.IO.MemoryStream::set_Position(System.Int64) */, L_2, (((int64_t)((int64_t)0))));
		BinaryFormatter_t1_1028 * L_3 = V_0;
		MemoryStream_t1_433 * L_4 = ___mem;
		NullCheck(L_3);
		Object_t * L_5 = BinaryFormatter_Deserialize_m1_9354(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Runtime.Remoting.Channels.AsyncRequest::.ctor(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void AsyncRequest__ctor_m1_8099 (AsyncRequest_t1_884 * __this, Object_t * ___msgRequest, Object_t * ___replySink, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___replySink;
		__this->___ReplySink_0 = L_0;
		Object_t * L_1 = ___msgRequest;
		__this->___MsgRequest_1 = L_1;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::.ctor()
extern "C" void ServerChannelSinkStack__ctor_m1_8100 (ServerChannelSinkStack_t1_885 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.IO.Stream System.Runtime.Remoting.Channels.ServerChannelSinkStack::GetResponseStream(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders)
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* IServerChannelSink_t1_1706_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2159;
extern "C" Stream_t1_405 * ServerChannelSinkStack_GetResponseStream_m1_8101 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___msg, Object_t * ___headers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		IServerChannelSink_t1_1706_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		_stringLiteral2159 = il2cpp_codegen_string_literal_from_index(2159);
		s_Il2CppMethodIntialized = true;
	}
	{
		ChanelSinkStackEntry_t1_876 * L_0 = (__this->____sinkStack_0);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RemotingException_t1_1025 * L_1 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_1, _stringLiteral2159, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ChanelSinkStackEntry_t1_876 * L_2 = (__this->____sinkStack_0);
		NullCheck(L_2);
		Object_t * L_3 = (L_2->___Sink_0);
		ChanelSinkStackEntry_t1_876 * L_4 = (__this->____sinkStack_0);
		NullCheck(L_4);
		Object_t * L_5 = (L_4->___State_1);
		Object_t * L_6 = ___msg;
		Object_t * L_7 = ___headers;
		NullCheck(((Object_t *)Castclass(L_3, IServerChannelSink_t1_1706_il2cpp_TypeInfo_var)));
		Stream_t1_405 * L_8 = (Stream_t1_405 *)InterfaceFuncInvoker4< Stream_t1_405 *, Object_t *, Object_t *, Object_t *, Object_t * >::Invoke(2 /* System.IO.Stream System.Runtime.Remoting.Channels.IServerChannelSink::GetResponseStream(System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders) */, IServerChannelSink_t1_1706_il2cpp_TypeInfo_var, ((Object_t *)Castclass(L_3, IServerChannelSink_t1_1706_il2cpp_TypeInfo_var)), __this, L_5, L_6, L_7);
		return L_8;
	}
}
// System.Object System.Runtime.Remoting.Channels.ServerChannelSinkStack::Pop(System.Runtime.Remoting.Channels.IServerChannelSink)
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2154;
extern "C" Object_t * ServerChannelSinkStack_Pop_m1_8102 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral2154 = il2cpp_codegen_string_literal_from_index(2154);
		s_Il2CppMethodIntialized = true;
	}
	ChanelSinkStackEntry_t1_876 * V_0 = {0};
	{
		goto IL_0030;
	}

IL_0005:
	{
		ChanelSinkStackEntry_t1_876 * L_0 = (__this->____sinkStack_0);
		V_0 = L_0;
		ChanelSinkStackEntry_t1_876 * L_1 = (__this->____sinkStack_0);
		NullCheck(L_1);
		ChanelSinkStackEntry_t1_876 * L_2 = (L_1->___Next_2);
		__this->____sinkStack_0 = L_2;
		ChanelSinkStackEntry_t1_876 * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = (L_3->___Sink_0);
		Object_t * L_5 = ___sink;
		if ((!(((Object_t*)(Object_t *)L_4) == ((Object_t*)(Object_t *)L_5))))
		{
			goto IL_0030;
		}
	}
	{
		ChanelSinkStackEntry_t1_876 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = (L_6->___State_1);
		return L_7;
	}

IL_0030:
	{
		ChanelSinkStackEntry_t1_876 * L_8 = (__this->____sinkStack_0);
		if (L_8)
		{
			goto IL_0005;
		}
	}
	{
		RemotingException_t1_1025 * L_9 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_9, _stringLiteral2154, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::Push(System.Runtime.Remoting.Channels.IServerChannelSink,System.Object)
extern TypeInfo* ChanelSinkStackEntry_t1_876_il2cpp_TypeInfo_var;
extern "C" void ServerChannelSinkStack_Push_m1_8103 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChanelSinkStackEntry_t1_876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___sink;
		Object_t * L_1 = ___state;
		ChanelSinkStackEntry_t1_876 * L_2 = (__this->____sinkStack_0);
		ChanelSinkStackEntry_t1_876 * L_3 = (ChanelSinkStackEntry_t1_876 *)il2cpp_codegen_object_new (ChanelSinkStackEntry_t1_876_il2cpp_TypeInfo_var);
		ChanelSinkStackEntry__ctor_m1_8063(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->____sinkStack_0 = L_3;
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::ServerCallback(System.IAsyncResult)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ServerChannelSinkStack_ServerCallback_m1_8104 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___ar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::Store(System.Runtime.Remoting.Channels.IServerChannelSink,System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ServerChannelSinkStack_Store_m1_8105 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::StoreAndDispatch(System.Runtime.Remoting.Channels.IServerChannelSink,System.Object)
extern TypeInfo* NotImplementedException_t1_1582_il2cpp_TypeInfo_var;
extern "C" void ServerChannelSinkStack_StoreAndDispatch_m1_8106 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___sink, Object_t * ___state, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1_1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1_1582 * L_0 = (NotImplementedException_t1_1582 *)il2cpp_codegen_object_new (NotImplementedException_t1_1582_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1_14412(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerChannelSinkStack::AsyncProcessResponse(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream)
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern TypeInfo* IServerChannelSink_t1_1706_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2153;
extern "C" void ServerChannelSinkStack_AsyncProcessResponse_m1_8107 (ServerChannelSinkStack_t1_885 * __this, Object_t * ___msg, Object_t * ___headers, Stream_t1_405 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		IServerChannelSink_t1_1706_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		_stringLiteral2153 = il2cpp_codegen_string_literal_from_index(2153);
		s_Il2CppMethodIntialized = true;
	}
	ChanelSinkStackEntry_t1_876 * V_0 = {0};
	{
		ChanelSinkStackEntry_t1_876 * L_0 = (__this->____sinkStack_0);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		RemotingException_t1_1025 * L_1 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
		RemotingException__ctor_m1_9174(L_1, _stringLiteral2153, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		ChanelSinkStackEntry_t1_876 * L_2 = (__this->____sinkStack_0);
		V_0 = L_2;
		ChanelSinkStackEntry_t1_876 * L_3 = (__this->____sinkStack_0);
		NullCheck(L_3);
		ChanelSinkStackEntry_t1_876 * L_4 = (L_3->___Next_2);
		__this->____sinkStack_0 = L_4;
		ChanelSinkStackEntry_t1_876 * L_5 = V_0;
		NullCheck(L_5);
		Object_t * L_6 = (L_5->___Sink_0);
		ChanelSinkStackEntry_t1_876 * L_7 = V_0;
		NullCheck(L_7);
		Object_t * L_8 = (L_7->___State_1);
		Object_t * L_9 = ___msg;
		Object_t * L_10 = ___headers;
		Stream_t1_405 * L_11 = ___stream;
		NullCheck(((Object_t *)Castclass(L_6, IServerChannelSink_t1_1706_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker5< Object_t *, Object_t *, Object_t *, Object_t *, Stream_t1_405 * >::Invoke(1 /* System.Void System.Runtime.Remoting.Channels.IServerChannelSink::AsyncProcessResponse(System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream) */, IServerChannelSink_t1_1706_il2cpp_TypeInfo_var, ((Object_t *)Castclass(L_6, IServerChannelSink_t1_1706_il2cpp_TypeInfo_var)), __this, L_8, L_9, L_10, L_11);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSink::.ctor()
extern "C" void ServerDispatchSink__ctor_m1_8108 (ServerDispatchSink_t1_886 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.ServerDispatchSink::get_NextChannelSink()
extern "C" Object_t * ServerDispatchSink_get_NextChannelSink_m1_8109 (ServerDispatchSink_t1_886 * __this, const MethodInfo* method)
{
	{
		return (Object_t *)NULL;
	}
}
// System.Collections.IDictionary System.Runtime.Remoting.Channels.ServerDispatchSink::get_Properties()
extern "C" Object_t * ServerDispatchSink_get_Properties_m1_8110 (ServerDispatchSink_t1_886 * __this, const MethodInfo* method)
{
	{
		return (Object_t *)NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSink::AsyncProcessResponse(System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ServerDispatchSink_AsyncProcessResponse_m1_8111 (ServerDispatchSink_t1_886 * __this, Object_t * ___sinkStack, Object_t * ___state, Object_t * ___msg, Object_t * ___headers, Stream_t1_405 * ___stream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.IO.Stream System.Runtime.Remoting.Channels.ServerDispatchSink::GetResponseStream(System.Runtime.Remoting.Channels.IServerResponseChannelSinkStack,System.Object,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders)
extern "C" Stream_t1_405 * ServerDispatchSink_GetResponseStream_m1_8112 (ServerDispatchSink_t1_886 * __this, Object_t * ___sinkStack, Object_t * ___state, Object_t * ___msg, Object_t * ___headers, const MethodInfo* method)
{
	{
		return (Stream_t1_405 *)NULL;
	}
}
// System.Runtime.Remoting.Channels.ServerProcessing System.Runtime.Remoting.Channels.ServerDispatchSink::ProcessMessage(System.Runtime.Remoting.Channels.IServerChannelSinkStack,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Channels.ITransportHeaders,System.IO.Stream,System.Runtime.Remoting.Messaging.IMessage&,System.Runtime.Remoting.Channels.ITransportHeaders&,System.IO.Stream&)
extern TypeInfo* ChannelServices_t1_871_il2cpp_TypeInfo_var;
extern "C" int32_t ServerDispatchSink_ProcessMessage_m1_8113 (ServerDispatchSink_t1_886 * __this, Object_t * ___sinkStack, Object_t * ___requestMsg, Object_t * ___requestHeaders, Stream_t1_405 * ___requestStream, Object_t ** ___responseMsg, Object_t ** ___responseHeaders, Stream_t1_405 ** ___responseStream, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ChannelServices_t1_871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(613);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t ** L_0 = ___responseHeaders;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		Stream_t1_405 ** L_1 = ___responseStream;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		Object_t * L_2 = ___sinkStack;
		Object_t * L_3 = ___requestMsg;
		Object_t ** L_4 = ___responseMsg;
		IL2CPP_RUNTIME_CLASS_INIT(ChannelServices_t1_871_il2cpp_TypeInfo_var);
		int32_t L_5 = ChannelServices_DispatchMessage_m1_8044(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::.ctor()
extern "C" void ServerDispatchSinkProvider__ctor_m1_8114 (ServerDispatchSinkProvider_t1_887 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::.ctor(System.Collections.IDictionary,System.Collections.ICollection)
extern "C" void ServerDispatchSinkProvider__ctor_m1_8115 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___properties, Object_t * ___providerData, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Runtime.Remoting.Channels.IServerChannelSinkProvider System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::get_Next()
extern "C" Object_t * ServerDispatchSinkProvider_get_Next_m1_8116 (ServerDispatchSinkProvider_t1_887 * __this, const MethodInfo* method)
{
	{
		return (Object_t *)NULL;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::set_Next(System.Runtime.Remoting.Channels.IServerChannelSinkProvider)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ServerDispatchSinkProvider_set_Next_m1_8117 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::CreateSink(System.Runtime.Remoting.Channels.IChannelReceiver)
extern TypeInfo* ServerDispatchSink_t1_886_il2cpp_TypeInfo_var;
extern "C" Object_t * ServerDispatchSinkProvider_CreateSink_m1_8118 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___channel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ServerDispatchSink_t1_886_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(653);
		s_Il2CppMethodIntialized = true;
	}
	{
		ServerDispatchSink_t1_886 * L_0 = (ServerDispatchSink_t1_886 *)il2cpp_codegen_object_new (ServerDispatchSink_t1_886_il2cpp_TypeInfo_var);
		ServerDispatchSink__ctor_m1_8108(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::GetChannelData(System.Runtime.Remoting.Channels.IChannelDataStore)
extern "C" void ServerDispatchSinkProvider_GetChannelData_m1_8119 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___channelData, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Channels.SinkProviderData::.ctor(System.String)
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void SinkProviderData__ctor_m1_8120 (SinkProviderData_t1_889 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___sinkName_0 = L_0;
		ArrayList_t1_170 * L_1 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_1, /*hidden argument*/NULL);
		__this->___children_1 = L_1;
		Hashtable_t1_100 * L_2 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_2, /*hidden argument*/NULL);
		__this->___properties_2 = L_2;
		return;
	}
}
// System.Collections.IList System.Runtime.Remoting.Channels.SinkProviderData::get_Children()
extern "C" Object_t * SinkProviderData_get_Children_m1_8121 (SinkProviderData_t1_889 * __this, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = (__this->___children_1);
		return L_0;
	}
}
// System.String System.Runtime.Remoting.Channels.SinkProviderData::get_Name()
extern "C" String_t* SinkProviderData_get_Name_m1_8122 (SinkProviderData_t1_889 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___sinkName_0);
		return L_0;
	}
}
// System.Collections.IDictionary System.Runtime.Remoting.Channels.SinkProviderData::get_Properties()
extern "C" Object_t * SinkProviderData_get_Properties_m1_8123 (SinkProviderData_t1_889 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___properties_2);
		return L_0;
	}
}
// System.Void System.Runtime.Remoting.Channels.TransportHeaders::.ctor()
extern TypeInfo* CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var;
extern TypeInfo* CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var;
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern "C" void TransportHeaders__ctor_m1_8124 (TransportHeaders_t1_890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveHashCodeProvider_t1_278_il2cpp_TypeInfo_var);
		CaseInsensitiveHashCodeProvider_t1_278 * L_0 = CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1_3158(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CaseInsensitiveComparer_t1_276_il2cpp_TypeInfo_var);
		CaseInsensitiveComparer_t1_276 * L_1 = CaseInsensitiveComparer_get_DefaultInvariant_m1_3150(NULL /*static, unused*/, /*hidden argument*/NULL);
		Hashtable_t1_100 * L_2 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3289(L_2, L_0, L_1, /*hidden argument*/NULL);
		__this->___hash_table_0 = L_2;
		return;
	}
}
// System.Object System.Runtime.Remoting.Channels.TransportHeaders::get_Item(System.Object)
extern "C" Object_t * TransportHeaders_get_Item_m1_8125 (TransportHeaders_t1_890 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___hash_table_0);
		Object_t * L_1 = ___key;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void System.Runtime.Remoting.Channels.TransportHeaders::set_Item(System.Object,System.Object)
extern "C" void TransportHeaders_set_Item_m1_8126 (TransportHeaders_t1_890 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___hash_table_0);
		Object_t * L_1 = ___key;
		Object_t * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(31 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_0, L_1, L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Runtime.Remoting.Channels.TransportHeaders::GetEnumerator()
extern "C" Object_t * TransportHeaders_GetEnumerator_m1_8127 (TransportHeaders_t1_890 * __this, const MethodInfo* method)
{
	{
		Hashtable_t1_100 * L_0 = (__this->___hash_table_0);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(36 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_0);
		return L_1;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::.ctor()
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" void Context__ctor_m1_8128 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		int32_t L_0 = Thread_GetDomainID_m1_12814(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___domain_id_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_count_9;
		int32_t L_2 = L_1;
		((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_count_9 = ((int32_t)((int32_t)L_2+(int32_t)1));
		__this->___context_id_1 = ((int32_t)((int32_t)1+(int32_t)L_2));
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::.cctor()
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" void Context__cctor_m1_8129 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	{
		Hashtable_t1_100 * L_0 = (Hashtable_t1_100 *)il2cpp_codegen_object_new (Hashtable_t1_100_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1_3279(L_0, /*hidden argument*/NULL);
		((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10 = L_0;
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::Finalize()
extern "C" void Context_Finalize_m1_8130 (Context_t1_891 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0xC, FINALLY_0005);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0005;
	}

FINALLY_0005:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(5)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(5)
	{
		IL2CPP_JUMP_TBL(0xC, IL_000c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_000c:
	{
		return;
	}
}
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::get_DefaultContext()
extern "C" Context_t1_891 * Context_get_DefaultContext_m1_8131 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Context_t1_891 * L_0 = AppDomain_InternalGetDefaultContext_m1_13142(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 System.Runtime.Remoting.Contexts.Context::get_ContextID()
extern "C" int32_t Context_get_ContextID_m1_8132 (Context_t1_891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___context_id_1);
		return L_0;
	}
}
// System.Runtime.Remoting.Contexts.IContextProperty[] System.Runtime.Remoting.Contexts.Context::get_ContextProperties()
extern const Il2CppType* IContextPropertyU5BU5D_t1_1715_0_0_0_var;
extern TypeInfo* IContextPropertyU5BU5D_t1_1715_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" IContextPropertyU5BU5D_t1_1715* Context_get_ContextProperties_m1_8133 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IContextPropertyU5BU5D_t1_1715_0_0_0_var = il2cpp_codegen_type_from_index(654);
		IContextPropertyU5BU5D_t1_1715_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(654);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (__this->___context_properties_7);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		return ((IContextPropertyU5BU5D_t1_1715*)SZArrayNew(IContextPropertyU5BU5D_t1_1715_il2cpp_TypeInfo_var, 0));
	}

IL_0012:
	{
		ArrayList_t1_170 * L_1 = (__this->___context_properties_7);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(IContextPropertyU5BU5D_t1_1715_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		Array_t * L_3 = (Array_t *)VirtFuncInvoker1< Array_t *, Type_t * >::Invoke(63 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_1, L_2);
		return ((IContextPropertyU5BU5D_t1_1715*)Castclass(L_3, IContextPropertyU5BU5D_t1_1715_il2cpp_TypeInfo_var));
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_IsDefaultContext()
extern "C" bool Context_get_IsDefaultContext_m1_8134 (Context_t1_891 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___context_id_1);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_NeedsContextSink()
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" bool Context_get_NeedsContextSink_m1_8135 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = (__this->___context_id_1);
		if (L_0)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_1 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_2 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		NullCheck(L_2);
		bool L_3 = DynamicPropertyCollection_get_HasProperties_m1_8163(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003f;
		}
	}

IL_0024:
	{
		DynamicPropertyCollection_t1_892 * L_4 = (__this->___context_dynamic_properties_12);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		DynamicPropertyCollection_t1_892 * L_5 = (__this->___context_dynamic_properties_12);
		NullCheck(L_5);
		bool L_6 = DynamicPropertyCollection_get_HasProperties_m1_8163(L_5, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_6));
		goto IL_003d;
	}

IL_003c:
	{
		G_B6_0 = 0;
	}

IL_003d:
	{
		G_B8_0 = G_B6_0;
		goto IL_0040;
	}

IL_003f:
	{
		G_B8_0 = 1;
	}

IL_0040:
	{
		return G_B8_0;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::RegisterDynamicProperty(System.Runtime.Remoting.Contexts.IDynamicProperty,System.ContextBoundObject,System.Runtime.Remoting.Contexts.Context)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" bool Context_RegisterDynamicProperty_m1_8136 (Object_t * __this /* static, unused */, Object_t * ___prop, ContextBoundObject_t1_897 * ___obj, Context_t1_891 * ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	DynamicPropertyCollection_t1_892 * V_0 = {0};
	{
		ContextBoundObject_t1_897 * L_0 = ___obj;
		Context_t1_891 * L_1 = ___ctx;
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_2 = Context_GetDynamicPropertyCollection_m1_8138(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DynamicPropertyCollection_t1_892 * L_3 = V_0;
		Object_t * L_4 = ___prop;
		NullCheck(L_3);
		bool L_5 = DynamicPropertyCollection_RegisterDynamicProperty_m1_8164(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::UnregisterDynamicProperty(System.String,System.ContextBoundObject,System.Runtime.Remoting.Contexts.Context)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" bool Context_UnregisterDynamicProperty_m1_8137 (Object_t * __this /* static, unused */, String_t* ___name, ContextBoundObject_t1_897 * ___obj, Context_t1_891 * ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	DynamicPropertyCollection_t1_892 * V_0 = {0};
	{
		ContextBoundObject_t1_897 * L_0 = ___obj;
		Context_t1_891 * L_1 = ___ctx;
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_2 = Context_GetDynamicPropertyCollection_m1_8138(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DynamicPropertyCollection_t1_892 * L_3 = V_0;
		String_t* L_4 = ___name;
		NullCheck(L_3);
		bool L_5 = DynamicPropertyCollection_UnregisterDynamicProperty_m1_8165(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Runtime.Remoting.Contexts.DynamicPropertyCollection System.Runtime.Remoting.Contexts.Context::GetDynamicPropertyCollection(System.ContextBoundObject,System.Runtime.Remoting.Contexts.Context)
extern TypeInfo* RemotingServices_t1_1027_il2cpp_TypeInfo_var;
extern TypeInfo* DynamicPropertyCollection_t1_892_il2cpp_TypeInfo_var;
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2160;
extern "C" DynamicPropertyCollection_t1_892 * Context_GetDynamicPropertyCollection_m1_8138 (Object_t * __this /* static, unused */, ContextBoundObject_t1_897 * ___obj, Context_t1_891 * ___ctx, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RemotingServices_t1_1027_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		DynamicPropertyCollection_t1_892_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(656);
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral2160 = il2cpp_codegen_string_literal_from_index(2160);
		s_Il2CppMethodIntialized = true;
	}
	RealProxy_t1_130 * V_0 = {0};
	{
		Context_t1_891 * L_0 = ___ctx;
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		ContextBoundObject_t1_897 * L_1 = ___obj;
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		ContextBoundObject_t1_897 * L_2 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		bool L_3 = RemotingServices_IsTransparentProxy_m1_9182(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		ContextBoundObject_t1_897 * L_4 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(RemotingServices_t1_1027_il2cpp_TypeInfo_var);
		RealProxy_t1_130 * L_5 = RemotingServices_GetRealProxy_m1_9196(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RealProxy_t1_130 * L_6 = V_0;
		NullCheck(L_6);
		Identity_t1_943 * L_7 = RealProxy_get_ObjectIdentity_m1_8997(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		DynamicPropertyCollection_t1_892 * L_8 = Identity_get_ClientDynamicProperties_m1_9062(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_002a:
	{
		ContextBoundObject_t1_897 * L_9 = ___obj;
		NullCheck(L_9);
		ServerIdentity_t1_70 * L_10 = MarshalByRefObject_get_ObjectIdentity_m1_1372(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		DynamicPropertyCollection_t1_892 * L_11 = Identity_get_ServerDynamicProperties_m1_9063(L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0036:
	{
		Context_t1_891 * L_12 = ___ctx;
		if (!L_12)
		{
			goto IL_005f;
		}
	}
	{
		ContextBoundObject_t1_897 * L_13 = ___obj;
		if (L_13)
		{
			goto IL_005f;
		}
	}
	{
		Context_t1_891 * L_14 = ___ctx;
		NullCheck(L_14);
		DynamicPropertyCollection_t1_892 * L_15 = (L_14->___context_dynamic_properties_12);
		if (L_15)
		{
			goto IL_0058;
		}
	}
	{
		Context_t1_891 * L_16 = ___ctx;
		DynamicPropertyCollection_t1_892 * L_17 = (DynamicPropertyCollection_t1_892 *)il2cpp_codegen_object_new (DynamicPropertyCollection_t1_892_il2cpp_TypeInfo_var);
		DynamicPropertyCollection__ctor_m1_8162(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		L_16->___context_dynamic_properties_12 = L_17;
	}

IL_0058:
	{
		Context_t1_891 * L_18 = ___ctx;
		NullCheck(L_18);
		DynamicPropertyCollection_t1_892 * L_19 = (L_18->___context_dynamic_properties_12);
		return L_19;
	}

IL_005f:
	{
		Context_t1_891 * L_20 = ___ctx;
		if (L_20)
		{
			goto IL_0085;
		}
	}
	{
		ContextBoundObject_t1_897 * L_21 = ___obj;
		if (L_21)
		{
			goto IL_0085;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_22 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		if (L_22)
		{
			goto IL_007f;
		}
	}
	{
		DynamicPropertyCollection_t1_892 * L_23 = (DynamicPropertyCollection_t1_892 *)il2cpp_codegen_object_new (DynamicPropertyCollection_t1_892_il2cpp_TypeInfo_var);
		DynamicPropertyCollection__ctor_m1_8162(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11 = L_23;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_24 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		return L_24;
	}

IL_0085:
	{
		ArgumentException_t1_1425 * L_25 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_25, _stringLiteral2160, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_25);
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::NotifyGlobalDynamicSinks(System.Boolean,System.Runtime.Remoting.Messaging.IMessage,System.Boolean,System.Boolean)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" void Context_NotifyGlobalDynamicSinks_m1_8139 (Object_t * __this /* static, unused */, bool ___start, Object_t * ___req_msg, bool ___client_site, bool ___async, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_0 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_1 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		NullCheck(L_1);
		bool L_2 = DynamicPropertyCollection_get_HasProperties_m1_8163(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_3 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		bool L_4 = ___start;
		Object_t * L_5 = ___req_msg;
		bool L_6 = ___client_site;
		bool L_7 = ___async;
		NullCheck(L_3);
		DynamicPropertyCollection_NotifyMessage_m1_8166(L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_HasGlobalDynamicSinks()
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" bool Context_get_HasGlobalDynamicSinks_m1_8140 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_0 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		DynamicPropertyCollection_t1_892 * L_1 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___global_dynamic_properties_11;
		NullCheck(L_1);
		bool L_2 = DynamicPropertyCollection_get_HasProperties_m1_8163(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::NotifyDynamicSinks(System.Boolean,System.Runtime.Remoting.Messaging.IMessage,System.Boolean,System.Boolean)
extern "C" void Context_NotifyDynamicSinks_m1_8141 (Context_t1_891 * __this, bool ___start, Object_t * ___req_msg, bool ___client_site, bool ___async, const MethodInfo* method)
{
	{
		DynamicPropertyCollection_t1_892 * L_0 = (__this->___context_dynamic_properties_12);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		DynamicPropertyCollection_t1_892 * L_1 = (__this->___context_dynamic_properties_12);
		NullCheck(L_1);
		bool L_2 = DynamicPropertyCollection_get_HasProperties_m1_8163(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		DynamicPropertyCollection_t1_892 * L_3 = (__this->___context_dynamic_properties_12);
		bool L_4 = ___start;
		Object_t * L_5 = ___req_msg;
		bool L_6 = ___client_site;
		bool L_7 = ___async;
		NullCheck(L_3);
		DynamicPropertyCollection_NotifyMessage_m1_8166(L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_HasDynamicSinks()
extern "C" bool Context_get_HasDynamicSinks_m1_8142 (Context_t1_891 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		DynamicPropertyCollection_t1_892 * L_0 = (__this->___context_dynamic_properties_12);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		DynamicPropertyCollection_t1_892 * L_1 = (__this->___context_dynamic_properties_12);
		NullCheck(L_1);
		bool L_2 = DynamicPropertyCollection_get_HasProperties_m1_8163(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.Context::get_HasExitSinks()
extern TypeInfo* ClientContextTerminatorSink_t1_928_il2cpp_TypeInfo_var;
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" bool Context_get_HasExitSinks_m1_8143 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClientContextTerminatorSink_t1_928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Object_t * L_0 = Context_GetClientContextSinkChain_m1_8149(__this, /*hidden argument*/NULL);
		if (!((ClientContextTerminatorSink_t1_928 *)IsInstClass(L_0, ClientContextTerminatorSink_t1_928_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		bool L_1 = Context_get_HasDynamicSinks_m1_8142(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		bool L_2 = Context_get_HasGlobalDynamicSinks_m1_8140(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_2));
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 1;
	}

IL_0023:
	{
		return G_B4_0;
	}
}
// System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IContextProperty_t1_1716_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" Object_t * Context_GetProperty_m1_8144 (Context_t1_891 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IContextProperty_t1_1716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t1_170 * L_0 = (__this->___context_properties_7);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (Object_t *)NULL;
	}

IL_000d:
	{
		ArrayList_t1_170 * L_1 = (__this->___context_properties_7);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_1);
		V_1 = L_2;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001e:
		{
			Object_t * L_3 = V_1;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_3);
			V_0 = ((Object_t *)Castclass(L_4, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
			Object_t * L_5 = V_0;
			NullCheck(L_5);
			String_t* L_6 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Contexts.IContextProperty::get_Name() */, IContextProperty_t1_1716_il2cpp_TypeInfo_var, L_5);
			String_t* L_7 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_8 = String_op_Equality_m1_601(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0042;
			}
		}

IL_003b:
		{
			Object_t * L_9 = V_0;
			V_2 = L_9;
			IL2CPP_LEAVE(0x66, FINALLY_0052);
		}

IL_0042:
		{
			Object_t * L_10 = V_1;
			NullCheck(L_10);
			bool L_11 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_001e;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		{
			Object_t * L_12 = V_1;
			V_3 = ((Object_t *)IsInst(L_12, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_13 = V_3;
			if (L_13)
			{
				goto IL_005d;
			}
		}

IL_005c:
		{
			IL2CPP_END_FINALLY(82)
		}

IL_005d:
		{
			Object_t * L_14 = V_3;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_14);
			IL2CPP_END_FINALLY(82)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0064:
	{
		return (Object_t *)NULL;
	}

IL_0066:
	{
		Object_t * L_15 = V_2;
		return L_15;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::SetProperty(System.Runtime.Remoting.Contexts.IContextProperty)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2161;
extern Il2CppCodeGenString* _stringLiteral2162;
extern Il2CppCodeGenString* _stringLiteral2163;
extern "C" void Context_SetProperty_m1_8145 (Context_t1_891 * __this, Object_t * ___prop, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		_stringLiteral2161 = il2cpp_codegen_string_literal_from_index(2161);
		_stringLiteral2162 = il2cpp_codegen_string_literal_from_index(2162);
		_stringLiteral2163 = il2cpp_codegen_string_literal_from_index(2163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___prop;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2161, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Context_t1_891 * L_2 = Context_get_DefaultContext_m1_8131(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(Context_t1_891 *)__this) == ((Object_t*)(Context_t1_891 *)L_2))))
		{
			goto IL_0027;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, _stringLiteral2162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0027:
	{
		bool L_4 = (__this->___frozen_8);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, _stringLiteral2163, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_003d:
	{
		ArrayList_t1_170 * L_6 = (__this->___context_properties_7);
		if (L_6)
		{
			goto IL_0053;
		}
	}
	{
		ArrayList_t1_170 * L_7 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_7, /*hidden argument*/NULL);
		__this->___context_properties_7 = L_7;
	}

IL_0053:
	{
		ArrayList_t1_170 * L_8 = (__this->___context_properties_7);
		Object_t * L_9 = ___prop;
		NullCheck(L_8);
		VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_8, L_9);
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::Freeze()
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IContextProperty_t1_1716_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void Context_Freeze_m1_8146 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IContextProperty_t1_1716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t1_170 * L_0 = (__this->___context_properties_7);
		if (!L_0)
		{
			goto IL_0051;
		}
	}
	{
		ArrayList_t1_170 * L_1 = (__this->___context_properties_7);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_1);
		V_1 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002f;
		}

IL_001c:
		{
			Object_t * L_3 = V_1;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_3);
			V_0 = ((Object_t *)Castclass(L_4, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
			Object_t * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker1< Context_t1_891 * >::Invoke(1 /* System.Void System.Runtime.Remoting.Contexts.IContextProperty::Freeze(System.Runtime.Remoting.Contexts.Context) */, IContextProperty_t1_1716_il2cpp_TypeInfo_var, L_5, __this);
		}

IL_002f:
		{
			Object_t * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_6);
			if (L_7)
			{
				goto IL_001c;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x51, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		{
			Object_t * L_8 = V_1;
			V_2 = ((Object_t *)IsInst(L_8, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_9 = V_2;
			if (L_9)
			{
				goto IL_004a;
			}
		}

IL_0049:
		{
			IL2CPP_END_FINALLY(63)
		}

IL_004a:
		{
			Object_t * L_10 = V_2;
			NullCheck(L_10);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_10);
			IL2CPP_END_FINALLY(63)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0051:
	{
		return;
	}
}
// System.String System.Runtime.Remoting.Contexts.Context::ToString()
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2164;
extern "C" String_t* Context_ToString_m1_8147 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral2164 = il2cpp_codegen_string_literal_from_index(2164);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___context_id_1);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral2164, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.Context::GetServerContextSinkChain()
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* ServerContextTerminatorSink_t1_961_il2cpp_TypeInfo_var;
extern TypeInfo* IContributeServerContextSink_t1_1767_il2cpp_TypeInfo_var;
extern "C" Object_t * Context_GetServerContextSinkChain_m1_8148 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		ServerContextTerminatorSink_t1_961_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(658);
		IContributeServerContextSink_t1_1767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(659);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	{
		Object_t * L_0 = (__this->___server_context_sink_chain_4);
		if (L_0)
		{
			goto IL_007d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Object_t * L_1 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___default_server_context_sink_3;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		ServerContextTerminatorSink_t1_961 * L_2 = (ServerContextTerminatorSink_t1_961 *)il2cpp_codegen_object_new (ServerContextTerminatorSink_t1_961_il2cpp_TypeInfo_var);
		ServerContextTerminatorSink__ctor_m1_8648(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___default_server_context_sink_3 = L_2;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Object_t * L_3 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___default_server_context_sink_3;
		__this->___server_context_sink_chain_4 = L_3;
		ArrayList_t1_170 * L_4 = (__this->___context_properties_7);
		if (!L_4)
		{
			goto IL_007d;
		}
	}
	{
		ArrayList_t1_170 * L_5 = (__this->___context_properties_7);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_5);
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)1));
		goto IL_0076;
	}

IL_0048:
	{
		ArrayList_t1_170 * L_7 = (__this->___context_properties_7);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		Object_t * L_9 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_7, L_8);
		V_1 = ((Object_t *)IsInst(L_9, IContributeServerContextSink_t1_1767_il2cpp_TypeInfo_var));
		Object_t * L_10 = V_1;
		if (!L_10)
		{
			goto IL_0072;
		}
	}
	{
		Object_t * L_11 = V_1;
		Object_t * L_12 = (__this->___server_context_sink_chain_4);
		NullCheck(L_11);
		Object_t * L_13 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.IContributeServerContextSink::GetServerContextSink(System.Runtime.Remoting.Messaging.IMessageSink) */, IContributeServerContextSink_t1_1767_il2cpp_TypeInfo_var, L_11, L_12);
		__this->___server_context_sink_chain_4 = L_13;
	}

IL_0072:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_0076:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_0048;
		}
	}

IL_007d:
	{
		Object_t * L_16 = (__this->___server_context_sink_chain_4);
		return L_16;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.Context::GetClientContextSinkChain()
extern TypeInfo* ClientContextTerminatorSink_t1_928_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IContextProperty_t1_1716_il2cpp_TypeInfo_var;
extern TypeInfo* IContributeClientContextSink_t1_1768_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" Object_t * Context_GetClientContextSinkChain_m1_8149 (Context_t1_891 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClientContextTerminatorSink_t1_928_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IContextProperty_t1_1716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		IContributeClientContextSink_t1_1768_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(660);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = (__this->___client_context_sink_chain_5);
		if (L_0)
		{
			goto IL_0080;
		}
	}
	{
		ClientContextTerminatorSink_t1_928 * L_1 = (ClientContextTerminatorSink_t1_928 *)il2cpp_codegen_object_new (ClientContextTerminatorSink_t1_928_il2cpp_TypeInfo_var);
		ClientContextTerminatorSink__ctor_m1_8337(L_1, __this, /*hidden argument*/NULL);
		__this->___client_context_sink_chain_5 = L_1;
		ArrayList_t1_170 * L_2 = (__this->___context_properties_7);
		if (!L_2)
		{
			goto IL_0080;
		}
	}
	{
		ArrayList_t1_170 * L_3 = (__this->___context_properties_7);
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_3);
		V_1 = L_4;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005e;
		}

IL_0033:
		{
			Object_t * L_5 = V_1;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_5);
			V_0 = ((Object_t *)Castclass(L_6, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
			Object_t * L_7 = V_0;
			V_2 = ((Object_t *)IsInst(L_7, IContributeClientContextSink_t1_1768_il2cpp_TypeInfo_var));
			Object_t * L_8 = V_2;
			if (!L_8)
			{
				goto IL_005e;
			}
		}

IL_004c:
		{
			Object_t * L_9 = V_2;
			Object_t * L_10 = (__this->___client_context_sink_chain_5);
			NullCheck(L_9);
			Object_t * L_11 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.IContributeClientContextSink::GetClientContextSink(System.Runtime.Remoting.Messaging.IMessageSink) */, IContributeClientContextSink_t1_1768_il2cpp_TypeInfo_var, L_9, L_10);
			__this->___client_context_sink_chain_5 = L_11;
		}

IL_005e:
		{
			Object_t * L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0033;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x80, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		{
			Object_t * L_14 = V_1;
			V_3 = ((Object_t *)IsInst(L_14, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_15 = V_3;
			if (L_15)
			{
				goto IL_0079;
			}
		}

IL_0078:
		{
			IL2CPP_END_FINALLY(110)
		}

IL_0079:
		{
			Object_t * L_16 = V_3;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(110)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x80, IL_0080)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0080:
	{
		Object_t * L_17 = (__this->___client_context_sink_chain_5);
		return L_17;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.Context::CreateServerObjectSinkChain(System.MarshalByRefObject,System.Boolean)
extern TypeInfo* StackBuilderSink_t1_964_il2cpp_TypeInfo_var;
extern TypeInfo* ServerObjectTerminatorSink_t1_962_il2cpp_TypeInfo_var;
extern TypeInfo* LeaseSink_t1_911_il2cpp_TypeInfo_var;
extern TypeInfo* IContextProperty_t1_1716_il2cpp_TypeInfo_var;
extern TypeInfo* IContributeObjectSink_t1_1769_il2cpp_TypeInfo_var;
extern "C" Object_t * Context_CreateServerObjectSinkChain_m1_8150 (Context_t1_891 * __this, MarshalByRefObject_t1_69 * ___obj, bool ___forceInternalExecute, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackBuilderSink_t1_964_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		ServerObjectTerminatorSink_t1_962_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		LeaseSink_t1_911_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(663);
		IContextProperty_t1_1716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		IContributeObjectSink_t1_1769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	{
		MarshalByRefObject_t1_69 * L_0 = ___obj;
		bool L_1 = ___forceInternalExecute;
		StackBuilderSink_t1_964 * L_2 = (StackBuilderSink_t1_964 *)il2cpp_codegen_object_new (StackBuilderSink_t1_964_il2cpp_TypeInfo_var);
		StackBuilderSink__ctor_m1_8660(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Object_t * L_3 = V_0;
		ServerObjectTerminatorSink_t1_962 * L_4 = (ServerObjectTerminatorSink_t1_962 *)il2cpp_codegen_object_new (ServerObjectTerminatorSink_t1_962_il2cpp_TypeInfo_var);
		ServerObjectTerminatorSink__ctor_m1_8652(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Object_t * L_5 = V_0;
		LeaseSink_t1_911 * L_6 = (LeaseSink_t1_911 *)il2cpp_codegen_object_new (LeaseSink_t1_911_il2cpp_TypeInfo_var);
		LeaseSink__ctor_m1_8254(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		ArrayList_t1_170 * L_7 = (__this->___context_properties_7);
		if (!L_7)
		{
			goto IL_0067;
		}
	}
	{
		ArrayList_t1_170 * L_8 = (__this->___context_properties_7);
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_8);
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
		goto IL_0060;
	}

IL_0034:
	{
		ArrayList_t1_170 * L_10 = (__this->___context_properties_7);
		int32_t L_11 = V_1;
		NullCheck(L_10);
		Object_t * L_12 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_10, L_11);
		V_2 = ((Object_t *)Castclass(L_12, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
		Object_t * L_13 = V_2;
		V_3 = ((Object_t *)IsInst(L_13, IContributeObjectSink_t1_1769_il2cpp_TypeInfo_var));
		Object_t * L_14 = V_3;
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		Object_t * L_15 = V_3;
		MarshalByRefObject_t1_69 * L_16 = ___obj;
		Object_t * L_17 = V_0;
		NullCheck(L_15);
		Object_t * L_18 = (Object_t *)InterfaceFuncInvoker2< Object_t *, MarshalByRefObject_t1_69 *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.IContributeObjectSink::GetObjectSink(System.MarshalByRefObject,System.Runtime.Remoting.Messaging.IMessageSink) */, IContributeObjectSink_t1_1769_il2cpp_TypeInfo_var, L_15, L_16, L_17);
		V_0 = L_18;
	}

IL_005c:
	{
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0060:
	{
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}

IL_0067:
	{
		Object_t * L_21 = V_0;
		return L_21;
	}
}
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.Context::CreateEnvoySink(System.MarshalByRefObject)
extern TypeInfo* EnvoyTerminatorSink_t1_937_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IContextProperty_t1_1716_il2cpp_TypeInfo_var;
extern TypeInfo* IContributeEnvoySink_t1_1770_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" Object_t * Context_CreateEnvoySink_m1_8151 (Context_t1_891 * __this, MarshalByRefObject_t1_69 * ___serverObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EnvoyTerminatorSink_t1_937_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(665);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IContextProperty_t1_1716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		IContributeEnvoySink_t1_1770_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(666);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(EnvoyTerminatorSink_t1_937_il2cpp_TypeInfo_var);
		EnvoyTerminatorSink_t1_937 * L_0 = ((EnvoyTerminatorSink_t1_937_StaticFields*)EnvoyTerminatorSink_t1_937_il2cpp_TypeInfo_var->static_fields)->___Instance_0;
		V_0 = L_0;
		ArrayList_t1_170 * L_1 = (__this->___context_properties_7);
		if (!L_1)
		{
			goto IL_0069;
		}
	}
	{
		ArrayList_t1_170 * L_2 = (__this->___context_properties_7);
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_2);
		V_2 = L_3;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0044;
		}

IL_0022:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_4);
			V_1 = ((Object_t *)Castclass(L_5, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
			Object_t * L_6 = V_1;
			V_3 = ((Object_t *)IsInst(L_6, IContributeEnvoySink_t1_1770_il2cpp_TypeInfo_var));
			Object_t * L_7 = V_3;
			if (!L_7)
			{
				goto IL_0044;
			}
		}

IL_003b:
		{
			Object_t * L_8 = V_3;
			MarshalByRefObject_t1_69 * L_9 = ___serverObject;
			Object_t * L_10 = V_0;
			NullCheck(L_8);
			Object_t * L_11 = (Object_t *)InterfaceFuncInvoker2< Object_t *, MarshalByRefObject_t1_69 *, Object_t * >::Invoke(0 /* System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.IContributeEnvoySink::GetEnvoySink(System.MarshalByRefObject,System.Runtime.Remoting.Messaging.IMessageSink) */, IContributeEnvoySink_t1_1770_il2cpp_TypeInfo_var, L_8, L_9, L_10);
			V_0 = L_11;
		}

IL_0044:
		{
			Object_t * L_12 = V_2;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0022;
			}
		}

IL_004f:
		{
			IL2CPP_LEAVE(0x69, FINALLY_0054);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0054;
	}

FINALLY_0054:
	{ // begin finally (depth: 1)
		{
			Object_t * L_14 = V_2;
			V_4 = ((Object_t *)IsInst(L_14, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_15 = V_4;
			if (L_15)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(84)
		}

IL_0061:
		{
			Object_t * L_16 = V_4;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(84)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(84)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0069:
	{
		Object_t * L_17 = V_0;
		return L_17;
	}
}
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::SwitchToContext(System.Runtime.Remoting.Contexts.Context)
extern "C" Context_t1_891 * Context_SwitchToContext_m1_8152 (Object_t * __this /* static, unused */, Context_t1_891 * ___newContext, const MethodInfo* method)
{
	{
		Context_t1_891 * L_0 = ___newContext;
		Context_t1_891 * L_1 = AppDomain_InternalSetContext_m1_13140(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Contexts.Context::CreateNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IContextProperty_t1_1716_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2165;
extern "C" Context_t1_891 * Context_CreateNewContext_m1_8153 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IContextProperty_t1_1716_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral2165 = il2cpp_codegen_string_literal_from_index(2165);
		s_Il2CppMethodIntialized = true;
	}
	Context_t1_891 * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Context_t1_891 * L_0 = (Context_t1_891 *)il2cpp_codegen_object_new (Context_t1_891_il2cpp_TypeInfo_var);
		Context__ctor_m1_8128(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Object_t * L_1 = ___msg;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(5 /* System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003b;
		}

IL_0017:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_4);
			V_1 = ((Object_t *)Castclass(L_5, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
			Context_t1_891 * L_6 = V_0;
			Object_t * L_7 = V_1;
			NullCheck(L_7);
			String_t* L_8 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Contexts.IContextProperty::get_Name() */, IContextProperty_t1_1716_il2cpp_TypeInfo_var, L_7);
			NullCheck(L_6);
			Object_t * L_9 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(6 /* System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String) */, L_6, L_8);
			if (L_9)
			{
				goto IL_003b;
			}
		}

IL_0034:
		{
			Context_t1_891 * L_10 = V_0;
			Object_t * L_11 = V_1;
			NullCheck(L_10);
			VirtActionInvoker1< Object_t * >::Invoke(7 /* System.Void System.Runtime.Remoting.Contexts.Context::SetProperty(System.Runtime.Remoting.Contexts.IContextProperty) */, L_10, L_11);
		}

IL_003b:
		{
			Object_t * L_12 = V_2;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0017;
			}
		}

IL_0046:
		{
			IL2CPP_LEAVE(0x60, FINALLY_004b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		{
			Object_t * L_14 = V_2;
			V_5 = ((Object_t *)IsInst(L_14, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_15 = V_5;
			if (L_15)
			{
				goto IL_0058;
			}
		}

IL_0057:
		{
			IL2CPP_END_FINALLY(75)
		}

IL_0058:
		{
			Object_t * L_16 = V_5;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(75)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0060:
	{
		Context_t1_891 * L_17 = V_0;
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(8 /* System.Void System.Runtime.Remoting.Contexts.Context::Freeze() */, L_17);
		Object_t * L_18 = ___msg;
		NullCheck(L_18);
		Object_t * L_19 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(5 /* System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_18);
		NullCheck(L_19);
		Object_t * L_20 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_19);
		V_4 = L_20;
	}

IL_0073:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009c;
		}

IL_0078:
		{
			Object_t * L_21 = V_4;
			NullCheck(L_21);
			Object_t * L_22 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_21);
			V_3 = ((Object_t *)Castclass(L_22, IContextProperty_t1_1716_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_3;
			Context_t1_891 * L_24 = V_0;
			NullCheck(L_23);
			bool L_25 = (bool)InterfaceFuncInvoker1< bool, Context_t1_891 * >::Invoke(2 /* System.Boolean System.Runtime.Remoting.Contexts.IContextProperty::IsNewContextOK(System.Runtime.Remoting.Contexts.Context) */, IContextProperty_t1_1716_il2cpp_TypeInfo_var, L_23, L_24);
			if (L_25)
			{
				goto IL_009c;
			}
		}

IL_0091:
		{
			RemotingException_t1_1025 * L_26 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
			RemotingException__ctor_m1_9174(L_26, _stringLiteral2165, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
		}

IL_009c:
		{
			Object_t * L_27 = V_4;
			NullCheck(L_27);
			bool L_28 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_27);
			if (L_28)
			{
				goto IL_0078;
			}
		}

IL_00a8:
		{
			IL2CPP_LEAVE(0xC3, FINALLY_00ad);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00ad;
	}

FINALLY_00ad:
	{ // begin finally (depth: 1)
		{
			Object_t * L_29 = V_4;
			V_6 = ((Object_t *)IsInst(L_29, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_30 = V_6;
			if (L_30)
			{
				goto IL_00bb;
			}
		}

IL_00ba:
		{
			IL2CPP_END_FINALLY(173)
		}

IL_00bb:
		{
			Object_t * L_31 = V_6;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_31);
			IL2CPP_END_FINALLY(173)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(173)
	{
		IL2CPP_JUMP_TBL(0xC3, IL_00c3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00c3:
	{
		Context_t1_891 * L_32 = V_0;
		return L_32;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::DoCallBack(System.Runtime.Remoting.Contexts.CrossContextDelegate)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* ContextCallbackObject_t1_893_il2cpp_TypeInfo_var;
extern "C" void Context_DoCallBack_m1_8154 (Context_t1_891 * __this, CrossContextDelegate_t1_1622 * ___deleg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		ContextCallbackObject_t1_893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		s_Il2CppMethodIntialized = true;
	}
	Context_t1_891 * V_0 = {0};
	Context_t1_891 * V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		Context_t1_891 * L_0 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			ContextCallbackObject_t1_893 * L_1 = (__this->___callback_object_13);
			if (L_1)
			{
				goto IL_002c;
			}
		}

IL_0013:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
			Context_t1_891 * L_2 = Context_SwitchToContext_m1_8152(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			V_1 = L_2;
			ContextCallbackObject_t1_893 * L_3 = (ContextCallbackObject_t1_893 *)il2cpp_codegen_object_new (ContextCallbackObject_t1_893_il2cpp_TypeInfo_var);
			ContextCallbackObject__ctor_m1_8168(L_3, /*hidden argument*/NULL);
			__this->___callback_object_13 = L_3;
			Context_t1_891 * L_4 = V_1;
			Context_SwitchToContext_m1_8152(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Context_t1_891 * L_5 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0038:
	{
		ContextCallbackObject_t1_893 * L_6 = (__this->___callback_object_13);
		CrossContextDelegate_t1_1622 * L_7 = ___deleg;
		NullCheck(L_6);
		ContextCallbackObject_DoCallBack_m1_8169(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.LocalDataStoreSlot System.Runtime.Remoting.Contexts.Context::AllocateDataSlot()
extern TypeInfo* LocalDataStoreSlot_t1_1564_il2cpp_TypeInfo_var;
extern "C" LocalDataStoreSlot_t1_1564 * Context_AllocateDataSlot_m1_8155 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LocalDataStoreSlot_t1_1564_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		s_Il2CppMethodIntialized = true;
	}
	{
		LocalDataStoreSlot_t1_1564 * L_0 = (LocalDataStoreSlot_t1_1564 *)il2cpp_codegen_object_new (LocalDataStoreSlot_t1_1564_il2cpp_TypeInfo_var);
		LocalDataStoreSlot__ctor_m1_14184(L_0, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.LocalDataStoreSlot System.Runtime.Remoting.Contexts.Context::AllocateNamedDataSlot(System.String)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" LocalDataStoreSlot_t1_1564 * Context_AllocateNamedDataSlot_m1_8156 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	LocalDataStoreSlot_t1_1564 * V_1 = {0};
	LocalDataStoreSlot_t1_1564 * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_0 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(25 /* System.Object System.Collections.Hashtable::get_SyncRoot() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
			LocalDataStoreSlot_t1_1564 * L_3 = Context_AllocateDataSlot_m1_8155(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_1 = L_3;
			Hashtable_t1_100 * L_4 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10;
			String_t* L_5 = ___name;
			LocalDataStoreSlot_t1_1564 * L_6 = V_1;
			NullCheck(L_4);
			VirtActionInvoker2< Object_t *, Object_t * >::Invoke(33 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_4, L_5, L_6);
			LocalDataStoreSlot_t1_1564 * L_7 = V_1;
			V_2 = L_7;
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}

IL_002a:
		{
			; // IL_002a: leave IL_0036
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		Object_t * L_8 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_JUMP_TBL(0x36, IL_0036)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0036:
	{
		LocalDataStoreSlot_t1_1564 * L_9 = V_2;
		return L_9;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::FreeNamedDataSlot(System.String)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern "C" void Context_FreeNamedDataSlot_m1_8157 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_0 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(25 /* System.Object System.Collections.Hashtable::get_SyncRoot() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_3 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10;
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(37 /* System.Void System.Collections.Hashtable::Remove(System.Object) */, L_3, L_4);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		Object_t * L_5 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0028:
	{
		return;
	}
}
// System.Object System.Runtime.Remoting.Contexts.Context::GetData(System.LocalDataStoreSlot)
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern "C" Object_t * Context_GetData_m1_8158 (Object_t * __this /* static, unused */, LocalDataStoreSlot_t1_1564 * ___slot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	Context_t1_891 * V_0 = {0};
	Context_t1_891 * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_0 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Context_t1_891 * L_1 = V_0;
		V_1 = L_1;
		Context_t1_891 * L_2 = V_1;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			Context_t1_891 * L_3 = V_0;
			NullCheck(L_3);
			ObjectU5BU5D_t1_272* L_4 = (L_3->___datastore_6);
			if (!L_4)
			{
				goto IL_003f;
			}
		}

IL_0019:
		{
			LocalDataStoreSlot_t1_1564 * L_5 = ___slot;
			NullCheck(L_5);
			int32_t L_6 = (L_5->___slot_0);
			Context_t1_891 * L_7 = V_0;
			NullCheck(L_7);
			ObjectU5BU5D_t1_272* L_8 = (L_7->___datastore_6);
			NullCheck(L_8);
			if ((((int32_t)L_6) >= ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
			{
				goto IL_003f;
			}
		}

IL_002c:
		{
			Context_t1_891 * L_9 = V_0;
			NullCheck(L_9);
			ObjectU5BU5D_t1_272* L_10 = (L_9->___datastore_6);
			LocalDataStoreSlot_t1_1564 * L_11 = ___slot;
			NullCheck(L_11);
			int32_t L_12 = (L_11->___slot_0);
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_12);
			int32_t L_13 = L_12;
			V_2 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_10, L_13, sizeof(Object_t *)));
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		}

IL_003f:
		{
			V_2 = NULL;
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		}

IL_0046:
		{
			; // IL_0046: leave IL_0052
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		Context_t1_891 * L_14 = V_1;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0052:
	{
		Object_t * L_15 = V_2;
		return L_15;
	}
}
// System.LocalDataStoreSlot System.Runtime.Remoting.Contexts.Context::GetNamedDataSlot(System.String)
extern TypeInfo* Context_t1_891_il2cpp_TypeInfo_var;
extern TypeInfo* LocalDataStoreSlot_t1_1564_il2cpp_TypeInfo_var;
extern "C" LocalDataStoreSlot_t1_1564 * Context_GetNamedDataSlot_m1_8159 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Context_t1_891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(618);
		LocalDataStoreSlot_t1_1564_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	LocalDataStoreSlot_t1_1564 * V_1 = {0};
	LocalDataStoreSlot_t1_1564 * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
		Hashtable_t1_100 * L_0 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(25 /* System.Object System.Collections.Hashtable::get_SyncRoot() */, L_0);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
			Hashtable_t1_100 * L_3 = ((Context_t1_891_StaticFields*)Context_t1_891_il2cpp_TypeInfo_var->static_fields)->___namedSlots_10;
			String_t* L_4 = ___name;
			NullCheck(L_3);
			Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(30 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_3, L_4);
			V_1 = ((LocalDataStoreSlot_t1_1564 *)IsInstSealed(L_5, LocalDataStoreSlot_t1_1564_il2cpp_TypeInfo_var));
			LocalDataStoreSlot_t1_1564 * L_6 = V_1;
			if (L_6)
			{
				goto IL_0034;
			}
		}

IL_0028:
		{
			String_t* L_7 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(Context_t1_891_il2cpp_TypeInfo_var);
			LocalDataStoreSlot_t1_1564 * L_8 = Context_AllocateNamedDataSlot_m1_8156(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}

IL_0034:
		{
			LocalDataStoreSlot_t1_1564 * L_9 = V_1;
			V_2 = L_9;
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}

IL_003b:
		{
			; // IL_003b: leave IL_0047
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Object_t * L_10 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0047:
	{
		LocalDataStoreSlot_t1_1564 * L_11 = V_2;
		return L_11;
	}
}
// System.Void System.Runtime.Remoting.Contexts.Context::SetData(System.LocalDataStoreSlot,System.Object)
extern TypeInfo* Thread_t1_901_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern "C" void Context_SetData_m1_8160 (Object_t * __this /* static, unused */, LocalDataStoreSlot_t1_1564 * ___slot, Object_t * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t1_901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	Context_t1_891 * V_0 = {0};
	Context_t1_891 * V_1 = {0};
	ObjectU5BU5D_t1_272* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1_901_il2cpp_TypeInfo_var);
		Context_t1_891 * L_0 = Thread_get_CurrentContext_m1_12799(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Context_t1_891 * L_1 = V_0;
		V_1 = L_1;
		Context_t1_891 * L_2 = V_1;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			Context_t1_891 * L_3 = V_0;
			NullCheck(L_3);
			ObjectU5BU5D_t1_272* L_4 = (L_3->___datastore_6);
			if (L_4)
			{
				goto IL_0031;
			}
		}

IL_0019:
		{
			Context_t1_891 * L_5 = V_0;
			LocalDataStoreSlot_t1_1564 * L_6 = ___slot;
			NullCheck(L_6);
			int32_t L_7 = (L_6->___slot_0);
			NullCheck(L_5);
			L_5->___datastore_6 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_7+(int32_t)2))));
			goto IL_0066;
		}

IL_0031:
		{
			LocalDataStoreSlot_t1_1564 * L_8 = ___slot;
			NullCheck(L_8);
			int32_t L_9 = (L_8->___slot_0);
			Context_t1_891 * L_10 = V_0;
			NullCheck(L_10);
			ObjectU5BU5D_t1_272* L_11 = (L_10->___datastore_6);
			NullCheck(L_11);
			if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
			{
				goto IL_0066;
			}
		}

IL_0044:
		{
			LocalDataStoreSlot_t1_1564 * L_12 = ___slot;
			NullCheck(L_12);
			int32_t L_13 = (L_12->___slot_0);
			V_2 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_13+(int32_t)2))));
			Context_t1_891 * L_14 = V_0;
			NullCheck(L_14);
			ObjectU5BU5D_t1_272* L_15 = (L_14->___datastore_6);
			ObjectU5BU5D_t1_272* L_16 = V_2;
			NullCheck(L_15);
			VirtActionInvoker2< Array_t *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, L_15, (Array_t *)(Array_t *)L_16, 0);
			Context_t1_891 * L_17 = V_0;
			ObjectU5BU5D_t1_272* L_18 = V_2;
			NullCheck(L_17);
			L_17->___datastore_6 = L_18;
		}

IL_0066:
		{
			Context_t1_891 * L_19 = V_0;
			NullCheck(L_19);
			ObjectU5BU5D_t1_272* L_20 = (L_19->___datastore_6);
			LocalDataStoreSlot_t1_1564 * L_21 = ___slot;
			NullCheck(L_21);
			int32_t L_22 = (L_21->___slot_0);
			Object_t * L_23 = ___data;
			NullCheck(L_20);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_22);
			ArrayElementTypeCheck (L_20, L_23);
			*((Object_t **)(Object_t **)SZArrayLdElema(L_20, L_22, sizeof(Object_t *))) = (Object_t *)L_23;
			IL2CPP_LEAVE(0x80, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		Context_t1_891 * L_24 = V_1;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(121)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x80, IL_0080)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0080:
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.DynamicPropertyCollection/DynamicPropertyReg::.ctor()
extern "C" void DynamicPropertyReg__ctor_m1_8161 (DynamicPropertyReg_t1_894 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.DynamicPropertyCollection::.ctor()
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern "C" void DynamicPropertyCollection__ctor_m1_8162 (DynamicPropertyCollection_t1_892 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArrayList_t1_170 * L_0 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3049(L_0, /*hidden argument*/NULL);
		__this->____properties_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.DynamicPropertyCollection::get_HasProperties()
extern "C" bool DynamicPropertyCollection_get_HasProperties_m1_8163 (DynamicPropertyCollection_t1_892 * __this, const MethodInfo* method)
{
	{
		ArrayList_t1_170 * L_0 = (__this->____properties_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_0);
		return ((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.DynamicPropertyCollection::RegisterDynamicProperty(System.Runtime.Remoting.Contexts.IDynamicProperty)
extern TypeInfo* IDynamicProperty_t1_895_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern TypeInfo* DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var;
extern TypeInfo* IContributeDynamicSink_t1_1771_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2166;
extern "C" bool DynamicPropertyCollection_RegisterDynamicProperty_m1_8164 (DynamicPropertyCollection_t1_892 * __this, Object_t * ___prop, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDynamicProperty_t1_895_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(669);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(670);
		IContributeDynamicSink_t1_1771_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(671);
		_stringLiteral2166 = il2cpp_codegen_string_literal_from_index(2166);
		s_Il2CppMethodIntialized = true;
	}
	DynamicPropertyCollection_t1_892 * V_0 = {0};
	ArrayList_t1_170 * V_1 = {0};
	DynamicPropertyReg_t1_894 * V_2 = {0};
	Object_t * V_3 = {0};
	bool V_4 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		DynamicPropertyCollection_t1_892 * L_0 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_1 = ___prop;
			NullCheck(L_1);
			String_t* L_2 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Contexts.IDynamicProperty::get_Name() */, IDynamicProperty_t1_895_il2cpp_TypeInfo_var, L_1);
			int32_t L_3 = DynamicPropertyCollection_FindProperty_m1_8167(__this, L_2, /*hidden argument*/NULL);
			if ((((int32_t)L_3) == ((int32_t)(-1))))
			{
				goto IL_0025;
			}
		}

IL_001a:
		{
			InvalidOperationException_t1_1559 * L_4 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1_14171(L_4, _stringLiteral2166, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_0025:
		{
			ArrayList_t1_170 * L_5 = (__this->____properties_0);
			ArrayList_t1_170 * L_6 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
			ArrayList__ctor_m1_3050(L_6, L_5, /*hidden argument*/NULL);
			V_1 = L_6;
			DynamicPropertyReg_t1_894 * L_7 = (DynamicPropertyReg_t1_894 *)il2cpp_codegen_object_new (DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var);
			DynamicPropertyReg__ctor_m1_8161(L_7, /*hidden argument*/NULL);
			V_2 = L_7;
			DynamicPropertyReg_t1_894 * L_8 = V_2;
			Object_t * L_9 = ___prop;
			NullCheck(L_8);
			L_8->___Property_0 = L_9;
			Object_t * L_10 = ___prop;
			V_3 = ((Object_t *)IsInst(L_10, IContributeDynamicSink_t1_1771_il2cpp_TypeInfo_var));
			Object_t * L_11 = V_3;
			if (!L_11)
			{
				goto IL_0057;
			}
		}

IL_004b:
		{
			DynamicPropertyReg_t1_894 * L_12 = V_2;
			Object_t * L_13 = V_3;
			NullCheck(L_13);
			Object_t * L_14 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Runtime.Remoting.Contexts.IDynamicMessageSink System.Runtime.Remoting.Contexts.IContributeDynamicSink::GetDynamicSink() */, IContributeDynamicSink_t1_1771_il2cpp_TypeInfo_var, L_13);
			NullCheck(L_12);
			L_12->___Sink_1 = L_14;
		}

IL_0057:
		{
			ArrayList_t1_170 * L_15 = V_1;
			DynamicPropertyReg_t1_894 * L_16 = V_2;
			NullCheck(L_15);
			VirtFuncInvoker1< int32_t, Object_t * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_15, L_16);
			ArrayList_t1_170 * L_17 = V_1;
			__this->____properties_0 = L_17;
			V_4 = 1;
			IL2CPP_LEAVE(0x7A, FINALLY_0073);
		}

IL_006e:
		{
			; // IL_006e: leave IL_007a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0073;
	}

FINALLY_0073:
	{ // begin finally (depth: 1)
		DynamicPropertyCollection_t1_892 * L_18 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(115)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(115)
	{
		IL2CPP_JUMP_TBL(0x7A, IL_007a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_007a:
	{
		bool L_19 = V_4;
		return L_19;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.DynamicPropertyCollection::UnregisterDynamicProperty(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RemotingException_t1_1025_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2167;
extern Il2CppCodeGenString* _stringLiteral2168;
extern "C" bool DynamicPropertyCollection_UnregisterDynamicProperty_m1_8165 (DynamicPropertyCollection_t1_892 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		RemotingException_t1_1025_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral2167 = il2cpp_codegen_string_literal_from_index(2167);
		_stringLiteral2168 = il2cpp_codegen_string_literal_from_index(2168);
		s_Il2CppMethodIntialized = true;
	}
	DynamicPropertyCollection_t1_892 * V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		DynamicPropertyCollection_t1_892 * L_0 = V_0;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_1 = ___name;
			int32_t L_2 = DynamicPropertyCollection_FindProperty_m1_8167(__this, L_1, /*hidden argument*/NULL);
			V_1 = L_2;
			int32_t L_3 = V_1;
			if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
			{
				goto IL_002d;
			}
		}

IL_0017:
		{
			String_t* L_4 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_5 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral2167, L_4, _stringLiteral2168, /*hidden argument*/NULL);
			RemotingException_t1_1025 * L_6 = (RemotingException_t1_1025 *)il2cpp_codegen_object_new (RemotingException_t1_1025_il2cpp_TypeInfo_var);
			RemotingException__ctor_m1_9174(L_6, L_5, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
		}

IL_002d:
		{
			ArrayList_t1_170 * L_7 = (__this->____properties_0);
			int32_t L_8 = V_1;
			NullCheck(L_7);
			VirtActionInvoker1< int32_t >::Invoke(43 /* System.Void System.Collections.ArrayList::RemoveAt(System.Int32) */, L_7, L_8);
			V_2 = 1;
			IL2CPP_LEAVE(0x4C, FINALLY_0045);
		}

IL_0040:
		{
			; // IL_0040: leave IL_004c
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		DynamicPropertyCollection_t1_892 * L_9 = V_0;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_004c:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Void System.Runtime.Remoting.Contexts.DynamicPropertyCollection::NotifyMessage(System.Boolean,System.Runtime.Remoting.Messaging.IMessage,System.Boolean,System.Boolean)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var;
extern TypeInfo* IDynamicMessageSink_t1_896_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void DynamicPropertyCollection_NotifyMessage_m1_8166 (DynamicPropertyCollection_t1_892 * __this, bool ___start, Object_t * ___msg, bool ___client_site, bool ___async, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(670);
		IDynamicMessageSink_t1_896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	ArrayList_t1_170 * V_0 = {0};
	DynamicPropertyReg_t1_894 * V_1 = {0};
	Object_t * V_2 = {0};
	DynamicPropertyReg_t1_894 * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t1_170 * L_0 = (__this->____properties_0);
		V_0 = L_0;
		bool L_1 = ___start;
		if (!L_1)
		{
			goto IL_0069;
		}
	}
	{
		ArrayList_t1_170 * L_2 = V_0;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_2);
		V_2 = L_3;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0019:
		{
			Object_t * L_4 = V_2;
			NullCheck(L_4);
			Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_4);
			V_1 = ((DynamicPropertyReg_t1_894 *)CastclassClass(L_5, DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var));
			DynamicPropertyReg_t1_894 * L_6 = V_1;
			NullCheck(L_6);
			Object_t * L_7 = (L_6->___Sink_1);
			if (!L_7)
			{
				goto IL_003f;
			}
		}

IL_0030:
		{
			DynamicPropertyReg_t1_894 * L_8 = V_1;
			NullCheck(L_8);
			Object_t * L_9 = (L_8->___Sink_1);
			Object_t * L_10 = ___msg;
			bool L_11 = ___client_site;
			bool L_12 = ___async;
			NullCheck(L_9);
			InterfaceActionInvoker3< Object_t *, bool, bool >::Invoke(1 /* System.Void System.Runtime.Remoting.Contexts.IDynamicMessageSink::ProcessMessageStart(System.Runtime.Remoting.Messaging.IMessage,System.Boolean,System.Boolean) */, IDynamicMessageSink_t1_896_il2cpp_TypeInfo_var, L_9, L_10, L_11, L_12);
		}

IL_003f:
		{
			Object_t * L_13 = V_2;
			NullCheck(L_13);
			bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_13);
			if (L_14)
			{
				goto IL_0019;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x64, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Object_t * L_15 = V_2;
			V_5 = ((Object_t *)IsInst(L_15, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_16 = V_5;
			if (L_16)
			{
				goto IL_005c;
			}
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}

IL_005c:
		{
			Object_t * L_17 = V_5;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_17);
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0064:
	{
		goto IL_00c4;
	}

IL_0069:
	{
		ArrayList_t1_170 * L_18 = V_0;
		NullCheck(L_18);
		Object_t * L_19 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(50 /* System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator() */, L_18);
		V_4 = L_19;
	}

IL_0071:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009d;
		}

IL_0076:
		{
			Object_t * L_20 = V_4;
			NullCheck(L_20);
			Object_t * L_21 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_20);
			V_3 = ((DynamicPropertyReg_t1_894 *)CastclassClass(L_21, DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var));
			DynamicPropertyReg_t1_894 * L_22 = V_3;
			NullCheck(L_22);
			Object_t * L_23 = (L_22->___Sink_1);
			if (!L_23)
			{
				goto IL_009d;
			}
		}

IL_008e:
		{
			DynamicPropertyReg_t1_894 * L_24 = V_3;
			NullCheck(L_24);
			Object_t * L_25 = (L_24->___Sink_1);
			Object_t * L_26 = ___msg;
			bool L_27 = ___client_site;
			bool L_28 = ___async;
			NullCheck(L_25);
			InterfaceActionInvoker3< Object_t *, bool, bool >::Invoke(0 /* System.Void System.Runtime.Remoting.Contexts.IDynamicMessageSink::ProcessMessageFinish(System.Runtime.Remoting.Messaging.IMessage,System.Boolean,System.Boolean) */, IDynamicMessageSink_t1_896_il2cpp_TypeInfo_var, L_25, L_26, L_27, L_28);
		}

IL_009d:
		{
			Object_t * L_29 = V_4;
			NullCheck(L_29);
			bool L_30 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_29);
			if (L_30)
			{
				goto IL_0076;
			}
		}

IL_00a9:
		{
			IL2CPP_LEAVE(0xC4, FINALLY_00ae);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00ae;
	}

FINALLY_00ae:
	{ // begin finally (depth: 1)
		{
			Object_t * L_31 = V_4;
			V_6 = ((Object_t *)IsInst(L_31, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_32 = V_6;
			if (L_32)
			{
				goto IL_00bc;
			}
		}

IL_00bb:
		{
			IL2CPP_END_FINALLY(174)
		}

IL_00bc:
		{
			Object_t * L_33 = V_6;
			NullCheck(L_33);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_33);
			IL2CPP_END_FINALLY(174)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(174)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00c4:
	{
		return;
	}
}
// System.Int32 System.Runtime.Remoting.Contexts.DynamicPropertyCollection::FindProperty(System.String)
extern TypeInfo* DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var;
extern TypeInfo* IDynamicProperty_t1_895_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" int32_t DynamicPropertyCollection_FindProperty_m1_8167 (DynamicPropertyCollection_t1_892 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(670);
		IDynamicProperty_t1_895_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(669);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0007:
	{
		ArrayList_t1_170 * L_0 = (__this->____properties_0);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* System.Object System.Collections.ArrayList::get_Item(System.Int32) */, L_0, L_1);
		NullCheck(((DynamicPropertyReg_t1_894 *)CastclassClass(L_2, DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var)));
		Object_t * L_3 = (((DynamicPropertyReg_t1_894 *)CastclassClass(L_2, DynamicPropertyReg_t1_894_il2cpp_TypeInfo_var))->___Property_0);
		NullCheck(L_3);
		String_t* L_4 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String System.Runtime.Remoting.Contexts.IDynamicProperty::get_Name() */, IDynamicProperty_t1_895_il2cpp_TypeInfo_var, L_3);
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1_601(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_002f:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_9 = V_0;
		ArrayList_t1_170 * L_10 = (__this->____properties_0);
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(23 /* System.Int32 System.Collections.ArrayList::get_Count() */, L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::.ctor()
extern "C" void ContextCallbackObject__ctor_m1_8168 (ContextCallbackObject_t1_893 * __this, const MethodInfo* method)
{
	{
		ContextBoundObject__ctor_m1_13430(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.ContextCallbackObject::DoCallBack(System.Runtime.Remoting.Contexts.CrossContextDelegate)
extern "C" void ContextCallbackObject_DoCallBack_m1_8169 (ContextCallbackObject_t1_893 * __this, CrossContextDelegate_t1_1622 * ___deleg, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::.ctor(System.String)
extern "C" void ContextAttribute__ctor_m1_8170 (ContextAttribute_t1_859 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___AttributeName_0 = L_0;
		return;
	}
}
// System.String System.Runtime.Remoting.Contexts.ContextAttribute::get_Name()
extern "C" String_t* ContextAttribute_get_Name_m1_8171 (ContextAttribute_t1_859 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___AttributeName_0);
		return L_0;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::Equals(System.Object)
extern TypeInfo* ContextAttribute_t1_859_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool ContextAttribute_Equals_m1_8172 (ContextAttribute_t1_859 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ContextAttribute_t1_859_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(673);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	ContextAttribute_t1_859 * V_0 = {0};
	{
		Object_t * L_0 = ___o;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___o;
		if (((ContextAttribute_t1_859 *)IsInstClass(L_1, ContextAttribute_t1_859_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}

IL_0015:
	{
		Object_t * L_2 = ___o;
		V_0 = ((ContextAttribute_t1_859 *)CastclassClass(L_2, ContextAttribute_t1_859_il2cpp_TypeInfo_var));
		ContextAttribute_t1_859 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = (L_3->___AttributeName_0);
		String_t* L_5 = (__this->___AttributeName_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		return 0;
	}

IL_0034:
	{
		return 1;
	}
}
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::Freeze(System.Runtime.Remoting.Contexts.Context)
extern "C" void ContextAttribute_Freeze_m1_8173 (ContextAttribute_t1_859 * __this, Context_t1_891 * ___newContext, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 System.Runtime.Remoting.Contexts.ContextAttribute::GetHashCode()
extern "C" int32_t ContextAttribute_GetHashCode_m1_8174 (ContextAttribute_t1_859 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___AttributeName_0);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		String_t* L_1 = (__this->___AttributeName_0);
		NullCheck(L_1);
		int32_t L_2 = String_GetHashCode_m1_577(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void System.Runtime.Remoting.Contexts.ContextAttribute::GetPropertiesForNewContext(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2169;
extern "C" void ContextAttribute_GetPropertiesForNewContext_m1_8175 (ContextAttribute_t1_859 * __this, Object_t * ___ctorMsg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		_stringLiteral2169 = il2cpp_codegen_string_literal_from_index(2169);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___ctorMsg;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2169, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___ctorMsg;
		NullCheck(L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(5 /* System.Collections.IList System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ContextProperties() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_2);
		V_0 = L_3;
		Object_t * L_4 = V_0;
		NullCheck(L_4);
		InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1_262_il2cpp_TypeInfo_var, L_4, __this);
		return;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::IsContextOK(System.Runtime.Remoting.Contexts.Context,System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2169;
extern Il2CppCodeGenString* _stringLiteral2170;
extern "C" bool ContextAttribute_IsContextOK_m1_8176 (ContextAttribute_t1_859 * __this, Context_t1_891 * ___ctx, Object_t * ___ctorMsg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		_stringLiteral2169 = il2cpp_codegen_string_literal_from_index(2169);
		_stringLiteral2170 = il2cpp_codegen_string_literal_from_index(2170);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___ctorMsg;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral2169, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Context_t1_891 * L_2 = ___ctx;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, _stringLiteral2170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		Object_t * L_4 = ___ctorMsg;
		NullCheck(L_4);
		Type_t * L_5 = (Type_t *)InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* System.Type System.Runtime.Remoting.Activation.IConstructionCallMessage::get_ActivationType() */, IConstructionCallMessage_t1_1702_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(126 /* System.Boolean System.Type::get_IsContextful() */, L_5);
		if (L_6)
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}

IL_0034:
	{
		Context_t1_891 * L_7 = ___ctx;
		String_t* L_8 = (__this->___AttributeName_0);
		NullCheck(L_7);
		Object_t * L_9 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(6 /* System.Runtime.Remoting.Contexts.IContextProperty System.Runtime.Remoting.Contexts.Context::GetProperty(System.String) */, L_7, L_8);
		V_0 = L_9;
		Object_t * L_10 = V_0;
		if (L_10)
		{
			goto IL_0049;
		}
	}
	{
		return 0;
	}

IL_0049:
	{
		Object_t * L_11 = V_0;
		if ((((Object_t*)(ContextAttribute_t1_859 *)__this) == ((Object_t*)(Object_t *)L_11)))
		{
			goto IL_0052;
		}
	}
	{
		return 0;
	}

IL_0052:
	{
		return 1;
	}
}
// System.Boolean System.Runtime.Remoting.Contexts.ContextAttribute::IsNewContextOK(System.Runtime.Remoting.Contexts.Context)
extern "C" bool ContextAttribute_IsNewContextOK_m1_8177 (ContextAttribute_t1_859 * __this, Context_t1_891 * ___newCtx, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Runtime.Remoting.Contexts.ContextProperty::.ctor(System.String,System.Object)
extern "C" void ContextProperty__ctor_m1_8178 (ContextProperty_t1_898 * __this, String_t* ___name, Object_t * ___prop, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___name_0 = L_0;
		Object_t * L_1 = ___prop;
		__this->___prop_1 = L_1;
		return;
	}
}
// System.String System.Runtime.Remoting.Contexts.ContextProperty::get_Name()
extern "C" String_t* ContextProperty_get_Name_m1_8179 (ContextProperty_t1_898 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___name_0);
		return L_0;
	}
}
// System.Object System.Runtime.Remoting.Contexts.ContextProperty::get_Property()
extern "C" Object_t * ContextProperty_get_Property_m1_8180 (ContextProperty_t1_898 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___prop_1);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
