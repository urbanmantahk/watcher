﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Diagnostics.SymbolStore.ISymbolDocumentWriter
struct ISymbolDocumentWriter_t1_525;
// System.Reflection.Emit.SequencePoint[]
struct SequencePointU5BU5D_t1_524;

#include "mscorlib_System_Object.h"

// System.Reflection.Emit.SequencePointList
struct  SequencePointList_t1_522  : public Object_t
{
	// System.Diagnostics.SymbolStore.ISymbolDocumentWriter System.Reflection.Emit.SequencePointList::doc
	Object_t * ___doc_1;
	// System.Reflection.Emit.SequencePoint[] System.Reflection.Emit.SequencePointList::points
	SequencePointU5BU5D_t1_524* ___points_2;
	// System.Int32 System.Reflection.Emit.SequencePointList::count
	int32_t ___count_3;
};
