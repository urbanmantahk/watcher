﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>
struct SingletonPattern_1_t8_372;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t6_65;
// UnityEngine.GameObject
struct GameObject_t6_97;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::.ctor()
extern "C" void SingletonPattern_1__ctor_m8_2032_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1__ctor_m8_2032(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1__ctor_m8_2032_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::.cctor()
extern "C" void SingletonPattern_1__cctor_m8_2033_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SingletonPattern_1__cctor_m8_2033(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SingletonPattern_1__cctor_m8_2033_gshared)(__this /* static, unused */, method)
// T VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::get_Instance()
extern "C" Object_t * SingletonPattern_1_get_Instance_m8_2034_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SingletonPattern_1_get_Instance_m8_2034(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))SingletonPattern_1_get_Instance_m8_2034_gshared)(__this /* static, unused */, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::set_Instance(T)
extern "C" void SingletonPattern_1_set_Instance_m8_2035_gshared (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method);
#define SingletonPattern_1_set_Instance_m8_2035(__this /* static, unused */, ___value, method) (( void (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))SingletonPattern_1_set_Instance_m8_2035_gshared)(__this /* static, unused */, ___value, method)
// UnityEngine.Transform VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::get_CachedTransform()
extern "C" Transform_t6_65 * SingletonPattern_1_get_CachedTransform_m8_2036_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_get_CachedTransform_m8_2036(__this, method) (( Transform_t6_65 * (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_get_CachedTransform_m8_2036_gshared)(__this, method)
// UnityEngine.GameObject VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::get_CachedGameObject()
extern "C" GameObject_t6_97 * SingletonPattern_1_get_CachedGameObject_m8_2037_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_get_CachedGameObject_m8_2037(__this, method) (( GameObject_t6_97 * (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_get_CachedGameObject_m8_2037_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::ResetStaticProperties()
extern "C" void SingletonPattern_1_ResetStaticProperties_m8_2038_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SingletonPattern_1_ResetStaticProperties_m8_2038(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SingletonPattern_1_ResetStaticProperties_m8_2038_gshared)(__this /* static, unused */, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Awake()
extern "C" void SingletonPattern_1_Awake_m8_2039_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_Awake_m8_2039(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_Awake_m8_2039_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Start()
extern "C" void SingletonPattern_1_Start_m8_2040_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_Start_m8_2040(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_Start_m8_2040_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Reset()
extern "C" void SingletonPattern_1_Reset_m8_2041_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_Reset_m8_2041(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_Reset_m8_2041_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::OnEnable()
extern "C" void SingletonPattern_1_OnEnable_m8_2042_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_OnEnable_m8_2042(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_OnEnable_m8_2042_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::OnDisable()
extern "C" void SingletonPattern_1_OnDisable_m8_2043_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_OnDisable_m8_2043(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_OnDisable_m8_2043_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::OnDestroy()
extern "C" void SingletonPattern_1_OnDestroy_m8_2044_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_OnDestroy_m8_2044(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_OnDestroy_m8_2044_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Init()
extern "C" void SingletonPattern_1_Init_m8_2045_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_Init_m8_2045(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_Init_m8_2045_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::ForceDestroy()
extern "C" void SingletonPattern_1_ForceDestroy_m8_2046_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method);
#define SingletonPattern_1_ForceDestroy_m8_2046(__this, method) (( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))SingletonPattern_1_ForceDestroy_m8_2046_gshared)(__this, method)
