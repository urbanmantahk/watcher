﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CompilerGlobalScopeAttribute
struct CompilerGlobalScopeAttribute_t1_679;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CompilerGlobalScopeAttribute::.ctor()
extern "C" void CompilerGlobalScopeAttribute__ctor_m1_7533 (CompilerGlobalScopeAttribute_t1_679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
