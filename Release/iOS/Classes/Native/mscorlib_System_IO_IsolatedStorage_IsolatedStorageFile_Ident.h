﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_ValueType.h"

// System.IO.IsolatedStorage.IsolatedStorageFile/Identities
struct  Identities_t1_396 
{
	// System.Object System.IO.IsolatedStorage.IsolatedStorageFile/Identities::Application
	Object_t * ___Application_0;
	// System.Object System.IO.IsolatedStorage.IsolatedStorageFile/Identities::Assembly
	Object_t * ___Assembly_1;
	// System.Object System.IO.IsolatedStorage.IsolatedStorageFile/Identities::Domain
	Object_t * ___Domain_2;
};
