﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CallConvThiscall
struct CallConvThiscall_t1_676;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CallConvThiscall::.ctor()
extern "C" void CallConvThiscall__ctor_m1_7529 (CallConvThiscall_t1_676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
