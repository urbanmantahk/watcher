﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ImporterEventKind.h"

// System.Runtime.InteropServices.ImporterEventKind
struct  ImporterEventKind_t1_804 
{
	// System.Int32 System.Runtime.InteropServices.ImporterEventKind::value__
	int32_t ___value___1;
};
