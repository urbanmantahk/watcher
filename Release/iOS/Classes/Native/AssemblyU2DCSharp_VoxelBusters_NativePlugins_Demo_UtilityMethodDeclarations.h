﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.Utility
struct Utility_t8_192;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.Utility::.ctor()
extern "C" void Utility__ctor_m8_1115 (Utility_t8_192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.Utility::Start()
extern "C" void Utility_Start_m8_1116 (Utility_t8_192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Demo.Utility::GetScreenshotPath()
extern "C" String_t* Utility_GetScreenshotPath_m8_1117 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
