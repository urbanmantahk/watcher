﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction
struct AndroidBillingTransaction_t8_210;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.BillingTransaction
struct BillingTransaction_t8_211;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"

// System.Void VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::.ctor(System.Collections.IDictionary)
extern "C" void AndroidBillingTransaction__ctor_m8_1193 (AndroidBillingTransaction_t8_210 * __this, Object_t * ____transactionInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::CreateJSONObject(VoxelBusters.NativePlugins.BillingTransaction)
extern "C" Object_t * AndroidBillingTransaction_CreateJSONObject_m8_1194 (Object_t * __this /* static, unused */, BillingTransaction_t8_211 * ____transaction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetPurchaseIdentifier(System.Collections.IDictionary)
extern "C" String_t* AndroidBillingTransaction_GetPurchaseIdentifier_m8_1195 (AndroidBillingTransaction_t8_210 * __this, Object_t * ____transactionJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eBillingTransactionVerificationState VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetValidationState(System.String)
extern "C" int32_t AndroidBillingTransaction_GetValidationState_m8_1196 (Object_t * __this /* static, unused */, String_t* ____validationState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetValidationState(VoxelBusters.NativePlugins.eBillingTransactionVerificationState)
extern "C" String_t* AndroidBillingTransaction_GetValidationState_m8_1197 (Object_t * __this /* static, unused */, int32_t ____state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetConvertedPurchaseState(System.Int32)
extern "C" int32_t AndroidBillingTransaction_GetConvertedPurchaseState_m8_1198 (Object_t * __this /* static, unused */, int32_t ____stateInt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.Internal.AndroidBillingTransaction::GetConvertedPurchaseState(VoxelBusters.NativePlugins.eBillingTransactionState)
extern "C" int32_t AndroidBillingTransaction_GetConvertedPurchaseState_m8_1199 (Object_t * __this /* static, unused */, int32_t ____state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
