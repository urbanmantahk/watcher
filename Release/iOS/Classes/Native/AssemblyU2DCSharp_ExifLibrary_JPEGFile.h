﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<ExifLibrary.JPEGSection>
struct List_1_t1_1906;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// ExifLibrary.JPEGFile
struct  JPEGFile_t8_111  : public Object_t
{
	// System.Collections.Generic.List`1<ExifLibrary.JPEGSection> ExifLibrary.JPEGFile::<Sections>k__BackingField
	List_1_t1_1906 * ___U3CSectionsU3Ek__BackingField_0;
	// System.Byte[] ExifLibrary.JPEGFile::<TrailingData>k__BackingField
	ByteU5BU5D_t1_109* ___U3CTrailingDataU3Ek__BackingField_1;
};
