﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.AppDomain
struct AppDomain_t1_1403;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t1_1494;
// System.ResolveEventHandler
struct ResolveEventHandler_t1_1495;
// System.EventHandler
struct EventHandler_t1_461;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t1_1496;
// System.AppDomainSetup
struct AppDomainSetup_t1_1497;
// System.String
struct String_t;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Principal.IPrincipal
struct IPrincipal_t1_1477;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Runtime.Remoting.ObjectHandle
struct ObjectHandle_t1_1019;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Object
struct Object_t;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1_466;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;
// System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.CustomAttributeBuilder>
struct IEnumerable_1_t1_1739;
// System.CrossAppDomainDelegate
struct CrossAppDomainDelegate_t1_1631;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t1_1738;
// System.Type
struct Type_t;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Runtime.Remoting.Messaging.CADMethodCallMessage
struct CADMethodCallMessage_t1_925;
// System.Runtime.Remoting.Messaging.CADMethodReturnMessage
struct CADMethodReturnMessage_t1_881;
// System.AppDomainManager
struct AppDomainManager_t1_1493;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.AppDomainInitializer
struct AppDomainInitializer_t1_1498;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderAccess.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
#include "mscorlib_System_Security_Principal_PrincipalPolicy.h"

// System.Void System.AppDomain::.ctor()
extern "C" void AppDomain__ctor_m1_13045 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_AssemblyLoad(System.AssemblyLoadEventHandler)
extern "C" void AppDomain_add_AssemblyLoad_m1_13046 (AppDomain_t1_1403 * __this, AssemblyLoadEventHandler_t1_1494 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_AssemblyLoad(System.AssemblyLoadEventHandler)
extern "C" void AppDomain_remove_AssemblyLoad_m1_13047 (AppDomain_t1_1403 * __this, AssemblyLoadEventHandler_t1_1494 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_AssemblyResolve(System.ResolveEventHandler)
extern "C" void AppDomain_add_AssemblyResolve_m1_13048 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_AssemblyResolve(System.ResolveEventHandler)
extern "C" void AppDomain_remove_AssemblyResolve_m1_13049 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_DomainUnload(System.EventHandler)
extern "C" void AppDomain_add_DomainUnload_m1_13050 (AppDomain_t1_1403 * __this, EventHandler_t1_461 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_DomainUnload(System.EventHandler)
extern "C" void AppDomain_remove_DomainUnload_m1_13051 (AppDomain_t1_1403 * __this, EventHandler_t1_461 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_ProcessExit(System.EventHandler)
extern "C" void AppDomain_add_ProcessExit_m1_13052 (AppDomain_t1_1403 * __this, EventHandler_t1_461 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_ProcessExit(System.EventHandler)
extern "C" void AppDomain_remove_ProcessExit_m1_13053 (AppDomain_t1_1403 * __this, EventHandler_t1_461 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_ResourceResolve(System.ResolveEventHandler)
extern "C" void AppDomain_add_ResourceResolve_m1_13054 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_ResourceResolve(System.ResolveEventHandler)
extern "C" void AppDomain_remove_ResourceResolve_m1_13055 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_TypeResolve(System.ResolveEventHandler)
extern "C" void AppDomain_add_TypeResolve_m1_13056 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_TypeResolve(System.ResolveEventHandler)
extern "C" void AppDomain_remove_TypeResolve_m1_13057 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_UnhandledException(System.UnhandledExceptionEventHandler)
extern "C" void AppDomain_add_UnhandledException_m1_13058 (AppDomain_t1_1403 * __this, UnhandledExceptionEventHandler_t1_1496 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_UnhandledException(System.UnhandledExceptionEventHandler)
extern "C" void AppDomain_remove_UnhandledException_m1_13059 (AppDomain_t1_1403 * __this, UnhandledExceptionEventHandler_t1_1496 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_ReflectionOnlyAssemblyResolve(System.ResolveEventHandler)
extern "C" void AppDomain_add_ReflectionOnlyAssemblyResolve_m1_13060 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::remove_ReflectionOnlyAssemblyResolve(System.ResolveEventHandler)
extern "C" void AppDomain_remove_ReflectionOnlyAssemblyResolve_m1_13061 (AppDomain_t1_1403 * __this, ResolveEventHandler_t1_1495 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainSetup System.AppDomain::getSetup()
extern "C" AppDomainSetup_t1_1497 * AppDomain_getSetup_m1_13062 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainSetup System.AppDomain::get_SetupInformationNoCopy()
extern "C" AppDomainSetup_t1_1497 * AppDomain_get_SetupInformationNoCopy_m1_13063 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainSetup System.AppDomain::get_SetupInformation()
extern "C" AppDomainSetup_t1_1497 * AppDomain_get_SetupInformation_m1_13064 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::get_BaseDirectory()
extern "C" String_t* AppDomain_get_BaseDirectory_m1_13065 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::get_RelativeSearchPath()
extern "C" String_t* AppDomain_get_RelativeSearchPath_m1_13066 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::get_DynamicDirectory()
extern "C" String_t* AppDomain_get_DynamicDirectory_m1_13067 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomain::get_ShadowCopyFiles()
extern "C" bool AppDomain_get_ShadowCopyFiles_m1_13068 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::getFriendlyName()
extern "C" String_t* AppDomain_getFriendlyName_m1_13069 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::get_FriendlyName()
extern "C" String_t* AppDomain_get_FriendlyName_m1_13070 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.AppDomain::get_Evidence()
extern "C" Evidence_t1_398 * AppDomain_get_Evidence_m1_13071 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IPrincipal System.AppDomain::get_DefaultPrincipal()
extern "C" Object_t * AppDomain_get_DefaultPrincipal_m1_13072 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.AppDomain::get_GrantedPermissionSet()
extern "C" PermissionSet_t1_563 * AppDomain_get_GrantedPermissionSet_m1_13073 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::getCurDomain()
extern "C" AppDomain_t1_1403 * AppDomain_getCurDomain_m1_13074 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
extern "C" AppDomain_t1_1403 * AppDomain_get_CurrentDomain_m1_13075 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::getRootDomain()
extern "C" AppDomain_t1_1403 * AppDomain_getRootDomain_m1_13076 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_DefaultDomain()
extern "C" AppDomain_t1_1403 * AppDomain_get_DefaultDomain_m1_13077 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::AppendPrivatePath(System.String)
extern "C" void AppDomain_AppendPrivatePath_m1_13078 (AppDomain_t1_1403 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::ClearPrivatePath()
extern "C" void AppDomain_ClearPrivatePath_m1_13079 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::ClearShadowCopyPath()
extern "C" void AppDomain_ClearShadowCopyPath_m1_13080 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.AppDomain::CreateInstance(System.String,System.String)
extern "C" ObjectHandle_t1_1019 * AppDomain_CreateInstance_m1_13081 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.AppDomain::CreateInstance(System.String,System.String,System.Object[])
extern "C" ObjectHandle_t1_1019 * AppDomain_CreateInstance_m1_13082 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.AppDomain::CreateInstance(System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" ObjectHandle_t1_1019 * AppDomain_CreateInstance_m1_13083 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::CreateInstanceAndUnwrap(System.String,System.String)
extern "C" Object_t * AppDomain_CreateInstanceAndUnwrap_m1_13084 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::CreateInstanceAndUnwrap(System.String,System.String,System.Object[])
extern "C" Object_t * AppDomain_CreateInstanceAndUnwrap_m1_13085 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::CreateInstanceAndUnwrap(System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" Object_t * AppDomain_CreateInstanceAndUnwrap_m1_13086 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.AppDomain::CreateInstanceFrom(System.String,System.String)
extern "C" ObjectHandle_t1_1019 * AppDomain_CreateInstanceFrom_m1_13087 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.AppDomain::CreateInstanceFrom(System.String,System.String,System.Object[])
extern "C" ObjectHandle_t1_1019 * AppDomain_CreateInstanceFrom_m1_13088 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, String_t* ___typeName, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.AppDomain::CreateInstanceFrom(System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" ObjectHandle_t1_1019 * AppDomain_CreateInstanceFrom_m1_13089 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::CreateInstanceFromAndUnwrap(System.String,System.String)
extern "C" Object_t * AppDomain_CreateInstanceFromAndUnwrap_m1_13090 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::CreateInstanceFromAndUnwrap(System.String,System.String,System.Object[])
extern "C" Object_t * AppDomain_CreateInstanceFromAndUnwrap_m1_13091 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::CreateInstanceFromAndUnwrap(System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" Object_t * AppDomain_CreateInstanceFromAndUnwrap_m1_13092 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13093 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.Security.Policy.Evidence)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13094 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.String)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13095 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, String_t* ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.String,System.Security.Policy.Evidence)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13096 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, String_t* ___dir, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13097 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, PermissionSet_t1_563 * ___requiredPermissions, PermissionSet_t1_563 * ___optionalPermissions, PermissionSet_t1_563 * ___refusedPermissions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.Security.Policy.Evidence,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13098 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, Evidence_t1_398 * ___evidence, PermissionSet_t1_563 * ___requiredPermissions, PermissionSet_t1_563 * ___optionalPermissions, PermissionSet_t1_563 * ___refusedPermissions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.String,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13099 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, String_t* ___dir, PermissionSet_t1_563 * ___requiredPermissions, PermissionSet_t1_563 * ___optionalPermissions, PermissionSet_t1_563 * ___refusedPermissions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.String,System.Security.Policy.Evidence,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13100 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, String_t* ___dir, Evidence_t1_398 * ___evidence, PermissionSet_t1_563 * ___requiredPermissions, PermissionSet_t1_563 * ___optionalPermissions, PermissionSet_t1_563 * ___refusedPermissions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.String,System.Security.Policy.Evidence,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet,System.Boolean)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13101 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, String_t* ___dir, Evidence_t1_398 * ___evidence, PermissionSet_t1_563 * ___requiredPermissions, PermissionSet_t1_563 * ___optionalPermissions, PermissionSet_t1_563 * ___refusedPermissions, bool ___isSynchronized, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.String,System.Security.Policy.Evidence,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet,System.Boolean,System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.CustomAttributeBuilder>)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13102 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, String_t* ___dir, Evidence_t1_398 * ___evidence, PermissionSet_t1_563 * ___requiredPermissions, PermissionSet_t1_563 * ___optionalPermissions, PermissionSet_t1_563 * ___refusedPermissions, bool ___isSynchronized, Object_t* ___assemblyAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess,System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.CustomAttributeBuilder>)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineDynamicAssembly_m1_13103 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, Object_t* ___assemblyAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.AssemblyBuilder System.AppDomain::DefineInternalDynamicAssembly(System.Reflection.AssemblyName,System.Reflection.Emit.AssemblyBuilderAccess)
extern "C" AssemblyBuilder_t1_466 * AppDomain_DefineInternalDynamicAssembly_m1_13104 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___name, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::DoCallBack(System.CrossAppDomainDelegate)
extern "C" void AppDomain_DoCallBack_m1_13105 (AppDomain_t1_1403 * __this, CrossAppDomainDelegate_t1_1631 * ___callBackDelegate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssembly(System.String)
extern "C" int32_t AppDomain_ExecuteAssembly_m1_13106 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssembly(System.String,System.Security.Policy.Evidence)
extern "C" int32_t AppDomain_ExecuteAssembly_m1_13107 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, Evidence_t1_398 * ___assemblySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssembly(System.String,System.Security.Policy.Evidence,System.String[])
extern "C" int32_t AppDomain_ExecuteAssembly_m1_13108 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, Evidence_t1_398 * ___assemblySecurity, StringU5BU5D_t1_238* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssembly(System.String,System.Security.Policy.Evidence,System.String[],System.Byte[],System.Configuration.Assemblies.AssemblyHashAlgorithm)
extern "C" int32_t AppDomain_ExecuteAssembly_m1_13109 (AppDomain_t1_1403 * __this, String_t* ___assemblyFile, Evidence_t1_398 * ___assemblySecurity, StringU5BU5D_t1_238* ___args, ByteU5BU5D_t1_109* ___hashValue, int32_t ___hashAlgorithm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssemblyInternal(System.Reflection.Assembly,System.String[])
extern "C" int32_t AppDomain_ExecuteAssemblyInternal_m1_13110 (AppDomain_t1_1403 * __this, Assembly_t1_467 * ___a, StringU5BU5D_t1_238* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssembly(System.Reflection.Assembly,System.String[])
extern "C" int32_t AppDomain_ExecuteAssembly_m1_13111 (AppDomain_t1_1403 * __this, Assembly_t1_467 * ___a, StringU5BU5D_t1_238* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly[] System.AppDomain::GetAssemblies(System.Boolean)
extern "C" AssemblyU5BU5D_t1_1738* AppDomain_GetAssemblies_m1_13112 (AppDomain_t1_1403 * __this, bool ___refOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly[] System.AppDomain::GetAssemblies()
extern "C" AssemblyU5BU5D_t1_1738* AppDomain_GetAssemblies_m1_13113 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::GetData(System.String)
extern "C" Object_t * AppDomain_GetData_m1_13114 (AppDomain_t1_1403 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.AppDomain::GetType()
extern "C" Type_t * AppDomain_GetType_m1_13115 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::InitializeLifetimeService()
extern "C" Object_t * AppDomain_InitializeLifetimeService_m1_13116 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::LoadAssembly(System.String,System.Security.Policy.Evidence,System.Boolean)
extern "C" Assembly_t1_467 * AppDomain_LoadAssembly_m1_13117 (AppDomain_t1_1403 * __this, String_t* ___assemblyRef, Evidence_t1_398 * ___securityEvidence, bool ___refOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.Reflection.AssemblyName)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13118 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___assemblyRef, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::LoadSatellite(System.Reflection.AssemblyName,System.Boolean)
extern "C" Assembly_t1_467 * AppDomain_LoadSatellite_m1_13119 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___assemblyRef, bool ___throwOnError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.Reflection.AssemblyName,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13120 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___assemblyRef, Evidence_t1_398 * ___assemblySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13121 (AppDomain_t1_1403 * __this, String_t* ___assemblyString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13122 (AppDomain_t1_1403 * __this, String_t* ___assemblyString, Evidence_t1_398 * ___assemblySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String,System.Security.Policy.Evidence,System.Boolean)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13123 (AppDomain_t1_1403 * __this, String_t* ___assemblyString, Evidence_t1_398 * ___assemblySecurity, bool ___refonly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.Byte[])
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13124 (AppDomain_t1_1403 * __this, ByteU5BU5D_t1_109* ___rawAssembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.Byte[],System.Byte[])
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13125 (AppDomain_t1_1403 * __this, ByteU5BU5D_t1_109* ___rawAssembly, ByteU5BU5D_t1_109* ___rawSymbolStore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::LoadAssemblyRaw(System.Byte[],System.Byte[],System.Security.Policy.Evidence,System.Boolean)
extern "C" Assembly_t1_467 * AppDomain_LoadAssemblyRaw_m1_13126 (AppDomain_t1_1403 * __this, ByteU5BU5D_t1_109* ___rawAssembly, ByteU5BU5D_t1_109* ___rawSymbolStore, Evidence_t1_398 * ___securityEvidence, bool ___refonly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.Byte[],System.Byte[],System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13127 (AppDomain_t1_1403 * __this, ByteU5BU5D_t1_109* ___rawAssembly, ByteU5BU5D_t1_109* ___rawSymbolStore, Evidence_t1_398 * ___securityEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.Byte[],System.Byte[],System.Security.Policy.Evidence,System.Boolean)
extern "C" Assembly_t1_467 * AppDomain_Load_m1_13128 (AppDomain_t1_1403 * __this, ByteU5BU5D_t1_109* ___rawAssembly, ByteU5BU5D_t1_109* ___rawSymbolStore, Evidence_t1_398 * ___securityEvidence, bool ___refonly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetAppDomainPolicy(System.Security.Policy.PolicyLevel)
extern "C" void AppDomain_SetAppDomainPolicy_m1_13129 (AppDomain_t1_1403 * __this, PolicyLevel_t1_1357 * ___domainPolicy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetCachePath(System.String)
extern "C" void AppDomain_SetCachePath_m1_13130 (AppDomain_t1_1403 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetPrincipalPolicy(System.Security.Principal.PrincipalPolicy)
extern "C" void AppDomain_SetPrincipalPolicy_m1_13131 (AppDomain_t1_1403 * __this, int32_t ___policy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetShadowCopyFiles()
extern "C" void AppDomain_SetShadowCopyFiles_m1_13132 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetShadowCopyPath(System.String)
extern "C" void AppDomain_SetShadowCopyPath_m1_13133 (AppDomain_t1_1403 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetThreadPrincipal(System.Security.Principal.IPrincipal)
extern "C" void AppDomain_SetThreadPrincipal_m1_13134 (AppDomain_t1_1403 * __this, Object_t * ___principal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::InternalSetDomainByID(System.Int32)
extern "C" AppDomain_t1_1403 * AppDomain_InternalSetDomainByID_m1_13135 (Object_t * __this /* static, unused */, int32_t ___domain_id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::InternalSetDomain(System.AppDomain)
extern "C" AppDomain_t1_1403 * AppDomain_InternalSetDomain_m1_13136 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::InternalPushDomainRef(System.AppDomain)
extern "C" void AppDomain_InternalPushDomainRef_m1_13137 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::InternalPushDomainRefByID(System.Int32)
extern "C" void AppDomain_InternalPushDomainRefByID_m1_13138 (Object_t * __this /* static, unused */, int32_t ___domain_id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::InternalPopDomainRef()
extern "C" void AppDomain_InternalPopDomainRef_m1_13139 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalSetContext(System.Runtime.Remoting.Contexts.Context)
extern "C" Context_t1_891 * AppDomain_InternalSetContext_m1_13140 (Object_t * __this /* static, unused */, Context_t1_891 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalGetContext()
extern "C" Context_t1_891 * AppDomain_InternalGetContext_m1_13141 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalGetDefaultContext()
extern "C" Context_t1_891 * AppDomain_InternalGetDefaultContext_m1_13142 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::InternalGetProcessGuid(System.String)
extern "C" String_t* AppDomain_InternalGetProcessGuid_m1_13143 (Object_t * __this /* static, unused */, String_t* ___newguid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::InvokeInDomain(System.AppDomain,System.Reflection.MethodInfo,System.Object,System.Object[])
extern "C" Object_t * AppDomain_InvokeInDomain_m1_13144 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, MethodInfo_t * ___method, Object_t * ___obj, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.AppDomain::InvokeInDomainByID(System.Int32,System.Reflection.MethodInfo,System.Object,System.Object[])
extern "C" Object_t * AppDomain_InvokeInDomainByID_m1_13145 (Object_t * __this /* static, unused */, int32_t ___domain_id, MethodInfo_t * ___method, Object_t * ___obj, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::GetProcessGuid()
extern "C" String_t* AppDomain_GetProcessGuid_m1_13146 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::CreateDomain(System.String)
extern "C" AppDomain_t1_1403 * AppDomain_CreateDomain_m1_13147 (Object_t * __this /* static, unused */, String_t* ___friendlyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::CreateDomain(System.String,System.Security.Policy.Evidence)
extern "C" AppDomain_t1_1403 * AppDomain_CreateDomain_m1_13148 (Object_t * __this /* static, unused */, String_t* ___friendlyName, Evidence_t1_398 * ___securityInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::createDomain(System.String,System.AppDomainSetup)
extern "C" AppDomain_t1_1403 * AppDomain_createDomain_m1_13149 (Object_t * __this /* static, unused */, String_t* ___friendlyName, AppDomainSetup_t1_1497 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::CreateDomain(System.String,System.Security.Policy.Evidence,System.AppDomainSetup)
extern "C" AppDomain_t1_1403 * AppDomain_CreateDomain_m1_13150 (Object_t * __this /* static, unused */, String_t* ___friendlyName, Evidence_t1_398 * ___securityInfo, AppDomainSetup_t1_1497 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::CreateDomain(System.String,System.Security.Policy.Evidence,System.String,System.String,System.Boolean)
extern "C" AppDomain_t1_1403 * AppDomain_CreateDomain_m1_13151 (Object_t * __this /* static, unused */, String_t* ___friendlyName, Evidence_t1_398 * ___securityInfo, String_t* ___appBasePath, String_t* ___appRelativeSearchPath, bool ___shadowCopyFiles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainSetup System.AppDomain::CreateDomainSetup(System.String,System.String,System.Boolean)
extern "C" AppDomainSetup_t1_1497 * AppDomain_CreateDomainSetup_m1_13152 (Object_t * __this /* static, unused */, String_t* ___appBasePath, String_t* ___appRelativeSearchPath, bool ___shadowCopyFiles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomain::InternalIsFinalizingForUnload(System.Int32)
extern "C" bool AppDomain_InternalIsFinalizingForUnload_m1_13153 (Object_t * __this /* static, unused */, int32_t ___domain_id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomain::IsFinalizingForUnload()
extern "C" bool AppDomain_IsFinalizingForUnload_m1_13154 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::InternalUnload(System.Int32)
extern "C" void AppDomain_InternalUnload_m1_13155 (Object_t * __this /* static, unused */, int32_t ___domain_id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::getDomainID()
extern "C" int32_t AppDomain_getDomainID_m1_13156 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::Unload(System.AppDomain)
extern "C" void AppDomain_Unload_m1_13157 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetData(System.String,System.Object)
extern "C" void AppDomain_SetData_m1_13158 (AppDomain_t1_1403 * __this, String_t* ___name, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::SetData(System.String,System.Object,System.Security.IPermission)
extern "C" void AppDomain_SetData_m1_13159 (AppDomain_t1_1403 * __this, String_t* ___name, Object_t * ___data, Object_t * ___permission, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::GetCurrentThreadId()
extern "C" int32_t AppDomain_GetCurrentThreadId_m1_13160 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::ToString()
extern "C" String_t* AppDomain_ToString_m1_13161 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::ValidateAssemblyName(System.String)
extern "C" void AppDomain_ValidateAssemblyName_m1_13162 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::DoAssemblyLoad(System.Reflection.Assembly)
extern "C" void AppDomain_DoAssemblyLoad_m1_13163 (AppDomain_t1_1403 * __this, Assembly_t1_467 * ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::DoAssemblyResolve(System.String,System.Boolean)
extern "C" Assembly_t1_467 * AppDomain_DoAssemblyResolve_m1_13164 (AppDomain_t1_1403 * __this, String_t* ___name, bool ___refonly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::DoTypeResolve(System.Object)
extern "C" Assembly_t1_467 * AppDomain_DoTypeResolve_m1_13165 (AppDomain_t1_1403 * __this, Object_t * ___name_or_tb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::DoDomainUnload()
extern "C" void AppDomain_DoDomainUnload_m1_13166 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::ProcessMessageInDomain(System.Byte[],System.Runtime.Remoting.Messaging.CADMethodCallMessage,System.Byte[]&,System.Runtime.Remoting.Messaging.CADMethodReturnMessage&)
extern "C" void AppDomain_ProcessMessageInDomain_m1_13167 (AppDomain_t1_1403 * __this, ByteU5BU5D_t1_109* ___arrRequest, CADMethodCallMessage_t1_925 * ___cadMsg, ByteU5BU5D_t1_109** ___arrResponse, CADMethodReturnMessage_t1_881 ** ___cadMrm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomainManager System.AppDomain::get_DomainManager()
extern "C" AppDomainManager_t1_1493 * AppDomain_get_DomainManager_m1_13168 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ActivationContext System.AppDomain::get_ActivationContext()
extern "C" ActivationContext_t1_717 * AppDomain_get_ActivationContext_m1_13169 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationIdentity System.AppDomain::get_ApplicationIdentity()
extern "C" ApplicationIdentity_t1_718 * AppDomain_get_ApplicationIdentity_m1_13170 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::get_Id()
extern "C" int32_t AppDomain_get_Id_m1_13171 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::ApplyPolicy(System.String)
extern "C" String_t* AppDomain_ApplyPolicy_m1_13172 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::CreateDomain(System.String,System.Security.Policy.Evidence,System.String,System.String,System.Boolean,System.AppDomainInitializer,System.String[])
extern "C" AppDomain_t1_1403 * AppDomain_CreateDomain_m1_13173 (Object_t * __this /* static, unused */, String_t* ___friendlyName, Evidence_t1_398 * ___securityInfo, String_t* ___appBasePath, String_t* ___appRelativeSearchPath, bool ___shadowCopyFiles, AppDomainInitializer_t1_1498 * ___adInit, StringU5BU5D_t1_238* ___adInitArgs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssemblyByName(System.String)
extern "C" int32_t AppDomain_ExecuteAssemblyByName_m1_13174 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssemblyByName(System.String,System.Security.Policy.Evidence)
extern "C" int32_t AppDomain_ExecuteAssemblyByName_m1_13175 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, Evidence_t1_398 * ___assemblySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssemblyByName(System.String,System.Security.Policy.Evidence,System.String[])
extern "C" int32_t AppDomain_ExecuteAssemblyByName_m1_13176 (AppDomain_t1_1403 * __this, String_t* ___assemblyName, Evidence_t1_398 * ___assemblySecurity, StringU5BU5D_t1_238* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.AppDomain::ExecuteAssemblyByName(System.Reflection.AssemblyName,System.Security.Policy.Evidence,System.String[])
extern "C" int32_t AppDomain_ExecuteAssemblyByName_m1_13177 (AppDomain_t1_1403 * __this, AssemblyName_t1_576 * ___assemblyName, Evidence_t1_398 * ___assemblySecurity, StringU5BU5D_t1_238* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AppDomain::IsDefaultAppDomain()
extern "C" bool AppDomain_IsDefaultAppDomain_m1_13178 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly[] System.AppDomain::ReflectionOnlyGetAssemblies()
extern "C" AssemblyU5BU5D_t1_1738* AppDomain_ReflectionOnlyGetAssemblies_m1_13179 (AppDomain_t1_1403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
