﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.RemotingServices/CACD
struct CACD_t1_1026;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.RemotingServices/CACD::.ctor()
extern "C" void CACD__ctor_m1_9177 (CACD_t1_1026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
