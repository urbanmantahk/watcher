﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Diagnostics_DebuggerBrowsableState.h"

// System.Diagnostics.DebuggerBrowsableState
struct  DebuggerBrowsableState_t1_327 
{
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___1;
};
