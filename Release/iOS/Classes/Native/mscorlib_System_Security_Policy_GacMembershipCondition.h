﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// System.Security.Policy.GacMembershipCondition
struct  GacMembershipCondition_t1_1349  : public Object_t
{
	// System.Int32 System.Security.Policy.GacMembershipCondition::version
	int32_t ___version_0;
};
