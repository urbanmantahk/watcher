﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.SearchPattern/Op
struct Op_t1_440;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_SearchPattern_OpCode.h"

// System.Void System.IO.SearchPattern/Op::.ctor(System.IO.SearchPattern/OpCode)
extern "C" void Op__ctor_m1_5151 (Op_t1_440 * __this, int32_t ___code, const MethodInfo* method) IL2CPP_METHOD_ATTR;
