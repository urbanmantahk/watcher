﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.ErrorMessage
struct ErrorMessage_t1_938;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.ErrorMessage::.ctor()
extern "C" void ErrorMessage__ctor_m1_8378 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.ErrorMessage::get_ArgCount()
extern "C" int32_t ErrorMessage_get_ArgCount_m1_8379 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.ErrorMessage::get_Args()
extern "C" ObjectU5BU5D_t1_272* ErrorMessage_get_Args_m1_8380 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.ErrorMessage::get_HasVarArgs()
extern "C" bool ErrorMessage_get_HasVarArgs_m1_8381 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.ErrorMessage::get_MethodBase()
extern "C" MethodBase_t1_335 * ErrorMessage_get_MethodBase_m1_8382 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.ErrorMessage::get_MethodName()
extern "C" String_t* ErrorMessage_get_MethodName_m1_8383 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.ErrorMessage::get_MethodSignature()
extern "C" Object_t * ErrorMessage_get_MethodSignature_m1_8384 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.ErrorMessage::get_Properties()
extern "C" Object_t * ErrorMessage_get_Properties_m1_8385 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.ErrorMessage::get_TypeName()
extern "C" String_t* ErrorMessage_get_TypeName_m1_8386 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.ErrorMessage::get_Uri()
extern "C" String_t* ErrorMessage_get_Uri_m1_8387 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.ErrorMessage::set_Uri(System.String)
extern "C" void ErrorMessage_set_Uri_m1_8388 (ErrorMessage_t1_938 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.ErrorMessage::GetArg(System.Int32)
extern "C" Object_t * ErrorMessage_GetArg_m1_8389 (ErrorMessage_t1_938 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.ErrorMessage::GetArgName(System.Int32)
extern "C" String_t* ErrorMessage_GetArgName_m1_8390 (ErrorMessage_t1_938 * __this, int32_t ___arg_num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.ErrorMessage::get_InArgCount()
extern "C" int32_t ErrorMessage_get_InArgCount_m1_8391 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.ErrorMessage::GetInArgName(System.Int32)
extern "C" String_t* ErrorMessage_GetInArgName_m1_8392 (ErrorMessage_t1_938 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.ErrorMessage::GetInArg(System.Int32)
extern "C" Object_t * ErrorMessage_GetInArg_m1_8393 (ErrorMessage_t1_938 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.ErrorMessage::get_InArgs()
extern "C" ObjectU5BU5D_t1_272* ErrorMessage_get_InArgs_m1_8394 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.ErrorMessage::get_LogicalCallContext()
extern "C" LogicalCallContext_t1_941 * ErrorMessage_get_LogicalCallContext_m1_8395 (ErrorMessage_t1_938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
