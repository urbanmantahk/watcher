﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t1_406;
// System.Text.Decoder
struct Decoder_t1_407;

#include "mscorlib_System_Object.h"

// System.Text.MLangCodePageEncoding/MLangDecoder
struct  MLangDecoder_t1_1441  : public Object_t
{
	// System.Text.Encoding System.Text.MLangCodePageEncoding/MLangDecoder::encoding
	Encoding_t1_406 * ___encoding_0;
	// System.Text.Decoder System.Text.MLangCodePageEncoding/MLangDecoder::realObject
	Decoder_t1_407 * ___realObject_1;
};
