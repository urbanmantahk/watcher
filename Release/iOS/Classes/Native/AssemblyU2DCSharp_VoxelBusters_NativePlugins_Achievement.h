﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion
struct ReportProgressCompletion_t8_220;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"

// VoxelBusters.NativePlugins.Achievement
struct  Achievement_t8_221  : public NPObject_t8_222
{
	// VoxelBusters.NativePlugins.Achievement/ReportProgressCompletion VoxelBusters.NativePlugins.Achievement::ReportProgressFinishedEvent
	ReportProgressCompletion_t8_220 * ___ReportProgressFinishedEvent_1;
	// System.String VoxelBusters.NativePlugins.Achievement::<GlobalIdentifier>k__BackingField
	String_t* ___U3CGlobalIdentifierU3Ek__BackingField_2;
};
