﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.DBNull
struct DBNull_t1_1523;

#include "mscorlib_System_Object.h"

// System.DBNull
struct  DBNull_t1_1523  : public Object_t
{
};
struct DBNull_t1_1523_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t1_1523 * ___Value_0;
};
