﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"

// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct  MethodCallDictionary_t1_944  : public MethodDictionary_t1_934
{
};
struct MethodCallDictionary_t1_944_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallDictionary::InternalKeys
	StringU5BU5D_t1_238* ___InternalKeys_6;
};
