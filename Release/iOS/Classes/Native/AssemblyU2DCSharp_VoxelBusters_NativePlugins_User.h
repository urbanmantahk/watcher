﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"

// VoxelBusters.NativePlugins.User
struct  User_t8_235  : public NPObject_t8_222
{
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.User::m_image
	Texture2D_t6_33 * ___m_image_1;
	// VoxelBusters.Utility.DownloadTexture/Completion VoxelBusters.NativePlugins.User::DownloadImageFinishedEvent
	Completion_t8_161 * ___DownloadImageFinishedEvent_2;
};
