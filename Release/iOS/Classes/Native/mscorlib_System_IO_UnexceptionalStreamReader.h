﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Boolean[]
struct BooleanU5BU5D_t1_458;

#include "mscorlib_System_IO_StreamReader.h"

// System.IO.UnexceptionalStreamReader
struct  UnexceptionalStreamReader_t1_457  : public StreamReader_t1_447
{
};
struct UnexceptionalStreamReader_t1_457_StaticFields{
	// System.Boolean[] System.IO.UnexceptionalStreamReader::newline
	BooleanU5BU5D_t1_458* ___newline_17;
	// System.Char System.IO.UnexceptionalStreamReader::newlineChar
	uint16_t ___newlineChar_18;
};
