﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.EventWaitHandleAuditRule
struct EventWaitHandleAuditRule_t1_1150;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleRights.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.EventWaitHandleAuditRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.EventWaitHandleRights,System.Security.AccessControl.AuditFlags)
extern "C" void EventWaitHandleAuditRule__ctor_m1_9849 (EventWaitHandleAuditRule_t1_1150 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.EventWaitHandleRights System.Security.AccessControl.EventWaitHandleAuditRule::get_EventWaitHandleRights()
extern "C" int32_t EventWaitHandleAuditRule_get_EventWaitHandleRights_m1_9850 (EventWaitHandleAuditRule_t1_1150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
