﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_XmlFieldOrderOptio.h"

// System.Runtime.Remoting.Metadata.XmlFieldOrderOption
struct  XmlFieldOrderOption_t1_1003 
{
	// System.Int32 System.Runtime.Remoting.Metadata.XmlFieldOrderOption::value__
	int32_t ___value___1;
};
