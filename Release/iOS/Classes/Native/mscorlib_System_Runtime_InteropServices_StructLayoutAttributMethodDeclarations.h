﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.StructLayoutAttribute
struct StructLayoutAttribute_t1_64;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_LayoutKind.h"

// System.Void System.Runtime.InteropServices.StructLayoutAttribute::.ctor(System.Int16)
extern "C" void StructLayoutAttribute__ctor_m1_1341 (StructLayoutAttribute_t1_64 * __this, int16_t ___layoutKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.StructLayoutAttribute::.ctor(System.Runtime.InteropServices.LayoutKind)
extern "C" void StructLayoutAttribute__ctor_m1_1342 (StructLayoutAttribute_t1_64 * __this, int32_t ___layoutKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.LayoutKind System.Runtime.InteropServices.StructLayoutAttribute::get_Value()
extern "C" int32_t StructLayoutAttribute_get_Value_m1_1343 (StructLayoutAttribute_t1_64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
