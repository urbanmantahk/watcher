﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "mscorlib_System_MulticastDelegate.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>
struct  Transform_1_t1_2671  : public MulticastDelegate_t1_21
{
};
