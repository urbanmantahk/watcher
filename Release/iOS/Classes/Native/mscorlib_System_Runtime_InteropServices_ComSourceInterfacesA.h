﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.ComSourceInterfacesAttribute
struct  ComSourceInterfacesAttribute_t1_775  : public Attribute_t1_2
{
	// System.String System.Runtime.InteropServices.ComSourceInterfacesAttribute::internalValue
	String_t* ___internalValue_0;
};
