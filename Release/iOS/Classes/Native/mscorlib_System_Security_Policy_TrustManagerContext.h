﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Security_Policy_TrustManagerUIContext.h"

// System.Security.Policy.TrustManagerContext
struct  TrustManagerContext_t1_1365  : public Object_t
{
	// System.Boolean System.Security.Policy.TrustManagerContext::_ignorePersistedDecision
	bool ____ignorePersistedDecision_0;
	// System.Boolean System.Security.Policy.TrustManagerContext::_noPrompt
	bool ____noPrompt_1;
	// System.Boolean System.Security.Policy.TrustManagerContext::_keepAlive
	bool ____keepAlive_2;
	// System.Boolean System.Security.Policy.TrustManagerContext::_persist
	bool ____persist_3;
	// System.ApplicationIdentity System.Security.Policy.TrustManagerContext::_previousId
	ApplicationIdentity_t1_718 * ____previousId_4;
	// System.Security.Policy.TrustManagerUIContext System.Security.Policy.TrustManagerContext::_ui
	int32_t ____ui_5;
};
