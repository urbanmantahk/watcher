﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_System_Xml_XmlCharacterData.h"

// System.Xml.XmlSignificantWhitespace
struct  XmlSignificantWhitespace_t4_165  : public XmlCharacterData_t4_125
{
};
