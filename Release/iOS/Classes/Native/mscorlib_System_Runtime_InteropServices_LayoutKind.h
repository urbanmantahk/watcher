﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_LayoutKind.h"

// System.Runtime.InteropServices.LayoutKind
struct  LayoutKind_t1_810 
{
	// System.Int32 System.Runtime.InteropServices.LayoutKind::value__
	int32_t ___value___1;
};
