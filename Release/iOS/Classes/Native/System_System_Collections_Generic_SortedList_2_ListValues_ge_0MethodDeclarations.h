﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>
struct ListValues_t3_282;
// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// ExifLibrary.IFD[]
struct IFDU5BU5D_t8_387;
// System.Collections.Generic.IEnumerator`1<ExifLibrary.IFD>
struct IEnumerator_1_t1_2882;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void ListValues__ctor_m3_2197_gshared (ListValues_t3_282 * __this, SortedList_2_t3_248 * ___host, const MethodInfo* method);
#define ListValues__ctor_m3_2197(__this, ___host, method) (( void (*) (ListValues_t3_282 *, SortedList_2_t3_248 *, const MethodInfo*))ListValues__ctor_m3_2197_gshared)(__this, ___host, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ListValues_System_Collections_IEnumerable_GetEnumerator_m3_2198_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_System_Collections_IEnumerable_GetEnumerator_m3_2198(__this, method) (( Object_t * (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_System_Collections_IEnumerable_GetEnumerator_m3_2198_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Add(TValue)
extern "C" void ListValues_Add_m3_2199_gshared (ListValues_t3_282 * __this, int32_t ___item, const MethodInfo* method);
#define ListValues_Add_m3_2199(__this, ___item, method) (( void (*) (ListValues_t3_282 *, int32_t, const MethodInfo*))ListValues_Add_m3_2199_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Remove(TValue)
extern "C" bool ListValues_Remove_m3_2200_gshared (ListValues_t3_282 * __this, int32_t ___value, const MethodInfo* method);
#define ListValues_Remove_m3_2200(__this, ___value, method) (( bool (*) (ListValues_t3_282 *, int32_t, const MethodInfo*))ListValues_Remove_m3_2200_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Clear()
extern "C" void ListValues_Clear_m3_2201_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_Clear_m3_2201(__this, method) (( void (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_Clear_m3_2201_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::CopyTo(TValue[],System.Int32)
extern "C" void ListValues_CopyTo_m3_2202_gshared (ListValues_t3_282 * __this, IFDU5BU5D_t8_387* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ListValues_CopyTo_m3_2202(__this, ___array, ___arrayIndex, method) (( void (*) (ListValues_t3_282 *, IFDU5BU5D_t8_387*, int32_t, const MethodInfo*))ListValues_CopyTo_m3_2202_gshared)(__this, ___array, ___arrayIndex, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Contains(TValue)
extern "C" bool ListValues_Contains_m3_2203_gshared (ListValues_t3_282 * __this, int32_t ___item, const MethodInfo* method);
#define ListValues_Contains_m3_2203(__this, ___item, method) (( bool (*) (ListValues_t3_282 *, int32_t, const MethodInfo*))ListValues_Contains_m3_2203_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::IndexOf(TValue)
extern "C" int32_t ListValues_IndexOf_m3_2204_gshared (ListValues_t3_282 * __this, int32_t ___item, const MethodInfo* method);
#define ListValues_IndexOf_m3_2204(__this, ___item, method) (( int32_t (*) (ListValues_t3_282 *, int32_t, const MethodInfo*))ListValues_IndexOf_m3_2204_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Insert(System.Int32,TValue)
extern "C" void ListValues_Insert_m3_2205_gshared (ListValues_t3_282 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define ListValues_Insert_m3_2205(__this, ___index, ___item, method) (( void (*) (ListValues_t3_282 *, int32_t, int32_t, const MethodInfo*))ListValues_Insert_m3_2205_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::RemoveAt(System.Int32)
extern "C" void ListValues_RemoveAt_m3_2206_gshared (ListValues_t3_282 * __this, int32_t ___index, const MethodInfo* method);
#define ListValues_RemoveAt_m3_2206(__this, ___index, method) (( void (*) (ListValues_t3_282 *, int32_t, const MethodInfo*))ListValues_RemoveAt_m3_2206_gshared)(__this, ___index, method)
// TValue System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_Item(System.Int32)
extern "C" int32_t ListValues_get_Item_m3_2207_gshared (ListValues_t3_282 * __this, int32_t ___index, const MethodInfo* method);
#define ListValues_get_Item_m3_2207(__this, ___index, method) (( int32_t (*) (ListValues_t3_282 *, int32_t, const MethodInfo*))ListValues_get_Item_m3_2207_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::set_Item(System.Int32,TValue)
extern "C" void ListValues_set_Item_m3_2208_gshared (ListValues_t3_282 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define ListValues_set_Item_m3_2208(__this, ___index, ___value, method) (( void (*) (ListValues_t3_282 *, int32_t, int32_t, const MethodInfo*))ListValues_set_Item_m3_2208_gshared)(__this, ___index, ___value, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::GetEnumerator()
extern "C" Object_t* ListValues_GetEnumerator_m3_2209_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_GetEnumerator_m3_2209(__this, method) (( Object_t* (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_GetEnumerator_m3_2209_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_Count()
extern "C" int32_t ListValues_get_Count_m3_2210_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_get_Count_m3_2210(__this, method) (( int32_t (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_get_Count_m3_2210_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_IsSynchronized()
extern "C" bool ListValues_get_IsSynchronized_m3_2211_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_get_IsSynchronized_m3_2211(__this, method) (( bool (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_get_IsSynchronized_m3_2211_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_IsReadOnly()
extern "C" bool ListValues_get_IsReadOnly_m3_2212_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_get_IsReadOnly_m3_2212(__this, method) (( bool (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_get_IsReadOnly_m3_2212_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_SyncRoot()
extern "C" Object_t * ListValues_get_SyncRoot_m3_2213_gshared (ListValues_t3_282 * __this, const MethodInfo* method);
#define ListValues_get_SyncRoot_m3_2213(__this, method) (( Object_t * (*) (ListValues_t3_282 *, const MethodInfo*))ListValues_get_SyncRoot_m3_2213_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::CopyTo(System.Array,System.Int32)
extern "C" void ListValues_CopyTo_m3_2214_gshared (ListValues_t3_282 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ListValues_CopyTo_m3_2214(__this, ___array, ___arrayIndex, method) (( void (*) (ListValues_t3_282 *, Array_t *, int32_t, const MethodInfo*))ListValues_CopyTo_m3_2214_gshared)(__this, ___array, ___arrayIndex, method)
