﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ParameterInfo
struct ParameterInfo_t1_627;
// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1_545;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type[]
struct TypeU5BU5D_t1_31;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"

// System.Void System.Reflection.ParameterInfo::.ctor()
extern "C" void ParameterInfo__ctor_m1_7221 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.Emit.ParameterBuilder,System.Type,System.Reflection.MemberInfo,System.Int32)
extern "C" void ParameterInfo__ctor_m1_7222 (ParameterInfo_t1_627 * __this, ParameterBuilder_t1_545 * ___pb, Type_t * ___type, MemberInfo_t * ___member, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::.ctor(System.Reflection.ParameterInfo,System.Reflection.MemberInfo)
extern "C" void ParameterInfo__ctor_m1_7223 (ParameterInfo_t1_627 * __this, ParameterInfo_t1_627 * ___pinfo, MemberInfo_t * ___member, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::.ctor(System.Type,System.Reflection.MemberInfo,System.Reflection.Emit.UnmanagedMarshal)
extern "C" void ParameterInfo__ctor_m1_7224 (ParameterInfo_t1_627 * __this, Type_t * ___type, MemberInfo_t * ___member, UnmanagedMarshal_t1_505 * ___marshalAs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::System.Runtime.InteropServices._ParameterInfo.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ParameterInfo_System_Runtime_InteropServices__ParameterInfo_GetIDsOfNames_m1_7225 (ParameterInfo_t1_627 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::System.Runtime.InteropServices._ParameterInfo.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ParameterInfo_System_Runtime_InteropServices__ParameterInfo_GetTypeInfo_m1_7226 (ParameterInfo_t1_627 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::System.Runtime.InteropServices._ParameterInfo.GetTypeInfoCount(System.UInt32&)
extern "C" void ParameterInfo_System_Runtime_InteropServices__ParameterInfo_GetTypeInfoCount_m1_7227 (ParameterInfo_t1_627 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ParameterInfo::System.Runtime.InteropServices._ParameterInfo.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void ParameterInfo_System_Runtime_InteropServices__ParameterInfo_Invoke_m1_7228 (ParameterInfo_t1_627 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.ParameterInfo::ToString()
extern "C" String_t* ParameterInfo_ToString_m1_7229 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.ParameterInfo::get_ParameterType()
extern "C" Type_t * ParameterInfo_get_ParameterType_m1_7230 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::get_Attributes()
extern "C" int32_t ParameterInfo_get_Attributes_m1_7231 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ParameterInfo::get_DefaultValue()
extern "C" Object_t * ParameterInfo_get_DefaultValue_m1_7232 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterInfo::get_IsIn()
extern "C" bool ParameterInfo_get_IsIn_m1_7233 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterInfo::get_IsLcid()
extern "C" bool ParameterInfo_get_IsLcid_m1_7234 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterInfo::get_IsOptional()
extern "C" bool ParameterInfo_get_IsOptional_m1_7235 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterInfo::get_IsOut()
extern "C" bool ParameterInfo_get_IsOut_m1_7236 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterInfo::get_IsRetval()
extern "C" bool ParameterInfo_get_IsRetval_m1_7237 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Reflection.ParameterInfo::get_Member()
extern "C" MemberInfo_t * ParameterInfo_get_Member_m1_7238 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.ParameterInfo::get_Name()
extern "C" String_t* ParameterInfo_get_Name_m1_7239 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ParameterInfo::get_Position()
extern "C" int32_t ParameterInfo_get_Position_m1_7240 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ParameterInfo::GetMetadataToken()
extern "C" int32_t ParameterInfo_GetMetadataToken_m1_7241 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ParameterInfo::get_MetadataToken()
extern "C" int32_t ParameterInfo_get_MetadataToken_m1_7242 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.ParameterInfo::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* ParameterInfo_GetCustomAttributes_m1_7243 (ParameterInfo_t1_627 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.ParameterInfo::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* ParameterInfo_GetCustomAttributes_m1_7244 (ParameterInfo_t1_627 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ParameterInfo::IsDefined(System.Type,System.Boolean)
extern "C" bool ParameterInfo_IsDefined_m1_7245 (ParameterInfo_t1_627 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.ParameterInfo::GetPseudoCustomAttributes()
extern "C" ObjectU5BU5D_t1_272* ParameterInfo_GetPseudoCustomAttributes_m1_7246 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.ParameterInfo::GetTypeModifiers(System.Boolean)
extern "C" TypeU5BU5D_t1_31* ParameterInfo_GetTypeModifiers_m1_7247 (ParameterInfo_t1_627 * __this, bool ___optional, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.ParameterInfo::GetOptionalCustomModifiers()
extern "C" TypeU5BU5D_t1_31* ParameterInfo_GetOptionalCustomModifiers_m1_7248 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.ParameterInfo::GetRequiredCustomModifiers()
extern "C" TypeU5BU5D_t1_31* ParameterInfo_GetRequiredCustomModifiers_m1_7249 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ParameterInfo::get_RawDefaultValue()
extern "C" Object_t * ParameterInfo_get_RawDefaultValue_m1_7250 (ParameterInfo_t1_627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
