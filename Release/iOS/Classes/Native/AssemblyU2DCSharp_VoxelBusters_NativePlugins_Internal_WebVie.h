﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView>
struct Dictionary_2_t1_1912;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.Internal.WebViewNative
struct  WebViewNative_t8_311  : public MonoBehaviour_t6_91
{
	// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.WebView> VoxelBusters.NativePlugins.Internal.WebViewNative::m_webviewCollection
	Dictionary_2_t1_1912 * ___m_webviewCollection_10;
};
