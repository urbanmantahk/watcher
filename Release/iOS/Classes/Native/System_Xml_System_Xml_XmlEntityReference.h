﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;

#include "System_Xml_System_Xml_XmlLinkedNode.h"

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t4_136  : public XmlLinkedNode_t4_118
{
	// System.String System.Xml.XmlEntityReference::entityName
	String_t* ___entityName_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastLinkedChild
	XmlLinkedNode_t4_118 * ___lastLinkedChild_7;
};
