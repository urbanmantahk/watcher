﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8
struct  U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168  : public Object_t
{
	// System.String VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8::_tagName
	String_t* ____tagName_0;
};
