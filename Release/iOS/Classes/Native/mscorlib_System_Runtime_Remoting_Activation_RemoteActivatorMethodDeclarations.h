﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Activation.RemoteActivator
struct RemoteActivator_t1_857;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1_851;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivatorLevel.h"

// System.Void System.Runtime.Remoting.Activation.RemoteActivator::.ctor()
extern "C" void RemoteActivator__ctor_m1_7971 (RemoteActivator_t1_857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.RemoteActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" Object_t * RemoteActivator_Activate_m1_7972 (RemoteActivator_t1_857 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Activation.RemoteActivator::InitializeLifetimeService()
extern "C" Object_t * RemoteActivator_InitializeLifetimeService_m1_7973 (RemoteActivator_t1_857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.RemoteActivator::get_Level()
extern "C" int32_t RemoteActivator_get_Level_m1_7974 (RemoteActivator_t1_857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.RemoteActivator::get_NextActivator()
extern "C" Object_t * RemoteActivator_get_NextActivator_m1_7975 (RemoteActivator_t1_857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Activation.RemoteActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void RemoteActivator_set_NextActivator_m1_7976 (RemoteActivator_t1_857 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
