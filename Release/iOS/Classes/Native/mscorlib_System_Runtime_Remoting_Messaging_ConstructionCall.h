﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1_851;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.IList
struct IList_t1_262;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Runtime.Remoting.Proxies.RemotingProxy
struct RemotingProxy_t1_932;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"

// System.Runtime.Remoting.Messaging.ConstructionCall
struct  ConstructionCall_t1_930  : public MethodCall_t1_931
{
	// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Messaging.ConstructionCall::_activator
	Object_t * ____activator_13;
	// System.Object[] System.Runtime.Remoting.Messaging.ConstructionCall::_activationAttributes
	ObjectU5BU5D_t1_272* ____activationAttributes_14;
	// System.Collections.IList System.Runtime.Remoting.Messaging.ConstructionCall::_contextProperties
	Object_t * ____contextProperties_15;
	// System.Type System.Runtime.Remoting.Messaging.ConstructionCall::_activationType
	Type_t * ____activationType_16;
	// System.String System.Runtime.Remoting.Messaging.ConstructionCall::_activationTypeName
	String_t* ____activationTypeName_17;
	// System.Boolean System.Runtime.Remoting.Messaging.ConstructionCall::_isContextOk
	bool ____isContextOk_18;
	// System.Runtime.Remoting.Proxies.RemotingProxy System.Runtime.Remoting.Messaging.ConstructionCall::_sourceProxy
	RemotingProxy_t1_932 * ____sourceProxy_19;
};
struct ConstructionCall_t1_930_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Remoting.Messaging.ConstructionCall::<>f__switch$map20
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map20_20;
};
