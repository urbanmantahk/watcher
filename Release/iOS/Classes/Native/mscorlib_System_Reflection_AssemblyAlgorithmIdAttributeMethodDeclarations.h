﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyAlgorithmIdAttribute
struct AssemblyAlgorithmIdAttribute_t1_564;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"

// System.Void System.Reflection.AssemblyAlgorithmIdAttribute::.ctor(System.Configuration.Assemblies.AssemblyHashAlgorithm)
extern "C" void AssemblyAlgorithmIdAttribute__ctor_m1_6667 (AssemblyAlgorithmIdAttribute_t1_564 * __this, int32_t ___algorithmId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyAlgorithmIdAttribute::.ctor(System.UInt32)
extern "C" void AssemblyAlgorithmIdAttribute__ctor_m1_6668 (AssemblyAlgorithmIdAttribute_t1_564 * __this, uint32_t ___algorithmId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Reflection.AssemblyAlgorithmIdAttribute::get_AlgorithmId()
extern "C" uint32_t AssemblyAlgorithmIdAttribute_get_AlgorithmId_m1_6669 (AssemblyAlgorithmIdAttribute_t1_564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
