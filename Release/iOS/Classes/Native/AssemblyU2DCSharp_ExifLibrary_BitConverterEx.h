﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"

// ExifLibrary.BitConverterEx
struct  BitConverterEx_t8_61  : public Object_t
{
	// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.BitConverterEx::mFrom
	int32_t ___mFrom_0;
	// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.BitConverterEx::mTo
	int32_t ___mTo_1;
};
