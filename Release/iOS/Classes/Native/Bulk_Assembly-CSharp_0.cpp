﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// Boomlagoon.JSON.JSONValue
struct JSONValue_t8_4;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// Boomlagoon.JSON.JSONArray
struct JSONArray_t8_6;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<Boomlagoon.JSON.JSONValue>
struct IEnumerator_1_t1_1914;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1_1742;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1_1894;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>
struct IEnumerator_1_t1_1916;
// AlertControl/<CheckInServer>c__Iterator0
struct U3CCheckInServerU3Ec__Iterator0_t8_8;
// AlertControl
struct AlertControl_t8_9;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// System.Action
struct Action_t5_11;
// LoginControl/<LoginRoute>c__Iterator1
struct U3CLoginRouteU3Ec__Iterator1_t8_10;
// LoginControl
struct LoginControl_t8_11;
// TimerSetting/<UpdateTimerToServer>c__Iterator2
struct U3CUpdateTimerToServerU3Ec__Iterator2_t8_12;
// TimerSetting
struct TimerSetting_t8_13;
// VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow
struct DemoGUIWindow_t8_14;
// VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu
struct DemoMainMenu_t8_16;
// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu[]
struct DemoSubMenuU5BU5D_t8_17;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu
struct DemoSubMenu_t8_18;
// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;
// VoxelBusters.AssetStoreProductUtility.Internal.Constants
struct Constants_t8_20;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct
struct AssetStoreProduct_t8_22;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// VoxelBusters.Utility.AndroidManifestGenerator
struct AndroidManifestGenerator_t8_25;
// VoxelBusters.Utility.ShaderUtility/ShaderInfo
struct ShaderInfo_t8_26;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>
struct List_1_t1_1900;
// UnityEngine.Shader
struct Shader_t6_71;
// VoxelBusters.Utility.ShaderUtility/ShaderProperty
struct ShaderProperty_t8_27;
// VoxelBusters.Utility.ShaderUtility
struct ShaderUtility_t8_29;
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>
struct List_1_t1_1901;
// UnityEngine.Material
struct Material_t6_72;
// System.Enum
struct Enum_t1_24;
// UnityEngine.GameObject
struct GameObject_t6_97;
// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.Transform
struct Transform_t6_65;
// System.Collections.IList
struct IList_t1_262;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Uri
struct Uri_t3_3;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.IO.FileInfo
struct FileInfo_t1_422;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_91;
// VoxelBusters.Utility.PlayerSettings
struct PlayerSettings_t8_39;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3
struct U3CTakeScreenshotU3Ec__Iterator3_t8_43;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t1_1902;
// UnityEngine.Color32[]
struct Color32U5BU5D_t6_280;
// VoxelBusters.Utility.FileOperations
struct FileOperations_t8_48;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.StreamWriter
struct StreamWriter_t1_448;
// VoxelBusters.Utility.Demo.JSONDemo
struct JSONDemo_t8_49;
// VoxelBusters.Utility.Demo.PlistDemo
struct PlistDemo_t8_50;
// VoxelBusters.Utility.Internal.JSONConstants
struct JSONConstants_t8_53;
// VoxelBusters.Utility.JSONReader
struct JSONReader_t8_56;
// VoxelBusters.Utility.JSONWriter
struct JSONWriter_t8_58;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.Array
struct Array_t;
// VoxelBusters.Utility.Plist
struct Plist_t8_51;
// System.Xml.XmlNode
struct XmlNode_t4_116;
// System.Xml.XmlWriter
struct XmlWriter_t4_182;
// VoxelBusters.Utility.NativeBinding
struct NativeBinding_t8_59;
// ExifLibrary.BitConverterEx
struct BitConverterEx_t8_61;
// ExifLibrary.ExifBitConverter
struct ExifBitConverter_t8_62;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;
// ExifLibrary.MathEx/Fraction32[]
struct Fraction32U5BU5D_t8_129;
// ExifLibrary.UnknownIFDSectionException
struct UnknownIFDSectionException_t8_95;
// ExifLibrary.UnknownEnumTypeException
struct UnknownEnumTypeException_t8_96;
// ExifLibrary.IFD0IsEmptyException
struct IFD0IsEmptyException_t8_97;
// ExifLibrary.ExifEncodedString
struct ExifEncodedString_t8_98;
// System.Text.Encoding
struct Encoding_t1_406;
// ExifLibrary.ExifDateTime
struct ExifDateTime_t8_100;
// ExifLibrary.ExifVersion
struct ExifVersion_t8_101;
// ExifLibrary.ExifPointSubjectArea
struct ExifPointSubjectArea_t8_102;
// ExifLibrary.ExifCircularSubjectArea
struct ExifCircularSubjectArea_t8_104;
// ExifLibrary.ExifRectangularSubjectArea
struct ExifRectangularSubjectArea_t8_105;
// ExifLibrary.GPSLatitudeLongitude
struct GPSLatitudeLongitude_t8_106;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_Extensions.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_ExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONLogger.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONLoggerMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONValueType.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONValueTypeMethodDeclarations.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONValue.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONValueMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Double.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONObject.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONArray.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONObjectMethodDeclarations.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONArrayMethodDeclarations.h"
#include "mscorlib_System_DoubleMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_42MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_42.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONObject_JSONParsingStat.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONObject_JSONParsingStatMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_13MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_13.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "mscorlib_System_CharMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "System_System_Text_RegularExpressions_GroupMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollectionMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_CaptureCollectionMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_CaptureMethodDeclarations.h"
#include "mscorlib_System_ByteMethodDeclarations.h"
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Match.h"
#include "System_System_Text_RegularExpressions_Group.h"
#include "System_System_Text_RegularExpressions_MatchMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_GroupCollection.h"
#include "System_System_Text_RegularExpressions_CaptureCollection.h"
#include "System_System_Text_RegularExpressions_Capture.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "mscorlib_System_Text_Encoding.h"
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "AssemblyU2DCSharp_AlertControl_U3CCheckInServerU3Ec__Iterato.h"
#include "AssemblyU2DCSharp_AlertControl_U3CCheckInServerU3Ec__IteratoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "System_Core_System_ActionMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "AssemblyU2DCSharp_AlertControl.h"
#include "UnityEngine_UnityEngine_WWW.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "System_Core_System_Action.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "AssemblyU2DCSharp_AlertControlMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ImageMethodDeclarations.h"
#include "mscorlib_System_TimeSpan.h"
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"
#include "AssemblyU2DCSharp_NPBindingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationSer_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginControl_U3CLoginRouteU3Ec__Iterator1.h"
#include "AssemblyU2DCSharp_LoginControl_U3CLoginRouteU3Ec__Iterator1MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginControlMethodDeclarations.h"
#include "AssemblyU2DCSharp_LoginControl.h"
#include "UnityEngine_UI_UnityEngine_UI_InputFieldMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField.h"
#include "AssemblyU2DCSharp_TimerSetting_U3CUpdateTimerToServerU3Ec__I.h"
#include "AssemblyU2DCSharp_TimerSetting_U3CUpdateTimerToServerU3Ec__IMethodDeclarations.h"
#include "AssemblyU2DCSharp_TimerSettingMethodDeclarations.h"
#include "AssemblyU2DCSharp_TimerSetting.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_DemoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIModalWindowMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIModalWindow.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_1.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIScrollViewMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIScrollView.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Inte.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_InteMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Inte_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Inte_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IDictionaryExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IDictionaryExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Asse.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_AsseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AndroidManifestGenera.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AndroidManifestGeneraMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AndroidManifestGenera_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AndroidManifestGenera_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_ShaderI.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_ShaderIMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_43.h"
#include "UnityEngine_UnityEngine_Shader.h"
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_ShaderP.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_ShaderPMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_eShader.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_eShaderMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtilityMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_44MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObjMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_44.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ComponentExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ComponentExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DateTimeExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_DateTimeExtensionsMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_DateTimeKind.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EnumerationExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_EnumerationExtensionsMethodDeclarations.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GameObjectExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GameObjectExtensionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TransformExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GenericsExtension.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GenericsExtensionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_45.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IOExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_IOExtensionsMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "System_System_UriMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "System_System_Uri.h"
#include "mscorlib_System_IO_Path.h"
#include "mscorlib_System_IO_PathMethodDeclarations.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_DirectoryInfoMethodDeclarations.h"
#include "mscorlib_System_IO_DirectoryInfo.h"
#include "mscorlib_System_IO_FileSystemInfoMethodDeclarations.h"
#include "mscorlib_System_IO_FileInfo.h"
#include "mscorlib_System_IO_FileSystemInfo.h"
#include "mscorlib_System_IO_DirectoryNotFoundExceptionMethodDeclarations.h"
#include "mscorlib_System_IO_DirectoryNotFoundException.h"
#include "mscorlib_System_IO_FileInfoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_MonoBehaviourExtensio.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_MonoBehaviourExtensioMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_PlayerSettings.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_PlayerSettingsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_NativeBindingMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ReflectionExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ReflectionExtensionsMethodDeclarations.h"
#include "mscorlib_System_MissingFieldExceptionMethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_FieldInfo.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfoMethodDeclarations.h"
#include "mscorlib_System_MissingFieldException.h"
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
#include "mscorlib_System_NullReferenceException.h"
#include "mscorlib_System_MissingMethodExceptionMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Reflection_Binder.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_MissingMethodException.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_StringExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_StringExtensionsMethodDeclarations.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "mscorlib_System_StringComparison.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensions_Enc.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensions_EncMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensions_U3C.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensions_U3CMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "mscorlib_System_Action_1_gen_5.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TextureExtensionsMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONUtilityMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_VectorExtensionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TransformExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TypeExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_TypeExtensionsMethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_TypeCode.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_VectorExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_FileOperations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_FileOperationsMethodDeclarations.h"
#include "mscorlib_System_IO_FileMethodDeclarations.h"
#include "mscorlib_System_IO_StreamWriter.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Demo_JSONDemo.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Demo_JSONDemoMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIExtensionsMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_6MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_6.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Demo_PlistDemo.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Demo_PlistDemoMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_PlistMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Plist.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONParserExtensions.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONParserExtensionsMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONConstant.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONConstantMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_eJSONToken.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_eJSONTokenMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONString.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONStringMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONReader.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONReaderMethodDeclarations.h"
#include "mscorlib_System_Int64MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONUtility.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONWriterMethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_JSONWriter.h"
#include "System_Xml_System_Xml_XmlDocumentMethodDeclarations.h"
#include "System_Xml_System_Xml_XmlDocument.h"
#include "System_Xml_System_Xml_XmlNode.h"
#include "System_Xml_System_Xml_XmlNodeMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "System_Xml_System_Xml_XmlNodeList.h"
#include "System_Xml_System_Xml_XmlNodeListMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_IO_StreamWriterMethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
#include "System_Xml_System_Xml_XmlWriterSettingsMethodDeclarations.h"
#include "mscorlib_System_Text_UTF8EncodingMethodDeclarations.h"
#include "System_Xml_System_Xml_XmlWriterMethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream.h"
#include "System_Xml_System_Xml_XmlWriterSettings.h"
#include "System_Xml_System_Xml_XmlWriter.h"
#include "mscorlib_System_Text_UTF8Encoding.h"
#include "System_Xml_System_Xml_ConformanceLevel.h"
#include "mscorlib_System_IO_Stream.h"
#include "mscorlib_System_BooleanMethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfoMethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_NativeBinding.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrderMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterExMethodDeclarations.h"
#include "mscorlib_System_BitConverter.h"
#include "mscorlib_System_BitConverterMethodDeclarations.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_UInt64.h"
#include "mscorlib_System_Int16.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifBitConverter.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifBitConverterMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Compression.h"
#include "AssemblyU2DCSharp_ExifLibrary_CompressionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_PhotometricInterpretation.h"
#include "AssemblyU2DCSharp_ExifLibrary_PhotometricInterpretationMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Orientation.h"
#include "AssemblyU2DCSharp_ExifLibrary_OrientationMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_PlanarConfiguration.h"
#include "AssemblyU2DCSharp_ExifLibrary_PlanarConfigurationMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_YCbCrPositioning.h"
#include "AssemblyU2DCSharp_ExifLibrary_YCbCrPositioningMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ResolutionUnit.h"
#include "AssemblyU2DCSharp_ExifLibrary_ResolutionUnitMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ColorSpace.h"
#include "AssemblyU2DCSharp_ExifLibrary_ColorSpaceMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureProgram.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureProgramMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MeteringMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_MeteringModeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_LightSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_LightSourceMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Flash.h"
#include "AssemblyU2DCSharp_ExifLibrary_FlashMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SensingMethod.h"
#include "AssemblyU2DCSharp_ExifLibrary_SensingMethodMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_FileSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_FileSourceMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneType.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneTypeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_CustomRendered.h"
#include "AssemblyU2DCSharp_ExifLibrary_CustomRenderedMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureModeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_WhiteBalance.h"
#include "AssemblyU2DCSharp_ExifLibrary_WhiteBalanceMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneCaptureType.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneCaptureTypeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GainControl.h"
#include "AssemblyU2DCSharp_ExifLibrary_GainControlMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Contrast.h"
#include "AssemblyU2DCSharp_ExifLibrary_ContrastMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Saturation.h"
#include "AssemblyU2DCSharp_ExifLibrary_SaturationMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Sharpness.h"
#include "AssemblyU2DCSharp_ExifLibrary_SharpnessMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SubjectDistanceRange.h"
#include "AssemblyU2DCSharp_ExifLibrary_SubjectDistanceRangeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRefMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLongitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLongitudeRefMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSAltitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSAltitudeRefMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSStatus.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSStatusMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSMeasureMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSMeasureModeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSSpeedRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSSpeedRefMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDirectionRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDirectionRefMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDistanceRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDistanceRefMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDifferential.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDifferentialMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownIFDSectionException.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownIFDSectionExceptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownEnumTypeException.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownEnumTypeExceptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD0IsEmptyException.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD0IsEmptyExceptionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEncodedString.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEncodedStringMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPropertyMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTagFactoryMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperabilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifDateTime.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifDateTimeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifVersion.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifVersionMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPointSubjectArea.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPointSubjectAreaMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUShortArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifUShortArray.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifCircularSubjectArea.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifCircularSubjectAreaMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifRectangularSubjectArea.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifRectangularSubjectAreaMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeLongitude.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeLongitudeMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalArrayMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalArray.h"

// !!0 Boomlagoon.JSON.Extensions::Pop<System.Object>(System.Collections.Generic.List`1<!!0>)
extern "C" Object_t * Extensions_Pop_TisObject_t_m8_1972_gshared (Object_t * __this /* static, unused */, List_1_t1_1894 * p0, const MethodInfo* method);
#define Extensions_Pop_TisObject_t_m8_1972(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, List_1_t1_1894 *, const MethodInfo*))Extensions_Pop_TisObject_t_m8_1972_gshared)(__this /* static, unused */, p0, method)
// !!0 Boomlagoon.JSON.Extensions::Pop<System.String>(System.Collections.Generic.List`1<!!0>)
#define Extensions_Pop_TisString_t_m8_1968(__this /* static, unused */, p0, method) (( String_t* (*) (Object_t * /* static, unused */, List_1_t1_1742 *, const MethodInfo*))Extensions_Pop_TisObject_t_m8_1972_gshared)(__this /* static, unused */, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* Component_GetComponentsInChildren_TisObject_t_m6_1919_gshared (Component_t6_26 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m6_1919(__this, p0, method) (( ObjectU5BU5D_t1_272* (*) (Component_t6_26 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m6_1919_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu>(System.Boolean)
#define Component_GetComponentsInChildren_TisDemoSubMenu_t8_18_m6_1917(__this, p0, method) (( DemoSubMenuU5BU5D_t8_17* (*) (Component_t6_26 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m6_1919_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m6_1906_gshared (GameObject_t6_97 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m6_1906(__this, method) (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<VoxelBusters.Utility.GUIScrollView>()
#define GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918(__this, method) (( GUIScrollView_t8_19 * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.Object>(System.Collections.IDictionary,System.String)
extern "C" Object_t * IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared (Object_t * __this /* static, unused */, Object_t * p0, String_t* p1, const MethodInfo* method);
#define IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 VoxelBusters.Utility.IDictionaryExtensions::GetIfAvailable<System.String>(System.Collections.IDictionary,System.String)
#define IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(__this /* static, unused */, p0, p1, method) (( String_t* (*) (Object_t * /* static, unused */, Object_t *, String_t*, const MethodInfo*))IDictionaryExtensions_GetIfAvailable_TisObject_t_m8_1973_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Byte>(!!0[],!!0)
extern "C" int32_t Array_IndexOf_TisByte_t1_11_m1_15039_gshared (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* p0, uint8_t p1, const MethodInfo* method);
#define Array_IndexOf_TisByte_t1_11_m1_15039(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, ByteU5BU5D_t1_109*, uint8_t, const MethodInfo*))Array_IndexOf_TisByte_t1_11_m1_15039_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Boomlagoon.JSON.JSONLogger::Log(System.String)
extern "C" void JSONLogger_Log_m8_0 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Debug_Log_m6_631(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONLogger::Error(System.String)
extern "C" void JSONLogger_Error_m8_1 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Debug_LogError_m6_632(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONValueType)
extern "C" void JSONValue__ctor_m8_2 (JSONValue_t8_4 * __this, int32_t ___type, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		JSONValue_set_Type_m8_10(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(System.String)
extern "C" void JSONValue__ctor_m8_3 (JSONValue_t8_4 * __this, String_t* ___str, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m8_10(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = ___str;
		JSONValue_set_Str_m8_12(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(System.Double)
extern "C" void JSONValue__ctor_m8_4 (JSONValue_t8_4 * __this, double ___number, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m8_10(__this, 1, /*hidden argument*/NULL);
		double L_0 = ___number;
		JSONValue_set_Number_m8_14(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONObject)
extern "C" void JSONValue__ctor_m8_5 (JSONValue_t8_4 * __this, JSONObject_t8_5 * ___obj, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JSONObject_t8_5 * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		JSONValue_set_Type_m8_10(__this, 5, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0018:
	{
		JSONValue_set_Type_m8_10(__this, 2, /*hidden argument*/NULL);
		JSONObject_t8_5 * L_1 = ___obj;
		JSONValue_set_Obj_m8_16(__this, L_1, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONArray)
extern "C" void JSONValue__ctor_m8_6 (JSONValue_t8_4 * __this, JSONArray_t8_6 * ___array, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m8_10(__this, 3, /*hidden argument*/NULL);
		JSONArray_t8_6 * L_0 = ___array;
		JSONValue_set_Array_m8_18(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(System.Boolean)
extern "C" void JSONValue__ctor_m8_7 (JSONValue_t8_4 * __this, bool ___boolean, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JSONValue_set_Type_m8_10(__this, 4, /*hidden argument*/NULL);
		bool L_0 = ___boolean;
		JSONValue_set_Boolean_m8_20(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONValue)
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern TypeInfo* JSONArray_t8_6_il2cpp_TypeInfo_var;
extern "C" void JSONValue__ctor_m8_8 (JSONValue_t8_4 * __this, JSONValue_t8_4 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		JSONArray_t8_6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_0 = ___value;
		NullCheck(L_0);
		int32_t L_1 = JSONValue_get_Type_m8_9(L_0, /*hidden argument*/NULL);
		JSONValue_set_Type_m8_10(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = JSONValue_get_Type_m8_9(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_0038;
		}
		if (L_3 == 1)
		{
			goto IL_005a;
		}
		if (L_3 == 2)
		{
			goto IL_006b;
		}
		if (L_3 == 3)
		{
			goto IL_008c;
		}
		if (L_3 == 4)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_00a2;
	}

IL_0038:
	{
		JSONValue_t8_4 * L_4 = ___value;
		NullCheck(L_4);
		String_t* L_5 = JSONValue_get_Str_m8_11(L_4, /*hidden argument*/NULL);
		JSONValue_set_Str_m8_12(__this, L_5, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_0049:
	{
		JSONValue_t8_4 * L_6 = ___value;
		NullCheck(L_6);
		bool L_7 = JSONValue_get_Boolean_m8_19(L_6, /*hidden argument*/NULL);
		JSONValue_set_Boolean_m8_20(__this, L_7, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_005a:
	{
		JSONValue_t8_4 * L_8 = ___value;
		NullCheck(L_8);
		double L_9 = JSONValue_get_Number_m8_13(L_8, /*hidden argument*/NULL);
		JSONValue_set_Number_m8_14(__this, L_9, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_006b:
	{
		JSONValue_t8_4 * L_10 = ___value;
		NullCheck(L_10);
		JSONObject_t8_5 * L_11 = JSONValue_get_Obj_m8_15(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0087;
		}
	}
	{
		JSONValue_t8_4 * L_12 = ___value;
		NullCheck(L_12);
		JSONObject_t8_5 * L_13 = JSONValue_get_Obj_m8_15(L_12, /*hidden argument*/NULL);
		JSONObject_t8_5 * L_14 = (JSONObject_t8_5 *)il2cpp_codegen_object_new (JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject__ctor_m8_43(L_14, L_13, /*hidden argument*/NULL);
		JSONValue_set_Obj_m8_16(__this, L_14, /*hidden argument*/NULL);
	}

IL_0087:
	{
		goto IL_00a2;
	}

IL_008c:
	{
		JSONValue_t8_4 * L_15 = ___value;
		NullCheck(L_15);
		JSONArray_t8_6 * L_16 = JSONValue_get_Array_m8_17(L_15, /*hidden argument*/NULL);
		JSONArray_t8_6 * L_17 = (JSONArray_t8_6 *)il2cpp_codegen_object_new (JSONArray_t8_6_il2cpp_TypeInfo_var);
		JSONArray__ctor_m8_30(L_17, L_16, /*hidden argument*/NULL);
		JSONValue_set_Array_m8_18(__this, L_17, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_00a2:
	{
		return;
	}
}
// Boomlagoon.JSON.JSONValueType Boomlagoon.JSON.JSONValue::get_Type()
extern "C" int32_t JSONValue_get_Type_m8_9 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CTypeU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Type(Boomlagoon.JSON.JSONValueType)
extern "C" void JSONValue_set_Type_m8_10 (JSONValue_t8_4 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CTypeU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String Boomlagoon.JSON.JSONValue::get_Str()
extern "C" String_t* JSONValue_get_Str_m8_11 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CStrU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Str(System.String)
extern "C" void JSONValue_set_Str_m8_12 (JSONValue_t8_4 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CStrU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Double Boomlagoon.JSON.JSONValue::get_Number()
extern "C" double JSONValue_get_Number_m8_13 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		double L_0 = (__this->___U3CNumberU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Number(System.Double)
extern "C" void JSONValue_set_Number_m8_14 (JSONValue_t8_4 * __this, double ___value, const MethodInfo* method)
{
	{
		double L_0 = ___value;
		__this->___U3CNumberU3Ek__BackingField_2 = L_0;
		return;
	}
}
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONValue::get_Obj()
extern "C" JSONObject_t8_5 * JSONValue_get_Obj_m8_15 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		JSONObject_t8_5 * L_0 = (__this->___U3CObjU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Obj(Boomlagoon.JSON.JSONObject)
extern "C" void JSONValue_set_Obj_m8_16 (JSONValue_t8_4 * __this, JSONObject_t8_5 * ___value, const MethodInfo* method)
{
	{
		JSONObject_t8_5 * L_0 = ___value;
		__this->___U3CObjU3Ek__BackingField_3 = L_0;
		return;
	}
}
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONValue::get_Array()
extern "C" JSONArray_t8_6 * JSONValue_get_Array_m8_17 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		JSONArray_t8_6 * L_0 = (__this->___U3CArrayU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Array(Boomlagoon.JSON.JSONArray)
extern "C" void JSONValue_set_Array_m8_18 (JSONValue_t8_4 * __this, JSONArray_t8_6 * ___value, const MethodInfo* method)
{
	{
		JSONArray_t8_6 * L_0 = ___value;
		__this->___U3CArrayU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean Boomlagoon.JSON.JSONValue::get_Boolean()
extern "C" bool JSONValue_get_Boolean_m8_19 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CBooleanU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Boolean(System.Boolean)
extern "C" void JSONValue_set_Boolean_m8_20 (JSONValue_t8_4 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CBooleanU3Ek__BackingField_5 = L_0;
		return;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::get_Parent()
extern "C" JSONValue_t8_4 * JSONValue_get_Parent_m8_21 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	{
		JSONValue_t8_4 * L_0 = (__this->___U3CParentU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void Boomlagoon.JSON.JSONValue::set_Parent(Boomlagoon.JSON.JSONValue)
extern "C" void JSONValue_set_Parent_m8_22 (JSONValue_t8_4 * __this, JSONValue_t8_4 * ___value, const MethodInfo* method)
{
	{
		JSONValue_t8_4 * L_0 = ___value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String Boomlagoon.JSON.JSONValue::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2147;
extern Il2CppCodeGenString* _stringLiteral3328;
extern Il2CppCodeGenString* _stringLiteral583;
extern Il2CppCodeGenString* _stringLiteral994;
extern "C" String_t* JSONValue_ToString_m8_23 (JSONValue_t8_4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral2147 = il2cpp_codegen_string_literal_from_index(2147);
		_stringLiteral3328 = il2cpp_codegen_string_literal_from_index(3328);
		_stringLiteral583 = il2cpp_codegen_string_literal_from_index(583);
		_stringLiteral994 = il2cpp_codegen_string_literal_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	double V_1 = 0.0;
	String_t* G_B7_0 = {0};
	{
		int32_t L_0 = JSONValue_get_Type_m8_9(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_006c;
		}
		if (L_1 == 1)
		{
			goto IL_005d;
		}
		if (L_1 == 2)
		{
			goto IL_002a;
		}
		if (L_1 == 3)
		{
			goto IL_0036;
		}
		if (L_1 == 4)
		{
			goto IL_0042;
		}
		if (L_1 == 5)
		{
			goto IL_0082;
		}
	}
	{
		goto IL_0088;
	}

IL_002a:
	{
		JSONObject_t8_5 * L_2 = JSONValue_get_Obj_m8_15(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String Boomlagoon.JSON.JSONObject::ToString() */, L_2);
		return L_3;
	}

IL_0036:
	{
		JSONArray_t8_6 * L_4 = JSONValue_get_Array_m8_17(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String Boomlagoon.JSON.JSONArray::ToString() */, L_4);
		return L_5;
	}

IL_0042:
	{
		bool L_6 = JSONValue_get_Boolean_m8_19(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		G_B7_0 = _stringLiteral2147;
		goto IL_005c;
	}

IL_0057:
	{
		G_B7_0 = _stringLiteral3328;
	}

IL_005c:
	{
		return G_B7_0;
	}

IL_005d:
	{
		double L_7 = JSONValue_get_Number_m8_13(__this, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = Double_ToString_m1_671((&V_1), /*hidden argument*/NULL);
		return L_8;
	}

IL_006c:
	{
		String_t* L_9 = JSONValue_get_Str_m8_11(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral583, L_9, _stringLiteral583, /*hidden argument*/NULL);
		return L_10;
	}

IL_0082:
	{
		return _stringLiteral994;
	}

IL_0088:
	{
		return _stringLiteral994;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(System.String)
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_24 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		JSONValue_t8_4 * L_1 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_3(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(System.Double)
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_25 (Object_t * __this /* static, unused */, double ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		s_Il2CppMethodIntialized = true;
	}
	{
		double L_0 = ___number;
		JSONValue_t8_4 * L_1 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_4(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(Boomlagoon.JSON.JSONObject)
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_26 (Object_t * __this /* static, unused */, JSONObject_t8_5 * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONObject_t8_5 * L_0 = ___obj;
		JSONValue_t8_4 * L_1 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_5(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(Boomlagoon.JSON.JSONArray)
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_27 (Object_t * __this /* static, unused */, JSONArray_t8_6 * ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		s_Il2CppMethodIntialized = true;
	}
	{
		JSONArray_t8_6 * L_0 = ___array;
		JSONValue_t8_4 * L_1 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_6(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(System.Boolean)
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_28 (Object_t * __this /* static, unused */, bool ___boolean, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___boolean;
		JSONValue_t8_4 * L_1 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_7(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Boomlagoon.JSON.JSONArray::.ctor()
extern TypeInfo* List_1_t1_1898_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15026_MethodInfo_var;
extern "C" void JSONArray__ctor_m8_29 (JSONArray_t8_6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1935);
		List_1__ctor_m1_15026_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484105);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1898 * L_0 = (List_1_t1_1898 *)il2cpp_codegen_object_new (List_1_t1_1898_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15026(L_0, /*hidden argument*/List_1__ctor_m1_15026_MethodInfo_var);
		__this->___values_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONArray::.ctor(Boomlagoon.JSON.JSONArray)
extern TypeInfo* List_1_t1_1898_il2cpp_TypeInfo_var;
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1918_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15026_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_15027_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_15028_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_15029_MethodInfo_var;
extern "C" void JSONArray__ctor_m8_30 (JSONArray_t8_6 * __this, JSONArray_t8_6 * ___array, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1935);
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		Enumerator_t1_1918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1936);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1__ctor_m1_15026_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484105);
		List_1_GetEnumerator_m1_15027_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		Enumerator_get_Current_m1_15028_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484107);
		Enumerator_MoveNext_m1_15029_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484108);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	Enumerator_t1_1918  V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1_1898 * L_0 = (List_1_t1_1898 *)il2cpp_codegen_object_new (List_1_t1_1898_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15026(L_0, /*hidden argument*/List_1__ctor_m1_15026_MethodInfo_var);
		__this->___values_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		List_1_t1_1898 * L_1 = (List_1_t1_1898 *)il2cpp_codegen_object_new (List_1_t1_1898_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15026(L_1, /*hidden argument*/List_1__ctor_m1_15026_MethodInfo_var);
		__this->___values_0 = L_1;
		JSONArray_t8_6 * L_2 = ___array;
		NullCheck(L_2);
		List_1_t1_1898 * L_3 = (L_2->___values_0);
		NullCheck(L_3);
		Enumerator_t1_1918  L_4 = List_1_GetEnumerator_m1_15027(L_3, /*hidden argument*/List_1_GetEnumerator_m1_15027_MethodInfo_var);
		V_1 = L_4;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0046;
		}

IL_002d:
		{
			JSONValue_t8_4 * L_5 = Enumerator_get_Current_m1_15028((&V_1), /*hidden argument*/Enumerator_get_Current_m1_15028_MethodInfo_var);
			V_0 = L_5;
			List_1_t1_1898 * L_6 = (__this->___values_0);
			JSONValue_t8_4 * L_7 = V_0;
			JSONValue_t8_4 * L_8 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
			JSONValue__ctor_m8_8(L_8, L_7, /*hidden argument*/NULL);
			NullCheck(L_6);
			VirtActionInvoker1< JSONValue_t8_4 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::Add(!0) */, L_6, L_8);
		}

IL_0046:
		{
			bool L_9 = Enumerator_MoveNext_m1_15029((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_15029_MethodInfo_var);
			if (L_9)
			{
				goto IL_002d;
			}
		}

IL_0052:
		{
			IL2CPP_LEAVE(0x63, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Enumerator_t1_1918  L_10 = V_1;
		Enumerator_t1_1918  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1_1918_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0063:
	{
		return;
	}
}
// System.Collections.IEnumerator Boomlagoon.JSON.JSONArray::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* Enumerator_t1_1918_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_15027_MethodInfo_var;
extern "C" Object_t * JSONArray_System_Collections_IEnumerable_GetEnumerator_m8_31 (JSONArray_t8_6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1936);
		List_1_GetEnumerator_m1_15027_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		NullCheck(L_0);
		Enumerator_t1_1918  L_1 = List_1_GetEnumerator_m1_15027(L_0, /*hidden argument*/List_1_GetEnumerator_m1_15027_MethodInfo_var);
		Enumerator_t1_1918  L_2 = L_1;
		Object_t * L_3 = Box(Enumerator_t1_1918_il2cpp_TypeInfo_var, &L_2);
		return (Object_t *)L_3;
	}
}
// System.Void Boomlagoon.JSON.JSONArray::Add(Boomlagoon.JSON.JSONValue)
extern "C" void JSONArray_Add_m8_32 (JSONArray_t8_6 * __this, JSONValue_t8_4 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		JSONValue_t8_4 * L_1 = ___value;
		NullCheck(L_0);
		VirtActionInvoker1< JSONValue_t8_4 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::Add(!0) */, L_0, L_1);
		return;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONArray::get_Item(System.Int32)
extern "C" JSONValue_t8_4 * JSONArray_get_Item_m8_33 (JSONArray_t8_6 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		JSONValue_t8_4 * L_2 = (JSONValue_t8_4 *)VirtFuncInvoker1< JSONValue_t8_4 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::get_Item(System.Int32) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Boomlagoon.JSON.JSONArray::set_Item(System.Int32,Boomlagoon.JSON.JSONValue)
extern "C" void JSONArray_set_Item_m8_34 (JSONArray_t8_6 * __this, int32_t ___index, JSONValue_t8_4 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		int32_t L_1 = ___index;
		JSONValue_t8_4 * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, JSONValue_t8_4 * >::Invoke(32 /* System.Void System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::set_Item(System.Int32,!0) */, L_0, L_1, L_2);
		return;
	}
}
// System.Int32 Boomlagoon.JSON.JSONArray::get_Length()
extern "C" int32_t JSONArray_get_Length_m8_35 (JSONArray_t8_6 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::get_Count() */, L_0);
		return L_1;
	}
}
// System.String Boomlagoon.JSON.JSONArray::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1918_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_15027_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_15028_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_15029_MethodInfo_var;
extern "C" String_t* JSONArray_ToString_m8_36 (JSONArray_t8_6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Enumerator_t1_1918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1936);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_15027_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		Enumerator_get_Current_m1_15028_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484107);
		Enumerator_MoveNext_m1_15029_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484108);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	JSONValue_t8_4 * V_1 = {0};
	Enumerator_t1_1918  V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)91), /*hidden argument*/NULL);
		List_1_t1_1898 * L_2 = (__this->___values_0);
		NullCheck(L_2);
		Enumerator_t1_1918  L_3 = List_1_GetEnumerator_m1_15027(L_2, /*hidden argument*/List_1_GetEnumerator_m1_15027_MethodInfo_var);
		V_2 = L_3;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0020:
		{
			JSONValue_t8_4 * L_4 = Enumerator_get_Current_m1_15028((&V_2), /*hidden argument*/Enumerator_get_Current_m1_15028_MethodInfo_var);
			V_1 = L_4;
			StringBuilder_t1_247 * L_5 = V_0;
			JSONValue_t8_4 * L_6 = V_1;
			NullCheck(L_6);
			String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String Boomlagoon.JSON.JSONValue::ToString() */, L_6);
			NullCheck(L_5);
			StringBuilder_Append_m1_12438(L_5, L_7, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_8 = V_0;
			NullCheck(L_8);
			StringBuilder_Append_m1_12452(L_8, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003e:
		{
			bool L_9 = Enumerator_MoveNext_m1_15029((&V_2), /*hidden argument*/Enumerator_MoveNext_m1_15029_MethodInfo_var);
			if (L_9)
			{
				goto IL_0020;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_t1_1918  L_10 = V_2;
		Enumerator_t1_1918  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1_1918_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005b:
	{
		List_1_t1_1898 * L_13 = (__this->___values_0);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::get_Count() */, L_13);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_007c;
		}
	}
	{
		StringBuilder_t1_247 * L_15 = V_0;
		StringBuilder_t1_247 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = StringBuilder_get_Length_m1_12424(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		StringBuilder_Remove_m1_12432(L_15, ((int32_t)((int32_t)L_17-(int32_t)1)), 1, /*hidden argument*/NULL);
	}

IL_007c:
	{
		StringBuilder_t1_247 * L_18 = V_0;
		NullCheck(L_18);
		StringBuilder_Append_m1_12452(L_18, ((int32_t)93), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_19 = V_0;
		NullCheck(L_19);
		String_t* L_20 = StringBuilder_ToString_m1_12428(L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Collections.Generic.IEnumerator`1<Boomlagoon.JSON.JSONValue> Boomlagoon.JSON.JSONArray::GetEnumerator()
extern TypeInfo* Enumerator_t1_1918_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_15027_MethodInfo_var;
extern "C" Object_t* JSONArray_GetEnumerator_m8_37 (JSONArray_t8_6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1936);
		List_1_GetEnumerator_m1_15027_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		NullCheck(L_0);
		Enumerator_t1_1918  L_1 = List_1_GetEnumerator_m1_15027(L_0, /*hidden argument*/List_1_GetEnumerator_m1_15027_MethodInfo_var);
		Enumerator_t1_1918  L_2 = L_1;
		Object_t * L_3 = Box(Enumerator_t1_1918_il2cpp_TypeInfo_var, &L_2);
		return (Object_t*)L_3;
	}
}
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONArray::Parse(System.String)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5083;
extern Il2CppCodeGenString* _stringLiteral173;
extern "C" JSONArray_t8_6 * JSONArray_Parse_m8_38 (Object_t * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		_stringLiteral5083 = il2cpp_codegen_string_literal_from_index(5083);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	JSONObject_t8_5 * V_0 = {0};
	JSONArray_t8_6 * G_B3_0 = {0};
	{
		String_t* L_0 = ___jsonString;
		uint16_t L_1 = ((int32_t)125);
		Object_t * L_2 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_557(NULL /*static, unused*/, _stringLiteral5083, L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_4 = JSONObject_Parse_m8_57(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		JSONObject_t8_5 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		G_B3_0 = ((JSONArray_t8_6 *)(NULL));
		goto IL_0034;
	}

IL_0024:
	{
		JSONObject_t8_5 * L_6 = V_0;
		NullCheck(L_6);
		JSONValue_t8_4 * L_7 = JSONObject_GetValue_m8_47(L_6, _stringLiteral173, /*hidden argument*/NULL);
		NullCheck(L_7);
		JSONArray_t8_6 * L_8 = JSONValue_get_Array_m8_17(L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0034:
	{
		return G_B3_0;
	}
}
// System.Void Boomlagoon.JSON.JSONArray::Clear()
extern "C" void JSONArray_Clear_m8_39 (JSONArray_t8_6 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1898 * L_0 = (__this->___values_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::Clear() */, L_0);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONArray::Remove(System.Int32)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5084;
extern Il2CppCodeGenString* _stringLiteral5085;
extern Il2CppCodeGenString* _stringLiteral106;
extern "C" void JSONArray_Remove_m8_40 (JSONArray_t8_6 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5084 = il2cpp_codegen_string_literal_from_index(5084);
		_stringLiteral5085 = il2cpp_codegen_string_literal_from_index(5085);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = ___index;
		List_1_t1_1898 * L_2 = (__this->___values_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::get_Count() */, L_2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1_1898 * L_4 = (__this->___values_0);
		int32_t L_5 = ___index;
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::RemoveAt(System.Int32) */, L_4, L_5);
		goto IL_006d;
	}

IL_0029:
	{
		ObjectU5BU5D_t1_272* L_6 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, _stringLiteral5084);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5084;
		ObjectU5BU5D_t1_272* L_7 = L_6;
		int32_t L_8 = ___index;
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 1, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_272* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral5085);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5085;
		ObjectU5BU5D_t1_272* L_12 = L_11;
		List_1_t1_1898 * L_13 = (__this->___values_0);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>::get_Count() */, L_13);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_272* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral106);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral106;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1_562(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONArray::op_Addition(Boomlagoon.JSON.JSONArray,Boomlagoon.JSON.JSONArray)
extern TypeInfo* JSONArray_t8_6_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1918_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_15027_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_15028_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_15029_MethodInfo_var;
extern "C" JSONArray_t8_6 * JSONArray_op_Addition_m8_41 (Object_t * __this /* static, unused */, JSONArray_t8_6 * ___lhs, JSONArray_t8_6 * ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONArray_t8_6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1933);
		Enumerator_t1_1918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1936);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_15027_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484106);
		Enumerator_get_Current_m1_15028_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484107);
		Enumerator_MoveNext_m1_15029_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484108);
		s_Il2CppMethodIntialized = true;
	}
	JSONArray_t8_6 * V_0 = {0};
	JSONValue_t8_4 * V_1 = {0};
	Enumerator_t1_1918  V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		JSONArray_t8_6 * L_0 = ___lhs;
		JSONArray_t8_6 * L_1 = (JSONArray_t8_6 *)il2cpp_codegen_object_new (JSONArray_t8_6_il2cpp_TypeInfo_var);
		JSONArray__ctor_m8_30(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONArray_t8_6 * L_2 = ___rhs;
		NullCheck(L_2);
		List_1_t1_1898 * L_3 = (L_2->___values_0);
		NullCheck(L_3);
		Enumerator_t1_1918  L_4 = List_1_GetEnumerator_m1_15027(L_3, /*hidden argument*/List_1_GetEnumerator_m1_15027_MethodInfo_var);
		V_2 = L_4;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0018:
		{
			JSONValue_t8_4 * L_5 = Enumerator_get_Current_m1_15028((&V_2), /*hidden argument*/Enumerator_get_Current_m1_15028_MethodInfo_var);
			V_1 = L_5;
			JSONArray_t8_6 * L_6 = V_0;
			JSONValue_t8_4 * L_7 = V_1;
			NullCheck(L_6);
			JSONArray_Add_m8_32(L_6, L_7, /*hidden argument*/NULL);
		}

IL_0027:
		{
			bool L_8 = Enumerator_MoveNext_m1_15029((&V_2), /*hidden argument*/Enumerator_MoveNext_m1_15029_MethodInfo_var);
			if (L_8)
			{
				goto IL_0018;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_t1_1918  L_9 = V_2;
		Enumerator_t1_1918  L_10 = L_9;
		Object_t * L_11 = Box(Enumerator_t1_1918_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_11);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0044:
	{
		JSONArray_t8_6 * L_12 = V_0;
		return L_12;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::.ctor()
extern TypeInfo* Dictionary_2_t1_1919_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15030_MethodInfo_var;
extern "C" void JSONObject__ctor_m8_42 (JSONObject_t8_5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1919_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1937);
		Dictionary_2__ctor_m1_15030_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484109);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1919 * L_0 = (Dictionary_2_t1_1919 *)il2cpp_codegen_object_new (Dictionary_2_t1_1919_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15030(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15030_MethodInfo_var);
		__this->___values_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::.ctor(Boomlagoon.JSON.JSONObject)
extern TypeInfo* Dictionary_2_t1_1919_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t1_1920_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t1_1916_il2cpp_TypeInfo_var;
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15030_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_15031_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_15032_MethodInfo_var;
extern "C" void JSONObject__ctor_m8_43 (JSONObject_t8_5 * __this, JSONObject_t8_5 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1919_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1937);
		IEnumerable_1_t1_1920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1938);
		IEnumerator_1_t1_1916_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1940);
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		Dictionary_2__ctor_m1_15030_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484109);
		KeyValuePair_2_get_Key_m1_15031_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484110);
		KeyValuePair_2_get_Value_m1_15032_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484111);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1_1915  V_0 = {0};
	Object_t* V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1_1919 * L_0 = (Dictionary_2_t1_1919 *)il2cpp_codegen_object_new (Dictionary_2_t1_1919_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15030(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15030_MethodInfo_var);
		__this->___values_0 = L_0;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Dictionary_2_t1_1919 * L_1 = (Dictionary_2_t1_1919 *)il2cpp_codegen_object_new (Dictionary_2_t1_1919_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15030(L_1, /*hidden argument*/Dictionary_2__ctor_m1_15030_MethodInfo_var);
		__this->___values_0 = L_1;
		JSONObject_t8_5 * L_2 = ___other;
		if (!L_2)
		{
			goto IL_0073;
		}
	}
	{
		JSONObject_t8_5 * L_3 = ___other;
		NullCheck(L_3);
		Object_t* L_4 = (L_3->___values_0);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1_1920_il2cpp_TypeInfo_var, L_4);
		V_1 = L_5;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0058;
		}

IL_0033:
		{
			Object_t* L_6 = V_1;
			NullCheck(L_6);
			KeyValuePair_2_t1_1915  L_7 = (KeyValuePair_2_t1_1915 )InterfaceFuncInvoker0< KeyValuePair_2_t1_1915  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::get_Current() */, IEnumerator_1_t1_1916_il2cpp_TypeInfo_var, L_6);
			V_0 = L_7;
			Object_t* L_8 = (__this->___values_0);
			String_t* L_9 = KeyValuePair_2_get_Key_m1_15031((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m1_15031_MethodInfo_var);
			JSONValue_t8_4 * L_10 = KeyValuePair_2_get_Value_m1_15032((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1_15032_MethodInfo_var);
			JSONValue_t8_4 * L_11 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
			JSONValue__ctor_m8_8(L_11, L_10, /*hidden argument*/NULL);
			NullCheck(L_8);
			InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_8, L_9, L_11);
		}

IL_0058:
		{
			Object_t* L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0033;
			}
		}

IL_0063:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		{
			Object_t* L_14 = V_1;
			if (L_14)
			{
				goto IL_006c;
			}
		}

IL_006b:
		{
			IL2CPP_END_FINALLY(104)
		}

IL_006c:
		{
			Object_t* L_15 = V_1;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(104)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0073:
	{
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::.cctor()
extern TypeInfo* Regex_t3_11_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5086;
extern "C" void JSONObject__cctor_m8_44 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t3_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1211);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral5086 = il2cpp_codegen_string_literal_from_index(5086);
		s_Il2CppMethodIntialized = true;
	}
	{
		Regex_t3_11 * L_0 = (Regex_t3_11 *)il2cpp_codegen_object_new (Regex_t3_11_il2cpp_TypeInfo_var);
		Regex__ctor_m3_17(L_0, _stringLiteral5086, /*hidden argument*/NULL);
		((JSONObject_t8_5_StaticFields*)JSONObject_t8_5_il2cpp_TypeInfo_var->static_fields)->___unicodeRegex_1 = L_0;
		((JSONObject_t8_5_StaticFields*)JSONObject_t8_5_il2cpp_TypeInfo_var->static_fields)->___unicodeBytes_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		return;
	}
}
// System.Collections.IEnumerator Boomlagoon.JSON.JSONObject::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* IEnumerable_1_t1_1920_il2cpp_TypeInfo_var;
extern "C" Object_t * JSONObject_System_Collections_IEnumerable_GetEnumerator_m8_45 (JSONObject_t8_5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t1_1920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1938);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		NullCheck(L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1_1920_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Boomlagoon.JSON.JSONObject::ContainsKey(System.String)
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern "C" bool JSONObject_ContainsKey_m8_46 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::ContainsKey(!0) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONObject::GetValue(System.String)
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern "C" JSONValue_t8_4 * JSONObject_GetValue_m8_47 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	{
		Object_t* L_0 = (__this->___values_0);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		InterfaceFuncInvoker2< bool, String_t*, JSONValue_t8_4 ** >::Invoke(3 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::TryGetValue(!0,!1&) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_0, L_1, (&V_0));
		JSONValue_t8_4 * L_2 = V_0;
		return L_2;
	}
}
// System.String Boomlagoon.JSON.JSONObject::GetString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5087;
extern "C" String_t* JSONObject_GetString_m8_48 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5087 = il2cpp_codegen_string_literal_from_index(5087);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	{
		String_t* L_0 = ___key;
		JSONValue_t8_4 * L_1 = JSONObject_GetValue_m8_47(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t8_4 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_3, _stringLiteral5087, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_5;
	}

IL_0024:
	{
		JSONValue_t8_4 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = JSONValue_get_Str_m8_11(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Double Boomlagoon.JSON.JSONObject::GetNumber(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5088;
extern "C" double JSONObject_GetNumber_m8_49 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5088 = il2cpp_codegen_string_literal_from_index(5088);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	{
		String_t* L_0 = ___key;
		JSONValue_t8_4 * L_1 = JSONObject_GetValue_m8_47(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t8_4 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_3, _stringLiteral5088, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_0028:
	{
		JSONValue_t8_4 * L_5 = V_0;
		NullCheck(L_5);
		double L_6 = JSONValue_get_Number_m8_13(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::GetObject(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5088;
extern "C" JSONObject_t8_5 * JSONObject_GetObject_m8_50 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5088 = il2cpp_codegen_string_literal_from_index(5088);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	{
		String_t* L_0 = ___key;
		JSONValue_t8_4 * L_1 = JSONObject_GetValue_m8_47(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t8_4 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_3, _stringLiteral5088, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}

IL_0020:
	{
		JSONValue_t8_4 * L_5 = V_0;
		NullCheck(L_5);
		JSONObject_t8_5 * L_6 = JSONValue_get_Obj_m8_15(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Boomlagoon.JSON.JSONObject::GetBoolean(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5088;
extern "C" bool JSONObject_GetBoolean_m8_51 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5088 = il2cpp_codegen_string_literal_from_index(5088);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	{
		String_t* L_0 = ___key;
		JSONValue_t8_4 * L_1 = JSONObject_GetValue_m8_47(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t8_4 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_3, _stringLiteral5088, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return 0;
	}

IL_0020:
	{
		JSONValue_t8_4 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = JSONValue_get_Boolean_m8_19(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONObject::GetArray(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5088;
extern "C" JSONArray_t8_6 * JSONObject_GetArray_m8_52 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5088 = il2cpp_codegen_string_literal_from_index(5088);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	{
		String_t* L_0 = ___key;
		JSONValue_t8_4 * L_1 = JSONObject_GetValue_m8_47(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONValue_t8_4 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_3, _stringLiteral5088, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (JSONArray_t8_6 *)NULL;
	}

IL_0020:
	{
		JSONValue_t8_4 * L_5 = V_0;
		NullCheck(L_5);
		JSONArray_t8_6 * L_6 = JSONValue_get_Array_m8_17(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONObject::get_Item(System.String)
extern "C" JSONValue_t8_4 * JSONObject_get_Item_m8_53 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key;
		JSONValue_t8_4 * L_1 = JSONObject_GetValue_m8_47(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::set_Item(System.String,Boomlagoon.JSON.JSONValue)
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern "C" void JSONObject_set_Item_m8_54 (JSONObject_t8_5 * __this, String_t* ___key, JSONValue_t8_4 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		String_t* L_1 = ___key;
		JSONValue_t8_4 * L_2 = ___value;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::Add(System.String,Boomlagoon.JSON.JSONValue)
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern "C" void JSONObject_Add_m8_55 (JSONObject_t8_5 * __this, String_t* ___key, JSONValue_t8_4 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		String_t* L_1 = ___key;
		JSONValue_t8_4 * L_2 = ___value;
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>)
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_15031_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_15032_MethodInfo_var;
extern "C" void JSONObject_Add_m8_56 (JSONObject_t8_5 * __this, KeyValuePair_2_t1_1915  ___pair, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		KeyValuePair_2_get_Key_m1_15031_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484110);
		KeyValuePair_2_get_Value_m1_15032_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484111);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		String_t* L_1 = KeyValuePair_2_get_Key_m1_15031((&___pair), /*hidden argument*/KeyValuePair_2_get_Key_m1_15031_MethodInfo_var);
		JSONValue_t8_4 * L_2 = KeyValuePair_2_get_Value_m1_15032((&___pair), /*hidden argument*/KeyValuePair_2_get_Value_m1_15032_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::Parse(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1742_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern TypeInfo* JSONValue_t8_4_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* JSONArray_t8_6_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14876_MethodInfo_var;
extern const MethodInfo* Extensions_Pop_TisString_t_m8_1968_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5089;
extern Il2CppCodeGenString* _stringLiteral5090;
extern Il2CppCodeGenString* _stringLiteral5091;
extern Il2CppCodeGenString* _stringLiteral5092;
extern Il2CppCodeGenString* _stringLiteral5093;
extern Il2CppCodeGenString* _stringLiteral5094;
extern Il2CppCodeGenString* _stringLiteral5095;
extern Il2CppCodeGenString* _stringLiteral5096;
extern Il2CppCodeGenString* _stringLiteral2147;
extern Il2CppCodeGenString* _stringLiteral3328;
extern Il2CppCodeGenString* _stringLiteral994;
extern Il2CppCodeGenString* _stringLiteral5097;
extern "C" JSONObject_t8_5 * JSONObject_Parse_m8_57 (Object_t * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		List_1_t1_1742_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		JSONValue_t8_4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1934);
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		JSONArray_t8_6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1933);
		List_1__ctor_m1_14876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		Extensions_Pop_TisString_t_m8_1968_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484112);
		_stringLiteral5089 = il2cpp_codegen_string_literal_from_index(5089);
		_stringLiteral5090 = il2cpp_codegen_string_literal_from_index(5090);
		_stringLiteral5091 = il2cpp_codegen_string_literal_from_index(5091);
		_stringLiteral5092 = il2cpp_codegen_string_literal_from_index(5092);
		_stringLiteral5093 = il2cpp_codegen_string_literal_from_index(5093);
		_stringLiteral5094 = il2cpp_codegen_string_literal_from_index(5094);
		_stringLiteral5095 = il2cpp_codegen_string_literal_from_index(5095);
		_stringLiteral5096 = il2cpp_codegen_string_literal_from_index(5096);
		_stringLiteral2147 = il2cpp_codegen_string_literal_from_index(2147);
		_stringLiteral3328 = il2cpp_codegen_string_literal_from_index(3328);
		_stringLiteral994 = il2cpp_codegen_string_literal_from_index(994);
		_stringLiteral5097 = il2cpp_codegen_string_literal_from_index(5097);
		s_Il2CppMethodIntialized = true;
	}
	JSONValue_t8_4 * V_0 = {0};
	List_1_t1_1742 * V_1 = {0};
	int32_t V_2 = {0};
	int32_t V_3 = 0;
	JSONValue_t8_4 * V_4 = {0};
	String_t* V_5 = {0};
	uint16_t V_6 = 0x0;
	String_t* V_7 = {0};
	double V_8 = 0.0;
	JSONValue_t8_4 * V_9 = {0};
	int32_t V_10 = {0};
	int32_t V_11 = {0};
	uint16_t V_12 = 0x0;
	int32_t G_B36_0 = 0;
	{
		String_t* L_0 = ___jsonString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (JSONObject_t8_5 *)NULL;
	}

IL_000d:
	{
		V_0 = (JSONValue_t8_4 *)NULL;
		List_1_t1_1742 * L_2 = (List_1_t1_1742 *)il2cpp_codegen_object_new (List_1_t1_1742_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14876(L_2, /*hidden argument*/List_1__ctor_m1_14876_MethodInfo_var);
		V_1 = L_2;
		V_2 = 0;
		V_3 = 0;
		goto IL_0735;
	}

IL_001e:
	{
		String_t* L_3 = ___jsonString;
		int32_t L_4 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		int32_t L_5 = JSONObject_SkipWhitespace_m8_58(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_2;
		V_10 = L_6;
		int32_t L_7 = V_10;
		if (L_7 == 0)
		{
			goto IL_0065;
		}
		if (L_7 == 1)
		{
			goto IL_0583;
		}
		if (L_7 == 2)
		{
			goto IL_00a0;
		}
		if (L_7 == 3)
		{
			goto IL_05be;
		}
		if (L_7 == 4)
		{
			goto IL_0150;
		}
		if (L_7 == 5)
		{
			goto IL_021c;
		}
		if (L_7 == 6)
		{
			goto IL_0195;
		}
		if (L_7 == 7)
		{
			goto IL_01b3;
		}
		if (L_7 == 8)
		{
			goto IL_02ec;
		}
		if (L_7 == 9)
		{
			goto IL_0372;
		}
		if (L_7 == 10)
		{
			goto IL_03fd;
		}
		if (L_7 == 11)
		{
			goto IL_066e;
		}
	}
	{
		goto IL_0731;
	}

IL_0065:
	{
		String_t* L_8 = ___jsonString;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1_442(L_8, L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)((int32_t)123))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_12 = JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)123), L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_007c:
	{
		JSONObject_t8_5 * L_13 = (JSONObject_t8_5 *)il2cpp_codegen_object_new (JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject__ctor_m8_42(L_13, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_14 = JSONValue_op_Implicit_m8_26(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		JSONValue_t8_4 * L_15 = V_0;
		if (!L_15)
		{
			goto IL_0096;
		}
	}
	{
		JSONValue_t8_4 * L_16 = V_4;
		JSONValue_t8_4 * L_17 = V_0;
		NullCheck(L_16);
		JSONValue_set_Parent_m8_22(L_16, L_17, /*hidden argument*/NULL);
	}

IL_0096:
	{
		JSONValue_t8_4 * L_18 = V_4;
		V_0 = L_18;
		V_2 = 4;
		goto IL_0731;
	}

IL_00a0:
	{
		String_t* L_19 = ___jsonString;
		int32_t L_20 = V_3;
		NullCheck(L_19);
		uint16_t L_21 = String_get_Chars_m1_442(L_19, L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)((int32_t)125))))
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_23 = JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)125), L_22, /*hidden argument*/NULL);
		return L_23;
	}

IL_00b7:
	{
		JSONValue_t8_4 * L_24 = V_0;
		NullCheck(L_24);
		JSONValue_t8_4 * L_25 = JSONValue_get_Parent_m8_21(L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00c9;
		}
	}
	{
		JSONValue_t8_4 * L_26 = V_0;
		NullCheck(L_26);
		JSONObject_t8_5 * L_27 = JSONValue_get_Obj_m8_15(L_26, /*hidden argument*/NULL);
		return L_27;
	}

IL_00c9:
	{
		JSONValue_t8_4 * L_28 = V_0;
		NullCheck(L_28);
		JSONValue_t8_4 * L_29 = JSONValue_get_Parent_m8_21(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		int32_t L_30 = JSONValue_get_Type_m8_9(L_29, /*hidden argument*/NULL);
		V_11 = L_30;
		int32_t L_31 = V_11;
		if ((((int32_t)L_31) == ((int32_t)2)))
		{
			goto IL_00eb;
		}
	}
	{
		int32_t L_32 = V_11;
		if ((((int32_t)L_32) == ((int32_t)3)))
		{
			goto IL_0116;
		}
	}
	{
		goto IL_0136;
	}

IL_00eb:
	{
		JSONValue_t8_4 * L_33 = V_0;
		NullCheck(L_33);
		JSONValue_t8_4 * L_34 = JSONValue_get_Parent_m8_21(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		JSONObject_t8_5 * L_35 = JSONValue_get_Obj_m8_15(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Object_t* L_36 = (L_35->___values_0);
		List_1_t1_1742 * L_37 = V_1;
		String_t* L_38 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_37, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		JSONValue_t8_4 * L_39 = V_0;
		NullCheck(L_39);
		JSONObject_t8_5 * L_40 = JSONValue_get_Obj_m8_15(L_39, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_41 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_5(L_41, L_40, /*hidden argument*/NULL);
		NullCheck(L_36);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_36, L_38, L_41);
		goto IL_0142;
	}

IL_0116:
	{
		JSONValue_t8_4 * L_42 = V_0;
		NullCheck(L_42);
		JSONValue_t8_4 * L_43 = JSONValue_get_Parent_m8_21(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		JSONArray_t8_6 * L_44 = JSONValue_get_Array_m8_17(L_43, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_45 = V_0;
		NullCheck(L_45);
		JSONObject_t8_5 * L_46 = JSONValue_get_Obj_m8_15(L_45, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_47 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_5(L_47, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		JSONArray_Add_m8_32(L_44, L_47, /*hidden argument*/NULL);
		goto IL_0142;
	}

IL_0136:
	{
		int32_t L_48 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_49 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5089, L_48, /*hidden argument*/NULL);
		return L_49;
	}

IL_0142:
	{
		JSONValue_t8_4 * L_50 = V_0;
		NullCheck(L_50);
		JSONValue_t8_4 * L_51 = JSONValue_get_Parent_m8_21(L_50, /*hidden argument*/NULL);
		V_0 = L_51;
		V_2 = 7;
		goto IL_0731;
	}

IL_0150:
	{
		String_t* L_52 = ___jsonString;
		int32_t L_53 = V_3;
		NullCheck(L_52);
		uint16_t L_54 = String_get_Chars_m1_442(L_52, L_53, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_54) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0169;
		}
	}
	{
		int32_t L_55 = V_3;
		V_3 = ((int32_t)((int32_t)L_55-(int32_t)1));
		V_2 = 2;
		goto IL_0731;
	}

IL_0169:
	{
		String_t* L_56 = ___jsonString;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		String_t* L_57 = JSONObject_ParseString_m8_59(NULL /*static, unused*/, L_56, (&V_3), /*hidden argument*/NULL);
		V_5 = L_57;
		String_t* L_58 = V_5;
		if (L_58)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_59 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_60 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5090, L_59, /*hidden argument*/NULL);
		return L_60;
	}

IL_0186:
	{
		List_1_t1_1742 * L_61 = V_1;
		String_t* L_62 = V_5;
		NullCheck(L_61);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_61, L_62);
		V_2 = 6;
		goto IL_0731;
	}

IL_0195:
	{
		String_t* L_63 = ___jsonString;
		int32_t L_64 = V_3;
		NullCheck(L_63);
		uint16_t L_65 = String_get_Chars_m1_442(L_63, L_64, /*hidden argument*/NULL);
		if ((((int32_t)L_65) == ((int32_t)((int32_t)58))))
		{
			goto IL_01ac;
		}
	}
	{
		int32_t L_66 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_67 = JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)58), L_66, /*hidden argument*/NULL);
		return L_67;
	}

IL_01ac:
	{
		V_2 = 5;
		goto IL_0731;
	}

IL_01b3:
	{
		String_t* L_68 = ___jsonString;
		int32_t L_69 = V_3;
		NullCheck(L_68);
		uint16_t L_70 = String_get_Chars_m1_442(L_68, L_69, /*hidden argument*/NULL);
		V_12 = L_70;
		uint16_t L_71 = V_12;
		if ((((int32_t)L_71) == ((int32_t)((int32_t)44))))
		{
			goto IL_01dc;
		}
	}
	{
		uint16_t L_72 = V_12;
		if ((((int32_t)L_72) == ((int32_t)((int32_t)93))))
		{
			goto IL_0200;
		}
	}
	{
		uint16_t L_73 = V_12;
		if ((((int32_t)L_73) == ((int32_t)((int32_t)125))))
		{
			goto IL_01f5;
		}
	}
	{
		goto IL_020b;
	}

IL_01dc:
	{
		JSONValue_t8_4 * L_74 = V_0;
		NullCheck(L_74);
		int32_t L_75 = JSONValue_get_Type_m8_9(L_74, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_75) == ((uint32_t)2))))
		{
			goto IL_01ee;
		}
	}
	{
		G_B36_0 = 4;
		goto IL_01ef;
	}

IL_01ee:
	{
		G_B36_0 = 5;
	}

IL_01ef:
	{
		V_2 = G_B36_0;
		goto IL_0217;
	}

IL_01f5:
	{
		V_2 = 2;
		int32_t L_76 = V_3;
		V_3 = ((int32_t)((int32_t)L_76-(int32_t)1));
		goto IL_0217;
	}

IL_0200:
	{
		V_2 = 3;
		int32_t L_77 = V_3;
		V_3 = ((int32_t)((int32_t)L_77-(int32_t)1));
		goto IL_0217;
	}

IL_020b:
	{
		int32_t L_78 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_79 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5091, L_78, /*hidden argument*/NULL);
		return L_79;
	}

IL_0217:
	{
		goto IL_0731;
	}

IL_021c:
	{
		String_t* L_80 = ___jsonString;
		int32_t L_81 = V_3;
		NullCheck(L_80);
		uint16_t L_82 = String_get_Chars_m1_442(L_80, L_81, /*hidden argument*/NULL);
		V_6 = L_82;
		uint16_t L_83 = V_6;
		if ((!(((uint32_t)L_83) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0235;
		}
	}
	{
		V_2 = 8;
		goto IL_02e3;
	}

IL_0235:
	{
		uint16_t L_84 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_85 = Char_IsDigit_m1_374(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_024a;
		}
	}
	{
		uint16_t L_86 = V_6;
		if ((!(((uint32_t)L_86) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_0252;
		}
	}

IL_024a:
	{
		V_2 = ((int32_t)9);
		goto IL_02e3;
	}

IL_0252:
	{
		uint16_t L_87 = V_6;
		V_12 = L_87;
		uint16_t L_88 = V_12;
		if (((int32_t)((int32_t)L_88-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_029c;
		}
		if (((int32_t)((int32_t)L_88-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_026c;
		}
		if (((int32_t)((int32_t)L_88-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_02a3;
		}
	}

IL_026c:
	{
		uint16_t L_89 = V_12;
		if ((((int32_t)L_89) == ((int32_t)((int32_t)102))))
		{
			goto IL_02c7;
		}
	}
	{
		uint16_t L_90 = V_12;
		if ((((int32_t)L_90) == ((int32_t)((int32_t)110))))
		{
			goto IL_02cf;
		}
	}
	{
		uint16_t L_91 = V_12;
		if ((((int32_t)L_91) == ((int32_t)((int32_t)116))))
		{
			goto IL_02c7;
		}
	}
	{
		uint16_t L_92 = V_12;
		if ((((int32_t)L_92) == ((int32_t)((int32_t)123))))
		{
			goto IL_0295;
		}
	}
	{
		goto IL_02d7;
	}

IL_0295:
	{
		V_2 = 0;
		goto IL_02e3;
	}

IL_029c:
	{
		V_2 = 1;
		goto IL_02e3;
	}

IL_02a3:
	{
		JSONValue_t8_4 * L_93 = V_0;
		NullCheck(L_93);
		int32_t L_94 = JSONValue_get_Type_m8_9(L_93, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_94) == ((uint32_t)3))))
		{
			goto IL_02b6;
		}
	}
	{
		V_2 = 3;
		goto IL_02c2;
	}

IL_02b6:
	{
		int32_t L_95 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_96 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5092, L_95, /*hidden argument*/NULL);
		return L_96;
	}

IL_02c2:
	{
		goto IL_02e3;
	}

IL_02c7:
	{
		V_2 = ((int32_t)10);
		goto IL_02e3;
	}

IL_02cf:
	{
		V_2 = ((int32_t)11);
		goto IL_02e3;
	}

IL_02d7:
	{
		int32_t L_97 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_98 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5093, L_97, /*hidden argument*/NULL);
		return L_98;
	}

IL_02e3:
	{
		int32_t L_99 = V_3;
		V_3 = ((int32_t)((int32_t)L_99-(int32_t)1));
		goto IL_0731;
	}

IL_02ec:
	{
		String_t* L_100 = ___jsonString;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		String_t* L_101 = JSONObject_ParseString_m8_59(NULL /*static, unused*/, L_100, (&V_3), /*hidden argument*/NULL);
		V_7 = L_101;
		String_t* L_102 = V_7;
		if (L_102)
		{
			goto IL_0309;
		}
	}
	{
		int32_t L_103 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_104 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5094, L_103, /*hidden argument*/NULL);
		return L_104;
	}

IL_0309:
	{
		JSONValue_t8_4 * L_105 = V_0;
		NullCheck(L_105);
		int32_t L_106 = JSONValue_get_Type_m8_9(L_105, /*hidden argument*/NULL);
		V_11 = L_106;
		int32_t L_107 = V_11;
		if ((((int32_t)L_107) == ((int32_t)2)))
		{
			goto IL_0326;
		}
	}
	{
		int32_t L_108 = V_11;
		if ((((int32_t)L_108) == ((int32_t)3)))
		{
			goto IL_0348;
		}
	}
	{
		goto IL_035f;
	}

IL_0326:
	{
		JSONValue_t8_4 * L_109 = V_0;
		NullCheck(L_109);
		JSONObject_t8_5 * L_110 = JSONValue_get_Obj_m8_15(L_109, /*hidden argument*/NULL);
		NullCheck(L_110);
		Object_t* L_111 = (L_110->___values_0);
		List_1_t1_1742 * L_112 = V_1;
		String_t* L_113 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_112, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		String_t* L_114 = V_7;
		JSONValue_t8_4 * L_115 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_3(L_115, L_114, /*hidden argument*/NULL);
		NullCheck(L_111);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_111, L_113, L_115);
		goto IL_036b;
	}

IL_0348:
	{
		JSONValue_t8_4 * L_116 = V_0;
		NullCheck(L_116);
		JSONArray_t8_6 * L_117 = JSONValue_get_Array_m8_17(L_116, /*hidden argument*/NULL);
		String_t* L_118 = V_7;
		JSONValue_t8_4 * L_119 = JSONValue_op_Implicit_m8_24(NULL /*static, unused*/, L_118, /*hidden argument*/NULL);
		NullCheck(L_117);
		JSONArray_Add_m8_32(L_117, L_119, /*hidden argument*/NULL);
		goto IL_036b;
	}

IL_035f:
	{
		JSONLogger_Error_m8_1(NULL /*static, unused*/, _stringLiteral5095, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}

IL_036b:
	{
		V_2 = 7;
		goto IL_0731;
	}

IL_0372:
	{
		String_t* L_120 = ___jsonString;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		double L_121 = JSONObject_ParseNumber_m8_60(NULL /*static, unused*/, L_120, (&V_3), /*hidden argument*/NULL);
		V_8 = L_121;
		double L_122 = V_8;
		bool L_123 = Double_IsNaN_m1_659(NULL /*static, unused*/, L_122, /*hidden argument*/NULL);
		if (!L_123)
		{
			goto IL_0394;
		}
	}
	{
		int32_t L_124 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_125 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5096, L_124, /*hidden argument*/NULL);
		return L_125;
	}

IL_0394:
	{
		JSONValue_t8_4 * L_126 = V_0;
		NullCheck(L_126);
		int32_t L_127 = JSONValue_get_Type_m8_9(L_126, /*hidden argument*/NULL);
		V_11 = L_127;
		int32_t L_128 = V_11;
		if ((((int32_t)L_128) == ((int32_t)2)))
		{
			goto IL_03b1;
		}
	}
	{
		int32_t L_129 = V_11;
		if ((((int32_t)L_129) == ((int32_t)3)))
		{
			goto IL_03d3;
		}
	}
	{
		goto IL_03ea;
	}

IL_03b1:
	{
		JSONValue_t8_4 * L_130 = V_0;
		NullCheck(L_130);
		JSONObject_t8_5 * L_131 = JSONValue_get_Obj_m8_15(L_130, /*hidden argument*/NULL);
		NullCheck(L_131);
		Object_t* L_132 = (L_131->___values_0);
		List_1_t1_1742 * L_133 = V_1;
		String_t* L_134 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_133, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		double L_135 = V_8;
		JSONValue_t8_4 * L_136 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_4(L_136, L_135, /*hidden argument*/NULL);
		NullCheck(L_132);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_132, L_134, L_136);
		goto IL_03f6;
	}

IL_03d3:
	{
		JSONValue_t8_4 * L_137 = V_0;
		NullCheck(L_137);
		JSONArray_t8_6 * L_138 = JSONValue_get_Array_m8_17(L_137, /*hidden argument*/NULL);
		double L_139 = V_8;
		JSONValue_t8_4 * L_140 = JSONValue_op_Implicit_m8_25(NULL /*static, unused*/, L_139, /*hidden argument*/NULL);
		NullCheck(L_138);
		JSONArray_Add_m8_32(L_138, L_140, /*hidden argument*/NULL);
		goto IL_03f6;
	}

IL_03ea:
	{
		JSONLogger_Error_m8_1(NULL /*static, unused*/, _stringLiteral5095, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}

IL_03f6:
	{
		V_2 = 7;
		goto IL_0731;
	}

IL_03fd:
	{
		String_t* L_141 = ___jsonString;
		int32_t L_142 = V_3;
		NullCheck(L_141);
		uint16_t L_143 = String_get_Chars_m1_442(L_141, L_142, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_143) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_04be;
		}
	}
	{
		String_t* L_144 = ___jsonString;
		NullCheck(L_144);
		int32_t L_145 = String_get_Length_m1_571(L_144, /*hidden argument*/NULL);
		int32_t L_146 = V_3;
		if ((((int32_t)L_145) < ((int32_t)((int32_t)((int32_t)L_146+(int32_t)4)))))
		{
			goto IL_0449;
		}
	}
	{
		String_t* L_147 = ___jsonString;
		int32_t L_148 = V_3;
		NullCheck(L_147);
		uint16_t L_149 = String_get_Chars_m1_442(L_147, ((int32_t)((int32_t)L_148+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_149) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0449;
		}
	}
	{
		String_t* L_150 = ___jsonString;
		int32_t L_151 = V_3;
		NullCheck(L_150);
		uint16_t L_152 = String_get_Chars_m1_442(L_150, ((int32_t)((int32_t)L_151+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_152) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0449;
		}
	}
	{
		String_t* L_153 = ___jsonString;
		int32_t L_154 = V_3;
		NullCheck(L_153);
		uint16_t L_155 = String_get_Chars_m1_442(L_153, ((int32_t)((int32_t)L_154+(int32_t)3)), /*hidden argument*/NULL);
		if ((((int32_t)L_155) == ((int32_t)((int32_t)101))))
		{
			goto IL_0455;
		}
	}

IL_0449:
	{
		int32_t L_156 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_157 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral2147, L_156, /*hidden argument*/NULL);
		return L_157;
	}

IL_0455:
	{
		JSONValue_t8_4 * L_158 = V_0;
		NullCheck(L_158);
		int32_t L_159 = JSONValue_get_Type_m8_9(L_158, /*hidden argument*/NULL);
		V_11 = L_159;
		int32_t L_160 = V_11;
		if ((((int32_t)L_160) == ((int32_t)2)))
		{
			goto IL_0472;
		}
	}
	{
		int32_t L_161 = V_11;
		if ((((int32_t)L_161) == ((int32_t)3)))
		{
			goto IL_0493;
		}
	}
	{
		goto IL_04a9;
	}

IL_0472:
	{
		JSONValue_t8_4 * L_162 = V_0;
		NullCheck(L_162);
		JSONObject_t8_5 * L_163 = JSONValue_get_Obj_m8_15(L_162, /*hidden argument*/NULL);
		NullCheck(L_163);
		Object_t* L_164 = (L_163->___values_0);
		List_1_t1_1742 * L_165 = V_1;
		String_t* L_166 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_165, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		JSONValue_t8_4 * L_167 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_7(L_167, 1, /*hidden argument*/NULL);
		NullCheck(L_164);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_164, L_166, L_167);
		goto IL_04b5;
	}

IL_0493:
	{
		JSONValue_t8_4 * L_168 = V_0;
		NullCheck(L_168);
		JSONArray_t8_6 * L_169 = JSONValue_get_Array_m8_17(L_168, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_170 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_7(L_170, 1, /*hidden argument*/NULL);
		NullCheck(L_169);
		JSONArray_Add_m8_32(L_169, L_170, /*hidden argument*/NULL);
		goto IL_04b5;
	}

IL_04a9:
	{
		JSONLogger_Error_m8_1(NULL /*static, unused*/, _stringLiteral5095, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}

IL_04b5:
	{
		int32_t L_171 = V_3;
		V_3 = ((int32_t)((int32_t)L_171+(int32_t)3));
		goto IL_057c;
	}

IL_04be:
	{
		String_t* L_172 = ___jsonString;
		NullCheck(L_172);
		int32_t L_173 = String_get_Length_m1_571(L_172, /*hidden argument*/NULL);
		int32_t L_174 = V_3;
		if ((((int32_t)L_173) < ((int32_t)((int32_t)((int32_t)L_174+(int32_t)5)))))
		{
			goto IL_050c;
		}
	}
	{
		String_t* L_175 = ___jsonString;
		int32_t L_176 = V_3;
		NullCheck(L_175);
		uint16_t L_177 = String_get_Chars_m1_442(L_175, ((int32_t)((int32_t)L_176+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_177) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_050c;
		}
	}
	{
		String_t* L_178 = ___jsonString;
		int32_t L_179 = V_3;
		NullCheck(L_178);
		uint16_t L_180 = String_get_Chars_m1_442(L_178, ((int32_t)((int32_t)L_179+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_180) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_050c;
		}
	}
	{
		String_t* L_181 = ___jsonString;
		int32_t L_182 = V_3;
		NullCheck(L_181);
		uint16_t L_183 = String_get_Chars_m1_442(L_181, ((int32_t)((int32_t)L_182+(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_183) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_050c;
		}
	}
	{
		String_t* L_184 = ___jsonString;
		int32_t L_185 = V_3;
		NullCheck(L_184);
		uint16_t L_186 = String_get_Chars_m1_442(L_184, ((int32_t)((int32_t)L_185+(int32_t)4)), /*hidden argument*/NULL);
		if ((((int32_t)L_186) == ((int32_t)((int32_t)101))))
		{
			goto IL_0518;
		}
	}

IL_050c:
	{
		int32_t L_187 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_188 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral3328, L_187, /*hidden argument*/NULL);
		return L_188;
	}

IL_0518:
	{
		JSONValue_t8_4 * L_189 = V_0;
		NullCheck(L_189);
		int32_t L_190 = JSONValue_get_Type_m8_9(L_189, /*hidden argument*/NULL);
		V_11 = L_190;
		int32_t L_191 = V_11;
		if ((((int32_t)L_191) == ((int32_t)2)))
		{
			goto IL_0535;
		}
	}
	{
		int32_t L_192 = V_11;
		if ((((int32_t)L_192) == ((int32_t)3)))
		{
			goto IL_0556;
		}
	}
	{
		goto IL_056c;
	}

IL_0535:
	{
		JSONValue_t8_4 * L_193 = V_0;
		NullCheck(L_193);
		JSONObject_t8_5 * L_194 = JSONValue_get_Obj_m8_15(L_193, /*hidden argument*/NULL);
		NullCheck(L_194);
		Object_t* L_195 = (L_194->___values_0);
		List_1_t1_1742 * L_196 = V_1;
		String_t* L_197 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_196, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		JSONValue_t8_4 * L_198 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_7(L_198, 0, /*hidden argument*/NULL);
		NullCheck(L_195);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_195, L_197, L_198);
		goto IL_0578;
	}

IL_0556:
	{
		JSONValue_t8_4 * L_199 = V_0;
		NullCheck(L_199);
		JSONArray_t8_6 * L_200 = JSONValue_get_Array_m8_17(L_199, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_201 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_7(L_201, 0, /*hidden argument*/NULL);
		NullCheck(L_200);
		JSONArray_Add_m8_32(L_200, L_201, /*hidden argument*/NULL);
		goto IL_0578;
	}

IL_056c:
	{
		JSONLogger_Error_m8_1(NULL /*static, unused*/, _stringLiteral5095, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}

IL_0578:
	{
		int32_t L_202 = V_3;
		V_3 = ((int32_t)((int32_t)L_202+(int32_t)4));
	}

IL_057c:
	{
		V_2 = 7;
		goto IL_0731;
	}

IL_0583:
	{
		String_t* L_203 = ___jsonString;
		int32_t L_204 = V_3;
		NullCheck(L_203);
		uint16_t L_205 = String_get_Chars_m1_442(L_203, L_204, /*hidden argument*/NULL);
		if ((((int32_t)L_205) == ((int32_t)((int32_t)91))))
		{
			goto IL_059a;
		}
	}
	{
		int32_t L_206 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_207 = JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)91), L_206, /*hidden argument*/NULL);
		return L_207;
	}

IL_059a:
	{
		JSONArray_t8_6 * L_208 = (JSONArray_t8_6 *)il2cpp_codegen_object_new (JSONArray_t8_6_il2cpp_TypeInfo_var);
		JSONArray__ctor_m8_29(L_208, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_209 = JSONValue_op_Implicit_m8_27(NULL /*static, unused*/, L_208, /*hidden argument*/NULL);
		V_9 = L_209;
		JSONValue_t8_4 * L_210 = V_0;
		if (!L_210)
		{
			goto IL_05b4;
		}
	}
	{
		JSONValue_t8_4 * L_211 = V_9;
		JSONValue_t8_4 * L_212 = V_0;
		NullCheck(L_211);
		JSONValue_set_Parent_m8_22(L_211, L_212, /*hidden argument*/NULL);
	}

IL_05b4:
	{
		JSONValue_t8_4 * L_213 = V_9;
		V_0 = L_213;
		V_2 = 5;
		goto IL_0731;
	}

IL_05be:
	{
		String_t* L_214 = ___jsonString;
		int32_t L_215 = V_3;
		NullCheck(L_214);
		uint16_t L_216 = String_get_Chars_m1_442(L_214, L_215, /*hidden argument*/NULL);
		if ((((int32_t)L_216) == ((int32_t)((int32_t)93))))
		{
			goto IL_05d5;
		}
	}
	{
		int32_t L_217 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_218 = JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)93), L_217, /*hidden argument*/NULL);
		return L_218;
	}

IL_05d5:
	{
		JSONValue_t8_4 * L_219 = V_0;
		NullCheck(L_219);
		JSONValue_t8_4 * L_220 = JSONValue_get_Parent_m8_21(L_219, /*hidden argument*/NULL);
		if (L_220)
		{
			goto IL_05e7;
		}
	}
	{
		JSONValue_t8_4 * L_221 = V_0;
		NullCheck(L_221);
		JSONObject_t8_5 * L_222 = JSONValue_get_Obj_m8_15(L_221, /*hidden argument*/NULL);
		return L_222;
	}

IL_05e7:
	{
		JSONValue_t8_4 * L_223 = V_0;
		NullCheck(L_223);
		JSONValue_t8_4 * L_224 = JSONValue_get_Parent_m8_21(L_223, /*hidden argument*/NULL);
		NullCheck(L_224);
		int32_t L_225 = JSONValue_get_Type_m8_9(L_224, /*hidden argument*/NULL);
		V_11 = L_225;
		int32_t L_226 = V_11;
		if ((((int32_t)L_226) == ((int32_t)2)))
		{
			goto IL_0609;
		}
	}
	{
		int32_t L_227 = V_11;
		if ((((int32_t)L_227) == ((int32_t)3)))
		{
			goto IL_0634;
		}
	}
	{
		goto IL_0654;
	}

IL_0609:
	{
		JSONValue_t8_4 * L_228 = V_0;
		NullCheck(L_228);
		JSONValue_t8_4 * L_229 = JSONValue_get_Parent_m8_21(L_228, /*hidden argument*/NULL);
		NullCheck(L_229);
		JSONObject_t8_5 * L_230 = JSONValue_get_Obj_m8_15(L_229, /*hidden argument*/NULL);
		NullCheck(L_230);
		Object_t* L_231 = (L_230->___values_0);
		List_1_t1_1742 * L_232 = V_1;
		String_t* L_233 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_232, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		JSONValue_t8_4 * L_234 = V_0;
		NullCheck(L_234);
		JSONArray_t8_6 * L_235 = JSONValue_get_Array_m8_17(L_234, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_236 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_6(L_236, L_235, /*hidden argument*/NULL);
		NullCheck(L_231);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_231, L_233, L_236);
		goto IL_0660;
	}

IL_0634:
	{
		JSONValue_t8_4 * L_237 = V_0;
		NullCheck(L_237);
		JSONValue_t8_4 * L_238 = JSONValue_get_Parent_m8_21(L_237, /*hidden argument*/NULL);
		NullCheck(L_238);
		JSONArray_t8_6 * L_239 = JSONValue_get_Array_m8_17(L_238, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_240 = V_0;
		NullCheck(L_240);
		JSONArray_t8_6 * L_241 = JSONValue_get_Array_m8_17(L_240, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_242 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_6(L_242, L_241, /*hidden argument*/NULL);
		NullCheck(L_239);
		JSONArray_Add_m8_32(L_239, L_242, /*hidden argument*/NULL);
		goto IL_0660;
	}

IL_0654:
	{
		int32_t L_243 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_244 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral5089, L_243, /*hidden argument*/NULL);
		return L_244;
	}

IL_0660:
	{
		JSONValue_t8_4 * L_245 = V_0;
		NullCheck(L_245);
		JSONValue_t8_4 * L_246 = JSONValue_get_Parent_m8_21(L_245, /*hidden argument*/NULL);
		V_0 = L_246;
		V_2 = 7;
		goto IL_0731;
	}

IL_066e:
	{
		String_t* L_247 = ___jsonString;
		int32_t L_248 = V_3;
		NullCheck(L_247);
		uint16_t L_249 = String_get_Chars_m1_442(L_247, L_248, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_249) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_072a;
		}
	}
	{
		String_t* L_250 = ___jsonString;
		NullCheck(L_250);
		int32_t L_251 = String_get_Length_m1_571(L_250, /*hidden argument*/NULL);
		int32_t L_252 = V_3;
		if ((((int32_t)L_251) < ((int32_t)((int32_t)((int32_t)L_252+(int32_t)4)))))
		{
			goto IL_06ba;
		}
	}
	{
		String_t* L_253 = ___jsonString;
		int32_t L_254 = V_3;
		NullCheck(L_253);
		uint16_t L_255 = String_get_Chars_m1_442(L_253, ((int32_t)((int32_t)L_254+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_255) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_06ba;
		}
	}
	{
		String_t* L_256 = ___jsonString;
		int32_t L_257 = V_3;
		NullCheck(L_256);
		uint16_t L_258 = String_get_Chars_m1_442(L_256, ((int32_t)((int32_t)L_257+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_258) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_06ba;
		}
	}
	{
		String_t* L_259 = ___jsonString;
		int32_t L_260 = V_3;
		NullCheck(L_259);
		uint16_t L_261 = String_get_Chars_m1_442(L_259, ((int32_t)((int32_t)L_260+(int32_t)3)), /*hidden argument*/NULL);
		if ((((int32_t)L_261) == ((int32_t)((int32_t)108))))
		{
			goto IL_06c6;
		}
	}

IL_06ba:
	{
		int32_t L_262 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_263 = JSONObject_Fail_m8_62(NULL /*static, unused*/, _stringLiteral994, L_262, /*hidden argument*/NULL);
		return L_263;
	}

IL_06c6:
	{
		JSONValue_t8_4 * L_264 = V_0;
		NullCheck(L_264);
		int32_t L_265 = JSONValue_get_Type_m8_9(L_264, /*hidden argument*/NULL);
		V_11 = L_265;
		int32_t L_266 = V_11;
		if ((((int32_t)L_266) == ((int32_t)2)))
		{
			goto IL_06e3;
		}
	}
	{
		int32_t L_267 = V_11;
		if ((((int32_t)L_267) == ((int32_t)3)))
		{
			goto IL_0704;
		}
	}
	{
		goto IL_071a;
	}

IL_06e3:
	{
		JSONValue_t8_4 * L_268 = V_0;
		NullCheck(L_268);
		JSONObject_t8_5 * L_269 = JSONValue_get_Obj_m8_15(L_268, /*hidden argument*/NULL);
		NullCheck(L_269);
		Object_t* L_270 = (L_269->___values_0);
		List_1_t1_1742 * L_271 = V_1;
		String_t* L_272 = Extensions_Pop_TisString_t_m8_1968(NULL /*static, unused*/, L_271, /*hidden argument*/Extensions_Pop_TisString_t_m8_1968_MethodInfo_var);
		JSONValue_t8_4 * L_273 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_2(L_273, 5, /*hidden argument*/NULL);
		NullCheck(L_270);
		InterfaceActionInvoker2< String_t*, JSONValue_t8_4 * >::Invoke(5 /* System.Void System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::set_Item(!0,!1) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_270, L_272, L_273);
		goto IL_0726;
	}

IL_0704:
	{
		JSONValue_t8_4 * L_274 = V_0;
		NullCheck(L_274);
		JSONArray_t8_6 * L_275 = JSONValue_get_Array_m8_17(L_274, /*hidden argument*/NULL);
		JSONValue_t8_4 * L_276 = (JSONValue_t8_4 *)il2cpp_codegen_object_new (JSONValue_t8_4_il2cpp_TypeInfo_var);
		JSONValue__ctor_m8_2(L_276, 5, /*hidden argument*/NULL);
		NullCheck(L_275);
		JSONArray_Add_m8_32(L_275, L_276, /*hidden argument*/NULL);
		goto IL_0726;
	}

IL_071a:
	{
		JSONLogger_Error_m8_1(NULL /*static, unused*/, _stringLiteral5095, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}

IL_0726:
	{
		int32_t L_277 = V_3;
		V_3 = ((int32_t)((int32_t)L_277+(int32_t)3));
	}

IL_072a:
	{
		V_2 = 7;
		goto IL_0731;
	}

IL_0731:
	{
		int32_t L_278 = V_3;
		V_3 = ((int32_t)((int32_t)L_278+(int32_t)1));
	}

IL_0735:
	{
		int32_t L_279 = V_3;
		String_t* L_280 = ___jsonString;
		NullCheck(L_280);
		int32_t L_281 = String_get_Length_m1_571(L_280, /*hidden argument*/NULL);
		if ((((int32_t)L_279) < ((int32_t)L_281)))
		{
			goto IL_001e;
		}
	}
	{
		JSONLogger_Error_m8_1(NULL /*static, unused*/, _stringLiteral5097, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}
}
// System.Int32 Boomlagoon.JSON.JSONObject::SkipWhitespace(System.String,System.Int32)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int32_t JSONObject_SkipWhitespace_m8_58 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___pos, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_000a;
	}

IL_0005:
	{
		int32_t L_0 = ___pos;
		___pos = ((int32_t)((int32_t)L_0+(int32_t)1));
	}

IL_000a:
	{
		int32_t L_1 = ___pos;
		String_t* L_2 = ___str;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_4 = ___str;
		int32_t L_5 = ___pos;
		NullCheck(L_4);
		uint16_t L_6 = String_get_Chars_m1_442(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_7 = Char_IsWhiteSpace_m1_398(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0005;
		}
	}

IL_0027:
	{
		int32_t L_8 = ___pos;
		return L_8;
	}
}
// System.String Boomlagoon.JSON.JSONObject::ParseString(System.String,System.Int32&)
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern "C" String_t* JSONObject_ParseString_m8_59 (Object_t * __this /* static, unused */, String_t* ___str, int32_t* ___startPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	Match_t3_13 * V_2 = {0};
	String_t* V_3 = {0};
	{
		String_t* L_0 = ___str;
		int32_t* L_1 = ___startPosition;
		NullCheck(L_0);
		uint16_t L_2 = String_get_Chars_m1_442(L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_001e;
		}
	}
	{
		int32_t* L_3 = ___startPosition;
		String_t* L_4 = ___str;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1_571(L_4, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)(*((int32_t*)L_3))+(int32_t)1))) < ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}

IL_001e:
	{
		int32_t* L_6 = ___startPosition;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)34), (*((int32_t*)L_6)), /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_002a:
	{
		String_t* L_7 = ___str;
		int32_t* L_8 = ___startPosition;
		NullCheck(L_7);
		int32_t L_9 = String_IndexOf_m1_501(L_7, ((int32_t)34), ((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		int32_t* L_11 = ___startPosition;
		if ((((int32_t)L_10) > ((int32_t)(*((int32_t*)L_11)))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t* L_12 = ___startPosition;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)34), ((int32_t)((int32_t)(*((int32_t*)L_12))+(int32_t)1)), /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_004d:
	{
		goto IL_0074;
	}

IL_0052:
	{
		String_t* L_13 = ___str;
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = String_IndexOf_m1_501(L_13, ((int32_t)34), ((int32_t)((int32_t)L_14+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_15;
		int32_t L_16 = V_0;
		int32_t* L_17 = ___startPosition;
		if ((((int32_t)L_16) > ((int32_t)(*((int32_t*)L_17)))))
		{
			goto IL_0074;
		}
	}
	{
		int32_t* L_18 = ___startPosition;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_Fail_m8_61(NULL /*static, unused*/, ((int32_t)34), ((int32_t)((int32_t)(*((int32_t*)L_18))+(int32_t)1)), /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_0074:
	{
		String_t* L_19 = ___str;
		int32_t L_20 = V_0;
		NullCheck(L_19);
		uint16_t L_21 = String_get_Chars_m1_442(L_19, ((int32_t)((int32_t)L_20-(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)((int32_t)92))))
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_22;
		int32_t L_23 = V_0;
		int32_t* L_24 = ___startPosition;
		if ((((int32_t)L_23) <= ((int32_t)((int32_t)((int32_t)(*((int32_t*)L_24))+(int32_t)1)))))
		{
			goto IL_00a5;
		}
	}
	{
		String_t* L_25 = ___str;
		int32_t* L_26 = ___startPosition;
		int32_t L_27 = V_0;
		int32_t* L_28 = ___startPosition;
		NullCheck(L_25);
		String_t* L_29 = String_Substring_m1_455(L_25, ((int32_t)((int32_t)(*((int32_t*)L_26))+(int32_t)1)), ((int32_t)((int32_t)((int32_t)((int32_t)L_27-(int32_t)(*((int32_t*)L_28))))-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_29;
	}

IL_00a5:
	{
		int32_t* L_30 = ___startPosition;
		int32_t L_31 = V_0;
		*((int32_t*)(L_30)) = (int32_t)L_31;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		Regex_t3_11 * L_32 = ((JSONObject_t8_5_StaticFields*)JSONObject_t8_5_il2cpp_TypeInfo_var->static_fields)->___unicodeRegex_1;
		String_t* L_33 = V_1;
		NullCheck(L_32);
		Match_t3_13 * L_34 = Regex_Match_m3_1402(L_32, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		Match_t3_13 * L_35 = V_2;
		NullCheck(L_35);
		bool L_36 = Group_get_Success_m3_19(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_00c4;
		}
	}
	{
		goto IL_0136;
	}

IL_00c4:
	{
		Match_t3_13 * L_37 = V_2;
		NullCheck(L_37);
		GroupCollection_t3_14 * L_38 = (GroupCollection_t3_14 *)VirtFuncInvoker0< GroupCollection_t3_14 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_37);
		NullCheck(L_38);
		Group_t3_15 * L_39 = GroupCollection_get_Item_m3_20(L_38, 1, /*hidden argument*/NULL);
		NullCheck(L_39);
		CaptureCollection_t3_177 * L_40 = Group_get_Captures_m3_1354(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		Capture_t3_176 * L_41 = CaptureCollection_get_Item_m3_1345(L_40, 0, /*hidden argument*/NULL);
		NullCheck(L_41);
		String_t* L_42 = Capture_get_Value_m3_21(L_41, /*hidden argument*/NULL);
		V_3 = L_42;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_43 = ((JSONObject_t8_5_StaticFields*)JSONObject_t8_5_il2cpp_TypeInfo_var->static_fields)->___unicodeBytes_2;
		String_t* L_44 = V_3;
		NullCheck(L_44);
		String_t* L_45 = String_Substring_m1_455(L_44, 0, 2, /*hidden argument*/NULL);
		uint8_t L_46 = Byte_Parse_m1_236(NULL /*static, unused*/, L_45, ((int32_t)515), /*hidden argument*/NULL);
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_43, 1, sizeof(uint8_t))) = (uint8_t)L_46;
		ByteU5BU5D_t1_109* L_47 = ((JSONObject_t8_5_StaticFields*)JSONObject_t8_5_il2cpp_TypeInfo_var->static_fields)->___unicodeBytes_2;
		String_t* L_48 = V_3;
		NullCheck(L_48);
		String_t* L_49 = String_Substring_m1_455(L_48, 2, 2, /*hidden argument*/NULL);
		uint8_t L_50 = Byte_Parse_m1_236(NULL /*static, unused*/, L_49, ((int32_t)515), /*hidden argument*/NULL);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_47, 0, sizeof(uint8_t))) = (uint8_t)L_50;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_51 = Encoding_get_Unicode_m1_12366(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_52 = ((JSONObject_t8_5_StaticFields*)JSONObject_t8_5_il2cpp_TypeInfo_var->static_fields)->___unicodeBytes_2;
		NullCheck(L_51);
		String_t* L_53 = (String_t*)VirtFuncInvoker1< String_t*, ByteU5BU5D_t1_109* >::Invoke(27 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_51, L_52);
		V_3 = L_53;
		String_t* L_54 = V_1;
		Match_t3_13 * L_55 = V_2;
		NullCheck(L_55);
		String_t* L_56 = Capture_get_Value_m3_21(L_55, /*hidden argument*/NULL);
		String_t* L_57 = V_3;
		NullCheck(L_54);
		String_t* L_58 = String_Replace_m1_536(L_54, L_56, L_57, /*hidden argument*/NULL);
		V_1 = L_58;
		goto IL_00a8;
	}

IL_0136:
	{
		String_t* L_59 = V_1;
		return L_59;
	}
}
// System.Double Boomlagoon.JSON.JSONObject::ParseNumber(System.String,System.Int32&)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" double JSONObject_ParseNumber_m8_60 (Object_t * __this /* static, unused */, String_t* ___str, int32_t* ___startPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	double V_1 = 0.0;
	{
		int32_t* L_0 = ___startPosition;
		String_t* L_1 = ___str;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		if ((((int32_t)(*((int32_t*)L_0))) >= ((int32_t)L_2)))
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_3 = ___str;
		int32_t* L_4 = ___startPosition;
		NullCheck(L_3);
		uint16_t L_5 = String_get_Chars_m1_442(L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_6 = Char_IsDigit_m1_374(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_7 = ___str;
		int32_t* L_8 = ___startPosition;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_442(L_7, (*((int32_t*)L_8)), /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)((int32_t)45))))
		{
			goto IL_0038;
		}
	}

IL_002e:
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_0038:
	{
		int32_t* L_10 = ___startPosition;
		V_0 = ((int32_t)((int32_t)(*((int32_t*)L_10))+(int32_t)1));
		goto IL_0046;
	}

IL_0042:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_12 = V_0;
		String_t* L_13 = ___str;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1_571(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) >= ((int32_t)L_14)))
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_15 = ___str;
		int32_t L_16 = V_0;
		NullCheck(L_15);
		uint16_t L_17 = String_get_Chars_m1_442(L_15, L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) == ((int32_t)((int32_t)44))))
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_18 = ___str;
		int32_t L_19 = V_0;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m1_442(L_18, L_19, /*hidden argument*/NULL);
		if ((((int32_t)L_20) == ((int32_t)((int32_t)93))))
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_21 = ___str;
		int32_t L_22 = V_0;
		NullCheck(L_21);
		uint16_t L_23 = String_get_Chars_m1_442(L_21, L_22, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0042;
		}
	}

IL_007c:
	{
		String_t* L_24 = ___str;
		int32_t* L_25 = ___startPosition;
		int32_t L_26 = V_0;
		int32_t* L_27 = ___startPosition;
		NullCheck(L_24);
		String_t* L_28 = String_Substring_m1_455(L_24, (*((int32_t*)L_25)), ((int32_t)((int32_t)L_26-(int32_t)(*((int32_t*)L_27)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_29 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_30 = Double_TryParse_m1_669(NULL /*static, unused*/, L_28, ((int32_t)167), L_29, (&V_1), /*hidden argument*/NULL);
		if (L_30)
		{
			goto IL_00a8;
		}
	}
	{
		return (std::numeric_limits<double>::quiet_NaN());
	}

IL_00a8:
	{
		int32_t* L_31 = ___startPosition;
		int32_t L_32 = V_0;
		*((int32_t*)(L_31)) = (int32_t)((int32_t)((int32_t)L_32-(int32_t)1));
		double L_33 = V_1;
		return L_33;
	}
}
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::Fail(System.Char,System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern "C" JSONObject_t8_5 * JSONObject_Fail_m8_61 (Object_t * __this /* static, unused */, uint16_t ___expected, int32_t ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ___expected;
		String_t* L_1 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_1 = String_CreateString_m1_586(L_1, L_0, 1, /*hidden argument*/NULL);
		int32_t L_2 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_3 = JSONObject_Fail_m8_62(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONObject::Fail(System.String,System.Int32)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5098;
extern Il2CppCodeGenString* _stringLiteral915;
extern "C" JSONObject_t8_5 * JSONObject_Fail_m8_62 (Object_t * __this /* static, unused */, String_t* ___expected, int32_t ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5098 = il2cpp_codegen_string_literal_from_index(5098);
		_stringLiteral915 = il2cpp_codegen_string_literal_from_index(915);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5098);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5098;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		String_t* L_2 = ___expected;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_272* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral915);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral915;
		ObjectU5BU5D_t1_272* L_4 = L_3;
		int32_t L_5 = ___position;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_562(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		JSONLogger_Error_m8_1(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (JSONObject_t8_5 *)NULL;
	}
}
// System.String Boomlagoon.JSON.JSONObject::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t1_1920_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t1_1916_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_1_t1_1921_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1_15031_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1_15032_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral583;
extern "C" String_t* JSONObject_ToString_m8_63 (JSONObject_t8_5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		IEnumerable_1_t1_1920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1938);
		IEnumerator_1_t1_1916_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1940);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		ICollection_1_t1_1921_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1942);
		KeyValuePair_2_get_Key_m1_15031_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484110);
		KeyValuePair_2_get_Value_m1_15032_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484111);
		_stringLiteral583 = il2cpp_codegen_string_literal_from_index(583);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	KeyValuePair_2_t1_1915  V_1 = {0};
	Object_t* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)123), /*hidden argument*/NULL);
		Object_t* L_2 = (__this->___values_0);
		NullCheck(L_2);
		Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1_1920_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0069;
		}

IL_0020:
		{
			Object_t* L_4 = V_2;
			NullCheck(L_4);
			KeyValuePair_2_t1_1915  L_5 = (KeyValuePair_2_t1_1915 )InterfaceFuncInvoker0< KeyValuePair_2_t1_1915  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::get_Current() */, IEnumerator_1_t1_1916_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			StringBuilder_t1_247 * L_6 = V_0;
			String_t* L_7 = KeyValuePair_2_get_Key_m1_15031((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1_15031_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_8 = String_Concat_m1_560(NULL /*static, unused*/, _stringLiteral583, L_7, _stringLiteral583, /*hidden argument*/NULL);
			NullCheck(L_6);
			StringBuilder_Append_m1_12438(L_6, L_8, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_9 = V_0;
			NullCheck(L_9);
			StringBuilder_Append_m1_12452(L_9, ((int32_t)58), /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_10 = V_0;
			JSONValue_t8_4 * L_11 = KeyValuePair_2_get_Value_m1_15032((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1_15032_MethodInfo_var);
			NullCheck(L_11);
			String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String Boomlagoon.JSON.JSONValue::ToString() */, L_11);
			NullCheck(L_10);
			StringBuilder_Append_m1_12438(L_10, L_12, /*hidden argument*/NULL);
			StringBuilder_t1_247 * L_13 = V_0;
			NullCheck(L_13);
			StringBuilder_Append_m1_12452(L_13, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0069:
		{
			Object_t* L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0020;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		{
			Object_t* L_16 = V_2;
			if (L_16)
			{
				goto IL_007d;
			}
		}

IL_007c:
		{
			IL2CPP_END_FINALLY(121)
		}

IL_007d:
		{
			Object_t* L_17 = V_2;
			NullCheck(L_17);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_17);
			IL2CPP_END_FINALLY(121)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0084:
	{
		Object_t* L_18 = (__this->___values_0);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::get_Count() */, ICollection_1_t1_1921_il2cpp_TypeInfo_var, L_18);
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		StringBuilder_t1_247 * L_20 = V_0;
		StringBuilder_t1_247 * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = StringBuilder_get_Length_m1_12424(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		StringBuilder_Remove_m1_12432(L_20, ((int32_t)((int32_t)L_22-(int32_t)1)), 1, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		StringBuilder_t1_247 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m1_12452(L_23, ((int32_t)125), /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_24 = V_0;
		NullCheck(L_24);
		String_t* L_25 = StringBuilder_ToString_m1_12428(L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>> Boomlagoon.JSON.JSONObject::GetEnumerator()
extern TypeInfo* IEnumerable_1_t1_1920_il2cpp_TypeInfo_var;
extern "C" Object_t* JSONObject_GetEnumerator_m8_64 (JSONObject_t8_5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t1_1920_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1938);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		NullCheck(L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::GetEnumerator() */, IEnumerable_1_t1_1920_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::Clear()
extern TypeInfo* ICollection_1_t1_1921_il2cpp_TypeInfo_var;
extern "C" void JSONObject_Clear_m8_65 (JSONObject_t8_5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_1_t1_1921_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1942);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>>::Clear() */, ICollection_1_t1_1921_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Boomlagoon.JSON.JSONObject::Remove(System.String)
extern TypeInfo* IDictionary_2_t1_1899_il2cpp_TypeInfo_var;
extern "C" void JSONObject_Remove_m8_66 (JSONObject_t8_5 * __this, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1_1899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1941);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (__this->___values_0);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::ContainsKey(!0) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Object_t* L_3 = (__this->___values_0);
		String_t* L_4 = ___key;
		NullCheck(L_3);
		InterfaceFuncInvoker1< bool, String_t* >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,Boomlagoon.JSON.JSONValue>::Remove(!0) */, IDictionary_2_t1_1899_il2cpp_TypeInfo_var, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void AlertControl/<CheckInServer>c__Iterator0::.ctor()
extern "C" void U3CCheckInServerU3Ec__Iterator0__ctor_m8_67 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AlertControl/<CheckInServer>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCheckInServerU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_68 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Object AlertControl/<CheckInServer>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCheckInServerU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8_69 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Boolean AlertControl/<CheckInServer>c__Iterator0::MoveNext()
extern TypeInfo* WWWForm_t6_79_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t6_10_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5123;
extern Il2CppCodeGenString* _stringLiteral5124;
extern Il2CppCodeGenString* _stringLiteral5125;
extern Il2CppCodeGenString* _stringLiteral5126;
extern Il2CppCodeGenString* _stringLiteral5127;
extern Il2CppCodeGenString* _stringLiteral5128;
extern Il2CppCodeGenString* _stringLiteral4199;
extern Il2CppCodeGenString* _stringLiteral5129;
extern "C" bool U3CCheckInServerU3Ec__Iterator0_MoveNext_m8_70 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t6_79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1943);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		WaitForSeconds_t6_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1808);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		_stringLiteral5123 = il2cpp_codegen_string_literal_from_index(5123);
		_stringLiteral5124 = il2cpp_codegen_string_literal_from_index(5124);
		_stringLiteral5125 = il2cpp_codegen_string_literal_from_index(5125);
		_stringLiteral5126 = il2cpp_codegen_string_literal_from_index(5126);
		_stringLiteral5127 = il2cpp_codegen_string_literal_from_index(5127);
		_stringLiteral5128 = il2cpp_codegen_string_literal_from_index(5128);
		_stringLiteral4199 = il2cpp_codegen_string_literal_from_index(4199);
		_stringLiteral5129 = il2cpp_codegen_string_literal_from_index(5129);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	double V_1 = 0.0;
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_7);
		V_0 = L_0;
		__this->___U24PC_7 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00c3;
		}
	}
	{
		goto IL_01e7;
	}

IL_0021:
	{
		WWWForm_t6_79 * L_2 = (WWWForm_t6_79 *)il2cpp_codegen_object_new (WWWForm_t6_79_il2cpp_TypeInfo_var);
		WWWForm__ctor_m6_560(L_2, /*hidden argument*/NULL);
		__this->___U3C_fU3E__0_0 = L_2;
		WWWForm_t6_79 * L_3 = (__this->___U3C_fU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5123, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m6_561(L_3, _stringLiteral5123, L_4, /*hidden argument*/NULL);
		AlertControl_t8_9 * L_5 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_5);
		String_t* L_6 = (L_5->___url_dev_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5124, L_6, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_8 = (__this->___U3C_fU3E__0_0);
		WWW_t6_78 * L_9 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_542(L_9, L_7, L_8, /*hidden argument*/NULL);
		__this->___U3C_wU3E__1_1 = L_9;
		__this->___U3C_elapseU3E__2_2 = (0.0f);
		float L_10 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3C_timeU3E__3_3 = L_10;
		__this->___U3C_timeoutU3E__4_4 = (15.0f);
		goto IL_00e7;
	}

IL_0092:
	{
		WWW_t6_78 * L_11 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_11);
		bool L_12 = WWW_get_isDone_m6_557(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_00f8;
	}

IL_00a7:
	{
		WaitForSeconds_t6_10 * L_13 = (WaitForSeconds_t6_10 *)il2cpp_codegen_object_new (WaitForSeconds_t6_10_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m6_10(L_13, (0.1f), /*hidden argument*/NULL);
		__this->___U24current_8 = L_13;
		__this->___U24PC_7 = 1;
		goto IL_01e9;
	}

IL_00c3:
	{
		float L_14 = (__this->___U3C_elapseU3E__2_2);
		float L_15 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = (__this->___U3C_timeU3E__3_3);
		__this->___U3C_elapseU3E__2_2 = ((float)((float)L_14+(float)((float)((float)L_15-(float)L_16))));
		float L_17 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3C_timeU3E__3_3 = L_17;
	}

IL_00e7:
	{
		float L_18 = (__this->___U3C_elapseU3E__2_2);
		float L_19 = (__this->___U3C_timeoutU3E__4_4);
		if ((((float)L_18) < ((float)L_19)))
		{
			goto IL_0092;
		}
	}

IL_00f8:
	{
		WWW_t6_78 * L_20 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_20);
		bool L_21 = WWW_get_isDone_m6_557(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0117;
		}
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5125, /*hidden argument*/NULL);
		goto IL_01e7;
	}

IL_0117:
	{
		WWW_t6_78 * L_22 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_22);
		String_t* L_23 = WWW_get_error_m6_554(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0141;
		}
	}
	{
		WWW_t6_78 * L_25 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_25);
		String_t* L_26 = WWW_get_text_m6_550(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0160;
		}
	}

IL_0141:
	{
		WWW_t6_78 * L_28 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_28);
		String_t* L_29 = WWW_get_error_m6_554(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5126, L_29, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		goto IL_01e7;
	}

IL_0160:
	{
		WWW_t6_78 * L_31 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_31);
		String_t* L_32 = WWW_get_text_m6_550(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5127, L_32, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		WWW_t6_78 * L_34 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_34);
		String_t* L_35 = WWW_get_text_m6_550(L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_36 = JSONObject_Parse_m8_57(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		__this->___U3C_jsonU3E__5_5 = L_36;
		JSONObject_t8_5 * L_37 = (__this->___U3C_jsonU3E__5_5);
		NullCheck(L_37);
		double L_38 = JSONObject_GetNumber_m8_49(L_37, _stringLiteral5128, /*hidden argument*/NULL);
		V_1 = L_38;
		bool L_39 = Double_Equals_m1_656((&V_1), (0.0), /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_01e0;
		}
	}
	{
		JSONObject_t8_5 * L_40 = (__this->___U3C_jsonU3E__5_5);
		NullCheck(L_40);
		bool L_41 = JSONObject_GetBoolean_m8_51(L_40, _stringLiteral4199, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_01e0;
		}
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5129, /*hidden argument*/NULL);
		Action_t5_11 * L_42 = (__this->____action_6);
		NullCheck(L_42);
		Action_Invoke_m5_51(L_42, /*hidden argument*/NULL);
	}

IL_01e0:
	{
		__this->___U24PC_7 = (-1);
	}

IL_01e7:
	{
		return 0;
	}

IL_01e9:
	{
		return 1;
	}
	// Dead block : IL_01eb: ldloc.2
}
// System.Void AlertControl/<CheckInServer>c__Iterator0::Dispose()
extern "C" void U3CCheckInServerU3Ec__Iterator0_Dispose_m8_71 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_7 = (-1);
		return;
	}
}
// System.Void AlertControl/<CheckInServer>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CCheckInServerU3Ec__Iterator0_Reset_m8_72 (U3CCheckInServerU3Ec__Iterator0_t8_8 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void AlertControl::.ctor()
extern Il2CppCodeGenString* _stringLiteral5099;
extern "C" void AlertControl__ctor_m8_73 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5099 = il2cpp_codegen_string_literal_from_index(5099);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___url_dev_17 = _stringLiteral5099;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::Update()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1003;
extern Il2CppCodeGenString* _stringLiteral5100;
extern "C" void AlertControl_Update_m8_74 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral1003 = il2cpp_codegen_string_literal_from_index(1003);
		_stringLiteral5100 = il2cpp_codegen_string_literal_from_index(5100);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		Text_t7_63 * L_0 = (__this->___currentTime_14);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = DateTime_ToString_m1_13898((&V_0), _stringLiteral1003, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		Text_t7_63 * L_3 = (__this->___currentDate_15);
		DateTime_t1_150  L_4 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = DateTime_ToString_m1_13898((&V_1), _stringLiteral5100, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_5);
		AlertControl_UpdateTimeDisplay_m8_76(__this, /*hidden argument*/NULL);
		AlertControl_ShowButton_m8_77(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::CheckIn()
extern TypeInfo* Action_t5_11_il2cpp_TypeInfo_var;
extern const MethodInfo* AlertControl_U3CCheckInU3Em__0_m8_85_MethodInfo_var;
extern "C" void AlertControl_CheckIn_m8_75 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t5_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1945);
		AlertControl_U3CCheckInU3Em__0_m8_85_MethodInfo_var = il2cpp_codegen_method_info_from_index(465);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = { (void*)AlertControl_U3CCheckInU3Em__0_m8_85_MethodInfo_var };
		Action_t5_11 * L_1 = (Action_t5_11 *)il2cpp_codegen_object_new (Action_t5_11_il2cpp_TypeInfo_var);
		Action__ctor_m5_50(L_1, __this, L_0, /*hidden argument*/NULL);
		Object_t * L_2 = AlertControl_CheckInServer_m8_84(__this, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::UpdateTimeDisplay()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral5102;
extern Il2CppCodeGenString* _stringLiteral5103;
extern Il2CppCodeGenString* _stringLiteral4252;
extern Il2CppCodeGenString* _stringLiteral3496;
extern Il2CppCodeGenString* _stringLiteral5100;
extern Il2CppCodeGenString* _stringLiteral5104;
extern "C" void AlertControl_UpdateTimeDisplay_m8_76 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		_stringLiteral5103 = il2cpp_codegen_string_literal_from_index(5103);
		_stringLiteral4252 = il2cpp_codegen_string_literal_from_index(4252);
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		_stringLiteral5100 = il2cpp_codegen_string_literal_from_index(5100);
		_stringLiteral5104 = il2cpp_codegen_string_literal_from_index(5104);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	TimeSpan_t1_368  V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	DateTime_t1_150  V_4 = {0};
	DateTime_t1_150  V_5 = {0};
	DateTime_t1_150  V_6 = {0};
	DateTime_t1_150  V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	double V_12 = 0.0;
	{
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___amTimerStart_2 = L_1;
		DateTime_t1_150  L_2 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = DateTime_get_Year_m1_13830((&V_2), /*hidden argument*/NULL);
		DateTime_t1_150  L_4 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_4;
		int32_t L_5 = DateTime_get_Month_m1_13815((&V_3), /*hidden argument*/NULL);
		DateTime_t1_150  L_6 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_6;
		int32_t L_7 = DateTime_get_Day_m1_13816((&V_4), /*hidden argument*/NULL);
		DateTime_t1_150 * L_8 = &(__this->___amTimerStart_2);
		int32_t L_9 = DateTime_get_Hour_m1_13820(L_8, /*hidden argument*/NULL);
		DateTime_t1_150 * L_10 = &(__this->___amTimerStart_2);
		int32_t L_11 = DateTime_get_Minute_m1_13821(L_10, /*hidden argument*/NULL);
		DateTime_t1_150 * L_12 = &(__this->___amTimerStart_2);
		int32_t L_13 = DateTime_get_Second_m1_13822(L_12, /*hidden argument*/NULL);
		DateTime_t1_150  L_14 = {0};
		DateTime__ctor_m1_13786(&L_14, L_3, L_5, L_7, L_9, L_11, L_13, /*hidden argument*/NULL);
		__this->___amTimerStart_2 = L_14;
		DateTime_t1_150 * L_15 = &(__this->___amTimerStart_2);
		DateTime_t1_150  L_16 = DateTime_AddMinutes_m1_13838(L_15, (150.0), /*hidden argument*/NULL);
		__this->___amTimerEnd_3 = L_16;
		String_t* L_17 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		DateTime_t1_150  L_18 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		__this->___pmTimerStart_4 = L_18;
		DateTime_t1_150  L_19 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_19;
		int32_t L_20 = DateTime_get_Year_m1_13830((&V_5), /*hidden argument*/NULL);
		DateTime_t1_150  L_21 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_21;
		int32_t L_22 = DateTime_get_Month_m1_13815((&V_6), /*hidden argument*/NULL);
		DateTime_t1_150  L_23 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_23;
		int32_t L_24 = DateTime_get_Day_m1_13816((&V_7), /*hidden argument*/NULL);
		DateTime_t1_150 * L_25 = &(__this->___pmTimerStart_4);
		int32_t L_26 = DateTime_get_Hour_m1_13820(L_25, /*hidden argument*/NULL);
		DateTime_t1_150 * L_27 = &(__this->___pmTimerStart_4);
		int32_t L_28 = DateTime_get_Minute_m1_13821(L_27, /*hidden argument*/NULL);
		DateTime_t1_150 * L_29 = &(__this->___pmTimerStart_4);
		int32_t L_30 = DateTime_get_Second_m1_13822(L_29, /*hidden argument*/NULL);
		DateTime_t1_150  L_31 = {0};
		DateTime__ctor_m1_13786(&L_31, L_20, L_22, L_24, L_26, L_28, L_30, /*hidden argument*/NULL);
		__this->___pmTimerStart_4 = L_31;
		DateTime_t1_150 * L_32 = &(__this->___pmTimerStart_4);
		DateTime_t1_150  L_33 = DateTime_AddMinutes_m1_13838(L_32, (150.0), /*hidden argument*/NULL);
		__this->___pmTimerEnd_5 = L_33;
		String_t* L_34 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5103, /*hidden argument*/NULL);
		DateTime_t1_150  L_35 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		__this->___lastCheckTime_6 = L_35;
		Text_t7_63 * L_36 = (__this->___lastCheck_10);
		DateTime_t1_150 * L_37 = &(__this->___lastCheckTime_6);
		int32_t L_38 = DateTime_get_Hour_m1_13820(L_37, /*hidden argument*/NULL);
		V_8 = L_38;
		String_t* L_39 = Int32_ToString_m1_103((&V_8), _stringLiteral3496, /*hidden argument*/NULL);
		DateTime_t1_150 * L_40 = &(__this->___lastCheckTime_6);
		int32_t L_41 = DateTime_get_Minute_m1_13821(L_40, /*hidden argument*/NULL);
		V_9 = L_41;
		String_t* L_42 = Int32_ToString_m1_103((&V_9), _stringLiteral3496, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral4252, L_39, L_42, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_36, L_43);
		Text_t7_63 * L_44 = (__this->___lastCheckDate_11);
		DateTime_t1_150 * L_45 = &(__this->___lastCheckTime_6);
		String_t* L_46 = DateTime_ToString_m1_13898(L_45, _stringLiteral5100, /*hidden argument*/NULL);
		NullCheck(L_44);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_44, L_46);
		DateTime_t1_150  L_47 = (__this->___amTimerStart_2);
		DateTime_t1_150  L_48 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_49 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_01c4;
		}
	}
	{
		DateTime_t1_150  L_50 = (__this->___amTimerStart_2);
		DateTime_t1_150  L_51 = (__this->___lastCheckTime_6);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_52 = DateTime_op_Subtraction_m1_13909(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		V_0 = L_52;
		DateTime_t1_150  L_53 = (__this->___amTimerStart_2);
		__this->___nextCheckTime_7 = L_53;
		DateTime_t1_150  L_54 = (__this->___amTimerEnd_3);
		__this->___nextEndTime_8 = L_54;
		goto IL_01ee;
	}

IL_01c4:
	{
		DateTime_t1_150  L_55 = (__this->___pmTimerStart_4);
		DateTime_t1_150  L_56 = (__this->___lastCheckTime_6);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_57 = DateTime_op_Subtraction_m1_13909(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		V_0 = L_57;
		DateTime_t1_150  L_58 = (__this->___pmTimerStart_4);
		__this->___nextCheckTime_7 = L_58;
		DateTime_t1_150  L_59 = (__this->___pmTimerEnd_5);
		__this->___nextEndTime_8 = L_59;
	}

IL_01ee:
	{
		Text_t7_63 * L_60 = (__this->___nextAlarmTime_12);
		DateTime_t1_150 * L_61 = &(__this->___nextCheckTime_7);
		int32_t L_62 = DateTime_get_Hour_m1_13820(L_61, /*hidden argument*/NULL);
		V_10 = L_62;
		String_t* L_63 = Int32_ToString_m1_103((&V_10), _stringLiteral3496, /*hidden argument*/NULL);
		DateTime_t1_150 * L_64 = &(__this->___nextCheckTime_7);
		int32_t L_65 = DateTime_get_Minute_m1_13821(L_64, /*hidden argument*/NULL);
		V_11 = L_65;
		String_t* L_66 = Int32_ToString_m1_103((&V_11), _stringLiteral3496, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_67 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral4252, L_63, L_66, /*hidden argument*/NULL);
		NullCheck(L_60);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_60, L_67);
		Text_t7_63 * L_68 = (__this->___nextAlarmDate_13);
		DateTime_t1_150 * L_69 = &(__this->___nextCheckTime_7);
		String_t* L_70 = DateTime_ToString_m1_13898(L_69, _stringLiteral5100, /*hidden argument*/NULL);
		NullCheck(L_68);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_68, L_70);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_71 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t1_150  L_72 = (__this->___lastCheckTime_6);
		TimeSpan_t1_368  L_73 = DateTime_op_Subtraction_m1_13909(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		V_1 = L_73;
		Image_t7_64 * L_74 = (__this->___timeFiller_9);
		double L_75 = TimeSpan_get_TotalSeconds_m1_14634((&V_1), /*hidden argument*/NULL);
		double L_76 = TimeSpan_get_TotalSeconds_m1_14634((&V_0), /*hidden argument*/NULL);
		V_12 = ((double)((double)L_75/(double)L_76));
		String_t* L_77 = Double_ToString_m1_673((&V_12), _stringLiteral5104, /*hidden argument*/NULL);
		float L_78 = Single_Parse_m1_627(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		NullCheck(L_74);
		Image_set_fillAmount_m7_570(L_74, L_78, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::ShowButton()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" void AlertControl_ShowButton_m8_77 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_150  L_0 = (__this->___nextCheckTime_7);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t6_97 * L_3 = (__this->___button_checkin_16);
		NullCheck(L_3);
		GameObject_SetActive_m6_742(L_3, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		DateTime_t1_150  L_4 = (__this->___nextEndTime_8);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_5 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		GameObject_t6_97 * L_7 = (__this->___button_checkin_16);
		NullCheck(L_7);
		GameObject_SetActive_m6_742(L_7, 0, /*hidden argument*/NULL);
		return;
	}

IL_0046:
	{
		GameObject_t6_97 * L_8 = (__this->___button_checkin_16);
		NullCheck(L_8);
		GameObject_SetActive_m6_742(L_8, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::RenewAMTimer()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5105;
extern Il2CppCodeGenString* _stringLiteral265;
extern Il2CppCodeGenString* _stringLiteral5106;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral5107;
extern Il2CppCodeGenString* _stringLiteral5108;
extern "C" void AlertControl_RenewAMTimer_m8_78 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5105 = il2cpp_codegen_string_literal_from_index(5105);
		_stringLiteral265 = il2cpp_codegen_string_literal_from_index(265);
		_stringLiteral5106 = il2cpp_codegen_string_literal_from_index(5106);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral5107 = il2cpp_codegen_string_literal_from_index(5107);
		_stringLiteral5108 = il2cpp_codegen_string_literal_from_index(5108);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	String_t* V_2 = {0};
	StringU5BU5D_t1_238* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	DateTime_t1_150  V_6 = {0};
	DateTime_t1_150  V_7 = {0};
	DateTime_t1_150  V_8 = {0};
	DateTime_t1_150  V_9 = {0};
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	{
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5105, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		CharU5BU5D_t1_16* L_2 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(_stringLiteral265);
		uint16_t L_3 = String_get_Chars_m1_442(_stringLiteral265, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0, sizeof(uint16_t))) = (uint16_t)L_3;
		NullCheck(L_1);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t1_238* L_5 = V_1;
		V_3 = L_5;
		V_4 = 0;
		goto IL_0042;
	}

IL_0030:
	{
		StringU5BU5D_t1_238* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_8, sizeof(String_t*)));
		String_t* L_9 = V_2;
		AlertControl_CancelLocalNotification_m8_80(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_4;
		V_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_11 = V_4;
		StringU5BU5D_t1_238* L_12 = V_3;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_13 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_14 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = DateTime_get_Month_m1_13815((&V_7), /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 0, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t1_272* L_18 = L_13;
		DateTime_t1_150  L_19 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_19;
		int32_t L_20 = DateTime_get_Day_m1_13816((&V_8), /*hidden argument*/NULL);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 1, sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t1_272* L_23 = L_18;
		DateTime_t1_150  L_24 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_24;
		int32_t L_25 = DateTime_get_Year_m1_13830((&V_9), /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		Object_t * L_27 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 2, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_272* L_28 = L_23;
		DateTime_t1_150 * L_29 = &(__this->___amTimerStart_2);
		int32_t L_30 = DateTime_get_Hour_m1_13820(L_29, /*hidden argument*/NULL);
		V_10 = L_30;
		String_t* L_31 = Int32_ToString_m1_101((&V_10), /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 3, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t1_272* L_32 = L_28;
		DateTime_t1_150 * L_33 = &(__this->___amTimerStart_2);
		int32_t L_34 = DateTime_get_Minute_m1_13821(L_33, /*hidden argument*/NULL);
		V_11 = L_34;
		String_t* L_35 = Int32_ToString_m1_101((&V_11), /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 4);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 4, sizeof(Object_t *))) = (Object_t *)L_35;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5106, L_32, /*hidden argument*/NULL);
		V_5 = L_36;
		String_t* L_37 = V_5;
		DateTime_t1_150  L_38 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		DateTime_AddDays_m1_13833((&V_6), (1.0), /*hidden argument*/NULL);
		String_t* L_39 = DateTime_ToString_m1_13896((&V_6), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5101, L_39, /*hidden argument*/NULL);
		DateTime_t1_150  L_40 = V_6;
		DateTime_t1_150  L_41 = L_40;
		Object_t * L_42 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_41);
		String_t* L_43 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5107, L_42, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		String_t* L_44 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_44;
		String_t* L_45 = V_0;
		DateTime_t1_150  L_46 = V_6;
		CrossPlatformNotification_t8_259 * L_47 = AlertControl_CreateNotification_m8_82(__this, L_46, 3, /*hidden argument*/NULL);
		String_t* L_48 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_47, /*hidden argument*/NULL);
		String_t* L_49 = String_Concat_m1_559(NULL /*static, unused*/, L_45, L_48, /*hidden argument*/NULL);
		V_0 = L_49;
		String_t* L_50 = V_0;
		DateTime_t1_150  L_51 = DateTime_AddMinutes_m1_13838((&V_6), (15.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_52 = AlertControl_CreateNotification_m8_82(__this, L_51, 3, /*hidden argument*/NULL);
		String_t* L_53 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_52, /*hidden argument*/NULL);
		String_t* L_54 = String_Concat_m1_560(NULL /*static, unused*/, L_50, _stringLiteral265, L_53, /*hidden argument*/NULL);
		V_0 = L_54;
		String_t* L_55 = V_0;
		DateTime_t1_150  L_56 = DateTime_AddMinutes_m1_13838((&V_6), (30.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_57 = AlertControl_CreateNotification_m8_82(__this, L_56, 3, /*hidden argument*/NULL);
		String_t* L_58 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_57, /*hidden argument*/NULL);
		String_t* L_59 = String_Concat_m1_560(NULL /*static, unused*/, L_55, _stringLiteral265, L_58, /*hidden argument*/NULL);
		V_0 = L_59;
		String_t* L_60 = V_0;
		DateTime_t1_150  L_61 = DateTime_AddMinutes_m1_13838((&V_6), (45.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_62 = AlertControl_CreateNotification_m8_82(__this, L_61, 3, /*hidden argument*/NULL);
		String_t* L_63 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_62, /*hidden argument*/NULL);
		String_t* L_64 = String_Concat_m1_560(NULL /*static, unused*/, L_60, _stringLiteral265, L_63, /*hidden argument*/NULL);
		V_0 = L_64;
		String_t* L_65 = V_0;
		DateTime_t1_150  L_66 = DateTime_AddMinutes_m1_13838((&V_6), (60.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_67 = AlertControl_CreateNotification_m8_82(__this, L_66, 3, /*hidden argument*/NULL);
		String_t* L_68 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_67, /*hidden argument*/NULL);
		String_t* L_69 = String_Concat_m1_560(NULL /*static, unused*/, L_65, _stringLiteral265, L_68, /*hidden argument*/NULL);
		V_0 = L_69;
		String_t* L_70 = V_0;
		String_t* L_71 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5108, L_70, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		String_t* L_72 = V_0;
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5105, L_72, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::RenewPMTimer()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5109;
extern Il2CppCodeGenString* _stringLiteral265;
extern Il2CppCodeGenString* _stringLiteral5110;
extern Il2CppCodeGenString* _stringLiteral5111;
extern Il2CppCodeGenString* _stringLiteral5112;
extern Il2CppCodeGenString* _stringLiteral5102;
extern Il2CppCodeGenString* _stringLiteral5113;
extern "C" void AlertControl_RenewPMTimer_m8_79 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5109 = il2cpp_codegen_string_literal_from_index(5109);
		_stringLiteral265 = il2cpp_codegen_string_literal_from_index(265);
		_stringLiteral5110 = il2cpp_codegen_string_literal_from_index(5110);
		_stringLiteral5111 = il2cpp_codegen_string_literal_from_index(5111);
		_stringLiteral5112 = il2cpp_codegen_string_literal_from_index(5112);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		_stringLiteral5113 = il2cpp_codegen_string_literal_from_index(5113);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	String_t* V_2 = {0};
	StringU5BU5D_t1_238* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	DateTime_t1_150  V_6 = {0};
	DateTime_t1_150  V_7 = {0};
	DateTime_t1_150  V_8 = {0};
	DateTime_t1_150  V_9 = {0};
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	{
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5109, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		CharU5BU5D_t1_16* L_2 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(_stringLiteral265);
		uint16_t L_3 = String_get_Chars_m1_442(_stringLiteral265, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0, sizeof(uint16_t))) = (uint16_t)L_3;
		NullCheck(L_1);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t1_238* L_5 = V_1;
		V_3 = L_5;
		V_4 = 0;
		goto IL_0042;
	}

IL_0030:
	{
		StringU5BU5D_t1_238* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_8, sizeof(String_t*)));
		String_t* L_9 = V_2;
		AlertControl_CancelLocalNotification_m8_80(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_4;
		V_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_11 = V_4;
		StringU5BU5D_t1_238* L_12 = V_3;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_13 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_14 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = DateTime_get_Month_m1_13815((&V_7), /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Object_t * L_17 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 0, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t1_272* L_18 = L_13;
		DateTime_t1_150  L_19 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_19;
		int32_t L_20 = DateTime_get_Day_m1_13816((&V_8), /*hidden argument*/NULL);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		ArrayElementTypeCheck (L_18, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 1, sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t1_272* L_23 = L_18;
		DateTime_t1_150  L_24 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_24;
		int32_t L_25 = DateTime_get_Year_m1_13830((&V_9), /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		Object_t * L_27 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 2, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_272* L_28 = L_23;
		DateTime_t1_150 * L_29 = &(__this->___pmTimerStart_4);
		int32_t L_30 = DateTime_get_Hour_m1_13820(L_29, /*hidden argument*/NULL);
		V_10 = L_30;
		String_t* L_31 = Int32_ToString_m1_101((&V_10), /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 3, sizeof(Object_t *))) = (Object_t *)L_31;
		ObjectU5BU5D_t1_272* L_32 = L_28;
		DateTime_t1_150 * L_33 = &(__this->___pmTimerStart_4);
		int32_t L_34 = DateTime_get_Minute_m1_13821(L_33, /*hidden argument*/NULL);
		V_11 = L_34;
		String_t* L_35 = Int32_ToString_m1_101((&V_11), /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 4);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 4, sizeof(Object_t *))) = (Object_t *)L_35;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5110, L_32, /*hidden argument*/NULL);
		V_5 = L_36;
		String_t* L_37 = V_5;
		DateTime_t1_150  L_38 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		DateTime_t1_150  L_39 = V_6;
		DateTime_t1_150  L_40 = L_39;
		Object_t * L_41 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_40);
		String_t* L_42 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5111, L_41, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		DateTime_AddDays_m1_13833((&V_6), (1.0), /*hidden argument*/NULL);
		DateTime_t1_150  L_43 = V_6;
		DateTime_t1_150  L_44 = L_43;
		Object_t * L_45 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_44);
		String_t* L_46 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5112, L_45, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		String_t* L_47 = DateTime_ToString_m1_13896((&V_6), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5102, L_47, /*hidden argument*/NULL);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_48;
		String_t* L_49 = V_0;
		DateTime_t1_150  L_50 = V_6;
		CrossPlatformNotification_t8_259 * L_51 = AlertControl_CreateNotification_m8_82(__this, L_50, 3, /*hidden argument*/NULL);
		String_t* L_52 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_51, /*hidden argument*/NULL);
		String_t* L_53 = String_Concat_m1_559(NULL /*static, unused*/, L_49, L_52, /*hidden argument*/NULL);
		V_0 = L_53;
		String_t* L_54 = V_0;
		DateTime_t1_150  L_55 = DateTime_AddMinutes_m1_13838((&V_6), (15.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_56 = AlertControl_CreateNotification_m8_82(__this, L_55, 3, /*hidden argument*/NULL);
		String_t* L_57 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_56, /*hidden argument*/NULL);
		String_t* L_58 = String_Concat_m1_560(NULL /*static, unused*/, L_54, _stringLiteral265, L_57, /*hidden argument*/NULL);
		V_0 = L_58;
		String_t* L_59 = V_0;
		DateTime_t1_150  L_60 = DateTime_AddMinutes_m1_13838((&V_6), (30.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_61 = AlertControl_CreateNotification_m8_82(__this, L_60, 3, /*hidden argument*/NULL);
		String_t* L_62 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_61, /*hidden argument*/NULL);
		String_t* L_63 = String_Concat_m1_560(NULL /*static, unused*/, L_59, _stringLiteral265, L_62, /*hidden argument*/NULL);
		V_0 = L_63;
		String_t* L_64 = V_0;
		DateTime_t1_150  L_65 = DateTime_AddMinutes_m1_13838((&V_6), (45.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_66 = AlertControl_CreateNotification_m8_82(__this, L_65, 3, /*hidden argument*/NULL);
		String_t* L_67 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_66, /*hidden argument*/NULL);
		String_t* L_68 = String_Concat_m1_560(NULL /*static, unused*/, L_64, _stringLiteral265, L_67, /*hidden argument*/NULL);
		V_0 = L_68;
		String_t* L_69 = V_0;
		DateTime_t1_150  L_70 = DateTime_AddMinutes_m1_13838((&V_6), (60.0), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_71 = AlertControl_CreateNotification_m8_82(__this, L_70, 3, /*hidden argument*/NULL);
		String_t* L_72 = AlertControl_ScheduleLocalNotification_m8_81(__this, L_71, /*hidden argument*/NULL);
		String_t* L_73 = String_Concat_m1_560(NULL /*static, unused*/, L_69, _stringLiteral265, L_72, /*hidden argument*/NULL);
		V_0 = L_73;
		String_t* L_74 = V_0;
		String_t* L_75 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5113, L_74, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		String_t* L_76 = V_0;
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5109, L_76, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlertControl::CancelLocalNotification(System.String)
extern "C" void AlertControl_CancelLocalNotification_m8_80 (AlertControl_t8_9 * __this, String_t* ____notificationID, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ____notificationID;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void VoxelBusters.NativePlugins.NotificationService::CancelLocalNotification(System.String) */, L_0, L_1);
		return;
	}
}
// System.String AlertControl::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* AlertControl_ScheduleLocalNotification_m8_81 (AlertControl_t8_9 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_1 = ____notification;
		NullCheck(L_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker1< String_t*, CrossPlatformNotification_t8_259 * >::Invoke(8 /* System.String VoxelBusters.NativePlugins.NotificationService::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification) */, L_0, L_1);
		return L_2;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification AlertControl::CreateNotification(System.DateTime,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral424;
extern Il2CppCodeGenString* _stringLiteral5114;
extern Il2CppCodeGenString* _stringLiteral5115;
extern Il2CppCodeGenString* _stringLiteral5116;
extern Il2CppCodeGenString* _stringLiteral5117;
extern Il2CppCodeGenString* _stringLiteral5118;
extern Il2CppCodeGenString* _stringLiteral5119;
extern Il2CppCodeGenString* _stringLiteral5120;
extern Il2CppCodeGenString* _stringLiteral5121;
extern "C" CrossPlatformNotification_t8_259 * AlertControl_CreateNotification_m8_82 (AlertControl_t8_9 * __this, DateTime_t1_150  ____time, int32_t ____repeatInterval, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1946);
		AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1947);
		CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1948);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral424 = il2cpp_codegen_string_literal_from_index(424);
		_stringLiteral5114 = il2cpp_codegen_string_literal_from_index(5114);
		_stringLiteral5115 = il2cpp_codegen_string_literal_from_index(5115);
		_stringLiteral5116 = il2cpp_codegen_string_literal_from_index(5116);
		_stringLiteral5117 = il2cpp_codegen_string_literal_from_index(5117);
		_stringLiteral5118 = il2cpp_codegen_string_literal_from_index(5118);
		_stringLiteral5119 = il2cpp_codegen_string_literal_from_index(5119);
		_stringLiteral5120 = il2cpp_codegen_string_literal_from_index(5120);
		_stringLiteral5121 = il2cpp_codegen_string_literal_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	iOSSpecificProperties_t8_266 * V_1 = {0};
	AndroidSpecificProperties_t8_265 * V_2 = {0};
	CrossPlatformNotification_t8_259 * V_3 = {0};
	{
		Dictionary_2_t1_1839 * L_0 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		NullCheck(L_1);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral424, _stringLiteral5114);
		iOSSpecificProperties_t8_266 * L_2 = (iOSSpecificProperties_t8_266 *)il2cpp_codegen_object_new (iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var);
		iOSSpecificProperties__ctor_m8_1502(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		iOSSpecificProperties_t8_266 * L_3 = V_1;
		NullCheck(L_3);
		iOSSpecificProperties_set_HasAction_m8_1507(L_3, 1, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_4 = V_1;
		NullCheck(L_4);
		iOSSpecificProperties_set_AlertAction_m8_1505(L_4, _stringLiteral5115, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_5 = (AndroidSpecificProperties_t8_265 *)il2cpp_codegen_object_new (AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var);
		AndroidSpecificProperties__ctor_m8_1488(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		AndroidSpecificProperties_t8_265 * L_6 = V_2;
		NullCheck(L_6);
		AndroidSpecificProperties_set_ContentTitle_m8_1491(L_6, _stringLiteral5116, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_7 = V_2;
		NullCheck(L_7);
		AndroidSpecificProperties_set_TickerText_m8_1493(L_7, _stringLiteral5117, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_8 = V_2;
		NullCheck(L_8);
		AndroidSpecificProperties_set_LargeIcon_m8_1499(L_8, _stringLiteral5118, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_9 = (CrossPlatformNotification_t8_259 *)il2cpp_codegen_object_new (CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var);
		CrossPlatformNotification__ctor_m8_1514(L_9, /*hidden argument*/NULL);
		V_3 = L_9;
		CrossPlatformNotification_t8_259 * L_10 = V_3;
		NullCheck(L_10);
		CrossPlatformNotification_set_AlertBody_m8_1517(L_10, _stringLiteral5119, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_11 = V_3;
		DateTime_t1_150  L_12 = ____time;
		NullCheck(L_11);
		CrossPlatformNotification_set_FireDate_m8_1519(L_11, L_12, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_13 = V_3;
		int32_t L_14 = ____repeatInterval;
		NullCheck(L_13);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(L_13, L_14, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_15 = V_3;
		NullCheck(L_15);
		CrossPlatformNotification_set_SoundName_m8_1525(L_15, _stringLiteral5120, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_16 = V_3;
		Object_t * L_17 = V_0;
		NullCheck(L_16);
		CrossPlatformNotification_set_UserInfo_m8_1523(L_16, L_17, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_18 = V_3;
		iOSSpecificProperties_t8_266 * L_19 = V_1;
		NullCheck(L_18);
		CrossPlatformNotification_set_iOSProperties_m8_1527(L_18, L_19, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_20 = V_3;
		AndroidSpecificProperties_t8_265 * L_21 = V_2;
		NullCheck(L_20);
		CrossPlatformNotification_set_AndroidProperties_m8_1529(L_20, L_21, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_22 = V_3;
		NullCheck(L_22);
		String_t* L_23 = CrossPlatformNotification_GetNotificationID_m8_1530(L_22, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_24 = V_3;
		NullCheck(L_24);
		DateTime_t1_150  L_25 = CrossPlatformNotification_get_FireDate_m8_1518(L_24, /*hidden argument*/NULL);
		DateTime_t1_150  L_26 = L_25;
		Object_t * L_27 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5121, L_23, L_27, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_29 = V_3;
		return L_29;
	}
}
// System.Void AlertControl::GoToSetting()
extern Il2CppCodeGenString* _stringLiteral5122;
extern "C" void AlertControl_GoToSetting_m8_83 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5122 = il2cpp_codegen_string_literal_from_index(5122);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_LoadLevel_m6_583(NULL /*static, unused*/, _stringLiteral5122, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AlertControl::CheckInServer(System.Action)
extern TypeInfo* U3CCheckInServerU3Ec__Iterator0_t8_8_il2cpp_TypeInfo_var;
extern "C" Object_t * AlertControl_CheckInServer_m8_84 (AlertControl_t8_9 * __this, Action_t5_11 * ____action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCheckInServerU3Ec__Iterator0_t8_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1949);
		s_Il2CppMethodIntialized = true;
	}
	U3CCheckInServerU3Ec__Iterator0_t8_8 * V_0 = {0};
	{
		U3CCheckInServerU3Ec__Iterator0_t8_8 * L_0 = (U3CCheckInServerU3Ec__Iterator0_t8_8 *)il2cpp_codegen_object_new (U3CCheckInServerU3Ec__Iterator0_t8_8_il2cpp_TypeInfo_var);
		U3CCheckInServerU3Ec__Iterator0__ctor_m8_67(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCheckInServerU3Ec__Iterator0_t8_8 * L_1 = V_0;
		Action_t5_11 * L_2 = ____action;
		NullCheck(L_1);
		L_1->____action_6 = L_2;
		U3CCheckInServerU3Ec__Iterator0_t8_8 * L_3 = V_0;
		Action_t5_11 * L_4 = ____action;
		NullCheck(L_3);
		L_3->___U3CU24U3E_action_9 = L_4;
		U3CCheckInServerU3Ec__Iterator0_t8_8 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_10 = __this;
		U3CCheckInServerU3Ec__Iterator0_t8_8 * L_6 = V_0;
		return L_6;
	}
}
// System.Void AlertControl::<CheckIn>m__0()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5103;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral5102;
extern "C" void AlertControl_U3CCheckInU3Em__0_m8_85 (AlertControl_t8_9 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5103 = il2cpp_codegen_string_literal_from_index(5103);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_0 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = DateTime_ToString_m1_13896((&V_0), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5103, L_1, /*hidden argument*/NULL);
		DateTime_t1_150 * L_2 = &(__this->___nextCheckTime_7);
		String_t* L_3 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		DateTime_t1_150  L_4 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		bool L_5 = DateTime_Equals_m1_13846(L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		AlertControl_RenewAMTimer_m8_78(__this, /*hidden argument*/NULL);
		String_t* L_6 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_7 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->___nextCheckTime_7 = L_7;
	}

IL_0051:
	{
		DateTime_t1_150 * L_8 = &(__this->___nextCheckTime_7);
		String_t* L_9 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_10 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		bool L_11 = DateTime_Equals_m1_13846(L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008b;
		}
	}
	{
		AlertControl_RenewPMTimer_m8_79(__this, /*hidden argument*/NULL);
		String_t* L_12 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_13 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->___nextCheckTime_7 = L_13;
	}

IL_008b:
	{
		return;
	}
}
// System.Void LoginControl/<LoginRoute>c__Iterator1::.ctor()
extern "C" void U3CLoginRouteU3Ec__Iterator1__ctor_m8_86 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LoginControl/<LoginRoute>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLoginRouteU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_87 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_9);
		return L_0;
	}
}
// System.Object LoginControl/<LoginRoute>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLoginRouteU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m8_88 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_9);
		return L_0;
	}
}
// System.Boolean LoginControl/<LoginRoute>c__Iterator1::MoveNext()
extern TypeInfo* WWWForm_t6_79_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t6_10_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5131;
extern Il2CppCodeGenString* _stringLiteral5132;
extern Il2CppCodeGenString* _stringLiteral5133;
extern Il2CppCodeGenString* _stringLiteral5125;
extern Il2CppCodeGenString* _stringLiteral5126;
extern Il2CppCodeGenString* _stringLiteral5128;
extern Il2CppCodeGenString* _stringLiteral4199;
extern Il2CppCodeGenString* _stringLiteral5134;
extern Il2CppCodeGenString* _stringLiteral5123;
extern Il2CppCodeGenString* _stringLiteral5135;
extern Il2CppCodeGenString* _stringLiteral5103;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral5102;
extern Il2CppCodeGenString* _stringLiteral5122;
extern Il2CppCodeGenString* _stringLiteral5130;
extern Il2CppCodeGenString* _stringLiteral5136;
extern "C" bool U3CLoginRouteU3Ec__Iterator1_MoveNext_m8_89 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t6_79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1943);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		WaitForSeconds_t6_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1808);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5131 = il2cpp_codegen_string_literal_from_index(5131);
		_stringLiteral5132 = il2cpp_codegen_string_literal_from_index(5132);
		_stringLiteral5133 = il2cpp_codegen_string_literal_from_index(5133);
		_stringLiteral5125 = il2cpp_codegen_string_literal_from_index(5125);
		_stringLiteral5126 = il2cpp_codegen_string_literal_from_index(5126);
		_stringLiteral5128 = il2cpp_codegen_string_literal_from_index(5128);
		_stringLiteral4199 = il2cpp_codegen_string_literal_from_index(4199);
		_stringLiteral5134 = il2cpp_codegen_string_literal_from_index(5134);
		_stringLiteral5123 = il2cpp_codegen_string_literal_from_index(5123);
		_stringLiteral5135 = il2cpp_codegen_string_literal_from_index(5135);
		_stringLiteral5103 = il2cpp_codegen_string_literal_from_index(5103);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		_stringLiteral5122 = il2cpp_codegen_string_literal_from_index(5122);
		_stringLiteral5130 = il2cpp_codegen_string_literal_from_index(5130);
		_stringLiteral5136 = il2cpp_codegen_string_literal_from_index(5136);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	double V_1 = 0.0;
	DateTime_t1_150  V_2 = {0};
	bool V_3 = false;
	{
		int32_t L_0 = (__this->___U24PC_8);
		V_0 = L_0;
		__this->___U24PC_8 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00d5;
		}
	}
	{
		goto IL_0291;
	}

IL_0021:
	{
		WWWForm_t6_79 * L_2 = (WWWForm_t6_79 *)il2cpp_codegen_object_new (WWWForm_t6_79_il2cpp_TypeInfo_var);
		WWWForm__ctor_m6_560(L_2, /*hidden argument*/NULL);
		__this->___U3C_fU3E__0_0 = L_2;
		WWWForm_t6_79 * L_3 = (__this->___U3C_fU3E__0_0);
		String_t* L_4 = (__this->____phone_1);
		NullCheck(L_3);
		WWWForm_AddField_m6_561(L_3, _stringLiteral5131, L_4, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_5 = (__this->___U3C_fU3E__0_0);
		String_t* L_6 = (__this->____password_2);
		NullCheck(L_5);
		WWWForm_AddField_m6_561(L_5, _stringLiteral5132, L_6, /*hidden argument*/NULL);
		LoginControl_t8_11 * L_7 = (__this->___U3CU3Ef__this_12);
		NullCheck(L_7);
		String_t* L_8 = (L_7->___url_dev_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5133, L_8, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_10 = (__this->___U3C_fU3E__0_0);
		WWW_t6_78 * L_11 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_542(L_11, L_9, L_10, /*hidden argument*/NULL);
		__this->___U3C_wU3E__1_3 = L_11;
		__this->___U3C_elapseU3E__2_4 = (0.0f);
		float L_12 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3C_timeU3E__3_5 = L_12;
		__this->___U3C_timeoutU3E__4_6 = (15.0f);
		goto IL_00f9;
	}

IL_00a4:
	{
		WWW_t6_78 * L_13 = (__this->___U3C_wU3E__1_3);
		NullCheck(L_13);
		bool L_14 = WWW_get_isDone_m6_557(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00b9;
		}
	}
	{
		goto IL_010a;
	}

IL_00b9:
	{
		WaitForSeconds_t6_10 * L_15 = (WaitForSeconds_t6_10 *)il2cpp_codegen_object_new (WaitForSeconds_t6_10_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m6_10(L_15, (0.1f), /*hidden argument*/NULL);
		__this->___U24current_9 = L_15;
		__this->___U24PC_8 = 1;
		goto IL_0293;
	}

IL_00d5:
	{
		float L_16 = (__this->___U3C_elapseU3E__2_4);
		float L_17 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = (__this->___U3C_timeU3E__3_5);
		__this->___U3C_elapseU3E__2_4 = ((float)((float)L_16+(float)((float)((float)L_17-(float)L_18))));
		float L_19 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3C_timeU3E__3_5 = L_19;
	}

IL_00f9:
	{
		float L_20 = (__this->___U3C_elapseU3E__2_4);
		float L_21 = (__this->___U3C_timeoutU3E__4_6);
		if ((((float)L_20) < ((float)L_21)))
		{
			goto IL_00a4;
		}
	}

IL_010a:
	{
		WWW_t6_78 * L_22 = (__this->___U3C_wU3E__1_3);
		NullCheck(L_22);
		bool L_23 = WWW_get_isDone_m6_557(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_012f;
		}
	}
	{
		LoginControl_t8_11 * L_24 = (__this->___U3CU3Ef__this_12);
		NullCheck(L_24);
		LoginControl_DisplayError_m8_96(L_24, _stringLiteral5125, /*hidden argument*/NULL);
		goto IL_0291;
	}

IL_012f:
	{
		WWW_t6_78 * L_25 = (__this->___U3C_wU3E__1_3);
		NullCheck(L_25);
		String_t* L_26 = WWW_get_error_m6_554(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_27 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0159;
		}
	}
	{
		WWW_t6_78 * L_28 = (__this->___U3C_wU3E__1_3);
		NullCheck(L_28);
		String_t* L_29 = WWW_get_text_m6_550(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_30 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_017e;
		}
	}

IL_0159:
	{
		LoginControl_t8_11 * L_31 = (__this->___U3CU3Ef__this_12);
		WWW_t6_78 * L_32 = (__this->___U3C_wU3E__1_3);
		NullCheck(L_32);
		String_t* L_33 = WWW_get_error_m6_554(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5126, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		LoginControl_DisplayError_m8_96(L_31, L_34, /*hidden argument*/NULL);
		goto IL_0291;
	}

IL_017e:
	{
		WWW_t6_78 * L_35 = (__this->___U3C_wU3E__1_3);
		NullCheck(L_35);
		String_t* L_36 = WWW_get_text_m6_550(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_37 = JSONObject_Parse_m8_57(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		__this->___U3C_jsonU3E__5_7 = L_37;
		JSONObject_t8_5 * L_38 = (__this->___U3C_jsonU3E__5_7);
		NullCheck(L_38);
		double L_39 = JSONObject_GetNumber_m8_49(L_38, _stringLiteral5128, /*hidden argument*/NULL);
		V_1 = L_39;
		bool L_40 = Double_Equals_m1_656((&V_1), (0.0), /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_026f;
		}
	}
	{
		JSONObject_t8_5 * L_41 = (__this->___U3C_jsonU3E__5_7);
		NullCheck(L_41);
		bool L_42 = JSONObject_GetBoolean_m8_51(L_41, _stringLiteral4199, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_026f;
		}
	}
	{
		PlayerPrefs_SetInt_m6_803(NULL /*static, unused*/, _stringLiteral5134, 1, /*hidden argument*/NULL);
		JSONObject_t8_5 * L_43 = (__this->___U3C_jsonU3E__5_7);
		NullCheck(L_43);
		JSONObject_t8_5 * L_44 = JSONObject_GetObject_m8_50(L_43, _stringLiteral5135, /*hidden argument*/NULL);
		NullCheck(L_44);
		String_t* L_45 = JSONObject_GetString_m8_48(L_44, _stringLiteral5123, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5123, L_45, /*hidden argument*/NULL);
		String_t* L_46 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5103, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_47 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0229;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_48 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_48;
		String_t* L_49 = DateTime_ToString_m1_13896((&V_2), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5103, L_49, /*hidden argument*/NULL);
	}

IL_0229:
	{
		String_t* L_50 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_51 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_0251;
		}
	}
	{
		String_t* L_52 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_53 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0260;
		}
	}

IL_0251:
	{
		Application_LoadLevel_m6_583(NULL /*static, unused*/, _stringLiteral5122, /*hidden argument*/NULL);
		goto IL_026a;
	}

IL_0260:
	{
		Application_LoadLevel_m6_583(NULL /*static, unused*/, _stringLiteral5130, /*hidden argument*/NULL);
	}

IL_026a:
	{
		goto IL_028a;
	}

IL_026f:
	{
		LoginControl_t8_11 * L_54 = (__this->___U3CU3Ef__this_12);
		JSONObject_t8_5 * L_55 = (__this->___U3C_jsonU3E__5_7);
		NullCheck(L_55);
		String_t* L_56 = JSONObject_GetString_m8_48(L_55, _stringLiteral5136, /*hidden argument*/NULL);
		NullCheck(L_54);
		LoginControl_DisplayError_m8_96(L_54, L_56, /*hidden argument*/NULL);
	}

IL_028a:
	{
		__this->___U24PC_8 = (-1);
	}

IL_0291:
	{
		return 0;
	}

IL_0293:
	{
		return 1;
	}
	// Dead block : IL_0295: ldloc.3
}
// System.Void LoginControl/<LoginRoute>c__Iterator1::Dispose()
extern "C" void U3CLoginRouteU3Ec__Iterator1_Dispose_m8_90 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_8 = (-1);
		return;
	}
}
// System.Void LoginControl/<LoginRoute>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CLoginRouteU3Ec__Iterator1_Reset_m8_91 (U3CLoginRouteU3Ec__Iterator1_t8_10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void LoginControl::.ctor()
extern Il2CppCodeGenString* _stringLiteral5099;
extern "C" void LoginControl__ctor_m8_92 (LoginControl_t8_11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5099 = il2cpp_codegen_string_literal_from_index(5099);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___url_dev_2 = _stringLiteral5099;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoginControl::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5123;
extern Il2CppCodeGenString* _stringLiteral5103;
extern Il2CppCodeGenString* _stringLiteral5130;
extern "C" void LoginControl_Start_m8_93 (LoginControl_t8_11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5123 = il2cpp_codegen_string_literal_from_index(5123);
		_stringLiteral5103 = il2cpp_codegen_string_literal_from_index(5103);
		_stringLiteral5130 = il2cpp_codegen_string_literal_from_index(5130);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	{
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5123, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0049;
		}
	}
	{
		String_t* L_2 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5103, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_4 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = DateTime_ToString_m1_13896((&V_0), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5103, L_5, /*hidden argument*/NULL);
	}

IL_003f:
	{
		Application_LoadLevel_m6_583(NULL /*static, unused*/, _stringLiteral5130, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return;
	}
}
// System.Void LoginControl::Login()
extern "C" void LoginControl_Login_m8_94 (LoginControl_t8_11 * __this, const MethodInfo* method)
{
	{
		InputField_t7_98 * L_0 = (__this->___phone_3);
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m7_629(L_0, /*hidden argument*/NULL);
		InputField_t7_98 * L_2 = (__this->___password_4);
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m7_629(L_2, /*hidden argument*/NULL);
		Object_t * L_4 = LoginControl_LoginRoute_m8_95(__this, L_1, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator LoginControl::LoginRoute(System.String,System.String)
extern TypeInfo* U3CLoginRouteU3Ec__Iterator1_t8_10_il2cpp_TypeInfo_var;
extern "C" Object_t * LoginControl_LoginRoute_m8_95 (LoginControl_t8_11 * __this, String_t* ____phone, String_t* ____password, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLoginRouteU3Ec__Iterator1_t8_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1950);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoginRouteU3Ec__Iterator1_t8_10 * V_0 = {0};
	{
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_0 = (U3CLoginRouteU3Ec__Iterator1_t8_10 *)il2cpp_codegen_object_new (U3CLoginRouteU3Ec__Iterator1_t8_10_il2cpp_TypeInfo_var);
		U3CLoginRouteU3Ec__Iterator1__ctor_m8_86(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_1 = V_0;
		String_t* L_2 = ____phone;
		NullCheck(L_1);
		L_1->____phone_1 = L_2;
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_3 = V_0;
		String_t* L_4 = ____password;
		NullCheck(L_3);
		L_3->____password_2 = L_4;
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_5 = V_0;
		String_t* L_6 = ____phone;
		NullCheck(L_5);
		L_5->___U3CU24U3E_phone_10 = L_6;
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_7 = V_0;
		String_t* L_8 = ____password;
		NullCheck(L_7);
		L_7->___U3CU24U3E_password_11 = L_8;
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_12 = __this;
		U3CLoginRouteU3Ec__Iterator1_t8_10 * L_10 = V_0;
		return L_10;
	}
}
// System.Void LoginControl::DisplayError(System.String)
extern "C" void LoginControl_DisplayError_m8_96 (LoginControl_t8_11 * __this, String_t* ____msg, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = (__this->___dialog_error_5);
		NullCheck(L_0);
		GameObject_SetActive_m6_742(L_0, 1, /*hidden argument*/NULL);
		Text_t7_63 * L_1 = (__this->___error_msg_6);
		String_t* L_2 = ____msg;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void TimerSetting/<UpdateTimerToServer>c__Iterator2::.ctor()
extern "C" void U3CUpdateTimerToServerU3Ec__Iterator2__ctor_m8_97 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object TimerSetting/<UpdateTimerToServer>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CUpdateTimerToServerU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_98 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Object TimerSetting/<UpdateTimerToServer>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CUpdateTimerToServerU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m8_99 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Boolean TimerSetting/<UpdateTimerToServer>c__Iterator2::MoveNext()
extern TypeInfo* WWWForm_t6_79_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t6_78_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t6_10_il2cpp_TypeInfo_var;
extern TypeInfo* JSONObject_t8_5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5123;
extern Il2CppCodeGenString* _stringLiteral5143;
extern Il2CppCodeGenString* _stringLiteral4252;
extern Il2CppCodeGenString* _stringLiteral5144;
extern Il2CppCodeGenString* _stringLiteral5145;
extern Il2CppCodeGenString* _stringLiteral5125;
extern Il2CppCodeGenString* _stringLiteral5126;
extern Il2CppCodeGenString* _stringLiteral5128;
extern Il2CppCodeGenString* _stringLiteral4199;
extern Il2CppCodeGenString* _stringLiteral5136;
extern "C" bool U3CUpdateTimerToServerU3Ec__Iterator2_MoveNext_m8_100 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t6_79_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1943);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		WWW_t6_78_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1944);
		WaitForSeconds_t6_10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1808);
		JSONObject_t8_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1932);
		_stringLiteral5123 = il2cpp_codegen_string_literal_from_index(5123);
		_stringLiteral5143 = il2cpp_codegen_string_literal_from_index(5143);
		_stringLiteral4252 = il2cpp_codegen_string_literal_from_index(4252);
		_stringLiteral5144 = il2cpp_codegen_string_literal_from_index(5144);
		_stringLiteral5145 = il2cpp_codegen_string_literal_from_index(5145);
		_stringLiteral5125 = il2cpp_codegen_string_literal_from_index(5125);
		_stringLiteral5126 = il2cpp_codegen_string_literal_from_index(5126);
		_stringLiteral5128 = il2cpp_codegen_string_literal_from_index(5128);
		_stringLiteral4199 = il2cpp_codegen_string_literal_from_index(4199);
		_stringLiteral5136 = il2cpp_codegen_string_literal_from_index(5136);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	double V_1 = 0.0;
	bool V_2 = false;
	{
		int32_t L_0 = (__this->___U24PC_7);
		V_0 = L_0;
		__this->___U24PC_7 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0123;
		}
	}
	{
		goto IL_025b;
	}

IL_0021:
	{
		WWWForm_t6_79 * L_2 = (WWWForm_t6_79 *)il2cpp_codegen_object_new (WWWForm_t6_79_il2cpp_TypeInfo_var);
		WWWForm__ctor_m6_560(L_2, /*hidden argument*/NULL);
		__this->___U3C_fU3E__0_0 = L_2;
		WWWForm_t6_79 * L_3 = (__this->___U3C_fU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5123, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m6_561(L_3, _stringLiteral5123, L_4, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_5 = (__this->___U3C_fU3E__0_0);
		TimerSetting_t8_13 * L_6 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_6);
		Text_t7_63 * L_7 = (L_6->___am_hour_2);
		TimerSetting_t8_13 * L_8 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_8);
		Text_t7_63 * L_9 = (L_8->___am_mins_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral4252, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		WWWForm_AddField_m6_561(L_5, _stringLiteral5143, L_10, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_11 = (__this->___U3C_fU3E__0_0);
		TimerSetting_t8_13 * L_12 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_12);
		Text_t7_63 * L_13 = (L_12->___pm_hour_4);
		TimerSetting_t8_13 * L_14 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_14);
		Text_t7_63 * L_15 = (L_14->___pm_mins_5);
		String_t* L_16 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral4252, L_13, L_15, /*hidden argument*/NULL);
		NullCheck(L_11);
		WWWForm_AddField_m6_561(L_11, _stringLiteral5144, L_16, /*hidden argument*/NULL);
		TimerSetting_t8_13 * L_17 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_17);
		String_t* L_18 = (L_17->___url_dev_13);
		String_t* L_19 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5145, L_18, /*hidden argument*/NULL);
		WWWForm_t6_79 * L_20 = (__this->___U3C_fU3E__0_0);
		WWW_t6_78 * L_21 = (WWW_t6_78 *)il2cpp_codegen_object_new (WWW_t6_78_il2cpp_TypeInfo_var);
		WWW__ctor_m6_542(L_21, L_19, L_20, /*hidden argument*/NULL);
		__this->___U3C_wU3E__1_1 = L_21;
		__this->___U3C_elapseU3E__2_2 = (0.0f);
		float L_22 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3C_timeU3E__3_3 = L_22;
		__this->___U3C_timeoutU3E__4_4 = (15.0f);
		goto IL_0147;
	}

IL_00f2:
	{
		WWW_t6_78 * L_23 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_23);
		bool L_24 = WWW_get_isDone_m6_557(L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0107;
		}
	}
	{
		goto IL_0158;
	}

IL_0107:
	{
		WaitForSeconds_t6_10 * L_25 = (WaitForSeconds_t6_10 *)il2cpp_codegen_object_new (WaitForSeconds_t6_10_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m6_10(L_25, (0.1f), /*hidden argument*/NULL);
		__this->___U24current_8 = L_25;
		__this->___U24PC_7 = 1;
		goto IL_025d;
	}

IL_0123:
	{
		float L_26 = (__this->___U3C_elapseU3E__2_2);
		float L_27 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_28 = (__this->___U3C_timeU3E__3_3);
		__this->___U3C_elapseU3E__2_2 = ((float)((float)L_26+(float)((float)((float)L_27-(float)L_28))));
		float L_29 = Time_get_time_m6_790(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3C_timeU3E__3_3 = L_29;
	}

IL_0147:
	{
		float L_30 = (__this->___U3C_elapseU3E__2_2);
		float L_31 = (__this->___U3C_timeoutU3E__4_4);
		if ((((float)L_30) < ((float)L_31)))
		{
			goto IL_00f2;
		}
	}

IL_0158:
	{
		WWW_t6_78 * L_32 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_32);
		bool L_33 = WWW_get_isDone_m6_557(L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_017d;
		}
	}
	{
		TimerSetting_t8_13 * L_34 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_34);
		TimerSetting_DisplayError_m8_118(L_34, _stringLiteral5125, /*hidden argument*/NULL);
		goto IL_025b;
	}

IL_017d:
	{
		WWW_t6_78 * L_35 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_35);
		String_t* L_36 = WWW_get_error_m6_554(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_37 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_01a7;
		}
	}
	{
		WWW_t6_78 * L_38 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_38);
		String_t* L_39 = WWW_get_text_m6_550(L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_01cc;
		}
	}

IL_01a7:
	{
		TimerSetting_t8_13 * L_41 = (__this->___U3CU3Ef__this_10);
		WWW_t6_78 * L_42 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_42);
		String_t* L_43 = WWW_get_error_m6_554(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5126, L_43, /*hidden argument*/NULL);
		NullCheck(L_41);
		TimerSetting_DisplayError_m8_118(L_41, L_44, /*hidden argument*/NULL);
		goto IL_025b;
	}

IL_01cc:
	{
		WWW_t6_78 * L_45 = (__this->___U3C_wU3E__1_1);
		NullCheck(L_45);
		String_t* L_46 = WWW_get_text_m6_550(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JSONObject_t8_5_il2cpp_TypeInfo_var);
		JSONObject_t8_5 * L_47 = JSONObject_Parse_m8_57(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		__this->___U3C_jsonU3E__5_5 = L_47;
		JSONObject_t8_5 * L_48 = (__this->___U3C_jsonU3E__5_5);
		NullCheck(L_48);
		double L_49 = JSONObject_GetNumber_m8_49(L_48, _stringLiteral5128, /*hidden argument*/NULL);
		V_1 = L_49;
		bool L_50 = Double_Equals_m1_656((&V_1), (0.0), /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_0239;
		}
	}
	{
		JSONObject_t8_5 * L_51 = (__this->___U3C_jsonU3E__5_5);
		NullCheck(L_51);
		bool L_52 = JSONObject_GetBoolean_m8_51(L_51, _stringLiteral4199, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0239;
		}
	}
	{
		TimerSetting_t8_13 * L_53 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_53);
		L_53->___updatedServer_12 = 1;
		Action_t5_11 * L_54 = (__this->____action_6);
		NullCheck(L_54);
		Action_Invoke_m5_51(L_54, /*hidden argument*/NULL);
		goto IL_0254;
	}

IL_0239:
	{
		TimerSetting_t8_13 * L_55 = (__this->___U3CU3Ef__this_10);
		JSONObject_t8_5 * L_56 = (__this->___U3C_jsonU3E__5_5);
		NullCheck(L_56);
		String_t* L_57 = JSONObject_GetString_m8_48(L_56, _stringLiteral5136, /*hidden argument*/NULL);
		NullCheck(L_55);
		TimerSetting_DisplayError_m8_118(L_55, L_57, /*hidden argument*/NULL);
	}

IL_0254:
	{
		__this->___U24PC_7 = (-1);
	}

IL_025b:
	{
		return 0;
	}

IL_025d:
	{
		return 1;
	}
	// Dead block : IL_025f: ldloc.2
}
// System.Void TimerSetting/<UpdateTimerToServer>c__Iterator2::Dispose()
extern "C" void U3CUpdateTimerToServerU3Ec__Iterator2_Dispose_m8_101 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_7 = (-1);
		return;
	}
}
// System.Void TimerSetting/<UpdateTimerToServer>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CUpdateTimerToServerU3Ec__Iterator2_Reset_m8_102 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void TimerSetting::.ctor()
extern Il2CppCodeGenString* _stringLiteral5099;
extern "C" void TimerSetting__ctor_m8_103 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5099 = il2cpp_codegen_string_literal_from_index(5099);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___url_dev_13 = _stringLiteral5099;
		__this->___startHour_14 = (2.0f);
		__this->___alarmInterval_15 = (15.0f);
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerSetting::Start()
extern "C" void TimerSetting_Start_m8_104 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	{
		TimerSetting_DisplayTimerSet_m8_110(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TimerSetting::CheckChangable()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral5102;
extern "C" bool TimerSetting_CheckChangable_m8_105 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	DateTime_t1_150  V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	{
		V_0 = 1;
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_2 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_3 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		DateTime_t1_150  L_4 = V_1;
		DateTime_t1_150  L_5 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		float L_7 = (__this->___alarmInterval_15);
		DateTime_t1_150  L_8 = DateTime_AddMinutes_m1_13838((&V_1), (((double)((double)((float)((float)L_7*(float)(6.0f)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_9 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		V_0 = 0;
	}

IL_005d:
	{
		String_t* L_11 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00b8;
		}
	}
	{
		String_t* L_13 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_14 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		DateTime_t1_150  L_15 = V_2;
		DateTime_t1_150  L_16 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_17 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_00b8;
		}
	}
	{
		float L_18 = (__this->___alarmInterval_15);
		DateTime_t1_150  L_19 = DateTime_AddMinutes_m1_13838((&V_2), (((double)((double)((float)((float)L_18*(float)(6.0f)))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_20 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_21 = DateTime_Compare_m1_13842(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_00b8;
		}
	}
	{
		V_0 = 0;
	}

IL_00b8:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Void TimerSetting::UpdateAMHour()
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" void TimerSetting_UpdateAMHour_m8_106 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Text_t7_63 * L_0 = (__this->___am_hour_2);
		Slider_t7_125 * L_1 = (__this->___am_hour_slider_6);
		NullCheck(L_1);
		float L_2 = (float)VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		float L_3 = (__this->___startHour_14);
		V_0 = ((float)((float)L_2+(float)L_3));
		String_t* L_4 = Single_ToString_m1_635((&V_0), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void TimerSetting::UpdateAMMins()
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" void TimerSetting_UpdateAMMins_m8_107 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Text_t7_63 * L_0 = (__this->___am_mins_3);
		Slider_t7_125 * L_1 = (__this->___am_mins_slider_7);
		NullCheck(L_1);
		float L_2 = (float)VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		float L_3 = (__this->___alarmInterval_15);
		V_0 = ((float)((float)L_2*(float)L_3));
		String_t* L_4 = Single_ToString_m1_635((&V_0), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void TimerSetting::UpdatePMHour()
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" void TimerSetting_UpdatePMHour_m8_108 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Text_t7_63 * L_0 = (__this->___pm_hour_4);
		Slider_t7_125 * L_1 = (__this->___pm_hour_slider_8);
		NullCheck(L_1);
		float L_2 = (float)VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		float L_3 = (__this->___startHour_14);
		V_0 = ((float)((float)L_2+(float)L_3));
		String_t* L_4 = Single_ToString_m1_635((&V_0), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void TimerSetting::UpdatePMMins()
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" void TimerSetting_UpdatePMMins_m8_109 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Text_t7_63 * L_0 = (__this->___pm_mins_5);
		Slider_t7_125 * L_1 = (__this->___pm_mins_slider_9);
		NullCheck(L_1);
		float L_2 = (float)VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		float L_3 = (__this->___alarmInterval_15);
		V_0 = ((float)((float)L_2*(float)L_3));
		String_t* L_4 = Single_ToString_m1_635((&V_0), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// System.Void TimerSetting::DisplayTimerSet()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral5102;
extern "C" void TimerSetting_DisplayTimerSet_m8_110 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_0 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		TimerSetting_LoadAMTimer_m8_111(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002a;
		}
	}
	{
		TimerSetting_LoadPMTimer_m8_112(__this, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Void TimerSetting::LoadAMTimer()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" void TimerSetting_LoadAMTimer_m8_111 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5101, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Slider_t7_125 * L_2 = (__this->___am_hour_slider_6);
		int32_t L_3 = DateTime_get_Hour_m1_13820((&V_0), /*hidden argument*/NULL);
		float L_4 = (__this->___startHour_14);
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, ((float)((float)(((float)((float)L_3)))-(float)L_4)));
		Slider_t7_125 * L_5 = (__this->___am_mins_slider_7);
		int32_t L_6 = DateTime_get_Minute_m1_13821((&V_0), /*hidden argument*/NULL);
		float L_7 = (__this->___alarmInterval_15);
		NullCheck(L_5);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_5, ((float)((float)(((float)((float)L_6)))/(float)L_7)));
		Text_t7_63 * L_8 = (__this->___am_hour_2);
		int32_t L_9 = DateTime_get_Hour_m1_13820((&V_0), /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = Int32_ToString_m1_103((&V_1), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_10);
		Text_t7_63 * L_11 = (__this->___am_mins_3);
		int32_t L_12 = DateTime_get_Minute_m1_13821((&V_0), /*hidden argument*/NULL);
		V_2 = L_12;
		String_t* L_13 = Int32_ToString_m1_103((&V_2), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_13);
		return;
	}
}
// System.Void TimerSetting::LoadPMTimer()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5102;
extern Il2CppCodeGenString* _stringLiteral3496;
extern "C" void TimerSetting_LoadPMTimer_m8_112 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		_stringLiteral3496 = il2cpp_codegen_string_literal_from_index(3496);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = PlayerPrefs_GetString_m6_807(NULL /*static, unused*/, _stringLiteral5102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_1 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Slider_t7_125 * L_2 = (__this->___pm_hour_slider_8);
		int32_t L_3 = DateTime_get_Hour_m1_13820((&V_0), /*hidden argument*/NULL);
		float L_4 = (__this->___startHour_14);
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, ((float)((float)(((float)((float)L_3)))-(float)((float)((float)L_4+(float)(12.0f))))));
		Slider_t7_125 * L_5 = (__this->___pm_mins_slider_9);
		int32_t L_6 = DateTime_get_Minute_m1_13821((&V_0), /*hidden argument*/NULL);
		float L_7 = (__this->___alarmInterval_15);
		NullCheck(L_5);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_5, ((float)((float)(((float)((float)L_6)))/(float)L_7)));
		Text_t7_63 * L_8 = (__this->___pm_hour_4);
		int32_t L_9 = DateTime_get_Hour_m1_13820((&V_0), /*hidden argument*/NULL);
		V_1 = L_9;
		String_t* L_10 = Int32_ToString_m1_103((&V_1), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_10);
		Text_t7_63 * L_11 = (__this->___pm_mins_5);
		int32_t L_12 = DateTime_get_Minute_m1_13821((&V_0), /*hidden argument*/NULL);
		V_2 = L_12;
		String_t* L_13 = Int32_ToString_m1_103((&V_2), _stringLiteral3496, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_13);
		return;
	}
}
// System.Void TimerSetting::SetTimer()
extern TypeInfo* Action_t5_11_il2cpp_TypeInfo_var;
extern const MethodInfo* TimerSetting_U3CSetTimerU3Em__1_m8_120_MethodInfo_var;
extern "C" void TimerSetting_SetTimer_m8_113 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t5_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1945);
		TimerSetting_U3CSetTimerU3Em__1_m8_120_MethodInfo_var = il2cpp_codegen_method_info_from_index(466);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void VoxelBusters.NativePlugins.NotificationService::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType) */, L_0, 7);
		__this->___updatedServer_12 = 0;
		IntPtr_t L_1 = { (void*)TimerSetting_U3CSetTimerU3Em__1_m8_120_MethodInfo_var };
		Action_t5_11 * L_2 = (Action_t5_11 *)il2cpp_codegen_object_new (Action_t5_11_il2cpp_TypeInfo_var);
		Action__ctor_m5_50(L_2, __this, L_1, /*hidden argument*/NULL);
		Object_t * L_3 = TimerSetting_UpdateTimerToServer_m8_117(__this, L_2, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m6_672(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerSetting::CancelAllLocalNotifications()
extern "C" void TimerSetting_CancelAllLocalNotifications_m8_114 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void VoxelBusters.NativePlugins.NotificationService::CancelAllLocalNotification() */, L_0);
		return;
	}
}
// System.String TimerSetting::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* TimerSetting_ScheduleLocalNotification_m8_115 (TimerSetting_t8_13 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method)
{
	{
		NotificationService_t8_261 * L_0 = NPBinding_get_NotificationService_m8_1929(NULL /*static, unused*/, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_1 = ____notification;
		NullCheck(L_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker1< String_t*, CrossPlatformNotification_t8_259 * >::Invoke(8 /* System.String VoxelBusters.NativePlugins.NotificationService::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification) */, L_0, L_1);
		return L_2;
	}
}
// VoxelBusters.NativePlugins.CrossPlatformNotification TimerSetting::CreateNotification(System.String,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1839_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14949_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5137;
extern Il2CppCodeGenString* _stringLiteral424;
extern Il2CppCodeGenString* _stringLiteral5114;
extern Il2CppCodeGenString* _stringLiteral5115;
extern Il2CppCodeGenString* _stringLiteral5116;
extern Il2CppCodeGenString* _stringLiteral5117;
extern Il2CppCodeGenString* _stringLiteral5118;
extern Il2CppCodeGenString* _stringLiteral5119;
extern Il2CppCodeGenString* _stringLiteral5120;
extern Il2CppCodeGenString* _stringLiteral5121;
extern "C" CrossPlatformNotification_t8_259 * TimerSetting_CreateNotification_m8_116 (TimerSetting_t8_13 * __this, String_t* ____time, int32_t ____repeatInterval, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2_t1_1839_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1603);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1946);
		AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1947);
		CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1948);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		Dictionary_2__ctor_m1_14949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483789);
		_stringLiteral5137 = il2cpp_codegen_string_literal_from_index(5137);
		_stringLiteral424 = il2cpp_codegen_string_literal_from_index(424);
		_stringLiteral5114 = il2cpp_codegen_string_literal_from_index(5114);
		_stringLiteral5115 = il2cpp_codegen_string_literal_from_index(5115);
		_stringLiteral5116 = il2cpp_codegen_string_literal_from_index(5116);
		_stringLiteral5117 = il2cpp_codegen_string_literal_from_index(5117);
		_stringLiteral5118 = il2cpp_codegen_string_literal_from_index(5118);
		_stringLiteral5119 = il2cpp_codegen_string_literal_from_index(5119);
		_stringLiteral5120 = il2cpp_codegen_string_literal_from_index(5120);
		_stringLiteral5121 = il2cpp_codegen_string_literal_from_index(5121);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	iOSSpecificProperties_t8_266 * V_1 = {0};
	AndroidSpecificProperties_t8_265 * V_2 = {0};
	CrossPlatformNotification_t8_259 * V_3 = {0};
	{
		String_t* L_0 = ____time;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5137, L_0, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Dictionary_2_t1_1839 * L_2 = (Dictionary_2_t1_1839 *)il2cpp_codegen_object_new (Dictionary_2_t1_1839_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14949(L_2, /*hidden argument*/Dictionary_2__ctor_m1_14949_MethodInfo_var);
		V_0 = L_2;
		Object_t * L_3 = V_0;
		NullCheck(L_3);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_3, _stringLiteral424, _stringLiteral5114);
		iOSSpecificProperties_t8_266 * L_4 = (iOSSpecificProperties_t8_266 *)il2cpp_codegen_object_new (iOSSpecificProperties_t8_266_il2cpp_TypeInfo_var);
		iOSSpecificProperties__ctor_m8_1502(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		iOSSpecificProperties_t8_266 * L_5 = V_1;
		NullCheck(L_5);
		iOSSpecificProperties_set_HasAction_m8_1507(L_5, 1, /*hidden argument*/NULL);
		iOSSpecificProperties_t8_266 * L_6 = V_1;
		NullCheck(L_6);
		iOSSpecificProperties_set_AlertAction_m8_1505(L_6, _stringLiteral5115, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_7 = (AndroidSpecificProperties_t8_265 *)il2cpp_codegen_object_new (AndroidSpecificProperties_t8_265_il2cpp_TypeInfo_var);
		AndroidSpecificProperties__ctor_m8_1488(L_7, /*hidden argument*/NULL);
		V_2 = L_7;
		AndroidSpecificProperties_t8_265 * L_8 = V_2;
		NullCheck(L_8);
		AndroidSpecificProperties_set_ContentTitle_m8_1491(L_8, _stringLiteral5116, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_9 = V_2;
		NullCheck(L_9);
		AndroidSpecificProperties_set_TickerText_m8_1493(L_9, _stringLiteral5117, /*hidden argument*/NULL);
		AndroidSpecificProperties_t8_265 * L_10 = V_2;
		NullCheck(L_10);
		AndroidSpecificProperties_set_LargeIcon_m8_1499(L_10, _stringLiteral5118, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_11 = (CrossPlatformNotification_t8_259 *)il2cpp_codegen_object_new (CrossPlatformNotification_t8_259_il2cpp_TypeInfo_var);
		CrossPlatformNotification__ctor_m8_1514(L_11, /*hidden argument*/NULL);
		V_3 = L_11;
		CrossPlatformNotification_t8_259 * L_12 = V_3;
		NullCheck(L_12);
		CrossPlatformNotification_set_AlertBody_m8_1517(L_12, _stringLiteral5119, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_13 = V_3;
		String_t* L_14 = ____time;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_15 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		CrossPlatformNotification_set_FireDate_m8_1519(L_13, L_15, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_16 = V_3;
		int32_t L_17 = ____repeatInterval;
		NullCheck(L_16);
		CrossPlatformNotification_set_RepeatInterval_m8_1521(L_16, L_17, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_18 = V_3;
		NullCheck(L_18);
		CrossPlatformNotification_set_SoundName_m8_1525(L_18, _stringLiteral5120, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_19 = V_3;
		Object_t * L_20 = V_0;
		NullCheck(L_19);
		CrossPlatformNotification_set_UserInfo_m8_1523(L_19, L_20, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_21 = V_3;
		iOSSpecificProperties_t8_266 * L_22 = V_1;
		NullCheck(L_21);
		CrossPlatformNotification_set_iOSProperties_m8_1527(L_21, L_22, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_23 = V_3;
		AndroidSpecificProperties_t8_265 * L_24 = V_2;
		NullCheck(L_23);
		CrossPlatformNotification_set_AndroidProperties_m8_1529(L_23, L_24, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_25 = V_3;
		NullCheck(L_25);
		String_t* L_26 = CrossPlatformNotification_GetNotificationID_m8_1530(L_25, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_27 = V_3;
		NullCheck(L_27);
		DateTime_t1_150  L_28 = CrossPlatformNotification_get_FireDate_m8_1518(L_27, /*hidden argument*/NULL);
		DateTime_t1_150  L_29 = L_28;
		Object_t * L_30 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_29);
		String_t* L_31 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5121, L_26, L_30, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_32 = V_3;
		return L_32;
	}
}
// System.Collections.IEnumerator TimerSetting::UpdateTimerToServer(System.Action)
extern TypeInfo* U3CUpdateTimerToServerU3Ec__Iterator2_t8_12_il2cpp_TypeInfo_var;
extern "C" Object_t * TimerSetting_UpdateTimerToServer_m8_117 (TimerSetting_t8_13 * __this, Action_t5_11 * ____action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CUpdateTimerToServerU3Ec__Iterator2_t8_12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1951);
		s_Il2CppMethodIntialized = true;
	}
	U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * V_0 = {0};
	{
		U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * L_0 = (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 *)il2cpp_codegen_object_new (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12_il2cpp_TypeInfo_var);
		U3CUpdateTimerToServerU3Ec__Iterator2__ctor_m8_97(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * L_1 = V_0;
		Action_t5_11 * L_2 = ____action;
		NullCheck(L_1);
		L_1->____action_6 = L_2;
		U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * L_3 = V_0;
		Action_t5_11 * L_4 = ____action;
		NullCheck(L_3);
		L_3->___U3CU24U3E_action_9 = L_4;
		U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_10 = __this;
		U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * L_6 = V_0;
		return L_6;
	}
}
// System.Void TimerSetting::DisplayError(System.String)
extern "C" void TimerSetting_DisplayError_m8_118 (TimerSetting_t8_13 * __this, String_t* ____msg, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = (__this->___dialog_error_10);
		NullCheck(L_0);
		GameObject_SetActive_m6_742(L_0, 1, /*hidden argument*/NULL);
		Text_t7_63 * L_1 = (__this->___error_msg_11);
		String_t* L_2 = ____msg;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void TimerSetting::Logout()
extern Il2CppCodeGenString* _stringLiteral5138;
extern "C" void TimerSetting_Logout_m8_119 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5138 = il2cpp_codegen_string_literal_from_index(5138);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayerPrefs_DeleteAll_m6_808(NULL /*static, unused*/, /*hidden argument*/NULL);
		Application_LoadLevel_m6_583(NULL /*static, unused*/, _stringLiteral5138, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerSetting::<SetTimer>m__1()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5139;
extern Il2CppCodeGenString* _stringLiteral5106;
extern Il2CppCodeGenString* _stringLiteral5140;
extern Il2CppCodeGenString* _stringLiteral999;
extern Il2CppCodeGenString* _stringLiteral5101;
extern Il2CppCodeGenString* _stringLiteral265;
extern Il2CppCodeGenString* _stringLiteral5108;
extern Il2CppCodeGenString* _stringLiteral5105;
extern Il2CppCodeGenString* _stringLiteral5110;
extern Il2CppCodeGenString* _stringLiteral5141;
extern Il2CppCodeGenString* _stringLiteral5102;
extern Il2CppCodeGenString* _stringLiteral5142;
extern Il2CppCodeGenString* _stringLiteral5113;
extern Il2CppCodeGenString* _stringLiteral5109;
extern Il2CppCodeGenString* _stringLiteral5130;
extern "C" void TimerSetting_U3CSetTimerU3Em__1_m8_120 (TimerSetting_t8_13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5139 = il2cpp_codegen_string_literal_from_index(5139);
		_stringLiteral5106 = il2cpp_codegen_string_literal_from_index(5106);
		_stringLiteral5140 = il2cpp_codegen_string_literal_from_index(5140);
		_stringLiteral999 = il2cpp_codegen_string_literal_from_index(999);
		_stringLiteral5101 = il2cpp_codegen_string_literal_from_index(5101);
		_stringLiteral265 = il2cpp_codegen_string_literal_from_index(265);
		_stringLiteral5108 = il2cpp_codegen_string_literal_from_index(5108);
		_stringLiteral5105 = il2cpp_codegen_string_literal_from_index(5105);
		_stringLiteral5110 = il2cpp_codegen_string_literal_from_index(5110);
		_stringLiteral5141 = il2cpp_codegen_string_literal_from_index(5141);
		_stringLiteral5102 = il2cpp_codegen_string_literal_from_index(5102);
		_stringLiteral5142 = il2cpp_codegen_string_literal_from_index(5142);
		_stringLiteral5113 = il2cpp_codegen_string_literal_from_index(5113);
		_stringLiteral5109 = il2cpp_codegen_string_literal_from_index(5109);
		_stringLiteral5130 = il2cpp_codegen_string_literal_from_index(5130);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	DateTime_t1_150  V_4 = {0};
	String_t* V_5 = {0};
	DateTime_t1_150  V_6 = {0};
	DateTime_t1_150  V_7 = {0};
	DateTime_t1_150  V_8 = {0};
	DateTime_t1_150  V_9 = {0};
	DateTime_t1_150  V_10 = {0};
	DateTime_t1_150  V_11 = {0};
	DateTime_t1_150  V_12 = {0};
	DateTime_t1_150  V_13 = {0};
	DateTime_t1_150  V_14 = {0};
	DateTime_t1_150  V_15 = {0};
	DateTime_t1_150  V_16 = {0};
	DateTime_t1_150  V_17 = {0};
	DateTime_t1_150  V_18 = {0};
	DateTime_t1_150  V_19 = {0};
	DateTime_t1_150  V_20 = {0};
	{
		bool L_0 = (__this->___updatedServer_12);
		if (!L_0)
		{
			goto IL_03b6;
		}
	}
	{
		TimerSetting_CancelAllLocalNotifications_m8_114(__this, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5139, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_1 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_2 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_2;
		int32_t L_3 = DateTime_get_Month_m1_13815((&V_6), /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_1;
		DateTime_t1_150  L_7 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_7;
		int32_t L_8 = DateTime_get_Day_m1_13816((&V_7), /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_272* L_11 = L_6;
		DateTime_t1_150  L_12 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_12;
		int32_t L_13 = DateTime_get_Year_m1_13830((&V_8), /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 2, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_272* L_16 = L_11;
		Text_t7_63 * L_17 = (__this->___am_hour_2);
		NullCheck(L_17);
		String_t* L_18 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(64 /* System.String UnityEngine.UI.Text::get_text() */, L_17);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 3, sizeof(Object_t *))) = (Object_t *)L_18;
		ObjectU5BU5D_t1_272* L_19 = L_16;
		Text_t7_63 * L_20 = (__this->___am_mins_3);
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(64 /* System.String UnityEngine.UI.Text::get_text() */, L_20);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4, sizeof(Object_t *))) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5106, L_19, /*hidden argument*/NULL);
		V_0 = L_22;
		String_t* L_23 = V_0;
		DateTime_t1_150  L_24 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		DateTime_t1_150  L_25 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_9 = L_25;
		String_t* L_26 = DateTime_ToString_m1_13898((&V_9), _stringLiteral5140, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_27 = String_Equals_m1_441(L_26, _stringLiteral999, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00c4;
		}
	}
	{
		DateTime_AddDays_m1_13833((&V_1), (1.0), /*hidden argument*/NULL);
	}

IL_00c4:
	{
		String_t* L_28 = DateTime_ToString_m1_13896((&V_1), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5101, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_2 = L_29;
		String_t* L_30 = V_2;
		String_t* L_31 = DateTime_ToString_m1_13896((&V_1), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_32 = TimerSetting_CreateNotification_m8_116(__this, L_31, 3, /*hidden argument*/NULL);
		String_t* L_33 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_32, /*hidden argument*/NULL);
		String_t* L_34 = String_Concat_m1_559(NULL /*static, unused*/, L_30, L_33, /*hidden argument*/NULL);
		V_2 = L_34;
		String_t* L_35 = V_2;
		DateTime_t1_150  L_36 = DateTime_AddMinutes_m1_13838((&V_1), (15.0), /*hidden argument*/NULL);
		V_10 = L_36;
		String_t* L_37 = DateTime_ToString_m1_13896((&V_10), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_38 = TimerSetting_CreateNotification_m8_116(__this, L_37, 3, /*hidden argument*/NULL);
		String_t* L_39 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_38, /*hidden argument*/NULL);
		String_t* L_40 = String_Concat_m1_560(NULL /*static, unused*/, L_35, _stringLiteral265, L_39, /*hidden argument*/NULL);
		V_2 = L_40;
		String_t* L_41 = V_2;
		DateTime_t1_150  L_42 = DateTime_AddMinutes_m1_13838((&V_1), (30.0), /*hidden argument*/NULL);
		V_11 = L_42;
		String_t* L_43 = DateTime_ToString_m1_13896((&V_11), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_44 = TimerSetting_CreateNotification_m8_116(__this, L_43, 3, /*hidden argument*/NULL);
		String_t* L_45 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_44, /*hidden argument*/NULL);
		String_t* L_46 = String_Concat_m1_560(NULL /*static, unused*/, L_41, _stringLiteral265, L_45, /*hidden argument*/NULL);
		V_2 = L_46;
		String_t* L_47 = V_2;
		DateTime_t1_150  L_48 = DateTime_AddMinutes_m1_13838((&V_1), (45.0), /*hidden argument*/NULL);
		V_12 = L_48;
		String_t* L_49 = DateTime_ToString_m1_13896((&V_12), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_50 = TimerSetting_CreateNotification_m8_116(__this, L_49, 3, /*hidden argument*/NULL);
		String_t* L_51 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_50, /*hidden argument*/NULL);
		String_t* L_52 = String_Concat_m1_560(NULL /*static, unused*/, L_47, _stringLiteral265, L_51, /*hidden argument*/NULL);
		V_2 = L_52;
		String_t* L_53 = V_2;
		DateTime_t1_150  L_54 = DateTime_AddMinutes_m1_13838((&V_1), (60.0), /*hidden argument*/NULL);
		V_13 = L_54;
		String_t* L_55 = DateTime_ToString_m1_13896((&V_13), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_56 = TimerSetting_CreateNotification_m8_116(__this, L_55, 3, /*hidden argument*/NULL);
		String_t* L_57 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_56, /*hidden argument*/NULL);
		String_t* L_58 = String_Concat_m1_560(NULL /*static, unused*/, L_53, _stringLiteral265, L_57, /*hidden argument*/NULL);
		V_2 = L_58;
		String_t* L_59 = V_2;
		String_t* L_60 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5108, L_59, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		String_t* L_61 = V_2;
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5105, L_61, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral5139, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_62 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_63 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_14 = L_63;
		int32_t L_64 = DateTime_get_Month_m1_13815((&V_14), /*hidden argument*/NULL);
		int32_t L_65 = L_64;
		Object_t * L_66 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_65);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 0);
		ArrayElementTypeCheck (L_62, L_66);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_62, 0, sizeof(Object_t *))) = (Object_t *)L_66;
		ObjectU5BU5D_t1_272* L_67 = L_62;
		DateTime_t1_150  L_68 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_15 = L_68;
		int32_t L_69 = DateTime_get_Day_m1_13816((&V_15), /*hidden argument*/NULL);
		int32_t L_70 = L_69;
		Object_t * L_71 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_70);
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 1);
		ArrayElementTypeCheck (L_67, L_71);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_67, 1, sizeof(Object_t *))) = (Object_t *)L_71;
		ObjectU5BU5D_t1_272* L_72 = L_67;
		DateTime_t1_150  L_73 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_16 = L_73;
		int32_t L_74 = DateTime_get_Year_m1_13830((&V_16), /*hidden argument*/NULL);
		int32_t L_75 = L_74;
		Object_t * L_76 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_75);
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 2);
		ArrayElementTypeCheck (L_72, L_76);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_72, 2, sizeof(Object_t *))) = (Object_t *)L_76;
		ObjectU5BU5D_t1_272* L_77 = L_72;
		Text_t7_63 * L_78 = (__this->___pm_hour_4);
		NullCheck(L_78);
		String_t* L_79 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(64 /* System.String UnityEngine.UI.Text::get_text() */, L_78);
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 3);
		ArrayElementTypeCheck (L_77, L_79);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_77, 3, sizeof(Object_t *))) = (Object_t *)L_79;
		ObjectU5BU5D_t1_272* L_80 = L_77;
		Text_t7_63 * L_81 = (__this->___pm_mins_5);
		NullCheck(L_81);
		String_t* L_82 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(64 /* System.String UnityEngine.UI.Text::get_text() */, L_81);
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 4);
		ArrayElementTypeCheck (L_80, L_82);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_80, 4, sizeof(Object_t *))) = (Object_t *)L_82;
		String_t* L_83 = String_Format_m1_551(NULL /*static, unused*/, _stringLiteral5110, L_80, /*hidden argument*/NULL);
		V_3 = L_83;
		String_t* L_84 = V_3;
		DateTime_t1_150  L_85 = DateTime_Parse_m1_13865(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		V_4 = L_85;
		String_t* L_86 = V_3;
		String_t* L_87 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5141, L_86, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		String_t* L_88 = DateTime_ToString_m1_13896((&V_4), /*hidden argument*/NULL);
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5102, L_88, /*hidden argument*/NULL);
		DateTime_t1_150  L_89 = V_4;
		DateTime_t1_150  L_90 = L_89;
		Object_t * L_91 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_90);
		DateTime_t1_150  L_92 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t1_150  L_93 = L_92;
		Object_t * L_94 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_93);
		String_t* L_95 = String_Format_m1_549(NULL /*static, unused*/, _stringLiteral5142, L_91, L_94, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		String_t* L_96 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_5 = L_96;
		String_t* L_97 = V_5;
		String_t* L_98 = DateTime_ToString_m1_13896((&V_4), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_99 = TimerSetting_CreateNotification_m8_116(__this, L_98, 3, /*hidden argument*/NULL);
		String_t* L_100 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_99, /*hidden argument*/NULL);
		String_t* L_101 = String_Concat_m1_559(NULL /*static, unused*/, L_97, L_100, /*hidden argument*/NULL);
		V_5 = L_101;
		String_t* L_102 = V_5;
		DateTime_t1_150  L_103 = DateTime_AddMinutes_m1_13838((&V_4), (15.0), /*hidden argument*/NULL);
		V_17 = L_103;
		String_t* L_104 = DateTime_ToString_m1_13896((&V_17), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_105 = TimerSetting_CreateNotification_m8_116(__this, L_104, 3, /*hidden argument*/NULL);
		String_t* L_106 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_105, /*hidden argument*/NULL);
		String_t* L_107 = String_Concat_m1_560(NULL /*static, unused*/, L_102, _stringLiteral265, L_106, /*hidden argument*/NULL);
		V_5 = L_107;
		String_t* L_108 = V_5;
		DateTime_t1_150  L_109 = DateTime_AddMinutes_m1_13838((&V_4), (30.0), /*hidden argument*/NULL);
		V_18 = L_109;
		String_t* L_110 = DateTime_ToString_m1_13896((&V_18), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_111 = TimerSetting_CreateNotification_m8_116(__this, L_110, 3, /*hidden argument*/NULL);
		String_t* L_112 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_111, /*hidden argument*/NULL);
		String_t* L_113 = String_Concat_m1_560(NULL /*static, unused*/, L_108, _stringLiteral265, L_112, /*hidden argument*/NULL);
		V_5 = L_113;
		String_t* L_114 = V_5;
		DateTime_t1_150  L_115 = DateTime_AddMinutes_m1_13838((&V_4), (45.0), /*hidden argument*/NULL);
		V_19 = L_115;
		String_t* L_116 = DateTime_ToString_m1_13896((&V_19), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_117 = TimerSetting_CreateNotification_m8_116(__this, L_116, 3, /*hidden argument*/NULL);
		String_t* L_118 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_117, /*hidden argument*/NULL);
		String_t* L_119 = String_Concat_m1_560(NULL /*static, unused*/, L_114, _stringLiteral265, L_118, /*hidden argument*/NULL);
		V_5 = L_119;
		String_t* L_120 = V_5;
		DateTime_t1_150  L_121 = DateTime_AddMinutes_m1_13838((&V_4), (60.0), /*hidden argument*/NULL);
		V_20 = L_121;
		String_t* L_122 = DateTime_ToString_m1_13896((&V_20), /*hidden argument*/NULL);
		CrossPlatformNotification_t8_259 * L_123 = TimerSetting_CreateNotification_m8_116(__this, L_122, 3, /*hidden argument*/NULL);
		String_t* L_124 = TimerSetting_ScheduleLocalNotification_m8_115(__this, L_123, /*hidden argument*/NULL);
		String_t* L_125 = String_Concat_m1_560(NULL /*static, unused*/, L_120, _stringLiteral265, L_124, /*hidden argument*/NULL);
		V_5 = L_125;
		String_t* L_126 = V_5;
		String_t* L_127 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5113, L_126, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_127, /*hidden argument*/NULL);
		String_t* L_128 = V_5;
		PlayerPrefs_SetString_m6_805(NULL /*static, unused*/, _stringLiteral5109, L_128, /*hidden argument*/NULL);
		Application_LoadLevel_m6_583(NULL /*static, unused*/, _stringLiteral5130, /*hidden argument*/NULL);
	}

IL_03b6:
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow::.ctor()
extern "C" void DemoGUIWindow__ctor_m8_121 (DemoGUIWindow_t8_14 * __this, const MethodInfo* method)
{
	{
		GUIModalWindow__ctor_m8_790(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow::OnGUIWindow()
extern "C" void DemoGUIWindow_OnGUIWindow_m8_122 (DemoGUIWindow_t8_14 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow::AdjustFontBasedOnScreen()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5146;
extern "C" void DemoGUIWindow_AdjustFontBasedOnScreen_m8_123 (DemoGUIWindow_t8_14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		_stringLiteral5146 = il2cpp_codegen_string_literal_from_index(5146);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_0 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIStyle_t6_176 * L_1 = GUISkin_get_box_m6_1352(L_0, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_Clamp_m6_440(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_2)))*(float)(0.03f)))))), 0, ((int32_t)36), /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_set_fontSize_m6_1476(L_1, L_3, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_4 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_t6_176 * L_5 = GUISkin_get_button_m6_1360(L_4, /*hidden argument*/NULL);
		int32_t L_6 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Mathf_Clamp_m6_440(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_6)))*(float)(0.03f)))))), 0, ((int32_t)36), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_set_fontSize_m6_1476(L_5, L_7, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_8 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		GUIStyle_t6_176 * L_9 = GUISkin_get_label_m6_1354(L_8, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = Mathf_Clamp_m6_440(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_10)))*(float)(0.03f)))))), 0, ((int32_t)36), /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyle_set_fontSize_m6_1476(L_9, L_11, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_12 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIStyle_t6_176 * L_13 = GUISkin_get_toggle_m6_1362(L_12, /*hidden argument*/NULL);
		int32_t L_14 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = Mathf_Clamp_m6_440(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_14)))*(float)(0.03f)))))), 0, ((int32_t)36), /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIStyle_set_fontSize_m6_1476(L_13, L_15, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_16 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_t6_176 * L_17 = GUISkin_GetStyle_m6_1398(L_16, _stringLiteral5146, /*hidden argument*/NULL);
		int32_t L_18 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_19 = Mathf_Clamp_m6_440(NULL /*static, unused*/, (((int32_t)((int32_t)((float)((float)(((float)((float)L_18)))*(float)(0.03f)))))), 0, ((int32_t)40), /*hidden argument*/NULL);
		NullCheck(L_17);
		GUIStyle_set_fontSize_m6_1476(L_17, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::.ctor()
extern "C" void DemoMainMenu__ctor_m8_124 (DemoMainMenu_t8_16 * __this, const MethodInfo* method)
{
	{
		DemoGUIWindow__ctor_m8_121(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::Start()
extern const MethodInfo* Component_GetComponentsInChildren_TisDemoSubMenu_t8_18_m6_1917_MethodInfo_var;
extern "C" void DemoMainMenu_Start_m8_125 (DemoMainMenu_t8_16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisDemoSubMenu_t8_18_m6_1917_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484115);
		s_Il2CppMethodIntialized = true;
	}
	DemoGUIWindow_t8_14 * V_0 = {0};
	DemoSubMenuU5BU5D_t8_17* V_1 = {0};
	int32_t V_2 = 0;
	{
		GUIModalWindow_Start_m8_795(__this, /*hidden argument*/NULL);
		DemoSubMenuU5BU5D_t8_17* L_0 = Component_GetComponentsInChildren_TisDemoSubMenu_t8_18_m6_1917(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisDemoSubMenu_t8_18_m6_1917_MethodInfo_var);
		__this->___m_subMenuList_7 = L_0;
		DemoSubMenuU5BU5D_t8_17* L_1 = (__this->___m_subMenuList_7);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0057;
	}

IL_0021:
	{
		DemoSubMenuU5BU5D_t8_17* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(DemoSubMenu_t8_18 **)(DemoSubMenu_t8_18 **)SZArrayLdElema(L_2, L_4, sizeof(DemoSubMenu_t8_18 *)));
		GUISkin_t6_169 * L_5 = GUIModalWindow_get_UISkin_m8_792(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		DemoGUIWindow_t8_14 * L_7 = V_0;
		NullCheck(L_7);
		GUISkin_t6_169 * L_8 = GUIModalWindow_get_UISkin_m8_792(L_7, /*hidden argument*/NULL);
		bool L_9 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_8, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0053;
		}
	}
	{
		DemoGUIWindow_t8_14 * L_10 = V_0;
		GUISkin_t6_169 * L_11 = GUIModalWindow_get_UISkin_m8_792(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIModalWindow_set_UISkin_m8_793(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0053:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_13 = V_2;
		DemoSubMenuU5BU5D_t8_17* L_14 = V_1;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_0021;
		}
	}
	{
		DemoMainMenu_DisableAllSubMenus_m8_127(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::Update()
extern "C" void DemoMainMenu_Update_m8_126 (DemoMainMenu_t8_16 * __this, const MethodInfo* method)
{
	{
		DemoSubMenu_t8_18 * L_0 = (__this->___m_currentSubMenu_8);
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		DemoSubMenu_t8_18 * L_2 = (__this->___m_currentSubMenu_8);
		NullCheck(L_2);
		GameObject_t6_97 * L_3 = Component_get_gameObject_m6_725(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m6_743(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		__this->___m_currentSubMenu_8 = (DemoSubMenu_t8_18 *)NULL;
	}

IL_002d:
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::DisableAllSubMenus()
extern "C" void DemoMainMenu_DisableAllSubMenus_m8_127 (DemoMainMenu_t8_16 * __this, const MethodInfo* method)
{
	DemoSubMenu_t8_18 * V_0 = {0};
	DemoSubMenuU5BU5D_t8_17* V_1 = {0};
	int32_t V_2 = 0;
	{
		DemoSubMenuU5BU5D_t8_17* L_0 = (__this->___m_subMenuList_7);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0022;
	}

IL_000e:
	{
		DemoSubMenuU5BU5D_t8_17* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(DemoSubMenu_t8_18 **)(DemoSubMenu_t8_18 **)SZArrayLdElema(L_1, L_3, sizeof(DemoSubMenu_t8_18 *)));
		DemoSubMenu_t8_18 * L_4 = V_0;
		NullCheck(L_4);
		GameObject_t6_97 * L_5 = Component_get_gameObject_m6_725(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m6_742(L_5, 0, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_7 = V_2;
		DemoSubMenuU5BU5D_t8_17* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::EnableSubMenu(VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu)
extern "C" void DemoMainMenu_EnableSubMenu_m8_128 (DemoMainMenu_t8_16 * __this, DemoSubMenu_t8_18 * ____enabledSubMenu, const MethodInfo* method)
{
	{
		DemoMainMenu_DisableAllSubMenus_m8_127(__this, /*hidden argument*/NULL);
		DemoSubMenu_t8_18 * L_0 = ____enabledSubMenu;
		NullCheck(L_0);
		GameObject_t6_97 * L_1 = Component_get_gameObject_m6_725(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m6_742(L_1, 1, /*hidden argument*/NULL);
		DemoSubMenu_t8_18 * L_2 = ____enabledSubMenu;
		__this->___m_currentSubMenu_8 = L_2;
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::OnGUIWindow()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void DemoMainMenu_OnGUIWindow_m8_129 (DemoMainMenu_t8_16 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DemoSubMenu_t8_18 * V_1 = {0};
	{
		DemoSubMenu_t8_18 * L_0 = (__this->___m_currentSubMenu_8);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008c;
		}
	}
	{
		GUIScrollView_t8_19 * L_2 = GUIModalWindow_get_RootScrollView_m8_791(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIScrollView_BeginScrollView_m8_809(L_2, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		String_t* L_3 = Object_get_name_m6_708(__this, /*hidden argument*/NULL);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, L_3, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_006e;
	}

IL_003a:
	{
		DemoSubMenuU5BU5D_t8_17* L_4 = (__this->___m_subMenuList_7);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_1 = (*(DemoSubMenu_t8_18 **)(DemoSubMenu_t8_18 **)SZArrayLdElema(L_4, L_6, sizeof(DemoSubMenu_t8_18 *)));
		DemoSubMenu_t8_18 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t6_97 * L_8 = Component_get_gameObject_m6_725(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m6_708(L_8, /*hidden argument*/NULL);
		bool L_10 = GUILayout_Button_m6_1252(NULL /*static, unused*/, L_9, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		DemoSubMenu_t8_18 * L_11 = V_1;
		DemoMainMenu_EnableSubMenu_m8_128(__this, L_11, /*hidden argument*/NULL);
		goto IL_007c;
	}

IL_006a:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_13 = V_0;
		DemoSubMenuU5BU5D_t8_17* L_14 = (__this->___m_subMenuList_7);
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_007c:
	{
		GUIScrollView_t8_19 * L_15 = GUIModalWindow_get_RootScrollView_m8_791(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		GUIScrollView_EndScrollView_m8_810(L_15, /*hidden argument*/NULL);
		GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::.ctor()
extern TypeInfo* List_1_t1_1742_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14876_MethodInfo_var;
extern "C" void DemoSubMenu__ctor_m8_130 (DemoSubMenu_t8_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1742_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		List_1__ctor_m1_14876_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1742 * L_0 = (List_1_t1_1742 *)il2cpp_codegen_object_new (List_1_t1_1742_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14876(L_0, /*hidden argument*/List_1__ctor_m1_14876_MethodInfo_var);
		__this->___m_results_7 = L_0;
		DemoGUIWindow__ctor_m8_121(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::Start()
extern const MethodInfo* GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918_MethodInfo_var;
extern "C" void DemoSubMenu_Start_m8_131 (DemoSubMenu_t8_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484116);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIModalWindow_Start_m8_795(__this, /*hidden argument*/NULL);
		GameObject_t6_97 * L_0 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIScrollView_t8_19 * L_1 = GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918(L_0, /*hidden argument*/GameObject_AddComponent_TisGUIScrollView_t8_19_m6_1918_MethodInfo_var);
		__this->___m_resultsScrollView_8 = L_1;
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::DrawPopButton(System.String)
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void DemoSubMenu_DrawPopButton_m8_132 (DemoSubMenu_t8_18 * __this, String_t* ____popTitle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____popTitle;
		bool L_1 = GUILayout_Button_m6_1252(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		GameObject_t6_97 * L_2 = Component_get_gameObject_m6_725(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m6_742(L_2, 0, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::OnGUIWindow()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void DemoSubMenu_OnGUIWindow_m8_133 (DemoSubMenu_t8_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	{
		DemoGUIWindow_OnGUIWindow_m8_122(__this, /*hidden argument*/NULL);
		String_t* L_0 = Object_get_name_m6_708(__this, /*hidden argument*/NULL);
		GUILayout_Box_m6_1250(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::AppendResult(System.String)
extern "C" void DemoSubMenu_AppendResult_m8_134 (DemoSubMenu_t8_18 * __this, String_t* ____result, const MethodInfo* method)
{
	{
		List_1_t1_1742 * L_0 = (__this->___m_results_7);
		String_t* L_1 = ____result;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::AddNewResult(System.String)
extern "C" void DemoSubMenu_AddNewResult_m8_135 (DemoSubMenu_t8_18 * __this, String_t* ____result, const MethodInfo* method)
{
	{
		List_1_t1_1742 * L_0 = (__this->___m_results_7);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_0);
		List_1_t1_1742 * L_1 = (__this->___m_results_7);
		String_t* L_2 = ____result;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_1, L_2);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu::DrawResults()
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void DemoSubMenu_DrawResults_m8_136 (DemoSubMenu_t8_18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	{
		GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t1_1742 * L_0 = (__this->___m_results_7);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		GUIScrollView_t8_19 * L_2 = (__this->___m_resultsScrollView_8);
		GUISkin_t6_169 * L_3 = GUIModalWindow_get_UISkin_m8_792(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUIStyle_t6_176 * L_4 = GUISkin_get_window_m6_1364(L_3, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_5 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 1));
		int32_t L_6 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t6_182 * L_7 = GUILayout_MinHeight_m6_1276(NULL /*static, unused*/, ((float)((float)(((float)((float)L_6)))*(float)(0.3f))), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_7;
		NullCheck(L_2);
		GUIScrollView_BeginScrollView_m8_808(L_2, L_4, L_5, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0081;
	}

IL_004d:
	{
		List_1_t1_1742 * L_8 = (__this->___m_results_7);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		String_t* L_10 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_8, L_9);
		V_1 = L_10;
		int32_t L_11 = V_0;
		if (L_11)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_12 = V_1;
		GUILayout_Box_m6_1250(NULL /*static, unused*/, L_12, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_0071:
	{
		String_t* L_13 = V_1;
		GUILayout_Label_m6_1247(NULL /*static, unused*/, L_13, ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
	}

IL_007d:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_15 = V_0;
		List_1_t1_1742 * L_16 = (__this->___m_results_7);
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_16);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_004d;
		}
	}
	{
		GUILayout_FlexibleSpace_m6_1259(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIScrollView_t8_19 * L_18 = (__this->___m_resultsScrollView_8);
		NullCheck(L_18);
		GUIScrollView_EndScrollView_m8_810(L_18, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.Constants::.ctor()
extern "C" void Constants__ctor_m8_137 (Constants_t8_20 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::.ctor(System.Boolean)
extern "C" void ProductUpdateInfo__ctor_m8_138 (ProductUpdateInfo_t8_21 * __this, bool ____newUpdateAvailable, const MethodInfo* method)
{
	{
		bool L_0 = ____newUpdateAvailable;
		ProductUpdateInfo_set_NewUpdateAvailable_m8_141(__this, L_0, /*hidden argument*/NULL);
		ProductUpdateInfo_set_VersionNumber_m8_143(__this, (String_t*)NULL, /*hidden argument*/NULL);
		ProductUpdateInfo_set_DownloadLink_m8_145(__this, (String_t*)NULL, /*hidden argument*/NULL);
		ProductUpdateInfo_set_AssetStoreLink_m8_147(__this, (String_t*)NULL, /*hidden argument*/NULL);
		ProductUpdateInfo_set_ReleaseNote_m8_149(__this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::.ctor(System.String,System.Collections.IDictionary)
extern const MethodInfo* IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5147;
extern Il2CppCodeGenString* _stringLiteral5148;
extern Il2CppCodeGenString* _stringLiteral5149;
extern Il2CppCodeGenString* _stringLiteral5150;
extern "C" void ProductUpdateInfo__ctor_m8_139 (ProductUpdateInfo_t8_21 * __this, String_t* ____currentVersion, Object_t * ____dataDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484117);
		_stringLiteral5147 = il2cpp_codegen_string_literal_from_index(5147);
		_stringLiteral5148 = il2cpp_codegen_string_literal_from_index(5148);
		_stringLiteral5149 = il2cpp_codegen_string_literal_from_index(5149);
		_stringLiteral5150 = il2cpp_codegen_string_literal_from_index(5150);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	{
		Object_t * L_0 = ____dataDict;
		String_t* L_1 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_0, _stringLiteral5147, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_0 = L_1;
		Object_t * L_2 = ____dataDict;
		String_t* L_3 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_2, _stringLiteral5148, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_1 = L_3;
		Object_t * L_4 = ____dataDict;
		String_t* L_5 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_4, _stringLiteral5149, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_2 = L_5;
		Object_t * L_6 = ____dataDict;
		String_t* L_7 = IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969(NULL /*static, unused*/, L_6, _stringLiteral5150, /*hidden argument*/IDictionaryExtensions_GetIfAvailable_TisString_t_m8_1969_MethodInfo_var);
		V_3 = L_7;
		String_t* L_8 = V_0;
		String_t* L_9 = ____currentVersion;
		NullCheck(L_8);
		int32_t L_10 = String_CompareTo_m1_476(L_8, L_9, /*hidden argument*/NULL);
		ProductUpdateInfo_set_NewUpdateAvailable_m8_141(__this, ((((int32_t)L_10) > ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		String_t* L_11 = V_0;
		ProductUpdateInfo_set_VersionNumber_m8_143(__this, L_11, /*hidden argument*/NULL);
		String_t* L_12 = V_1;
		ProductUpdateInfo_set_DownloadLink_m8_145(__this, L_12, /*hidden argument*/NULL);
		String_t* L_13 = V_2;
		ProductUpdateInfo_set_AssetStoreLink_m8_147(__this, L_13, /*hidden argument*/NULL);
		String_t* L_14 = V_3;
		ProductUpdateInfo_set_ReleaseNote_m8_149(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_NewUpdateAvailable()
extern "C" bool ProductUpdateInfo_get_NewUpdateAvailable_m8_140 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CNewUpdateAvailableU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_NewUpdateAvailable(System.Boolean)
extern "C" void ProductUpdateInfo_set_NewUpdateAvailable_m8_141 (ProductUpdateInfo_t8_21 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CNewUpdateAvailableU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_VersionNumber()
extern "C" String_t* ProductUpdateInfo_get_VersionNumber_m8_142 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CVersionNumberU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_VersionNumber(System.String)
extern "C" void ProductUpdateInfo_set_VersionNumber_m8_143 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CVersionNumberU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_DownloadLink()
extern "C" String_t* ProductUpdateInfo_get_DownloadLink_m8_144 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CDownloadLinkU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_DownloadLink(System.String)
extern "C" void ProductUpdateInfo_set_DownloadLink_m8_145 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CDownloadLinkU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_AssetStoreLink()
extern "C" String_t* ProductUpdateInfo_get_AssetStoreLink_m8_146 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CAssetStoreLinkU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_AssetStoreLink(System.String)
extern "C" void ProductUpdateInfo_set_AssetStoreLink_m8_147 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CAssetStoreLinkU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_ReleaseNote()
extern "C" String_t* ProductUpdateInfo_get_ReleaseNote_m8_148 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CReleaseNoteU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_ReleaseNote(System.String)
extern "C" void ProductUpdateInfo_set_ReleaseNote_m8_149 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CReleaseNoteU3Ek__BackingField_8 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo
extern "C" void ProductUpdateInfo_t8_21_marshal(const ProductUpdateInfo_t8_21& unmarshaled, ProductUpdateInfo_t8_21_marshaled& marshaled)
{
	marshaled.___U3CNewUpdateAvailableU3Ek__BackingField_4 = unmarshaled.___U3CNewUpdateAvailableU3Ek__BackingField_4;
	marshaled.___U3CVersionNumberU3Ek__BackingField_5 = il2cpp_codegen_marshal_string(unmarshaled.___U3CVersionNumberU3Ek__BackingField_5);
	marshaled.___U3CDownloadLinkU3Ek__BackingField_6 = il2cpp_codegen_marshal_string(unmarshaled.___U3CDownloadLinkU3Ek__BackingField_6);
	marshaled.___U3CAssetStoreLinkU3Ek__BackingField_7 = il2cpp_codegen_marshal_string(unmarshaled.___U3CAssetStoreLinkU3Ek__BackingField_7);
	marshaled.___U3CReleaseNoteU3Ek__BackingField_8 = il2cpp_codegen_marshal_string(unmarshaled.___U3CReleaseNoteU3Ek__BackingField_8);
}
extern "C" void ProductUpdateInfo_t8_21_marshal_back(const ProductUpdateInfo_t8_21_marshaled& marshaled, ProductUpdateInfo_t8_21& unmarshaled)
{
	unmarshaled.___U3CNewUpdateAvailableU3Ek__BackingField_4 = marshaled.___U3CNewUpdateAvailableU3Ek__BackingField_4;
	unmarshaled.___U3CVersionNumberU3Ek__BackingField_5 = il2cpp_codegen_marshal_string_result(marshaled.___U3CVersionNumberU3Ek__BackingField_5);
	unmarshaled.___U3CDownloadLinkU3Ek__BackingField_6 = il2cpp_codegen_marshal_string_result(marshaled.___U3CDownloadLinkU3Ek__BackingField_6);
	unmarshaled.___U3CAssetStoreLinkU3Ek__BackingField_7 = il2cpp_codegen_marshal_string_result(marshaled.___U3CAssetStoreLinkU3Ek__BackingField_7);
	unmarshaled.___U3CReleaseNoteU3Ek__BackingField_8 = il2cpp_codegen_marshal_string_result(marshaled.___U3CReleaseNoteU3Ek__BackingField_8);
}
// Conversion method for clean up from marshalling of: VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo
extern "C" void ProductUpdateInfo_t8_21_marshal_cleanup(ProductUpdateInfo_t8_21_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___U3CVersionNumberU3Ek__BackingField_5);
	marshaled.___U3CVersionNumberU3Ek__BackingField_5 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___U3CDownloadLinkU3Ek__BackingField_6);
	marshaled.___U3CDownloadLinkU3Ek__BackingField_6 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___U3CAssetStoreLinkU3Ek__BackingField_7);
	marshaled.___U3CAssetStoreLinkU3Ek__BackingField_7 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___U3CReleaseNoteU3Ek__BackingField_8);
	marshaled.___U3CReleaseNoteU3Ek__BackingField_8 = NULL;
}
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::.ctor()
extern "C" void AssetStoreProduct__ctor_m8_150 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::.ctor(System.String,System.String,System.String)
extern "C" void AssetStoreProduct__ctor_m8_151 (AssetStoreProduct_t8_22 * __this, String_t* ____pName, String_t* ____pVersion, String_t* ____logoPath, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____pName;
		AssetStoreProduct_set_ProductName_m8_153(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____pVersion;
		AssetStoreProduct_set_ProductVersion_m8_155(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::get_ProductName()
extern "C" String_t* AssetStoreProduct_get_ProductName_m8_152 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CProductNameU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::set_ProductName(System.String)
extern "C" void AssetStoreProduct_set_ProductName_m8_153 (AssetStoreProduct_t8_22 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CProductNameU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.String VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::get_ProductVersion()
extern "C" String_t* AssetStoreProduct_get_ProductVersion_m8_154 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CProductVersionU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::set_ProductVersion(System.String)
extern "C" void AssetStoreProduct_set_ProductVersion_m8_155 (AssetStoreProduct_t8_22 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CProductVersionU3Ek__BackingField_11 = L_0;
		return;
	}
}
// UnityEngine.Texture2D VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::get_LogoTexture()
extern "C" Texture2D_t6_33 * AssetStoreProduct_get_LogoTexture_m8_156 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = (__this->___U3CLogoTextureU3Ek__BackingField_12);
		return L_0;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::set_LogoTexture(UnityEngine.Texture2D)
extern "C" void AssetStoreProduct_set_LogoTexture_m8_157 (AssetStoreProduct_t8_22 * __this, Texture2D_t6_33 * ___value, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___value;
		__this->___U3CLogoTextureU3Ek__BackingField_12 = L_0;
		return;
	}
}
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::Finalize()
extern "C" void AssetStoreProduct_Finalize_m8_158 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AssetStoreProduct_set_LogoTexture_m8_157(__this, (Texture2D_t6_33 *)NULL, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.AndroidManifestGenerator/Feature::.ctor(System.String,System.Boolean)
extern "C" void Feature__ctor_m8_159 (Feature_t8_24 * __this, String_t* ____name, bool ____required, const MethodInfo* method)
{
	{
		String_t* L_0 = ____name;
		Feature_set_Name_m8_161(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ____required;
		Feature_set_Required_m8_163(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.AndroidManifestGenerator/Feature::get_Name()
extern "C" String_t* Feature_get_Name_m8_160 (Feature_t8_24 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CNameU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.AndroidManifestGenerator/Feature::set_Name(System.String)
extern "C" void Feature_set_Name_m8_161 (Feature_t8_24 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.Utility.AndroidManifestGenerator/Feature::get_Required()
extern "C" bool Feature_get_Required_m8_162 (Feature_t8_24 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CRequiredU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.AndroidManifestGenerator/Feature::set_Required(System.Boolean)
extern "C" void Feature_set_Required_m8_163 (Feature_t8_24 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CRequiredU3Ek__BackingField_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: VoxelBusters.Utility.AndroidManifestGenerator/Feature
extern "C" void Feature_t8_24_marshal(const Feature_t8_24& unmarshaled, Feature_t8_24_marshaled& marshaled)
{
	marshaled.___U3CNameU3Ek__BackingField_0 = il2cpp_codegen_marshal_string(unmarshaled.___U3CNameU3Ek__BackingField_0);
	marshaled.___U3CRequiredU3Ek__BackingField_1 = unmarshaled.___U3CRequiredU3Ek__BackingField_1;
}
extern "C" void Feature_t8_24_marshal_back(const Feature_t8_24_marshaled& marshaled, Feature_t8_24& unmarshaled)
{
	unmarshaled.___U3CNameU3Ek__BackingField_0 = il2cpp_codegen_marshal_string_result(marshaled.___U3CNameU3Ek__BackingField_0);
	unmarshaled.___U3CRequiredU3Ek__BackingField_1 = marshaled.___U3CRequiredU3Ek__BackingField_1;
}
// Conversion method for clean up from marshalling of: VoxelBusters.Utility.AndroidManifestGenerator/Feature
extern "C" void Feature_t8_24_marshal_cleanup(Feature_t8_24_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___U3CNameU3Ek__BackingField_0);
	marshaled.___U3CNameU3Ek__BackingField_0 = NULL;
}
// System.Void VoxelBusters.Utility.AndroidManifestGenerator::.ctor()
extern "C" void AndroidManifestGenerator__ctor_m8_164 (AndroidManifestGenerator_t8_25 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderInfo::.ctor()
extern "C" void ShaderInfo__ctor_m8_165 (ShaderInfo_t8_26 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.ShaderUtility/ShaderInfo::get_Name()
extern "C" String_t* ShaderInfo_get_Name_m8_166 (ShaderInfo_t8_26 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_name_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderInfo::set_Name(System.String)
extern "C" void ShaderInfo_set_Name_m8_167 (ShaderInfo_t8_26 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_name_0 = L_0;
		return;
	}
}
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty> VoxelBusters.Utility.ShaderUtility/ShaderInfo::get_PropertyList()
extern "C" List_1_t1_1900 * ShaderInfo_get_PropertyList_m8_168 (ShaderInfo_t8_26 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1900 * L_0 = (__this->___m_propertyList_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderInfo::set_PropertyList(System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderProperty>)
extern "C" void ShaderInfo_set_PropertyList_m8_169 (ShaderInfo_t8_26 * __this, List_1_t1_1900 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1900 * L_0 = ___value;
		__this->___m_propertyList_1 = L_0;
		return;
	}
}
// UnityEngine.Shader VoxelBusters.Utility.ShaderUtility/ShaderInfo::GetShader()
extern "C" Shader_t6_71 * ShaderInfo_GetShader_m8_170 (ShaderInfo_t8_26 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = ShaderInfo_get_Name_m8_166(__this, /*hidden argument*/NULL);
		Shader_t6_71 * L_1 = Shader_Find_m6_493(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderProperty::.ctor()
extern "C" void ShaderProperty__ctor_m8_171 (ShaderProperty_t8_27 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.ShaderUtility/ShaderProperty::get_Name()
extern "C" String_t* ShaderProperty_get_Name_m8_172 (ShaderProperty_t8_27 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_name_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderProperty::set_Name(System.String)
extern "C" void ShaderProperty_set_Name_m8_173 (ShaderProperty_t8_27 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_name_0 = L_0;
		return;
	}
}
// VoxelBusters.Utility.ShaderUtility/eShaderPropertyType VoxelBusters.Utility.ShaderUtility/ShaderProperty::get_Type()
extern "C" int32_t ShaderProperty_get_Type_m8_174 (ShaderProperty_t8_27 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_type_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility/ShaderProperty::set_Type(VoxelBusters.Utility.ShaderUtility/eShaderPropertyType)
extern "C" void ShaderProperty_set_Type_m8_175 (ShaderProperty_t8_27 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_type_1 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility::.ctor()
extern TypeInfo* List_1_t1_1901_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* AdvancedScriptableObject_1_t8_30_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15033_MethodInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1__ctor_m8_1970_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5156;
extern Il2CppCodeGenString* _stringLiteral5157;
extern "C" void ShaderUtility__ctor_m8_176 (ShaderUtility_t8_29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1956);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		AdvancedScriptableObject_1_t8_30_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1957);
		List_1__ctor_m1_15033_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484118);
		AdvancedScriptableObject_1__ctor_m8_1970_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484119);
		_stringLiteral5156 = il2cpp_codegen_string_literal_from_index(5156);
		_stringLiteral5157 = il2cpp_codegen_string_literal_from_index(5157);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1901 * L_0 = (List_1_t1_1901 *)il2cpp_codegen_object_new (List_1_t1_1901_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15033(L_0, /*hidden argument*/List_1__ctor_m1_15033_MethodInfo_var);
		__this->___m_shaderInfoList_8 = L_0;
		StringU5BU5D_t1_238* L_1 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 2));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral5156);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5156;
		StringU5BU5D_t1_238* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral5157);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 1, sizeof(String_t*))) = (String_t*)_stringLiteral5157;
		__this->___m_builtInAssetPathList_9 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(AdvancedScriptableObject_1_t8_30_il2cpp_TypeInfo_var);
		AdvancedScriptableObject_1__ctor_m8_1970(__this, /*hidden argument*/AdvancedScriptableObject_1__ctor_m8_1970_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo> VoxelBusters.Utility.ShaderUtility::get_ShaderInfoList()
extern "C" List_1_t1_1901 * ShaderUtility_get_ShaderInfoList_m8_177 (ShaderUtility_t8_29 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1901 * L_0 = (__this->___m_shaderInfoList_8);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility::set_ShaderInfoList(System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>)
extern "C" void ShaderUtility_set_ShaderInfoList_m8_178 (ShaderUtility_t8_29 * __this, List_1_t1_1901 * ___value, const MethodInfo* method)
{
	{
		List_1_t1_1901 * L_0 = ___value;
		__this->___m_shaderInfoList_8 = L_0;
		return;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility::OnEnable()
extern TypeInfo* List_1_t1_1901_il2cpp_TypeInfo_var;
extern const MethodInfo* AdvancedScriptableObject_1_OnEnable_m8_1971_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_15033_MethodInfo_var;
extern "C" void ShaderUtility_OnEnable_m8_179 (ShaderUtility_t8_29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1901_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1956);
		AdvancedScriptableObject_1_OnEnable_m8_1971_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484120);
		List_1__ctor_m1_15033_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484118);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdvancedScriptableObject_1_OnEnable_m8_1971(__this, /*hidden argument*/AdvancedScriptableObject_1_OnEnable_m8_1971_MethodInfo_var);
		List_1_t1_1901 * L_0 = (__this->___m_shaderInfoList_8);
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		List_1_t1_1901 * L_1 = (List_1_t1_1901 *)il2cpp_codegen_object_new (List_1_t1_1901_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15033(L_1, /*hidden argument*/List_1__ctor_m1_15033_MethodInfo_var);
		__this->___m_shaderInfoList_8 = L_1;
	}

IL_001c:
	{
		return;
	}
}
// VoxelBusters.Utility.ShaderUtility/ShaderInfo VoxelBusters.Utility.ShaderUtility::GetShaderInfo(UnityEngine.Material)
extern "C" ShaderInfo_t8_26 * ShaderUtility_GetShaderInfo_m8_180 (ShaderUtility_t8_29 * __this, Material_t6_72 * ____material, const MethodInfo* method)
{
	Shader_t6_71 * V_0 = {0};
	{
		Material_t6_72 * L_0 = ____material;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (ShaderInfo_t8_26 *)NULL;
	}

IL_000e:
	{
		Material_t6_72 * L_2 = ____material;
		NullCheck(L_2);
		Shader_t6_71 * L_3 = Material_get_shader_m6_496(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Shader_t6_71 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		return (ShaderInfo_t8_26 *)NULL;
	}

IL_0023:
	{
		Shader_t6_71 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m6_708(L_6, /*hidden argument*/NULL);
		ShaderInfo_t8_26 * L_8 = ShaderUtility_GetShaderInfo_m8_181(__this, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// VoxelBusters.Utility.ShaderUtility/ShaderInfo VoxelBusters.Utility.ShaderUtility::GetShaderInfo(System.String)
extern "C" ShaderInfo_t8_26 * ShaderUtility_GetShaderInfo_m8_181 (ShaderUtility_t8_29 * __this, String_t* ____shaderName, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ShaderInfo_t8_26 * V_1 = {0};
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0007:
	{
		List_1_t1_1901 * L_0 = (__this->___m_shaderInfoList_8);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		ShaderInfo_t8_26 * L_2 = (ShaderInfo_t8_26 *)VirtFuncInvoker1< ShaderInfo_t8_26 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Item(System.Int32) */, L_0, L_1);
		V_1 = L_2;
		String_t* L_3 = ____shaderName;
		ShaderInfo_t8_26 * L_4 = V_1;
		NullCheck(L_4);
		String_t* L_5 = ShaderInfo_get_Name_m8_166(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_6 = String_Equals_m1_441(L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		ShaderInfo_t8_26 * L_7 = V_1;
		return L_7;
	}

IL_0027:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_9 = V_0;
		List_1_t1_1901 * L_10 = (__this->___m_shaderInfoList_8);
		NullCheck(L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Count() */, L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0007;
		}
	}
	{
		return (ShaderInfo_t8_26 *)NULL;
	}
}
// System.Void VoxelBusters.Utility.ShaderUtility::ReloadShaderUtility()
extern "C" void ShaderUtility_ReloadShaderUtility_m8_182 (ShaderUtility_t8_29 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToDateTimeUTC(System.String,System.String)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  DateTimeExtensions_ToDateTimeUTC_m8_183 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		String_t* L_0 = ____string;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (DateTime_t1_150_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t1_150  L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		String_t* L_2 = ____string;
		String_t* L_3 = ____format;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_4 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_5 = DateTime_ParseExact_m1_13869(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DateTime_t1_150  L_6 = DateTime_ToUniversalTime_m1_13901((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToDateTimeLocal(System.String,System.String)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  DateTimeExtensions_ToDateTimeLocal_m8_184 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		String_t* L_0 = ____string;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (DateTime_t1_150_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t1_150  L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		String_t* L_2 = ____string;
		String_t* L_3 = ____format;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_4 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_5 = DateTime_ParseExact_m1_13869(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DateTime_t1_150  L_6 = DateTime_ToLocalTime_m1_13900((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToZuluFormatDateTimeUTC(System.String)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5158;
extern "C" DateTime_t1_150  DateTimeExtensions_ToZuluFormatDateTimeUTC_m8_185 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral5158 = il2cpp_codegen_string_literal_from_index(5158);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		String_t* L_0 = ____string;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (DateTime_t1_150_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t1_150  L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		String_t* L_2 = ____string;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_3 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_4 = DateTime_ParseExact_m1_13869(NULL /*static, unused*/, L_2, _stringLiteral5158, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		DateTime_t1_150  L_5 = DateTime_ToUniversalTime_m1_13901((&V_1), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToZuluFormatDateTimeLocal(System.String)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5158;
extern "C" DateTime_t1_150  DateTimeExtensions_ToZuluFormatDateTimeLocal_m8_186 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral5158 = il2cpp_codegen_string_literal_from_index(5158);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	{
		String_t* L_0 = ____string;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		Initobj (DateTime_t1_150_il2cpp_TypeInfo_var, (&V_0));
		DateTime_t1_150  L_1 = V_0;
		return L_1;
	}

IL_0010:
	{
		String_t* L_2 = ____string;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_3 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_4 = DateTime_ParseExact_m1_13869(NULL /*static, unused*/, L_2, _stringLiteral5158, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		DateTime_t1_150  L_5 = DateTime_ToLocalTime_m1_13900((&V_1), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String VoxelBusters.Utility.DateTimeExtensions::ToStringUsingZuluFormat(System.DateTime)
extern Il2CppCodeGenString* _stringLiteral5158;
extern "C" String_t* DateTimeExtensions_ToStringUsingZuluFormat_m8_187 (Object_t * __this /* static, unused */, DateTime_t1_150  ____dateTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5158 = il2cpp_codegen_string_literal_from_index(5158);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	String_t* V_2 = {0};
	{
		String_t* L_0 = DateTime_ToString_m1_13898((&____dateTime), _stringLiteral5158, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		String_t* L_5 = String_Remove_m1_539(L_3, ((int32_t)((int32_t)L_4-(int32_t)3)), 1, /*hidden argument*/NULL);
		V_2 = L_5;
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToDateTimeFromJavaTime(System.Int64)
extern TypeInfo* TimeSpan_t1_368_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  DateTimeExtensions_ToDateTimeFromJavaTime_m8_188 (Object_t * __this /* static, unused */, int64_t ____time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TimeSpan_t1_368_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1_368  V_0 = {0};
	DateTime_t1_150  V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	{
		int64_t L_0 = ____time;
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t1_368_il2cpp_TypeInfo_var);
		TimeSpan_t1_368  L_1 = TimeSpan_FromMilliseconds_m1_14647(NULL /*static, unused*/, (((double)((double)L_0))), /*hidden argument*/NULL);
		V_0 = L_1;
		DateTime__ctor_m1_13793((&V_1), ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		TimeSpan_t1_368  L_2 = V_0;
		DateTime_t1_150  L_3 = DateTime_Add_m1_13832((&V_1), L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		DateTime_t1_150  L_4 = V_2;
		return L_4;
	}
}
// System.Int64 VoxelBusters.Utility.DateTimeExtensions::ToJavaTimeFromDateTime(System.DateTime)
extern "C" int64_t DateTimeExtensions_ToJavaTimeFromDateTime_m8_189 (Object_t * __this /* static, unused */, DateTime_t1_150  ____dateTime, const MethodInfo* method)
{
	DateTime_t1_150  V_0 = {0};
	int64_t V_1 = 0;
	DateTime_t1_150  V_2 = {0};
	TimeSpan_t1_368  V_3 = {0};
	{
		DateTime__ctor_m1_13793((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		DateTime_t1_150  L_0 = DateTime_ToUniversalTime_m1_13901((&____dateTime), /*hidden argument*/NULL);
		V_2 = L_0;
		DateTime_t1_150  L_1 = V_0;
		TimeSpan_t1_368  L_2 = DateTime_Subtract_m1_13887((&V_2), L_1, /*hidden argument*/NULL);
		V_3 = L_2;
		double L_3 = TimeSpan_get_TotalMilliseconds_m1_14632((&V_3), /*hidden argument*/NULL);
		V_1 = (((int64_t)((int64_t)L_3)));
		int64_t L_4 = V_1;
		return L_4;
	}
}
// System.Int32 VoxelBusters.Utility.EnumerationExtensions::GetValue(System.Enum)
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" int32_t EnumerationExtensions_GetValue_m8_190 (Object_t * __this /* static, unused */, Enum_t1_24 * ____enum, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Type_t * V_1 = {0};
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		Enum_t1_24 * L_0 = ____enum;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m1_5(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Type_t * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Array_t * L_3 = Enum_GetValues_m1_936(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator System.Array::GetEnumerator() */, L_3);
		V_3 = L_4;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0037;
		}

IL_001a:
		{
			Object_t * L_5 = V_3;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_5);
			V_2 = ((*(int32_t*)((int32_t*)UnBox (L_6, Int32_t1_3_il2cpp_TypeInfo_var))));
			Enum_t1_24 * L_7 = ____enum;
			int32_t L_8 = V_2;
			if (!((int32_t)((int32_t)((*(int32_t*)((int32_t*)UnBox (L_7, Int32_t1_3_il2cpp_TypeInfo_var))))&(int32_t)L_8)))
			{
				goto IL_0037;
			}
		}

IL_0033:
		{
			int32_t L_9 = V_0;
			int32_t L_10 = V_2;
			V_0 = ((int32_t)((int32_t)L_9|(int32_t)L_10));
		}

IL_0037:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_001a;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		{
			Object_t * L_13 = V_3;
			V_4 = ((Object_t *)IsInst(L_13, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_14 = V_4;
			if (L_14)
			{
				goto IL_0054;
			}
		}

IL_0053:
		{
			IL2CPP_END_FINALLY(71)
		}

IL_0054:
		{
			Object_t * L_15 = V_4;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(71)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005c:
	{
		int32_t L_16 = V_0;
		return L_16;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,System.String)
extern TypeInfo* GameObject_t6_97_il2cpp_TypeInfo_var;
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_191 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, String_t* ____childName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t6_97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1770);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_97 * V_0 = {0};
	{
		String_t* L_0 = ____childName;
		GameObject_t6_97 * L_1 = (GameObject_t6_97 *)il2cpp_codegen_object_new (GameObject_t6_97_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_732(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t6_97 * L_2 = ____parentGO;
		GameObject_t6_97 * L_3 = V_0;
		Vector3_t6_48  L_4 = Vector3_get_zero_m6_255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t6_50  L_5 = Quaternion_get_identity_m6_286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_48  L_6 = Vector3_get_zero_m6_255(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t6_97 * L_7 = GameObjectExtensions_AddChild_m8_194(NULL /*static, unused*/, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern TypeInfo* GameObject_t6_97_il2cpp_TypeInfo_var;
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_192 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, String_t* ____childName, Vector3_t6_48  ____localPosition, Quaternion_t6_50  ____localRotation, Vector3_t6_48  ____localScale, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t6_97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1770);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t6_97 * V_0 = {0};
	{
		String_t* L_0 = ____childName;
		GameObject_t6_97 * L_1 = (GameObject_t6_97 *)il2cpp_codegen_object_new (GameObject_t6_97_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_732(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t6_97 * L_2 = ____parentGO;
		GameObject_t6_97 * L_3 = V_0;
		Vector3_t6_48  L_4 = ____localPosition;
		Quaternion_t6_50  L_5 = ____localRotation;
		Vector3_t6_48  L_6 = ____localScale;
		GameObject_t6_97 * L_7 = GameObjectExtensions_AddChild_m8_194(NULL /*static, unused*/, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_193 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, GameObject_t6_97 * ____childGO, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = ____parentGO;
		GameObject_t6_97 * L_1 = ____childGO;
		Vector3_t6_48  L_2 = Vector3_get_zero_m6_255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t6_50  L_3 = Quaternion_get_identity_m6_286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_48  L_4 = Vector3_get_zero_m6_255(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t6_97 * L_5 = GameObjectExtensions_AddChild_m8_194(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::AddChild(UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" GameObject_t6_97 * GameObjectExtensions_AddChild_m8_194 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____parentGO, GameObject_t6_97 * ____childGO, Vector3_t6_48  ____localPosition, Quaternion_t6_50  ____localRotation, Vector3_t6_48  ____localScale, const MethodInfo* method)
{
	Transform_t6_65 * V_0 = {0};
	Transform_t6_65 * V_1 = {0};
	{
		GameObject_t6_97 * L_0 = ____parentGO;
		NullCheck(L_0);
		Transform_t6_65 * L_1 = GameObject_get_transform_m6_739(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t6_97 * L_2 = ____childGO;
		NullCheck(L_2);
		Transform_t6_65 * L_3 = GameObject_get_transform_m6_739(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Transform_t6_65 * L_4 = V_1;
		Transform_t6_65 * L_5 = V_0;
		NullCheck(L_4);
		Transform_set_parent_m6_772(L_4, L_5, /*hidden argument*/NULL);
		Transform_t6_65 * L_6 = V_1;
		Vector3_t6_48  L_7 = ____localPosition;
		NullCheck(L_6);
		Transform_set_localPosition_m6_757(L_6, L_7, /*hidden argument*/NULL);
		Transform_t6_65 * L_8 = V_1;
		Quaternion_t6_50  L_9 = ____localRotation;
		NullCheck(L_8);
		Transform_set_localRotation_m6_764(L_8, L_9, /*hidden argument*/NULL);
		Transform_t6_65 * L_10 = V_1;
		Vector3_t6_48  L_11 = ____localScale;
		NullCheck(L_10);
		Transform_set_localScale_m6_768(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t6_97 * L_12 = ____childGO;
		return L_12;
	}
}
// System.String VoxelBusters.Utility.GameObjectExtensions::GetPath(UnityEngine.GameObject)
extern "C" String_t* GameObjectExtensions_GetPath_m8_195 (Object_t * __this /* static, unused */, GameObject_t6_97 * ____gameObject, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = ____gameObject;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000e:
	{
		GameObject_t6_97 * L_2 = ____gameObject;
		NullCheck(L_2);
		Transform_t6_65 * L_3 = GameObject_get_transform_m6_739(L_2, /*hidden argument*/NULL);
		String_t* L_4 = TransformExtensions_GetPath_m8_244(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::CreateGameObjectAtPath(System.String)
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern "C" GameObject_t6_97 * GameObjectExtensions_CreateGameObjectAtPath_m8_196 (Object_t * __this /* static, unused */, String_t* ____path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ____path;
		CharU5BU5D_t1_16* L_1 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_1, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_0);
		String_t* L_2 = String_Trim_m1_458(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		CharU5BU5D_t1_16* L_4 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_4, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_3);
		StringU5BU5D_t1_238* L_5 = String_Split_m1_448(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		StringU5BU5D_t1_238* L_6 = V_1;
		NullCheck(L_6);
		V_2 = (((int32_t)((int32_t)(((Array_t *)L_6)->max_length))));
		V_3 = 0;
		StringU5BU5D_t1_238* L_7 = V_1;
		int32_t L_8 = V_2;
		GameObject_t6_97 * L_9 = GameObjectExtensions_CreateGameObject_m8_197(NULL /*static, unused*/, L_7, (&V_3), L_8, (Transform_t6_65 *)NULL, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.GameObject VoxelBusters.Utility.GameObjectExtensions::CreateGameObject(System.String[],System.Int32&,System.Int32,UnityEngine.Transform)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_97_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" GameObject_t6_97 * GameObjectExtensions_CreateGameObject_m8_197 (Object_t * __this /* static, unused */, StringU5BU5D_t1_238* ____pathComponents, int32_t* ____index, int32_t ____count, Transform_t6_65 * ____parentTransform, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameObject_t6_97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1770);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	GameObject_t6_97 * V_1 = {0};
	Transform_t6_65 * V_2 = {0};
	GameObject_t6_97 * V_3 = {0};
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	{
		int32_t L_0 = ____count;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (GameObject_t6_97 *)NULL;
	}

IL_0008:
	{
		int32_t* L_1 = ____index;
		int32_t L_2 = ____count;
		if ((((int32_t)(*((int32_t*)L_1))) < ((int32_t)L_2)))
		{
			goto IL_0017;
		}
	}
	{
		Transform_t6_65 * L_3 = ____parentTransform;
		NullCheck(L_3);
		GameObject_t6_97 * L_4 = Component_get_gameObject_m6_725(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0017:
	{
		int32_t* L_5 = ____index;
		if ((*((int32_t*)L_5)))
		{
			goto IL_005b;
		}
	}
	{
		StringU5BU5D_t1_238* L_6 = ____pathComponents;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		V_0 = (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_7, sizeof(String_t*)));
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral740, L_8, /*hidden argument*/NULL);
		GameObject_t6_97 * L_10 = GameObject_Find_m6_749(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		GameObject_t6_97 * L_11 = V_1;
		bool L_12 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_11, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0046;
		}
	}
	{
		String_t* L_13 = V_0;
		GameObject_t6_97 * L_14 = (GameObject_t6_97 *)il2cpp_codegen_object_new (GameObject_t6_97_il2cpp_TypeInfo_var);
		GameObject__ctor_m6_732(L_14, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
	}

IL_0046:
	{
		int32_t* L_15 = ____index;
		int32_t* L_16 = ____index;
		*((int32_t*)(L_15)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_16))+(int32_t)1));
		StringU5BU5D_t1_238* L_17 = ____pathComponents;
		int32_t* L_18 = ____index;
		int32_t L_19 = ____count;
		GameObject_t6_97 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t6_65 * L_21 = GameObject_get_transform_m6_739(L_20, /*hidden argument*/NULL);
		GameObject_t6_97 * L_22 = GameObjectExtensions_CreateGameObject_m8_197(NULL /*static, unused*/, L_17, L_18, L_19, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_005b:
	{
		Transform_t6_65 * L_23 = ____parentTransform;
		StringU5BU5D_t1_238* L_24 = ____pathComponents;
		int32_t* L_25 = ____index;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, (*((int32_t*)L_25)));
		int32_t L_26 = (*((int32_t*)L_25));
		NullCheck(L_23);
		Transform_t6_65 * L_27 = Transform_FindChild_m6_787(L_23, (*(String_t**)(String_t**)SZArrayLdElema(L_24, L_26, sizeof(String_t*))), /*hidden argument*/NULL);
		V_2 = L_27;
		Transform_t6_65 * L_28 = V_2;
		bool L_29 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_28, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0082;
		}
	}
	{
		int32_t* L_30 = ____index;
		int32_t* L_31 = ____index;
		*((int32_t*)(L_30)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)1));
		StringU5BU5D_t1_238* L_32 = ____pathComponents;
		int32_t* L_33 = ____index;
		int32_t L_34 = ____count;
		Transform_t6_65 * L_35 = V_2;
		GameObject_t6_97 * L_36 = GameObjectExtensions_CreateGameObject_m8_197(NULL /*static, unused*/, L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		return L_36;
	}

IL_0082:
	{
		Transform_t6_65 * L_37 = ____parentTransform;
		NullCheck(L_37);
		GameObject_t6_97 * L_38 = Component_get_gameObject_m6_725(L_37, /*hidden argument*/NULL);
		V_3 = L_38;
		goto IL_00a6;
	}

IL_008e:
	{
		StringU5BU5D_t1_238* L_39 = ____pathComponents;
		int32_t* L_40 = ____index;
		int32_t* L_41 = ____index;
		int32_t L_42 = (*((int32_t*)L_41));
		V_5 = L_42;
		*((int32_t*)(L_40)) = (int32_t)((int32_t)((int32_t)L_42+(int32_t)1));
		int32_t L_43 = V_5;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_43);
		int32_t L_44 = L_43;
		V_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_39, L_44, sizeof(String_t*)));
		GameObject_t6_97 * L_45 = V_3;
		String_t* L_46 = V_4;
		GameObject_t6_97 * L_47 = GameObjectExtensions_AddChild_m8_191(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		V_3 = L_47;
	}

IL_00a6:
	{
		int32_t* L_48 = ____index;
		int32_t L_49 = ____count;
		if ((((int32_t)(*((int32_t*)L_48))) < ((int32_t)L_49)))
		{
			goto IL_008e;
		}
	}
	{
		GameObject_t6_97 * L_50 = V_3;
		return L_50;
	}
}
// System.Object[] VoxelBusters.Utility.GenericsExtension::ToArray(System.Collections.IEnumerator)
extern TypeInfo* List_1_t1_1894_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15034_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1_15035_MethodInfo_var;
extern "C" ObjectU5BU5D_t1_272* GenericsExtension_ToArray_m8_198 (Object_t * __this /* static, unused */, Object_t * ____enumerator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1958);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		List_1__ctor_m1_15034_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484121);
		List_1_ToArray_m1_15035_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484122);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1_1894 * V_0 = {0};
	{
		Object_t * L_0 = ____enumerator;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t1_272*)NULL;
	}

IL_0008:
	{
		List_1_t1_1894 * L_1 = (List_1_t1_1894 *)il2cpp_codegen_object_new (List_1_t1_1894_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15034(L_1, /*hidden argument*/List_1__ctor_m1_15034_MethodInfo_var);
		V_0 = L_1;
		goto IL_001f;
	}

IL_0013:
	{
		List_1_t1_1894 * L_2 = V_0;
		Object_t * L_3 = ____enumerator;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_2);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_2, L_4);
	}

IL_001f:
	{
		Object_t * L_5 = ____enumerator;
		NullCheck(L_5);
		bool L_6 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_5);
		if (L_6)
		{
			goto IL_0013;
		}
	}
	{
		List_1_t1_1894 * L_7 = V_0;
		NullCheck(L_7);
		ObjectU5BU5D_t1_272* L_8 = List_1_ToArray_m1_15035(L_7, /*hidden argument*/List_1_ToArray_m1_15035_MethodInfo_var);
		return L_8;
	}
}
// System.Object[] VoxelBusters.Utility.GenericsExtension::ToArray(System.Collections.IList)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern "C" ObjectU5BU5D_t1_272* GenericsExtension_ToArray_m8_199 (Object_t * __this /* static, unused */, Object_t * ____listObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t1_272* V_1 = {0};
	int32_t V_2 = 0;
	{
		Object_t * L_0 = ____listObject;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t1_272*)NULL;
	}

IL_0008:
	{
		Object_t * L_1 = ____listObject;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_1 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, L_3));
		V_2 = 0;
		goto IL_002b;
	}

IL_001d:
	{
		ObjectU5BU5D_t1_272* L_4 = V_1;
		int32_t L_5 = V_2;
		Object_t * L_6 = ____listObject;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		Object_t * L_8 = (Object_t *)InterfaceFuncInvoker1< Object_t *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1_262_il2cpp_TypeInfo_var, L_6, L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5, sizeof(Object_t *))) = (Object_t *)L_8;
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_10 = V_2;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_001d;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_12 = V_1;
		return L_12;
	}
}
// System.Object[] VoxelBusters.Utility.GenericsExtension::ToArray(System.Collections.ICollection)
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern "C" ObjectU5BU5D_t1_272* GenericsExtension_ToArray_m8_200 (Object_t * __this /* static, unused */, Object_t * ____collection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t V_1 = 0;
	ObjectU5BU5D_t1_272* V_2 = {0};
	int32_t V_3 = 0;
	{
		Object_t * L_0 = ____collection;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t1_272*)NULL;
	}

IL_0008:
	{
		Object_t * L_1 = ____collection;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		Object_t * L_3 = ____collection;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_3);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, L_5));
		V_3 = 0;
		goto IL_0031;
	}

IL_0024:
	{
		ObjectU5BU5D_t1_272* L_6 = V_2;
		int32_t L_7 = V_3;
		int32_t L_8 = L_7;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
		Object_t * L_9 = V_0;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_9);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
		ArrayElementTypeCheck (L_6, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, L_8, sizeof(Object_t *))) = (Object_t *)L_10;
	}

IL_0031:
	{
		Object_t * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_11);
		if (L_12)
		{
			goto IL_0024;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_13 = V_2;
		return L_13;
	}
}
// System.String VoxelBusters.Utility.IOExtensions::MakeRelativePath(System.String,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5160;
extern Il2CppCodeGenString* _stringLiteral5161;
extern "C" String_t* IOExtensions_MakeRelativePath_m8_201 (Object_t * __this /* static, unused */, String_t* ____fromPath, String_t* ____toPath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		_stringLiteral5160 = il2cpp_codegen_string_literal_from_index(5160);
		_stringLiteral5161 = il2cpp_codegen_string_literal_from_index(5161);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____fromPath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral5160, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0016:
	{
		String_t* L_3 = ____toPath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_5 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_5, _stringLiteral5161, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002c:
	{
		String_t* L_6 = ____fromPath;
		Uri_t3_3 * L_7 = (Uri_t3_3 *)il2cpp_codegen_object_new (Uri_t3_3_il2cpp_TypeInfo_var);
		Uri__ctor_m3_23(L_7, L_6, /*hidden argument*/NULL);
		String_t* L_8 = ____toPath;
		String_t* L_9 = IOExtensions_MakeRelativePath_m8_202(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.String VoxelBusters.Utility.IOExtensions::MakeRelativePath(System.Uri,System.String)
extern TypeInfo* Uri_t3_3_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5162;
extern Il2CppCodeGenString* _stringLiteral5163;
extern "C" String_t* IOExtensions_MakeRelativePath_m8_202 (Object_t * __this /* static, unused */, Uri_t3_3 * ____fromUri, String_t* ____toPath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t3_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		_stringLiteral5162 = il2cpp_codegen_string_literal_from_index(5162);
		_stringLiteral5163 = il2cpp_codegen_string_literal_from_index(5163);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t3_3 * V_0 = {0};
	Uri_t3_3 * V_1 = {0};
	String_t* V_2 = {0};
	{
		Uri_t3_3 * L_0 = ____fromUri;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		bool L_1 = Uri_op_Equality_m3_26(NULL /*static, unused*/, L_0, (Uri_t3_3 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_2, _stringLiteral5162, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0017:
	{
		String_t* L_3 = ____toPath;
		Uri_t3_3 * L_4 = (Uri_t3_3 *)il2cpp_codegen_object_new (Uri_t3_3_il2cpp_TypeInfo_var);
		Uri__ctor_m3_23(L_4, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Uri_t3_3 * L_5 = ____fromUri;
		NullCheck(L_5);
		String_t* L_6 = Uri_get_Scheme_m3_33(L_5, /*hidden argument*/NULL);
		Uri_t3_3 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = Uri_get_Scheme_m3_33(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_10 = ____toPath;
		return L_10;
	}

IL_0036:
	{
		Uri_t3_3 * L_11 = ____fromUri;
		Uri_t3_3 * L_12 = V_0;
		NullCheck(L_11);
		Uri_t3_3 * L_13 = Uri_MakeRelativeUri_m3_1747(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Uri_t3_3 * L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t3_3_il2cpp_TypeInfo_var);
		String_t* L_16 = Uri_UnescapeDataString_m3_1768(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		Uri_t3_3 * L_17 = V_0;
		NullCheck(L_17);
		String_t* L_18 = Uri_get_Scheme_m3_33(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_19 = String_ToUpperInvariant_m1_545(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1_601(NULL /*static, unused*/, L_19, _stringLiteral5163, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		uint16_t L_22 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___AltDirectorySeparatorChar_1;
		uint16_t L_23 = ((Path_t1_437_StaticFields*)Path_t1_437_il2cpp_TypeInfo_var->static_fields)->___DirectorySeparatorChar_2;
		NullCheck(L_21);
		String_t* L_24 = String_Replace_m1_535(L_21, L_22, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
	}

IL_0075:
	{
		String_t* L_25 = V_2;
		return L_25;
	}
}
// System.Boolean VoxelBusters.Utility.IOExtensions::AssignPermissionRecursively(System.String,System.IO.FileAttributes)
extern TypeInfo* DirectoryInfo_t1_399_il2cpp_TypeInfo_var;
extern "C" bool IOExtensions_AssignPermissionRecursively_m8_203 (Object_t * __this /* static, unused */, String_t* ____directoryPath, int32_t ____attribute, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DirectoryInfo_t1_399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		s_Il2CppMethodIntialized = true;
	}
	DirectoryInfo_t1_399 * V_0 = {0};
	{
		String_t* L_0 = ____directoryPath;
		DirectoryInfo_t1_399 * L_1 = (DirectoryInfo_t1_399 *)il2cpp_codegen_object_new (DirectoryInfo_t1_399_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m1_4791(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DirectoryInfo_t1_399 * L_2 = V_0;
		int32_t L_3 = ____attribute;
		bool L_4 = IOExtensions_AssignPermissionRecursively_m8_204(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean VoxelBusters.Utility.IOExtensions::AssignPermissionRecursively(System.IO.DirectoryInfo,System.IO.FileAttributes)
extern "C" bool IOExtensions_AssignPermissionRecursively_m8_204 (Object_t * __this /* static, unused */, DirectoryInfo_t1_399 * ____directoryInfo, int32_t ____attribute, const MethodInfo* method)
{
	FileInfo_t1_422 * V_0 = {0};
	FileInfoU5BU5D_t1_1680* V_1 = {0};
	int32_t V_2 = 0;
	DirectoryInfo_t1_399 * V_3 = {0};
	DirectoryInfoU5BU5D_t1_1681* V_4 = {0};
	int32_t V_5 = 0;
	{
		DirectoryInfo_t1_399 * L_0 = ____directoryInfo;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		DirectoryInfo_t1_399 * L_1 = ____directoryInfo;
		DirectoryInfo_t1_399 * L_2 = L_1;
		NullCheck(L_2);
		int32_t L_3 = FileSystemInfo_get_Attributes_m1_5021(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ____attribute;
		NullCheck(L_2);
		FileSystemInfo_set_Attributes_m1_5022(L_2, ((int32_t)((int32_t)L_3|(int32_t)L_4)), /*hidden argument*/NULL);
		DirectoryInfo_t1_399 * L_5 = ____directoryInfo;
		NullCheck(L_5);
		FileInfoU5BU5D_t1_1680* L_6 = DirectoryInfo_GetFiles_m1_4801(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		V_2 = 0;
		goto IL_003a;
	}

IL_0024:
	{
		FileInfoU5BU5D_t1_1680* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_0 = (*(FileInfo_t1_422 **)(FileInfo_t1_422 **)SZArrayLdElema(L_7, L_9, sizeof(FileInfo_t1_422 *)));
		FileInfo_t1_422 * L_10 = V_0;
		FileInfo_t1_422 * L_11 = L_10;
		NullCheck(L_11);
		int32_t L_12 = FileSystemInfo_get_Attributes_m1_5021(L_11, /*hidden argument*/NULL);
		int32_t L_13 = ____attribute;
		NullCheck(L_11);
		FileSystemInfo_set_Attributes_m1_5022(L_11, ((int32_t)((int32_t)L_12|(int32_t)L_13)), /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		FileInfoU5BU5D_t1_1680* L_16 = V_1;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		DirectoryInfo_t1_399 * L_17 = ____directoryInfo;
		NullCheck(L_17);
		DirectoryInfoU5BU5D_t1_1681* L_18 = DirectoryInfo_GetDirectories_m1_4803(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		V_5 = 0;
		goto IL_0067;
	}

IL_0053:
	{
		DirectoryInfoU5BU5D_t1_1681* L_19 = V_4;
		int32_t L_20 = V_5;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		V_3 = (*(DirectoryInfo_t1_399 **)(DirectoryInfo_t1_399 **)SZArrayLdElema(L_19, L_21, sizeof(DirectoryInfo_t1_399 *)));
		DirectoryInfo_t1_399 * L_22 = V_3;
		int32_t L_23 = ____attribute;
		IOExtensions_AssignPermissionRecursively_m8_204(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		int32_t L_24 = V_5;
		V_5 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_25 = V_5;
		DirectoryInfoU5BU5D_t1_1681* L_26 = V_4;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		return 1;
	}
}
// System.Void VoxelBusters.Utility.IOExtensions::CopyFilesRecursively(System.String,System.String,System.Boolean,System.Boolean)
extern TypeInfo* DirectoryInfo_t1_399_il2cpp_TypeInfo_var;
extern "C" void IOExtensions_CopyFilesRecursively_m8_205 (Object_t * __this /* static, unused */, String_t* ____sourceDirectory, String_t* ____destinationDirectory, bool ____excludeMetaFiles, bool ____deleteDestinationFolderIfExists, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DirectoryInfo_t1_399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		s_Il2CppMethodIntialized = true;
	}
	DirectoryInfo_t1_399 * V_0 = {0};
	DirectoryInfo_t1_399 * V_1 = {0};
	{
		String_t* L_0 = ____sourceDirectory;
		DirectoryInfo_t1_399 * L_1 = (DirectoryInfo_t1_399 *)il2cpp_codegen_object_new (DirectoryInfo_t1_399_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m1_4791(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ____destinationDirectory;
		DirectoryInfo_t1_399 * L_3 = (DirectoryInfo_t1_399 *)il2cpp_codegen_object_new (DirectoryInfo_t1_399_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m1_4791(L_3, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		DirectoryInfo_t1_399 * L_4 = V_0;
		DirectoryInfo_t1_399 * L_5 = V_1;
		bool L_6 = ____excludeMetaFiles;
		bool L_7 = ____deleteDestinationFolderIfExists;
		IOExtensions_CopyFilesRecursively_m8_206(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.IOExtensions::CopyFilesRecursively(System.IO.DirectoryInfo,System.IO.DirectoryInfo,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* DirectoryNotFoundException_t1_412_il2cpp_TypeInfo_var;
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern TypeInfo* DirectoryInfo_t1_399_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5164;
extern Il2CppCodeGenString* _stringLiteral5165;
extern "C" void IOExtensions_CopyFilesRecursively_m8_206 (Object_t * __this /* static, unused */, DirectoryInfo_t1_399 * ____sourceDirectoryInfo, DirectoryInfo_t1_399 * ____destinationDirectoryInfo, bool ____excludeMetaFiles, bool ____deleteDestinationFolderIfExists, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		DirectoryNotFoundException_t1_412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		DirectoryInfo_t1_399_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		_stringLiteral5164 = il2cpp_codegen_string_literal_from_index(5164);
		_stringLiteral5165 = il2cpp_codegen_string_literal_from_index(5165);
		s_Il2CppMethodIntialized = true;
	}
	FileInfoU5BU5D_t1_1680* V_0 = {0};
	FileInfo_t1_422 * V_1 = {0};
	FileInfoU5BU5D_t1_1680* V_2 = {0};
	int32_t V_3 = 0;
	FileInfo_t1_422 * V_4 = {0};
	FileInfoU5BU5D_t1_1680* V_5 = {0};
	int32_t V_6 = 0;
	DirectoryInfoU5BU5D_t1_1681* V_7 = {0};
	DirectoryInfo_t1_399 * V_8 = {0};
	DirectoryInfoU5BU5D_t1_1681* V_9 = {0};
	int32_t V_10 = 0;
	{
		DirectoryInfo_t1_399 * L_0 = ____sourceDirectoryInfo;
		NullCheck(L_0);
		bool L_1 = DirectoryInfo_get_Exists_m1_4795(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		DirectoryInfo_t1_399 * L_2 = ____sourceDirectoryInfo;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5164, L_3, /*hidden argument*/NULL);
		DirectoryNotFoundException_t1_412 * L_5 = (DirectoryNotFoundException_t1_412 *)il2cpp_codegen_object_new (DirectoryNotFoundException_t1_412_il2cpp_TypeInfo_var);
		DirectoryNotFoundException__ctor_m1_4815(L_5, L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0021:
	{
		bool L_6 = ____deleteDestinationFolderIfExists;
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		DirectoryInfo_t1_399 * L_7 = ____destinationDirectoryInfo;
		NullCheck(L_7);
		bool L_8 = DirectoryInfo_get_Exists_m1_4795(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0039;
		}
	}
	{
		DirectoryInfo_t1_399 * L_9 = ____destinationDirectoryInfo;
		NullCheck(L_9);
		DirectoryInfo_Delete_m1_4808(L_9, 1, /*hidden argument*/NULL);
	}

IL_0039:
	{
		DirectoryInfo_t1_399 * L_10 = ____destinationDirectoryInfo;
		NullCheck(L_10);
		DirectoryInfo_Create_m1_4799(L_10, /*hidden argument*/NULL);
		DirectoryInfo_t1_399 * L_11 = ____sourceDirectoryInfo;
		NullCheck(L_11);
		FileInfoU5BU5D_t1_1680* L_12 = DirectoryInfo_GetFiles_m1_4801(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		bool L_13 = ____excludeMetaFiles;
		if (!L_13)
		{
			goto IL_0091;
		}
	}
	{
		FileInfoU5BU5D_t1_1680* L_14 = V_0;
		V_2 = L_14;
		V_3 = 0;
		goto IL_0083;
	}

IL_0055:
	{
		FileInfoU5BU5D_t1_1680* L_15 = V_2;
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		V_1 = (*(FileInfo_t1_422 **)(FileInfo_t1_422 **)SZArrayLdElema(L_15, L_17, sizeof(FileInfo_t1_422 *)));
		FileInfo_t1_422 * L_18 = V_1;
		NullCheck(L_18);
		String_t* L_19 = FileSystemInfo_get_Extension_m1_5020(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m1_601(NULL /*static, unused*/, L_19, _stringLiteral5165, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_007f;
	}

IL_0073:
	{
		FileInfo_t1_422 * L_21 = V_1;
		DirectoryInfo_t1_399 * L_22 = ____destinationDirectoryInfo;
		NullCheck(L_22);
		String_t* L_23 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_22);
		IOExtensions_CopyFile_m8_207(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
	}

IL_007f:
	{
		int32_t L_24 = V_3;
		V_3 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_25 = V_3;
		FileInfoU5BU5D_t1_1680* L_26 = V_2;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0055;
		}
	}
	{
		goto IL_00c1;
	}

IL_0091:
	{
		FileInfoU5BU5D_t1_1680* L_27 = V_0;
		V_5 = L_27;
		V_6 = 0;
		goto IL_00b6;
	}

IL_009c:
	{
		FileInfoU5BU5D_t1_1680* L_28 = V_5;
		int32_t L_29 = V_6;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		V_4 = (*(FileInfo_t1_422 **)(FileInfo_t1_422 **)SZArrayLdElema(L_28, L_30, sizeof(FileInfo_t1_422 *)));
		FileInfo_t1_422 * L_31 = V_4;
		DirectoryInfo_t1_399 * L_32 = ____destinationDirectoryInfo;
		NullCheck(L_32);
		String_t* L_33 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_32);
		IOExtensions_CopyFile_m8_207(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_6;
		V_6 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00b6:
	{
		int32_t L_35 = V_6;
		FileInfoU5BU5D_t1_1680* L_36 = V_5;
		NullCheck(L_36);
		if ((((int32_t)L_35) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_36)->max_length)))))))
		{
			goto IL_009c;
		}
	}

IL_00c1:
	{
		DirectoryInfo_t1_399 * L_37 = ____sourceDirectoryInfo;
		NullCheck(L_37);
		DirectoryInfoU5BU5D_t1_1681* L_38 = DirectoryInfo_GetDirectories_m1_4803(L_37, /*hidden argument*/NULL);
		V_7 = L_38;
		DirectoryInfoU5BU5D_t1_1681* L_39 = V_7;
		V_9 = L_39;
		V_10 = 0;
		goto IL_0102;
	}

IL_00d5:
	{
		DirectoryInfoU5BU5D_t1_1681* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = L_41;
		V_8 = (*(DirectoryInfo_t1_399 **)(DirectoryInfo_t1_399 **)SZArrayLdElema(L_40, L_42, sizeof(DirectoryInfo_t1_399 *)));
		DirectoryInfo_t1_399 * L_43 = V_8;
		DirectoryInfo_t1_399 * L_44 = ____destinationDirectoryInfo;
		NullCheck(L_44);
		String_t* L_45 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(10 /* System.String System.IO.FileSystemInfo::get_FullName() */, L_44);
		DirectoryInfo_t1_399 * L_46 = V_8;
		NullCheck(L_46);
		String_t* L_47 = DirectoryInfo_get_Name_m1_4796(L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_48 = Path_Combine_m1_5123(NULL /*static, unused*/, L_45, L_47, /*hidden argument*/NULL);
		DirectoryInfo_t1_399 * L_49 = (DirectoryInfo_t1_399 *)il2cpp_codegen_object_new (DirectoryInfo_t1_399_il2cpp_TypeInfo_var);
		DirectoryInfo__ctor_m1_4791(L_49, L_48, /*hidden argument*/NULL);
		bool L_50 = ____excludeMetaFiles;
		IOExtensions_CopyFilesRecursively_m8_206(NULL /*static, unused*/, L_43, L_49, L_50, 1, /*hidden argument*/NULL);
		int32_t L_51 = V_10;
		V_10 = ((int32_t)((int32_t)L_51+(int32_t)1));
	}

IL_0102:
	{
		int32_t L_52 = V_10;
		DirectoryInfoU5BU5D_t1_1681* L_53 = V_9;
		NullCheck(L_53);
		if ((((int32_t)L_52) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_53)->max_length)))))))
		{
			goto IL_00d5;
		}
	}
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.IOExtensions::CopyFile(System.IO.FileInfo,System.String)
extern "C" void IOExtensions_CopyFile_m8_207 (Object_t * __this /* static, unused */, FileInfo_t1_422 * ____sourceFileInfo, String_t* ____destinationFolderPath, const MethodInfo* method)
{
	{
		FileInfo_t1_422 * L_0 = ____sourceFileInfo;
		String_t* L_1 = ____destinationFolderPath;
		FileInfo_t1_422 * L_2 = ____sourceFileInfo;
		NullCheck(L_2);
		String_t* L_3 = FileInfo_get_Name_m1_4901(L_2, /*hidden argument*/NULL);
		IOExtensions_CopyFile_m8_208(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.IOExtensions::CopyFile(System.IO.FileInfo,System.String,System.String)
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern "C" void IOExtensions_CopyFile_m8_208 (Object_t * __this /* static, unused */, FileInfo_t1_422 * ____sourceFileInfo, String_t* ____destinationFolderPath, String_t* ____destinationFileName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		FileInfo_t1_422 * L_0 = ____sourceFileInfo;
		NullCheck(L_0);
		int32_t L_1 = FileSystemInfo_get_Attributes_m1_5021(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FileInfo_t1_422 * L_2 = ____sourceFileInfo;
		NullCheck(L_2);
		FileSystemInfo_set_Attributes_m1_5022(L_2, ((int32_t)128), /*hidden argument*/NULL);
		FileInfo_t1_422 * L_3 = ____sourceFileInfo;
		String_t* L_4 = ____destinationFolderPath;
		String_t* L_5 = ____destinationFileName;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_6 = Path_Combine_m1_5123(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		FileInfo_CopyTo_m1_4921(L_3, L_6, 1, /*hidden argument*/NULL);
		FileInfo_t1_422 * L_7 = ____sourceFileInfo;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		FileSystemInfo_set_Attributes_m1_5022(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.MonoBehaviourExtensions::.cctor()
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern "C" void MonoBehaviourExtensions__cctor_m8_209 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		s_Il2CppMethodIntialized = true;
	}
	{
		((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___timeScale_1 = (1.0f);
		return;
	}
}
// System.Void VoxelBusters.Utility.MonoBehaviourExtensions::PauseUnity(UnityEngine.MonoBehaviour)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5166;
extern "C" void MonoBehaviourExtensions_PauseUnity_m8_210 (Object_t * __this /* static, unused */, MonoBehaviour_t6_91 * ____monoTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		_stringLiteral5166 = il2cpp_codegen_string_literal_from_index(5166);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		bool L_0 = ((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___isPaused_0;
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		Debug_LogWarning_m6_636(NULL /*static, unused*/, _stringLiteral5166, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___isPaused_0 = 1;
		float L_1 = Time_get_timeScale_m6_794(NULL /*static, unused*/, /*hidden argument*/NULL);
		((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___timeScale_1 = L_1;
		Time_set_timeScale_m6_795(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.MonoBehaviourExtensions::ResumeUnity(UnityEngine.MonoBehaviour)
extern TypeInfo* MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5167;
extern "C" void MonoBehaviourExtensions_ResumeUnity_m8_211 (Object_t * __this /* static, unused */, MonoBehaviour_t6_91 * ____monoTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1959);
		_stringLiteral5167 = il2cpp_codegen_string_literal_from_index(5167);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		bool L_0 = ((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___isPaused_0;
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		Debug_LogWarning_m6_636(NULL /*static, unused*/, _stringLiteral5167, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var);
		((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___isPaused_0 = 0;
		float L_1 = ((MonoBehaviourExtensions_t8_38_StaticFields*)MonoBehaviourExtensions_t8_38_il2cpp_TypeInfo_var->static_fields)->___timeScale_1;
		Time_set_timeScale_m6_795(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.PlayerSettings::.ctor()
extern "C" void PlayerSettings__ctor_m8_212 (PlayerSettings_t8_39 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.PlayerSettings::GetBundleVersion()
extern "C" String_t* PlayerSettings_GetBundleVersion_m8_213 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = NativeBinding_GetBundleVersion_m8_338(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String VoxelBusters.Utility.PlayerSettings::GetBundleIdentifier()
extern "C" String_t* PlayerSettings_GetBundleIdentifier_m8_214 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = NativeBinding_GetBundleIdentifier_m8_339(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::SetFieldValue(System.Object,System.String,System.Object)
extern "C" void ReflectionExtensions_SetFieldValue_m8_215 (Object_t * __this /* static, unused */, Object_t * ____object, String_t* ____name, Object_t * ____value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ____object;
		Object_t * L_1 = ____object;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m1_5(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ____name;
		Object_t * L_4 = ____value;
		ReflectionExtensions_SetFieldValue_m8_216(NULL /*static, unused*/, L_0, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::SetFieldValue(System.Object,System.Type,System.String,System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MissingFieldException_t1_1569_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5168;
extern "C" void ReflectionExtensions_SetFieldValue_m8_216 (Object_t * __this /* static, unused */, Object_t * ____object, Type_t * ____objectType, String_t* ____name, Object_t * ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		MissingFieldException_t1_1569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1101);
		_stringLiteral5168 = il2cpp_codegen_string_literal_from_index(5168);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	FieldInfo_t * V_1 = {0};
	{
		V_0 = ((int32_t)52);
		Type_t * L_0 = ____objectType;
		String_t* L_1 = ____name;
		int32_t L_2 = V_0;
		NullCheck(L_0);
		FieldInfo_t * L_3 = (FieldInfo_t *)VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(176 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_0, L_1, L_2);
		V_1 = L_3;
		FieldInfo_t * L_4 = V_1;
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		FieldInfo_t * L_5 = V_1;
		Object_t * L_6 = ____object;
		Object_t * L_7 = ____value;
		NullCheck(L_5);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(44 /* System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object) */, L_5, L_6, L_7);
		goto IL_004e;
	}

IL_001f:
	{
		Type_t * L_8 = ____objectType;
		NullCheck(L_8);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_8);
		if (!L_9)
		{
			goto IL_003d;
		}
	}
	{
		Object_t * L_10 = ____object;
		Type_t * L_11 = ____objectType;
		NullCheck(L_11);
		Type_t * L_12 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_11);
		String_t* L_13 = ____name;
		Object_t * L_14 = ____value;
		ReflectionExtensions_SetFieldValue_m8_216(NULL /*static, unused*/, L_10, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_003d:
	{
		String_t* L_15 = ____name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5168, L_15, /*hidden argument*/NULL);
		MissingFieldException_t1_1569 * L_17 = (MissingFieldException_t1_1569 *)il2cpp_codegen_object_new (MissingFieldException_t1_1569_il2cpp_TypeInfo_var);
		MissingFieldException__ctor_m1_14269(L_17, L_16, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_17);
	}

IL_004e:
	{
		return;
	}
}
// System.Object VoxelBusters.Utility.ReflectionExtensions::GetStaticValue(System.Type,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MissingFieldException_t1_1569_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5168;
extern "C" Object_t * ReflectionExtensions_GetStaticValue_m8_217 (Object_t * __this /* static, unused */, Type_t * ____objectType, String_t* ____name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		MissingFieldException_t1_1569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1101);
		_stringLiteral5168 = il2cpp_codegen_string_literal_from_index(5168);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	FieldInfo_t * V_1 = {0};
	{
		V_0 = ((int32_t)56);
		Type_t * L_0 = ____objectType;
		String_t* L_1 = ____name;
		int32_t L_2 = V_0;
		NullCheck(L_0);
		FieldInfo_t * L_3 = (FieldInfo_t *)VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(176 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_0, L_1, L_2);
		V_1 = L_3;
		FieldInfo_t * L_4 = V_1;
		if (!L_4)
		{
			goto IL_001a;
		}
	}
	{
		FieldInfo_t * L_5 = V_1;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(70 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_5, NULL);
		return L_6;
	}

IL_001a:
	{
		Type_t * L_7 = ____objectType;
		NullCheck(L_7);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_7);
		if (!L_8)
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_9 = ____objectType;
		NullCheck(L_9);
		Type_t * L_10 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_9);
		String_t* L_11 = ____name;
		Object_t * L_12 = ReflectionExtensions_GetStaticValue_m8_217(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0032:
	{
		String_t* L_13 = ____name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5168, L_13, /*hidden argument*/NULL);
		MissingFieldException_t1_1569 * L_15 = (MissingFieldException_t1_1569 *)il2cpp_codegen_object_new (MissingFieldException_t1_1569_il2cpp_TypeInfo_var);
		MissingFieldException__ctor_m1_14269(L_15, L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_15);
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::SetStaticValue(System.Type,System.String,System.Object)
extern "C" void ReflectionExtensions_SetStaticValue_m8_218 (Object_t * __this /* static, unused */, Type_t * ____objectType, String_t* ____name, Object_t * ____value, const MethodInfo* method)
{
	int32_t V_0 = {0};
	FieldInfo_t * V_1 = {0};
	{
		V_0 = ((int32_t)56);
		Type_t * L_0 = ____objectType;
		String_t* L_1 = ____name;
		int32_t L_2 = V_0;
		NullCheck(L_0);
		FieldInfo_t * L_3 = (FieldInfo_t *)VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(176 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_0, L_1, L_2);
		V_1 = L_3;
		FieldInfo_t * L_4 = V_1;
		if (!L_4)
		{
			goto IL_001f;
		}
	}
	{
		FieldInfo_t * L_5 = V_1;
		Object_t * L_6 = ____value;
		NullCheck(L_5);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(44 /* System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object) */, L_5, NULL, L_6);
		goto IL_0037;
	}

IL_001f:
	{
		Type_t * L_7 = ____objectType;
		NullCheck(L_7);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_7);
		if (!L_8)
		{
			goto IL_0037;
		}
	}
	{
		Type_t * L_9 = ____objectType;
		NullCheck(L_9);
		Type_t * L_10 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_9);
		String_t* L_11 = ____name;
		Object_t * L_12 = ____value;
		ReflectionExtensions_SetStaticValue_m8_218(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeMethod(System.Object,System.String,System.Object)
extern TypeInfo* NullReferenceException_t1_1584_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5169;
extern "C" void ReflectionExtensions_InvokeMethod_m8_219 (Object_t * __this /* static, unused */, Object_t * ____object, String_t* ____method, Object_t * ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1_1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		_stringLiteral5169 = il2cpp_codegen_string_literal_from_index(5169);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	ObjectU5BU5D_t1_272* V_1 = {0};
	TypeU5BU5D_t1_31* V_2 = {0};
	{
		Object_t * L_0 = ____object;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullReferenceException_t1_1584 * L_1 = (NullReferenceException_t1_1584 *)il2cpp_codegen_object_new (NullReferenceException_t1_1584_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m1_14421(L_1, _stringLiteral5169, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ____object;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m1_5(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = (ObjectU5BU5D_t1_272*)NULL;
		V_2 = (TypeU5BU5D_t1_31*)NULL;
		Object_t * L_4 = ____value;
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_5 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		Object_t * L_6 = ____value;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 0, sizeof(Object_t *))) = (Object_t *)L_6;
		V_1 = L_5;
		TypeU5BU5D_t1_31* L_7 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Object_t * L_8 = ____value;
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m1_5(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, L_9);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_7, 0, sizeof(Type_t *))) = (Type_t *)L_9;
		V_2 = L_7;
	}

IL_003d:
	{
		Object_t * L_10 = ____object;
		Type_t * L_11 = V_0;
		String_t* L_12 = ____method;
		ObjectU5BU5D_t1_272* L_13 = V_1;
		TypeU5BU5D_t1_31* L_14 = V_2;
		ReflectionExtensions_InvokeMethod_m8_221(NULL /*static, unused*/, L_10, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeMethod(System.Object,System.String,System.Object[],System.Type[])
extern "C" void ReflectionExtensions_InvokeMethod_m8_220 (Object_t * __this /* static, unused */, Object_t * ____object, String_t* ____method, ObjectU5BU5D_t1_272* ____argValues, TypeU5BU5D_t1_31* ____argTypes, const MethodInfo* method)
{
	{
		Object_t * L_0 = ____object;
		Object_t * L_1 = ____object;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m1_5(L_1, /*hidden argument*/NULL);
		String_t* L_3 = ____method;
		ObjectU5BU5D_t1_272* L_4 = ____argValues;
		TypeU5BU5D_t1_31* L_5 = ____argTypes;
		ReflectionExtensions_InvokeMethod_m8_221(NULL /*static, unused*/, L_0, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeMethod(System.Object,System.Type,System.String,System.Object[],System.Type[])
extern TypeInfo* MissingMethodException_t1_1571_il2cpp_TypeInfo_var;
extern "C" void ReflectionExtensions_InvokeMethod_m8_221 (Object_t * __this /* static, unused */, Object_t * ____object, Type_t * ____objectType, String_t* ____method, ObjectU5BU5D_t1_272* ____argValues, TypeU5BU5D_t1_31* ____argTypes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MissingMethodException_t1_1571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1022);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	MethodInfo_t * V_1 = {0};
	{
		V_0 = ((int32_t)262196);
		V_1 = (MethodInfo_t *)NULL;
		ObjectU5BU5D_t1_272* L_0 = ____argValues;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Type_t * L_1 = ____objectType;
		String_t* L_2 = ____method;
		int32_t L_3 = V_0;
		TypeU5BU5D_t1_31* L_4 = ____argTypes;
		NullCheck(L_1);
		MethodInfo_t * L_5 = (MethodInfo_t *)VirtFuncInvoker5< MethodInfo_t *, String_t*, int32_t, Binder_t1_585 *, TypeU5BU5D_t1_31*, ParameterModifierU5BU5D_t1_1669* >::Invoke(84 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[]) */, L_1, L_2, L_3, (Binder_t1_585 *)NULL, L_4, (ParameterModifierU5BU5D_t1_1669*)(ParameterModifierU5BU5D_t1_1669*)NULL);
		V_1 = L_5;
		goto IL_0029;
	}

IL_0020:
	{
		Type_t * L_6 = ____objectType;
		String_t* L_7 = ____method;
		int32_t L_8 = V_0;
		NullCheck(L_6);
		MethodInfo_t * L_9 = (MethodInfo_t *)VirtFuncInvoker2< MethodInfo_t *, String_t*, int32_t >::Invoke(81 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags) */, L_6, L_7, L_8);
		V_1 = L_9;
	}

IL_0029:
	{
		MethodInfo_t * L_10 = V_1;
		if (!L_10)
		{
			goto IL_003d;
		}
	}
	{
		MethodInfo_t * L_11 = V_1;
		Object_t * L_12 = ____object;
		ObjectU5BU5D_t1_272* L_13 = ____argValues;
		NullCheck(L_11);
		VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t1_272* >::Invoke(43 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_11, L_12, L_13);
		goto IL_0063;
	}

IL_003d:
	{
		Type_t * L_14 = ____objectType;
		NullCheck(L_14);
		Type_t * L_15 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_14);
		if (!L_15)
		{
			goto IL_005d;
		}
	}
	{
		Object_t * L_16 = ____object;
		Type_t * L_17 = ____objectType;
		NullCheck(L_17);
		Type_t * L_18 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_17);
		String_t* L_19 = ____method;
		ObjectU5BU5D_t1_272* L_20 = ____argValues;
		TypeU5BU5D_t1_31* L_21 = ____argTypes;
		ReflectionExtensions_InvokeMethod_m8_221(NULL /*static, unused*/, L_16, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_005d:
	{
		MissingMethodException_t1_1571 * L_22 = (MissingMethodException_t1_1571 *)il2cpp_codegen_object_new (MissingMethodException_t1_1571_il2cpp_TypeInfo_var);
		MissingMethodException__ctor_m1_14281(L_22, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_22);
	}

IL_0063:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.ReflectionExtensions::InvokeStaticMethod(System.Type,System.String,System.Object[],System.Type[])
extern TypeInfo* MissingMethodException_t1_1571_il2cpp_TypeInfo_var;
extern "C" void ReflectionExtensions_InvokeStaticMethod_m8_222 (Object_t * __this /* static, unused */, Type_t * ____objectType, String_t* ____method, ObjectU5BU5D_t1_272* ____argValues, TypeU5BU5D_t1_31* ____argTypes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MissingMethodException_t1_1571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1022);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	MethodInfo_t * V_1 = {0};
	{
		V_0 = ((int32_t)262200);
		V_1 = (MethodInfo_t *)NULL;
		ObjectU5BU5D_t1_272* L_0 = ____argValues;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Type_t * L_1 = ____objectType;
		String_t* L_2 = ____method;
		int32_t L_3 = V_0;
		TypeU5BU5D_t1_31* L_4 = ____argTypes;
		NullCheck(L_1);
		MethodInfo_t * L_5 = (MethodInfo_t *)VirtFuncInvoker5< MethodInfo_t *, String_t*, int32_t, Binder_t1_585 *, TypeU5BU5D_t1_31*, ParameterModifierU5BU5D_t1_1669* >::Invoke(84 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[]) */, L_1, L_2, L_3, (Binder_t1_585 *)NULL, L_4, (ParameterModifierU5BU5D_t1_1669*)(ParameterModifierU5BU5D_t1_1669*)NULL);
		V_1 = L_5;
		goto IL_0028;
	}

IL_001f:
	{
		Type_t * L_6 = ____objectType;
		String_t* L_7 = ____method;
		int32_t L_8 = V_0;
		NullCheck(L_6);
		MethodInfo_t * L_9 = (MethodInfo_t *)VirtFuncInvoker2< MethodInfo_t *, String_t*, int32_t >::Invoke(81 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags) */, L_6, L_7, L_8);
		V_1 = L_9;
	}

IL_0028:
	{
		MethodInfo_t * L_10 = V_1;
		if (!L_10)
		{
			goto IL_003c;
		}
	}
	{
		MethodInfo_t * L_11 = V_1;
		ObjectU5BU5D_t1_272* L_12 = ____argValues;
		NullCheck(L_11);
		VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t1_272* >::Invoke(43 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_11, NULL, L_12);
		goto IL_0060;
	}

IL_003c:
	{
		Type_t * L_13 = ____objectType;
		NullCheck(L_13);
		Type_t * L_14 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_13);
		if (!L_14)
		{
			goto IL_005a;
		}
	}
	{
		Type_t * L_15 = ____objectType;
		NullCheck(L_15);
		Type_t * L_16 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_15);
		String_t* L_17 = ____method;
		ObjectU5BU5D_t1_272* L_18 = ____argValues;
		TypeU5BU5D_t1_31* L_19 = ____argTypes;
		ReflectionExtensions_InvokeStaticMethod_m8_222(NULL /*static, unused*/, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_0060;
	}

IL_005a:
	{
		MissingMethodException_t1_1571 * L_20 = (MissingMethodException_t1_1571 *)il2cpp_codegen_object_new (MissingMethodException_t1_1571_il2cpp_TypeInfo_var);
		MissingMethodException__ctor_m1_14281(L_20, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_0060:
	{
		return;
	}
}
// System.Reflection.FieldInfo VoxelBusters.Utility.ReflectionExtensions::GetFieldWithName(System.Type,System.String,System.Boolean)
extern "C" FieldInfo_t * ReflectionExtensions_GetFieldWithName_m8_223 (Object_t * __this /* static, unused */, Type_t * ____type, String_t* ____name, bool ____isPublic, const MethodInfo* method)
{
	int32_t V_0 = {0};
	FieldInfo_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		V_0 = 4;
		bool L_0 = ____isPublic;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1|(int32_t)((int32_t)16)));
		goto IL_0017;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2|(int32_t)((int32_t)32)));
	}

IL_0017:
	{
		Type_t * L_3 = ____type;
		String_t* L_4 = ____name;
		int32_t L_5 = V_0;
		NullCheck(L_3);
		FieldInfo_t * L_6 = (FieldInfo_t *)VirtFuncInvoker2< FieldInfo_t *, String_t*, int32_t >::Invoke(176 /* System.Reflection.FieldInfo System.Type::GetField(System.String,System.Reflection.BindingFlags) */, L_3, L_4, L_5);
		V_1 = L_6;
		FieldInfo_t * L_7 = V_1;
		if (L_7)
		{
			goto IL_003c;
		}
	}
	{
		Type_t * L_8 = ____type;
		NullCheck(L_8);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_8);
		V_2 = L_9;
		Type_t * L_10 = V_2;
		if (!L_10)
		{
			goto IL_003c;
		}
	}
	{
		Type_t * L_11 = V_2;
		String_t* L_12 = ____name;
		bool L_13 = ____isPublic;
		FieldInfo_t * L_14 = ReflectionExtensions_GetFieldWithName_m8_223(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_003c:
	{
		FieldInfo_t * L_15 = V_1;
		return L_15;
	}
}
// System.String VoxelBusters.Utility.StringExtensions::GetPrintableString(System.String)
extern Il2CppCodeGenString* _stringLiteral4818;
extern "C" String_t* StringExtensions_GetPrintableString_m8_224 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4818 = il2cpp_codegen_string_literal_from_index(4818);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = {0};
	{
		String_t* L_0 = ____string;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = _stringLiteral4818;
		goto IL_0011;
	}

IL_0010:
	{
		String_t* L_1 = ____string;
		G_B3_0 = L_1;
	}

IL_0011:
	{
		return G_B3_0;
	}
}
// System.Boolean VoxelBusters.Utility.StringExtensions::Contains(System.String,System.String,System.Boolean)
extern "C" bool StringExtensions_Contains_m8_225 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____stringToCheck, bool ____ignoreCase, const MethodInfo* method)
{
	{
		bool L_0 = ____ignoreCase;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ____string;
		String_t* L_2 = ____stringToCheck;
		NullCheck(L_1);
		bool L_3 = String_Contains_m1_520(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_000e:
	{
		String_t* L_4 = ____string;
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m1_540(L_4, /*hidden argument*/NULL);
		String_t* L_6 = ____stringToCheck;
		NullCheck(L_6);
		String_t* L_7 = String_ToLower_m1_540(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = String_Contains_m1_520(L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String VoxelBusters.Utility.StringExtensions::ToBase64(System.String)
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern "C" String_t* StringExtensions_ToBase64_m8_226 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_0 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ____string;
		NullCheck(L_0);
		ByteU5BU5D_t1_109* L_2 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		V_0 = L_2;
		ByteU5BU5D_t1_109* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		String_t* L_4 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		return L_5;
	}
}
// System.String VoxelBusters.Utility.StringExtensions::FromBase64(System.String)
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern "C" String_t* StringExtensions_FromBase64_m8_227 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	{
		String_t* L_0 = ____string;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = Convert_FromBase64String_m1_13441(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_2 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_3 = V_0;
		ByteU5BU5D_t1_109* L_4 = V_0;
		NullCheck(L_4);
		NullCheck(L_2);
		String_t* L_5 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, (((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))));
		V_1 = L_5;
		String_t* L_6 = V_1;
		return L_6;
	}
}
// System.String VoxelBusters.Utility.StringExtensions::StringBetween(System.String,System.String,System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* StringExtensions_StringBetween_m8_228 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____startString, String_t* ____endString, bool ____ignoreCase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = ____ignoreCase;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 5;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 4;
	}

IL_000d:
	{
		V_0 = G_B3_0;
		String_t* L_1 = ____startString;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ____startString;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		G_B6_0 = L_3;
		goto IL_0020;
	}

IL_001f:
	{
		G_B6_0 = 0;
	}

IL_0020:
	{
		V_1 = G_B6_0;
		String_t* L_4 = ____string;
		String_t* L_5 = ____startString;
		int32_t L_6 = V_0;
		NullCheck(L_4);
		int32_t L_7 = String_IndexOf_m1_488(L_4, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		String_t* L_8 = ____string;
		String_t* L_9 = ____endString;
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		NullCheck(L_8);
		int32_t L_13 = String_IndexOf_m1_489(L_8, L_9, ((int32_t)((int32_t)L_10+(int32_t)L_11)), L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_15 = V_3;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_4 = L_16;
		goto IL_0064;
	}

IL_0050:
	{
		int32_t L_17 = V_3;
		int32_t L_18 = V_2;
		int32_t L_19 = V_1;
		V_5 = ((int32_t)((int32_t)L_17-(int32_t)((int32_t)((int32_t)L_18+(int32_t)L_19))));
		String_t* L_20 = ____string;
		int32_t L_21 = V_2;
		int32_t L_22 = V_1;
		int32_t L_23 = V_5;
		NullCheck(L_20);
		String_t* L_24 = String_Substring_m1_455(L_20, ((int32_t)((int32_t)L_21+(int32_t)L_22)), L_23, /*hidden argument*/NULL);
		V_4 = L_24;
	}

IL_0064:
	{
		String_t* L_25 = V_4;
		return L_25;
	}
}
// System.Void VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::.ctor()
extern "C" void U3CTakeScreenshotU3Ec__Iterator3__ctor_m8_229 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CTakeScreenshotU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_230 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CTakeScreenshotU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m8_231 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t6_13_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_15036_MethodInfo_var;
extern "C" bool U3CTakeScreenshotU3Ec__Iterator3_MoveNext_m8_232 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t6_13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1876);
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		Action_1_Invoke_m1_15036_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484123);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_00a3;
	}

IL_0021:
	{
		WaitForEndOfFrame_t6_13 * L_2 = (WaitForEndOfFrame_t6_13 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t6_13_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m6_12(L_2, /*hidden argument*/NULL);
		__this->___U24current_3 = L_2;
		__this->___U24PC_2 = 1;
		goto IL_00a5;
	}

IL_0038:
	{
		int32_t L_3 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_5 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_155(L_5, L_3, L_4, /*hidden argument*/NULL);
		__this->___U3C_textureU3E__0_0 = L_5;
		Texture2D_t6_33 * L_6 = (__this->___U3C_textureU3E__0_0);
		int32_t L_7 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_9 = {0};
		Rect__ctor_m6_295(&L_9, (0.0f), (0.0f), (((float)((float)L_7))), (((float)((float)L_8))), /*hidden argument*/NULL);
		NullCheck(L_6);
		Texture2D_ReadPixels_m6_170(L_6, L_9, 0, 0, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_10 = (__this->___U3C_textureU3E__0_0);
		NullCheck(L_10);
		Texture2D_Apply_m6_169(L_10, /*hidden argument*/NULL);
		Action_1_t1_1902 * L_11 = (__this->____onCompletionHandler_1);
		if (!L_11)
		{
			goto IL_009c;
		}
	}
	{
		Action_1_t1_1902 * L_12 = (__this->____onCompletionHandler_1);
		Texture2D_t6_33 * L_13 = (__this->___U3C_textureU3E__0_0);
		NullCheck(L_12);
		Action_1_Invoke_m1_15036(L_12, L_13, /*hidden argument*/Action_1_Invoke_m1_15036_MethodInfo_var);
	}

IL_009c:
	{
		__this->___U24PC_2 = (-1);
	}

IL_00a3:
	{
		return 0;
	}

IL_00a5:
	{
		return 1;
	}
	// Dead block : IL_00a7: ldloc.1
}
// System.Void VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::Dispose()
extern "C" void U3CTakeScreenshotU3Ec__Iterator3_Dispose_m8_233 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CTakeScreenshotU3Ec__Iterator3_Reset_m8_234 (U3CTakeScreenshotU3Ec__Iterator3_t8_43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.String VoxelBusters.Utility.TextureExtensions::ToString(UnityEngine.Texture2D,VoxelBusters.Utility.TextureExtensions/EncodeTo)
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern "C" String_t* TextureExtensions_ToString_m8_235 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____img, int32_t ____encodeTo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	String_t* V_1 = {0};
	ByteU5BU5D_t1_109* G_B3_0 = {0};
	{
		int32_t L_0 = ____encodeTo;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Texture2D_t6_33 * L_1 = ____img;
		NullCheck(L_1);
		ByteU5BU5D_t1_109* L_2 = Texture2D_EncodeToJPG_m6_174(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		Texture2D_t6_33 * L_3 = ____img;
		NullCheck(L_3);
		ByteU5BU5D_t1_109* L_4 = Texture2D_EncodeToPNG_m6_172(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		String_t* L_6 = Convert_ToBase64String_m1_13445(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		String_t* L_7 = V_1;
		return L_7;
	}
}
// System.String VoxelBusters.Utility.TextureExtensions::Serialise(UnityEngine.Texture2D,VoxelBusters.Utility.TextureExtensions/EncodeTo)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5173;
extern Il2CppCodeGenString* _stringLiteral5174;
extern Il2CppCodeGenString* _stringLiteral5175;
extern "C" String_t* TextureExtensions_Serialise_m8_236 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____img, int32_t ____encodeTo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5173 = il2cpp_codegen_string_literal_from_index(5173);
		_stringLiteral5174 = il2cpp_codegen_string_literal_from_index(5174);
		_stringLiteral5175 = il2cpp_codegen_string_literal_from_index(5175);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	String_t* V_1 = {0};
	{
		Dictionary_2_t1_1903 * L_0 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_0;
		Object_t * L_1 = V_0;
		Texture2D_t6_33 * L_2 = ____img;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_1, _stringLiteral5173, L_5);
		Object_t * L_6 = V_0;
		Texture2D_t6_33 * L_7 = ____img;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_7);
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_6, _stringLiteral5174, L_10);
		Object_t * L_11 = V_0;
		Texture2D_t6_33 * L_12 = ____img;
		int32_t L_13 = ____encodeTo;
		String_t* L_14 = TextureExtensions_ToString_m8_235(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_11, _stringLiteral5175, L_14);
		Object_t * L_15 = V_0;
		String_t* L_16 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		String_t* L_17 = V_1;
		return L_17;
	}
}
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::Deserialise(System.String)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5173;
extern Il2CppCodeGenString* _stringLiteral5174;
extern Il2CppCodeGenString* _stringLiteral5175;
extern "C" Texture2D_t6_33 * TextureExtensions_Deserialise_m8_237 (Object_t * __this /* static, unused */, String_t* ____strImg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5173 = il2cpp_codegen_string_literal_from_index(5173);
		_stringLiteral5174 = il2cpp_codegen_string_literal_from_index(5174);
		_stringLiteral5175 = il2cpp_codegen_string_literal_from_index(5175);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	{
		String_t* L_0 = ____strImg;
		Object_t * L_1 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t1_1903 *)IsInstClass(L_1, Dictionary_2_t1_1903_il2cpp_TypeInfo_var));
		Object_t * L_2 = V_0;
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		Object_t * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_3, _stringLiteral5173);
		V_1 = ((*(int32_t*)((int32_t*)UnBox (L_4, Int32_t1_3_il2cpp_TypeInfo_var))));
		Object_t * L_5 = V_0;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_5, _stringLiteral5174);
		V_2 = ((*(int32_t*)((int32_t*)UnBox (L_6, Int32_t1_3_il2cpp_TypeInfo_var))));
		Object_t * L_7 = V_0;
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_7, _stringLiteral5175);
		V_3 = ((String_t*)IsInstSealed(L_8, String_t_il2cpp_TypeInfo_var));
		String_t* L_9 = V_3;
		int32_t L_10 = V_1;
		int32_t L_11 = V_2;
		Texture2D_t6_33 * L_12 = TextureExtensions_CreateTexture_m8_238(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_004e:
	{
		return (Texture2D_t6_33 *)NULL;
	}
}
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::CreateTexture(System.String,System.Int32,System.Int32)
extern TypeInfo* Convert_t1_1522_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern "C" Texture2D_t6_33 * TextureExtensions_CreateTexture_m8_238 (Object_t * __this /* static, unused */, String_t* ____texDataB64, int32_t ____width, int32_t ____height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t1_1522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	Texture2D_t6_33 * V_1 = {0};
	{
		String_t* L_0 = ____texDataB64;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1_1522_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = Convert_FromBase64String_m1_13441(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ____width;
		int32_t L_3 = ____height;
		Texture2D_t6_33 * L_4 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_155(L_4, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Texture2D_t6_33 * L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = V_0;
		NullCheck(L_5);
		Texture2D_LoadImage_m6_166(L_5, L_6, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_7 = V_1;
		return L_7;
	}
}
// System.Collections.IEnumerator VoxelBusters.Utility.TextureExtensions::TakeScreenshot(System.Action`1<UnityEngine.Texture2D>)
extern TypeInfo* U3CTakeScreenshotU3Ec__Iterator3_t8_43_il2cpp_TypeInfo_var;
extern "C" Object_t * TextureExtensions_TakeScreenshot_m8_239 (Object_t * __this /* static, unused */, Action_1_t1_1902 * ____onCompletionHandler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CTakeScreenshotU3Ec__Iterator3_t8_43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1961);
		s_Il2CppMethodIntialized = true;
	}
	U3CTakeScreenshotU3Ec__Iterator3_t8_43 * V_0 = {0};
	{
		U3CTakeScreenshotU3Ec__Iterator3_t8_43 * L_0 = (U3CTakeScreenshotU3Ec__Iterator3_t8_43 *)il2cpp_codegen_object_new (U3CTakeScreenshotU3Ec__Iterator3_t8_43_il2cpp_TypeInfo_var);
		U3CTakeScreenshotU3Ec__Iterator3__ctor_m8_229(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTakeScreenshotU3Ec__Iterator3_t8_43 * L_1 = V_0;
		Action_1_t1_1902 * L_2 = ____onCompletionHandler;
		NullCheck(L_1);
		L_1->____onCompletionHandler_1 = L_2;
		U3CTakeScreenshotU3Ec__Iterator3_t8_43 * L_3 = V_0;
		Action_1_t1_1902 * L_4 = ____onCompletionHandler;
		NullCheck(L_3);
		L_3->___U3CU24U3E_onCompletionHandler_4 = L_4;
		U3CTakeScreenshotU3Ec__Iterator3_t8_43 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::Rotate(UnityEngine.Texture2D,System.Single)
extern TypeInfo* Color32U5BU5D_t6_280_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern "C" Texture2D_t6_33 * TextureExtensions_Rotate_m8_240 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, float ____angle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color32U5BU5D_t6_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1962);
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	Vector2_t6_47  V_6 = {0};
	Vector2_t6_47  V_7 = {0};
	Vector2_t6_47  V_8 = {0};
	Vector2_t6_47  V_9 = {0};
	Vector2_t6_47  V_10 = {0};
	Color32U5BU5D_t6_280* V_11 = {0};
	Color32U5BU5D_t6_280* V_12 = {0};
	int32_t V_13 = 0;
	Vector2_t6_47  V_14 = {0};
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	Texture2D_t6_33 * V_17 = {0};
	{
		Texture2D_t6_33 * L_0 = ____inputTex;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		V_0 = L_1;
		Texture2D_t6_33 * L_2 = ____inputTex;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		V_1 = L_3;
		float L_4 = ____angle;
		V_4 = (fmodf(((float)((float)L_4-(float)(45.0f))), (360.0f)));
		float L_5 = V_4;
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		float L_6 = V_4;
		V_4 = ((float)((float)L_6+(float)(360.0f)));
	}

IL_0033:
	{
		float L_7 = V_4;
		V_5 = ((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)L_7/(float)(90.0f))))))+(int32_t)1));
		int32_t L_8 = V_5;
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_9 = V_5;
		if ((!(((uint32_t)L_9) == ((uint32_t)3))))
		{
			goto IL_0059;
		}
	}

IL_0050:
	{
		int32_t L_10 = V_1;
		V_2 = L_10;
		int32_t L_11 = V_0;
		V_3 = L_11;
		goto IL_005d;
	}

IL_0059:
	{
		int32_t L_12 = V_0;
		V_2 = L_12;
		int32_t L_13 = V_1;
		V_3 = L_13;
	}

IL_005d:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		Vector2__ctor_m6_215((&V_6), ((float)((float)(((float)((float)L_14)))*(float)(0.5f))), ((float)((float)(((float)((float)L_15)))*(float)(0.5f))), /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		int32_t L_17 = V_3;
		Vector2_t6_47  L_18 = {0};
		Vector2__ctor_m6_215(&L_18, ((float)((float)(((float)((float)((-L_16)))))*(float)(0.5f))), ((float)((float)(((float)((float)((-L_17)))))*(float)(0.5f))), /*hidden argument*/NULL);
		float L_19 = ____angle;
		Vector2_t6_47  L_20 = VectorExtensions_Rotate_m8_248(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		Vector2_t6_47  L_21 = V_6;
		Vector2_t6_47  L_22 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_7 = L_22;
		Vector2_t6_47  L_23 = Vector2_get_right_m6_228(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_24 = ____angle;
		Vector2_t6_47  L_25 = VectorExtensions_Rotate_m8_248(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		Vector2_t6_47  L_26 = Vector2_get_up_m6_227(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = ____angle;
		Vector2_t6_47  L_28 = VectorExtensions_Rotate_m8_248(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		V_9 = L_28;
		Vector2_t6_47  L_29 = V_7;
		V_10 = L_29;
		Texture2D_t6_33 * L_30 = ____inputTex;
		NullCheck(L_30);
		Color32U5BU5D_t6_280* L_31 = Texture2D_GetPixels32_m6_167(L_30, 0, /*hidden argument*/NULL);
		V_11 = L_31;
		Color32U5BU5D_t6_280* L_32 = V_11;
		NullCheck(L_32);
		V_12 = ((Color32U5BU5D_t6_280*)SZArrayNew(Color32U5BU5D_t6_280_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_32)->max_length))))));
		V_13 = 0;
		goto IL_0132;
	}

IL_00d4:
	{
		Vector2_t6_47  L_33 = V_10;
		V_14 = L_33;
		int32_t L_34 = V_13;
		int32_t L_35 = V_2;
		V_15 = ((int32_t)((int32_t)L_34*(int32_t)L_35));
		V_16 = 0;
		goto IL_0119;
	}

IL_00e6:
	{
		Color32U5BU5D_t6_280* L_36 = V_12;
		int32_t L_37 = V_15;
		int32_t L_38 = L_37;
		V_15 = ((int32_t)((int32_t)L_38+(int32_t)1));
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_38);
		Texture2D_t6_33 * L_39 = ____inputTex;
		Color32U5BU5D_t6_280* L_40 = V_11;
		Vector2_t6_47  L_41 = V_14;
		Color_t6_40  L_42 = TextureExtensions_GetPixel_m8_241(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		Color32_t6_49  L_43 = Color32_op_Implicit_m6_283(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		(*(Color32_t6_49 *)((Color32_t6_49 *)(Color32_t6_49 *)SZArrayLdElema(L_36, L_38, sizeof(Color32_t6_49 )))) = L_43;
		Vector2_t6_47  L_44 = V_14;
		Vector2_t6_47  L_45 = V_8;
		Vector2_t6_47  L_46 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		V_14 = L_46;
		int32_t L_47 = V_16;
		V_16 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0119:
	{
		int32_t L_48 = V_16;
		int32_t L_49 = V_2;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_00e6;
		}
	}
	{
		Vector2_t6_47  L_50 = V_10;
		Vector2_t6_47  L_51 = V_9;
		Vector2_t6_47  L_52 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		V_10 = L_52;
		int32_t L_53 = V_13;
		V_13 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_0132:
	{
		int32_t L_54 = V_13;
		int32_t L_55 = V_3;
		if ((((int32_t)L_54) < ((int32_t)L_55)))
		{
			goto IL_00d4;
		}
	}
	{
		int32_t L_56 = V_2;
		int32_t L_57 = V_3;
		Texture2D_t6_33 * L_58 = ____inputTex;
		NullCheck(L_58);
		int32_t L_59 = Texture2D_get_format_m6_158(L_58, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_60 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_156(L_60, L_56, L_57, L_59, 0, /*hidden argument*/NULL);
		V_17 = L_60;
		Texture2D_t6_33 * L_61 = V_17;
		Color32U5BU5D_t6_280* L_62 = V_12;
		NullCheck(L_61);
		Texture2D_SetPixels32_m6_165(L_61, L_62, 0, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_63 = V_17;
		NullCheck(L_63);
		Texture2D_Apply_m6_169(L_63, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_64 = V_17;
		return L_64;
	}
}
// UnityEngine.Color VoxelBusters.Utility.TextureExtensions::GetPixel(UnityEngine.Texture2D,UnityEngine.Color32[],UnityEngine.Vector2)
extern "C" Color_t6_40  TextureExtensions_GetPixel_m8_241 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, Color32U5BU5D_t6_280* ____pixels, Vector2_t6_47  ____coordinate, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		float L_0 = ((&____coordinate)->___x_1);
		V_0 = (((int32_t)((int32_t)L_0)));
		float L_1 = ((&____coordinate)->___y_2);
		V_1 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_0;
		Texture2D_t6_33 * L_3 = ____inputTex;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_3);
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}

IL_0025:
	{
		Color_t6_40  L_6 = Color_get_clear_m6_278(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}

IL_002b:
	{
		int32_t L_7 = V_1;
		Texture2D_t6_33 * L_8 = ____inputTex;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_8);
		if ((((int32_t)L_7) >= ((int32_t)L_9)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_0044;
		}
	}

IL_003e:
	{
		Color_t6_40  L_11 = Color_get_clear_m6_278(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_11;
	}

IL_0044:
	{
		Color32U5BU5D_t6_280* L_12 = ____pixels;
		int32_t L_13 = V_1;
		Texture2D_t6_33 * L_14 = ____inputTex;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_14);
		int32_t L_16 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)((int32_t)((int32_t)L_13*(int32_t)L_15))+(int32_t)L_16)));
		Color_t6_40  L_17 = Color32_op_Implicit_m6_284(NULL /*static, unused*/, (*(Color32_t6_49 *)((Color32_t6_49 *)(Color32_t6_49 *)SZArrayLdElema(L_12, ((int32_t)((int32_t)((int32_t)((int32_t)L_13*(int32_t)L_15))+(int32_t)L_16)), sizeof(Color32_t6_49 )))), /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::MirrorTexture(UnityEngine.Texture2D,System.Boolean,System.Boolean)
extern TypeInfo* Color32U5BU5D_t6_280_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5176;
extern Il2CppCodeGenString* _stringLiteral5177;
extern "C" Texture2D_t6_33 * TextureExtensions_MirrorTexture_m8_242 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, bool ____mirrorHorizontally, bool ____mirrorVertically, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color32U5BU5D_t6_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1962);
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5176 = il2cpp_codegen_string_literal_from_index(5176);
		_stringLiteral5177 = il2cpp_codegen_string_literal_from_index(5177);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Color32U5BU5D_t6_280* V_4 = {0};
	Color32U5BU5D_t6_280* V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	Texture2D_t6_33 * V_12 = {0};
	int32_t G_B4_0 = 0;
	int32_t G_B8_0 = 0;
	{
		Texture2D_t6_33 * L_0 = ____inputTex;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2-(int32_t)1));
		Texture2D_t6_33 * L_3 = ____inputTex;
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_3);
		V_2 = L_4;
		int32_t L_5 = V_2;
		V_3 = ((int32_t)((int32_t)L_5-(int32_t)1));
		Texture2D_t6_33 * L_6 = ____inputTex;
		NullCheck(L_6);
		Color32U5BU5D_t6_280* L_7 = Texture2D_GetPixels32_m6_167(L_6, 0, /*hidden argument*/NULL);
		V_4 = L_7;
		Color32U5BU5D_t6_280* L_8 = V_4;
		NullCheck(L_8);
		V_5 = ((Color32U5BU5D_t6_280*)SZArrayNew(Color32U5BU5D_t6_280_il2cpp_TypeInfo_var, (((int32_t)((int32_t)(((Array_t *)L_8)->max_length))))));
		V_6 = 0;
		goto IL_00a4;
	}

IL_0032:
	{
		int32_t L_9 = V_6;
		int32_t L_10 = V_0;
		V_7 = ((int32_t)((int32_t)L_9*(int32_t)L_10));
		bool L_11 = ____mirrorVertically;
		if (!L_11)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_6;
		G_B4_0 = ((int32_t)((int32_t)L_12-(int32_t)L_13));
		goto IL_0049;
	}

IL_0047:
	{
		int32_t L_14 = V_6;
		G_B4_0 = L_14;
	}

IL_0049:
	{
		V_8 = G_B4_0;
		int32_t L_15 = V_8;
		int32_t L_16 = V_0;
		V_9 = ((int32_t)((int32_t)L_15*(int32_t)L_16));
		V_10 = 0;
		goto IL_0096;
	}

IL_0059:
	{
		bool L_17 = ____mirrorHorizontally;
		if (!L_17)
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_18 = V_1;
		int32_t L_19 = V_10;
		G_B8_0 = ((int32_t)((int32_t)L_18-(int32_t)L_19));
		goto IL_006a;
	}

IL_0068:
	{
		int32_t L_20 = V_10;
		G_B8_0 = L_20;
	}

IL_006a:
	{
		V_11 = G_B8_0;
		Color32U5BU5D_t6_280* L_21 = V_5;
		int32_t L_22 = V_9;
		int32_t L_23 = V_11;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)L_22+(int32_t)L_23)));
		Color32U5BU5D_t6_280* L_24 = V_4;
		int32_t L_25 = V_7;
		int32_t L_26 = L_25;
		V_7 = ((int32_t)((int32_t)L_26+(int32_t)1));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_26);
		(*(Color32_t6_49 *)((Color32_t6_49 *)(Color32_t6_49 *)SZArrayLdElema(L_21, ((int32_t)((int32_t)L_22+(int32_t)L_23)), sizeof(Color32_t6_49 )))) = (*(Color32_t6_49 *)((Color32_t6_49 *)(Color32_t6_49 *)SZArrayLdElema(L_24, L_26, sizeof(Color32_t6_49 ))));
		int32_t L_27 = V_10;
		V_10 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_28 = V_10;
		int32_t L_29 = V_0;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_31 = V_6;
		int32_t L_32 = V_2;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_33 = V_0;
		int32_t L_34 = V_2;
		Texture2D_t6_33 * L_35 = ____inputTex;
		NullCheck(L_35);
		int32_t L_36 = Texture2D_get_format_m6_158(L_35, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_37 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_156(L_37, L_33, L_34, L_36, 0, /*hidden argument*/NULL);
		V_12 = L_37;
		Texture2D_t6_33 * L_38 = V_12;
		Color32U5BU5D_t6_280* L_39 = V_5;
		NullCheck(L_38);
		Texture2D_SetPixels32_m6_165(L_38, L_39, 0, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_40 = V_12;
		NullCheck(L_40);
		Texture2D_Apply_m6_169(L_40, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_41 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		ArrayElementTypeCheck (L_41, _stringLiteral5176);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5176;
		ObjectU5BU5D_t1_272* L_42 = L_41;
		Texture2D_t6_33 * L_43 = V_12;
		NullCheck(L_43);
		int32_t L_44 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_43);
		int32_t L_45 = L_44;
		Object_t * L_46 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 1);
		ArrayElementTypeCheck (L_42, L_46);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 1, sizeof(Object_t *))) = (Object_t *)L_46;
		ObjectU5BU5D_t1_272* L_47 = L_42;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 2);
		ArrayElementTypeCheck (L_47, _stringLiteral5177);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_47, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5177;
		ObjectU5BU5D_t1_272* L_48 = L_47;
		Texture2D_t6_33 * L_49 = V_12;
		NullCheck(L_49);
		int32_t L_50 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_49);
		int32_t L_51 = L_50;
		Object_t * L_52 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_51);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 3);
		ArrayElementTypeCheck (L_48, L_52);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, 3, sizeof(Object_t *))) = (Object_t *)L_52;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m1_562(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_54 = V_12;
		return L_54;
	}
}
// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions::Scale(UnityEngine.Texture2D,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* ColorU5BU5D_t6_281_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern "C" Texture2D_t6_33 * TextureExtensions_Scale_m8_243 (Object_t * __this /* static, unused */, Texture2D_t6_33 * ____inputTex, float ____scaleFactor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		ColorU5BU5D_t6_281_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1963);
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ColorU5BU5D_t6_281* V_2 = {0};
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	Texture2D_t6_33 * V_8 = {0};
	{
		float L_0 = ____scaleFactor;
		if ((!(((float)L_0) == ((float)(1.0f)))))
		{
			goto IL_000d;
		}
	}
	{
		Texture2D_t6_33 * L_1 = ____inputTex;
		return L_1;
	}

IL_000d:
	{
		float L_2 = ____scaleFactor;
		if ((!(((float)L_2) == ((float)(0.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		Texture2D_t6_33 * L_3 = Texture2D_get_blackTexture_m6_160(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001e:
	{
		Texture2D_t6_33 * L_4 = ____inputTex;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		float L_6 = ____scaleFactor;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m6_437(NULL /*static, unused*/, ((float)((float)(((float)((float)L_5)))*(float)L_6)), /*hidden argument*/NULL);
		V_0 = L_7;
		Texture2D_t6_33 * L_8 = ____inputTex;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_8);
		float L_10 = ____scaleFactor;
		int32_t L_11 = Mathf_RoundToInt_m6_437(NULL /*static, unused*/, ((float)((float)(((float)((float)L_9)))*(float)L_10)), /*hidden argument*/NULL);
		V_1 = L_11;
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		V_2 = ((ColorU5BU5D_t6_281*)SZArrayNew(ColorU5BU5D_t6_281_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_12*(int32_t)L_13))));
		V_3 = 0;
		goto IL_00a0;
	}

IL_004c:
	{
		int32_t L_14 = V_3;
		int32_t L_15 = V_1;
		V_4 = ((float)((float)(((float)((float)L_14)))/(float)((float)((float)(((float)((float)L_15)))-(float)(1.0f)))));
		int32_t L_16 = V_3;
		int32_t L_17 = V_0;
		V_5 = ((int32_t)((int32_t)L_16*(int32_t)L_17));
		V_6 = 0;
		goto IL_0094;
	}

IL_0066:
	{
		int32_t L_18 = V_6;
		int32_t L_19 = V_0;
		V_7 = ((float)((float)(((float)((float)L_18)))/(float)((float)((float)(((float)((float)L_19)))-(float)(1.0f)))));
		ColorU5BU5D_t6_281* L_20 = V_2;
		int32_t L_21 = V_5;
		int32_t L_22 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)((int32_t)L_21+(int32_t)L_22)));
		Texture2D_t6_33 * L_23 = ____inputTex;
		float L_24 = V_7;
		float L_25 = V_4;
		NullCheck(L_23);
		Color_t6_40  L_26 = Texture2D_GetPixelBilinear_m6_161(L_23, L_24, L_25, /*hidden argument*/NULL);
		(*(Color_t6_40 *)((Color_t6_40 *)(Color_t6_40 *)SZArrayLdElema(L_20, ((int32_t)((int32_t)L_21+(int32_t)L_22)), sizeof(Color_t6_40 )))) = L_26;
		int32_t L_27 = V_6;
		V_6 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0094:
	{
		int32_t L_28 = V_6;
		int32_t L_29 = V_0;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_31 = V_3;
		int32_t L_32 = V_1;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		Texture2D_t6_33 * L_35 = ____inputTex;
		NullCheck(L_35);
		int32_t L_36 = Texture2D_get_format_m6_158(L_35, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_37 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_156(L_37, L_33, L_34, L_36, 0, /*hidden argument*/NULL);
		V_8 = L_37;
		Texture2D_t6_33 * L_38 = V_8;
		ColorU5BU5D_t6_281* L_39 = V_2;
		NullCheck(L_38);
		Texture2D_SetPixels_m6_162(L_38, L_39, 0, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_40 = V_8;
		NullCheck(L_40);
		Texture2D_Apply_m6_169(L_40, /*hidden argument*/NULL);
		Texture2D_t6_33 * L_41 = V_8;
		return L_41;
	}
}
// System.String VoxelBusters.Utility.TransformExtensions::GetPath(UnityEngine.Transform)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral740;
extern "C" String_t* TransformExtensions_GetPath_m8_244 (Object_t * __this /* static, unused */, Transform_t6_65 * ____transform, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral740 = il2cpp_codegen_string_literal_from_index(740);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t6_65 * V_0 = {0};
	{
		Transform_t6_65 * L_0 = ____transform;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000e:
	{
		Transform_t6_65 * L_2 = ____transform;
		NullCheck(L_2);
		Transform_t6_65 * L_3 = Transform_get_parent_m6_771(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t6_65 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0032;
		}
	}
	{
		Transform_t6_65 * L_6 = ____transform;
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m6_708(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral740, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0032:
	{
		Transform_t6_65 * L_9 = V_0;
		String_t* L_10 = TransformExtensions_GetPath_m8_244(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Transform_t6_65 * L_11 = ____transform;
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m6_708(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_560(NULL /*static, unused*/, L_10, _stringLiteral740, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void VoxelBusters.Utility.TypeExtensions::.cctor()
extern const Il2CppType* Object_t_0_0_0_var;
extern const Il2CppType* DBNull_t1_1523_0_0_0_var;
extern const Il2CppType* Boolean_t1_20_0_0_0_var;
extern const Il2CppType* Char_t1_15_0_0_0_var;
extern const Il2CppType* SByte_t1_12_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* Int16_t1_13_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern const Il2CppType* Int32_t1_3_0_0_0_var;
extern const Il2CppType* UInt32_t1_8_0_0_0_var;
extern const Il2CppType* Int64_t1_7_0_0_0_var;
extern const Il2CppType* UInt64_t1_10_0_0_0_var;
extern const Il2CppType* Single_t1_17_0_0_0_var;
extern const Il2CppType* Double_t1_18_0_0_0_var;
extern const Il2CppType* Decimal_t1_19_0_0_0_var;
extern const Il2CppType* DateTime_t1_150_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* TypeExtensions_t8_46_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TypeExtensions__cctor_m8_245 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_0_0_0_var = il2cpp_codegen_type_from_index(0);
		DBNull_t1_1523_0_0_0_var = il2cpp_codegen_type_from_index(1079);
		Boolean_t1_20_0_0_0_var = il2cpp_codegen_type_from_index(54);
		Char_t1_15_0_0_0_var = il2cpp_codegen_type_from_index(12);
		SByte_t1_12_0_0_0_var = il2cpp_codegen_type_from_index(27);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		Int16_t1_13_0_0_0_var = il2cpp_codegen_type_from_index(28);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Int32_t1_3_0_0_0_var = il2cpp_codegen_type_from_index(11);
		UInt32_t1_8_0_0_0_var = il2cpp_codegen_type_from_index(23);
		Int64_t1_7_0_0_0_var = il2cpp_codegen_type_from_index(21);
		UInt64_t1_10_0_0_0_var = il2cpp_codegen_type_from_index(24);
		Single_t1_17_0_0_0_var = il2cpp_codegen_type_from_index(47);
		Double_t1_18_0_0_0_var = il2cpp_codegen_type_from_index(48);
		Decimal_t1_19_0_0_0_var = il2cpp_codegen_type_from_index(50);
		DateTime_t1_150_0_0_0_var = il2cpp_codegen_type_from_index(186);
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		TypeExtensions_t8_46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1964);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, ((int32_t)19)));
		TypeU5BU5D_t1_31* L_0 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 1, sizeof(Type_t *))) = (Type_t *)L_1;
		TypeU5BU5D_t1_31* L_2 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(DBNull_t1_1523_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 2, sizeof(Type_t *))) = (Type_t *)L_3;
		TypeU5BU5D_t1_31* L_4 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Boolean_t1_20_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 3, sizeof(Type_t *))) = (Type_t *)L_5;
		TypeU5BU5D_t1_31* L_6 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_7 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Char_t1_15_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_6, 4, sizeof(Type_t *))) = (Type_t *)L_7;
		TypeU5BU5D_t1_31* L_8 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(SByte_t1_12_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, L_9);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_8, 5, sizeof(Type_t *))) = (Type_t *)L_9;
		TypeU5BU5D_t1_31* L_10 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_11 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 6);
		ArrayElementTypeCheck (L_10, L_11);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_10, 6, sizeof(Type_t *))) = (Type_t *)L_11;
		TypeU5BU5D_t1_31* L_12 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_13 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int16_t1_13_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_13);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_12, 7, sizeof(Type_t *))) = (Type_t *)L_13;
		TypeU5BU5D_t1_31* L_14 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_15 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 8);
		ArrayElementTypeCheck (L_14, L_15);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_14, 8, sizeof(Type_t *))) = (Type_t *)L_15;
		TypeU5BU5D_t1_31* L_16 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_17 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int32_t1_3_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, L_17);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_16, ((int32_t)9), sizeof(Type_t *))) = (Type_t *)L_17;
		TypeU5BU5D_t1_31* L_18 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_19 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UInt32_t1_8_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)10));
		ArrayElementTypeCheck (L_18, L_19);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_18, ((int32_t)10), sizeof(Type_t *))) = (Type_t *)L_19;
		TypeU5BU5D_t1_31* L_20 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_21 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int64_t1_7_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)11));
		ArrayElementTypeCheck (L_20, L_21);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_20, ((int32_t)11), sizeof(Type_t *))) = (Type_t *)L_21;
		TypeU5BU5D_t1_31* L_22 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_23 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UInt64_t1_10_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)12));
		ArrayElementTypeCheck (L_22, L_23);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_22, ((int32_t)12), sizeof(Type_t *))) = (Type_t *)L_23;
		TypeU5BU5D_t1_31* L_24 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_25 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Single_t1_17_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)13));
		ArrayElementTypeCheck (L_24, L_25);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_24, ((int32_t)13), sizeof(Type_t *))) = (Type_t *)L_25;
		TypeU5BU5D_t1_31* L_26 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_27 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Double_t1_18_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)14));
		ArrayElementTypeCheck (L_26, L_27);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_26, ((int32_t)14), sizeof(Type_t *))) = (Type_t *)L_27;
		TypeU5BU5D_t1_31* L_28 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_29 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Decimal_t1_19_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)15));
		ArrayElementTypeCheck (L_28, L_29);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_28, ((int32_t)15), sizeof(Type_t *))) = (Type_t *)L_29;
		TypeU5BU5D_t1_31* L_30 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_31 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(DateTime_t1_150_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)16));
		ArrayElementTypeCheck (L_30, L_31);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_30, ((int32_t)16), sizeof(Type_t *))) = (Type_t *)L_31;
		TypeU5BU5D_t1_31* L_32 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		Type_t * L_33 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)18));
		ArrayElementTypeCheck (L_32, L_33);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_32, ((int32_t)18), sizeof(Type_t *))) = (Type_t *)L_33;
		return;
	}
}
// System.Object VoxelBusters.Utility.TypeExtensions::DefaultValue(System.Type)
extern "C" Object_t * TypeExtensions_DefaultValue_m8_246 (Object_t * __this /* static, unused */, Type_t * ____type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ____type;
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(147 /* System.Boolean System.Type::get_IsValueType() */, L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Type_t * L_2 = ____type;
		Object_t * L_3 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		return NULL;
	}
}
// System.Type VoxelBusters.Utility.TypeExtensions::GetTypeFromTypeCode(System.TypeCode)
extern TypeInfo* TypeExtensions_t8_46_il2cpp_TypeInfo_var;
extern "C" Type_t * TypeExtensions_GetTypeFromTypeCode_m8_247 (Object_t * __this /* static, unused */, int32_t ____typeCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeExtensions_t8_46_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1964);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TypeExtensions_t8_46_il2cpp_TypeInfo_var);
		TypeU5BU5D_t1_31* L_0 = ((TypeExtensions_t8_46_StaticFields*)TypeExtensions_t8_46_il2cpp_TypeInfo_var->static_fields)->___typeCodesToType_0;
		int32_t L_1 = ____typeCode;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return (*(Type_t **)(Type_t **)SZArrayLdElema(L_0, L_2, sizeof(Type_t *)));
	}
}
// UnityEngine.Vector2 VoxelBusters.Utility.VectorExtensions::Rotate(UnityEngine.Vector2,System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  VectorExtensions_Rotate_m8_248 (Object_t * __this /* static, unused */, Vector2_t6_47  ____vec, float ____angleDeg, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector2_t6_47  V_3 = {0};
	{
		float L_0 = ____angleDeg;
		V_0 = ((float)((float)(0.0174532924f)*(float)L_0));
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = cosf(L_1);
		V_1 = L_2;
		float L_3 = V_0;
		float L_4 = sinf(L_3);
		V_2 = L_4;
		Vector2_t6_47  L_5 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = ((&____vec)->___x_1);
		float L_7 = V_1;
		float L_8 = ((&____vec)->___y_2);
		float L_9 = V_2;
		(&V_3)->___x_1 = ((float)((float)((float)((float)L_6*(float)L_7))-(float)((float)((float)L_8*(float)L_9))));
		float L_10 = ((&____vec)->___x_1);
		float L_11 = V_2;
		float L_12 = ((&____vec)->___y_2);
		float L_13 = V_1;
		(&V_3)->___y_2 = ((float)((float)((float)((float)L_10*(float)L_11))+(float)((float)((float)L_12*(float)L_13))));
		Vector2_t6_47  L_14 = V_3;
		return L_14;
	}
}
// System.Void VoxelBusters.Utility.FileOperations::.ctor()
extern "C" void FileOperations__ctor_m8_249 (FileOperations_t8_48 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.FileOperations::Delete(System.String)
extern "C" void FileOperations_Delete_m8_250 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method)
{
	{
		String_t* L_0 = ____filePath;
		File_Delete_m1_4856(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.FileOperations::Move(System.String,System.String)
extern "C" void FileOperations_Move_m8_251 (Object_t * __this /* static, unused */, String_t* ____sourcePath, String_t* ____destinationPath, const MethodInfo* method)
{
	{
		String_t* L_0 = ____sourcePath;
		String_t* L_1 = ____destinationPath;
		File_Move_m1_4865(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean VoxelBusters.Utility.FileOperations::Exists(System.String)
extern "C" bool FileOperations_Exists_m8_252 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method)
{
	{
		String_t* L_0 = ____filePath;
		bool L_1 = File_Exists_m1_4857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] VoxelBusters.Utility.FileOperations::ReadAllBytes(System.String)
extern "C" ByteU5BU5D_t1_109* FileOperations_ReadAllBytes_m8_253 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method)
{
	{
		String_t* L_0 = ____filePath;
		ByteU5BU5D_t1_109* L_1 = File_ReadAllBytes_m1_4882(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.FileOperations::WriteAllBytes(System.String,System.Byte[])
extern "C" void FileOperations_WriteAllBytes_m8_254 (Object_t * __this /* static, unused */, String_t* ____filePath, ByteU5BU5D_t1_109* ____bytes, const MethodInfo* method)
{
	{
		String_t* L_0 = ____filePath;
		ByteU5BU5D_t1_109* L_1 = ____bytes;
		File_WriteAllBytes_m1_4888(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.IO.StreamWriter VoxelBusters.Utility.FileOperations::CreateText(System.String)
extern "C" StreamWriter_t1_448 * FileOperations_CreateText_m8_255 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method)
{
	{
		String_t* L_0 = ____filePath;
		StreamWriter_t1_448 * L_1 = File_CreateText_m1_4855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String VoxelBusters.Utility.FileOperations::ReadAllText(System.String)
extern "C" String_t* FileOperations_ReadAllText_m8_256 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method)
{
	{
		String_t* L_0 = ____filePath;
		String_t* L_1 = File_ReadAllText_m1_4886(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void VoxelBusters.Utility.FileOperations::Rename(System.String,System.String)
extern TypeInfo* Path_t1_437_il2cpp_TypeInfo_var;
extern "C" void FileOperations_Rename_m8_257 (Object_t * __this /* static, unused */, String_t* ____filePath, String_t* ____newFileName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Path_t1_437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		String_t* L_0 = ____filePath;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t1_437_il2cpp_TypeInfo_var);
		String_t* L_1 = Path_GetFileName_m1_5127(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ____filePath;
		String_t* L_3 = V_0;
		String_t* L_4 = ____newFileName;
		NullCheck(L_2);
		String_t* L_5 = String_Replace_m1_536(L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = ____filePath;
		bool L_7 = File_Exists_m1_4857(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_8 = V_1;
		bool L_9 = File_Exists_m1_4857(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_10 = V_1;
		File_Delete_m1_4856(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_002c:
	{
		String_t* L_11 = ____filePath;
		String_t* L_12 = V_1;
		File_Move_m1_4865(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.JSONDemo::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5178;
extern "C" void JSONDemo__ctor_m8_258 (JSONDemo_t8_49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		_stringLiteral5178 = il2cpp_codegen_string_literal_from_index(5178);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_JSONObject_2 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_result_3 = L_1;
		StringU5BU5D_t1_238* L_2 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, _stringLiteral5178);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5178;
		ArrayList_t1_170 * L_3 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3050(L_3, (Object_t *)(Object_t *)L_2, /*hidden argument*/NULL);
		__this->___m_GUIButtons_4 = L_3;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.JSONDemo::OnGUI()
extern TypeInfo* Action_1_t1_1917_il2cpp_TypeInfo_var;
extern const MethodInfo* JSONDemo_OnGUIButtonPressed_m8_260_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15038_MethodInfo_var;
extern "C" void JSONDemo_OnGUI_m8_259 (JSONDemo_t8_49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t1_1917_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1965);
		JSONDemo_OnGUIButtonPressed_m8_260_MethodInfo_var = il2cpp_codegen_method_info_from_index(477);
		Action_1__ctor_m1_15038_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484126);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___m_JSONObject_2);
		Rect_t6_51  L_1 = {0};
		Rect__ctor_m6_295(&L_1, (0.05f), (0.01f), (0.9f), (0.44f), /*hidden argument*/NULL);
		String_t* L_2 = GUIExtensions_TextArea_m8_815(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_JSONObject_2 = L_2;
		ArrayList_t1_170 * L_3 = (__this->___m_GUIButtons_4);
		IntPtr_t L_4 = { (void*)JSONDemo_OnGUIButtonPressed_m8_260_MethodInfo_var };
		Action_1_t1_1917 * L_5 = (Action_1_t1_1917 *)il2cpp_codegen_object_new (Action_1_t1_1917_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15038(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m1_15038_MethodInfo_var);
		Rect_t6_51  L_6 = {0};
		Rect__ctor_m6_295(&L_6, (0.05f), (0.45f), (0.9f), (0.1f), /*hidden argument*/NULL);
		GUIExtensions_Buttons_m8_816(NULL /*static, unused*/, L_3, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_7 = (__this->___m_result_3);
		Rect_t6_51  L_8 = {0};
		Rect__ctor_m6_295(&L_8, (0.05f), (0.55f), (0.9f), (0.44f), /*hidden argument*/NULL);
		GUIExtensions_TextArea_m8_815(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.JSONDemo::OnGUIButtonPressed(System.String)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5179;
extern Il2CppCodeGenString* _stringLiteral5180;
extern Il2CppCodeGenString* _stringLiteral5181;
extern Il2CppCodeGenString* _stringLiteral5182;
extern "C" void JSONDemo_OnGUIButtonPressed_m8_260 (JSONDemo_t8_49 * __this, String_t* ____buttonName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral5179 = il2cpp_codegen_string_literal_from_index(5179);
		_stringLiteral5180 = il2cpp_codegen_string_literal_from_index(5180);
		_stringLiteral5181 = il2cpp_codegen_string_literal_from_index(5181);
		_stringLiteral5182 = il2cpp_codegen_string_literal_from_index(5182);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	{
		V_0 = 0;
		String_t* L_0 = (__this->___m_JSONObject_2);
		Object_t * L_1 = JSONUtility_FromJSON_m8_299(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_004f;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_3 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral5179);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5179;
		ObjectU5BU5D_t1_272* L_4 = L_3;
		Object_t * L_5 = V_1;
		String_t* L_6 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_272* L_7 = L_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral5180);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5180;
		ObjectU5BU5D_t1_272* L_8 = L_7;
		Object_t * L_9 = V_1;
		NullCheck(L_9);
		Type_t * L_10 = Object_GetType_m1_5(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_562(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___m_result_3 = L_11;
		goto IL_0088;
	}

IL_004f:
	{
		ObjectU5BU5D_t1_272* L_12 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		ArrayElementTypeCheck (L_12, _stringLiteral5181);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5181;
		ObjectU5BU5D_t1_272* L_13 = L_12;
		int32_t L_14 = V_0;
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_272* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, _stringLiteral5182);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5182;
		ObjectU5BU5D_t1_272* L_18 = L_17;
		String_t* L_19 = (__this->___m_JSONObject_2);
		int32_t L_20 = V_0;
		NullCheck(L_19);
		String_t* L_21 = String_Substring_m1_454(L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 3, sizeof(Object_t *))) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_562(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		__this->___m_result_3 = L_22;
	}

IL_0088:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::.ctor()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* ArrayList_t1_170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3487;
extern Il2CppCodeGenString* _stringLiteral5183;
extern Il2CppCodeGenString* _stringLiteral4199;
extern Il2CppCodeGenString* _stringLiteral5184;
extern Il2CppCodeGenString* _stringLiteral5185;
extern Il2CppCodeGenString* _stringLiteral5186;
extern Il2CppCodeGenString* _stringLiteral5187;
extern Il2CppCodeGenString* _stringLiteral2818;
extern "C" void PlistDemo__ctor_m8_261 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		ArrayList_t1_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		_stringLiteral3487 = il2cpp_codegen_string_literal_from_index(3487);
		_stringLiteral5183 = il2cpp_codegen_string_literal_from_index(5183);
		_stringLiteral4199 = il2cpp_codegen_string_literal_from_index(4199);
		_stringLiteral5184 = il2cpp_codegen_string_literal_from_index(5184);
		_stringLiteral5185 = il2cpp_codegen_string_literal_from_index(5185);
		_stringLiteral5186 = il2cpp_codegen_string_literal_from_index(5186);
		_stringLiteral5187 = il2cpp_codegen_string_literal_from_index(5187);
		_stringLiteral2818 = il2cpp_codegen_string_literal_from_index(2818);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_input_3 = _stringLiteral3487;
		__this->___m_keyPath_4 = _stringLiteral5183;
		__this->___m_result_5 = _stringLiteral4199;
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral5184);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral5184;
		StringU5BU5D_t1_238* L_1 = L_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, _stringLiteral5185);
		*((String_t**)(String_t**)SZArrayLdElema(L_1, 1, sizeof(String_t*))) = (String_t*)_stringLiteral5185;
		StringU5BU5D_t1_238* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		ArrayElementTypeCheck (L_2, _stringLiteral5186);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 2, sizeof(String_t*))) = (String_t*)_stringLiteral5186;
		StringU5BU5D_t1_238* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 3);
		ArrayElementTypeCheck (L_3, _stringLiteral5187);
		*((String_t**)(String_t**)SZArrayLdElema(L_3, 3, sizeof(String_t*))) = (String_t*)_stringLiteral5187;
		StringU5BU5D_t1_238* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 4);
		ArrayElementTypeCheck (L_4, _stringLiteral2818);
		*((String_t**)(String_t**)SZArrayLdElema(L_4, 4, sizeof(String_t*))) = (String_t*)_stringLiteral2818;
		ArrayList_t1_170 * L_5 = (ArrayList_t1_170 *)il2cpp_codegen_object_new (ArrayList_t1_170_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1_3050(L_5, (Object_t *)(Object_t *)L_4, /*hidden argument*/NULL);
		__this->___m_GUIButtons_6 = L_5;
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::OnGUI()
extern TypeInfo* Action_1_t1_1917_il2cpp_TypeInfo_var;
extern const MethodInfo* PlistDemo_OnGUIButtonPressed_m8_263_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1_15038_MethodInfo_var;
extern "C" void PlistDemo_OnGUI_m8_262 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t1_1917_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1965);
		PlistDemo_OnGUIButtonPressed_m8_263_MethodInfo_var = il2cpp_codegen_method_info_from_index(479);
		Action_1__ctor_m1_15038_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484126);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___m_input_3);
		Rect_t6_51  L_1 = {0};
		Rect__ctor_m6_295(&L_1, (0.05f), (0.01f), (0.9f), (0.39f), /*hidden argument*/NULL);
		String_t* L_2 = GUIExtensions_TextArea_m8_815(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_input_3 = L_2;
		String_t* L_3 = (__this->___m_keyPath_4);
		Rect_t6_51  L_4 = {0};
		Rect__ctor_m6_295(&L_4, (0.05f), (0.4f), (0.9f), (0.05f), /*hidden argument*/NULL);
		String_t* L_5 = GUIExtensions_TextArea_m8_815(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->___m_keyPath_4 = L_5;
		ArrayList_t1_170 * L_6 = (__this->___m_GUIButtons_6);
		IntPtr_t L_7 = { (void*)PlistDemo_OnGUIButtonPressed_m8_263_MethodInfo_var };
		Action_1_t1_1917 * L_8 = (Action_1_t1_1917 *)il2cpp_codegen_object_new (Action_1_t1_1917_il2cpp_TypeInfo_var);
		Action_1__ctor_m1_15038(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m1_15038_MethodInfo_var);
		Rect_t6_51  L_9 = {0};
		Rect__ctor_m6_295(&L_9, (0.05f), (0.45f), (0.9f), (0.25f), /*hidden argument*/NULL);
		GUIExtensions_Buttons_m8_816(NULL /*static, unused*/, L_6, L_8, L_9, /*hidden argument*/NULL);
		String_t* L_10 = (__this->___m_result_5);
		Rect_t6_51  L_11 = {0};
		Rect__ctor_m6_295(&L_11, (0.05f), (0.7f), (0.9f), (0.29f), /*hidden argument*/NULL);
		GUIExtensions_TextArea_m8_815(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::OnGUIButtonPressed(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5184;
extern Il2CppCodeGenString* _stringLiteral5185;
extern Il2CppCodeGenString* _stringLiteral5186;
extern Il2CppCodeGenString* _stringLiteral5187;
extern Il2CppCodeGenString* _stringLiteral2818;
extern "C" void PlistDemo_OnGUIButtonPressed_m8_263 (PlistDemo_t8_50 * __this, String_t* ____buttonName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5184 = il2cpp_codegen_string_literal_from_index(5184);
		_stringLiteral5185 = il2cpp_codegen_string_literal_from_index(5185);
		_stringLiteral5186 = il2cpp_codegen_string_literal_from_index(5186);
		_stringLiteral5187 = il2cpp_codegen_string_literal_from_index(5187);
		_stringLiteral2818 = il2cpp_codegen_string_literal_from_index(2818);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____buttonName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1_601(NULL /*static, unused*/, L_0, _stringLiteral5184, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		PlistDemo_LoadPlistAtPath_m8_264(__this, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_001b:
	{
		String_t* L_2 = ____buttonName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_601(NULL /*static, unused*/, L_2, _stringLiteral5185, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		PlistDemo_LoadPlistText_m8_265(__this, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_0036:
	{
		String_t* L_4 = ____buttonName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1_601(NULL /*static, unused*/, L_4, _stringLiteral5186, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		PlistDemo_GetKeyPathValue_m8_266(__this, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_0051:
	{
		String_t* L_6 = ____buttonName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1_601(NULL /*static, unused*/, L_6, _stringLiteral5187, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006c;
		}
	}
	{
		PlistDemo_AddValue_m8_267(__this, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_006c:
	{
		String_t* L_8 = ____buttonName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1_601(NULL /*static, unused*/, L_8, _stringLiteral2818, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0082;
		}
	}
	{
		PlistDemo_Save_m8_268(__this, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::LoadPlistAtPath()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5188;
extern Il2CppCodeGenString* _stringLiteral5189;
extern "C" void PlistDemo_LoadPlistAtPath_m8_264 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5188 = il2cpp_codegen_string_literal_from_index(5188);
		_stringLiteral5189 = il2cpp_codegen_string_literal_from_index(5189);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___m_input_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5188;
		return;
	}

IL_001c:
	{
		String_t* L_2 = (__this->___m_input_3);
		Plist_t8_51 * L_3 = Plist_LoadPlistAtPath_m8_320(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->___m_plist_2 = L_3;
		Plist_t8_51 * L_4 = (__this->___m_plist_2);
		String_t* L_5 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5189, L_5, /*hidden argument*/NULL);
		__this->___m_result_5 = L_6;
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::LoadPlistText()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5188;
extern Il2CppCodeGenString* _stringLiteral5189;
extern "C" void PlistDemo_LoadPlistText_m8_265 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5188 = il2cpp_codegen_string_literal_from_index(5188);
		_stringLiteral5189 = il2cpp_codegen_string_literal_from_index(5189);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___m_input_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5188;
		return;
	}

IL_001c:
	{
		String_t* L_2 = (__this->___m_input_3);
		Plist_t8_51 * L_3 = Plist_Load_m8_321(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->___m_plist_2 = L_3;
		Plist_t8_51 * L_4 = (__this->___m_plist_2);
		String_t* L_5 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5189, L_5, /*hidden argument*/NULL);
		__this->___m_result_5 = L_6;
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::GetKeyPathValue()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5190;
extern Il2CppCodeGenString* _stringLiteral5191;
extern Il2CppCodeGenString* _stringLiteral5192;
extern "C" void PlistDemo_GetKeyPathValue_m8_266 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5190 = il2cpp_codegen_string_literal_from_index(5190);
		_stringLiteral5191 = il2cpp_codegen_string_literal_from_index(5191);
		_stringLiteral5192 = il2cpp_codegen_string_literal_from_index(5192);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Plist_t8_51 * L_0 = (__this->___m_plist_2);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5190;
		return;
	}

IL_0017:
	{
		Plist_t8_51 * L_1 = (__this->___m_plist_2);
		String_t* L_2 = (__this->___m_keyPath_4);
		NullCheck(L_1);
		Object_t * L_3 = Plist_GetKeyPathValue_m8_326(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		ObjectU5BU5D_t1_272* L_4 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral5191);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral5191;
		ObjectU5BU5D_t1_272* L_5 = L_4;
		String_t* L_6 = (__this->___m_keyPath_4);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_272* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral5192);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral5192;
		ObjectU5BU5D_t1_272* L_8 = L_7;
		Object_t * L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_562(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___m_result_5 = L_10;
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::AddValue()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5190;
extern Il2CppCodeGenString* _stringLiteral5193;
extern Il2CppCodeGenString* _stringLiteral5189;
extern "C" void PlistDemo_AddValue_m8_267 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5190 = il2cpp_codegen_string_literal_from_index(5190);
		_stringLiteral5193 = il2cpp_codegen_string_literal_from_index(5193);
		_stringLiteral5189 = il2cpp_codegen_string_literal_from_index(5189);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Plist_t8_51 * L_0 = (__this->___m_plist_2);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5190;
		return;
	}

IL_0017:
	{
		String_t* L_1 = (__this->___m_input_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5193;
		return;
	}

IL_0033:
	{
		String_t* L_3 = (__this->___m_input_3);
		Object_t * L_4 = JSONUtility_FromJSON_m8_298(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Object_t * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0077;
		}
	}
	{
		Plist_t8_51 * L_6 = (__this->___m_plist_2);
		String_t* L_7 = (__this->___m_keyPath_4);
		Object_t * L_8 = V_0;
		NullCheck(L_6);
		Plist_AddValue_m8_327(L_6, L_7, L_8, /*hidden argument*/NULL);
		Plist_t8_51 * L_9 = (__this->___m_plist_2);
		String_t* L_10 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5189, L_10, /*hidden argument*/NULL);
		__this->___m_result_5 = L_11;
		goto IL_00a9;
	}

IL_0077:
	{
		Plist_t8_51 * L_12 = (__this->___m_plist_2);
		String_t* L_13 = (__this->___m_keyPath_4);
		String_t* L_14 = (__this->___m_input_3);
		NullCheck(L_12);
		Plist_AddValue_m8_327(L_12, L_13, L_14, /*hidden argument*/NULL);
		Plist_t8_51 * L_15 = (__this->___m_plist_2);
		String_t* L_16 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5189, L_16, /*hidden argument*/NULL);
		__this->___m_result_5 = L_17;
	}

IL_00a9:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Demo.PlistDemo::Save()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5190;
extern Il2CppCodeGenString* _stringLiteral5194;
extern "C" void PlistDemo_Save_m8_268 (PlistDemo_t8_50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5190 = il2cpp_codegen_string_literal_from_index(5190);
		_stringLiteral5194 = il2cpp_codegen_string_literal_from_index(5194);
		s_Il2CppMethodIntialized = true;
	}
	{
		Plist_t8_51 * L_0 = (__this->___m_plist_2);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5190;
		return;
	}

IL_0017:
	{
		String_t* L_1 = (__this->___m_input_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		__this->___m_result_5 = _stringLiteral5194;
		return;
	}

IL_0033:
	{
		Plist_t8_51 * L_3 = (__this->___m_plist_2);
		String_t* L_4 = (__this->___m_input_3);
		NullCheck(L_3);
		Plist_Save_m8_328(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.JSONParserExtensions::ToJSON(System.Collections.IDictionary)
extern "C" String_t* JSONParserExtensions_ToJSON_m8_269 (Object_t * __this /* static, unused */, Object_t * ____dictionary, const MethodInfo* method)
{
	String_t* V_0 = {0};
	String_t* G_B3_0 = {0};
	{
		Object_t * L_0 = ____dictionary;
		String_t* L_1 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = JSONUtility_IsNull_m8_297(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_0019;
	}

IL_0018:
	{
		String_t* L_4 = V_0;
		G_B3_0 = L_4;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.String VoxelBusters.Utility.JSONParserExtensions::ToJSON(System.Collections.IList)
extern "C" String_t* JSONParserExtensions_ToJSON_m8_270 (Object_t * __this /* static, unused */, Object_t * ____list, const MethodInfo* method)
{
	String_t* V_0 = {0};
	String_t* G_B3_0 = {0};
	{
		Object_t * L_0 = ____list;
		String_t* L_1 = JSONUtility_ToJSON_m8_296(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = JSONUtility_IsNull_m8_297(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		G_B3_0 = ((String_t*)(NULL));
		goto IL_0019;
	}

IL_0018:
	{
		String_t* L_4 = V_0;
		G_B3_0 = L_4;
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Void VoxelBusters.Utility.Internal.JSONConstants::.ctor()
extern "C" void JSONConstants__ctor_m8_271 (JSONConstants_t8_53 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Internal.JSONString::.ctor(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void JSONString__ctor_m8_272 (JSONString_t8_55 * __this, String_t* ____JSONString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	JSONString_t8_55 * G_B2_0 = {0};
	JSONString_t8_55 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	JSONString_t8_55 * G_B3_1 = {0};
	{
		String_t* L_0 = ____JSONString;
		JSONString_set_Value_m8_274(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ____JSONString;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		JSONString_set_IsNullOrEmpty_m8_276(__this, L_2, /*hidden argument*/NULL);
		bool L_3 = JSONString_get_IsNullOrEmpty_m8_275(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_3)
		{
			G_B2_0 = __this;
			goto IL_0025;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_002b;
	}

IL_0025:
	{
		String_t* L_4 = ____JSONString;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1_571(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_002b:
	{
		JSONString_set_Length_m8_278(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.Internal.JSONString::get_Value()
extern "C" String_t* JSONString_get_Value_m8_273 (JSONString_t8_55 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CValueU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.Internal.JSONString::set_Value(System.String)
extern "C" void JSONString_set_Value_m8_274 (JSONString_t8_55 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CValueU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Boolean VoxelBusters.Utility.Internal.JSONString::get_IsNullOrEmpty()
extern "C" bool JSONString_get_IsNullOrEmpty_m8_275 (JSONString_t8_55 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsNullOrEmptyU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.Internal.JSONString::set_IsNullOrEmpty(System.Boolean)
extern "C" void JSONString_set_IsNullOrEmpty_m8_276 (JSONString_t8_55 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsNullOrEmptyU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Int32 VoxelBusters.Utility.Internal.JSONString::get_Length()
extern "C" int32_t JSONString_get_Length_m8_277 (JSONString_t8_55 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CLengthU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.Internal.JSONString::set_Length(System.Int32)
extern "C" void JSONString_set_Length_m8_278 (JSONString_t8_55 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CLengthU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Char VoxelBusters.Utility.Internal.JSONString::get_Item(System.Int32)
extern "C" uint16_t JSONString_get_Item_m8_279 (JSONString_t8_55 * __this, int32_t ____index, const MethodInfo* method)
{
	{
		String_t* L_0 = JSONString_get_Value_m8_273(__this, /*hidden argument*/NULL);
		int32_t L_1 = ____index;
		NullCheck(L_0);
		uint16_t L_2 = String_get_Chars_m1_442(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: VoxelBusters.Utility.Internal.JSONString
extern "C" void JSONString_t8_55_marshal(const JSONString_t8_55& unmarshaled, JSONString_t8_55_marshaled& marshaled)
{
	marshaled.___U3CValueU3Ek__BackingField_0 = il2cpp_codegen_marshal_string(unmarshaled.___U3CValueU3Ek__BackingField_0);
	marshaled.___U3CIsNullOrEmptyU3Ek__BackingField_1 = unmarshaled.___U3CIsNullOrEmptyU3Ek__BackingField_1;
	marshaled.___U3CLengthU3Ek__BackingField_2 = unmarshaled.___U3CLengthU3Ek__BackingField_2;
}
extern "C" void JSONString_t8_55_marshal_back(const JSONString_t8_55_marshaled& marshaled, JSONString_t8_55& unmarshaled)
{
	unmarshaled.___U3CValueU3Ek__BackingField_0 = il2cpp_codegen_marshal_string_result(marshaled.___U3CValueU3Ek__BackingField_0);
	unmarshaled.___U3CIsNullOrEmptyU3Ek__BackingField_1 = marshaled.___U3CIsNullOrEmptyU3Ek__BackingField_1;
	unmarshaled.___U3CLengthU3Ek__BackingField_2 = marshaled.___U3CLengthU3Ek__BackingField_2;
}
// Conversion method for clean up from marshalling of: VoxelBusters.Utility.Internal.JSONString
extern "C" void JSONString_t8_55_marshal_cleanup(JSONString_t8_55_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___U3CValueU3Ek__BackingField_0);
	marshaled.___U3CValueU3Ek__BackingField_0 = NULL;
}
// System.Void VoxelBusters.Utility.JSONReader::.ctor()
extern "C" void JSONReader__ctor_m8_280 (JSONReader_t8_56 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONReader::.ctor(System.String)
extern "C" void JSONReader__ctor_m8_281 (JSONReader_t8_56 * __this, String_t* ____inputJSONString, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ____inputJSONString;
		JSONString_t8_55  L_1 = {0};
		JSONString__ctor_m8_272(&L_1, L_0, /*hidden argument*/NULL);
		JSONReader_set_JSONString_m8_283(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// VoxelBusters.Utility.Internal.JSONString VoxelBusters.Utility.JSONReader::get_JSONString()
extern "C" JSONString_t8_55  JSONReader_get_JSONString_m8_282 (JSONReader_t8_56 * __this, const MethodInfo* method)
{
	{
		JSONString_t8_55  L_0 = (__this->___U3CJSONStringU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.JSONReader::set_JSONString(VoxelBusters.Utility.Internal.JSONString)
extern "C" void JSONReader_set_JSONString_m8_283 (JSONReader_t8_56 * __this, JSONString_t8_55  ___value, const MethodInfo* method)
{
	{
		JSONString_t8_55  L_0 = ___value;
		__this->___U3CJSONStringU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Object VoxelBusters.Utility.JSONReader::Deserialise()
extern "C" Object_t * JSONReader_Deserialise_m8_284 (JSONReader_t8_56 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	JSONString_t8_55  V_1 = {0};
	{
		JSONString_t8_55  L_0 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		bool L_1 = JSONString_get_IsNullOrEmpty_m8_275((&V_1), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		return NULL;
	}

IL_0015:
	{
		V_0 = 0;
		Object_t * L_2 = JSONReader_ReadValue_m8_286(__this, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object VoxelBusters.Utility.JSONReader::Deserialise(System.Int32&)
extern "C" Object_t * JSONReader_Deserialise_m8_285 (JSONReader_t8_56 * __this, int32_t* ____errorIndex, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	JSONString_t8_55  V_2 = {0};
	JSONString_t8_55  V_3 = {0};
	{
		JSONString_t8_55  L_0 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_2 = L_0;
		bool L_1 = JSONString_get_IsNullOrEmpty_m8_275((&V_2), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		return NULL;
	}

IL_0015:
	{
		V_0 = 0;
		Object_t * L_2 = JSONReader_ReadValue_m8_286(__this, (&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_0;
		JSONString_t8_55  L_4 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_3 = L_4;
		int32_t L_5 = JSONString_get_Length_m8_277((&V_3), /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t* L_6 = ____errorIndex;
		int32_t L_7 = V_0;
		*((int32_t*)(L_6)) = (int32_t)L_7;
		goto IL_003f;
	}

IL_003c:
	{
		int32_t* L_8 = ____errorIndex;
		*((int32_t*)(L_8)) = (int32_t)(-1);
	}

IL_003f:
	{
		Object_t * L_9 = V_1;
		return L_9;
	}
}
// System.Object VoxelBusters.Utility.JSONReader::ReadValue(System.Int32&)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5195;
extern "C" Object_t * JSONReader_ReadValue_m8_286 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5195 = il2cpp_codegen_string_literal_from_index(5195);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t* L_0 = ____index;
		JSONReader_RemoveWhiteSpace_m8_295(__this, L_0, /*hidden argument*/NULL);
		int32_t* L_1 = ____index;
		int32_t L_2 = JSONReader_LookAhead_m8_293(__this, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4 == 0)
		{
			goto IL_004d;
		}
		if (L_4 == 1)
		{
			goto IL_008f;
		}
		if (L_4 == 2)
		{
			goto IL_0055;
		}
		if (L_4 == 3)
		{
			goto IL_008f;
		}
		if (L_4 == 4)
		{
			goto IL_008f;
		}
		if (L_4 == 5)
		{
			goto IL_008f;
		}
		if (L_4 == 6)
		{
			goto IL_005d;
		}
		if (L_4 == 7)
		{
			goto IL_0065;
		}
		if (L_4 == 8)
		{
			goto IL_008f;
		}
		if (L_4 == 9)
		{
			goto IL_0075;
		}
		if (L_4 == 10)
		{
			goto IL_0082;
		}
		if (L_4 == 11)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_008f;
	}

IL_004d:
	{
		int32_t* L_5 = ____index;
		Object_t * L_6 = JSONReader_ReadObject_m8_287(__this, L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0055:
	{
		int32_t* L_7 = ____index;
		Object_t * L_8 = JSONReader_ReadArray_m8_289(__this, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_005d:
	{
		int32_t* L_9 = ____index;
		String_t* L_10 = JSONReader_ReadString_m8_291(__this, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0065:
	{
		int32_t* L_11 = ____index;
		Object_t * L_12 = JSONReader_ReadNumber_m8_292(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_006d:
	{
		int32_t* L_13 = ____index;
		int32_t* L_14 = ____index;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_14))+(int32_t)4));
		return NULL;
	}

IL_0075:
	{
		int32_t* L_15 = ____index;
		int32_t* L_16 = ____index;
		*((int32_t*)(L_15)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_16))+(int32_t)4));
		bool L_17 = 1;
		Object_t * L_18 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_17);
		return L_18;
	}

IL_0082:
	{
		int32_t* L_19 = ____index;
		int32_t* L_20 = ____index;
		*((int32_t*)(L_19)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_20))+(int32_t)5));
		bool L_21 = 0;
		Object_t * L_22 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_21);
		return L_22;
	}

IL_008f:
	{
		int32_t* L_23 = ____index;
		int32_t L_24 = (*((int32_t*)L_23));
		Object_t * L_25 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_25, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_00aa:
	{
		return NULL;
	}
}
// System.Object VoxelBusters.Utility.JSONReader::ReadObject(System.Int32&)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5195;
extern "C" Object_t * JSONReader_ReadObject_m8_287 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5195 = il2cpp_codegen_string_literal_from_index(5195);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	bool V_1 = false;
	int32_t V_2 = {0};
	String_t* V_3 = {0};
	Object_t * V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = {0};
	{
		Dictionary_2_t1_1903 * L_0 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_0, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		int32_t* L_1 = ____index;
		int32_t* L_2 = ____index;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_2))+(int32_t)1));
		goto IL_00be;
	}

IL_0013:
	{
		int32_t* L_3 = ____index;
		int32_t L_4 = JSONReader_LookAhead_m8_293(__this, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_2;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t* L_6 = ____index;
		int32_t L_7 = (*((int32_t*)L_6));
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_8, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return NULL;
	}

IL_003c:
	{
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t* L_11 = ____index;
		JSONReader_NextToken_m8_294(__this, L_11, /*hidden argument*/NULL);
		V_1 = 1;
		goto IL_00be;
	}

IL_0052:
	{
		int32_t* L_12 = ____index;
		int32_t L_13 = JSONReader_ReadKeyValuePair_m8_288(__this, L_12, (&V_3), (&V_4), /*hidden argument*/NULL);
		V_5 = L_13;
		int32_t L_14 = V_5;
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_00be;
		}
	}
	{
		Object_t * L_15 = V_0;
		String_t* L_16 = V_3;
		Object_t * L_17 = V_4;
		NullCheck(L_15);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_15, L_16, L_17);
		int32_t* L_18 = ____index;
		int32_t L_19 = JSONReader_LookAhead_m8_293(__this, (*((int32_t*)L_18)), /*hidden argument*/NULL);
		V_6 = L_19;
		int32_t L_20 = V_6;
		if ((!(((uint32_t)L_20) == ((uint32_t)5))))
		{
			goto IL_008f;
		}
	}
	{
		int32_t* L_21 = ____index;
		JSONReader_NextToken_m8_294(__this, L_21, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_008f:
	{
		int32_t L_22 = V_6;
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t* L_23 = ____index;
		JSONReader_NextToken_m8_294(__this, L_23, /*hidden argument*/NULL);
		V_1 = 1;
		goto IL_00be;
	}

IL_00a6:
	{
		int32_t* L_24 = ____index;
		int32_t L_25 = (*((int32_t*)L_24));
		Object_t * L_26 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_26, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		return NULL;
	}

IL_00be:
	{
		bool L_28 = V_1;
		if (!L_28)
		{
			goto IL_0013;
		}
	}
	{
		Object_t * L_29 = V_0;
		return L_29;
	}
}
// System.Int32 VoxelBusters.Utility.JSONReader::ReadKeyValuePair(System.Int32&,System.String&,System.Object&)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5195;
extern "C" int32_t JSONReader_ReadKeyValuePair_m8_288 (JSONReader_t8_56 * __this, int32_t* ____index, String_t** ____key, Object_t ** ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral5195 = il2cpp_codegen_string_literal_from_index(5195);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t** L_0 = ____key;
		*((Object_t **)(L_0)) = (Object_t *)NULL;
		Object_t ** L_1 = ____value;
		*((Object_t **)(L_1)) = (Object_t *)NULL;
		String_t** L_2 = ____key;
		int32_t* L_3 = ____index;
		Object_t * L_4 = JSONReader_ReadValue_m8_286(__this, L_3, /*hidden argument*/NULL);
		*((Object_t **)(L_2)) = (Object_t *)((String_t*)IsInstSealed(L_4, String_t_il2cpp_TypeInfo_var));
		String_t** L_5 = ____key;
		if ((*((String_t**)L_5)))
		{
			goto IL_0033;
		}
	}
	{
		int32_t* L_6 = ____index;
		int32_t L_7 = (*((int32_t*)L_6));
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_8, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return (-1);
	}

IL_0033:
	{
		int32_t* L_10 = ____index;
		int32_t L_11 = JSONReader_NextToken_m8_294(__this, L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)4)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t* L_12 = ____index;
		int32_t L_13 = (*((int32_t*)L_12));
		Object_t * L_14 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_14, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return (-1);
	}

IL_0058:
	{
		Object_t ** L_16 = ____value;
		int32_t* L_17 = ____index;
		Object_t * L_18 = JSONReader_ReadValue_m8_286(__this, L_17, /*hidden argument*/NULL);
		*((Object_t **)(L_16)) = (Object_t *)L_18;
		return 0;
	}
}
// System.Object VoxelBusters.Utility.JSONReader::ReadArray(System.Int32&)
extern TypeInfo* List_1_t1_1894_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15034_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5195;
extern "C" Object_t * JSONReader_ReadArray_m8_289 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1958);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		List_1__ctor_m1_15034_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484121);
		_stringLiteral5195 = il2cpp_codegen_string_literal_from_index(5195);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	bool V_1 = false;
	int32_t V_2 = {0};
	Object_t * V_3 = {0};
	int32_t V_4 = {0};
	{
		List_1_t1_1894 * L_0 = (List_1_t1_1894 *)il2cpp_codegen_object_new (List_1_t1_1894_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15034(L_0, /*hidden argument*/List_1__ctor_m1_15034_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		int32_t* L_1 = ____index;
		int32_t* L_2 = ____index;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_2))+(int32_t)1));
		goto IL_00b1;
	}

IL_0013:
	{
		int32_t* L_3 = ____index;
		int32_t L_4 = JSONReader_LookAhead_m8_293(__this, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_2;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t* L_6 = ____index;
		int32_t L_7 = (*((int32_t*)L_6));
		Object_t * L_8 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_8, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return NULL;
	}

IL_003c:
	{
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t* L_11 = ____index;
		JSONReader_NextToken_m8_294(__this, L_11, /*hidden argument*/NULL);
		V_1 = 1;
		goto IL_00b1;
	}

IL_0052:
	{
		int32_t* L_12 = ____index;
		JSONReader_ReadArrayElement_m8_290(__this, L_12, (&V_3), /*hidden argument*/NULL);
		Object_t * L_13 = V_0;
		Object_t * L_14 = V_3;
		NullCheck(L_13);
		InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1_262_il2cpp_TypeInfo_var, L_13, L_14);
		int32_t* L_15 = ____index;
		int32_t L_16 = JSONReader_LookAhead_m8_293(__this, (*((int32_t*)L_15)), /*hidden argument*/NULL);
		V_4 = L_16;
		int32_t L_17 = V_4;
		if ((!(((uint32_t)L_17) == ((uint32_t)5))))
		{
			goto IL_0082;
		}
	}
	{
		int32_t* L_18 = ____index;
		JSONReader_NextToken_m8_294(__this, L_18, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_0082:
	{
		int32_t L_19 = V_4;
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_0099;
		}
	}
	{
		int32_t* L_20 = ____index;
		JSONReader_NextToken_m8_294(__this, L_20, /*hidden argument*/NULL);
		V_1 = 1;
		goto IL_00b1;
	}

IL_0099:
	{
		int32_t* L_21 = ____index;
		int32_t L_22 = (*((int32_t*)L_21));
		Object_t * L_23 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Format_m1_548(NULL /*static, unused*/, _stringLiteral5195, L_23, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return NULL;
	}

IL_00b1:
	{
		bool L_25 = V_1;
		if (!L_25)
		{
			goto IL_0013;
		}
	}
	{
		Object_t * L_26 = V_0;
		return L_26;
	}
}
// System.Void VoxelBusters.Utility.JSONReader::ReadArrayElement(System.Int32&,System.Object&)
extern "C" void JSONReader_ReadArrayElement_m8_290 (JSONReader_t8_56 * __this, int32_t* ____index, Object_t ** ____element, const MethodInfo* method)
{
	{
		Object_t ** L_0 = ____element;
		int32_t* L_1 = ____index;
		Object_t * L_2 = JSONReader_ReadValue_m8_286(__this, L_1, /*hidden argument*/NULL);
		*((Object_t **)(L_0)) = (Object_t *)L_2;
		return;
	}
}
// System.String VoxelBusters.Utility.JSONReader::ReadString(System.Int32&)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* JSONReader_ReadString_m8_291 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	bool V_1 = false;
	uint16_t V_2 = 0x0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	uint16_t V_5 = 0x0;
	JSONString_t8_55  V_6 = {0};
	JSONString_t8_55  V_7 = {0};
	int32_t V_8 = 0;
	JSONString_t8_55  V_9 = {0};
	JSONString_t8_55  V_10 = {0};
	JSONString_t8_55  V_11 = {0};
	JSONString_t8_55  V_12 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		int32_t* L_1 = ____index;
		int32_t* L_2 = ____index;
		*((int32_t*)(L_1)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_2))+(int32_t)1));
		goto IL_01b5;
	}

IL_0013:
	{
		int32_t* L_3 = ____index;
		JSONString_t8_55  L_4 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_6 = L_4;
		int32_t L_5 = JSONString_get_Length_m8_277((&V_6), /*hidden argument*/NULL);
		if ((!(((uint32_t)(*((int32_t*)L_3))) == ((uint32_t)L_5))))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_01bb;
	}

IL_002e:
	{
		JSONString_t8_55  L_6 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_7 = L_6;
		int32_t* L_7 = ____index;
		int32_t* L_8 = ____index;
		int32_t L_9 = (*((int32_t*)L_8));
		V_8 = L_9;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_8;
		uint16_t L_11 = JSONString_get_Item_m8_279((&V_7), L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		uint16_t L_12 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0058;
		}
	}
	{
		V_1 = 1;
		goto IL_01b5;
	}

IL_0058:
	{
		uint16_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_01ad;
		}
	}
	{
		int32_t* L_14 = ____index;
		JSONString_t8_55  L_15 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_9 = L_15;
		int32_t L_16 = JSONString_get_Length_m8_277((&V_9), /*hidden argument*/NULL);
		if ((!(((uint32_t)(*((int32_t*)L_14))) == ((uint32_t)L_16))))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_01bb;
	}

IL_007b:
	{
		JSONString_t8_55  L_17 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_10 = L_17;
		int32_t* L_18 = ____index;
		int32_t* L_19 = ____index;
		int32_t L_20 = (*((int32_t*)L_19));
		V_8 = L_20;
		*((int32_t*)(L_18)) = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
		int32_t L_21 = V_8;
		uint16_t L_22 = JSONString_get_Item_m8_279((&V_10), L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		uint16_t L_23 = V_2;
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00ac;
		}
	}
	{
		StringBuilder_t1_247 * L_24 = V_0;
		NullCheck(L_24);
		StringBuilder_Append_m1_12452(L_24, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_00ac:
	{
		uint16_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00c2;
		}
	}
	{
		StringBuilder_t1_247 * L_26 = V_0;
		NullCheck(L_26);
		StringBuilder_Append_m1_12452(L_26, ((int32_t)92), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_00c2:
	{
		uint16_t L_27 = V_2;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00d8;
		}
	}
	{
		StringBuilder_t1_247 * L_28 = V_0;
		NullCheck(L_28);
		StringBuilder_Append_m1_12452(L_28, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_00d8:
	{
		uint16_t L_29 = V_2;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00ed;
		}
	}
	{
		StringBuilder_t1_247 * L_30 = V_0;
		NullCheck(L_30);
		StringBuilder_Append_m1_12452(L_30, 8, /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_00ed:
	{
		uint16_t L_31 = V_2;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0103;
		}
	}
	{
		StringBuilder_t1_247 * L_32 = V_0;
		NullCheck(L_32);
		StringBuilder_Append_m1_12452(L_32, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_0103:
	{
		uint16_t L_33 = V_2;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0119;
		}
	}
	{
		StringBuilder_t1_247 * L_34 = V_0;
		NullCheck(L_34);
		StringBuilder_Append_m1_12452(L_34, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_0119:
	{
		uint16_t L_35 = V_2;
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_012f;
		}
	}
	{
		StringBuilder_t1_247 * L_36 = V_0;
		NullCheck(L_36);
		StringBuilder_Append_m1_12452(L_36, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_012f:
	{
		uint16_t L_37 = V_2;
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0145;
		}
	}
	{
		StringBuilder_t1_247 * L_38 = V_0;
		NullCheck(L_38);
		StringBuilder_Append_m1_12452(L_38, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_01a8;
	}

IL_0145:
	{
		uint16_t L_39 = V_2;
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01a8;
		}
	}
	{
		JSONString_t8_55  L_40 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_11 = L_40;
		int32_t L_41 = JSONString_get_Length_m8_277((&V_11), /*hidden argument*/NULL);
		int32_t* L_42 = ____index;
		V_3 = ((int32_t)((int32_t)L_41-(int32_t)(*((int32_t*)L_42))));
		int32_t L_43 = V_3;
		if ((((int32_t)L_43) < ((int32_t)4)))
		{
			goto IL_01a3;
		}
	}
	{
		JSONString_t8_55  L_44 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_12 = L_44;
		String_t* L_45 = JSONString_get_Value_m8_273((&V_12), /*hidden argument*/NULL);
		int32_t* L_46 = ____index;
		NullCheck(L_45);
		String_t* L_47 = String_Substring_m1_455(L_45, (*((int32_t*)L_46)), 4, /*hidden argument*/NULL);
		V_4 = L_47;
		String_t* L_48 = V_4;
		int32_t L_49 = Int32_Parse_m1_87(NULL /*static, unused*/, L_48, ((int32_t)515), /*hidden argument*/NULL);
		V_5 = (((int32_t)((uint16_t)L_49)));
		StringBuilder_t1_247 * L_50 = V_0;
		uint16_t L_51 = V_5;
		NullCheck(L_50);
		StringBuilder_Append_m1_12452(L_50, L_51, /*hidden argument*/NULL);
		int32_t* L_52 = ____index;
		int32_t* L_53 = ____index;
		*((int32_t*)(L_52)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_53))+(int32_t)4));
		goto IL_01a8;
	}

IL_01a3:
	{
		goto IL_01bb;
	}

IL_01a8:
	{
		goto IL_01b5;
	}

IL_01ad:
	{
		StringBuilder_t1_247 * L_54 = V_0;
		uint16_t L_55 = V_2;
		NullCheck(L_54);
		StringBuilder_Append_m1_12452(L_54, L_55, /*hidden argument*/NULL);
	}

IL_01b5:
	{
		bool L_56 = V_1;
		if (!L_56)
		{
			goto IL_0013;
		}
	}

IL_01bb:
	{
		bool L_57 = V_1;
		if (L_57)
		{
			goto IL_01c3;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_01c3:
	{
		StringBuilder_t1_247 * L_58 = V_0;
		NullCheck(L_58);
		String_t* L_59 = StringBuilder_ToString_m1_12428(L_58, /*hidden argument*/NULL);
		return L_59;
	}
}
// System.Object VoxelBusters.Utility.JSONReader::ReadNumber(System.Int32&)
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5196;
extern "C" Object_t * JSONReader_ReadNumber_m8_292 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		_stringLiteral5196 = il2cpp_codegen_string_literal_from_index(5196);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int64_t V_4 = 0;
	double V_5 = 0.0;
	JSONString_t8_55  V_6 = {0};
	JSONString_t8_55  V_7 = {0};
	JSONString_t8_55  V_8 = {0};
	{
		int32_t* L_0 = ____index;
		V_0 = (*((int32_t*)L_0));
		V_1 = 0;
		goto IL_004c;
	}

IL_000a:
	{
		JSONString_t8_55  L_1 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_6 = L_1;
		int32_t L_2 = V_0;
		uint16_t L_3 = JSONString_get_Item_m8_279((&V_6), L_2, /*hidden argument*/NULL);
		NullCheck(_stringLiteral5196);
		int32_t L_4 = String_IndexOf_m1_500(_stringLiteral5196, L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int32_t L_6 = V_0;
		JSONString_t8_55  L_7 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_7 = L_7;
		int32_t L_8 = JSONString_get_Length_m8_277((&V_7), /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0045;
		}
	}
	{
		V_1 = 1;
	}

IL_0045:
	{
		goto IL_004c;
	}

IL_004a:
	{
		V_1 = 1;
	}

IL_004c:
	{
		bool L_9 = V_1;
		if (!L_9)
		{
			goto IL_000a;
		}
	}
	{
		int32_t L_10 = V_0;
		int32_t* L_11 = ____index;
		V_2 = ((int32_t)((int32_t)L_10-(int32_t)(*((int32_t*)L_11))));
		JSONString_t8_55  L_12 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_8 = L_12;
		String_t* L_13 = JSONString_get_Value_m8_273((&V_8), /*hidden argument*/NULL);
		int32_t* L_14 = ____index;
		int32_t L_15 = V_2;
		NullCheck(L_13);
		String_t* L_16 = String_Substring_m1_455(L_13, (*((int32_t*)L_14)), L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		int32_t* L_17 = ____index;
		int32_t L_18 = V_0;
		*((int32_t*)(L_17)) = (int32_t)L_18;
		String_t* L_19 = V_3;
		bool L_20 = Int64_TryParse_m1_141(NULL /*static, unused*/, L_19, (&V_4), /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		int64_t L_21 = V_4;
		int64_t L_22 = L_21;
		Object_t * L_23 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_22);
		return L_23;
	}

IL_0087:
	{
		String_t* L_24 = V_3;
		bool L_25 = Double_TryParse_m1_670(NULL /*static, unused*/, L_24, (&V_5), /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_009c;
		}
	}
	{
		double L_26 = V_5;
		double L_27 = L_26;
		Object_t * L_28 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_27);
		return L_28;
	}

IL_009c:
	{
		return NULL;
	}
}
// VoxelBusters.Utility.eJSONToken VoxelBusters.Utility.JSONReader::LookAhead(System.Int32)
extern "C" int32_t JSONReader_LookAhead_m8_293 (JSONReader_t8_56 * __this, int32_t ____index, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ____index;
		V_0 = L_0;
		int32_t L_1 = JSONReader_NextToken_m8_294(__this, (&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// VoxelBusters.Utility.eJSONToken VoxelBusters.Utility.JSONReader::NextToken(System.Int32&)
extern "C" int32_t JSONReader_NextToken_m8_294 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	uint16_t V_0 = 0x0;
	JSONString_t8_55  V_1 = {0};
	JSONString_t8_55  V_2 = {0};
	int32_t V_3 = 0;
	uint16_t V_4 = 0x0;
	JSONString_t8_55  V_5 = {0};
	JSONString_t8_55  V_6 = {0};
	JSONString_t8_55  V_7 = {0};
	JSONString_t8_55  V_8 = {0};
	JSONString_t8_55  V_9 = {0};
	JSONString_t8_55  V_10 = {0};
	JSONString_t8_55  V_11 = {0};
	JSONString_t8_55  V_12 = {0};
	JSONString_t8_55  V_13 = {0};
	JSONString_t8_55  V_14 = {0};
	JSONString_t8_55  V_15 = {0};
	JSONString_t8_55  V_16 = {0};
	JSONString_t8_55  V_17 = {0};
	JSONString_t8_55  V_18 = {0};
	JSONString_t8_55  V_19 = {0};
	JSONString_t8_55  V_20 = {0};
	{
		int32_t* L_0 = ____index;
		JSONString_t8_55  L_1 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = JSONString_get_Length_m8_277((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)(*((int32_t*)L_0))) == ((uint32_t)L_2))))
		{
			goto IL_0018;
		}
	}
	{
		return (int32_t)(((int32_t)12));
	}

IL_0018:
	{
		int32_t* L_3 = ____index;
		JSONReader_RemoveWhiteSpace_m8_295(__this, L_3, /*hidden argument*/NULL);
		JSONString_t8_55  L_4 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t* L_5 = ____index;
		int32_t* L_6 = ____index;
		int32_t L_7 = (*((int32_t*)L_6));
		V_3 = L_7;
		*((int32_t*)(L_5)) = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_3;
		uint16_t L_9 = JSONString_get_Item_m8_279((&V_2), L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		uint16_t L_10 = V_0;
		V_4 = L_10;
		uint16_t L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00e5;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00e3;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_00a8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00e7;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00e1;
		}
	}

IL_00a8:
	{
		uint16_t L_12 = V_4;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00dd;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00be;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00df;
		}
	}

IL_00be:
	{
		uint16_t L_13 = V_4;
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00d9;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00e9;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00db;
		}
	}
	{
		goto IL_00e9;
	}

IL_00d9:
	{
		return (int32_t)(0);
	}

IL_00db:
	{
		return (int32_t)(1);
	}

IL_00dd:
	{
		return (int32_t)(2);
	}

IL_00df:
	{
		return (int32_t)(3);
	}

IL_00e1:
	{
		return (int32_t)(4);
	}

IL_00e3:
	{
		return (int32_t)(5);
	}

IL_00e5:
	{
		return (int32_t)(6);
	}

IL_00e7:
	{
		return (int32_t)(7);
	}

IL_00e9:
	{
		int32_t* L_14 = ____index;
		int32_t* L_15 = ____index;
		*((int32_t*)(L_14)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_15))-(int32_t)1));
		int32_t* L_16 = ____index;
		JSONString_t8_55  L_17 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = JSONString_get_Length_m8_277((&V_5), /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)(*((int32_t*)L_16))+(int32_t)4))) >= ((int32_t)L_18)))
		{
			goto IL_0176;
		}
	}
	{
		JSONString_t8_55  L_19 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_6 = L_19;
		int32_t* L_20 = ____index;
		uint16_t L_21 = JSONString_get_Item_m8_279((&V_6), (*((int32_t*)L_20)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_0176;
		}
	}
	{
		JSONString_t8_55  L_22 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_7 = L_22;
		int32_t* L_23 = ____index;
		uint16_t L_24 = JSONString_get_Item_m8_279((&V_7), ((int32_t)((int32_t)(*((int32_t*)L_23))+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_0176;
		}
	}
	{
		JSONString_t8_55  L_25 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_8 = L_25;
		int32_t* L_26 = ____index;
		uint16_t L_27 = JSONString_get_Item_m8_279((&V_8), ((int32_t)((int32_t)(*((int32_t*)L_26))+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0176;
		}
	}
	{
		JSONString_t8_55  L_28 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_9 = L_28;
		int32_t* L_29 = ____index;
		uint16_t L_30 = JSONString_get_Item_m8_279((&V_9), ((int32_t)((int32_t)(*((int32_t*)L_29))+(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0176;
		}
	}
	{
		int32_t* L_31 = ____index;
		int32_t* L_32 = ____index;
		*((int32_t*)(L_31)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_32))+(int32_t)4));
		return (int32_t)(((int32_t)11));
	}

IL_0176:
	{
		int32_t* L_33 = ____index;
		JSONString_t8_55  L_34 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_10 = L_34;
		int32_t L_35 = JSONString_get_Length_m8_277((&V_10), /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)(*((int32_t*)L_33))+(int32_t)4))) >= ((int32_t)L_35)))
		{
			goto IL_01fd;
		}
	}
	{
		JSONString_t8_55  L_36 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_11 = L_36;
		int32_t* L_37 = ____index;
		uint16_t L_38 = JSONString_get_Item_m8_279((&V_11), (*((int32_t*)L_37)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_01fd;
		}
	}
	{
		JSONString_t8_55  L_39 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_12 = L_39;
		int32_t* L_40 = ____index;
		uint16_t L_41 = JSONString_get_Item_m8_279((&V_12), ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_01fd;
		}
	}
	{
		JSONString_t8_55  L_42 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_13 = L_42;
		int32_t* L_43 = ____index;
		uint16_t L_44 = JSONString_get_Item_m8_279((&V_13), ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01fd;
		}
	}
	{
		JSONString_t8_55  L_45 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_14 = L_45;
		int32_t* L_46 = ____index;
		uint16_t L_47 = JSONString_get_Item_m8_279((&V_14), ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_47) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_01fd;
		}
	}
	{
		int32_t* L_48 = ____index;
		int32_t* L_49 = ____index;
		*((int32_t*)(L_48)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_49))+(int32_t)4));
		return (int32_t)(((int32_t)9));
	}

IL_01fd:
	{
		int32_t* L_50 = ____index;
		JSONString_t8_55  L_51 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_15 = L_51;
		int32_t L_52 = JSONString_get_Length_m8_277((&V_15), /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)(*((int32_t*)L_50))+(int32_t)5))) >= ((int32_t)L_52)))
		{
			goto IL_029e;
		}
	}
	{
		JSONString_t8_55  L_53 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_16 = L_53;
		int32_t* L_54 = ____index;
		uint16_t L_55 = JSONString_get_Item_m8_279((&V_16), (*((int32_t*)L_54)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_55) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_029e;
		}
	}
	{
		JSONString_t8_55  L_56 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_17 = L_56;
		int32_t* L_57 = ____index;
		uint16_t L_58 = JSONString_get_Item_m8_279((&V_17), ((int32_t)((int32_t)(*((int32_t*)L_57))+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_029e;
		}
	}
	{
		JSONString_t8_55  L_59 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_18 = L_59;
		int32_t* L_60 = ____index;
		uint16_t L_61 = JSONString_get_Item_m8_279((&V_18), ((int32_t)((int32_t)(*((int32_t*)L_60))+(int32_t)2)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_029e;
		}
	}
	{
		JSONString_t8_55  L_62 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_19 = L_62;
		int32_t* L_63 = ____index;
		uint16_t L_64 = JSONString_get_Item_m8_279((&V_19), ((int32_t)((int32_t)(*((int32_t*)L_63))+(int32_t)3)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_64) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_029e;
		}
	}
	{
		JSONString_t8_55  L_65 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_20 = L_65;
		int32_t* L_66 = ____index;
		uint16_t L_67 = JSONString_get_Item_m8_279((&V_20), ((int32_t)((int32_t)(*((int32_t*)L_66))+(int32_t)4)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_67) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_029e;
		}
	}
	{
		int32_t* L_68 = ____index;
		int32_t* L_69 = ____index;
		*((int32_t*)(L_68)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_69))+(int32_t)5));
		return (int32_t)(((int32_t)10));
	}

IL_029e:
	{
		return (int32_t)(((int32_t)12));
	}
}
// System.Void VoxelBusters.Utility.JSONReader::RemoveWhiteSpace(System.Int32&)
extern Il2CppCodeGenString* _stringLiteral5197;
extern "C" void JSONReader_RemoveWhiteSpace_m8_295 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5197 = il2cpp_codegen_string_literal_from_index(5197);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint16_t V_1 = 0x0;
	JSONString_t8_55  V_2 = {0};
	JSONString_t8_55  V_3 = {0};
	{
		JSONString_t8_55  L_0 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = JSONString_get_Length_m8_277((&V_2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0046;
	}

IL_0014:
	{
		JSONString_t8_55  L_2 = JSONReader_get_JSONString_m8_282(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		int32_t* L_3 = ____index;
		uint16_t L_4 = JSONString_get_Item_m8_279((&V_3), (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_1 = L_4;
		uint16_t L_5 = V_1;
		NullCheck(_stringLiteral5197);
		int32_t L_6 = String_IndexOf_m1_500(_stringLiteral5197, L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t* L_7 = ____index;
		int32_t* L_8 = ____index;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1));
		goto IL_0046;
	}

IL_0041:
	{
		goto IL_004e;
	}

IL_0046:
	{
		int32_t* L_9 = ____index;
		int32_t L_10 = V_0;
		if ((((int32_t)(*((int32_t*)L_9))) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}

IL_004e:
	{
		return;
	}
}
// System.String VoxelBusters.Utility.JSONUtility::ToJSON(System.Object)
extern TypeInfo* JSONWriter_t8_58_il2cpp_TypeInfo_var;
extern "C" String_t* JSONUtility_ToJSON_m8_296 (Object_t * __this /* static, unused */, Object_t * ____object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONWriter_t8_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1966);
		s_Il2CppMethodIntialized = true;
	}
	JSONWriter_t8_58 * V_0 = {0};
	{
		JSONWriter_t8_58 * L_0 = (JSONWriter_t8_58 *)il2cpp_codegen_object_new (JSONWriter_t8_58_il2cpp_TypeInfo_var);
		JSONWriter__ctor_m8_300(L_0, ((int32_t)512), /*hidden argument*/NULL);
		V_0 = L_0;
		JSONWriter_t8_58 * L_1 = V_0;
		Object_t * L_2 = ____object;
		NullCheck(L_1);
		String_t* L_3 = JSONWriter_Serialise_m8_303(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean VoxelBusters.Utility.JSONUtility::IsNull(System.String)
extern Il2CppCodeGenString* _stringLiteral994;
extern "C" bool JSONUtility_IsNull_m8_297 (Object_t * __this /* static, unused */, String_t* ____jsonStr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral994 = il2cpp_codegen_string_literal_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ____jsonStr;
		NullCheck(L_0);
		bool L_1 = String_Equals_m1_441(L_0, _stringLiteral994, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Object VoxelBusters.Utility.JSONUtility::FromJSON(System.String)
extern TypeInfo* JSONReader_t8_56_il2cpp_TypeInfo_var;
extern "C" Object_t * JSONUtility_FromJSON_m8_298 (Object_t * __this /* static, unused */, String_t* ____inputJSONString, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONReader_t8_56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1967);
		s_Il2CppMethodIntialized = true;
	}
	JSONReader_t8_56 * V_0 = {0};
	{
		String_t* L_0 = ____inputJSONString;
		JSONReader_t8_56 * L_1 = (JSONReader_t8_56 *)il2cpp_codegen_object_new (JSONReader_t8_56_il2cpp_TypeInfo_var);
		JSONReader__ctor_m8_281(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONReader_t8_56 * L_2 = V_0;
		NullCheck(L_2);
		Object_t * L_3 = JSONReader_Deserialise_m8_284(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Object VoxelBusters.Utility.JSONUtility::FromJSON(System.String,System.Int32&)
extern TypeInfo* JSONReader_t8_56_il2cpp_TypeInfo_var;
extern "C" Object_t * JSONUtility_FromJSON_m8_299 (Object_t * __this /* static, unused */, String_t* ____inputJSONString, int32_t* ____errorIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JSONReader_t8_56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1967);
		s_Il2CppMethodIntialized = true;
	}
	JSONReader_t8_56 * V_0 = {0};
	{
		String_t* L_0 = ____inputJSONString;
		JSONReader_t8_56 * L_1 = (JSONReader_t8_56 *)il2cpp_codegen_object_new (JSONReader_t8_56_il2cpp_TypeInfo_var);
		JSONReader__ctor_m8_281(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JSONReader_t8_56 * L_2 = V_0;
		int32_t* L_3 = ____errorIndex;
		NullCheck(L_2);
		Object_t * L_4 = JSONReader_Deserialise_m8_285(L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::.ctor(System.Int32)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" void JSONWriter__ctor_m8_300 (JSONWriter_t8_58 * __this, int32_t ____bufferLength, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ____bufferLength;
		StringBuilder_t1_247 * L_1 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12415(L_1, L_0, /*hidden argument*/NULL);
		JSONWriter_set_StringBuilder_m8_302(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Text.StringBuilder VoxelBusters.Utility.JSONWriter::get_StringBuilder()
extern "C" StringBuilder_t1_247 * JSONWriter_get_StringBuilder_m8_301 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = (__this->___U3CStringBuilderU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::set_StringBuilder(System.Text.StringBuilder)
extern "C" void JSONWriter_set_StringBuilder_m8_302 (JSONWriter_t8_58 * __this, StringBuilder_t1_247 * ___value, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = ___value;
		__this->___U3CStringBuilderU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.String VoxelBusters.Utility.JSONWriter::Serialise(System.Object)
extern "C" String_t* JSONWriter_Serialise_m8_303 (JSONWriter_t8_58 * __this, Object_t * ____objectValue, const MethodInfo* method)
{
	{
		Object_t * L_0 = ____objectValue;
		JSONWriter_WriteObjectValue_m8_304(__this, L_0, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_1 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = StringBuilder_ToString_m1_12428(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteObjectValue(System.Object)
extern TypeInfo* Array_t_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void JSONWriter_WriteObjectValue_m8_304 (JSONWriter_t8_58 * __this, Object_t * ____objectVal, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Array_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(69);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	{
		Object_t * L_0 = ____objectVal;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		JSONWriter_WriteNullValue_m8_310(__this, /*hidden argument*/NULL);
		return;
	}

IL_000d:
	{
		Object_t * L_1 = ____objectVal;
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m1_5(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Type_t * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(141 /* System.Boolean System.Type::get_IsPrimitive() */, L_3);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		Object_t * L_5 = ____objectVal;
		JSONWriter_WritePrimitive_m8_308(__this, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		Type_t * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(127 /* System.Boolean System.Type::get_IsEnum() */, L_6);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		Object_t * L_8 = ____objectVal;
		JSONWriter_WriteEnum_m8_309(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		Type_t * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = (bool)VirtFuncInvoker0< bool >::Invoke(120 /* System.Boolean System.Type::get_IsArray() */, L_9);
		if (!L_10)
		{
			goto IL_0052;
		}
	}
	{
		Object_t * L_11 = ____objectVal;
		JSONWriter_WriteArray_m8_306(__this, ((Array_t *)IsInstClass(L_11, Array_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}

IL_0052:
	{
		Object_t * L_12 = ____objectVal;
		if (!((Object_t *)IsInst(L_12, IList_t1_262_il2cpp_TypeInfo_var)))
		{
			goto IL_006a;
		}
	}
	{
		Object_t * L_13 = ____objectVal;
		JSONWriter_WriteList_m8_307(__this, ((Object_t *)IsInst(L_13, IList_t1_262_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}

IL_006a:
	{
		Object_t * L_14 = ____objectVal;
		if (!((Object_t *)IsInst(L_14, IDictionary_t1_35_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		Object_t * L_15 = ____objectVal;
		JSONWriter_WriteDictionary_m8_305(__this, ((Object_t *)IsInst(L_15, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}

IL_0082:
	{
		Object_t * L_16 = ____objectVal;
		NullCheck(L_16);
		String_t* L_17 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		JSONWriter_WriteString_m8_311(__this, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteDictionary(System.Collections.IDictionary)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern "C" void JSONWriter_WriteDictionary_m8_305 (JSONWriter_t8_58 * __this, Object_t * ____dict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * V_1 = {0};
	{
		V_0 = 1;
		Object_t * L_0 = ____dict;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(9 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_0);
		V_1 = L_1;
		StringBuilder_t1_247 * L_2 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m1_12452(L_2, ((int32_t)123), /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		goto IL_0037;
	}

IL_0029:
	{
		StringBuilder_t1_247 * L_4 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		StringBuilder_Append_m1_12452(L_4, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0037:
	{
		Object_t * L_5 = V_1;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(1 /* System.Object System.Collections.IDictionaryEnumerator::get_Key() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		JSONWriter_WriteString_m8_311(__this, L_7, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_8 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)58), /*hidden argument*/NULL);
		Object_t * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionaryEnumerator::get_Value() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, L_9);
		JSONWriter_WriteObjectValue_m8_304(__this, L_10, /*hidden argument*/NULL);
	}

IL_0062:
	{
		Object_t * L_11 = V_1;
		NullCheck(L_11);
		bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_11);
		if (L_12)
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t1_247 * L_13 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		StringBuilder_Append_m1_12452(L_13, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteArray(System.Array)
extern "C" void JSONWriter_WriteArray_m8_306 (JSONWriter_t8_58 * __this, Array_t * ____array, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)91), /*hidden argument*/NULL);
		Array_t * L_1 = ____array;
		NullCheck(L_1);
		int32_t L_2 = Array_get_Rank_m1_994(L_1, /*hidden argument*/NULL);
		V_6 = L_2;
		int32_t L_3 = V_6;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = V_6;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_006a;
		}
	}
	{
		goto IL_0101;
	}

IL_002b:
	{
		Array_t * L_5 = ____array;
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m1_992(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		V_1 = 0;
		goto IL_005e;
	}

IL_0039:
	{
		int32_t L_7 = V_1;
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		StringBuilder_t1_247 * L_8 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_004d:
	{
		Array_t * L_9 = ____array;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		Object_t * L_11 = Array_GetValue_m1_1011(L_9, L_10, /*hidden argument*/NULL);
		JSONWriter_WriteObjectValue_m8_304(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_13 = V_1;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0101;
	}

IL_006a:
	{
		Array_t * L_15 = ____array;
		NullCheck(L_15);
		int32_t L_16 = Array_GetLength_m1_996(L_15, 0, /*hidden argument*/NULL);
		V_2 = L_16;
		Array_t * L_17 = ____array;
		NullCheck(L_17);
		int32_t L_18 = Array_GetLength_m1_996(L_17, 1, /*hidden argument*/NULL);
		V_3 = L_18;
		V_4 = 0;
		goto IL_00f4;
	}

IL_0082:
	{
		int32_t L_19 = V_4;
		if (!L_19)
		{
			goto IL_0097;
		}
	}
	{
		StringBuilder_t1_247 * L_20 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		StringBuilder_Append_m1_12452(L_20, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0097:
	{
		StringBuilder_t1_247 * L_21 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m1_12452(L_21, ((int32_t)91), /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_00d8;
	}

IL_00ad:
	{
		int32_t L_22 = V_5;
		if (!L_22)
		{
			goto IL_00c2;
		}
	}
	{
		StringBuilder_t1_247 * L_23 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		StringBuilder_Append_m1_12452(L_23, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_00c2:
	{
		Array_t * L_24 = ____array;
		int32_t L_25 = V_4;
		int32_t L_26 = V_5;
		NullCheck(L_24);
		Object_t * L_27 = Array_GetValue_m1_1012(L_24, L_25, L_26, /*hidden argument*/NULL);
		JSONWriter_WriteObjectValue_m8_304(__this, L_27, /*hidden argument*/NULL);
		int32_t L_28 = V_5;
		V_5 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00d8:
	{
		int32_t L_29 = V_5;
		int32_t L_30 = V_3;
		if ((((int32_t)L_29) < ((int32_t)L_30)))
		{
			goto IL_00ad;
		}
	}
	{
		StringBuilder_t1_247 * L_31 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_31);
		StringBuilder_Append_m1_12452(L_31, ((int32_t)93), /*hidden argument*/NULL);
		int32_t L_32 = V_4;
		V_4 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00f4:
	{
		int32_t L_33 = V_4;
		int32_t L_34 = V_2;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0082;
		}
	}
	{
		goto IL_0101;
	}

IL_0101:
	{
		StringBuilder_t1_247 * L_35 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		StringBuilder_Append_m1_12452(L_35, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteList(System.Collections.IList)
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern "C" void JSONWriter_WriteList_m8_307 (JSONWriter_t8_58 * __this, Object_t * ____list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object_t * L_0 = ____list;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t1_280_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		StringBuilder_t1_247 * L_2 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m1_12452(L_2, ((int32_t)91), /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0041;
	}

IL_001c:
	{
		int32_t L_3 = V_1;
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		StringBuilder_t1_247 * L_4 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		StringBuilder_Append_m1_12452(L_4, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0030:
	{
		Object_t * L_5 = ____list;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Object_t * L_7 = (Object_t *)InterfaceFuncInvoker1< Object_t *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1_262_il2cpp_TypeInfo_var, L_5, L_6);
		JSONWriter_WriteObjectValue_m8_304(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t1_247 * L_11 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Append_m1_12452(L_11, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WritePrimitive(System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2147;
extern Il2CppCodeGenString* _stringLiteral3328;
extern "C" void JSONWriter_WritePrimitive_m8_308 (JSONWriter_t8_58 * __this, Object_t * ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		_stringLiteral2147 = il2cpp_codegen_string_literal_from_index(2147);
		_stringLiteral3328 = il2cpp_codegen_string_literal_from_index(3328);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ____value;
		if (!((Object_t *)IsInstSealed(L_0, Boolean_t1_20_il2cpp_TypeInfo_var)))
		{
			goto IL_0042;
		}
	}
	{
		Object_t * L_1 = ____value;
		if (!((*(bool*)((bool*)UnBox (L_1, Boolean_t1_20_il2cpp_TypeInfo_var)))))
		{
			goto IL_002c;
		}
	}
	{
		StringBuilder_t1_247 * L_2 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m1_12438(L_2, _stringLiteral2147, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_002c:
	{
		StringBuilder_t1_247 * L_3 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		StringBuilder_Append_m1_12438(L_3, _stringLiteral3328, /*hidden argument*/NULL);
	}

IL_003d:
	{
		goto IL_007f;
	}

IL_0042:
	{
		Object_t * L_4 = ____value;
		if (!((Object_t *)IsInstSealed(L_4, Char_t1_15_il2cpp_TypeInfo_var)))
		{
			goto IL_0072;
		}
	}
	{
		StringBuilder_t1_247 * L_5 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_t1_247 * L_6 = StringBuilder_Append_m1_12452(L_5, ((int32_t)34), /*hidden argument*/NULL);
		Object_t * L_7 = ____value;
		NullCheck(L_6);
		StringBuilder_t1_247 * L_8 = StringBuilder_Append_m1_12452(L_6, ((*(uint16_t*)((uint16_t*)UnBox (L_7, Char_t1_15_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_Append_m1_12452(L_8, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0072:
	{
		StringBuilder_t1_247 * L_9 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		Object_t * L_10 = ____value;
		NullCheck(L_9);
		StringBuilder_Append_m1_12446(L_9, L_10, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteEnum(System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void JSONWriter_WriteEnum_m8_309 (JSONWriter_t8_58 * __this, Object_t * ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		Object_t * L_1 = ____value;
		NullCheck(L_0);
		StringBuilder_Append_m1_12444(L_0, ((*(int32_t*)((int32_t*)UnBox (L_1, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteNullValue()
extern Il2CppCodeGenString* _stringLiteral994;
extern "C" void JSONWriter_WriteNullValue_m8_310 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral994 = il2cpp_codegen_string_literal_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12438(L_0, _stringLiteral994, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5198;
extern Il2CppCodeGenString* _stringLiteral160;
extern "C" void JSONWriter_WriteString_m8_311 (JSONWriter_t8_58 * __this, String_t* ____stringVal, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5198 = il2cpp_codegen_string_literal_from_index(5198);
		_stringLiteral160 = il2cpp_codegen_string_literal_from_index(160);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	{
		String_t* L_0 = ____stringVal;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		StringBuilder_t1_247 * L_2 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m1_12452(L_2, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_001c:
	{
		String_t* L_3 = ____stringVal;
		int32_t L_4 = V_1;
		int32_t L_5 = L_4;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
		NullCheck(L_3);
		uint16_t L_6 = String_get_Chars_m1_442(L_3, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		uint16_t L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_004a;
		}
	}
	{
		StringBuilder_t1_247 * L_8 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		StringBuilder_t1_247 * L_9 = StringBuilder_Append_m1_12452(L_8, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_9);
		StringBuilder_Append_m1_12452(L_9, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_004a:
	{
		uint16_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_006c;
		}
	}
	{
		StringBuilder_t1_247 * L_11 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_t1_247 * L_12 = StringBuilder_Append_m1_12452(L_11, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_12);
		StringBuilder_Append_m1_12452(L_12, ((int32_t)92), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_006c:
	{
		uint16_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_008e;
		}
	}
	{
		StringBuilder_t1_247 * L_14 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		StringBuilder_t1_247 * L_15 = StringBuilder_Append_m1_12452(L_14, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_15);
		StringBuilder_Append_m1_12452(L_15, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_008e:
	{
		uint16_t L_16 = V_2;
		if ((!(((uint32_t)L_16) == ((uint32_t)8))))
		{
			goto IL_00af;
		}
	}
	{
		StringBuilder_t1_247 * L_17 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		StringBuilder_t1_247 * L_18 = StringBuilder_Append_m1_12452(L_17, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_18);
		StringBuilder_Append_m1_12452(L_18, ((int32_t)98), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_00af:
	{
		uint16_t L_19 = V_2;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_00d1;
		}
	}
	{
		StringBuilder_t1_247 * L_20 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		StringBuilder_t1_247 * L_21 = StringBuilder_Append_m1_12452(L_20, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m1_12452(L_21, ((int32_t)102), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_00d1:
	{
		uint16_t L_22 = V_2;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00f3;
		}
	}
	{
		StringBuilder_t1_247 * L_23 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		StringBuilder_t1_247 * L_24 = StringBuilder_Append_m1_12452(L_23, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m1_12452(L_24, ((int32_t)110), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_00f3:
	{
		uint16_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0115;
		}
	}
	{
		StringBuilder_t1_247 * L_26 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_t1_247 * L_27 = StringBuilder_Append_m1_12452(L_26, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_27);
		StringBuilder_Append_m1_12452(L_27, ((int32_t)114), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_0115:
	{
		uint16_t L_28 = V_2;
		if ((!(((uint32_t)L_28) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0137;
		}
	}
	{
		StringBuilder_t1_247 * L_29 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		StringBuilder_t1_247 * L_30 = StringBuilder_Append_m1_12452(L_29, ((int32_t)92), /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m1_12452(L_30, ((int32_t)116), /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_0137:
	{
		uint16_t L_31 = V_2;
		if ((((int32_t)L_31) <= ((int32_t)((int32_t)127))))
		{
			goto IL_016b;
		}
	}
	{
		uint16_t L_32 = V_2;
		V_4 = L_32;
		String_t* L_33 = Int32_ToString_m1_103((&V_4), _stringLiteral160, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5198, L_33, /*hidden argument*/NULL);
		V_3 = L_34;
		StringBuilder_t1_247 * L_35 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		String_t* L_36 = V_3;
		NullCheck(L_35);
		StringBuilder_Append_m1_12438(L_35, L_36, /*hidden argument*/NULL);
		goto IL_0178;
	}

IL_016b:
	{
		StringBuilder_t1_247 * L_37 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		uint16_t L_38 = V_2;
		NullCheck(L_37);
		StringBuilder_Append_m1_12452(L_37, L_38, /*hidden argument*/NULL);
	}

IL_0178:
	{
		int32_t L_39 = V_1;
		int32_t L_40 = V_0;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t1_247 * L_41 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		StringBuilder_Append_m1_12452(L_41, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteDictionaryStart()
extern "C" void JSONWriter_WriteDictionaryStart_m8_312 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)123), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteKeyValuePair(System.String,System.Object,System.Boolean)
extern "C" void JSONWriter_WriteKeyValuePair_m8_313 (JSONWriter_t8_58 * __this, String_t* ____key, Object_t * ____value, bool ____appendSeperator, const MethodInfo* method)
{
	{
		String_t* L_0 = ____key;
		JSONWriter_WriteString_m8_311(__this, L_0, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_1 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m1_12452(L_1, ((int32_t)58), /*hidden argument*/NULL);
		Object_t * L_2 = ____value;
		JSONWriter_WriteObjectValue_m8_304(__this, L_2, /*hidden argument*/NULL);
		bool L_3 = ____appendSeperator;
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		StringBuilder_t1_247 * L_4 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		StringBuilder_Append_m1_12452(L_4, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteKeyValuePairSeperator()
extern "C" void JSONWriter_WriteKeyValuePairSeperator_m8_314 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)58), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteDictionaryEnd()
extern "C" void JSONWriter_WriteDictionaryEnd_m8_315 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteArrayStart()
extern "C" void JSONWriter_WriteArrayStart_m8_316 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)91), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteArrayEnd()
extern "C" void JSONWriter_WriteArrayEnd_m8_317 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.JSONWriter::WriteElementSeperator()
extern "C" void JSONWriter_WriteElementSeperator_m8_318 (JSONWriter_t8_58 * __this, const MethodInfo* method)
{
	{
		StringBuilder_t1_247 * L_0 = JSONWriter_get_StringBuilder_m8_301(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Append_m1_12452(L_0, ((int32_t)44), /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::.ctor()
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern "C" void Plist__ctor_m8_319 (Plist_t8_51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2__ctor_m1_15037(__this, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		return;
	}
}
// VoxelBusters.Utility.Plist VoxelBusters.Utility.Plist::LoadPlistAtPath(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Plist_t8_51_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5199;
extern "C" Plist_t8_51 * Plist_LoadPlistAtPath_m8_320 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Plist_t8_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1968);
		_stringLiteral5199 = il2cpp_codegen_string_literal_from_index(5199);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Plist_t8_51 * V_1 = {0};
	{
		String_t* L_0 = ____filePath;
		bool L_1 = File_Exists_m1_4857(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_2 = ____filePath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5199, L_2, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return (Plist_t8_51 *)NULL;
	}

IL_001d:
	{
		String_t* L_4 = ____filePath;
		String_t* L_5 = File_ReadAllText_m1_4886(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Plist_t8_51 * L_6 = (Plist_t8_51 *)il2cpp_codegen_object_new (Plist_t8_51_il2cpp_TypeInfo_var);
		Plist__ctor_m8_319(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		Plist_t8_51 * L_7 = V_1;
		String_t* L_8 = V_0;
		NullCheck(L_7);
		Plist_ParsePlistText_m8_322(L_7, L_8, /*hidden argument*/NULL);
		Plist_t8_51 * L_9 = V_1;
		return L_9;
	}
}
// VoxelBusters.Utility.Plist VoxelBusters.Utility.Plist::Load(System.String)
extern TypeInfo* Plist_t8_51_il2cpp_TypeInfo_var;
extern "C" Plist_t8_51 * Plist_Load_m8_321 (Object_t * __this /* static, unused */, String_t* ____plistTextContents, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Plist_t8_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1968);
		s_Il2CppMethodIntialized = true;
	}
	Plist_t8_51 * V_0 = {0};
	{
		Plist_t8_51 * L_0 = (Plist_t8_51 *)il2cpp_codegen_object_new (Plist_t8_51_il2cpp_TypeInfo_var);
		Plist__ctor_m8_319(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Plist_t8_51 * L_1 = V_0;
		String_t* L_2 = ____plistTextContents;
		NullCheck(L_1);
		Plist_ParsePlistText_m8_322(L_1, L_2, /*hidden argument*/NULL);
		Plist_t8_51 * L_3 = V_0;
		return L_3;
	}
}
// System.Void VoxelBusters.Utility.Plist::ParsePlistText(System.String)
extern TypeInfo* XmlDocument_t4_123_il2cpp_TypeInfo_var;
extern "C" void Plist_ParsePlistText_m8_322 (Plist_t8_51 * __this, String_t* ____text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		XmlDocument_t4_123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1319);
		s_Il2CppMethodIntialized = true;
	}
	XmlDocument_t4_123 * V_0 = {0};
	XmlNode_t4_116 * V_1 = {0};
	XmlNode_t4_116 * V_2 = {0};
	Object_t * V_3 = {0};
	{
		XmlDocument_t4_123 * L_0 = (XmlDocument_t4_123 *)il2cpp_codegen_object_new (XmlDocument_t4_123_il2cpp_TypeInfo_var);
		XmlDocument__ctor_m4_397(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		XmlDocument_t4_123 * L_1 = V_0;
		String_t* L_2 = ____text;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(53 /* System.Void System.Xml.XmlDocument::LoadXml(System.String) */, L_1, L_2);
		XmlDocument_t4_123 * L_3 = V_0;
		NullCheck(L_3);
		XmlNode_t4_116 * L_4 = (XmlNode_t4_116 *)VirtFuncInvoker0< XmlNode_t4_116 * >::Invoke(14 /* System.Xml.XmlNode System.Xml.XmlNode::get_LastChild() */, L_3);
		V_1 = L_4;
		XmlNode_t4_116 * L_5 = V_1;
		NullCheck(L_5);
		XmlNode_t4_116 * L_6 = (XmlNode_t4_116 *)VirtFuncInvoker0< XmlNode_t4_116 * >::Invoke(10 /* System.Xml.XmlNode System.Xml.XmlNode::get_FirstChild() */, L_5);
		V_2 = L_6;
		V_3 = __this;
		XmlNode_t4_116 * L_7 = V_2;
		if (!L_7)
		{
			goto IL_002c;
		}
	}
	{
		XmlNode_t4_116 * L_8 = V_2;
		Plist_ParseDictionaryNode_m8_325(__this, L_8, (&V_3), /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Object VoxelBusters.Utility.Plist::ParseValueNode(System.Xml.XmlNode)
extern TypeInfo* Plist_t8_51_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1894_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_15034_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2147;
extern Il2CppCodeGenString* _stringLiteral3328;
extern Il2CppCodeGenString* _stringLiteral2260;
extern Il2CppCodeGenString* _stringLiteral5200;
extern Il2CppCodeGenString* _stringLiteral309;
extern Il2CppCodeGenString* _stringLiteral5201;
extern Il2CppCodeGenString* _stringLiteral173;
extern "C" Object_t * Plist_ParseValueNode_m8_323 (Plist_t8_51 * __this, XmlNode_t4_116 * ____valNode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Plist_t8_51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1968);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		List_1_t1_1894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1958);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		List_1__ctor_m1_15034_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484121);
		_stringLiteral2147 = il2cpp_codegen_string_literal_from_index(2147);
		_stringLiteral3328 = il2cpp_codegen_string_literal_from_index(3328);
		_stringLiteral2260 = il2cpp_codegen_string_literal_from_index(2260);
		_stringLiteral5200 = il2cpp_codegen_string_literal_from_index(5200);
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		_stringLiteral5201 = il2cpp_codegen_string_literal_from_index(5201);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	String_t* V_2 = {0};
	Dictionary_2_t1_92 * V_3 = {0};
	int32_t V_4 = 0;
	{
		XmlNode_t4_116 * L_0 = ____valNode;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(16 /* System.String System.Xml.XmlNode::get_Name() */, L_0);
		V_2 = L_1;
		String_t* L_2 = V_2;
		if (!L_2)
		{
			goto IL_010b;
		}
	}
	{
		Dictionary_2_t1_92 * L_3 = ((Plist_t8_51_StaticFields*)Plist_t8_51_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_16;
		if (L_3)
		{
			goto IL_0078;
		}
	}
	{
		Dictionary_2_t1_92 * L_4 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_4, 7, /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_3 = L_4;
		Dictionary_2_t1_92 * L_5 = V_3;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral2147, 0);
		Dictionary_2_t1_92 * L_6 = V_3;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral3328, 1);
		Dictionary_2_t1_92 * L_7 = V_3;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral2260, 2);
		Dictionary_2_t1_92 * L_8 = V_3;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral5200, 3);
		Dictionary_2_t1_92 * L_9 = V_3;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral309, 4);
		Dictionary_2_t1_92 * L_10 = V_3;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral5201, 5);
		Dictionary_2_t1_92 * L_11 = V_3;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral173, 6);
		Dictionary_2_t1_92 * L_12 = V_3;
		((Plist_t8_51_StaticFields*)Plist_t8_51_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_16 = L_12;
	}

IL_0078:
	{
		Dictionary_2_t1_92 * L_13 = ((Plist_t8_51_StaticFields*)Plist_t8_51_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_16;
		String_t* L_14 = V_2;
		NullCheck(L_13);
		bool L_15 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_13, L_14, (&V_4));
		if (!L_15)
		{
			goto IL_010b;
		}
	}
	{
		int32_t L_16 = V_4;
		if (L_16 == 0)
		{
			goto IL_00b2;
		}
		if (L_16 == 1)
		{
			goto IL_00b9;
		}
		if (L_16 == 2)
		{
			goto IL_00c0;
		}
		if (L_16 == 3)
		{
			goto IL_00d1;
		}
		if (L_16 == 4)
		{
			goto IL_00e2;
		}
		if (L_16 == 5)
		{
			goto IL_00e9;
		}
		if (L_16 == 6)
		{
			goto IL_00fa;
		}
	}
	{
		goto IL_010b;
	}

IL_00b2:
	{
		bool L_17 = 1;
		Object_t * L_18 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_17);
		return L_18;
	}

IL_00b9:
	{
		bool L_19 = 0;
		Object_t * L_20 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_19);
		return L_20;
	}

IL_00c0:
	{
		XmlNode_t4_116 * L_21 = ____valNode;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_21);
		int32_t L_23 = Int32_Parse_m1_97(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		int32_t L_24 = L_23;
		Object_t * L_25 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_24);
		return L_25;
	}

IL_00d1:
	{
		XmlNode_t4_116 * L_26 = ____valNode;
		NullCheck(L_26);
		String_t* L_27 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_26);
		float L_28 = Single_Parse_m1_627(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		float L_29 = L_28;
		Object_t * L_30 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_29);
		return L_30;
	}

IL_00e2:
	{
		XmlNode_t4_116 * L_31 = ____valNode;
		NullCheck(L_31);
		String_t* L_32 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_31);
		return L_32;
	}

IL_00e9:
	{
		Dictionary_2_t1_1903 * L_33 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_33, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		V_0 = L_33;
		XmlNode_t4_116 * L_34 = ____valNode;
		Plist_ParseDictionaryNode_m8_325(__this, L_34, (&V_0), /*hidden argument*/NULL);
		Object_t * L_35 = V_0;
		return L_35;
	}

IL_00fa:
	{
		List_1_t1_1894 * L_36 = (List_1_t1_1894 *)il2cpp_codegen_object_new (List_1_t1_1894_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15034(L_36, /*hidden argument*/List_1__ctor_m1_15034_MethodInfo_var);
		V_1 = L_36;
		XmlNode_t4_116 * L_37 = ____valNode;
		Plist_ParseListNode_m8_324(__this, L_37, (&V_1), /*hidden argument*/NULL);
		Object_t * L_38 = V_1;
		return L_38;
	}

IL_010b:
	{
		return NULL;
	}
}
// System.Void VoxelBusters.Utility.Plist::ParseListNode(System.Xml.XmlNode,System.Collections.IList&)
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* XmlNode_t4_116_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void Plist_ParseListNode_m8_324 (Plist_t8_51 * __this, XmlNode_t4_116 * ____listNode, Object_t ** ____parsedList, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		XmlNode_t4_116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1310);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	XmlNode_t4_116 * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		XmlNode_t4_116 * L_0 = ____listNode;
		NullCheck(L_0);
		XmlNodeList_t4_147 * L_1 = (XmlNodeList_t4_147 *)VirtFuncInvoker0< XmlNodeList_t4_147 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_0);
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(7 /* System.Collections.IEnumerator System.Xml.XmlNodeList::GetEnumerator() */, L_1);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			Object_t * L_3 = V_1;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_3);
			V_0 = ((XmlNode_t4_116 *)CastclassClass(L_4, XmlNode_t4_116_il2cpp_TypeInfo_var));
			Object_t ** L_5 = ____parsedList;
			XmlNode_t4_116 * L_6 = V_0;
			Object_t * L_7 = Plist_ParseValueNode_m8_323(__this, L_6, /*hidden argument*/NULL);
			NullCheck((*((Object_t **)L_5)));
			InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1_262_il2cpp_TypeInfo_var, (*((Object_t **)L_5)), L_7);
		}

IL_002c:
		{
			Object_t * L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		{
			Object_t * L_10 = V_1;
			V_2 = ((Object_t *)IsInst(L_10, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_11 = V_2;
			if (L_11)
			{
				goto IL_0047;
			}
		}

IL_0046:
		{
			IL2CPP_END_FINALLY(60)
		}

IL_0047:
		{
			Object_t * L_12 = V_2;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_12);
			IL2CPP_END_FINALLY(60)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_004e:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::ParseDictionaryNode(System.Xml.XmlNode,System.Collections.IDictionary&)
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void Plist_ParseDictionaryNode_m8_325 (Plist_t8_51 * __this, XmlNode_t4_116 * ____dictNode, Object_t ** ____parsedDict, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	XmlNode_t4_116 * V_2 = {0};
	XmlNode_t4_116 * V_3 = {0};
	{
		XmlNode_t4_116 * L_0 = ____dictNode;
		NullCheck(L_0);
		XmlNodeList_t4_147 * L_1 = (XmlNodeList_t4_147 *)VirtFuncInvoker0< XmlNodeList_t4_147 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Xml.XmlNodeList::get_Count() */, L_1);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0047;
	}

IL_0013:
	{
		XmlNode_t4_116 * L_3 = ____dictNode;
		NullCheck(L_3);
		XmlNodeList_t4_147 * L_4 = (XmlNodeList_t4_147 *)VirtFuncInvoker0< XmlNodeList_t4_147 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_3);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		XmlNode_t4_116 * L_6 = (XmlNode_t4_116 *)VirtFuncInvoker1< XmlNode_t4_116 *, int32_t >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNodeList::Item(System.Int32) */, L_4, L_5);
		V_2 = L_6;
		XmlNode_t4_116 * L_7 = ____dictNode;
		NullCheck(L_7);
		XmlNodeList_t4_147 * L_8 = (XmlNodeList_t4_147 *)VirtFuncInvoker0< XmlNodeList_t4_147 * >::Invoke(9 /* System.Xml.XmlNodeList System.Xml.XmlNode::get_ChildNodes() */, L_7);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		XmlNode_t4_116 * L_10 = (XmlNode_t4_116 *)VirtFuncInvoker1< XmlNode_t4_116 *, int32_t >::Invoke(8 /* System.Xml.XmlNode System.Xml.XmlNodeList::Item(System.Int32) */, L_8, ((int32_t)((int32_t)L_9+(int32_t)1)));
		V_3 = L_10;
		Object_t ** L_11 = ____parsedDict;
		XmlNode_t4_116 * L_12 = V_2;
		NullCheck(L_12);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(12 /* System.String System.Xml.XmlNode::get_InnerText() */, L_12);
		XmlNode_t4_116 * L_14 = V_3;
		Object_t * L_15 = Plist_ParseValueNode_m8_323(__this, L_14, /*hidden argument*/NULL);
		NullCheck((*((Object_t **)L_11)));
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, (*((Object_t **)L_11)), L_13, L_15);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)2));
	}

IL_0047:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Object VoxelBusters.Utility.Plist::GetKeyPathValue(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1_257_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5202;
extern "C" Object_t * Plist_GetKeyPathValue_m8_326 (Plist_t8_51 * __this, String_t* ____keyPath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		KeyNotFoundException_t1_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1969);
		_stringLiteral5202 = il2cpp_codegen_string_literal_from_index(5202);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	Object_t * V_5 = {0};
	KeyNotFoundException_t1_257 * V_6 = {0};
	Object_t * V_7 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ____keyPath;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		V_0 = __this;
		String_t* L_2 = ____keyPath;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		StringU5BU5D_t1_238* L_5 = V_1;
		NullCheck(L_5);
		V_2 = (((int32_t)((int32_t)(((Array_t *)L_5)->max_length))));
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		{
			V_3 = 0;
			goto IL_0062;
		}

IL_002c:
		{
			StringU5BU5D_t1_238* L_6 = V_1;
			int32_t L_7 = V_3;
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
			int32_t L_8 = L_7;
			V_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_8, sizeof(String_t*)));
			Object_t * L_9 = V_0;
			V_5 = ((Object_t *)IsInst(L_9, IDictionary_t1_35_il2cpp_TypeInfo_var));
			Object_t * L_10 = V_5;
			if (!L_10)
			{
				goto IL_004e;
			}
		}

IL_0040:
		{
			Object_t * L_11 = V_5;
			String_t* L_12 = V_4;
			NullCheck(L_11);
			bool L_13 = (bool)InterfaceFuncInvoker1< bool, Object_t * >::Invoke(8 /* System.Boolean System.Collections.IDictionary::Contains(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_11, L_12);
			if (L_13)
			{
				goto IL_0054;
			}
		}

IL_004e:
		{
			KeyNotFoundException_t1_257 * L_14 = (KeyNotFoundException_t1_257 *)il2cpp_codegen_object_new (KeyNotFoundException_t1_257_il2cpp_TypeInfo_var);
			KeyNotFoundException__ctor_m1_2781(L_14, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
		}

IL_0054:
		{
			Object_t * L_15 = V_5;
			String_t* L_16 = V_4;
			NullCheck(L_15);
			Object_t * L_17 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_15, L_16);
			V_0 = L_17;
			int32_t L_18 = V_3;
			V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
		}

IL_0062:
		{
			int32_t L_19 = V_3;
			int32_t L_20 = V_2;
			if ((((int32_t)L_19) < ((int32_t)L_20)))
			{
				goto IL_002c;
			}
		}

IL_0069:
		{
			Object_t * L_21 = V_0;
			V_7 = L_21;
			goto IL_009b;
		}

IL_0071:
		{
			; // IL_0071: leave IL_009b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (KeyNotFoundException_t1_257_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0076;
		throw e;
	}

CATCH_0076:
	{ // begin catch(System.Collections.Generic.KeyNotFoundException)
		{
			V_6 = ((KeyNotFoundException_t1_257 *)__exception_local);
			KeyNotFoundException_t1_257 * L_22 = V_6;
			NullCheck(L_22);
			String_t* L_23 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_22);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Concat_m1_559(NULL /*static, unused*/, _stringLiteral5202, L_23, /*hidden argument*/NULL);
			Debug_LogWarning_m6_636(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
			V_7 = NULL;
			goto IL_009b;
		}

IL_0096:
		{
			; // IL_0096: leave IL_009b
		}
	} // end catch (depth: 1)

IL_009b:
	{
		Object_t * L_25 = V_7;
		return L_25;
	}
}
// System.Void VoxelBusters.Utility.Plist::AddValue(System.String,System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void Plist_AddValue_m8_327 (Plist_t8_51 * __this, String_t* ____keyPath, Object_t * ____newValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	String_t* V_1 = {0};
	String_t* V_2 = {0};
	Object_t * V_3 = {0};
	{
		String_t* L_0 = ____keyPath;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		____keyPath = L_1;
	}

IL_000d:
	{
		String_t* L_2 = ____keyPath;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		StringU5BU5D_t1_238* L_5 = V_0;
		StringU5BU5D_t1_238* L_6 = V_0;
		NullCheck(L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))-(int32_t)1)));
		int32_t L_7 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))-(int32_t)1));
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(L_5, L_7, sizeof(String_t*)));
		String_t* L_8 = ____keyPath;
		String_t* L_9 = ____keyPath;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1_571(L_9, /*hidden argument*/NULL);
		String_t* L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m1_571(L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_13 = String_Substring_m1_455(L_8, 0, ((int32_t)((int32_t)L_10-(int32_t)L_12)), /*hidden argument*/NULL);
		CharU5BU5D_t1_16* L_14 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck(L_13);
		String_t* L_15 = String_TrimEnd_m1_460(L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		String_t* L_16 = V_2;
		Object_t * L_17 = Plist_GetKeyPathValue_m8_326(__this, L_16, /*hidden argument*/NULL);
		V_3 = ((Object_t *)IsInst(L_17, IDictionary_t1_35_il2cpp_TypeInfo_var));
		Object_t * L_18 = V_3;
		if (!L_18)
		{
			goto IL_0067;
		}
	}
	{
		Object_t * L_19 = V_3;
		String_t* L_20 = V_1;
		Object_t * L_21 = ____newValue;
		NullCheck(L_19);
		InterfaceActionInvoker2< Object_t *, Object_t * >::Invoke(3 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_19, L_20, L_21);
	}

IL_0067:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::Save(System.String)
extern TypeInfo* StreamWriter_t1_448_il2cpp_TypeInfo_var;
extern TypeInfo* MemoryStream_t1_433_il2cpp_TypeInfo_var;
extern TypeInfo* XmlWriterSettings_t4_187_il2cpp_TypeInfo_var;
extern TypeInfo* UTF8Encoding_t1_1453_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5203;
extern Il2CppCodeGenString* _stringLiteral5204;
extern Il2CppCodeGenString* _stringLiteral5205;
extern Il2CppCodeGenString* _stringLiteral1921;
extern Il2CppCodeGenString* _stringLiteral3878;
extern "C" void Plist_Save_m8_328 (Plist_t8_51 * __this, String_t* ____saveToPath, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StreamWriter_t1_448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		MemoryStream_t1_433_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		XmlWriterSettings_t4_187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1372);
		UTF8Encoding_t1_1453_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral5203 = il2cpp_codegen_string_literal_from_index(5203);
		_stringLiteral5204 = il2cpp_codegen_string_literal_from_index(5204);
		_stringLiteral5205 = il2cpp_codegen_string_literal_from_index(5205);
		_stringLiteral1921 = il2cpp_codegen_string_literal_from_index(1921);
		_stringLiteral3878 = il2cpp_codegen_string_literal_from_index(3878);
		s_Il2CppMethodIntialized = true;
	}
	StreamWriter_t1_448 * V_0 = {0};
	MemoryStream_t1_433 * V_1 = {0};
	XmlWriterSettings_t4_187 * V_2 = {0};
	XmlWriter_t4_182 * V_3 = {0};
	String_t* V_4 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ____saveToPath;
		StreamWriter_t1_448 * L_1 = (StreamWriter_t1_448 *)il2cpp_codegen_object_new (StreamWriter_t1_448_il2cpp_TypeInfo_var);
		StreamWriter__ctor_m1_5240(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			MemoryStream_t1_433 * L_2 = (MemoryStream_t1_433 *)il2cpp_codegen_object_new (MemoryStream_t1_433_il2cpp_TypeInfo_var);
			MemoryStream__ctor_m1_5044(L_2, /*hidden argument*/NULL);
			V_1 = L_2;
		}

IL_000d:
		try
		{ // begin try (depth: 2)
			{
				XmlWriterSettings_t4_187 * L_3 = (XmlWriterSettings_t4_187 *)il2cpp_codegen_object_new (XmlWriterSettings_t4_187_il2cpp_TypeInfo_var);
				XmlWriterSettings__ctor_m4_1033(L_3, /*hidden argument*/NULL);
				V_2 = L_3;
				XmlWriterSettings_t4_187 * L_4 = V_2;
				UTF8Encoding_t1_1453 * L_5 = (UTF8Encoding_t1_1453 *)il2cpp_codegen_object_new (UTF8Encoding_t1_1453_il2cpp_TypeInfo_var);
				UTF8Encoding__ctor_m1_12549(L_5, 1, /*hidden argument*/NULL);
				NullCheck(L_4);
				XmlWriterSettings_set_Encoding_m4_1040(L_4, L_5, /*hidden argument*/NULL);
				XmlWriterSettings_t4_187 * L_6 = V_2;
				NullCheck(L_6);
				XmlWriterSettings_set_ConformanceLevel_m4_1038(L_6, 2, /*hidden argument*/NULL);
				XmlWriterSettings_t4_187 * L_7 = V_2;
				NullCheck(L_7);
				XmlWriterSettings_set_Indent_m4_1042(L_7, 1, /*hidden argument*/NULL);
				MemoryStream_t1_433 * L_8 = V_1;
				XmlWriterSettings_t4_187 * L_9 = V_2;
				XmlWriter_t4_182 * L_10 = XmlWriter_Create_m4_1022(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
				V_3 = L_10;
			}

IL_0035:
			try
			{ // begin try (depth: 3)
				XmlWriter_t4_182 * L_11 = V_3;
				NullCheck(L_11);
				VirtActionInvoker0::Invoke(22 /* System.Void System.Xml.XmlWriter::WriteStartDocument() */, L_11);
				XmlWriter_t4_182 * L_12 = V_3;
				NullCheck(L_12);
				VirtActionInvoker4< String_t*, String_t*, String_t*, String_t* >::Invoke(13 /* System.Void System.Xml.XmlWriter::WriteDocType(System.String,System.String,System.String,System.String) */, L_12, _stringLiteral5203, _stringLiteral5204, _stringLiteral5205, (String_t*)NULL);
				XmlWriter_t4_182 * L_13 = V_3;
				NullCheck(L_13);
				XmlWriter_WriteStartElement_m4_1032(L_13, _stringLiteral5203, /*hidden argument*/NULL);
				XmlWriter_t4_182 * L_14 = V_3;
				NullCheck(L_14);
				XmlWriter_WriteAttributeString_m4_1028(L_14, _stringLiteral1921, _stringLiteral3878, /*hidden argument*/NULL);
				XmlWriter_t4_182 * L_15 = V_3;
				Plist_WriteXMLDictionaryNode_m8_334(__this, L_15, __this, /*hidden argument*/NULL);
				XmlWriter_t4_182 * L_16 = V_3;
				NullCheck(L_16);
				VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_16);
				XmlWriter_t4_182 * L_17 = V_3;
				NullCheck(L_17);
				VirtActionInvoker0::Invoke(15 /* System.Void System.Xml.XmlWriter::WriteEndDocument() */, L_17);
				XmlWriter_t4_182 * L_18 = V_3;
				NullCheck(L_18);
				VirtActionInvoker0::Invoke(9 /* System.Void System.Xml.XmlWriter::Flush() */, L_18);
				XmlWriter_t4_182 * L_19 = V_3;
				NullCheck(L_19);
				VirtActionInvoker0::Invoke(7 /* System.Void System.Xml.XmlWriter::Close() */, L_19);
				IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
				Encoding_t1_406 * L_20 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
				MemoryStream_t1_433 * L_21 = V_1;
				NullCheck(L_21);
				ByteU5BU5D_t1_109* L_22 = (ByteU5BU5D_t1_109*)VirtFuncInvoker0< ByteU5BU5D_t1_109* >::Invoke(33 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_21);
				NullCheck(L_20);
				String_t* L_23 = (String_t*)VirtFuncInvoker1< String_t*, ByteU5BU5D_t1_109* >::Invoke(27 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_20, L_22);
				V_4 = L_23;
				StreamWriter_t1_448 * L_24 = V_0;
				String_t* L_25 = V_4;
				NullCheck(L_24);
				VirtActionInvoker1< String_t* >::Invoke(21 /* System.Void System.IO.StreamWriter::Write(System.String) */, L_24, L_25);
				IL2CPP_LEAVE(0xB8, FINALLY_00ab);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t1_33 *)e.ex;
				goto FINALLY_00ab;
			}

FINALLY_00ab:
			{ // begin finally (depth: 3)
				{
					XmlWriter_t4_182 * L_26 = V_3;
					if (!L_26)
					{
						goto IL_00b7;
					}
				}

IL_00b1:
				{
					XmlWriter_t4_182 * L_27 = V_3;
					NullCheck(L_27);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_27);
				}

IL_00b7:
				{
					IL2CPP_END_FINALLY(171)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(171)
			{
				IL2CPP_JUMP_TBL(0xB8, IL_00b8)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
			}

IL_00b8:
			{
				IL2CPP_LEAVE(0xCA, FINALLY_00bd);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1_33 *)e.ex;
			goto FINALLY_00bd;
		}

FINALLY_00bd:
		{ // begin finally (depth: 2)
			{
				MemoryStream_t1_433 * L_28 = V_1;
				if (!L_28)
				{
					goto IL_00c9;
				}
			}

IL_00c3:
			{
				MemoryStream_t1_433 * L_29 = V_1;
				NullCheck(L_29);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_29);
			}

IL_00c9:
			{
				IL2CPP_END_FINALLY(189)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(189)
		{
			IL2CPP_JUMP_TBL(0xCA, IL_00ca)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
		}

IL_00ca:
		{
			IL2CPP_LEAVE(0xDC, FINALLY_00cf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00cf;
	}

FINALLY_00cf:
	{ // begin finally (depth: 1)
		{
			StreamWriter_t1_448 * L_30 = V_0;
			if (!L_30)
			{
				goto IL_00db;
			}
		}

IL_00d5:
		{
			StreamWriter_t1_448 * L_31 = V_0;
			NullCheck(L_31);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_31);
		}

IL_00db:
		{
			IL2CPP_END_FINALLY(207)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(207)
	{
		IL2CPP_JUMP_TBL(0xDC, IL_00dc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00dc:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::WriteXMLNode(System.Xml.XmlWriter,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IList_t1_262_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern "C" void Plist_WriteXMLNode_m8_329 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, Object_t * ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IList_t1_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ____value;
		if (!((Object_t *)IsInstSealed(L_0, Boolean_t1_20_il2cpp_TypeInfo_var)))
		{
			goto IL_001d;
		}
	}
	{
		XmlWriter_t4_182 * L_1 = ____xmlWriter;
		Object_t * L_2 = ____value;
		Plist_WriteXMLBoolNode_m8_330(__this, L_1, ((*(bool*)((bool*)UnBox (L_2, Boolean_t1_20_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_001d:
	{
		Object_t * L_3 = ____value;
		if (((Object_t *)IsInstSealed(L_3, Int32_t1_3_il2cpp_TypeInfo_var)))
		{
			goto IL_0033;
		}
	}
	{
		Object_t * L_4 = ____value;
		if (!((Object_t *)IsInstSealed(L_4, Int64_t1_7_il2cpp_TypeInfo_var)))
		{
			goto IL_0045;
		}
	}

IL_0033:
	{
		XmlWriter_t4_182 * L_5 = ____xmlWriter;
		Object_t * L_6 = ____value;
		Plist_WriteXMLIntegerNode_m8_331(__this, L_5, ((*(int32_t*)((int32_t*)UnBox (L_6, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0045:
	{
		Object_t * L_7 = ____value;
		if (!((String_t*)IsInstSealed(L_7, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0062;
		}
	}
	{
		XmlWriter_t4_182 * L_8 = ____xmlWriter;
		Object_t * L_9 = ____value;
		Plist_WriteXMLStringNode_m8_332(__this, L_8, ((String_t*)IsInstSealed(L_9, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0062:
	{
		Object_t * L_10 = ____value;
		if (!((Object_t *)IsInst(L_10, IList_t1_262_il2cpp_TypeInfo_var)))
		{
			goto IL_007f;
		}
	}
	{
		XmlWriter_t4_182 * L_11 = ____xmlWriter;
		Object_t * L_12 = ____value;
		Plist_WriteXMLListNode_m8_333(__this, L_11, ((Object_t *)IsInst(L_12, IList_t1_262_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_007f:
	{
		Object_t * L_13 = ____value;
		if (!((Object_t *)IsInst(L_13, IDictionary_t1_35_il2cpp_TypeInfo_var)))
		{
			goto IL_0097;
		}
	}
	{
		XmlWriter_t4_182 * L_14 = ____xmlWriter;
		Object_t * L_15 = ____value;
		Plist_WriteXMLDictionaryNode_m8_334(__this, L_14, ((Object_t *)IsInst(L_15, IDictionary_t1_35_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_0097:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::WriteXMLBoolNode(System.Xml.XmlWriter,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Plist_WriteXMLBoolNode_m8_330 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, bool ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriter_t4_182 * L_0 = ____xmlWriter;
		String_t* L_1 = Boolean_ToString_m1_824((&____value), /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = String_ToLower_m1_540(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		XmlWriter_WriteElementString_m4_1030(L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::WriteXMLIntegerNode(System.Xml.XmlWriter,System.Int32)
extern TypeInfo* NumberFormatInfo_t1_361_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2260;
extern "C" void Plist_WriteXMLIntegerNode_m8_331 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, int32_t ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NumberFormatInfo_t1_361_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		_stringLiteral2260 = il2cpp_codegen_string_literal_from_index(2260);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriter_t4_182 * L_0 = ____xmlWriter;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1_361_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1_361 * L_1 = NumberFormatInfo_get_InvariantInfo_m1_4339(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Int32_ToString_m1_102((&____value), L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		XmlWriter_WriteElementString_m4_1030(L_0, _stringLiteral2260, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::WriteXMLStringNode(System.Xml.XmlWriter,System.String)
extern Il2CppCodeGenString* _stringLiteral309;
extern "C" void Plist_WriteXMLStringNode_m8_332 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, String_t* ____value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		s_Il2CppMethodIntialized = true;
	}
	{
		XmlWriter_t4_182 * L_0 = ____xmlWriter;
		String_t* L_1 = ____value;
		NullCheck(L_0);
		XmlWriter_WriteElementString_m4_1030(L_0, _stringLiteral309, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::WriteXMLListNode(System.Xml.XmlWriter,System.Collections.IList)
extern TypeInfo* List_1_t1_1894_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_15034_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral173;
extern "C" void Plist_WriteXMLListNode_m8_333 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, Object_t * ____listValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1958);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1__ctor_m1_15034_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484121);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ____listValue;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1_1894 * L_1 = (List_1_t1_1894 *)il2cpp_codegen_object_new (List_1_t1_1894_il2cpp_TypeInfo_var);
		List_1__ctor_m1_15034(L_1, /*hidden argument*/List_1__ctor_m1_15034_MethodInfo_var);
		____listValue = L_1;
	}

IL_000d:
	{
		XmlWriter_t4_182 * L_2 = ____xmlWriter;
		NullCheck(L_2);
		XmlWriter_WriteStartElement_m4_1032(L_2, _stringLiteral173, /*hidden argument*/NULL);
		Object_t * L_3 = ____listValue;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_3);
		V_1 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_0024:
		{
			Object_t * L_5 = V_1;
			NullCheck(L_5);
			Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_5);
			V_0 = L_6;
			XmlWriter_t4_182 * L_7 = ____xmlWriter;
			Object_t * L_8 = V_0;
			Plist_WriteXMLNode_m8_329(__this, L_7, L_8, /*hidden argument*/NULL);
		}

IL_0033:
		{
			Object_t * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0024;
			}
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		{
			Object_t * L_11 = V_1;
			V_2 = ((Object_t *)IsInst(L_11, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_12 = V_2;
			if (L_12)
			{
				goto IL_004e;
			}
		}

IL_004d:
		{
			IL2CPP_END_FINALLY(67)
		}

IL_004e:
		{
			Object_t * L_13 = V_2;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_13);
			IL2CPP_END_FINALLY(67)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0055:
	{
		XmlWriter_t4_182 * L_14 = ____xmlWriter;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_14);
		return;
	}
}
// System.Void VoxelBusters.Utility.Plist::WriteXMLDictionaryNode(System.Xml.XmlWriter,System.Collections.IDictionary)
extern TypeInfo* Dictionary_2_t1_1903_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_t1_35_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t1_1677_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_15037_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5201;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void Plist_WriteXMLDictionaryNode_m8_334 (Plist_t8_51 * __this, XmlWriter_t4_182 * ____xmlWriter, Object_t * ____dictValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1960);
		IDictionary_t1_35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		IEnumerable_t1_1677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(253);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		Dictionary_2__ctor_m1_15037_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		_stringLiteral5201 = il2cpp_codegen_string_literal_from_index(5201);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ____dictValue;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		Dictionary_2_t1_1903 * L_1 = (Dictionary_2_t1_1903 *)il2cpp_codegen_object_new (Dictionary_2_t1_1903_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_15037(L_1, /*hidden argument*/Dictionary_2__ctor_m1_15037_MethodInfo_var);
		____dictValue = L_1;
	}

IL_000d:
	{
		XmlWriter_t4_182 * L_2 = ____xmlWriter;
		NullCheck(L_2);
		XmlWriter_WriteStartElement_m4_1032(L_2, _stringLiteral5201, /*hidden argument*/NULL);
		Object_t * L_3 = ____dictValue;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_4);
		Object_t * L_5 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t1_1677_il2cpp_TypeInfo_var, L_4);
		V_1 = L_5;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_0029:
		{
			Object_t * L_6 = V_1;
			NullCheck(L_6);
			Object_t * L_7 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_6);
			V_0 = L_7;
			XmlWriter_t4_182 * L_8 = ____xmlWriter;
			Object_t * L_9 = V_0;
			NullCheck(L_9);
			String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
			NullCheck(L_8);
			XmlWriter_WriteElementString_m4_1030(L_8, _stringLiteral793, L_10, /*hidden argument*/NULL);
			XmlWriter_t4_182 * L_11 = ____xmlWriter;
			Object_t * L_12 = ____dictValue;
			Object_t * L_13 = V_0;
			NullCheck(L_12);
			Object_t * L_14 = (Object_t *)InterfaceFuncInvoker1< Object_t *, Object_t * >::Invoke(2 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t1_35_il2cpp_TypeInfo_var, L_12, L_13);
			Plist_WriteXMLNode_m8_329(__this, L_11, L_14, /*hidden argument*/NULL);
		}

IL_004f:
		{
			Object_t * L_15 = V_1;
			NullCheck(L_15);
			bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0029;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x71, FINALLY_005f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_005f;
	}

FINALLY_005f:
	{ // begin finally (depth: 1)
		{
			Object_t * L_17 = V_1;
			V_2 = ((Object_t *)IsInst(L_17, IDisposable_t1_1035_il2cpp_TypeInfo_var));
			Object_t * L_18 = V_2;
			if (L_18)
			{
				goto IL_006a;
			}
		}

IL_0069:
		{
			IL2CPP_END_FINALLY(95)
		}

IL_006a:
		{
			Object_t * L_19 = V_2;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_19);
			IL2CPP_END_FINALLY(95)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(95)
	{
		IL2CPP_JUMP_TBL(0x71, IL_0071)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0071:
	{
		XmlWriter_t4_182 * L_20 = ____xmlWriter;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(16 /* System.Void System.Xml.XmlWriter::WriteEndElement() */, L_20);
		return;
	}
}
// System.Void VoxelBusters.Utility.NativeBinding::.ctor()
extern "C" void NativeBinding__ctor_m8_335 (NativeBinding_t8_59 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m6_671(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String VoxelBusters.Utility.NativeBinding::utilityBundleVersion()
extern "C" {char* DEFAULT_CALL utilityBundleVersion();}
extern "C" String_t* NativeBinding_utilityBundleVersion_m8_336 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)utilityBundleVersion;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'utilityBundleVersion'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String VoxelBusters.Utility.NativeBinding::utilityBundleIdentifier()
extern "C" {char* DEFAULT_CALL utilityBundleIdentifier();}
extern "C" String_t* NativeBinding_utilityBundleIdentifier_m8_337 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)utilityBundleIdentifier;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'utilityBundleIdentifier'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	char* _return_value = _il2cpp_pinvoke_func();
	String_t* __return_value_unmarshaled = { 0 };
	__return_value_unmarshaled = il2cpp_codegen_marshal_string_result(_return_value);
	il2cpp_codegen_marshal_free(_return_value);
	_return_value = NULL;

	return __return_value_unmarshaled;
}
// System.String VoxelBusters.Utility.NativeBinding::GetBundleVersion()
extern "C" String_t* NativeBinding_GetBundleVersion_m8_338 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = NativeBinding_utilityBundleVersion_m8_336(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String VoxelBusters.Utility.NativeBinding::GetBundleIdentifier()
extern "C" String_t* NativeBinding_GetBundleIdentifier_m8_339 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		String_t* L_0 = NativeBinding_utilityBundleIdentifier_m8_337(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ExifLibrary.BitConverterEx::.ctor(ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" void BitConverterEx__ctor_m8_340 (BitConverterEx_t8_61 * __this, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___from;
		__this->___mFrom_0 = L_0;
		int32_t L_1 = ___to;
		__this->___mTo_1 = L_1;
		return;
	}
}
// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.BitConverterEx::get_SystemByteOrder()
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" int32_t BitConverterEx_get_SystemByteOrder_m8_341 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t1_1508_StaticFields*)BitConverter_t1_1508_il2cpp_TypeInfo_var->static_fields)->___IsLittleEndian_1;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 2;
	}

IL_0011:
	{
		return (int32_t)(G_B3_0);
	}
}
// ExifLibrary.BitConverterEx ExifLibrary.BitConverterEx::get_LittleEndian()
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern "C" BitConverterEx_t8_61 * BitConverterEx_get_LittleEndian_m8_342 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		s_Il2CppMethodIntialized = true;
	}
	{
		BitConverterEx_t8_61 * L_0 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_0, 1, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// ExifLibrary.BitConverterEx ExifLibrary.BitConverterEx::get_BigEndian()
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern "C" BitConverterEx_t8_61 * BitConverterEx_get_BigEndian_m8_343 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		s_Il2CppMethodIntialized = true;
	}
	{
		BitConverterEx_t8_61 * L_0 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_0, 2, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// ExifLibrary.BitConverterEx ExifLibrary.BitConverterEx::get_SystemEndian()
extern TypeInfo* BitConverterEx_t8_61_il2cpp_TypeInfo_var;
extern "C" BitConverterEx_t8_61 * BitConverterEx_get_SystemEndian_m8_344 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverterEx_t8_61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1970);
		s_Il2CppMethodIntialized = true;
	}
	{
		BitConverterEx_t8_61 * L_0 = (BitConverterEx_t8_61 *)il2cpp_codegen_object_new (BitConverterEx_t8_61_il2cpp_TypeInfo_var);
		BitConverterEx__ctor_m8_340(L_0, 0, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Char ExifLibrary.BitConverterEx::ToChar(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" uint16_t BitConverterEx_ToChar_m8_345 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 2, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		uint16_t L_6 = BitConverter_ToChar_m1_13322(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.UInt16 ExifLibrary.BitConverterEx::ToUInt16(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" uint16_t BitConverterEx_ToUInt16_m8_346 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 2, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		uint16_t L_6 = BitConverter_ToUInt16_m1_13326(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.UInt32 ExifLibrary.BitConverterEx::ToUInt32(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" uint32_t BitConverterEx_ToUInt32_m8_347 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 4, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		uint32_t L_6 = BitConverter_ToUInt32_m1_13327(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.UInt64 ExifLibrary.BitConverterEx::ToUInt64(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" uint64_t BitConverterEx_ToUInt64_m8_348 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 8, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		uint64_t L_6 = BitConverter_ToUInt64_m1_13328(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int16 ExifLibrary.BitConverterEx::ToInt16(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" int16_t BitConverterEx_ToInt16_m8_349 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 2, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		int16_t L_6 = BitConverter_ToInt16_m1_13323(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 ExifLibrary.BitConverterEx::ToInt32(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" int32_t BitConverterEx_ToInt32_m8_350 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 4, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		int32_t L_6 = BitConverter_ToInt32_m1_13324(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int64 ExifLibrary.BitConverterEx::ToInt64(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" int64_t BitConverterEx_ToInt64_m8_351 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 8, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		int64_t L_6 = BitConverter_ToInt64_m1_13325(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single ExifLibrary.BitConverterEx::ToSingle(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" float BitConverterEx_ToSingle_m8_352 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 4, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		float L_6 = BitConverter_ToSingle_m1_13329(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Double ExifLibrary.BitConverterEx::ToDouble(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" double BitConverterEx_ToDouble_m8_353 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, L_1, 8, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t1_109* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		double L_6 = BitConverter_ToDouble_m1_13330(NULL /*static, unused*/, L_5, 0, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt16,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_354 (Object_t * __this /* static, unused */, uint16_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		uint16_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13315(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_355 (Object_t * __this /* static, unused */, uint32_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		uint32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13316(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt64,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_356 (Object_t * __this /* static, unused */, uint64_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		uint64_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13317(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int16,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_357 (Object_t * __this /* static, unused */, int16_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		int16_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13312(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_358 (Object_t * __this /* static, unused */, int32_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13313(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int64,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_359 (Object_t * __this /* static, unused */, int64_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		int64_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13314(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Single,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_360 (Object_t * __this /* static, unused */, float ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		float L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13318(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Double,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_361 (Object_t * __this /* static, unused */, double ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		double L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		ByteU5BU5D_t1_109* L_1 = BitConverter_GetBytes_m1_13319(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t1_109* L_2 = V_0;
		int32_t L_3 = ___from;
		int32_t L_4 = ___to;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_CheckData_m8_380(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_0;
		return L_6;
	}
}
// System.Char ExifLibrary.BitConverterEx::ToChar(System.Byte[],System.Int32)
extern "C" uint16_t BitConverterEx_ToChar_m8_362 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		uint16_t L_4 = BitConverterEx_ToChar_m8_345(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt16 ExifLibrary.BitConverterEx::ToUInt16(System.Byte[],System.Int32)
extern "C" uint16_t BitConverterEx_ToUInt16_m8_363 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		uint16_t L_4 = BitConverterEx_ToUInt16_m8_346(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt32 ExifLibrary.BitConverterEx::ToUInt32(System.Byte[],System.Int32)
extern "C" uint32_t BitConverterEx_ToUInt32_m8_364 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		uint32_t L_4 = BitConverterEx_ToUInt32_m8_347(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt64 ExifLibrary.BitConverterEx::ToUInt64(System.Byte[],System.Int32)
extern "C" uint64_t BitConverterEx_ToUInt64_m8_365 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		uint64_t L_4 = BitConverterEx_ToUInt64_m8_348(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int16 ExifLibrary.BitConverterEx::ToInt16(System.Byte[],System.Int32)
extern "C" int16_t BitConverterEx_ToInt16_m8_366 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		int16_t L_4 = BitConverterEx_ToInt16_m8_349(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 ExifLibrary.BitConverterEx::ToInt32(System.Byte[],System.Int32)
extern "C" int32_t BitConverterEx_ToInt32_m8_367 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		int32_t L_4 = BitConverterEx_ToInt32_m8_350(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int64 ExifLibrary.BitConverterEx::ToInt64(System.Byte[],System.Int32)
extern "C" int64_t BitConverterEx_ToInt64_m8_368 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		int64_t L_4 = BitConverterEx_ToInt64_m8_351(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single ExifLibrary.BitConverterEx::ToSingle(System.Byte[],System.Int32)
extern "C" float BitConverterEx_ToSingle_m8_369 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		float L_4 = BitConverterEx_ToSingle_m8_352(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Double ExifLibrary.BitConverterEx::ToDouble(System.Byte[],System.Int32)
extern "C" double BitConverterEx_ToDouble_m8_370 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		int32_t L_1 = ___startIndex;
		int32_t L_2 = (__this->___mFrom_0);
		int32_t L_3 = (__this->___mTo_1);
		double L_4 = BitConverterEx_ToDouble_m8_353(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt16)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_371 (BitConverterEx_t8_61 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt32)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_372 (BitConverterEx_t8_61 * __this, uint32_t ___value, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt64)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_373 (BitConverterEx_t8_61 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_356(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int16)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_374 (BitConverterEx_t8_61 * __this, int16_t ___value, const MethodInfo* method)
{
	{
		int16_t L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_357(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_375 (BitConverterEx_t8_61 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int64)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_376 (BitConverterEx_t8_61 * __this, int64_t ___value, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_359(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Single)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_377 (BitConverterEx_t8_61 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_360(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Double)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_378 (BitConverterEx_t8_61 * __this, double ___value, const MethodInfo* method)
{
	{
		double L_0 = ___value;
		int32_t L_1 = (__this->___mFrom_0);
		int32_t L_2 = (__this->___mTo_1);
		ByteU5BU5D_t1_109* L_3 = BitConverterEx_GetBytes_m8_361(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::CheckData(System.Byte[],System.Int32,System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* BitConverterEx_CheckData_m8_379 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___length, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	{
		int32_t L_0 = ___from;
		int32_t L_1 = BitConverterEx_CheckByteOrder_m8_381(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___from = L_1;
		int32_t L_2 = ___to;
		int32_t L_3 = BitConverterEx_CheckByteOrder_m8_381(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		___to = L_3;
		int32_t L_4 = ___length;
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, L_4));
		ByteU5BU5D_t1_109* L_5 = ___value;
		int32_t L_6 = ___startIndex;
		ByteU5BU5D_t1_109* L_7 = V_0;
		int32_t L_8 = ___length;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, L_6, (Array_t *)(Array_t *)L_7, 0, L_8, /*hidden argument*/NULL);
		int32_t L_9 = ___from;
		int32_t L_10 = ___to;
		if ((((int32_t)L_9) == ((int32_t)L_10)))
		{
			goto IL_0030;
		}
	}
	{
		ByteU5BU5D_t1_109* L_11 = V_0;
		Array_Reverse_m1_1052(NULL /*static, unused*/, (Array_t *)(Array_t *)L_11, /*hidden argument*/NULL);
	}

IL_0030:
	{
		ByteU5BU5D_t1_109* L_12 = V_0;
		return L_12;
	}
}
// System.Byte[] ExifLibrary.BitConverterEx::CheckData(System.Byte[],ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_CheckData_m8_380 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___value;
		ByteU5BU5D_t1_109* L_1 = ___value;
		NullCheck(L_1);
		int32_t L_2 = ___from;
		int32_t L_3 = ___to;
		ByteU5BU5D_t1_109* L_4 = BitConverterEx_CheckData_m8_379(NULL /*static, unused*/, L_0, 0, (((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))), L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.BitConverterEx::CheckByteOrder(ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* BitConverter_t1_1508_il2cpp_TypeInfo_var;
extern "C" int32_t BitConverterEx_CheckByteOrder_m8_381 (Object_t * __this /* static, unused */, int32_t ___order, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BitConverter_t1_1508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___order;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1_1508_il2cpp_TypeInfo_var);
		bool L_1 = ((BitConverter_t1_1508_StaticFields*)BitConverter_t1_1508_il2cpp_TypeInfo_var->static_fields)->___IsLittleEndian_1;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0012:
	{
		return (int32_t)(2);
	}

IL_0014:
	{
		int32_t L_2 = ___order;
		return L_2;
	}
}
// System.Void ExifLibrary.ExifBitConverter::.ctor(ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" void ExifBitConverter__ctor_m8_382 (ExifBitConverter_t8_62 * __this, int32_t ___from, int32_t ___to, const MethodInfo* method)
{
	{
		int32_t L_0 = ___from;
		int32_t L_1 = ___to;
		BitConverterEx__ctor_m8_340(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String ExifLibrary.ExifBitConverter::ToAscii(System.Byte[],System.Boolean)
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_IndexOf_TisByte_t1_11_m1_15039_MethodInfo_var;
extern "C" String_t* ExifBitConverter_ToAscii_m8_383 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, bool ___endatfirstnull, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Array_IndexOf_TisByte_t1_11_m1_15039_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484128);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t1_109* L_0 = ___data;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
		bool L_1 = ___endatfirstnull;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		ByteU5BU5D_t1_109* L_2 = ___data;
		int32_t L_3 = Array_IndexOf_TisByte_t1_11_m1_15039(NULL /*static, unused*/, L_2, 0, /*hidden argument*/Array_IndexOf_TisByte_t1_11_m1_15039_MethodInfo_var);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		ByteU5BU5D_t1_109* L_5 = ___data;
		NullCheck(L_5);
		V_0 = (((int32_t)((int32_t)(((Array_t *)L_5)->max_length))));
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_6 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_7 = ___data;
		int32_t L_8 = V_0;
		NullCheck(L_6);
		String_t* L_9 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t1_109*, int32_t, int32_t >::Invoke(26 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_6, L_7, 0, L_8);
		return L_9;
	}
}
// System.String ExifLibrary.ExifBitConverter::ToAscii(System.Byte[])
extern "C" String_t* ExifBitConverter_ToAscii_m8_384 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___data;
		String_t* L_1 = ExifBitConverter_ToAscii_m8_383(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String ExifLibrary.ExifBitConverter::ToString(System.Byte[])
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern "C" String_t* ExifBitConverter_ToString_m8_385 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	uint8_t V_1 = 0x0;
	ByteU5BU5D_t1_109* V_2 = {0};
	int32_t V_3 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ByteU5BU5D_t1_109* L_1 = ___data;
		V_2 = L_1;
		V_3 = 0;
		goto IL_001f;
	}

IL_000f:
	{
		ByteU5BU5D_t1_109* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_2, L_4, sizeof(uint8_t)));
		StringBuilder_t1_247 * L_5 = V_0;
		uint8_t L_6 = V_1;
		NullCheck(L_5);
		StringBuilder_Append_m1_12440(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_8 = V_3;
		ByteU5BU5D_t1_109* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length)))))))
		{
			goto IL_000f;
		}
	}
	{
		StringBuilder_t1_247 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = StringBuilder_ToString_m1_12428(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.DateTime ExifLibrary.ExifBitConverter::ToDateTime(System.Byte[],System.Boolean)
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5206;
extern Il2CppCodeGenString* _stringLiteral5207;
extern "C" DateTime_t1_150  ExifBitConverter_ToDateTime_m8_386 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, bool ___hastime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral5206 = il2cpp_codegen_string_literal_from_index(5206);
		_stringLiteral5207 = il2cpp_codegen_string_literal_from_index(5207);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		ByteU5BU5D_t1_109* L_0 = ___data;
		String_t* L_1 = ExifBitConverter_ToAscii_m8_384(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___hastime;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_4 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_5 = DateTime_ParseExact_m1_13869(NULL /*static, unused*/, L_3, _stringLiteral5206, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001e:
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_7 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_8 = DateTime_ParseExact_m1_13869(NULL /*static, unused*/, L_6, _stringLiteral5207, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.DateTime ExifLibrary.ExifBitConverter::ToDateTime(System.Byte[])
extern "C" DateTime_t1_150  ExifBitConverter_ToDateTime_m8_387 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, const MethodInfo* method)
{
	{
		ByteU5BU5D_t1_109* L_0 = ___data;
		DateTime_t1_150  L_1 = ExifBitConverter_ToDateTime_m8_386(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.ExifBitConverter::ToURational(System.Byte[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" UFraction32_t8_121  ExifBitConverter_ToURational_m8_388 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___frombyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		V_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_0 = ___data;
		ByteU5BU5D_t1_109* L_1 = V_0;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, 0, (Array_t *)(Array_t *)L_1, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = ___data;
		ByteU5BU5D_t1_109* L_3 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, 4, (Array_t *)(Array_t *)L_3, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = V_0;
		int32_t L_5 = ___frombyteorder;
		uint32_t L_6 = BitConverterEx_ToUInt32_m8_347(NULL /*static, unused*/, L_4, 0, L_5, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_7 = V_1;
		int32_t L_8 = ___frombyteorder;
		uint32_t L_9 = BitConverterEx_ToUInt32_m8_347(NULL /*static, unused*/, L_7, 0, L_8, 0, /*hidden argument*/NULL);
		UFraction32_t8_121  L_10 = {0};
		UFraction32__ctor_m8_726(&L_10, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// ExifLibrary.MathEx/Fraction32 ExifLibrary.ExifBitConverter::ToSRational(System.Byte[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" Fraction32_t8_127  ExifBitConverter_ToSRational_m8_389 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___frombyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	{
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		V_1 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_0 = ___data;
		ByteU5BU5D_t1_109* L_1 = V_0;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, 0, (Array_t *)(Array_t *)L_1, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_2 = ___data;
		ByteU5BU5D_t1_109* L_3 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, 4, (Array_t *)(Array_t *)L_3, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = V_0;
		int32_t L_5 = ___frombyteorder;
		int32_t L_6 = BitConverterEx_ToInt32_m8_350(NULL /*static, unused*/, L_4, 0, L_5, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_7 = V_1;
		int32_t L_8 = ___frombyteorder;
		int32_t L_9 = BitConverterEx_ToInt32_m8_350(NULL /*static, unused*/, L_7, 0, L_8, 0, /*hidden argument*/NULL);
		Fraction32_t8_127  L_10 = {0};
		Fraction32__ctor_m8_659(&L_10, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.UInt16[] ExifLibrary.ExifBitConverter::ToUShortArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* UInt16U5BU5D_t1_1231_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" UInt16U5BU5D_t1_1231* ExifBitConverter_ToUShortArray_m8_390 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt16U5BU5D_t1_1231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(70);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	UInt16U5BU5D_t1_1231* V_0 = {0};
	uint32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		int32_t L_0 = ___count;
		V_0 = ((UInt16U5BU5D_t1_1231*)SZArrayNew(UInt16U5BU5D_t1_1231_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_0035;
	}

IL_000e:
	{
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		ByteU5BU5D_t1_109* L_1 = ___data;
		uint32_t L_2 = V_1;
		ByteU5BU5D_t1_109* L_3 = V_2;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)L_2*(int32_t)2)))))))), (Array_t *)(Array_t *)L_3, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)2))), /*hidden argument*/NULL);
		UInt16U5BU5D_t1_1231* L_4 = V_0;
		uint32_t L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = V_2;
		int32_t L_7 = ___frombyteorder;
		uint16_t L_8 = BitConverterEx_ToUInt16_m8_346(NULL /*static, unused*/, L_6, 0, L_7, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (((uintptr_t)L_5)));
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_4, (((uintptr_t)L_5)), sizeof(uint16_t))) = (uint16_t)L_8;
		uint32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		uint32_t L_10 = V_1;
		int32_t L_11 = ___count;
		if ((((int64_t)(((int64_t)((uint64_t)L_10)))) < ((int64_t)(((int64_t)((int64_t)L_11))))))
		{
			goto IL_000e;
		}
	}
	{
		UInt16U5BU5D_t1_1231* L_12 = V_0;
		return L_12;
	}
}
// System.UInt32[] ExifLibrary.ExifBitConverter::ToUIntArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" UInt32U5BU5D_t1_142* ExifBitConverter_ToUIntArray_m8_391 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	UInt32U5BU5D_t1_142* V_0 = {0};
	uint32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		int32_t L_0 = ___count;
		V_0 = ((UInt32U5BU5D_t1_142*)SZArrayNew(UInt32U5BU5D_t1_142_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_0035;
	}

IL_000e:
	{
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_1 = ___data;
		uint32_t L_2 = V_1;
		ByteU5BU5D_t1_109* L_3 = V_2;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)L_2*(int32_t)4)))))))), (Array_t *)(Array_t *)L_3, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)4))), /*hidden argument*/NULL);
		UInt32U5BU5D_t1_142* L_4 = V_0;
		uint32_t L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = V_2;
		int32_t L_7 = ___frombyteorder;
		uint32_t L_8 = BitConverterEx_ToUInt32_m8_347(NULL /*static, unused*/, L_6, 0, L_7, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (((uintptr_t)L_5)));
		*((uint32_t*)(uint32_t*)SZArrayLdElema(L_4, (((uintptr_t)L_5)), sizeof(uint32_t))) = (uint32_t)L_8;
		uint32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		uint32_t L_10 = V_1;
		int32_t L_11 = ___count;
		if ((((int64_t)(((int64_t)((uint64_t)L_10)))) < ((int64_t)(((int64_t)((int64_t)L_11))))))
		{
			goto IL_000e;
		}
	}
	{
		UInt32U5BU5D_t1_142* L_12 = V_0;
		return L_12;
	}
}
// System.Int32[] ExifLibrary.ExifBitConverter::ToSIntArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" Int32U5BU5D_t1_275* ExifBitConverter_ToSIntArray_m8_392 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___byteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1_275* V_0 = {0};
	uint32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		int32_t L_0 = ___count;
		V_0 = ((Int32U5BU5D_t1_275*)SZArrayNew(Int32U5BU5D_t1_275_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_0035;
	}

IL_000e:
	{
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_1 = ___data;
		uint32_t L_2 = V_1;
		ByteU5BU5D_t1_109* L_3 = V_2;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)L_2*(int32_t)4)))))))), (Array_t *)(Array_t *)L_3, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)4))), /*hidden argument*/NULL);
		Int32U5BU5D_t1_275* L_4 = V_0;
		uint32_t L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = V_2;
		int32_t L_7 = ___byteorder;
		int32_t L_8 = BitConverterEx_ToInt32_m8_350(NULL /*static, unused*/, L_6, 0, L_7, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (((uintptr_t)L_5)));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_4, (((uintptr_t)L_5)), sizeof(int32_t))) = (int32_t)L_8;
		uint32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0035:
	{
		uint32_t L_10 = V_1;
		int32_t L_11 = ___count;
		if ((((int64_t)(((int64_t)((uint64_t)L_10)))) < ((int64_t)(((int64_t)((int64_t)L_11))))))
		{
			goto IL_000e;
		}
	}
	{
		Int32U5BU5D_t1_275* L_12 = V_0;
		return L_12;
	}
}
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.ExifBitConverter::ToURationalArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* UFraction32U5BU5D_t8_122_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" UFraction32U5BU5D_t8_122* ExifBitConverter_ToURationalArray_m8_393 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32U5BU5D_t8_122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1971);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	UFraction32U5BU5D_t8_122* V_0 = {0};
	uint32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	{
		int32_t L_0 = ___count;
		V_0 = ((UFraction32U5BU5D_t8_122*)SZArrayNew(UFraction32U5BU5D_t8_122_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_005f;
	}

IL_000e:
	{
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		V_3 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_1 = ___data;
		uint32_t L_2 = V_1;
		ByteU5BU5D_t1_109* L_3 = V_2;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)L_2*(int32_t)8)))))))), (Array_t *)(Array_t *)L_3, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)4))), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = ___data;
		uint32_t L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = V_3;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)8))+(int32_t)4)))))))), (Array_t *)(Array_t *)L_6, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)4))), /*hidden argument*/NULL);
		UFraction32U5BU5D_t8_122* L_7 = V_0;
		uint32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, (((uintptr_t)L_8)));
		ByteU5BU5D_t1_109* L_9 = V_2;
		int32_t L_10 = ___frombyteorder;
		uint32_t L_11 = BitConverterEx_ToUInt32_m8_347(NULL /*static, unused*/, L_9, 0, L_10, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_12 = V_3;
		int32_t L_13 = ___frombyteorder;
		uint32_t L_14 = BitConverterEx_ToUInt32_m8_347(NULL /*static, unused*/, L_12, 0, L_13, 0, /*hidden argument*/NULL);
		UFraction32_Set_m8_741(((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_7, (((uintptr_t)L_8)), sizeof(UFraction32_t8_121 ))), L_11, L_14, /*hidden argument*/NULL);
		uint32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005f:
	{
		uint32_t L_16 = V_1;
		int32_t L_17 = ___count;
		if ((((int64_t)(((int64_t)((uint64_t)L_16)))) < ((int64_t)(((int64_t)((int64_t)L_17))))))
		{
			goto IL_000e;
		}
	}
	{
		UFraction32U5BU5D_t8_122* L_18 = V_0;
		return L_18;
	}
}
// ExifLibrary.MathEx/Fraction32[] ExifLibrary.ExifBitConverter::ToSRationalArray(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* Fraction32U5BU5D_t8_129_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" Fraction32U5BU5D_t8_129* ExifBitConverter_ToSRationalArray_m8_394 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___count, int32_t ___frombyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Fraction32U5BU5D_t8_129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1973);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	Fraction32U5BU5D_t8_129* V_0 = {0};
	uint32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	{
		int32_t L_0 = ___count;
		V_0 = ((Fraction32U5BU5D_t8_129*)SZArrayNew(Fraction32U5BU5D_t8_129_il2cpp_TypeInfo_var, L_0));
		V_1 = 0;
		goto IL_005f;
	}

IL_000e:
	{
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		V_3 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		ByteU5BU5D_t1_109* L_1 = ___data;
		uint32_t L_2 = V_1;
		ByteU5BU5D_t1_109* L_3 = V_2;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)L_2*(int32_t)8)))))))), (Array_t *)(Array_t *)L_3, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)4))), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_4 = ___data;
		uint32_t L_5 = V_1;
		ByteU5BU5D_t1_109* L_6 = V_3;
		Array_Copy_m1_1042(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (((int64_t)((uint64_t)(((uint32_t)((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)8))+(int32_t)4)))))))), (Array_t *)(Array_t *)L_6, (((int64_t)((int64_t)0))), (((int64_t)((int64_t)4))), /*hidden argument*/NULL);
		Fraction32U5BU5D_t8_129* L_7 = V_0;
		uint32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, (((uintptr_t)L_8)));
		ByteU5BU5D_t1_109* L_9 = V_2;
		int32_t L_10 = ___frombyteorder;
		int32_t L_11 = BitConverterEx_ToInt32_m8_350(NULL /*static, unused*/, L_9, 0, L_10, 0, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_12 = V_3;
		int32_t L_13 = ___frombyteorder;
		int32_t L_14 = BitConverterEx_ToInt32_m8_350(NULL /*static, unused*/, L_12, 0, L_13, 0, /*hidden argument*/NULL);
		Fraction32_Set_m8_679(((Fraction32_t8_127 *)(Fraction32_t8_127 *)SZArrayLdElema(L_7, (((uintptr_t)L_8)), sizeof(Fraction32_t8_127 ))), L_11, L_14, /*hidden argument*/NULL);
		uint32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005f:
	{
		uint32_t L_16 = V_1;
		int32_t L_17 = ___count;
		if ((((int64_t)(((int64_t)((uint64_t)L_16)))) < ((int64_t)(((int64_t)((int64_t)L_17))))))
		{
			goto IL_000e;
		}
	}
	{
		Fraction32U5BU5D_t8_129* L_18 = V_0;
		return L_18;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.String,System.Boolean)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_395 (Object_t * __this /* static, unused */, String_t* ___value, bool ___addnull, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___addnull;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_1 = ___value;
		uint16_t L_2 = 0;
		Object_t * L_3 = Box(Char_t1_15_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_556(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		___value = L_4;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_5 = Encoding_get_UTF8_m1_12363(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = ___value;
		NullCheck(L_5);
		ByteU5BU5D_t1_109* L_7 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, L_6);
		return L_7;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.String)
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_396 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		ByteU5BU5D_t1_109* L_1 = ExifBitConverter_GetBytes_m8_395(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.DateTime,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t1_277_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5206;
extern Il2CppCodeGenString* _stringLiteral5207;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_397 (Object_t * __this /* static, unused */, DateTime_t1_150  ___value, bool ___hastime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CultureInfo_t1_277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		_stringLiteral5206 = il2cpp_codegen_string_literal_from_index(5206);
		_stringLiteral5207 = il2cpp_codegen_string_literal_from_index(5207);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		bool L_1 = ___hastime;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_2 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = DateTime_ToString_m1_13899((&___value), _stringLiteral5206, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0035;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1_277_il2cpp_TypeInfo_var);
		CultureInfo_t1_277 * L_4 = CultureInfo_get_InvariantCulture_m1_3911(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = DateTime_ToString_m1_13899((&___value), _stringLiteral5207, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0035:
	{
		String_t* L_6 = V_0;
		ByteU5BU5D_t1_109* L_7 = ExifBitConverter_GetBytes_m8_395(NULL /*static, unused*/, L_6, 1, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/UFraction32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_398 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		uint32_t L_0 = UFraction32_get_Numerator_m8_733((&___value), /*hidden argument*/NULL);
		int32_t L_1 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_2 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		uint32_t L_3 = UFraction32_get_Denominator_m8_735((&___value), /*hidden argument*/NULL);
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, L_3, 0, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 8));
		ByteU5BU5D_t1_109* L_6 = V_0;
		ByteU5BU5D_t1_109* L_7 = V_2;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, 0, (Array_t *)(Array_t *)L_7, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_8 = V_1;
		ByteU5BU5D_t1_109* L_9 = V_2;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_8, 0, (Array_t *)(Array_t *)L_9, 4, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_10 = V_2;
		return L_10;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/Fraction32,ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_399 (Object_t * __this /* static, unused */, Fraction32_t8_127  ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		int32_t L_0 = Fraction32_get_Numerator_m8_666((&___value), /*hidden argument*/NULL);
		int32_t L_1 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_2 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Fraction32_get_Denominator_m8_668((&___value), /*hidden argument*/NULL);
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, L_3, 0, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		V_2 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 8));
		ByteU5BU5D_t1_109* L_6 = V_0;
		ByteU5BU5D_t1_109* L_7 = V_2;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, 0, (Array_t *)(Array_t *)L_7, 0, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_8 = V_1;
		ByteU5BU5D_t1_109* L_9 = V_2;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_8, 0, (Array_t *)(Array_t *)L_9, 4, 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_10 = V_2;
		return L_10;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.UInt16[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_400 (Object_t * __this /* static, unused */, UInt16U5BU5D_t1_1231* ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		UInt16U5BU5D_t1_1231* L_0 = ___value;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)2*(int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_0012:
	{
		UInt16U5BU5D_t1_1231* L_1 = ___value;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_1, L_3, sizeof(uint16_t))), 0, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_2;
		ByteU5BU5D_t1_109* L_7 = V_0;
		int32_t L_8 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, 0, (Array_t *)(Array_t *)L_7, ((int32_t)((int32_t)L_8*(int32_t)2)), 2, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_1;
		UInt16U5BU5D_t1_1231* L_11 = ___value;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t1_109* L_12 = V_0;
		return L_12;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.UInt32[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_401 (Object_t * __this /* static, unused */, UInt32U5BU5D_t1_142* ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		UInt32U5BU5D_t1_142* L_0 = ___value;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)4*(int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_0012:
	{
		UInt32U5BU5D_t1_142* L_1 = ___value;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, (*(uint32_t*)(uint32_t*)SZArrayLdElema(L_1, L_3, sizeof(uint32_t))), 0, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_2;
		ByteU5BU5D_t1_109* L_7 = V_0;
		int32_t L_8 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, 0, (Array_t *)(Array_t *)L_7, ((int32_t)((int32_t)L_8*(int32_t)4)), 4, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_1;
		UInt32U5BU5D_t1_142* L_11 = ___value;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t1_109* L_12 = V_0;
		return L_12;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(System.Int32[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_402 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_275* ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	{
		Int32U5BU5D_t1_275* L_0 = ___value;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)4*(int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_0012:
	{
		Int32U5BU5D_t1_275* L_1 = ___value;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, (*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3, sizeof(int32_t))), 0, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		ByteU5BU5D_t1_109* L_6 = V_2;
		ByteU5BU5D_t1_109* L_7 = V_0;
		int32_t L_8 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, 0, (Array_t *)(Array_t *)L_7, ((int32_t)((int32_t)L_8*(int32_t)4)), 4, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_1;
		Int32U5BU5D_t1_275* L_11 = ___value;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_11)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t1_109* L_12 = V_0;
		return L_12;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/UFraction32[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_403 (Object_t * __this /* static, unused */, UFraction32U5BU5D_t8_122* ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	{
		UFraction32U5BU5D_t8_122* L_0 = ___value;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)8*(int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))))));
		V_1 = 0;
		goto IL_0058;
	}

IL_0012:
	{
		UFraction32U5BU5D_t8_122* L_1 = ___value;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		uint32_t L_3 = UFraction32_get_Numerator_m8_733(((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_1, L_2, sizeof(UFraction32_t8_121 ))), /*hidden argument*/NULL);
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, L_3, 0, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		UFraction32U5BU5D_t8_122* L_6 = ___value;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		uint32_t L_8 = UFraction32_get_Denominator_m8_735(((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_6, L_7, sizeof(UFraction32_t8_121 ))), /*hidden argument*/NULL);
		int32_t L_9 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_10 = BitConverterEx_GetBytes_m8_355(NULL /*static, unused*/, L_8, 0, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		ByteU5BU5D_t1_109* L_11 = V_2;
		ByteU5BU5D_t1_109* L_12 = V_0;
		int32_t L_13 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_11, 0, (Array_t *)(Array_t *)L_12, ((int32_t)((int32_t)L_13*(int32_t)8)), 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_14 = V_3;
		ByteU5BU5D_t1_109* L_15 = V_0;
		int32_t L_16 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_14, 0, (Array_t *)(Array_t *)L_15, ((int32_t)((int32_t)((int32_t)((int32_t)L_16*(int32_t)8))+(int32_t)4)), 4, /*hidden argument*/NULL);
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_18 = V_1;
		UFraction32U5BU5D_t8_122* L_19 = ___value;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t1_109* L_20 = V_0;
		return L_20;
	}
}
// System.Byte[] ExifLibrary.ExifBitConverter::GetBytes(ExifLibrary.MathEx/Fraction32[],ExifLibrary.BitConverterEx/ByteOrder)
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t1_109* ExifBitConverter_GetBytes_m8_404 (Object_t * __this /* static, unused */, Fraction32U5BU5D_t8_129* ___value, int32_t ___tobyteorder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	{
		Fraction32U5BU5D_t8_129* L_0 = ___value;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)8*(int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))))));
		V_1 = 0;
		goto IL_0058;
	}

IL_0012:
	{
		Fraction32U5BU5D_t8_129* L_1 = ___value;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = Fraction32_get_Numerator_m8_666(((Fraction32_t8_127 *)(Fraction32_t8_127 *)SZArrayLdElema(L_1, L_2, sizeof(Fraction32_t8_127 ))), /*hidden argument*/NULL);
		int32_t L_4 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_5 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, L_3, 0, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Fraction32U5BU5D_t8_129* L_6 = ___value;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = Fraction32_get_Denominator_m8_668(((Fraction32_t8_127 *)(Fraction32_t8_127 *)SZArrayLdElema(L_6, L_7, sizeof(Fraction32_t8_127 ))), /*hidden argument*/NULL);
		int32_t L_9 = ___tobyteorder;
		ByteU5BU5D_t1_109* L_10 = BitConverterEx_GetBytes_m8_358(NULL /*static, unused*/, L_8, 0, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		ByteU5BU5D_t1_109* L_11 = V_2;
		ByteU5BU5D_t1_109* L_12 = V_0;
		int32_t L_13 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_11, 0, (Array_t *)(Array_t *)L_12, ((int32_t)((int32_t)L_13*(int32_t)8)), 4, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_14 = V_3;
		ByteU5BU5D_t1_109* L_15 = V_0;
		int32_t L_16 = V_1;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_14, 0, (Array_t *)(Array_t *)L_15, ((int32_t)((int32_t)((int32_t)((int32_t)L_16*(int32_t)8))+(int32_t)4)), 4, /*hidden argument*/NULL);
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_18 = V_1;
		Fraction32U5BU5D_t8_129* L_19 = ___value;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_19)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		ByteU5BU5D_t1_109* L_20 = V_0;
		return L_20;
	}
}
// System.Void ExifLibrary.UnknownIFDSectionException::.ctor()
extern Il2CppCodeGenString* _stringLiteral5208;
extern "C" void UnknownIFDSectionException__ctor_m8_405 (UnknownIFDSectionException_t8_95 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5208 = il2cpp_codegen_string_literal_from_index(5208);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_1238(__this, _stringLiteral5208, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.UnknownIFDSectionException::.ctor(System.String)
extern "C" void UnknownIFDSectionException__ctor_m8_406 (UnknownIFDSectionException_t8_95 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.UnknownEnumTypeException::.ctor()
extern Il2CppCodeGenString* _stringLiteral5209;
extern "C" void UnknownEnumTypeException__ctor_m8_407 (UnknownEnumTypeException_t8_96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5209 = il2cpp_codegen_string_literal_from_index(5209);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_1238(__this, _stringLiteral5209, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.UnknownEnumTypeException::.ctor(System.String)
extern "C" void UnknownEnumTypeException__ctor_m8_408 (UnknownEnumTypeException_t8_96 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.IFD0IsEmptyException::.ctor()
extern Il2CppCodeGenString* _stringLiteral5210;
extern "C" void IFD0IsEmptyException__ctor_m8_409 (IFD0IsEmptyException_t8_97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5210 = il2cpp_codegen_string_literal_from_index(5210);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_1238(__this, _stringLiteral5210, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.IFD0IsEmptyException::.ctor(System.String)
extern "C" void IFD0IsEmptyException__ctor_m8_410 (IFD0IsEmptyException_t8_97 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExifLibrary.ExifEncodedString::.ctor(ExifLibrary.ExifTag,System.String,System.Text.Encoding)
extern "C" void ExifEncodedString__ctor_m8_411 (ExifEncodedString_t8_98 * __this, int32_t ___tag, String_t* ___value, Encoding_t1_406 * ___encoding, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___value;
		__this->___mValue_3 = L_1;
		Encoding_t1_406 * L_2 = ___encoding;
		__this->___mEncoding_4 = L_2;
		return;
	}
}
// System.Object ExifLibrary.ExifEncodedString::get__Value()
extern "C" Object_t * ExifEncodedString_get__Value_m8_412 (ExifEncodedString_t8_98 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = ExifEncodedString_get_Value_m8_414(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEncodedString::set__Value(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ExifEncodedString_set__Value_m8_413 (ExifEncodedString_t8_98 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifEncodedString_set_Value_m8_415(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.String ExifLibrary.ExifEncodedString::get_Value()
extern "C" String_t* ExifEncodedString_get_Value_m8_414 (ExifEncodedString_t8_98 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEncodedString::set_Value(System.String)
extern "C" void ExifEncodedString_set_Value_m8_415 (ExifEncodedString_t8_98 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Text.Encoding ExifLibrary.ExifEncodedString::get_Encoding()
extern "C" Encoding_t1_406 * ExifEncodedString_get_Encoding_m8_416 (ExifEncodedString_t8_98 * __this, const MethodInfo* method)
{
	{
		Encoding_t1_406 * L_0 = (__this->___mEncoding_4);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEncodedString::set_Encoding(System.Text.Encoding)
extern "C" void ExifEncodedString_set_Encoding_m8_417 (ExifEncodedString_t8_98 * __this, Encoding_t1_406 * ___value, const MethodInfo* method)
{
	{
		Encoding_t1_406 * L_0 = ___value;
		__this->___mEncoding_4 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifEncodedString::ToString()
extern "C" String_t* ExifEncodedString_ToString_m8_418 (ExifEncodedString_t8_98 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEncodedString::get_Interoperability()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5211;
extern Il2CppCodeGenString* _stringLiteral3152;
extern Il2CppCodeGenString* _stringLiteral5212;
extern Il2CppCodeGenString* _stringLiteral5213;
extern Il2CppCodeGenString* _stringLiteral5214;
extern Il2CppCodeGenString* _stringLiteral3254;
extern Il2CppCodeGenString* _stringLiteral5215;
extern "C" ExifInterOperability_t8_113  ExifEncodedString_get_Interoperability_m8_419 (ExifEncodedString_t8_98 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		_stringLiteral5211 = il2cpp_codegen_string_literal_from_index(5211);
		_stringLiteral3152 = il2cpp_codegen_string_literal_from_index(3152);
		_stringLiteral5212 = il2cpp_codegen_string_literal_from_index(5212);
		_stringLiteral5213 = il2cpp_codegen_string_literal_from_index(5213);
		_stringLiteral5214 = il2cpp_codegen_string_literal_from_index(5214);
		_stringLiteral3254 = il2cpp_codegen_string_literal_from_index(3254);
		_stringLiteral5215 = il2cpp_codegen_string_literal_from_index(5215);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	ByteU5BU5D_t1_109* V_1 = {0};
	ByteU5BU5D_t1_109* V_2 = {0};
	ByteU5BU5D_t1_109* V_3 = {0};
	ByteU5BU5D_t1_109* G_B12_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		Encoding_t1_406 * L_1 = (__this->___mEncoding_4);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		V_0 = _stringLiteral5211;
		goto IL_0091;
	}

IL_001c:
	{
		Encoding_t1_406 * L_2 = (__this->___mEncoding_4);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(30 /* System.String System.Text.Encoding::get_EncodingName() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1_601(NULL /*static, unused*/, L_3, _stringLiteral3152, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		V_0 = _stringLiteral5212;
		goto IL_0091;
	}

IL_0041:
	{
		Encoding_t1_406 * L_5 = (__this->___mEncoding_4);
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(30 /* System.String System.Text.Encoding::get_EncodingName() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1_601(NULL /*static, unused*/, L_6, _stringLiteral5213, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		V_0 = _stringLiteral5214;
		goto IL_0091;
	}

IL_0066:
	{
		Encoding_t1_406 * L_8 = (__this->___mEncoding_4);
		NullCheck(L_8);
		String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(30 /* System.String System.Text.Encoding::get_EncodingName() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1_601(NULL /*static, unused*/, L_9, _stringLiteral3254, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008b;
		}
	}
	{
		V_0 = _stringLiteral5215;
		goto IL_0091;
	}

IL_008b:
	{
		V_0 = _stringLiteral5211;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_11 = Encoding_get_ASCII_m1_12357(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_12 = V_0;
		NullCheck(L_11);
		ByteU5BU5D_t1_109* L_13 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, L_12);
		V_1 = L_13;
		Encoding_t1_406 * L_14 = (__this->___mEncoding_4);
		if (L_14)
		{
			goto IL_00bd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_15 = Encoding_get_ASCII_m1_12357(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_16 = (__this->___mValue_3);
		NullCheck(L_15);
		ByteU5BU5D_t1_109* L_17 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_15, L_16);
		G_B12_0 = L_17;
		goto IL_00ce;
	}

IL_00bd:
	{
		Encoding_t1_406 * L_18 = (__this->___mEncoding_4);
		String_t* L_19 = (__this->___mValue_3);
		NullCheck(L_18);
		ByteU5BU5D_t1_109* L_20 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_18, L_19);
		G_B12_0 = L_20;
	}

IL_00ce:
	{
		V_2 = G_B12_0;
		ByteU5BU5D_t1_109* L_21 = V_1;
		NullCheck(L_21);
		ByteU5BU5D_t1_109* L_22 = V_2;
		NullCheck(L_22);
		V_3 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_21)->max_length))))+(int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length))))))));
		ByteU5BU5D_t1_109* L_23 = V_1;
		ByteU5BU5D_t1_109* L_24 = V_3;
		ByteU5BU5D_t1_109* L_25 = V_1;
		NullCheck(L_25);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_23, 0, (Array_t *)(Array_t *)L_24, 0, (((int32_t)((int32_t)(((Array_t *)L_25)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_26 = V_2;
		ByteU5BU5D_t1_109* L_27 = V_3;
		ByteU5BU5D_t1_109* L_28 = V_1;
		NullCheck(L_28);
		ByteU5BU5D_t1_109* L_29 = V_2;
		NullCheck(L_29);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_26, 0, (Array_t *)(Array_t *)L_27, (((int32_t)((int32_t)(((Array_t *)L_28)->max_length)))), (((int32_t)((int32_t)(((Array_t *)L_29)->max_length)))), /*hidden argument*/NULL);
		int32_t L_30 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_31 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_32 = V_3;
		NullCheck(L_32);
		ByteU5BU5D_t1_109* L_33 = V_3;
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, L_31, 7, (((int32_t)((int32_t)(((Array_t *)L_32)->max_length)))), L_33, /*hidden argument*/NULL);
		return L_34;
	}
}
// System.String ExifLibrary.ExifEncodedString::op_Implicit(ExifLibrary.ExifEncodedString)
extern "C" String_t* ExifEncodedString_op_Implicit_m8_420 (Object_t * __this /* static, unused */, ExifEncodedString_t8_98 * ___obj, const MethodInfo* method)
{
	{
		ExifEncodedString_t8_98 * L_0 = ___obj;
		NullCheck(L_0);
		String_t* L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifDateTime::.ctor(ExifLibrary.ExifTag,System.DateTime)
extern "C" void ExifDateTime__ctor_m8_421 (ExifDateTime_t8_100 * __this, int32_t ___tag, DateTime_t1_150  ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		DateTime_t1_150  L_1 = ___value;
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.Object ExifLibrary.ExifDateTime::get__Value()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" Object_t * ExifDateTime_get__Value_m8_422 (ExifDateTime_t8_100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_150  L_0 = ExifDateTime_get_Value_m8_424(__this, /*hidden argument*/NULL);
		DateTime_t1_150  L_1 = L_0;
		Object_t * L_2 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifDateTime::set__Value(System.Object)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" void ExifDateTime_set__Value_m8_423 (ExifDateTime_t8_100 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifDateTime_set_Value_m8_425(__this, ((*(DateTime_t1_150 *)((DateTime_t1_150 *)UnBox (L_0, DateTime_t1_150_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime ExifLibrary.ExifDateTime::get_Value()
extern "C" DateTime_t1_150  ExifDateTime_get_Value_m8_424 (ExifDateTime_t8_100 * __this, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifDateTime::set_Value(System.DateTime)
extern "C" void ExifDateTime_set_Value_m8_425 (ExifDateTime_t8_100 * __this, DateTime_t1_150  ___value, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.String ExifLibrary.ExifDateTime::ToString()
extern Il2CppCodeGenString* _stringLiteral5216;
extern "C" String_t* ExifDateTime_ToString_m8_426 (ExifDateTime_t8_100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral5216 = il2cpp_codegen_string_literal_from_index(5216);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_150 * L_0 = &(__this->___mValue_3);
		String_t* L_1 = DateTime_ToString_m1_13898(L_0, _stringLiteral5216, /*hidden argument*/NULL);
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifDateTime::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifDateTime_get_Interoperability_m8_427 (ExifDateTime_t8_100 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		DateTime_t1_150  L_2 = (__this->___mValue_3);
		ByteU5BU5D_t1_109* L_3 = ExifBitConverter_GetBytes_m8_397(NULL /*static, unused*/, L_2, 1, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_4 = {0};
		ExifInterOperability__ctor_m8_496(&L_4, L_1, 2, ((int32_t)20), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.DateTime ExifLibrary.ExifDateTime::op_Implicit(ExifLibrary.ExifDateTime)
extern "C" DateTime_t1_150  ExifDateTime_op_Implicit_m8_428 (Object_t * __this /* static, unused */, ExifDateTime_t8_100 * ___obj, const MethodInfo* method)
{
	{
		ExifDateTime_t8_100 * L_0 = ___obj;
		NullCheck(L_0);
		DateTime_t1_150  L_1 = (L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifVersion::.ctor(ExifLibrary.ExifTag,System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ExifVersion__ctor_m8_429 (ExifVersion_t8_101 * __this, int32_t ___tag, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___tag;
		ExifProperty__ctor_m8_502(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___value;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)4)))
		{
			goto IL_0026;
		}
	}
	{
		String_t* L_3 = ___value;
		NullCheck(L_3);
		String_t* L_4 = String_Substring_m1_455(L_3, 0, 4, /*hidden argument*/NULL);
		__this->___mValue_3 = L_4;
		goto IL_0059;
	}

IL_0026:
	{
		String_t* L_5 = ___value;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) >= ((int32_t)4)))
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_7 = ___value;
		String_t* L_8 = ___value;
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_571(L_8, /*hidden argument*/NULL);
		String_t* L_10 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_10 = String_CreateString_m1_586(L_10, ((int32_t)32), ((int32_t)((int32_t)4-(int32_t)L_9)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_559(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		__this->___mValue_3 = L_11;
		goto IL_0059;
	}

IL_0052:
	{
		String_t* L_12 = ___value;
		__this->___mValue_3 = L_12;
	}

IL_0059:
	{
		return;
	}
}
// System.Object ExifLibrary.ExifVersion::get__Value()
extern "C" Object_t * ExifVersion_get__Value_m8_430 (ExifVersion_t8_101 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = ExifVersion_get_Value_m8_432(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifVersion::set__Value(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ExifVersion_set__Value_m8_431 (ExifVersion_t8_101 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		ExifVersion_set_Value_m8_433(__this, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.String ExifLibrary.ExifVersion::get_Value()
extern "C" String_t* ExifVersion_get_Value_m8_432 (ExifVersion_t8_101 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifVersion::set_Value(System.String)
extern "C" void ExifVersion_set_Value_m8_433 (ExifVersion_t8_101 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		NullCheck(L_0);
		String_t* L_1 = String_Substring_m1_455(L_0, 0, 4, /*hidden argument*/NULL);
		__this->___mValue_3 = L_1;
		return;
	}
}
// System.String ExifLibrary.ExifVersion::ToString()
extern "C" String_t* ExifVersion_ToString_m8_434 (ExifVersion_t8_101 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mValue_3);
		return L_0;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifVersion::get_Interoperability()
extern TypeInfo* Encoding_t1_406_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifVersion_get_Interoperability_m8_435 (ExifVersion_t8_101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t1_406_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t1_109* V_0 = {0};
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		int32_t L_0 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		if ((((int32_t)L_0) == ((int32_t)((int32_t)236864))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_1 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		if ((((int32_t)L_1) == ((int32_t)((int32_t)240960))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)400002)))))
		{
			goto IL_0053;
		}
	}

IL_0030:
	{
		int32_t L_3 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_4 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t1_406_il2cpp_TypeInfo_var);
		Encoding_t1_406 * L_5 = Encoding_get_ASCII_m1_12357(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = (__this->___mValue_3);
		NullCheck(L_5);
		ByteU5BU5D_t1_109* L_7 = (ByteU5BU5D_t1_109*)VirtFuncInvoker1< ByteU5BU5D_t1_109*, String_t* >::Invoke(11 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, L_6);
		ExifInterOperability_t8_113  L_8 = {0};
		ExifInterOperability__ctor_m8_496(&L_8, L_4, 7, 4, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0053:
	{
		V_0 = ((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 4));
		V_1 = 0;
		goto IL_0081;
	}

IL_0061:
	{
		ByteU5BU5D_t1_109* L_9 = V_0;
		int32_t L_10 = V_1;
		String_t* L_11 = (__this->___mValue_3);
		NullCheck(L_11);
		uint16_t L_12 = String_get_Chars_m1_442(L_11, 0, /*hidden argument*/NULL);
		V_2 = L_12;
		String_t* L_13 = Char_ToString_m1_409((&V_2), /*hidden argument*/NULL);
		uint8_t L_14 = Byte_Parse_m1_238(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10, sizeof(uint8_t))) = (uint8_t)L_14;
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) < ((int32_t)4)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_17 = (((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_18 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		ByteU5BU5D_t1_109* L_19 = V_0;
		ExifInterOperability_t8_113  L_20 = {0};
		ExifInterOperability__ctor_m8_496(&L_20, L_18, 7, 4, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void ExifLibrary.ExifPointSubjectArea::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifPointSubjectArea__ctor_m8_436 (ExifPointSubjectArea_t8_102 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		UInt16U5BU5D_t1_1231* L_1 = ___value;
		ExifUShortArray__ctor_m8_541(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt16[] ExifLibrary.ExifPointSubjectArea::get_Value()
extern "C" UInt16U5BU5D_t1_1231* ExifPointSubjectArea_get_Value_m8_437 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifPointSubjectArea::set_Value(System.UInt16[])
extern "C" void ExifPointSubjectArea_set_Value_m8_438 (ExifPointSubjectArea_t8_102 * __this, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = ___value;
		((ExifUShortArray_t8_103 *)__this)->___mValue_3 = L_0;
		return;
	}
}
// System.UInt16 ExifLibrary.ExifPointSubjectArea::get_X()
extern "C" uint16_t ExifPointSubjectArea_get_X_m8_439 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1, sizeof(uint16_t)));
	}
}
// System.Void ExifLibrary.ExifPointSubjectArea::set_X(System.UInt16)
extern "C" void ExifPointSubjectArea_set_X_m8_440 (ExifPointSubjectArea_t8_102 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		uint16_t L_1 = ___value;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0, sizeof(uint16_t))) = (uint16_t)L_1;
		return;
	}
}
// System.UInt16 ExifLibrary.ExifPointSubjectArea::get_Y()
extern "C" uint16_t ExifPointSubjectArea_get_Y_m8_441 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		int32_t L_1 = 1;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1, sizeof(uint16_t)));
	}
}
// System.Void ExifLibrary.ExifPointSubjectArea::set_Y(System.UInt16)
extern "C" void ExifPointSubjectArea_set_Y_m8_442 (ExifPointSubjectArea_t8_102 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		uint16_t L_1 = ___value;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 1, sizeof(uint16_t))) = (uint16_t)L_1;
		return;
	}
}
// System.String ExifLibrary.ExifPointSubjectArea::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5217;
extern "C" String_t* ExifPointSubjectArea_ToString_m8_443 (ExifPointSubjectArea_t8_102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral5217 = il2cpp_codegen_string_literal_from_index(5217);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		UInt16U5BU5D_t1_1231* L_2 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		uint16_t L_4 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_3, sizeof(uint16_t)));
		Object_t * L_5 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_4);
		UInt16U5BU5D_t1_1231* L_6 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		uint16_t L_8 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_6, L_7, sizeof(uint16_t)));
		Object_t * L_9 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1_12461(L_1, _stringLiteral5217, L_5, L_9, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = StringBuilder_ToString_m1_12428(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void ExifLibrary.ExifCircularSubjectArea::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifCircularSubjectArea__ctor_m8_444 (ExifCircularSubjectArea_t8_104 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		UInt16U5BU5D_t1_1231* L_1 = ___value;
		ExifPointSubjectArea__ctor_m8_436(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt16 ExifLibrary.ExifCircularSubjectArea::get_Diamater()
extern "C" uint16_t ExifCircularSubjectArea_get_Diamater_m8_445 (ExifCircularSubjectArea_t8_104 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		int32_t L_1 = 2;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1, sizeof(uint16_t)));
	}
}
// System.Void ExifLibrary.ExifCircularSubjectArea::set_Diamater(System.UInt16)
extern "C" void ExifCircularSubjectArea_set_Diamater_m8_446 (ExifCircularSubjectArea_t8_104 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		uint16_t L_1 = ___value;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 2, sizeof(uint16_t))) = (uint16_t)L_1;
		return;
	}
}
// System.String ExifLibrary.ExifCircularSubjectArea::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5218;
extern "C" String_t* ExifCircularSubjectArea_ToString_m8_447 (ExifCircularSubjectArea_t8_104 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral5218 = il2cpp_codegen_string_literal_from_index(5218);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		UInt16U5BU5D_t1_1231* L_2 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		uint16_t L_4 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_3, sizeof(uint16_t)));
		Object_t * L_5 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_4);
		UInt16U5BU5D_t1_1231* L_6 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		uint16_t L_8 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_6, L_7, sizeof(uint16_t)));
		Object_t * L_9 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_8);
		UInt16U5BU5D_t1_1231* L_10 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		int32_t L_11 = 2;
		uint16_t L_12 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_10, L_11, sizeof(uint16_t)));
		Object_t * L_13 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1_12462(L_1, _stringLiteral5218, L_5, L_9, L_13, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_14 = V_0;
		NullCheck(L_14);
		String_t* L_15 = StringBuilder_ToString_m1_12428(L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void ExifLibrary.ExifRectangularSubjectArea::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifRectangularSubjectArea__ctor_m8_448 (ExifRectangularSubjectArea_t8_105 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		UInt16U5BU5D_t1_1231* L_1 = ___value;
		ExifPointSubjectArea__ctor_m8_436(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt16 ExifLibrary.ExifRectangularSubjectArea::get_Width()
extern "C" uint16_t ExifRectangularSubjectArea_get_Width_m8_449 (ExifRectangularSubjectArea_t8_105 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		int32_t L_1 = 2;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1, sizeof(uint16_t)));
	}
}
// System.Void ExifLibrary.ExifRectangularSubjectArea::set_Width(System.UInt16)
extern "C" void ExifRectangularSubjectArea_set_Width_m8_450 (ExifRectangularSubjectArea_t8_105 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		uint16_t L_1 = ___value;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 2, sizeof(uint16_t))) = (uint16_t)L_1;
		return;
	}
}
// System.UInt16 ExifLibrary.ExifRectangularSubjectArea::get_Height()
extern "C" uint16_t ExifRectangularSubjectArea_get_Height_m8_451 (ExifRectangularSubjectArea_t8_105 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 3);
		int32_t L_1 = 3;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_1, sizeof(uint16_t)));
	}
}
// System.Void ExifLibrary.ExifRectangularSubjectArea::set_Height(System.UInt16)
extern "C" void ExifRectangularSubjectArea_set_Height_m8_452 (ExifRectangularSubjectArea_t8_105 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1_1231* L_0 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		uint16_t L_1 = ___value;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 3);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 3, sizeof(uint16_t))) = (uint16_t)L_1;
		return;
	}
}
// System.String ExifLibrary.ExifRectangularSubjectArea::ToString()
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5219;
extern "C" String_t* ExifRectangularSubjectArea_ToString_m8_453 (ExifRectangularSubjectArea_t8_105 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		_stringLiteral5219 = il2cpp_codegen_string_literal_from_index(5219);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1_247 * L_1 = V_0;
		ObjectU5BU5D_t1_272* L_2 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		UInt16U5BU5D_t1_1231* L_3 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		uint16_t L_5 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_3, L_4, sizeof(uint16_t)));
		Object_t * L_6 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_272* L_7 = L_2;
		UInt16U5BU5D_t1_1231* L_8 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		int32_t L_9 = 1;
		uint16_t L_10 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_8, L_9, sizeof(uint16_t)));
		Object_t * L_11 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_7;
		UInt16U5BU5D_t1_1231* L_13 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		int32_t L_14 = 2;
		uint16_t L_15 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_13, L_14, sizeof(uint16_t)));
		Object_t * L_16 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_272* L_17 = L_12;
		UInt16U5BU5D_t1_1231* L_18 = (((ExifUShortArray_t8_103 *)__this)->___mValue_3);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		int32_t L_19 = 3;
		uint16_t L_20 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_18, L_19, sizeof(uint16_t)));
		Object_t * L_21 = Box(UInt16_t1_14_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		ArrayElementTypeCheck (L_17, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 3, sizeof(Object_t *))) = (Object_t *)L_21;
		NullCheck(L_1);
		StringBuilder_AppendFormat_m1_12458(L_1, _stringLiteral5219, L_17, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_22 = V_0;
		NullCheck(L_22);
		String_t* L_23 = StringBuilder_ToString_m1_12428(L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void ExifLibrary.GPSLatitudeLongitude::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSLatitudeLongitude__ctor_m8_454 (GPSLatitudeLongitude_t8_106 * __this, int32_t ___tag, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		UFraction32U5BU5D_t8_122* L_1 = ___value;
		ExifURationalArray__ctor_m8_576(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.GPSLatitudeLongitude::get_Value()
extern "C" UFraction32U5BU5D_t8_122* GPSLatitudeLongitude_get_Value_m8_455 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Value(ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSLatitudeLongitude_set_Value_m8_456 (GPSLatitudeLongitude_t8_106 * __this, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = ___value;
		((ExifURationalArray_t8_107 *)__this)->___mValue_3 = L_0;
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSLatitudeLongitude::get_Degrees()
extern "C" UFraction32_t8_121  GPSLatitudeLongitude_get_Degrees_m8_457 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		return (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 0, sizeof(UFraction32_t8_121 ))));
	}
}
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Degrees(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSLatitudeLongitude_set_Degrees_m8_458 (GPSLatitudeLongitude_t8_106 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		UFraction32_t8_121  L_1 = ___value;
		(*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 0, sizeof(UFraction32_t8_121 )))) = L_1;
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSLatitudeLongitude::get_Minutes()
extern "C" UFraction32_t8_121  GPSLatitudeLongitude_get_Minutes_m8_459 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		return (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 1, sizeof(UFraction32_t8_121 ))));
	}
}
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Minutes(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSLatitudeLongitude_set_Minutes_m8_460 (GPSLatitudeLongitude_t8_106 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 1);
		UFraction32_t8_121  L_1 = ___value;
		(*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 1, sizeof(UFraction32_t8_121 )))) = L_1;
		return;
	}
}
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSLatitudeLongitude::get_Seconds()
extern "C" UFraction32_t8_121  GPSLatitudeLongitude_get_Seconds_m8_461 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		return (*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 2, sizeof(UFraction32_t8_121 ))));
	}
}
// System.Void ExifLibrary.GPSLatitudeLongitude::set_Seconds(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSLatitudeLongitude_set_Seconds_m8_462 (GPSLatitudeLongitude_t8_106 * __this, UFraction32_t8_121  ___value, const MethodInfo* method)
{
	{
		UFraction32U5BU5D_t8_122* L_0 = (((ExifURationalArray_t8_107 *)__this)->___mValue_3);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 2);
		UFraction32_t8_121  L_1 = ___value;
		(*(UFraction32_t8_121 *)((UFraction32_t8_121 *)(UFraction32_t8_121 *)SZArrayLdElema(L_0, 2, sizeof(UFraction32_t8_121 )))) = L_1;
		return;
	}
}
// System.Single ExifLibrary.GPSLatitudeLongitude::ToFloat()
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern "C" float GPSLatitudeLongitude_ToFloat_m8_463 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = GPSLatitudeLongitude_get_Degrees_m8_457(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UFraction32_t8_121  L_2 = GPSLatitudeLongitude_get_Minutes_m8_459(__this, /*hidden argument*/NULL);
		float L_3 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		UFraction32_t8_121  L_4 = GPSLatitudeLongitude_get_Seconds_m8_461(__this, /*hidden argument*/NULL);
		float L_5 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return ((float)((float)((float)((float)L_1+(float)((float)((float)L_3/(float)(60.0f)))))+(float)((float)((float)L_5/(float)(3600.0f)))));
	}
}
// System.String ExifLibrary.GPSLatitudeLongitude::ToString()
extern TypeInfo* UFraction32_t8_121_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5220;
extern "C" String_t* GPSLatitudeLongitude_ToString_m8_464 (GPSLatitudeLongitude_t8_106 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UFraction32_t8_121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1972);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5220 = il2cpp_codegen_string_literal_from_index(5220);
		s_Il2CppMethodIntialized = true;
	}
	{
		UFraction32_t8_121  L_0 = GPSLatitudeLongitude_get_Degrees_m8_457(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UFraction32_t8_121_il2cpp_TypeInfo_var);
		float L_1 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_2);
		UFraction32_t8_121  L_4 = GPSLatitudeLongitude_get_Minutes_m8_459(__this, /*hidden argument*/NULL);
		float L_5 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_6);
		UFraction32_t8_121  L_8 = GPSLatitudeLongitude_get_Seconds_m8_461(__this, /*hidden argument*/NULL);
		float L_9 = UFraction32_op_Explicit_m8_783(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1_550(NULL /*static, unused*/, _stringLiteral5220, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single ExifLibrary.GPSLatitudeLongitude::op_Explicit(ExifLibrary.GPSLatitudeLongitude)
extern "C" float GPSLatitudeLongitude_op_Explicit_m8_465 (Object_t * __this /* static, unused */, GPSLatitudeLongitude_t8_106 * ___obj, const MethodInfo* method)
{
	{
		GPSLatitudeLongitude_t8_106 * L_0 = ___obj;
		NullCheck(L_0);
		float L_1 = GPSLatitudeLongitude_ToFloat_m8_463(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
