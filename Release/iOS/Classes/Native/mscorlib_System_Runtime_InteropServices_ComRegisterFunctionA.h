﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.ComRegisterFunctionAttribute
struct  ComRegisterFunctionAttribute_t1_774  : public Attribute_t1_2
{
};
