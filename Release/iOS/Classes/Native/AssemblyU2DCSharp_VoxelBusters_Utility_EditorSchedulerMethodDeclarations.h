﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.EditorScheduler
struct EditorScheduler_t8_152;
// VoxelBusters.Utility.EditorScheduler/CallbackFunction
struct CallbackFunction_t8_151;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.EditorScheduler::.ctor()
extern "C" void EditorScheduler__ctor_m8_878 (EditorScheduler_t8_152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorScheduler::.cctor()
extern "C" void EditorScheduler__cctor_m8_879 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorScheduler::add_ScheduleUpdate(VoxelBusters.Utility.EditorScheduler/CallbackFunction)
extern "C" void EditorScheduler_add_ScheduleUpdate_m8_880 (Object_t * __this /* static, unused */, CallbackFunction_t8_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorScheduler::remove_ScheduleUpdate(VoxelBusters.Utility.EditorScheduler/CallbackFunction)
extern "C" void EditorScheduler_remove_ScheduleUpdate_m8_881 (Object_t * __this /* static, unused */, CallbackFunction_t8_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorScheduler::Update()
extern "C" void EditorScheduler_Update_m8_882 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
