﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.NameOrId
struct NameOrId_t1_663;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.NameOrId::.ctor(System.String)
extern "C" void NameOrId__ctor_m1_7464 (NameOrId_t1_663 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.NameOrId::.ctor(System.Int32)
extern "C" void NameOrId__ctor_m1_7465 (NameOrId_t1_663 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Resources.NameOrId::get_IsName()
extern "C" bool NameOrId_get_IsName_m1_7466 (NameOrId_t1_663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.NameOrId::get_Name()
extern "C" String_t* NameOrId_get_Name_m1_7467 (NameOrId_t1_663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.NameOrId::get_Id()
extern "C" int32_t NameOrId_get_Id_m1_7468 (NameOrId_t1_663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.NameOrId::ToString()
extern "C" String_t* NameOrId_ToString_m1_7469 (NameOrId_t1_663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
