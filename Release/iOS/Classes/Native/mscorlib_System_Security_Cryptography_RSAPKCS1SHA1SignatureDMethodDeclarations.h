﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
struct RSAPKCS1SHA1SignatureDescription_t1_1262;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct AsymmetricSignatureDeformatter_t1_1185;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription::.ctor()
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m1_10722 (RSAPKCS1SHA1SignatureDescription_t1_1262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricSignatureDeformatter System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription::CreateDeformatter(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" AsymmetricSignatureDeformatter_t1_1185 * RSAPKCS1SHA1SignatureDescription_CreateDeformatter_m1_10723 (RSAPKCS1SHA1SignatureDescription_t1_1262 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
