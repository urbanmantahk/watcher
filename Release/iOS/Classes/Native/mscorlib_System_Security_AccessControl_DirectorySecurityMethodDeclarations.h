﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.DirectorySecurity
struct DirectorySecurity_t1_1147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"

// System.Void System.Security.AccessControl.DirectorySecurity::.ctor()
extern "C" void DirectorySecurity__ctor_m1_9833 (DirectorySecurity_t1_1147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DirectorySecurity::.ctor(System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void DirectorySecurity__ctor_m1_9834 (DirectorySecurity_t1_1147 * __this, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
