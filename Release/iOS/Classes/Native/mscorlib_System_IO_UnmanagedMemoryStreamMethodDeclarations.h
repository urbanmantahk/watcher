﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.UnmanagedMemoryStream
struct UnmanagedMemoryStream_t1_460;
// System.EventHandler
struct EventHandler_t1_461;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_FileAccess.h"
#include "mscorlib_System_IO_SeekOrigin.h"

// System.Void System.IO.UnmanagedMemoryStream::.ctor()
extern "C" void UnmanagedMemoryStream__ctor_m1_5428 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::.ctor(System.Byte*,System.Int64)
extern "C" void UnmanagedMemoryStream__ctor_m1_5429 (UnmanagedMemoryStream_t1_460 * __this, uint8_t* ___pointer, int64_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::.ctor(System.Byte*,System.Int64,System.Int64,System.IO.FileAccess)
extern "C" void UnmanagedMemoryStream__ctor_m1_5430 (UnmanagedMemoryStream_t1_460 * __this, uint8_t* ___pointer, int64_t ___length, int64_t ___capacity, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::add_Closed(System.EventHandler)
extern "C" void UnmanagedMemoryStream_add_Closed_m1_5431 (UnmanagedMemoryStream_t1_460 * __this, EventHandler_t1_461 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::remove_Closed(System.EventHandler)
extern "C" void UnmanagedMemoryStream_remove_Closed_m1_5432 (UnmanagedMemoryStream_t1_460 * __this, EventHandler_t1_461 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.UnmanagedMemoryStream::get_CanRead()
extern "C" bool UnmanagedMemoryStream_get_CanRead_m1_5433 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.UnmanagedMemoryStream::get_CanSeek()
extern "C" bool UnmanagedMemoryStream_get_CanSeek_m1_5434 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.UnmanagedMemoryStream::get_CanWrite()
extern "C" bool UnmanagedMemoryStream_get_CanWrite_m1_5435 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.UnmanagedMemoryStream::get_Capacity()
extern "C" int64_t UnmanagedMemoryStream_get_Capacity_m1_5436 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.UnmanagedMemoryStream::get_Length()
extern "C" int64_t UnmanagedMemoryStream_get_Length_m1_5437 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.UnmanagedMemoryStream::get_Position()
extern "C" int64_t UnmanagedMemoryStream_get_Position_m1_5438 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::set_Position(System.Int64)
extern "C" void UnmanagedMemoryStream_set_Position_m1_5439 (UnmanagedMemoryStream_t1_460 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte* System.IO.UnmanagedMemoryStream::get_PositionPointer()
extern "C" uint8_t* UnmanagedMemoryStream_get_PositionPointer_m1_5440 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::set_PositionPointer(System.Byte*)
extern "C" void UnmanagedMemoryStream_set_PositionPointer_m1_5441 (UnmanagedMemoryStream_t1_460 * __this, uint8_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.UnmanagedMemoryStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t UnmanagedMemoryStream_Read_m1_5442 (UnmanagedMemoryStream_t1_460 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.UnmanagedMemoryStream::ReadByte()
extern "C" int32_t UnmanagedMemoryStream_ReadByte_m1_5443 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.UnmanagedMemoryStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C" int64_t UnmanagedMemoryStream_Seek_m1_5444 (UnmanagedMemoryStream_t1_460 * __this, int64_t ___offset, int32_t ___loc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::SetLength(System.Int64)
extern "C" void UnmanagedMemoryStream_SetLength_m1_5445 (UnmanagedMemoryStream_t1_460 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::Flush()
extern "C" void UnmanagedMemoryStream_Flush_m1_5446 (UnmanagedMemoryStream_t1_460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::Dispose(System.Boolean)
extern "C" void UnmanagedMemoryStream_Dispose_m1_5447 (UnmanagedMemoryStream_t1_460 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C" void UnmanagedMemoryStream_Write_m1_5448 (UnmanagedMemoryStream_t1_460 * __this, ByteU5BU5D_t1_109* ___buffer, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::WriteByte(System.Byte)
extern "C" void UnmanagedMemoryStream_WriteByte_m1_5449 (UnmanagedMemoryStream_t1_460 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnmanagedMemoryStream::Initialize(System.Byte*,System.Int64,System.Int64,System.IO.FileAccess)
extern "C" void UnmanagedMemoryStream_Initialize_m1_5450 (UnmanagedMemoryStream_t1_460 * __this, uint8_t* ___pointer, int64_t ___length, int64_t ___capacity, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
