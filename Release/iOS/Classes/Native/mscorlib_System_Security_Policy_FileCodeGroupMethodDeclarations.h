﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.FileCodeGroup
struct FileCodeGroup_t1_1346;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Security.Policy.CodeGroup
struct CodeGroup_t1_1339;
// System.String
struct String_t;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"

// System.Void System.Security.Policy.FileCodeGroup::.ctor(System.Security.Policy.IMembershipCondition,System.Security.Permissions.FileIOPermissionAccess)
extern "C" void FileCodeGroup__ctor_m1_11478 (FileCodeGroup_t1_1346 * __this, Object_t * ___membershipCondition, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.FileCodeGroup::.ctor(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void FileCodeGroup__ctor_m1_11479 (FileCodeGroup_t1_1346 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.FileCodeGroup::Copy()
extern "C" CodeGroup_t1_1339 * FileCodeGroup_Copy_m1_11480 (FileCodeGroup_t1_1346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.FileCodeGroup::get_MergeLogic()
extern "C" String_t* FileCodeGroup_get_MergeLogic_m1_11481 (FileCodeGroup_t1_1346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.FileCodeGroup::Resolve(System.Security.Policy.Evidence)
extern "C" PolicyStatement_t1_1334 * FileCodeGroup_Resolve_m1_11482 (FileCodeGroup_t1_1346 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.FileCodeGroup::ResolveMatchingCodeGroups(System.Security.Policy.Evidence)
extern "C" CodeGroup_t1_1339 * FileCodeGroup_ResolveMatchingCodeGroups_m1_11483 (FileCodeGroup_t1_1346 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.FileCodeGroup::get_AttributeString()
extern "C" String_t* FileCodeGroup_get_AttributeString_m1_11484 (FileCodeGroup_t1_1346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.FileCodeGroup::get_PermissionSetName()
extern "C" String_t* FileCodeGroup_get_PermissionSetName_m1_11485 (FileCodeGroup_t1_1346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.FileCodeGroup::Equals(System.Object)
extern "C" bool FileCodeGroup_Equals_m1_11486 (FileCodeGroup_t1_1346 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.FileCodeGroup::GetHashCode()
extern "C" int32_t FileCodeGroup_GetHashCode_m1_11487 (FileCodeGroup_t1_1346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.FileCodeGroup::ParseXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void FileCodeGroup_ParseXml_m1_11488 (FileCodeGroup_t1_1346 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.FileCodeGroup::CreateXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void FileCodeGroup_CreateXml_m1_11489 (FileCodeGroup_t1_1346 * __this, SecurityElement_t1_242 * ___element, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
