﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.SectionExceeds64KBException
struct SectionExceeds64KBException_t8_135;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLibrary.SectionExceeds64KBException::.ctor()
extern "C" void SectionExceeds64KBException__ctor_m8_637 (SectionExceeds64KBException_t8_135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.SectionExceeds64KBException::.ctor(System.String)
extern "C" void SectionExceeds64KBException__ctor_m8_638 (SectionExceeds64KBException_t8_135 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
