﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_CompilerServices_MethodCodeType.h"

// System.Runtime.CompilerServices.MethodCodeType
struct  MethodCodeType_t1_703 
{
	// System.Int32 System.Runtime.CompilerServices.MethodCodeType::value__
	int32_t ___value___1;
};
