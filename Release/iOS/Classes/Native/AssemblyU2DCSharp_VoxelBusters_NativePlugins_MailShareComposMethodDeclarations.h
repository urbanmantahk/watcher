﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MailShareComposer
struct MailShareComposer_t8_279;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MailShareComposer::.ctor()
extern "C" void MailShareComposer__ctor_m8_1643 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_Subject()
extern "C" String_t* MailShareComposer_get_Subject_m8_1644 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_Subject(System.String)
extern "C" void MailShareComposer_set_Subject_m8_1645 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_Body()
extern "C" String_t* MailShareComposer_get_Body_m8_1646 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_Body(System.String)
extern "C" void MailShareComposer_set_Body_m8_1647 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.MailShareComposer::get_IsHTMLBody()
extern "C" bool MailShareComposer_get_IsHTMLBody_m8_1648 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_IsHTMLBody(System.Boolean)
extern "C" void MailShareComposer_set_IsHTMLBody_m8_1649 (MailShareComposer_t8_279 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.MailShareComposer::get_ToRecipients()
extern "C" StringU5BU5D_t1_238* MailShareComposer_get_ToRecipients_m8_1650 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_ToRecipients(System.String[])
extern "C" void MailShareComposer_set_ToRecipients_m8_1651 (MailShareComposer_t8_279 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.MailShareComposer::get_CCRecipients()
extern "C" StringU5BU5D_t1_238* MailShareComposer_get_CCRecipients_m8_1652 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_CCRecipients(System.String[])
extern "C" void MailShareComposer_set_CCRecipients_m8_1653 (MailShareComposer_t8_279 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] VoxelBusters.NativePlugins.MailShareComposer::get_BCCRecipients()
extern "C" StringU5BU5D_t1_238* MailShareComposer_get_BCCRecipients_m8_1654 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_BCCRecipients(System.String[])
extern "C" void MailShareComposer_set_BCCRecipients_m8_1655 (MailShareComposer_t8_279 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] VoxelBusters.NativePlugins.MailShareComposer::get_AttachmentData()
extern "C" ByteU5BU5D_t1_109* MailShareComposer_get_AttachmentData_m8_1656 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_AttachmentData(System.Byte[])
extern "C" void MailShareComposer_set_AttachmentData_m8_1657 (MailShareComposer_t8_279 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_AttachmentFileName()
extern "C" String_t* MailShareComposer_get_AttachmentFileName_m8_1658 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_AttachmentFileName(System.String)
extern "C" void MailShareComposer_set_AttachmentFileName_m8_1659 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.MailShareComposer::get_MimeType()
extern "C" String_t* MailShareComposer_get_MimeType_m8_1660 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::set_MimeType(System.String)
extern "C" void MailShareComposer_set_MimeType_m8_1661 (MailShareComposer_t8_279 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.MailShareComposer::get_IsReadyToShowView()
extern "C" bool MailShareComposer_get_IsReadyToShowView_m8_1662 (MailShareComposer_t8_279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::AttachImage(UnityEngine.Texture2D)
extern "C" void MailShareComposer_AttachImage_m8_1663 (MailShareComposer_t8_279 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::AddAttachmentAtPath(System.String,System.String)
extern "C" void MailShareComposer_AddAttachmentAtPath_m8_1664 (MailShareComposer_t8_279 * __this, String_t* ____attachmentPath, String_t* ____MIMEType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MailShareComposer::AddAttachment(System.Byte[],System.String,System.String)
extern "C" void MailShareComposer_AddAttachment_m8_1665 (MailShareComposer_t8_279 * __this, ByteU5BU5D_t1_109* ____attachmentData, String_t* ____attachmentFileName, String_t* ____MIMEType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
