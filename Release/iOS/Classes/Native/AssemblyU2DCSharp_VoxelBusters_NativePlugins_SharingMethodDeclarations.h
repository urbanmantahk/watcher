﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing
struct Sharing_t8_273;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.MailShareComposer
struct MailShareComposer_t8_279;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// VoxelBusters.NativePlugins.MessageShareComposer
struct MessageShareComposer_t8_281;
// VoxelBusters.NativePlugins.FBShareComposer
struct FBShareComposer_t8_282;
// VoxelBusters.NativePlugins.TwitterShareComposer
struct TwitterShareComposer_t8_284;
// VoxelBusters.NativePlugins.WhatsAppShareComposer
struct WhatsAppShareComposer_t8_285;
// VoxelBusters.NativePlugins.IShareView
struct IShareView_t8_274;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// VoxelBusters.NativePlugins.ShareSheet
struct ShareSheet_t8_289;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eShareResult.h"

// System.Void VoxelBusters.NativePlugins.Sharing::.ctor()
extern "C" void Sharing__ctor_m8_1586 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SharingFinished(System.String)
extern "C" void Sharing_SharingFinished_m8_1587 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ParseSharingFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseSharingFinishedData_m8_1588 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Sharing::SharingFailedResponse()
extern "C" String_t* Sharing_SharingFailedResponse_m8_1589 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::MailShareFinished(System.String)
extern "C" void Sharing_MailShareFinished_m8_1590 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ParseMailShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseMailShareFinishedData_m8_1591 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Sharing::MailShareFailedResponse()
extern "C" String_t* Sharing_MailShareFailedResponse_m8_1592 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsMailServiceAvailable()
extern "C" bool Sharing_IsMailServiceAvailable_m8_1593 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowMailShareComposer(VoxelBusters.NativePlugins.MailShareComposer)
extern "C" void Sharing_ShowMailShareComposer_m8_1594 (Sharing_t8_273 * __this, MailShareComposer_t8_279 * ____composer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendPlainTextMail(System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendPlainTextMail_m8_1595 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendHTMLTextMail(System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendHTMLTextMail_m8_1596 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____htmlBody, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendMailWithScreenshot(System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendMailWithScreenshot_m8_1597 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendMailWithTexture(System.String,System.String,System.Boolean,UnityEngine.Texture2D,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendMailWithTexture_m8_1598 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, Texture2D_t6_33 * ____texture, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendMailWithAttachment(System.String,System.String,System.Boolean,System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendMailWithAttachment_m8_1599 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, String_t* ____attachmentPath, String_t* ____mimeType, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendMail(System.String,System.String,System.Boolean,System.Byte[],System.String,System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendMail_m8_1600 (Sharing_t8_273 * __this, String_t* ____subject, String_t* ____body, bool ____isHTMLBody, ByteU5BU5D_t1_109* ____attachmentByteArray, String_t* ____mimeType, String_t* ____attachmentFileNameWithExtn, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::MessagingShareFinished(System.String)
extern "C" void Sharing_MessagingShareFinished_m8_1601 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ParseMessagingShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseMessagingShareFinishedData_m8_1602 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Sharing::MessagingShareFailedResponse()
extern "C" String_t* Sharing_MessagingShareFailedResponse_m8_1603 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsMessagingServiceAvailable()
extern "C" bool Sharing_IsMessagingServiceAvailable_m8_1604 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowMessageShareComposer(VoxelBusters.NativePlugins.MessageShareComposer)
extern "C" void Sharing_ShowMessageShareComposer_m8_1605 (Sharing_t8_273 * __this, MessageShareComposer_t8_281 * ____composer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::SendTextMessage(System.String,System.String[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_SendTextMessage_m8_1606 (Sharing_t8_273 * __this, String_t* ____body, StringU5BU5D_t1_238* ____recipients, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::FBShareFinished(System.String)
extern "C" void Sharing_FBShareFinished_m8_1607 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::TwitterShareFinished(System.String)
extern "C" void Sharing_TwitterShareFinished_m8_1608 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ParseFBShareFinishedResponse(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseFBShareFinishedResponse_m8_1609 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ParseTwitterShareFinishedResponse(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseTwitterShareFinishedResponse_m8_1610 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Sharing::FBShareFailedResponse()
extern "C" String_t* Sharing_FBShareFailedResponse_m8_1611 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Sharing::TwitterShareFailedResponse()
extern "C" String_t* Sharing_TwitterShareFailedResponse_m8_1612 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsFBShareServiceAvailable()
extern "C" bool Sharing_IsFBShareServiceAvailable_m8_1613 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsTwitterShareServiceAvailable()
extern "C" bool Sharing_IsTwitterShareServiceAvailable_m8_1614 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowFBShareComposer(VoxelBusters.NativePlugins.FBShareComposer)
extern "C" void Sharing_ShowFBShareComposer_m8_1615 (Sharing_t8_273 * __this, FBShareComposer_t8_282 * ____composer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowTwitterShareComposer(VoxelBusters.NativePlugins.TwitterShareComposer)
extern "C" void Sharing_ShowTwitterShareComposer_m8_1616 (Sharing_t8_273 * __this, TwitterShareComposer_t8_284 * ____composer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::WhatsAppShareFinished(System.String)
extern "C" void Sharing_WhatsAppShareFinished_m8_1617 (Sharing_t8_273 * __this, String_t* ____reasonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ParseWhatsAppShareFinishedData(System.String,VoxelBusters.NativePlugins.eShareResult&)
extern "C" void Sharing_ParseWhatsAppShareFinishedData_m8_1618 (Sharing_t8_273 * __this, String_t* ____resultString, int32_t* ____shareResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Sharing::WhatsAppShareFailedResponse()
extern "C" String_t* Sharing_WhatsAppShareFailedResponse_m8_1619 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Sharing::IsWhatsAppServiceAvailable()
extern "C" bool Sharing_IsWhatsAppServiceAvailable_m8_1620 (Sharing_t8_273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowWhatsAppShareComposer(VoxelBusters.NativePlugins.WhatsAppShareComposer)
extern "C" void Sharing_ShowWhatsAppShareComposer_m8_1621 (Sharing_t8_273 * __this, WhatsAppShareComposer_t8_285 * ____composer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareTextMessageOnWhatsApp(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareTextMessageOnWhatsApp_m8_1622 (Sharing_t8_273 * __this, String_t* ____message, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareScreenshotOnWhatsApp(VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareScreenshotOnWhatsApp_m8_1623 (Sharing_t8_273 * __this, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnWhatsApp_m8_1624 (Sharing_t8_273 * __this, String_t* ____imagePath, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(UnityEngine.Texture2D,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnWhatsApp_m8_1625 (Sharing_t8_273 * __this, Texture2D_t6_33 * ____texture, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnWhatsApp(System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnWhatsApp_m8_1626 (Sharing_t8_273 * __this, ByteU5BU5D_t1_109* ____imageByteArray, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowView(VoxelBusters.NativePlugins.IShareView,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShowView_m8_1627 (Sharing_t8_273 * __this, Object_t * ____shareView, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoxelBusters.NativePlugins.Sharing::ShowViewCoroutine(VoxelBusters.NativePlugins.IShareView,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" Object_t * Sharing_ShowViewCoroutine_m8_1628 (Sharing_t8_273 * __this, Object_t * ____shareView, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShowShareSheet(VoxelBusters.NativePlugins.ShareSheet)
extern "C" void Sharing_ShowShareSheet_m8_1629 (Sharing_t8_273 * __this, ShareSheet_t8_289 * ____shareSheet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareTextMessageOnSocialNetwork(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareTextMessageOnSocialNetwork_m8_1630 (Sharing_t8_273 * __this, String_t* ____message, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareURLOnSocialNetwork(System.String,System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareURLOnSocialNetwork_m8_1631 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareScreenShotOnSocialNetwork(System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareScreenShotOnSocialNetwork_m8_1632 (Sharing_t8_273 * __this, String_t* ____message, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnSocialNetwork(System.String,UnityEngine.Texture2D,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnSocialNetwork_m8_1633 (Sharing_t8_273 * __this, String_t* ____message, Texture2D_t6_33 * ____texture, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnSocialNetwork(System.String,System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnSocialNetwork_m8_1634 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____imagePath, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageOnSocialNetwork(System.String,System.Byte[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageOnSocialNetwork_m8_1635 (Sharing_t8_273 * __this, String_t* ____message, ByteU5BU5D_t1_109* ____imageByteArray, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareMessage(System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareMessage_m8_1636 (Sharing_t8_273 * __this, String_t* ____message, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareURL(System.String,System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareURL_m8_1637 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareScreenShot(System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareScreenShot_m8_1638 (Sharing_t8_273 * __this, String_t* ____message, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImage(System.String,UnityEngine.Texture2D,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImage_m8_1639 (Sharing_t8_273 * __this, String_t* ____message, Texture2D_t6_33 * ____texture, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::ShareImageAtPath(System.String,System.String,VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_ShareImageAtPath_m8_1640 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____imagePath, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::Share(System.String,System.String,System.Byte[],VoxelBusters.NativePlugins.eShareOptions[],VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_Share_m8_1641 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, ByteU5BU5D_t1_109* ____imageByteArray, eShareOptionsU5BU5D_t8_186* ____excludedOptions, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing::Share(System.String,System.String,System.Byte[],System.String,VoxelBusters.NativePlugins.Sharing/SharingCompletion)
extern "C" void Sharing_Share_m8_1642 (Sharing_t8_273 * __this, String_t* ____message, String_t* ____URLString, ByteU5BU5D_t1_109* ____imageByteArray, String_t* ____excludedOptionsJsonString, SharingCompletion_t8_271 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
