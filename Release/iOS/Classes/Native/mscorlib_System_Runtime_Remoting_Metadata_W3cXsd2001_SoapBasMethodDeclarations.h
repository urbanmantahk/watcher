﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary
struct SoapBase64Binary_t1_966;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::.ctor()
extern "C" void SoapBase64Binary__ctor_m1_8674 (SoapBase64Binary_t1_966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::.ctor(System.Byte[])
extern "C" void SoapBase64Binary__ctor_m1_8675 (SoapBase64Binary_t1_966 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::get_Value()
extern "C" ByteU5BU5D_t1_109* SoapBase64Binary_get_Value_m1_8676 (SoapBase64Binary_t1_966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::set_Value(System.Byte[])
extern "C" void SoapBase64Binary_set_Value_m1_8677 (SoapBase64Binary_t1_966 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::get_XsdType()
extern "C" String_t* SoapBase64Binary_get_XsdType_m1_8678 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::GetXsdType()
extern "C" String_t* SoapBase64Binary_GetXsdType_m1_8679 (SoapBase64Binary_t1_966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::Parse(System.String)
extern "C" SoapBase64Binary_t1_966 * SoapBase64Binary_Parse_m1_8680 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapBase64Binary::ToString()
extern "C" String_t* SoapBase64Binary_ToString_m1_8681 (SoapBase64Binary_t1_966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
