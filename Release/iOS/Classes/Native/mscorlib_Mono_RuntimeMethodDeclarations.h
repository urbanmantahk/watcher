﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Runtime
struct Runtime_t1_255;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Runtime::.ctor()
extern "C" void Runtime__ctor_m1_2777 (Runtime_t1_255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Runtime::mono_runtime_install_handlers()
extern "C" void Runtime_mono_runtime_install_handlers_m1_2778 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Runtime::InstallSignalHandlers()
extern "C" void Runtime_InstallSignalHandlers_m1_2779 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Runtime::GetDisplayName()
extern "C" String_t* Runtime_GetDisplayName_m1_2780 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
