﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifUShortArray
struct ExifUShortArray_t8_103;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifUShortArray::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifUShortArray__ctor_m8_541 (ExifUShortArray_t8_103 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifUShortArray::get__Value()
extern "C" Object_t * ExifUShortArray_get__Value_m8_542 (ExifUShortArray_t8_103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUShortArray::set__Value(System.Object)
extern "C" void ExifUShortArray_set__Value_m8_543 (ExifUShortArray_t8_103 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] ExifLibrary.ExifUShortArray::get_Value()
extern "C" UInt16U5BU5D_t1_1231* ExifUShortArray_get_Value_m8_544 (ExifUShortArray_t8_103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUShortArray::set_Value(System.UInt16[])
extern "C" void ExifUShortArray_set_Value_m8_545 (ExifUShortArray_t8_103 * __this, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifUShortArray::ToString()
extern "C" String_t* ExifUShortArray_ToString_m8_546 (ExifUShortArray_t8_103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUShortArray::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUShortArray_get_Interoperability_m8_547 (ExifUShortArray_t8_103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16[] ExifLibrary.ExifUShortArray::op_Implicit(ExifLibrary.ExifUShortArray)
extern "C" UInt16U5BU5D_t1_1231* ExifUShortArray_op_Implicit_m8_548 (Object_t * __this /* static, unused */, ExifUShortArray_t8_103 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
