﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Principal_IdentityReference.h"

// System.Security.Principal.SecurityIdentifier
struct  SecurityIdentifier_t1_1132  : public IdentityReference_t1_1120
{
	// System.String System.Security.Principal.SecurityIdentifier::_value
	String_t* ____value_0;
};
struct SecurityIdentifier_t1_1132_StaticFields{
	// System.Int32 System.Security.Principal.SecurityIdentifier::MaxBinaryLength
	int32_t ___MaxBinaryLength_1;
	// System.Int32 System.Security.Principal.SecurityIdentifier::MinBinaryLength
	int32_t ___MinBinaryLength_2;
};
