﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.ActivationContext
struct ActivationContext_t1_717;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// System.Runtime.Hosting.ActivationArguments
struct  ActivationArguments_t1_716  : public Object_t
{
	// System.ActivationContext System.Runtime.Hosting.ActivationArguments::_context
	ActivationContext_t1_717 * ____context_0;
	// System.ApplicationIdentity System.Runtime.Hosting.ActivationArguments::_identity
	ApplicationIdentity_t1_718 * ____identity_1;
	// System.String[] System.Runtime.Hosting.ActivationArguments::_data
	StringU5BU5D_t1_238* ____data_2;
};
