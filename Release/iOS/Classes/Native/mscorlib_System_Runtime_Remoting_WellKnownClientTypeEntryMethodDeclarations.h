﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.WellKnownClientTypeEntry
struct WellKnownClientTypeEntry_t1_1039;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.WellKnownClientTypeEntry::.ctor(System.Type,System.String)
extern "C" void WellKnownClientTypeEntry__ctor_m1_9316 (WellKnownClientTypeEntry_t1_1039 * __this, Type_t * ___type, String_t* ___objectUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.WellKnownClientTypeEntry::.ctor(System.String,System.String,System.String)
extern "C" void WellKnownClientTypeEntry__ctor_m1_9317 (WellKnownClientTypeEntry_t1_1039 * __this, String_t* ___typeName, String_t* ___assemblyName, String_t* ___objectUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.WellKnownClientTypeEntry::get_ApplicationUrl()
extern "C" String_t* WellKnownClientTypeEntry_get_ApplicationUrl_m1_9318 (WellKnownClientTypeEntry_t1_1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.WellKnownClientTypeEntry::set_ApplicationUrl(System.String)
extern "C" void WellKnownClientTypeEntry_set_ApplicationUrl_m1_9319 (WellKnownClientTypeEntry_t1_1039 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.WellKnownClientTypeEntry::get_ObjectType()
extern "C" Type_t * WellKnownClientTypeEntry_get_ObjectType_m1_9320 (WellKnownClientTypeEntry_t1_1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.WellKnownClientTypeEntry::get_ObjectUrl()
extern "C" String_t* WellKnownClientTypeEntry_get_ObjectUrl_m1_9321 (WellKnownClientTypeEntry_t1_1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.WellKnownClientTypeEntry::ToString()
extern "C" String_t* WellKnownClientTypeEntry_ToString_m1_9322 (WellKnownClientTypeEntry_t1_1039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
