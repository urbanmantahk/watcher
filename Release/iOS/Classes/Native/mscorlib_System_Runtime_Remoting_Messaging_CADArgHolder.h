﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.CADArgHolder
struct  CADArgHolder_t1_921  : public Object_t
{
	// System.Int32 System.Runtime.Remoting.Messaging.CADArgHolder::index
	int32_t ___index_0;
};
