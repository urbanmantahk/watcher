﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_15.h"

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15480_gshared (InternalEnumerator_1_t1_1963 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15480(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1963 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15480_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15481_gshared (InternalEnumerator_1_t1_1963 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15481(__this, method) (( void (*) (InternalEnumerator_1_t1_1963 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15481_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15482_gshared (InternalEnumerator_1_t1_1963 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15482(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1963 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15482_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15483_gshared (InternalEnumerator_1_t1_1963 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15483(__this, method) (( void (*) (InternalEnumerator_1_t1_1963 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15483_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15484_gshared (InternalEnumerator_1_t1_1963 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15484(__this, method) (( bool (*) (InternalEnumerator_1_t1_1963 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15484_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m1_15485_gshared (InternalEnumerator_1_t1_1963 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15485(__this, method) (( int8_t (*) (InternalEnumerator_1_t1_1963 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15485_gshared)(__this, method)
