﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.SHA512
struct SHA512_t1_1257;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.SHA512::.ctor()
extern "C" void SHA512__ctor_m1_10686 (SHA512_t1_1257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SHA512 System.Security.Cryptography.SHA512::Create()
extern "C" SHA512_t1_1257 * SHA512_Create_m1_10687 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SHA512 System.Security.Cryptography.SHA512::Create(System.String)
extern "C" SHA512_t1_1257 * SHA512_Create_m1_10688 (Object_t * __this /* static, unused */, String_t* ___hashName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
