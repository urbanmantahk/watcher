﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t1_142;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifUIntArray
struct  ExifUIntArray_t8_119  : public ExifProperty_t8_99
{
	// System.UInt32[] ExifLibrary.ExifUIntArray::mValue
	UInt32U5BU5D_t1_142* ___mValue_3;
};
