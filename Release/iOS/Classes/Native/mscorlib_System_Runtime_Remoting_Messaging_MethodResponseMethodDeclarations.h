﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MethodResponse
struct MethodResponse_t1_936;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Exception
struct Exception_t1_33;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Runtime.Remoting.Messaging.CADMethodReturnMessage
struct CADMethodReturnMessage_t1_881;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.String
struct String_t;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Remoting.Messaging.MethodResponse::.ctor(System.Runtime.Remoting.Messaging.Header[],System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" void MethodResponse__ctor_m1_8511 (MethodResponse_t1_936 * __this, HeaderU5BU5D_t1_927* ___h1, Object_t * ___mcm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::.ctor(System.Exception,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" void MethodResponse__ctor_m1_8512 (MethodResponse_t1_936 * __this, Exception_t1_33 * ___e, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::.ctor(System.Object,System.Object[],System.Runtime.Remoting.Messaging.LogicalCallContext,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" void MethodResponse__ctor_m1_8513 (MethodResponse_t1_936 * __this, Object_t * ___returnValue, ObjectU5BU5D_t1_272* ___outArgs, LogicalCallContext_t1_941 * ___callCtx, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::.ctor(System.Runtime.Remoting.Messaging.IMethodCallMessage,System.Runtime.Remoting.Messaging.CADMethodReturnMessage)
extern "C" void MethodResponse__ctor_m1_8514 (MethodResponse_t1_936 * __this, Object_t * ___msg, CADMethodReturnMessage_t1_881 * ___retmsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MethodResponse__ctor_m1_8515 (MethodResponse_t1_936 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodResponse::System.Runtime.Remoting.Messaging.IInternalMessage.get_Uri()
extern "C" String_t* MethodResponse_System_Runtime_Remoting_Messaging_IInternalMessage_get_Uri_m1_8516 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::System.Runtime.Remoting.Messaging.IInternalMessage.set_Uri(System.String)
extern "C" void MethodResponse_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m1_8517 (MethodResponse_t1_936 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.Runtime.Remoting.Messaging.MethodResponse::System.Runtime.Remoting.Messaging.IInternalMessage.get_TargetIdentity()
extern "C" Identity_t1_943 * MethodResponse_System_Runtime_Remoting_Messaging_IInternalMessage_get_TargetIdentity_m1_8518 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::System.Runtime.Remoting.Messaging.IInternalMessage.set_TargetIdentity(System.Runtime.Remoting.Identity)
extern "C" void MethodResponse_System_Runtime_Remoting_Messaging_IInternalMessage_set_TargetIdentity_m1_8519 (MethodResponse_t1_936 * __this, Identity_t1_943 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::InitMethodProperty(System.String,System.Object)
extern "C" void MethodResponse_InitMethodProperty_m1_8520 (MethodResponse_t1_936 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MethodResponse::get_ArgCount()
extern "C" int32_t MethodResponse_get_ArgCount_m1_8521 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MethodResponse::get_Args()
extern "C" ObjectU5BU5D_t1_272* MethodResponse_get_Args_m1_8522 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.Remoting.Messaging.MethodResponse::get_Exception()
extern "C" Exception_t1_33 * MethodResponse_get_Exception_m1_8523 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.MethodResponse::get_HasVarArgs()
extern "C" bool MethodResponse_get_HasVarArgs_m1_8524 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MethodResponse::get_LogicalCallContext()
extern "C" LogicalCallContext_t1_941 * MethodResponse_get_LogicalCallContext_m1_8525 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.Messaging.MethodResponse::get_MethodBase()
extern "C" MethodBase_t1_335 * MethodResponse_get_MethodBase_m1_8526 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodResponse::get_MethodName()
extern "C" String_t* MethodResponse_get_MethodName_m1_8527 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodResponse::get_MethodSignature()
extern "C" Object_t * MethodResponse_get_MethodSignature_m1_8528 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.MethodResponse::get_OutArgCount()
extern "C" int32_t MethodResponse_get_OutArgCount_m1_8529 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.MethodResponse::get_OutArgs()
extern "C" ObjectU5BU5D_t1_272* MethodResponse_get_OutArgs_m1_8530 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodResponse::get_Properties()
extern "C" Object_t * MethodResponse_get_Properties_m1_8531 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodResponse::get_ReturnValue()
extern "C" Object_t * MethodResponse_get_ReturnValue_m1_8532 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodResponse::get_TypeName()
extern "C" String_t* MethodResponse_get_TypeName_m1_8533 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodResponse::get_Uri()
extern "C" String_t* MethodResponse_get_Uri_m1_8534 (MethodResponse_t1_936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::set_Uri(System.String)
extern "C" void MethodResponse_set_Uri_m1_8535 (MethodResponse_t1_936 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodResponse::GetArg(System.Int32)
extern "C" Object_t * MethodResponse_GetArg_m1_8536 (MethodResponse_t1_936 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodResponse::GetArgName(System.Int32)
extern "C" String_t* MethodResponse_GetArgName_m1_8537 (MethodResponse_t1_936 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MethodResponse_GetObjectData_m1_8538 (MethodResponse_t1_936 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodResponse::GetOutArg(System.Int32)
extern "C" Object_t * MethodResponse_GetOutArg_m1_8539 (MethodResponse_t1_936 * __this, int32_t ___argNum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.MethodResponse::GetOutArgName(System.Int32)
extern "C" String_t* MethodResponse_GetOutArgName_m1_8540 (MethodResponse_t1_936 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodResponse::HeaderHandler(System.Runtime.Remoting.Messaging.Header[])
extern "C" Object_t * MethodResponse_HeaderHandler_m1_8541 (MethodResponse_t1_936 * __this, HeaderU5BU5D_t1_927* ___h, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodResponse::RootSetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MethodResponse_RootSetObjectData_m1_8542 (MethodResponse_t1_936 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
