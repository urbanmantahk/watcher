﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void FUNCDESC_t1_792_marshal(const FUNCDESC_t1_792& unmarshaled, FUNCDESC_t1_792_marshaled& marshaled);
extern "C" void FUNCDESC_t1_792_marshal_back(const FUNCDESC_t1_792_marshaled& marshaled, FUNCDESC_t1_792& unmarshaled);
extern "C" void FUNCDESC_t1_792_marshal_cleanup(FUNCDESC_t1_792_marshaled& marshaled);
