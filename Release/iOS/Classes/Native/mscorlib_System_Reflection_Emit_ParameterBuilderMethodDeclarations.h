﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ParameterBuilder
struct ParameterBuilder_t1_545;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_Emit_ParameterToken.h"

// System.Void System.Reflection.Emit.ParameterBuilder::.ctor(System.Reflection.MethodBase,System.Int32,System.Reflection.ParameterAttributes,System.String)
extern "C" void ParameterBuilder__ctor_m1_6274 (ParameterBuilder_t1_545 * __this, MethodBase_t1_335 * ___mb, int32_t ___pos, int32_t ___attributes, String_t* ___strParamName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::System.Runtime.InteropServices._ParameterBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ParameterBuilder_System_Runtime_InteropServices__ParameterBuilder_GetIDsOfNames_m1_6275 (ParameterBuilder_t1_545 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::System.Runtime.InteropServices._ParameterBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ParameterBuilder_System_Runtime_InteropServices__ParameterBuilder_GetTypeInfo_m1_6276 (ParameterBuilder_t1_545 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::System.Runtime.InteropServices._ParameterBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void ParameterBuilder_System_Runtime_InteropServices__ParameterBuilder_GetTypeInfoCount_m1_6277 (ParameterBuilder_t1_545 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::System.Runtime.InteropServices._ParameterBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void ParameterBuilder_System_Runtime_InteropServices__ParameterBuilder_Invoke_m1_6278 (ParameterBuilder_t1_545 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ParameterBuilder::get_Attributes()
extern "C" int32_t ParameterBuilder_get_Attributes_m1_6279 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterBuilder::get_IsIn()
extern "C" bool ParameterBuilder_get_IsIn_m1_6280 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterBuilder::get_IsOut()
extern "C" bool ParameterBuilder_get_IsOut_m1_6281 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ParameterBuilder::get_IsOptional()
extern "C" bool ParameterBuilder_get_IsOptional_m1_6282 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ParameterBuilder::get_Name()
extern "C" String_t* ParameterBuilder_get_Name_m1_6283 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ParameterBuilder::get_Position()
extern "C" int32_t ParameterBuilder_get_Position_m1_6284 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ParameterToken System.Reflection.Emit.ParameterBuilder::GetToken()
extern "C" ParameterToken_t1_546  ParameterBuilder_GetToken_m1_6285 (ParameterBuilder_t1_545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::SetConstant(System.Object)
extern "C" void ParameterBuilder_SetConstant_m1_6286 (ParameterBuilder_t1_545 * __this, Object_t * ___defaultValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void ParameterBuilder_SetCustomAttribute_m1_6287 (ParameterBuilder_t1_545 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void ParameterBuilder_SetCustomAttribute_m1_6288 (ParameterBuilder_t1_545 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.ParameterBuilder::SetMarshal(System.Reflection.Emit.UnmanagedMarshal)
extern "C" void ParameterBuilder_SetMarshal_m1_6289 (ParameterBuilder_t1_545 * __this, UnmanagedMarshal_t1_505 * ___unmanagedMarshal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
