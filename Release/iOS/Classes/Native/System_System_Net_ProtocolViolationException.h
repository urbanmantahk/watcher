﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_InvalidOperationException.h"

// System.Net.ProtocolViolationException
struct  ProtocolViolationException_t3_123  : public InvalidOperationException_t1_1559
{
};
