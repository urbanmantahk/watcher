﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/ReadOnlyListWrapper
struct ReadOnlyListWrapper_t1_271;
// System.Collections.IList
struct IList_t1_262;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/ReadOnlyListWrapper::.ctor(System.Collections.IList)
extern "C" void ReadOnlyListWrapper__ctor_m1_3027 (ReadOnlyListWrapper_t1_271 * __this, Object_t * ___innerList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.ArrayList/ReadOnlyListWrapper::get_ErrorMessage()
extern "C" String_t* ReadOnlyListWrapper_get_ErrorMessage_m1_3028 (ReadOnlyListWrapper_t1_271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ReadOnlyListWrapper::get_IsReadOnly()
extern "C" bool ReadOnlyListWrapper_get_IsReadOnly_m1_3029 (ReadOnlyListWrapper_t1_271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ReadOnlyListWrapper::get_Item(System.Int32)
extern "C" Object_t * ReadOnlyListWrapper_get_Item_m1_3030 (ReadOnlyListWrapper_t1_271 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ReadOnlyListWrapper::set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyListWrapper_set_Item_m1_3031 (ReadOnlyListWrapper_t1_271 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
