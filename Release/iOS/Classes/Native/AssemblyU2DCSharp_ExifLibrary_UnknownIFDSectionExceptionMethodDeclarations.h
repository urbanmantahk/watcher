﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.UnknownIFDSectionException
struct UnknownIFDSectionException_t8_95;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLibrary.UnknownIFDSectionException::.ctor()
extern "C" void UnknownIFDSectionException__ctor_m8_405 (UnknownIFDSectionException_t8_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.UnknownIFDSectionException::.ctor(System.String)
extern "C" void UnknownIFDSectionException__ctor_m8_406 (UnknownIFDSectionException_t8_95 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
