﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.UnknownWrapper
struct UnknownWrapper_t1_842;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.UnknownWrapper::.ctor(System.Object)
extern "C" void UnknownWrapper__ctor_m1_7936 (UnknownWrapper_t1_842 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.UnknownWrapper::get_WrappedObject()
extern "C" Object_t * UnknownWrapper_get_WrappedObject_m1_7937 (UnknownWrapper_t1_842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
