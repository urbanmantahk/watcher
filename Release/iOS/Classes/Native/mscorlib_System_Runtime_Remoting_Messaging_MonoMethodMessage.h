﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MonoMethod
struct MonoMethod_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Remoting.Messaging.AsyncResult
struct AsyncResult_t1_916;
// System.String
struct String_t;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct MethodCallDictionary_t1_944;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_CallType.h"

// System.Runtime.Remoting.Messaging.MonoMethodMessage
struct  MonoMethodMessage_t1_919  : public Object_t
{
	// System.Reflection.MonoMethod System.Runtime.Remoting.Messaging.MonoMethodMessage::method
	MonoMethod_t * ___method_0;
	// System.Object[] System.Runtime.Remoting.Messaging.MonoMethodMessage::args
	ObjectU5BU5D_t1_272* ___args_1;
	// System.String[] System.Runtime.Remoting.Messaging.MonoMethodMessage::names
	StringU5BU5D_t1_238* ___names_2;
	// System.Byte[] System.Runtime.Remoting.Messaging.MonoMethodMessage::arg_types
	ByteU5BU5D_t1_109* ___arg_types_3;
	// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.MonoMethodMessage::ctx
	LogicalCallContext_t1_941 * ___ctx_4;
	// System.Object System.Runtime.Remoting.Messaging.MonoMethodMessage::rval
	Object_t * ___rval_5;
	// System.Exception System.Runtime.Remoting.Messaging.MonoMethodMessage::exc
	Exception_t1_33 * ___exc_6;
	// System.Runtime.Remoting.Messaging.AsyncResult System.Runtime.Remoting.Messaging.MonoMethodMessage::asyncResult
	AsyncResult_t1_916 * ___asyncResult_7;
	// System.Runtime.Remoting.Messaging.CallType System.Runtime.Remoting.Messaging.MonoMethodMessage::call_type
	int32_t ___call_type_8;
	// System.String System.Runtime.Remoting.Messaging.MonoMethodMessage::uri
	String_t* ___uri_9;
	// System.Runtime.Remoting.Messaging.MethodCallDictionary System.Runtime.Remoting.Messaging.MonoMethodMessage::properties
	MethodCallDictionary_t1_944 * ___properties_10;
	// System.Type[] System.Runtime.Remoting.Messaging.MonoMethodMessage::methodSignature
	TypeU5BU5D_t1_31* ___methodSignature_11;
	// System.Runtime.Remoting.Identity System.Runtime.Remoting.Messaging.MonoMethodMessage::identity
	Identity_t1_943 * ___identity_12;
};
