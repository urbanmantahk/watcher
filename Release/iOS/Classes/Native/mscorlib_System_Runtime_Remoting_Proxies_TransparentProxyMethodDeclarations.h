﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Proxies.TransparentProxy
struct TransparentProxy_t1_1005;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Proxies.TransparentProxy::.ctor()
extern "C" void TransparentProxy__ctor_m1_8988 (TransparentProxy_t1_1005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
