﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UIIOS
struct UIIOS_t8_304;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion
struct SingleFieldPromptCompletion_t8_301;
// VoxelBusters.NativePlugins.UI/LoginPromptCompletion
struct LoginPromptCompletion_t8_302;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eToastMessageLe.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void VoxelBusters.NativePlugins.UIIOS::.ctor()
extern "C" void UIIOS__ctor_m8_1779 (UIIOS_t8_304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ParseAlertDialogDismissedData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void UIIOS_ParseAlertDialogDismissedData_m8_1780 (UIIOS_t8_304 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____callerTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::showAlertDialog(System.String,System.String,System.String,System.String)
extern "C" void UIIOS_showAlertDialog_m8_1781 (Object_t * __this /* static, unused */, String_t* ____title, String_t* ____message, String_t* ____buttonsList, String_t* ____callerTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowAlertDialogWithMultipleButtons(System.String,System.String,System.String[],System.String)
extern "C" void UIIOS_ShowAlertDialogWithMultipleButtons_m8_1782 (UIIOS_t8_304 * __this, String_t* ____title, String_t* ____message, StringU5BU5D_t1_238* ____buttonsList, String_t* ____callbackTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ParseSingleFieldPromptClosedData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void UIIOS_ParseSingleFieldPromptClosedData_m8_1783 (UIIOS_t8_304 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____inputText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ParseLoginPromptClosedData(System.Collections.IDictionary,System.String&,System.String&,System.String&)
extern "C" void UIIOS_ParseLoginPromptClosedData_m8_1784 (UIIOS_t8_304 * __this, Object_t * ____dataDict, String_t** ____buttonPressed, String_t** ____usernameText, String_t** ____passwordText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::showSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String)
extern "C" void UIIOS_showSingleFieldPromptDialog_m8_1785 (Object_t * __this /* static, unused */, String_t* ____title, String_t* ____message, String_t* ____placeholder, bool ____useSecureText, String_t* ____buttonsList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::showLoginPromptDialog(System.String,System.String,System.String,System.String,System.String)
extern "C" void UIIOS_showLoginPromptDialog_m8_1786 (Object_t * __this /* static, unused */, String_t* ____title, String_t* ____message, String_t* ____placeholder1, String_t* ____placeholder2, String_t* ____buttonsList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowSingleFieldPromptDialog(System.String,System.String,System.String,System.Boolean,System.String[],VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion)
extern "C" void UIIOS_ShowSingleFieldPromptDialog_m8_1787 (UIIOS_t8_304 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder, bool ____useSecureText, StringU5BU5D_t1_238* ____buttonsList, SingleFieldPromptCompletion_t8_301 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowLoginPromptDialog(System.String,System.String,System.String,System.String,System.String[],VoxelBusters.NativePlugins.UI/LoginPromptCompletion)
extern "C" void UIIOS_ShowLoginPromptDialog_m8_1788 (UIIOS_t8_304 * __this, String_t* ____title, String_t* ____message, String_t* ____placeholder1, String_t* ____placeholder2, StringU5BU5D_t1_238* ____buttonsList, LoginPromptCompletion_t8_302 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::setPopoverPoint(System.Single,System.Single)
extern "C" void UIIOS_setPopoverPoint_m8_1789 (Object_t * __this /* static, unused */, float ___x, float ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::ShowToast(System.String,VoxelBusters.NativePlugins.eToastMessageLength)
extern "C" void UIIOS_ShowToast_m8_1790 (UIIOS_t8_304 * __this, String_t* ____message, int32_t ____length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UIIOS::SetPopoverPoint(UnityEngine.Vector2)
extern "C" void UIIOS_SetPopoverPoint_m8_1791 (UIIOS_t8_304 * __this, Vector2_t6_47  ____position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
