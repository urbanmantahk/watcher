﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.OperationCanceledException
struct OperationCanceledException_t1_1589;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.OperationCanceledException::.ctor()
extern "C" void OperationCanceledException__ctor_m1_14538 (OperationCanceledException_t1_1589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.OperationCanceledException::.ctor(System.String)
extern "C" void OperationCanceledException__ctor_m1_14539 (OperationCanceledException_t1_1589 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.OperationCanceledException::.ctor(System.String,System.Exception)
extern "C" void OperationCanceledException__ctor_m1_14540 (OperationCanceledException_t1_1589 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.OperationCanceledException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void OperationCanceledException__ctor_m1_14541 (OperationCanceledException_t1_1589 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
