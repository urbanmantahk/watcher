﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange
struct EnumeratorWithRange_t1_260;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::.ctor(System.Collections.IEnumerator,System.Int32,System.Int32)
extern "C" void EnumeratorWithRange__ctor_m1_2797 (EnumeratorWithRange_t1_260 * __this, Object_t * ___enumerator, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::Clone()
extern "C" Object_t * EnumeratorWithRange_Clone_m1_2798 (EnumeratorWithRange_t1_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::get_Current()
extern "C" Object_t * EnumeratorWithRange_get_Current_m1_2799 (EnumeratorWithRange_t1_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::MoveNext()
extern "C" bool EnumeratorWithRange_MoveNext_m1_2800 (EnumeratorWithRange_t1_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListAdapter/EnumeratorWithRange::Reset()
extern "C" void EnumeratorWithRange_Reset_m1_2801 (EnumeratorWithRange_t1_260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
