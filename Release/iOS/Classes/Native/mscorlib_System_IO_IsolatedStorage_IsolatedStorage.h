﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_MarshalByRefObject.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageScope.h"

// System.IO.IsolatedStorage.IsolatedStorage
struct  IsolatedStorage_t1_394  : public MarshalByRefObject_t1_69
{
	// System.IO.IsolatedStorage.IsolatedStorageScope System.IO.IsolatedStorage.IsolatedStorage::storage_scope
	int32_t ___storage_scope_1;
	// System.Object System.IO.IsolatedStorage.IsolatedStorage::_assemblyIdentity
	Object_t * ____assemblyIdentity_2;
	// System.Object System.IO.IsolatedStorage.IsolatedStorage::_domainIdentity
	Object_t * ____domainIdentity_3;
	// System.Object System.IO.IsolatedStorage.IsolatedStorage::_applicationIdentity
	Object_t * ____applicationIdentity_4;
};
