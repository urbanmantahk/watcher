﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_DTDCollectionBase.h"

// Mono.Xml.DTDNotationDeclarationCollection
struct  DTDNotationDeclarationCollection_t4_87  : public DTDCollectionBase_t4_91
{
};
