﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Score/ReportScoreCompletion
struct ReportScoreCompletion_t8_232;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.Score/ReportScoreCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void ReportScoreCompletion__ctor_m8_1300 (ReportScoreCompletion_t8_232 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score/ReportScoreCompletion::Invoke(System.Boolean,System.String)
extern "C" void ReportScoreCompletion_Invoke_m8_1301 (ReportScoreCompletion_t8_232 * __this, bool ____success, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReportScoreCompletion_t8_232(Il2CppObject* delegate, bool ____success, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.Score/ReportScoreCompletion::BeginInvoke(System.Boolean,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * ReportScoreCompletion_BeginInvoke_m8_1302 (ReportScoreCompletion_t8_232 * __this, bool ____success, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Score/ReportScoreCompletion::EndInvoke(System.IAsyncResult)
extern "C" void ReportScoreCompletion_EndInvoke_m8_1303 (ReportScoreCompletion_t8_232 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
