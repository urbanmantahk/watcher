﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boomlagoon.JSON.JSONValue
struct JSONValue_t8_4;
// System.String
struct String_t;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// Boomlagoon.JSON.JSONArray
struct JSONArray_t8_6;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Boomlagoon_JSON_JSONValueType.h"

// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONValueType)
extern "C" void JSONValue__ctor_m8_2 (JSONValue_t8_4 * __this, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::.ctor(System.String)
extern "C" void JSONValue__ctor_m8_3 (JSONValue_t8_4 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::.ctor(System.Double)
extern "C" void JSONValue__ctor_m8_4 (JSONValue_t8_4 * __this, double ___number, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONObject)
extern "C" void JSONValue__ctor_m8_5 (JSONValue_t8_4 * __this, JSONObject_t8_5 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONArray)
extern "C" void JSONValue__ctor_m8_6 (JSONValue_t8_4 * __this, JSONArray_t8_6 * ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::.ctor(System.Boolean)
extern "C" void JSONValue__ctor_m8_7 (JSONValue_t8_4 * __this, bool ___boolean, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::.ctor(Boomlagoon.JSON.JSONValue)
extern "C" void JSONValue__ctor_m8_8 (JSONValue_t8_4 * __this, JSONValue_t8_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValueType Boomlagoon.JSON.JSONValue::get_Type()
extern "C" int32_t JSONValue_get_Type_m8_9 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Type(Boomlagoon.JSON.JSONValueType)
extern "C" void JSONValue_set_Type_m8_10 (JSONValue_t8_4 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boomlagoon.JSON.JSONValue::get_Str()
extern "C" String_t* JSONValue_get_Str_m8_11 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Str(System.String)
extern "C" void JSONValue_set_Str_m8_12 (JSONValue_t8_4 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Boomlagoon.JSON.JSONValue::get_Number()
extern "C" double JSONValue_get_Number_m8_13 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Number(System.Double)
extern "C" void JSONValue_set_Number_m8_14 (JSONValue_t8_4 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONObject Boomlagoon.JSON.JSONValue::get_Obj()
extern "C" JSONObject_t8_5 * JSONValue_get_Obj_m8_15 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Obj(Boomlagoon.JSON.JSONObject)
extern "C" void JSONValue_set_Obj_m8_16 (JSONValue_t8_4 * __this, JSONObject_t8_5 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONValue::get_Array()
extern "C" JSONArray_t8_6 * JSONValue_get_Array_m8_17 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Array(Boomlagoon.JSON.JSONArray)
extern "C" void JSONValue_set_Array_m8_18 (JSONValue_t8_4 * __this, JSONArray_t8_6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Boomlagoon.JSON.JSONValue::get_Boolean()
extern "C" bool JSONValue_get_Boolean_m8_19 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Boolean(System.Boolean)
extern "C" void JSONValue_set_Boolean_m8_20 (JSONValue_t8_4 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::get_Parent()
extern "C" JSONValue_t8_4 * JSONValue_get_Parent_m8_21 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONValue::set_Parent(Boomlagoon.JSON.JSONValue)
extern "C" void JSONValue_set_Parent_m8_22 (JSONValue_t8_4 * __this, JSONValue_t8_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boomlagoon.JSON.JSONValue::ToString()
extern "C" String_t* JSONValue_ToString_m8_23 (JSONValue_t8_4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(System.String)
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_24 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(System.Double)
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_25 (Object_t * __this /* static, unused */, double ___number, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(Boomlagoon.JSON.JSONObject)
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_26 (Object_t * __this /* static, unused */, JSONObject_t8_5 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(Boomlagoon.JSON.JSONArray)
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_27 (Object_t * __this /* static, unused */, JSONArray_t8_6 * ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONValue::op_Implicit(System.Boolean)
extern "C" JSONValue_t8_4 * JSONValue_op_Implicit_m8_28 (Object_t * __this /* static, unused */, bool ___boolean, const MethodInfo* method) IL2CPP_METHOD_ATTR;
