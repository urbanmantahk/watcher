﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.TwitterUser
struct  TwitterUser_t8_298  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.TwitterUser::<UserID>k__BackingField
	String_t* ___U3CUserIDU3Ek__BackingField_0;
	// System.String VoxelBusters.NativePlugins.TwitterUser::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.Boolean VoxelBusters.NativePlugins.TwitterUser::<IsVerified>k__BackingField
	bool ___U3CIsVerifiedU3Ek__BackingField_2;
	// System.Boolean VoxelBusters.NativePlugins.TwitterUser::<IsProtected>k__BackingField
	bool ___U3CIsProtectedU3Ek__BackingField_3;
	// System.String VoxelBusters.NativePlugins.TwitterUser::<ProfileImageURL>k__BackingField
	String_t* ___U3CProfileImageURLU3Ek__BackingField_4;
};
