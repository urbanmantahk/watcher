﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// Mono.Globalization.Unicode.Normalization
struct  Normalization_t1_117  : public Object_t
{
};
struct Normalization_t1_117_StaticFields{
	// System.Byte* Mono.Globalization.Unicode.Normalization::props
	uint8_t* ___props_17;
	// System.Int32* Mono.Globalization.Unicode.Normalization::mappedChars
	int32_t* ___mappedChars_18;
	// System.Int16* Mono.Globalization.Unicode.Normalization::charMapIndex
	int16_t* ___charMapIndex_19;
	// System.Int16* Mono.Globalization.Unicode.Normalization::helperIndex
	int16_t* ___helperIndex_20;
	// System.UInt16* Mono.Globalization.Unicode.Normalization::mapIdxToComposite
	uint16_t* ___mapIdxToComposite_21;
	// System.Byte* Mono.Globalization.Unicode.Normalization::combiningClass
	uint8_t* ___combiningClass_22;
	// System.Object Mono.Globalization.Unicode.Normalization::forLock
	Object_t * ___forLock_23;
	// System.Boolean Mono.Globalization.Unicode.Normalization::isReady
	bool ___isReady_24;
};
