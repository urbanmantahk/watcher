﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m5_66_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m5_66(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m5_66_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m5_67_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m5_67(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m5_67_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m5_68_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m5_68(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m5_68_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m5_69_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m5_69(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m5_69_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m5_70_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m5_70(__this, method) (( Object_t* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m5_70_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern "C" bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m5_71_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m5_71(__this, method) (( bool (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m5_71_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m5_72_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m5_72(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m5_72_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m5_73_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m5_73(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t5_20 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m5_73_gshared)(__this, method)
