﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>
struct GetEnumeratorU3Ec__Iterator0_t3_262;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator0__ctor_m3_1977_gshared (GetEnumeratorU3Ec__Iterator0_t3_262 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0__ctor_m3_1977(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator0_t3_262 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0__ctor_m3_1977_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C" KeyValuePair_2_t1_2015  GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_1978_gshared (GetEnumeratorU3Ec__Iterator0_t3_262 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_1978(__this, method) (( KeyValuePair_2_t1_2015  (*) (GetEnumeratorU3Ec__Iterator0_t3_262 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_1978_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_1979_gshared (GetEnumeratorU3Ec__Iterator0_t3_262 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_1979(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator0_t3_262 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_1979_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator0_MoveNext_m3_1980_gshared (GetEnumeratorU3Ec__Iterator0_t3_262 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_MoveNext_m3_1980(__this, method) (( bool (*) (GetEnumeratorU3Ec__Iterator0_t3_262 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_MoveNext_m3_1980_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator0_Dispose_m3_1981_gshared (GetEnumeratorU3Ec__Iterator0_t3_262 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Dispose_m3_1981(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator0_t3_262 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Dispose_m3_1981_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Object,System.Object>::Reset()
extern "C" void GetEnumeratorU3Ec__Iterator0_Reset_m3_1982_gshared (GetEnumeratorU3Ec__Iterator0_t3_262 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Reset_m3_1982(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator0_t3_262 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Reset_m3_1982_gshared)(__this, method)
