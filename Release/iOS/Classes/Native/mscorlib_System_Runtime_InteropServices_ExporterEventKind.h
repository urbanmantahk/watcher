﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ExporterEventKind.h"

// System.Runtime.InteropServices.ExporterEventKind
struct  ExporterEventKind_t1_789 
{
	// System.Int32 System.Runtime.InteropServices.ExporterEventKind::value__
	int32_t ___value___1;
};
