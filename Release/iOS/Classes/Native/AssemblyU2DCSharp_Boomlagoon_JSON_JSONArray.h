﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue>
struct List_1_t1_1898;

#include "mscorlib_System_Object.h"

// Boomlagoon.JSON.JSONArray
struct  JSONArray_t8_6  : public Object_t
{
	// System.Collections.Generic.List`1<Boomlagoon.JSON.JSONValue> Boomlagoon.JSON.JSONArray::values
	List_1_t1_1898 * ___values_0;
};
