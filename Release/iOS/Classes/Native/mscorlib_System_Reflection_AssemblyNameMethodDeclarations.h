﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyName
struct AssemblyName_t1_576;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_t1_577;
// System.Version
struct Version_t1_578;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionComp.h"

// System.Void System.Reflection.AssemblyName::.ctor()
extern "C" void AssemblyName__ctor_m1_6695 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::.ctor(System.String)
extern "C" void AssemblyName__ctor_m1_6696 (AssemblyName_t1_576 * __this, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void AssemblyName__ctor_m1_6697 (AssemblyName_t1_576 * __this, SerializationInfo_t1_293 * ___si, StreamingContext_t1_1050  ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::System.Runtime.InteropServices._AssemblyName.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void AssemblyName_System_Runtime_InteropServices__AssemblyName_GetIDsOfNames_m1_6698 (AssemblyName_t1_576 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::System.Runtime.InteropServices._AssemblyName.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void AssemblyName_System_Runtime_InteropServices__AssemblyName_GetTypeInfo_m1_6699 (AssemblyName_t1_576 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::System.Runtime.InteropServices._AssemblyName.GetTypeInfoCount(System.UInt32&)
extern "C" void AssemblyName_System_Runtime_InteropServices__AssemblyName_GetTypeInfoCount_m1_6700 (AssemblyName_t1_576 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::System.Runtime.InteropServices._AssemblyName.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void AssemblyName_System_Runtime_InteropServices__AssemblyName_Invoke_m1_6701 (AssemblyName_t1_576 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.AssemblyName::ParseName(System.Reflection.AssemblyName,System.String)
extern "C" bool AssemblyName_ParseName_m1_6702 (Object_t * __this /* static, unused */, AssemblyName_t1_576 * ___aname, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ProcessorArchitecture System.Reflection.AssemblyName::get_ProcessorArchitecture()
extern "C" int32_t AssemblyName_get_ProcessorArchitecture_m1_6703 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_ProcessorArchitecture(System.Reflection.ProcessorArchitecture)
extern "C" void AssemblyName_set_ProcessorArchitecture_m1_6704 (AssemblyName_t1_576 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyName::get_Name()
extern "C" String_t* AssemblyName_get_Name_m1_6705 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_Name(System.String)
extern "C" void AssemblyName_set_Name_m1_6706 (AssemblyName_t1_576 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyName::get_CodeBase()
extern "C" String_t* AssemblyName_get_CodeBase_m1_6707 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_CodeBase(System.String)
extern "C" void AssemblyName_set_CodeBase_m1_6708 (AssemblyName_t1_576 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyName::get_EscapedCodeBase()
extern "C" String_t* AssemblyName_get_EscapedCodeBase_m1_6709 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Reflection.AssemblyName::get_CultureInfo()
extern "C" CultureInfo_t1_277 * AssemblyName_get_CultureInfo_m1_6710 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_CultureInfo(System.Globalization.CultureInfo)
extern "C" void AssemblyName_set_CultureInfo_m1_6711 (AssemblyName_t1_576 * __this, CultureInfo_t1_277 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyNameFlags System.Reflection.AssemblyName::get_Flags()
extern "C" int32_t AssemblyName_get_Flags_m1_6712 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_Flags(System.Reflection.AssemblyNameFlags)
extern "C" void AssemblyName_set_Flags_m1_6713 (AssemblyName_t1_576 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyName::get_FullName()
extern "C" String_t* AssemblyName_get_FullName_m1_6714 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Assemblies.AssemblyHashAlgorithm System.Reflection.AssemblyName::get_HashAlgorithm()
extern "C" int32_t AssemblyName_get_HashAlgorithm_m1_6715 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_HashAlgorithm(System.Configuration.Assemblies.AssemblyHashAlgorithm)
extern "C" void AssemblyName_set_HashAlgorithm_m1_6716 (AssemblyName_t1_576 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.StrongNameKeyPair System.Reflection.AssemblyName::get_KeyPair()
extern "C" StrongNameKeyPair_t1_577 * AssemblyName_get_KeyPair_m1_6717 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_KeyPair(System.Reflection.StrongNameKeyPair)
extern "C" void AssemblyName_set_KeyPair_m1_6718 (AssemblyName_t1_576 * __this, StrongNameKeyPair_t1_577 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Reflection.AssemblyName::get_Version()
extern "C" Version_t1_578 * AssemblyName_get_Version_m1_6719 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_Version(System.Version)
extern "C" void AssemblyName_set_Version_m1_6720 (AssemblyName_t1_576 * __this, Version_t1_578 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Configuration.Assemblies.AssemblyVersionCompatibility System.Reflection.AssemblyName::get_VersionCompatibility()
extern "C" int32_t AssemblyName_get_VersionCompatibility_m1_6721 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::set_VersionCompatibility(System.Configuration.Assemblies.AssemblyVersionCompatibility)
extern "C" void AssemblyName_set_VersionCompatibility_m1_6722 (AssemblyName_t1_576 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyName::ToString()
extern "C" String_t* AssemblyName_ToString_m1_6723 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.AssemblyName::GetPublicKey()
extern "C" ByteU5BU5D_t1_109* AssemblyName_GetPublicKey_m1_6724 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.AssemblyName::GetPublicKeyToken()
extern "C" ByteU5BU5D_t1_109* AssemblyName_GetPublicKeyToken_m1_6725 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.AssemblyName::get_IsPublicKeyValid()
extern "C" bool AssemblyName_get_IsPublicKeyValid_m1_6726 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.AssemblyName::InternalGetPublicKeyToken()
extern "C" ByteU5BU5D_t1_109* AssemblyName_InternalGetPublicKeyToken_m1_6727 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.AssemblyName::ComputePublicKeyToken()
extern "C" ByteU5BU5D_t1_109* AssemblyName_ComputePublicKeyToken_m1_6728 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.AssemblyName::ReferenceMatchesDefinition(System.Reflection.AssemblyName,System.Reflection.AssemblyName)
extern "C" bool AssemblyName_ReferenceMatchesDefinition_m1_6729 (Object_t * __this /* static, unused */, AssemblyName_t1_576 * ___reference, AssemblyName_t1_576 * ___definition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::SetPublicKey(System.Byte[])
extern "C" void AssemblyName_SetPublicKey_m1_6730 (AssemblyName_t1_576 * __this, ByteU5BU5D_t1_109* ___publicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::SetPublicKeyToken(System.Byte[])
extern "C" void AssemblyName_SetPublicKeyToken_m1_6731 (AssemblyName_t1_576 * __this, ByteU5BU5D_t1_109* ___publicKeyToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void AssemblyName_GetObjectData_m1_6732 (AssemblyName_t1_576 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.AssemblyName::Clone()
extern "C" Object_t * AssemblyName_Clone_m1_6733 (AssemblyName_t1_576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyName::OnDeserialization(System.Object)
extern "C" void AssemblyName_OnDeserialization_m1_6734 (AssemblyName_t1_576 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.AssemblyName::GetAssemblyName(System.String)
extern "C" AssemblyName_t1_576 * AssemblyName_GetAssemblyName_m1_6735 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
