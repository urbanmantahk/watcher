﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "mscorlib_System_Object.h"

// System.Security.PermissionBuilder
struct  PermissionBuilder_t1_1391  : public Object_t
{
};
struct PermissionBuilder_t1_1391_StaticFields{
	// System.Object[] System.Security.PermissionBuilder::psNone
	ObjectU5BU5D_t1_272* ___psNone_0;
};
