﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_ActivationContext_ContextForm.h"

// System.ActivationContext
struct  ActivationContext_t1_717  : public Object_t
{
	// System.ActivationContext/ContextForm System.ActivationContext::_form
	int32_t ____form_0;
	// System.ApplicationIdentity System.ActivationContext::_appid
	ApplicationIdentity_t1_718 * ____appid_1;
	// System.Boolean System.ActivationContext::_disposed
	bool ____disposed_2;
};
