﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.Internal.JSONConstants
struct JSONConstants_t8_53;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.Internal.JSONConstants::.ctor()
extern "C" void JSONConstants__ctor_m8_271 (JSONConstants_t8_53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
