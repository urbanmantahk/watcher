﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.UmAlQuraCalendar
struct  UmAlQuraCalendar_t1_392  : public Calendar_t1_338
{
	// System.Int32 System.Globalization.UmAlQuraCalendar::M_AddHijriDate
	int32_t ___M_AddHijriDate_10;
};
struct UmAlQuraCalendar_t1_392_StaticFields{
	// System.Int32 System.Globalization.UmAlQuraCalendar::M_MinFixed
	int32_t ___M_MinFixed_8;
	// System.Int32 System.Globalization.UmAlQuraCalendar::M_MaxFixed
	int32_t ___M_MaxFixed_9;
	// System.DateTime System.Globalization.UmAlQuraCalendar::Min
	DateTime_t1_150  ___Min_11;
	// System.DateTime System.Globalization.UmAlQuraCalendar::Max
	DateTime_t1_150  ___Max_12;
};
