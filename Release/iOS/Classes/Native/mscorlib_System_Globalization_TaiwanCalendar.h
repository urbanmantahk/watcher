﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCGregorianEraHandler
struct CCGregorianEraHandler_t1_353;

#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.TaiwanCalendar
struct  TaiwanCalendar_t1_387  : public Calendar_t1_338
{
};
struct TaiwanCalendar_t1_387_StaticFields{
	// System.Globalization.CCGregorianEraHandler System.Globalization.TaiwanCalendar::M_EraHandler
	CCGregorianEraHandler_t1_353 * ___M_EraHandler_7;
	// System.DateTime System.Globalization.TaiwanCalendar::TaiwanMin
	DateTime_t1_150  ___TaiwanMin_8;
	// System.DateTime System.Globalization.TaiwanCalendar::TaiwanMax
	DateTime_t1_150  ___TaiwanMax_9;
};
