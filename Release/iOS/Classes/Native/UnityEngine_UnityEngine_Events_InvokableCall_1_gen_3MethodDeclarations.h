﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t6_338;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t6_312;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m6_1992_gshared (InvokableCall_1_t6_338 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m6_1992(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t6_338 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m6_1992_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m6_1993_gshared (InvokableCall_1_t6_338 * __this, UnityAction_1_t6_312 * ___action, const MethodInfo* method);
#define InvokableCall_1__ctor_m6_1993(__this, ___action, method) (( void (*) (InvokableCall_1_t6_338 *, UnityAction_1_t6_312 *, const MethodInfo*))InvokableCall_1__ctor_m6_1993_gshared)(__this, ___action, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m6_1994_gshared (InvokableCall_1_t6_338 * __this, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m6_1994(__this, ___args, method) (( void (*) (InvokableCall_1_t6_338 *, ObjectU5BU5D_t1_272*, const MethodInfo*))InvokableCall_1_Invoke_m6_1994_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m6_1995_gshared (InvokableCall_1_t6_338 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m6_1995(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t6_338 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m6_1995_gshared)(__this, ___targetObj, ___method, method)
