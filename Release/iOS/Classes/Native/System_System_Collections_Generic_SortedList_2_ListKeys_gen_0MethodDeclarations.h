﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>
struct ListKeys_t3_279;
// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_2773;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void ListKeys__ctor_m3_2167_gshared (ListKeys_t3_279 * __this, SortedList_2_t3_248 * ___host, const MethodInfo* method);
#define ListKeys__ctor_m3_2167(__this, ___host, method) (( void (*) (ListKeys_t3_279 *, SortedList_2_t3_248 *, const MethodInfo*))ListKeys__ctor_m3_2167_gshared)(__this, ___host, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ListKeys_System_Collections_IEnumerable_GetEnumerator_m3_2168_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_System_Collections_IEnumerable_GetEnumerator_m3_2168(__this, method) (( Object_t * (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_System_Collections_IEnumerable_GetEnumerator_m3_2168_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Add(TKey)
extern "C" void ListKeys_Add_m3_2169_gshared (ListKeys_t3_279 * __this, int32_t ___item, const MethodInfo* method);
#define ListKeys_Add_m3_2169(__this, ___item, method) (( void (*) (ListKeys_t3_279 *, int32_t, const MethodInfo*))ListKeys_Add_m3_2169_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Remove(TKey)
extern "C" bool ListKeys_Remove_m3_2170_gshared (ListKeys_t3_279 * __this, int32_t ___key, const MethodInfo* method);
#define ListKeys_Remove_m3_2170(__this, ___key, method) (( bool (*) (ListKeys_t3_279 *, int32_t, const MethodInfo*))ListKeys_Remove_m3_2170_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Clear()
extern "C" void ListKeys_Clear_m3_2171_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_Clear_m3_2171(__this, method) (( void (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_Clear_m3_2171_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::CopyTo(TKey[],System.Int32)
extern "C" void ListKeys_CopyTo_m3_2172_gshared (ListKeys_t3_279 * __this, Int32U5BU5D_t1_275* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ListKeys_CopyTo_m3_2172(__this, ___array, ___arrayIndex, method) (( void (*) (ListKeys_t3_279 *, Int32U5BU5D_t1_275*, int32_t, const MethodInfo*))ListKeys_CopyTo_m3_2172_gshared)(__this, ___array, ___arrayIndex, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Contains(TKey)
extern "C" bool ListKeys_Contains_m3_2173_gshared (ListKeys_t3_279 * __this, int32_t ___item, const MethodInfo* method);
#define ListKeys_Contains_m3_2173(__this, ___item, method) (( bool (*) (ListKeys_t3_279 *, int32_t, const MethodInfo*))ListKeys_Contains_m3_2173_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::IndexOf(TKey)
extern "C" int32_t ListKeys_IndexOf_m3_2174_gshared (ListKeys_t3_279 * __this, int32_t ___item, const MethodInfo* method);
#define ListKeys_IndexOf_m3_2174(__this, ___item, method) (( int32_t (*) (ListKeys_t3_279 *, int32_t, const MethodInfo*))ListKeys_IndexOf_m3_2174_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Insert(System.Int32,TKey)
extern "C" void ListKeys_Insert_m3_2175_gshared (ListKeys_t3_279 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define ListKeys_Insert_m3_2175(__this, ___index, ___item, method) (( void (*) (ListKeys_t3_279 *, int32_t, int32_t, const MethodInfo*))ListKeys_Insert_m3_2175_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::RemoveAt(System.Int32)
extern "C" void ListKeys_RemoveAt_m3_2176_gshared (ListKeys_t3_279 * __this, int32_t ___index, const MethodInfo* method);
#define ListKeys_RemoveAt_m3_2176(__this, ___index, method) (( void (*) (ListKeys_t3_279 *, int32_t, const MethodInfo*))ListKeys_RemoveAt_m3_2176_gshared)(__this, ___index, method)
// TKey System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_Item(System.Int32)
extern "C" int32_t ListKeys_get_Item_m3_2177_gshared (ListKeys_t3_279 * __this, int32_t ___index, const MethodInfo* method);
#define ListKeys_get_Item_m3_2177(__this, ___index, method) (( int32_t (*) (ListKeys_t3_279 *, int32_t, const MethodInfo*))ListKeys_get_Item_m3_2177_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::set_Item(System.Int32,TKey)
extern "C" void ListKeys_set_Item_m3_2178_gshared (ListKeys_t3_279 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define ListKeys_set_Item_m3_2178(__this, ___index, ___value, method) (( void (*) (ListKeys_t3_279 *, int32_t, int32_t, const MethodInfo*))ListKeys_set_Item_m3_2178_gshared)(__this, ___index, ___value, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::GetEnumerator()
extern "C" Object_t* ListKeys_GetEnumerator_m3_2179_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_GetEnumerator_m3_2179(__this, method) (( Object_t* (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_GetEnumerator_m3_2179_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_Count()
extern "C" int32_t ListKeys_get_Count_m3_2180_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_get_Count_m3_2180(__this, method) (( int32_t (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_get_Count_m3_2180_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_IsSynchronized()
extern "C" bool ListKeys_get_IsSynchronized_m3_2181_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_get_IsSynchronized_m3_2181(__this, method) (( bool (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_get_IsSynchronized_m3_2181_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_IsReadOnly()
extern "C" bool ListKeys_get_IsReadOnly_m3_2182_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_get_IsReadOnly_m3_2182(__this, method) (( bool (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_get_IsReadOnly_m3_2182_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_SyncRoot()
extern "C" Object_t * ListKeys_get_SyncRoot_m3_2183_gshared (ListKeys_t3_279 * __this, const MethodInfo* method);
#define ListKeys_get_SyncRoot_m3_2183(__this, method) (( Object_t * (*) (ListKeys_t3_279 *, const MethodInfo*))ListKeys_get_SyncRoot_m3_2183_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::CopyTo(System.Array,System.Int32)
extern "C" void ListKeys_CopyTo_m3_2184_gshared (ListKeys_t3_279 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ListKeys_CopyTo_m3_2184(__this, ___array, ___arrayIndex, method) (( void (*) (ListKeys_t3_279 *, Array_t *, int32_t, const MethodInfo*))ListKeys_CopyTo_m3_2184_gshared)(__this, ___array, ___arrayIndex, method)
