﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Globalization_CalendarAlgorithmType.h"

// System.Globalization.CalendarAlgorithmType
struct  CalendarAlgorithmType_t1_339 
{
	// System.Int32 System.Globalization.CalendarAlgorithmType::value__
	int32_t ___value___1;
};
