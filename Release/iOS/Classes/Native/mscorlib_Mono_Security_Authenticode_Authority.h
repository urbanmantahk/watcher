﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_Mono_Security_Authenticode_Authority.h"

// Mono.Security.Authenticode.Authority
struct  Authority_t1_144 
{
	// System.Int32 Mono.Security.Authenticode.Authority::value__
	int32_t ___value___1;
};
