﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.UtilityDemo
struct UtilityDemo_t8_189;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::.ctor()
extern "C" void UtilityDemo__ctor_m8_1107 (UtilityDemo_t8_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::Start()
extern "C" void UtilityDemo_Start_m8_1108 (UtilityDemo_t8_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::DisplayFeatureFunctionalities()
extern "C" void UtilityDemo_DisplayFeatureFunctionalities_m8_1109 (UtilityDemo_t8_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Demo.UtilityDemo::GetUUID()
extern "C" String_t* UtilityDemo_GetUUID_m8_1110 (UtilityDemo_t8_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::OpenStoreLink(System.String)
extern "C" void UtilityDemo_OpenStoreLink_m8_1111 (UtilityDemo_t8_189 * __this, String_t* ____applicationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::AskForReviewNow()
extern "C" void UtilityDemo_AskForReviewNow_m8_1112 (UtilityDemo_t8_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UtilityDemo::SetApplicationIconBadgeNumber()
extern "C" void UtilityDemo_SetApplicationIconBadgeNumber_m8_1113 (UtilityDemo_t8_189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
