﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings
struct iOSSettings_t8_319;
// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings
struct AndroidSettings_t8_317;
// VoxelBusters.NativePlugins.ApplicationSettings/Features
struct Features_t8_318;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.ApplicationSettings
struct  ApplicationSettings_t8_320  : public Object_t
{
	// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings VoxelBusters.NativePlugins.ApplicationSettings::m_iOS
	iOSSettings_t8_319 * ___m_iOS_0;
	// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings VoxelBusters.NativePlugins.ApplicationSettings::m_android
	AndroidSettings_t8_317 * ___m_android_1;
	// VoxelBusters.NativePlugins.ApplicationSettings/Features VoxelBusters.NativePlugins.ApplicationSettings::m_supportedFeatures
	Features_t8_318 * ___m_supportedFeatures_2;
};
