﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t1_2351;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t1_1841;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_287;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t1_2815;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m1_20362_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_20362(__this, method) (( void (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1__ctor_m1_20362_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_20363_gshared (Collection_1_t1_2351 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_20363(__this, ___list, method) (( void (*) (Collection_1_t1_2351 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_20363_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20364_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20364(__this, method) (( bool (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20364_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_20365_gshared (Collection_1_t1_2351 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_20365(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2351 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_20365_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20366_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20366(__this, method) (( Object_t * (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20366_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_20367_gshared (Collection_1_t1_2351 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_20367(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2351 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_20367_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_20368_gshared (Collection_1_t1_2351 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_20368(__this, ___value, method) (( bool (*) (Collection_1_t1_2351 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_20368_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_20369_gshared (Collection_1_t1_2351 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_20369(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2351 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_20369_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_20370_gshared (Collection_1_t1_2351 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_20370(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2351 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_20370_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_20371_gshared (Collection_1_t1_2351 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_20371(__this, ___value, method) (( void (*) (Collection_1_t1_2351 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_20371_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20372_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20372(__this, method) (( bool (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20372_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20373_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20373(__this, method) (( Object_t * (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20373_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_20374_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_20374(__this, method) (( bool (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_20374_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_20375_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_20375(__this, method) (( bool (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_20375_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_20376_gshared (Collection_1_t1_2351 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_20376(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2351 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_20376_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_20377_gshared (Collection_1_t1_2351 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_20377(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2351 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_20377_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m1_20378_gshared (Collection_1_t1_2351 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_20378(__this, ___item, method) (( void (*) (Collection_1_t1_2351 *, UIVertex_t6_158 , const MethodInfo*))Collection_1_Add_m1_20378_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m1_20379_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_20379(__this, method) (( void (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_Clear_m1_20379_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_20380_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_20380(__this, method) (( void (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_ClearItems_m1_20380_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m1_20381_gshared (Collection_1_t1_2351 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_20381(__this, ___item, method) (( bool (*) (Collection_1_t1_2351 *, UIVertex_t6_158 , const MethodInfo*))Collection_1_Contains_m1_20381_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_20382_gshared (Collection_1_t1_2351 * __this, UIVertexU5BU5D_t6_287* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_20382(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2351 *, UIVertexU5BU5D_t6_287*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_20382_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_20383_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_20383(__this, method) (( Object_t* (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_GetEnumerator_m1_20383_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_20384_gshared (Collection_1_t1_2351 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_20384(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2351 *, UIVertex_t6_158 , const MethodInfo*))Collection_1_IndexOf_m1_20384_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_20385_gshared (Collection_1_t1_2351 * __this, int32_t ___index, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_20385(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2351 *, int32_t, UIVertex_t6_158 , const MethodInfo*))Collection_1_Insert_m1_20385_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_20386_gshared (Collection_1_t1_2351 * __this, int32_t ___index, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_20386(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2351 *, int32_t, UIVertex_t6_158 , const MethodInfo*))Collection_1_InsertItem_m1_20386_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_20387_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_20387(__this, method) (( Object_t* (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_get_Items_m1_20387_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m1_20388_gshared (Collection_1_t1_2351 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_20388(__this, ___item, method) (( bool (*) (Collection_1_t1_2351 *, UIVertex_t6_158 , const MethodInfo*))Collection_1_Remove_m1_20388_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_20389_gshared (Collection_1_t1_2351 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_20389(__this, ___index, method) (( void (*) (Collection_1_t1_2351 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_20389_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_20390_gshared (Collection_1_t1_2351 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_20390(__this, ___index, method) (( void (*) (Collection_1_t1_2351 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_20390_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_20391_gshared (Collection_1_t1_2351 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_20391(__this, method) (( int32_t (*) (Collection_1_t1_2351 *, const MethodInfo*))Collection_1_get_Count_m1_20391_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t6_158  Collection_1_get_Item_m1_20392_gshared (Collection_1_t1_2351 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_20392(__this, ___index, method) (( UIVertex_t6_158  (*) (Collection_1_t1_2351 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_20392_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_20393_gshared (Collection_1_t1_2351 * __this, int32_t ___index, UIVertex_t6_158  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_20393(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2351 *, int32_t, UIVertex_t6_158 , const MethodInfo*))Collection_1_set_Item_m1_20393_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_20394_gshared (Collection_1_t1_2351 * __this, int32_t ___index, UIVertex_t6_158  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_20394(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2351 *, int32_t, UIVertex_t6_158 , const MethodInfo*))Collection_1_SetItem_m1_20394_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_20395_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_20395(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_20395_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t6_158  Collection_1_ConvertItem_m1_20396_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_20396(__this /* static, unused */, ___item, method) (( UIVertex_t6_158  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_20396_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_20397_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_20397(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_20397_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_20398_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_20398(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_20398_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_20399_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_20399(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_20399_gshared)(__this /* static, unused */, ___list, method)
