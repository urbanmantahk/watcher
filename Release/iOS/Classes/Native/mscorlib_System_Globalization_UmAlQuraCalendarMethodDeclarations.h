﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.UmAlQuraCalendar
struct UmAlQuraCalendar_t1_392;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.UmAlQuraCalendar::.ctor()
extern "C" void UmAlQuraCalendar__ctor_m1_4553 (UmAlQuraCalendar_t1_392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::.cctor()
extern "C" void UmAlQuraCalendar__cctor_m1_4554 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.UmAlQuraCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* UmAlQuraCalendar_get_Eras_m1_4555 (UmAlQuraCalendar_t1_392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::get_TwoDigitYearMax()
extern "C" int32_t UmAlQuraCalendar_get_TwoDigitYearMax_m1_4556 (UmAlQuraCalendar_t1_392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void UmAlQuraCalendar_set_TwoDigitYearMax_m1_4557 (UmAlQuraCalendar_t1_392 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::get_AddHijriDate()
extern "C" int32_t UmAlQuraCalendar_get_AddHijriDate_m1_4558 (UmAlQuraCalendar_t1_392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::set_AddHijriDate(System.Int32)
extern "C" void UmAlQuraCalendar_set_AddHijriDate_m1_4559 (UmAlQuraCalendar_t1_392 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::M_CheckFixedHijri(System.String,System.Int32)
extern "C" void UmAlQuraCalendar_M_CheckFixedHijri_m1_4560 (UmAlQuraCalendar_t1_392 * __this, String_t* ___param, int32_t ___rdHijri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::M_CheckDateTime(System.DateTime)
extern "C" void UmAlQuraCalendar_M_CheckDateTime_m1_4561 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::M_FromDateTime(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_M_FromDateTime_m1_4562 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::M_ToDateTime(System.Int32)
extern "C" DateTime_t1_150  UmAlQuraCalendar_M_ToDateTime_m1_4563 (UmAlQuraCalendar_t1_392 * __this, int32_t ___rd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::M_ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  UmAlQuraCalendar_M_ToDateTime_m1_4564 (UmAlQuraCalendar_t1_392 * __this, int32_t ___date, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::M_CheckEra(System.Int32&)
extern "C" void UmAlQuraCalendar_M_CheckEra_m1_4565 (UmAlQuraCalendar_t1_392 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void UmAlQuraCalendar_M_CheckYE_m1_4566 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::M_CheckYME(System.Int32,System.Int32,System.Int32&)
extern "C" void UmAlQuraCalendar_M_CheckYME_m1_4567 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.UmAlQuraCalendar::M_CheckYMDE(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" void UmAlQuraCalendar_M_CheckYMDE_m1_4568 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  UmAlQuraCalendar_AddMonths_m1_4569 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  UmAlQuraCalendar_AddYears_m1_4570 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_GetDayOfMonth_m1_4571 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.UmAlQuraCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_GetDayOfWeek_m1_4572 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_GetDayOfYear_m1_4573 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t UmAlQuraCalendar_GetDaysInMonth_m1_4574 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t UmAlQuraCalendar_GetDaysInYear_m1_4575 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetEra(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_GetEra_m1_4576 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t UmAlQuraCalendar_GetLeapMonth_m1_4577 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetMonth(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_GetMonth_m1_4578 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t UmAlQuraCalendar_GetMonthsInYear_m1_4579 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::GetYear(System.DateTime)
extern "C" int32_t UmAlQuraCalendar_GetYear_m1_4580 (UmAlQuraCalendar_t1_392 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.UmAlQuraCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool UmAlQuraCalendar_IsLeapDay_m1_4581 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.UmAlQuraCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool UmAlQuraCalendar_IsLeapMonth_m1_4582 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.UmAlQuraCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool UmAlQuraCalendar_IsLeapYear_m1_4583 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  UmAlQuraCalendar_ToDateTime_m1_4584 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.UmAlQuraCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t UmAlQuraCalendar_ToFourDigitYear_m1_4585 (UmAlQuraCalendar_t1_392 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  UmAlQuraCalendar_get_MinSupportedDateTime_m1_4586 (UmAlQuraCalendar_t1_392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.UmAlQuraCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  UmAlQuraCalendar_get_MaxSupportedDateTime_m1_4587 (UmAlQuraCalendar_t1_392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
