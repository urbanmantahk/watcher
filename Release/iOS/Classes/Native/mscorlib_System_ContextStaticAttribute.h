﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.ContextStaticAttribute
struct  ContextStaticAttribute_t1_1520  : public Attribute_t1_2
{
};
