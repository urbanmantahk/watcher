﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_GCLatencyMode.h"

// System.Boolean System.Runtime.GCSettings::get_IsServerGC()
extern "C" bool GCSettings_get_IsServerGC_m1_9700 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.GCLatencyMode System.Runtime.GCSettings::get_LatencyMode()
extern "C" int32_t GCSettings_get_LatencyMode_m1_9701 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.GCSettings::set_LatencyMode(System.Runtime.GCLatencyMode)
extern "C" void GCSettings_set_LatencyMode_m1_9702 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
