﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.BufferedStream
struct BufferedStream_t1_409;
// System.IO.Stream
struct Stream_t1_405;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_SeekOrigin.h"

// System.Void System.IO.BufferedStream::.ctor(System.IO.Stream)
extern "C" void BufferedStream__ctor_m1_4737 (BufferedStream_t1_409 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::.ctor(System.IO.Stream,System.Int32)
extern "C" void BufferedStream__ctor_m1_4738 (BufferedStream_t1_409 * __this, Stream_t1_405 * ___stream, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.BufferedStream::get_CanRead()
extern "C" bool BufferedStream_get_CanRead_m1_4739 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.BufferedStream::get_CanWrite()
extern "C" bool BufferedStream_get_CanWrite_m1_4740 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.BufferedStream::get_CanSeek()
extern "C" bool BufferedStream_get_CanSeek_m1_4741 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.BufferedStream::get_Length()
extern "C" int64_t BufferedStream_get_Length_m1_4742 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.BufferedStream::get_Position()
extern "C" int64_t BufferedStream_get_Position_m1_4743 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::set_Position(System.Int64)
extern "C" void BufferedStream_set_Position_m1_4744 (BufferedStream_t1_409 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::Dispose(System.Boolean)
extern "C" void BufferedStream_Dispose_m1_4745 (BufferedStream_t1_409 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::Flush()
extern "C" void BufferedStream_Flush_m1_4746 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.BufferedStream::Seek(System.Int64,System.IO.SeekOrigin)
extern "C" int64_t BufferedStream_Seek_m1_4747 (BufferedStream_t1_409 * __this, int64_t ___offset, int32_t ___origin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::SetLength(System.Int64)
extern "C" void BufferedStream_SetLength_m1_4748 (BufferedStream_t1_409 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.BufferedStream::ReadByte()
extern "C" int32_t BufferedStream_ReadByte_m1_4749 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::WriteByte(System.Byte)
extern "C" void BufferedStream_WriteByte_m1_4750 (BufferedStream_t1_409 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.BufferedStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t BufferedStream_Read_m1_4751 (BufferedStream_t1_409 * __this, ByteU5BU5D_t1_109* ___array, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C" void BufferedStream_Write_m1_4752 (BufferedStream_t1_409 * __this, ByteU5BU5D_t1_109* ___array, int32_t ___offset, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.BufferedStream::CheckObjectDisposedException()
extern "C" void BufferedStream_CheckObjectDisposedException_m1_4753 (BufferedStream_t1_409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
