﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.SystemAcl
struct SystemAcl_t1_1133;
// System.Security.AccessControl.RawAcl
struct RawAcl_t1_1170;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Security.AccessControl.SystemAcl::.ctor(System.Boolean,System.Boolean,System.Int32)
extern "C" void SystemAcl__ctor_m1_10089 (SystemAcl_t1_1133 * __this, bool ___isContainer, bool ___isDS, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.RawAcl)
extern "C" void SystemAcl__ctor_m1_10090 (SystemAcl_t1_1133 * __this, bool ___isContainer, bool ___isDS, RawAcl_t1_1170 * ___rawAcl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::.ctor(System.Boolean,System.Boolean,System.Byte,System.Int32)
extern "C" void SystemAcl__ctor_m1_10091 (SystemAcl_t1_1133 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::AddAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void SystemAcl_AddAudit_m1_10092 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::AddAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" void SystemAcl_AddAudit_m1_10093 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.SystemAcl::RemoveAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" bool SystemAcl_RemoveAudit_m1_10094 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.SystemAcl::RemoveAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" bool SystemAcl_RemoveAudit_m1_10095 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::RemoveAuditSpecific(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void SystemAcl_RemoveAuditSpecific_m1_10096 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::RemoveAuditSpecific(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" void SystemAcl_RemoveAuditSpecific_m1_10097 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::SetAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void SystemAcl_SetAudit_m1_10098 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.SystemAcl::SetAudit(System.Security.AccessControl.AuditFlags,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" void SystemAcl_SetAudit_m1_10099 (SystemAcl_t1_1133 * __this, int32_t ___auditFlags, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
