﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CommonSecurityDescriptor
struct CommonSecurityDescriptor_t1_1130;
// System.Security.AccessControl.RawSecurityDescriptor
struct RawSecurityDescriptor_t1_1171;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Security.AccessControl.SystemAcl
struct SystemAcl_t1_1133;
// System.Security.AccessControl.DiscretionaryAcl
struct DiscretionaryAcl_t1_1134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_ControlFlags.h"

// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.RawSecurityDescriptor)
extern "C" void CommonSecurityDescriptor__ctor_m1_9758 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, RawSecurityDescriptor_t1_1171 * ___rawSecurityDescriptor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.String)
extern "C" void CommonSecurityDescriptor__ctor_m1_9759 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, String_t* ___sddlForm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.Byte[],System.Int32)
extern "C" void CommonSecurityDescriptor__ctor_m1_9760 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.ControlFlags,System.Security.Principal.SecurityIdentifier,System.Security.Principal.SecurityIdentifier,System.Security.AccessControl.SystemAcl,System.Security.AccessControl.DiscretionaryAcl)
extern "C" void CommonSecurityDescriptor__ctor_m1_9761 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isContainer, bool ___isDS, int32_t ___flags, SecurityIdentifier_t1_1132 * ___owner, SecurityIdentifier_t1_1132 * ___group, SystemAcl_t1_1133 * ___systemAcl, DiscretionaryAcl_t1_1134 * ___discretionaryAcl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.ControlFlags System.Security.AccessControl.CommonSecurityDescriptor::get_ControlFlags()
extern "C" int32_t CommonSecurityDescriptor_get_ControlFlags_m1_9762 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.DiscretionaryAcl System.Security.AccessControl.CommonSecurityDescriptor::get_DiscretionaryAcl()
extern "C" DiscretionaryAcl_t1_1134 * CommonSecurityDescriptor_get_DiscretionaryAcl_m1_9763 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_DiscretionaryAcl(System.Security.AccessControl.DiscretionaryAcl)
extern "C" void CommonSecurityDescriptor_set_DiscretionaryAcl_m1_9764 (CommonSecurityDescriptor_t1_1130 * __this, DiscretionaryAcl_t1_1134 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.CommonSecurityDescriptor::get_Group()
extern "C" SecurityIdentifier_t1_1132 * CommonSecurityDescriptor_get_Group_m1_9765 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_Group(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonSecurityDescriptor_set_Group_m1_9766 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsContainer()
extern "C" bool CommonSecurityDescriptor_get_IsContainer_m1_9767 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsDiscretionaryAclCanonical()
extern "C" bool CommonSecurityDescriptor_get_IsDiscretionaryAclCanonical_m1_9768 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsDS()
extern "C" bool CommonSecurityDescriptor_get_IsDS_m1_9769 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::get_IsSystemAclCanonical()
extern "C" bool CommonSecurityDescriptor_get_IsSystemAclCanonical_m1_9770 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.CommonSecurityDescriptor::get_Owner()
extern "C" SecurityIdentifier_t1_1132 * CommonSecurityDescriptor_get_Owner_m1_9771 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_Owner(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonSecurityDescriptor_set_Owner_m1_9772 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.SystemAcl System.Security.AccessControl.CommonSecurityDescriptor::get_SystemAcl()
extern "C" SystemAcl_t1_1133 * CommonSecurityDescriptor_get_SystemAcl_m1_9773 (CommonSecurityDescriptor_t1_1130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::set_SystemAcl(System.Security.AccessControl.SystemAcl)
extern "C" void CommonSecurityDescriptor_set_SystemAcl_m1_9774 (CommonSecurityDescriptor_t1_1130 * __this, SystemAcl_t1_1133 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::PurgeAccessControl(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonSecurityDescriptor_PurgeAccessControl_m1_9775 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::PurgeAudit(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonSecurityDescriptor_PurgeAudit_m1_9776 (CommonSecurityDescriptor_t1_1130 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::SetDiscretionaryAclProtection(System.Boolean,System.Boolean)
extern "C" void CommonSecurityDescriptor_SetDiscretionaryAclProtection_m1_9777 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonSecurityDescriptor::SetSystemAclProtection(System.Boolean,System.Boolean)
extern "C" void CommonSecurityDescriptor_SetSystemAclProtection_m1_9778 (CommonSecurityDescriptor_t1_1130 * __this, bool ___isProtected, bool ___preserveInheritance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
