﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t7_64;
// UnityEngine.UI.Text
struct Text_t7_63;
// UnityEngine.GameObject
struct GameObject_t6_97;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "mscorlib_System_DateTime.h"

// AlertControl
struct  AlertControl_t8_9  : public MonoBehaviour_t6_91
{
	// System.DateTime AlertControl::amTimerStart
	DateTime_t1_150  ___amTimerStart_2;
	// System.DateTime AlertControl::amTimerEnd
	DateTime_t1_150  ___amTimerEnd_3;
	// System.DateTime AlertControl::pmTimerStart
	DateTime_t1_150  ___pmTimerStart_4;
	// System.DateTime AlertControl::pmTimerEnd
	DateTime_t1_150  ___pmTimerEnd_5;
	// System.DateTime AlertControl::lastCheckTime
	DateTime_t1_150  ___lastCheckTime_6;
	// System.DateTime AlertControl::nextCheckTime
	DateTime_t1_150  ___nextCheckTime_7;
	// System.DateTime AlertControl::nextEndTime
	DateTime_t1_150  ___nextEndTime_8;
	// UnityEngine.UI.Image AlertControl::timeFiller
	Image_t7_64 * ___timeFiller_9;
	// UnityEngine.UI.Text AlertControl::lastCheck
	Text_t7_63 * ___lastCheck_10;
	// UnityEngine.UI.Text AlertControl::lastCheckDate
	Text_t7_63 * ___lastCheckDate_11;
	// UnityEngine.UI.Text AlertControl::nextAlarmTime
	Text_t7_63 * ___nextAlarmTime_12;
	// UnityEngine.UI.Text AlertControl::nextAlarmDate
	Text_t7_63 * ___nextAlarmDate_13;
	// UnityEngine.UI.Text AlertControl::currentTime
	Text_t7_63 * ___currentTime_14;
	// UnityEngine.UI.Text AlertControl::currentDate
	Text_t7_63 * ___currentDate_15;
	// UnityEngine.GameObject AlertControl::button_checkin
	GameObject_t6_97 * ___button_checkin_16;
	// System.String AlertControl::url_dev
	String_t* ___url_dev_17;
};
