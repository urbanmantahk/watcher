﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComSourceInterfacesAttribute
struct ComSourceInterfacesAttribute_t1_775;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.String)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7613 (ComSourceInterfacesAttribute_t1_775 * __this, String_t* ___sourceInterfaces, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7614 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type,System.Type)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7615 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface1, Type_t * ___sourceInterface2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type,System.Type,System.Type)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7616 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface1, Type_t * ___sourceInterface2, Type_t * ___sourceInterface3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ComSourceInterfacesAttribute::.ctor(System.Type,System.Type,System.Type,System.Type)
extern "C" void ComSourceInterfacesAttribute__ctor_m1_7617 (ComSourceInterfacesAttribute_t1_775 * __this, Type_t * ___sourceInterface1, Type_t * ___sourceInterface2, Type_t * ___sourceInterface3, Type_t * ___sourceInterface4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.ComSourceInterfacesAttribute::get_Value()
extern "C" String_t* ComSourceInterfacesAttribute_get_Value_m1_7618 (ComSourceInterfacesAttribute_t1_775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
