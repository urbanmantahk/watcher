﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.FileDialogPermission
struct FileDialogPermission_t1_1271;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_FileDialogPermissionAcc.h"

// System.Void System.Security.Permissions.FileDialogPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void FileDialogPermission__ctor_m1_10803 (FileDialogPermission_t1_1271 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileDialogPermission::.ctor(System.Security.Permissions.FileDialogPermissionAccess)
extern "C" void FileDialogPermission__ctor_m1_10804 (FileDialogPermission_t1_1271 * __this, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.FileDialogPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t FileDialogPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_10805 (FileDialogPermission_t1_1271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileDialogPermissionAccess System.Security.Permissions.FileDialogPermission::get_Access()
extern "C" int32_t FileDialogPermission_get_Access_m1_10806 (FileDialogPermission_t1_1271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileDialogPermission::set_Access(System.Security.Permissions.FileDialogPermissionAccess)
extern "C" void FileDialogPermission_set_Access_m1_10807 (FileDialogPermission_t1_1271 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileDialogPermission::Copy()
extern "C" Object_t * FileDialogPermission_Copy_m1_10808 (FileDialogPermission_t1_1271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileDialogPermission::FromXml(System.Security.SecurityElement)
extern "C" void FileDialogPermission_FromXml_m1_10809 (FileDialogPermission_t1_1271 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileDialogPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * FileDialogPermission_Intersect_m1_10810 (FileDialogPermission_t1_1271 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileDialogPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool FileDialogPermission_IsSubsetOf_m1_10811 (FileDialogPermission_t1_1271 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileDialogPermission::IsUnrestricted()
extern "C" bool FileDialogPermission_IsUnrestricted_m1_10812 (FileDialogPermission_t1_1271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.FileDialogPermission::ToXml()
extern "C" SecurityElement_t1_242 * FileDialogPermission_ToXml_m1_10813 (FileDialogPermission_t1_1271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileDialogPermission::Union(System.Security.IPermission)
extern "C" Object_t * FileDialogPermission_Union_m1_10814 (FileDialogPermission_t1_1271 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileDialogPermission System.Security.Permissions.FileDialogPermission::Cast(System.Security.IPermission)
extern "C" FileDialogPermission_t1_1271 * FileDialogPermission_Cast_m1_10815 (FileDialogPermission_t1_1271 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
