﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IDictionary[]
struct IDictionaryU5BU5D_t1_861;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.AggregateEnumerator
struct  AggregateEnumerator_t1_862  : public Object_t
{
	// System.Collections.IDictionary[] System.Runtime.Remoting.Channels.AggregateEnumerator::dictionaries
	IDictionaryU5BU5D_t1_861* ___dictionaries_0;
	// System.Int32 System.Runtime.Remoting.Channels.AggregateEnumerator::pos
	int32_t ___pos_1;
	// System.Collections.IDictionaryEnumerator System.Runtime.Remoting.Channels.AggregateEnumerator::currente
	Object_t * ___currente_2;
};
