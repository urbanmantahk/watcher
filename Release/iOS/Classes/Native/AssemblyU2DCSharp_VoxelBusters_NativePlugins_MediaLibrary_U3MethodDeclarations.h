﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB
struct U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB::.ctor()
extern "C" void U3CPickImageFinishedU3Ec__AnonStoreyB__ctor_m8_1363 (U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/<PickImageFinished>c__AnonStoreyB::<>m__C(UnityEngine.Texture2D,System.String)
extern "C" void U3CPickImageFinishedU3Ec__AnonStoreyB_U3CU3Em__C_m8_1364 (U3CPickImageFinishedU3Ec__AnonStoreyB_t8_244 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
