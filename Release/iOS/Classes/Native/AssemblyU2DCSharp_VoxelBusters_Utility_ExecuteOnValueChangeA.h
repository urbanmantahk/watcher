﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_PropertyAttribute.h"

// VoxelBusters.Utility.ExecuteOnValueChangeAttribute
struct  ExecuteOnValueChangeAttribute_t8_148  : public PropertyAttribute_t6_236
{
	// System.String VoxelBusters.Utility.ExecuteOnValueChangeAttribute::<InvokeMethod>k__BackingField
	String_t* ___U3CInvokeMethodU3Ek__BackingField_0;
};
