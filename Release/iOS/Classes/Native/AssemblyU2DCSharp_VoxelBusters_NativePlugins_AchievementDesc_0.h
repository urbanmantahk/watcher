﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje.h"

// VoxelBusters.NativePlugins.AchievementDescription
struct  AchievementDescription_t8_225  : public NPObject_t8_222
{
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.AchievementDescription::m_image
	Texture2D_t6_33 * ___m_image_1;
	// VoxelBusters.Utility.DownloadTexture/Completion VoxelBusters.NativePlugins.AchievementDescription::DownloadImageFinishedEvent
	Completion_t8_161 * ___DownloadImageFinishedEvent_2;
	// System.String VoxelBusters.NativePlugins.AchievementDescription::<GlobalIdentifier>k__BackingField
	String_t* ___U3CGlobalIdentifierU3Ek__BackingField_3;
	// System.String VoxelBusters.NativePlugins.AchievementDescription::<InstanceID>k__BackingField
	String_t* ___U3CInstanceIDU3Ek__BackingField_4;
};
