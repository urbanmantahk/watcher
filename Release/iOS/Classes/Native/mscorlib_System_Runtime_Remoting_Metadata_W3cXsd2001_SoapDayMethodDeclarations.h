﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay
struct SoapDay_t1_969;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::.ctor()
extern "C" void SoapDay__ctor_m1_8699 (SoapDay_t1_969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::.ctor(System.DateTime)
extern "C" void SoapDay__ctor_m1_8700 (SoapDay_t1_969 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::.cctor()
extern "C" void SoapDay__cctor_m1_8701 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::get_Value()
extern "C" DateTime_t1_150  SoapDay_get_Value_m1_8702 (SoapDay_t1_969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::set_Value(System.DateTime)
extern "C" void SoapDay_set_Value_m1_8703 (SoapDay_t1_969 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::get_XsdType()
extern "C" String_t* SoapDay_get_XsdType_m1_8704 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::GetXsdType()
extern "C" String_t* SoapDay_GetXsdType_m1_8705 (SoapDay_t1_969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::Parse(System.String)
extern "C" SoapDay_t1_969 * SoapDay_Parse_m1_8706 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDay::ToString()
extern "C" String_t* SoapDay_ToString_m1_8707 (SoapDay_t1_969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
