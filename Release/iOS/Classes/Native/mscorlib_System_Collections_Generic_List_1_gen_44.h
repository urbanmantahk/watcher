﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.ShaderUtility/ShaderInfo[]
struct ShaderInfoU5BU5D_t8_376;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>
struct  List_1_t1_1901  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	ShaderInfoU5BU5D_t8_376* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1901_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	ShaderInfoU5BU5D_t8_376* ___EmptyArray_4;
};
