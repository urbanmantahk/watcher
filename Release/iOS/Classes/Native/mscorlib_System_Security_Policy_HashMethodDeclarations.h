﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.Hash
struct Hash_t1_1350;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.Policy.Hash::.ctor(System.Reflection.Assembly)
extern "C" void Hash__ctor_m1_11516 (Hash_t1_1350 * __this, Assembly_t1_467 * ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::.ctor()
extern "C" void Hash__ctor_m1_11517 (Hash_t1_1350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Hash__ctor_m1_11518 (Hash_t1_1350 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Hash::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t Hash_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11519 (Hash_t1_1350 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Hash::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t Hash_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11520 (Hash_t1_1350 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Hash::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t Hash_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11521 (Hash_t1_1350 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.Hash::get_MD5()
extern "C" ByteU5BU5D_t1_109* Hash_get_MD5_m1_11522 (Hash_t1_1350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.Hash::get_SHA1()
extern "C" ByteU5BU5D_t1_109* Hash_get_SHA1_m1_11523 (Hash_t1_1350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.Hash::GenerateHash(System.Security.Cryptography.HashAlgorithm)
extern "C" ByteU5BU5D_t1_109* Hash_GenerateHash_m1_11524 (Hash_t1_1350 * __this, HashAlgorithm_t1_162 * ___hashAlg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Hash_GetObjectData_m1_11525 (Hash_t1_1350 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Hash::ToString()
extern "C" String_t* Hash_ToString_m1_11526 (Hash_t1_1350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.Hash::GetData()
extern "C" ByteU5BU5D_t1_109* Hash_GetData_m1_11527 (Hash_t1_1350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Hash System.Security.Policy.Hash::CreateMD5(System.Byte[])
extern "C" Hash_t1_1350 * Hash_CreateMD5_m1_11528 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___md5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Hash System.Security.Policy.Hash::CreateSHA1(System.Byte[])
extern "C" Hash_t1_1350 * Hash_CreateSHA1_m1_11529 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___sha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
