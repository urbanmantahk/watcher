﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_InteropServices_CriticalHandle.h"

// Microsoft.Win32.SafeHandles.CriticalHandleMinusOneIsInvalid
struct  CriticalHandleMinusOneIsInvalid_t1_82  : public CriticalHandle_t1_83
{
};
