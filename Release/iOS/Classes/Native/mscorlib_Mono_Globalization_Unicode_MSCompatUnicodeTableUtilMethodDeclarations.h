﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
struct MSCompatUnicodeTableUtil_t1_115;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Globalization.Unicode.MSCompatUnicodeTableUtil::.ctor()
extern "C" void MSCompatUnicodeTableUtil__ctor_m1_1605 (MSCompatUnicodeTableUtil_t1_115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.MSCompatUnicodeTableUtil::.cctor()
extern "C" void MSCompatUnicodeTableUtil__cctor_m1_1606 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
