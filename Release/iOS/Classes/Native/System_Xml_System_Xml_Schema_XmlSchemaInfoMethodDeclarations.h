﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t4_62;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t4_119;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t4_63;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4_55;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t4_58;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4_64;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaValidity.h"

// System.Void System.Xml.Schema.XmlSchemaInfo::.ctor(System.Xml.Schema.IXmlSchemaInfo)
extern "C" void XmlSchemaInfo__ctor_m4_85 (XmlSchemaInfo_t4_62 * __this, Object_t * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlSchemaInfo::get_IsDefault()
extern "C" bool XmlSchemaInfo_get_IsDefault_m4_86 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.Schema.XmlSchemaInfo::get_IsNil()
extern "C" bool XmlSchemaInfo_get_IsNil_m4_87 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::get_MemberType()
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaInfo_get_MemberType_m4_88 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::get_SchemaAttribute()
extern "C" XmlSchemaAttribute_t4_55 * XmlSchemaInfo_get_SchemaAttribute_m4_89 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::get_SchemaElement()
extern "C" XmlSchemaElement_t4_58 * XmlSchemaInfo_get_SchemaElement_m4_90 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::get_SchemaType()
extern "C" XmlSchemaType_t4_64 * XmlSchemaInfo_get_SchemaType_m4_91 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::get_Validity()
extern "C" int32_t XmlSchemaInfo_get_Validity_m4_92 (XmlSchemaInfo_t4_62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
