﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>
struct  U3CGetEnumeratorU3Ec__Iterator1_t3_288  : public Object_t
{
	// System.Int32 System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1::<current>__1
	KeyValuePair_2_t1_2681  ___U3CcurrentU3E__1_1;
	// System.Int32 System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1::$PC
	int32_t ___U24PC_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1::$current
	KeyValuePair_2_t1_2681  ___U24current_3;
	// System.Collections.Generic.SortedList`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1::<>f__this
	SortedList_2_t3_248 * ___U3CU3Ef__this_4;
};
