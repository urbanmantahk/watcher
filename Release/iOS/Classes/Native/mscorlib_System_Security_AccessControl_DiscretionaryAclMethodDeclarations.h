﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.DiscretionaryAcl
struct DiscretionaryAcl_t1_1134;
// System.Security.AccessControl.RawAcl
struct RawAcl_t1_1170;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Security.AccessControl.DiscretionaryAcl::.ctor(System.Boolean,System.Boolean,System.Int32)
extern "C" void DiscretionaryAcl__ctor_m1_9835 (DiscretionaryAcl_t1_1134 * __this, bool ___isContainer, bool ___isDS, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::.ctor(System.Boolean,System.Boolean,System.Security.AccessControl.RawAcl)
extern "C" void DiscretionaryAcl__ctor_m1_9836 (DiscretionaryAcl_t1_1134 * __this, bool ___isContainer, bool ___isDS, RawAcl_t1_1170 * ___rawAcl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::.ctor(System.Boolean,System.Boolean,System.Byte,System.Int32)
extern "C" void DiscretionaryAcl__ctor_m1_9837 (DiscretionaryAcl_t1_1134 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::AddAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void DiscretionaryAcl_AddAccess_m1_9838 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::AddAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" void DiscretionaryAcl_AddAccess_m1_9839 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.DiscretionaryAcl::RemoveAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" bool DiscretionaryAcl_RemoveAccess_m1_9840 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.DiscretionaryAcl::RemoveAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" bool DiscretionaryAcl_RemoveAccess_m1_9841 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::RemoveAccessSpecific(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void DiscretionaryAcl_RemoveAccessSpecific_m1_9842 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::RemoveAccessSpecific(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" void DiscretionaryAcl_RemoveAccessSpecific_m1_9843 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::SetAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void DiscretionaryAcl_SetAccess_m1_9844 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.DiscretionaryAcl::SetAccess(System.Security.AccessControl.AccessControlType,System.Security.Principal.SecurityIdentifier,System.Int32,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.ObjectAceFlags,System.Guid,System.Guid)
extern "C" void DiscretionaryAcl_SetAccess_m1_9845 (DiscretionaryAcl_t1_1134 * __this, int32_t ___accessType, SecurityIdentifier_t1_1132 * ___sid, int32_t ___accessMask, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___objectFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
