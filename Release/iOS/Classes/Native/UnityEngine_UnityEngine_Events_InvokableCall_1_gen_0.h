﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t6_311;

#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t6_333  : public BaseInvokableCall_t6_257
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1::Delegate
	UnityAction_1_t6_311 * ___Delegate_0;
};
