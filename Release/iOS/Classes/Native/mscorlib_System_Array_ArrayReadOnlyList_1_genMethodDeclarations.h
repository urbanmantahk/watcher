﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1_1966;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m1_15496_gshared (ArrayReadOnlyList_1_t1_1966 * __this, ObjectU5BU5D_t1_272* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m1_15496(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, ObjectU5BU5D_t1_272*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m1_15496_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_15497_gshared (ArrayReadOnlyList_1_t1_1966 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_15497(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_1966 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_15497_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m1_15498_gshared (ArrayReadOnlyList_1_t1_1966 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m1_15498(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_1966 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m1_15498_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m1_15499_gshared (ArrayReadOnlyList_1_t1_1966 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m1_15499(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m1_15499_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m1_15500_gshared (ArrayReadOnlyList_1_t1_1966 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m1_15500(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_1966 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m1_15500_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m1_15501_gshared (ArrayReadOnlyList_1_t1_1966 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m1_15501(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1_1966 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m1_15501_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m1_15502_gshared (ArrayReadOnlyList_1_t1_1966 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m1_15502(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m1_15502_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m1_15503_gshared (ArrayReadOnlyList_1_t1_1966 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m1_15503(__this, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m1_15503_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m1_15504_gshared (ArrayReadOnlyList_1_t1_1966 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m1_15504(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_1966 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m1_15504_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m1_15505_gshared (ArrayReadOnlyList_1_t1_1966 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m1_15505(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m1_15505_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m1_15506_gshared (ArrayReadOnlyList_1_t1_1966 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m1_15506(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1_1966 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m1_15506_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m1_15507_gshared (ArrayReadOnlyList_1_t1_1966 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m1_15507(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_1966 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m1_15507_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m1_15508_gshared (ArrayReadOnlyList_1_t1_1966 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m1_15508(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m1_15508_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m1_15509_gshared (ArrayReadOnlyList_1_t1_1966 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m1_15509(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_1966 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m1_15509_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m1_15510_gshared (ArrayReadOnlyList_1_t1_1966 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m1_15510(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_1966 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m1_15510_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t1_33 * ArrayReadOnlyList_1_ReadOnlyError_m1_15511_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m1_15511(__this /* static, unused */, method) (( Exception_t1_33 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m1_15511_gshared)(__this /* static, unused */, method)
