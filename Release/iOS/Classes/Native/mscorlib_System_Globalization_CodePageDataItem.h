﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Globalization.CodePageDataItem
struct  CodePageDataItem_t1_359  : public Object_t
{
	// System.String System.Globalization.CodePageDataItem::m_bodyName
	String_t* ___m_bodyName_0;
	// System.Int32 System.Globalization.CodePageDataItem::m_codePage
	int32_t ___m_codePage_1;
	// System.Int32 System.Globalization.CodePageDataItem::m_dataIndex
	int32_t ___m_dataIndex_2;
	// System.String System.Globalization.CodePageDataItem::m_description
	String_t* ___m_description_3;
	// System.UInt32 System.Globalization.CodePageDataItem::m_flags
	uint32_t ___m_flags_4;
	// System.String System.Globalization.CodePageDataItem::m_headerName
	String_t* ___m_headerName_5;
	// System.Int32 System.Globalization.CodePageDataItem::m_uiFamilyCodePage
	int32_t ___m_uiFamilyCodePage_6;
	// System.String System.Globalization.CodePageDataItem::m_webName
	String_t* ___m_webName_7;
};
