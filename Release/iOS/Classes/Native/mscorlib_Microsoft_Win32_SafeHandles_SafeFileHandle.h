﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOn.h"

// Microsoft.Win32.SafeHandles.SafeFileHandle
struct  SafeFileHandle_t1_85  : public SafeHandleZeroOrMinusOneIsInvalid_t1_86
{
};
