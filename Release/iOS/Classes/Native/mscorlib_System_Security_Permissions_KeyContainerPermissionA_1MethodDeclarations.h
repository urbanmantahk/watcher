﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator
struct KeyContainerPermissionAccessEntryEnumerator_t1_1291;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object
struct Object_t;
// System.Security.Permissions.KeyContainerPermissionAccessEntry
struct KeyContainerPermissionAccessEntry_t1_1290;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator::.ctor(System.Collections.ArrayList)
extern "C" void KeyContainerPermissionAccessEntryEnumerator__ctor_m1_10998 (KeyContainerPermissionAccessEntryEnumerator_t1_1291 * __this, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * KeyContainerPermissionAccessEntryEnumerator_System_Collections_IEnumerator_get_Current_m1_10999 (KeyContainerPermissionAccessEntryEnumerator_t1_1291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionAccessEntry System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator::get_Current()
extern "C" KeyContainerPermissionAccessEntry_t1_1290 * KeyContainerPermissionAccessEntryEnumerator_get_Current_m1_11000 (KeyContainerPermissionAccessEntryEnumerator_t1_1291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator::MoveNext()
extern "C" bool KeyContainerPermissionAccessEntryEnumerator_MoveNext_m1_11001 (KeyContainerPermissionAccessEntryEnumerator_t1_1291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermissionAccessEntryEnumerator::Reset()
extern "C" void KeyContainerPermissionAccessEntryEnumerator_Reset_m1_11002 (KeyContainerPermissionAccessEntryEnumerator_t1_1291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
