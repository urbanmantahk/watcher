﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"

// System.Security.Permissions.FileIOPermissionAttribute
struct  FileIOPermissionAttribute_t1_1276  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.FileIOPermissionAttribute::append
	String_t* ___append_2;
	// System.String System.Security.Permissions.FileIOPermissionAttribute::path
	String_t* ___path_3;
	// System.String System.Security.Permissions.FileIOPermissionAttribute::read
	String_t* ___read_4;
	// System.String System.Security.Permissions.FileIOPermissionAttribute::write
	String_t* ___write_5;
	// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermissionAttribute::allFiles
	int32_t ___allFiles_6;
	// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermissionAttribute::allLocalFiles
	int32_t ___allLocalFiles_7;
	// System.String System.Security.Permissions.FileIOPermissionAttribute::changeAccessControl
	String_t* ___changeAccessControl_8;
	// System.String System.Security.Permissions.FileIOPermissionAttribute::viewAccessControl
	String_t* ___viewAccessControl_9;
};
