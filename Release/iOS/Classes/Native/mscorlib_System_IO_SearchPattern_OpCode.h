﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_IO_SearchPattern_OpCode.h"

// System.IO.SearchPattern/OpCode
struct  OpCode_t1_441 
{
	// System.Int32 System.IO.SearchPattern/OpCode::value__
	int32_t ___value___1;
};
