﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.ExceptionHandlingClause[]
struct ExceptionHandlingClauseU5BU5D_t1_611;
// System.Reflection.LocalVariableInfo[]
struct LocalVariableInfoU5BU5D_t1_612;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.Reflection.MethodBody
struct  MethodBody_t1_610  : public Object_t
{
	// System.Reflection.ExceptionHandlingClause[] System.Reflection.MethodBody::clauses
	ExceptionHandlingClauseU5BU5D_t1_611* ___clauses_0;
	// System.Reflection.LocalVariableInfo[] System.Reflection.MethodBody::locals
	LocalVariableInfoU5BU5D_t1_612* ___locals_1;
	// System.Byte[] System.Reflection.MethodBody::il
	ByteU5BU5D_t1_109* ___il_2;
	// System.Boolean System.Reflection.MethodBody::init_locals
	bool ___init_locals_3;
	// System.Int32 System.Reflection.MethodBody::sig_token
	int32_t ___sig_token_4;
	// System.Int32 System.Reflection.MethodBody::max_stack
	int32_t ___max_stack_5;
};
