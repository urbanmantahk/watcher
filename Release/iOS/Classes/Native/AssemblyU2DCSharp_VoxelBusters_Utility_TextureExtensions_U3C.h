﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t1_1902;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3
struct  U3CTakeScreenshotU3Ec__Iterator3_t8_43  : public Object_t
{
	// UnityEngine.Texture2D VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::<_texture>__0
	Texture2D_t6_33 * ___U3C_textureU3E__0_0;
	// System.Action`1<UnityEngine.Texture2D> VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::_onCompletionHandler
	Action_1_t1_1902 * ____onCompletionHandler_1;
	// System.Int32 VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// System.Object VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::$current
	Object_t * ___U24current_3;
	// System.Action`1<UnityEngine.Texture2D> VoxelBusters.Utility.TextureExtensions/<TakeScreenshot>c__Iterator3::<$>_onCompletionHandler
	Action_1_t1_1902 * ___U3CU24U3E_onCompletionHandler_4;
};
