﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Guid.h"

// System.Security.AccessControl.ObjectAuditRule
struct  ObjectAuditRule_t1_1166  : public AuditRule_t1_1119
{
	// System.Guid System.Security.AccessControl.ObjectAuditRule::inherited_object_type
	Guid_t1_319  ___inherited_object_type_6;
	// System.Guid System.Security.AccessControl.ObjectAuditRule::object_type
	Guid_t1_319  ___object_type_7;
};
