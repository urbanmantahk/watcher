﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t4_28;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdUnsignedLong::.ctor()
extern "C" void XsdUnsignedLong__ctor_m4_43 (XsdUnsignedLong_t4_28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
