﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32GroupIconResource
struct Win32GroupIconResource_t1_668;
// System.Resources.Win32IconResource[]
struct Win32IconResourceU5BU5D_t1_669;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.Win32GroupIconResource::.ctor(System.Int32,System.Int32,System.Resources.Win32IconResource[])
extern "C" void Win32GroupIconResource__ctor_m1_7483 (Win32GroupIconResource_t1_668 * __this, int32_t ___id, int32_t ___language, Win32IconResourceU5BU5D_t1_669* ___icons, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32GroupIconResource::WriteTo(System.IO.Stream)
extern "C" void Win32GroupIconResource_WriteTo_m1_7484 (Win32GroupIconResource_t1_668 * __this, Stream_t1_405 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
