﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_EventArgs.h"
#include "mscorlib_System_ConsoleSpecialKey.h"

// System.ConsoleCancelEventArgs
struct  ConsoleCancelEventArgs_t1_1513  : public EventArgs_t1_158
{
	// System.Boolean System.ConsoleCancelEventArgs::cancel
	bool ___cancel_1;
	// System.ConsoleSpecialKey System.ConsoleCancelEventArgs::specialKey
	int32_t ___specialKey_2;
};
