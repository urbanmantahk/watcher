﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t1_2373;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t1_1843;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_289;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t1_2822;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m1_20744_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_20744(__this, method) (( void (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1__ctor_m1_20744_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_20745_gshared (Collection_1_t1_2373 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_20745(__this, ___list, method) (( void (*) (Collection_1_t1_2373 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_20745_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20746_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20746(__this, method) (( bool (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20746_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_20747_gshared (Collection_1_t1_2373 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_20747(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2373 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_20747_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20748_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20748(__this, method) (( Object_t * (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_20748_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_20749_gshared (Collection_1_t1_2373 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_20749(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2373 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_20749_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_20750_gshared (Collection_1_t1_2373 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_20750(__this, ___value, method) (( bool (*) (Collection_1_t1_2373 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_20750_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_20751_gshared (Collection_1_t1_2373 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_20751(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2373 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_20751_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_20752_gshared (Collection_1_t1_2373 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_20752(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2373 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_20752_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_20753_gshared (Collection_1_t1_2373 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_20753(__this, ___value, method) (( void (*) (Collection_1_t1_2373 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_20753_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20754_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20754(__this, method) (( bool (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_20754_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20755_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20755(__this, method) (( Object_t * (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_20755_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_20756_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_20756(__this, method) (( bool (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_20756_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_20757_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_20757(__this, method) (( bool (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_20757_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_20758_gshared (Collection_1_t1_2373 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_20758(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2373 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_20758_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_20759_gshared (Collection_1_t1_2373 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_20759(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2373 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_20759_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m1_20760_gshared (Collection_1_t1_2373 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_20760(__this, ___item, method) (( void (*) (Collection_1_t1_2373 *, UILineInfo_t6_151 , const MethodInfo*))Collection_1_Add_m1_20760_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m1_20761_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_20761(__this, method) (( void (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_Clear_m1_20761_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_20762_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_20762(__this, method) (( void (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_ClearItems_m1_20762_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m1_20763_gshared (Collection_1_t1_2373 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_20763(__this, ___item, method) (( bool (*) (Collection_1_t1_2373 *, UILineInfo_t6_151 , const MethodInfo*))Collection_1_Contains_m1_20763_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_20764_gshared (Collection_1_t1_2373 * __this, UILineInfoU5BU5D_t6_289* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_20764(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2373 *, UILineInfoU5BU5D_t6_289*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_20764_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_20765_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_20765(__this, method) (( Object_t* (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_GetEnumerator_m1_20765_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_20766_gshared (Collection_1_t1_2373 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_20766(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2373 *, UILineInfo_t6_151 , const MethodInfo*))Collection_1_IndexOf_m1_20766_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_20767_gshared (Collection_1_t1_2373 * __this, int32_t ___index, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_20767(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2373 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))Collection_1_Insert_m1_20767_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_20768_gshared (Collection_1_t1_2373 * __this, int32_t ___index, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_20768(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2373 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))Collection_1_InsertItem_m1_20768_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_20769_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_20769(__this, method) (( Object_t* (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_get_Items_m1_20769_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m1_20770_gshared (Collection_1_t1_2373 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_20770(__this, ___item, method) (( bool (*) (Collection_1_t1_2373 *, UILineInfo_t6_151 , const MethodInfo*))Collection_1_Remove_m1_20770_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_20771_gshared (Collection_1_t1_2373 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_20771(__this, ___index, method) (( void (*) (Collection_1_t1_2373 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_20771_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_20772_gshared (Collection_1_t1_2373 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_20772(__this, ___index, method) (( void (*) (Collection_1_t1_2373 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_20772_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_20773_gshared (Collection_1_t1_2373 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_20773(__this, method) (( int32_t (*) (Collection_1_t1_2373 *, const MethodInfo*))Collection_1_get_Count_m1_20773_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t6_151  Collection_1_get_Item_m1_20774_gshared (Collection_1_t1_2373 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_20774(__this, ___index, method) (( UILineInfo_t6_151  (*) (Collection_1_t1_2373 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_20774_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_20775_gshared (Collection_1_t1_2373 * __this, int32_t ___index, UILineInfo_t6_151  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_20775(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2373 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))Collection_1_set_Item_m1_20775_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_20776_gshared (Collection_1_t1_2373 * __this, int32_t ___index, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_20776(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2373 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))Collection_1_SetItem_m1_20776_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_20777_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_20777(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_20777_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t6_151  Collection_1_ConvertItem_m1_20778_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_20778(__this /* static, unused */, ___item, method) (( UILineInfo_t6_151  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_20778_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_20779_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_20779(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_20779_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_20780_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_20780(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_20780_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_20781_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_20781(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_20781_gshared)(__this /* static, unused */, ___list, method)
