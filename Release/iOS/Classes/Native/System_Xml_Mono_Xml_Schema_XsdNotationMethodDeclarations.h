﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t4_20;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdNotation::.ctor()
extern "C" void XsdNotation__ctor_m4_33 (XsdNotation_t4_20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNotation::get_TokenizedType()
extern "C" int32_t XsdNotation_get_TokenizedType_m4_34 (XsdNotation_t4_20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
