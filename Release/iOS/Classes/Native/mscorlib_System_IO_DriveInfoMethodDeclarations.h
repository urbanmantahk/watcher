﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.DriveInfo
struct DriveInfo_t1_415;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.IO.DirectoryInfo
struct DirectoryInfo_t1_399;
// System.IO.StreamReader
struct StreamReader_t1_447;
// System.IO.DriveInfo[]
struct DriveInfoU5BU5D_t1_1682;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_DriveInfo__DriveType.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_IO_DriveType.h"
#include "mscorlib_System_IO_MonoIOError.h"

// System.Void System.IO.DriveInfo::.ctor(System.IO.DriveInfo/_DriveType,System.String,System.String)
extern "C" void DriveInfo__ctor_m1_4818 (DriveInfo_t1_415 * __this, int32_t ____drive_type, String_t* ___path, String_t* ___fstype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveInfo::.ctor(System.String)
extern "C" void DriveInfo__ctor_m1_4819 (DriveInfo_t1_415 * __this, String_t* ___driveName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveInfo::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DriveInfo_System_Runtime_Serialization_ISerializable_GetObjectData_m1_4820 (DriveInfo_t1_415 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveInfo::GetDiskFreeSpace(System.String,System.UInt64&,System.UInt64&,System.UInt64&)
extern "C" void DriveInfo_GetDiskFreeSpace_m1_4821 (Object_t * __this /* static, unused */, String_t* ___path, uint64_t* ___availableFreeSpace, uint64_t* ___totalSize, uint64_t* ___totalFreeSpace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.DriveInfo::get_AvailableFreeSpace()
extern "C" int64_t DriveInfo_get_AvailableFreeSpace_m1_4822 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.DriveInfo::get_TotalFreeSpace()
extern "C" int64_t DriveInfo_get_TotalFreeSpace_m1_4823 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IO.DriveInfo::get_TotalSize()
extern "C" int64_t DriveInfo_get_TotalSize_m1_4824 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DriveInfo::get_VolumeLabel()
extern "C" String_t* DriveInfo_get_VolumeLabel_m1_4825 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.DriveInfo::set_VolumeLabel(System.String)
extern "C" void DriveInfo_set_VolumeLabel_m1_4826 (DriveInfo_t1_415 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DriveInfo::get_DriveFormat()
extern "C" String_t* DriveInfo_get_DriveFormat_m1_4827 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DriveType System.IO.DriveInfo::get_DriveType()
extern "C" int32_t DriveInfo_get_DriveType_m1_4828 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DriveInfo::get_Name()
extern "C" String_t* DriveInfo_get_Name_m1_4829 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DirectoryInfo System.IO.DriveInfo::get_RootDirectory()
extern "C" DirectoryInfo_t1_399 * DriveInfo_get_RootDirectory_m1_4830 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.DriveInfo::get_IsReady()
extern "C" bool DriveInfo_get_IsReady_m1_4831 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamReader System.IO.DriveInfo::TryOpen(System.String)
extern "C" StreamReader_t1_447 * DriveInfo_TryOpen_m1_4832 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DriveInfo[] System.IO.DriveInfo::LinuxGetDrives()
extern "C" DriveInfoU5BU5D_t1_1682* DriveInfo_LinuxGetDrives_m1_4833 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DriveInfo[] System.IO.DriveInfo::UnixGetDrives()
extern "C" DriveInfoU5BU5D_t1_1682* DriveInfo_UnixGetDrives_m1_4834 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DriveInfo[] System.IO.DriveInfo::WindowsGetDrives()
extern "C" DriveInfoU5BU5D_t1_1682* DriveInfo_WindowsGetDrives_m1_4835 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.DriveInfo[] System.IO.DriveInfo::GetDrives()
extern "C" DriveInfoU5BU5D_t1_1682* DriveInfo_GetDrives_m1_4836 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.DriveInfo::ToString()
extern "C" String_t* DriveInfo_ToString_m1_4837 (DriveInfo_t1_415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.DriveInfo::GetDiskFreeSpaceInternal(System.String,System.UInt64&,System.UInt64&,System.UInt64&,System.IO.MonoIOError&)
extern "C" bool DriveInfo_GetDiskFreeSpaceInternal_m1_4838 (Object_t * __this /* static, unused */, String_t* ___pathName, uint64_t* ___freeBytesAvail, uint64_t* ___totalNumberOfBytes, uint64_t* ___totalNumberOfFreeBytes, int32_t* ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.IO.DriveInfo::GetDriveTypeInternal(System.String)
extern "C" uint32_t DriveInfo_GetDriveTypeInternal_m1_4839 (Object_t * __this /* static, unused */, String_t* ___rootPathName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
