﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper
struct DictionaryWrapper_t1_951;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t1_1718;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage,System.Collections.IDictionary)
extern "C" void DictionaryWrapper__ctor_m1_8545 (DictionaryWrapper_t1_951 * __this, Object_t * ___message, Object_t * ___wrappedDictionary, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper::.cctor()
extern "C" void DictionaryWrapper__cctor_m1_8546 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper::AllocInternalProperties()
extern "C" Object_t * DictionaryWrapper_AllocInternalProperties_m1_8547 (DictionaryWrapper_t1_951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper::SetMethodProperty(System.String,System.Object)
extern "C" void DictionaryWrapper_SetMethodProperty_m1_8548 (DictionaryWrapper_t1_951 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper/DictionaryWrapper::GetMethodProperty(System.String)
extern "C" Object_t * DictionaryWrapper_GetMethodProperty_m1_8549 (DictionaryWrapper_t1_951 * __this, String_t* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
