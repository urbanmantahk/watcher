﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CrossAppDomainDelegate
struct CrossAppDomainDelegate_t1_1631;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.CrossAppDomainDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void CrossAppDomainDelegate__ctor_m1_14859 (CrossAppDomainDelegate_t1_1631 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CrossAppDomainDelegate::Invoke()
extern "C" void CrossAppDomainDelegate_Invoke_m1_14860 (CrossAppDomainDelegate_t1_1631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CrossAppDomainDelegate_t1_1631(Il2CppObject* delegate);
// System.IAsyncResult System.CrossAppDomainDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * CrossAppDomainDelegate_BeginInvoke_m1_14861 (CrossAppDomainDelegate_t1_1631 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CrossAppDomainDelegate::EndInvoke(System.IAsyncResult)
extern "C" void CrossAppDomainDelegate_EndInvoke_m1_14862 (CrossAppDomainDelegate_t1_1631 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
