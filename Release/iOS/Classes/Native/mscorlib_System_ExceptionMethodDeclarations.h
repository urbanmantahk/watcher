﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Exception
struct Exception_t1_33;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Exception::.ctor()
extern "C" void Exception__ctor_m1_1237 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C" void Exception__ctor_m1_1238 (Exception_t1_33 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Exception__ctor_m1_1239 (Exception_t1_33 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String,System.Exception)
extern "C" void Exception__ctor_m1_1240 (Exception_t1_33 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::get_InnerException()
extern "C" Exception_t1_33 * Exception_get_InnerException_m1_1241 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Exception::get_HelpLink()
extern "C" String_t* Exception_get_HelpLink_m1_1242 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_HelpLink(System.String)
extern "C" void Exception_set_HelpLink_m1_1243 (Exception_t1_33 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Exception::get_HResult()
extern "C" int32_t Exception_get_HResult_m1_1244 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_HResult(System.Int32)
extern "C" void Exception_set_HResult_m1_1245 (Exception_t1_33 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::SetMessage(System.String)
extern "C" void Exception_SetMessage_m1_1246 (Exception_t1_33 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::SetStackTrace(System.String)
extern "C" void Exception_SetStackTrace_m1_1247 (Exception_t1_33 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Exception::get_ClassName()
extern "C" String_t* Exception_get_ClassName_m1_1248 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Exception::get_Message()
extern "C" String_t* Exception_get_Message_m1_1249 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Exception::get_Source()
extern "C" String_t* Exception_get_Source_m1_1250 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_Source(System.String)
extern "C" void Exception_set_Source_m1_1251 (Exception_t1_33 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Exception::get_StackTrace()
extern "C" String_t* Exception_get_StackTrace_m1_1252 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Exception::get_TargetSite()
extern "C" MethodBase_t1_335 * Exception_get_TargetSite_m1_1253 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Exception::get_Data()
extern "C" Object_t * Exception_get_Data_m1_1254 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::GetBaseException()
extern "C" Exception_t1_33 * Exception_GetBaseException_m1_1255 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Exception_GetObjectData_m1_1256 (Exception_t1_33 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Exception::ToString()
extern "C" String_t* Exception_ToString_m1_1257 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::FixRemotingException()
extern "C" Exception_t1_33 * Exception_FixRemotingException_m1_1258 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::GetFullNameForStackTrace(System.Text.StringBuilder,System.Reflection.MethodBase)
extern "C" void Exception_GetFullNameForStackTrace_m1_1259 (Exception_t1_33 * __this, StringBuilder_t1_247 * ___sb, MethodBase_t1_335 * ___mi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Exception::GetType()
extern "C" Type_t * Exception_GetType_m1_1260 (Exception_t1_33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
