﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_SignatureHelper_SignatureHel.h"

// System.Reflection.Emit.SignatureHelper/SignatureHelperType
struct  SignatureHelperType_t1_550 
{
	// System.Int32 System.Reflection.Emit.SignatureHelper/SignatureHelperType::value__
	int32_t ___value___1;
};
