﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request.h"

// VoxelBusters.Utility.DownloadTexture
struct  DownloadTexture_t8_162  : public Request_t8_155
{
	// System.Boolean VoxelBusters.Utility.DownloadTexture::<AutoFixOrientation>k__BackingField
	bool ___U3CAutoFixOrientationU3Ek__BackingField_4;
	// System.Single VoxelBusters.Utility.DownloadTexture::<ScaleFactor>k__BackingField
	float ___U3CScaleFactorU3Ek__BackingField_5;
	// VoxelBusters.Utility.DownloadTexture/Completion VoxelBusters.Utility.DownloadTexture::<OnCompletion>k__BackingField
	Completion_t8_161 * ___U3COnCompletionU3Ek__BackingField_6;
};
