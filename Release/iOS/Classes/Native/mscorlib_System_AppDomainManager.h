﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Hosting.ApplicationActivator
struct ApplicationActivator_t1_719;

#include "mscorlib_System_MarshalByRefObject.h"
#include "mscorlib_System_AppDomainManagerInitializationOptions.h"

// System.AppDomainManager
struct  AppDomainManager_t1_1493  : public MarshalByRefObject_t1_69
{
	// System.Runtime.Hosting.ApplicationActivator System.AppDomainManager::_activator
	ApplicationActivator_t1_719 * ____activator_1;
	// System.AppDomainManagerInitializationOptions System.AppDomainManager::_flags
	int32_t ____flags_2;
};
