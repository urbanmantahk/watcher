﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_TypeKind.h"

// System.Reflection.Emit.TypeKind
struct  TypeKind_t1_488 
{
	// System.Int32 System.Reflection.Emit.TypeKind::value__
	int32_t ___value___1;
};
