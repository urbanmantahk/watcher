﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Versioning.ResourceConsumptionAttribute
struct ResourceConsumptionAttribute_t1_1100;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Versioning_ResourceScope.h"

// System.Void System.Runtime.Versioning.ResourceConsumptionAttribute::.ctor(System.Runtime.Versioning.ResourceScope)
extern "C" void ResourceConsumptionAttribute__ctor_m1_9686 (ResourceConsumptionAttribute_t1_1100 * __this, int32_t ___resourceScope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Versioning.ResourceConsumptionAttribute::.ctor(System.Runtime.Versioning.ResourceScope,System.Runtime.Versioning.ResourceScope)
extern "C" void ResourceConsumptionAttribute__ctor_m1_9687 (ResourceConsumptionAttribute_t1_1100 * __this, int32_t ___resourceScope, int32_t ___consumptionScope, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Versioning.ResourceScope System.Runtime.Versioning.ResourceConsumptionAttribute::get_ConsumptionScope()
extern "C" int32_t ResourceConsumptionAttribute_get_ConsumptionScope_m1_9688 (ResourceConsumptionAttribute_t1_1100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Versioning.ResourceScope System.Runtime.Versioning.ResourceConsumptionAttribute::get_ResourceScope()
extern "C" int32_t ResourceConsumptionAttribute_get_ResourceScope_m1_9689 (ResourceConsumptionAttribute_t1_1100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
