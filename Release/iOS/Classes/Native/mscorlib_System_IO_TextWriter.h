﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t1_16;
// System.IFormatProvider
struct IFormatProvider_t1_455;
// System.IO.TextWriter
struct TextWriter_t1_449;

#include "mscorlib_System_Object.h"

// System.IO.TextWriter
struct  TextWriter_t1_449  : public Object_t
{
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t1_16* ___CoreNewLine_0;
	// System.IFormatProvider System.IO.TextWriter::internalFormatProvider
	Object_t * ___internalFormatProvider_1;
};
struct TextWriter_t1_449_StaticFields{
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t1_449 * ___Null_2;
};
