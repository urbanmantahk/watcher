﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t1_332;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.String)
extern "C" void DebuggerTypeProxyAttribute__ctor_m1_3601 (DebuggerTypeProxyAttribute_t1_332 * __this, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.Type)
extern "C" void DebuggerTypeProxyAttribute__ctor_m1_3602 (DebuggerTypeProxyAttribute_t1_332 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerTypeProxyAttribute::get_ProxyTypeName()
extern "C" String_t* DebuggerTypeProxyAttribute_get_ProxyTypeName_m1_3603 (DebuggerTypeProxyAttribute_t1_332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Diagnostics.DebuggerTypeProxyAttribute::get_Target()
extern "C" Type_t * DebuggerTypeProxyAttribute_get_Target_m1_3604 (DebuggerTypeProxyAttribute_t1_332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::set_Target(System.Type)
extern "C" void DebuggerTypeProxyAttribute_set_Target_m1_3605 (DebuggerTypeProxyAttribute_t1_332 * __this, Type_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.DebuggerTypeProxyAttribute::get_TargetTypeName()
extern "C" String_t* DebuggerTypeProxyAttribute_get_TargetTypeName_m1_3606 (DebuggerTypeProxyAttribute_t1_332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::set_TargetTypeName(System.String)
extern "C" void DebuggerTypeProxyAttribute_set_TargetTypeName_m1_3607 (DebuggerTypeProxyAttribute_t1_332 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
