﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t3_235;
// System.UriParser
struct UriParser_t3_232;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"

// System.Uri
struct  Uri_t3_3  : public Object_t
{
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String System.Uri::source
	String_t* ___source_1;
	// System.String System.Uri::scheme
	String_t* ___scheme_2;
	// System.String System.Uri::host
	String_t* ___host_3;
	// System.Int32 System.Uri::port
	int32_t ___port_4;
	// System.String System.Uri::path
	String_t* ___path_5;
	// System.String System.Uri::query
	String_t* ___query_6;
	// System.String System.Uri::fragment
	String_t* ___fragment_7;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_11;
	// System.String[] System.Uri::segments
	StringU5BU5D_t1_238* ___segments_12;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_13;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_14;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_15;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_16;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_17;
	// System.UriParser System.Uri::parser
	UriParser_t3_232 * ___parser_31;
};
struct Uri_t3_3_StaticFields{
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_18;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_19;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_20;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_21;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_22;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_23;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_24;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_25;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_26;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_27;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_28;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_29;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_t3_235* ___schemes_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map12
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map12_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map13
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map13_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map14_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map15_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map16_36;
};
