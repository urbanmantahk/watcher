﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComAliasNameAttribute
struct ComAliasNameAttribute_t1_767;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComAliasNameAttribute::.ctor(System.String)
extern "C" void ComAliasNameAttribute__ctor_m1_7599 (ComAliasNameAttribute_t1_767 * __this, String_t* ___alias, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.ComAliasNameAttribute::get_Value()
extern "C" String_t* ComAliasNameAttribute_get_Value_m1_7600 (ComAliasNameAttribute_t1_767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
