﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatte_0MethodDeclarations.h"

// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::.ctor()
#define SingletonPattern_1__ctor_m8_2017(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1__ctor_m8_2032_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::.cctor()
#define SingletonPattern_1__cctor_m8_2360(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SingletonPattern_1__cctor_m8_2033_gshared)(__this /* static, unused */, method)
// T VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::get_Instance()
#define SingletonPattern_1_get_Instance_m8_2016(__this /* static, unused */, method) (( NPBinding_t8_333 * (*) (Object_t * /* static, unused */, const MethodInfo*))SingletonPattern_1_get_Instance_m8_2034_gshared)(__this /* static, unused */, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::set_Instance(T)
#define SingletonPattern_1_set_Instance_m8_2361(__this /* static, unused */, ___value, method) (( void (*) (Object_t * /* static, unused */, NPBinding_t8_333 *, const MethodInfo*))SingletonPattern_1_set_Instance_m8_2035_gshared)(__this /* static, unused */, ___value, method)
// UnityEngine.Transform VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::get_CachedTransform()
#define SingletonPattern_1_get_CachedTransform_m8_2362(__this, method) (( Transform_t6_65 * (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_get_CachedTransform_m8_2036_gshared)(__this, method)
// UnityEngine.GameObject VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::get_CachedGameObject()
#define SingletonPattern_1_get_CachedGameObject_m8_2018(__this, method) (( GameObject_t6_97 * (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_get_CachedGameObject_m8_2037_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::ResetStaticProperties()
#define SingletonPattern_1_ResetStaticProperties_m8_2363(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SingletonPattern_1_ResetStaticProperties_m8_2038_gshared)(__this /* static, unused */, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::Awake()
#define SingletonPattern_1_Awake_m8_2364(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_Awake_m8_2039_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::Start()
#define SingletonPattern_1_Start_m8_2365(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_Start_m8_2040_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::Reset()
#define SingletonPattern_1_Reset_m8_2366(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_Reset_m8_2041_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::OnEnable()
#define SingletonPattern_1_OnEnable_m8_2367(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_OnEnable_m8_2042_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::OnDisable()
#define SingletonPattern_1_OnDisable_m8_2368(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_OnDisable_m8_2043_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::OnDestroy()
#define SingletonPattern_1_OnDestroy_m8_2369(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_OnDestroy_m8_2044_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::Init()
#define SingletonPattern_1_Init_m8_2019(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_Init_m8_2045_gshared)(__this, method)
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<NPBinding>::ForceDestroy()
#define SingletonPattern_1_ForceDestroy_m8_2370(__this, method) (( void (*) (SingletonPattern_1_t8_334 *, const MethodInfo*))SingletonPattern_1_ForceDestroy_m8_2046_gshared)(__this, method)
