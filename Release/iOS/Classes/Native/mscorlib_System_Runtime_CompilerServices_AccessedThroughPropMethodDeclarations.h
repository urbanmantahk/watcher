﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.AccessedThroughPropertyAttribute
struct AccessedThroughPropertyAttribute_t1_672;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.AccessedThroughPropertyAttribute::.ctor(System.String)
extern "C" void AccessedThroughPropertyAttribute__ctor_m1_7524 (AccessedThroughPropertyAttribute_t1_672 * __this, String_t* ___propertyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.CompilerServices.AccessedThroughPropertyAttribute::get_PropertyName()
extern "C" String_t* AccessedThroughPropertyAttribute_get_PropertyName_m1_7525 (AccessedThroughPropertyAttribute_t1_672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
