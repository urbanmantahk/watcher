﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.EventInfo/AddEvent`2<System.Object,System.Object>
struct AddEvent_2_t1_2094;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.EventInfo/AddEvent`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void AddEvent_2__ctor_m1_16726_gshared (AddEvent_2_t1_2094 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define AddEvent_2__ctor_m1_16726(__this, ___object, ___method, method) (( void (*) (AddEvent_2_t1_2094 *, Object_t *, IntPtr_t, const MethodInfo*))AddEvent_2__ctor_m1_16726_gshared)(__this, ___object, ___method, method)
// System.Void System.Reflection.EventInfo/AddEvent`2<System.Object,System.Object>::Invoke(T,D)
extern "C" void AddEvent_2_Invoke_m1_16727_gshared (AddEvent_2_t1_2094 * __this, Object_t * ____this, Object_t * ___dele, const MethodInfo* method);
#define AddEvent_2_Invoke_m1_16727(__this, ____this, ___dele, method) (( void (*) (AddEvent_2_t1_2094 *, Object_t *, Object_t *, const MethodInfo*))AddEvent_2_Invoke_m1_16727_gshared)(__this, ____this, ___dele, method)
// System.IAsyncResult System.Reflection.EventInfo/AddEvent`2<System.Object,System.Object>::BeginInvoke(T,D,System.AsyncCallback,System.Object)
extern "C" Object_t * AddEvent_2_BeginInvoke_m1_16728_gshared (AddEvent_2_t1_2094 * __this, Object_t * ____this, Object_t * ___dele, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method);
#define AddEvent_2_BeginInvoke_m1_16728(__this, ____this, ___dele, ___callback, ___object, method) (( Object_t * (*) (AddEvent_2_t1_2094 *, Object_t *, Object_t *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))AddEvent_2_BeginInvoke_m1_16728_gshared)(__this, ____this, ___dele, ___callback, ___object, method)
// System.Void System.Reflection.EventInfo/AddEvent`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void AddEvent_2_EndInvoke_m1_16729_gshared (AddEvent_2_t1_2094 * __this, Object_t * ___result, const MethodInfo* method);
#define AddEvent_2_EndInvoke_m1_16729(__this, ___result, method) (( void (*) (AddEvent_2_t1_2094 *, Object_t *, const MethodInfo*))AddEvent_2_EndInvoke_m1_16729_gshared)(__this, ___result, method)
