﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.Reflection.ObfuscationAttribute
struct  ObfuscationAttribute_t1_625  : public Attribute_t1_2
{
	// System.Boolean System.Reflection.ObfuscationAttribute::exclude
	bool ___exclude_0;
	// System.Boolean System.Reflection.ObfuscationAttribute::strip
	bool ___strip_1;
	// System.Boolean System.Reflection.ObfuscationAttribute::applyToMembers
	bool ___applyToMembers_2;
	// System.String System.Reflection.ObfuscationAttribute::feature
	String_t* ___feature_3;
};
