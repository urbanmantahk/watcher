﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>/$ArrayType$2100
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU242100_t1_1659 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU242100_t1_1659__padding[2100];
	};
};
#pragma pack(pop, tp)
