﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1_148;
// Mono.Security.X509.X509Certificate[]
struct X509CertificateU5BU5D_t1_1674;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
struct X509CertificateEnumerator_t1_192;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509CertificateCollection::.ctor()
extern "C" void X509CertificateCollection__ctor_m1_2288 (X509CertificateCollection_t1_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::.ctor(Mono.Security.X509.X509Certificate[])
extern "C" void X509CertificateCollection__ctor_m1_2289 (X509CertificateCollection_t1_148 * __this, X509CertificateU5BU5D_t1_1674* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::.ctor(Mono.Security.X509.X509CertificateCollection)
extern "C" void X509CertificateCollection__ctor_m1_2290 (X509CertificateCollection_t1_148 * __this, X509CertificateCollection_t1_148 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Mono.Security.X509.X509CertificateCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m1_2291 (X509CertificateCollection_t1_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.X509CertificateCollection::get_Item(System.Int32)
extern "C" X509Certificate_t1_151 * X509CertificateCollection_get_Item_m1_2292 (X509CertificateCollection_t1_148 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::set_Item(System.Int32,Mono.Security.X509.X509Certificate)
extern "C" void X509CertificateCollection_set_Item_m1_2293 (X509CertificateCollection_t1_148 * __this, int32_t ___index, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509CertificateCollection::Add(Mono.Security.X509.X509Certificate)
extern "C" int32_t X509CertificateCollection_Add_m1_2294 (X509CertificateCollection_t1_148 * __this, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::AddRange(Mono.Security.X509.X509Certificate[])
extern "C" void X509CertificateCollection_AddRange_m1_2295 (X509CertificateCollection_t1_148 * __this, X509CertificateU5BU5D_t1_1674* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::AddRange(Mono.Security.X509.X509CertificateCollection)
extern "C" void X509CertificateCollection_AddRange_m1_2296 (X509CertificateCollection_t1_148 * __this, X509CertificateCollection_t1_148 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509CertificateCollection::Contains(Mono.Security.X509.X509Certificate)
extern "C" bool X509CertificateCollection_Contains_m1_2297 (X509CertificateCollection_t1_148 * __this, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::CopyTo(Mono.Security.X509.X509Certificate[],System.Int32)
extern "C" void X509CertificateCollection_CopyTo_m1_2298 (X509CertificateCollection_t1_148 * __this, X509CertificateU5BU5D_t1_1674* ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator Mono.Security.X509.X509CertificateCollection::GetEnumerator()
extern "C" X509CertificateEnumerator_t1_192 * X509CertificateCollection_GetEnumerator_m1_2299 (X509CertificateCollection_t1_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509CertificateCollection::GetHashCode()
extern "C" int32_t X509CertificateCollection_GetHashCode_m1_2300 (X509CertificateCollection_t1_148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.X509CertificateCollection::IndexOf(Mono.Security.X509.X509Certificate)
extern "C" int32_t X509CertificateCollection_IndexOf_m1_2301 (X509CertificateCollection_t1_148 * __this, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::Insert(System.Int32,Mono.Security.X509.X509Certificate)
extern "C" void X509CertificateCollection_Insert_m1_2302 (X509CertificateCollection_t1_148 * __this, int32_t ___index, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509CertificateCollection::Remove(Mono.Security.X509.X509Certificate)
extern "C" void X509CertificateCollection_Remove_m1_2303 (X509CertificateCollection_t1_148 * __this, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.X509CertificateCollection::Compare(System.Byte[],System.Byte[])
extern "C" bool X509CertificateCollection_Compare_m1_2304 (X509CertificateCollection_t1_148 * __this, ByteU5BU5D_t1_109* ___array1, ByteU5BU5D_t1_109* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
