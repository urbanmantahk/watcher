﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t1_2314;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t1_1922;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_2773;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m1_19625_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_19625(__this, method) (( void (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1__ctor_m1_19625_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_19626_gshared (Collection_1_t1_2314 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_19626(__this, ___list, method) (( void (*) (Collection_1_t1_2314 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_19626_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19627_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19627(__this, method) (( bool (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19627_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_19628_gshared (Collection_1_t1_2314 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_19628(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2314 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_19628_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19629_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19629(__this, method) (( Object_t * (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_19629_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_19630_gshared (Collection_1_t1_2314 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_19630(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2314 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_19630_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_19631_gshared (Collection_1_t1_2314 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_19631(__this, ___value, method) (( bool (*) (Collection_1_t1_2314 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_19631_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_19632_gshared (Collection_1_t1_2314 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_19632(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2314 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_19632_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_19633_gshared (Collection_1_t1_2314 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_19633(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2314 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_19633_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_19634_gshared (Collection_1_t1_2314 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_19634(__this, ___value, method) (( void (*) (Collection_1_t1_2314 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_19634_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19635_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19635(__this, method) (( bool (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_19635_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19636_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19636(__this, method) (( Object_t * (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_19636_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_19637_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_19637(__this, method) (( bool (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_19637_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_19638_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_19638(__this, method) (( bool (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_19638_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_19639_gshared (Collection_1_t1_2314 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_19639(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_19639_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_19640_gshared (Collection_1_t1_2314 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_19640(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2314 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_19640_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m1_19641_gshared (Collection_1_t1_2314 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m1_19641(__this, ___item, method) (( void (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_Add_m1_19641_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m1_19642_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_19642(__this, method) (( void (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_Clear_m1_19642_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_19643_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_19643(__this, method) (( void (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_ClearItems_m1_19643_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m1_19644_gshared (Collection_1_t1_2314 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_19644(__this, ___item, method) (( bool (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_Contains_m1_19644_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_19645_gshared (Collection_1_t1_2314 * __this, Int32U5BU5D_t1_275* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_19645(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2314 *, Int32U5BU5D_t1_275*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_19645_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_19646_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_19646(__this, method) (( Object_t* (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_GetEnumerator_m1_19646_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_19647_gshared (Collection_1_t1_2314 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_19647(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m1_19647_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_19648_gshared (Collection_1_t1_2314 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_19648(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2314 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1_19648_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_19649_gshared (Collection_1_t1_2314 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_19649(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2314 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m1_19649_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_19650_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_19650(__this, method) (( Object_t* (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_get_Items_m1_19650_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m1_19651_gshared (Collection_1_t1_2314 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_19651(__this, ___item, method) (( bool (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_Remove_m1_19651_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_19652_gshared (Collection_1_t1_2314 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_19652(__this, ___index, method) (( void (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_19652_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_19653_gshared (Collection_1_t1_2314 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_19653(__this, ___index, method) (( void (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_19653_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_19654_gshared (Collection_1_t1_2314 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_19654(__this, method) (( int32_t (*) (Collection_1_t1_2314 *, const MethodInfo*))Collection_1_get_Count_m1_19654_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m1_19655_gshared (Collection_1_t1_2314 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_19655(__this, ___index, method) (( int32_t (*) (Collection_1_t1_2314 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_19655_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_19656_gshared (Collection_1_t1_2314 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_19656(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2314 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1_19656_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_19657_gshared (Collection_1_t1_2314 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_19657(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2314 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1_19657_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_19658_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_19658(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_19658_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m1_19659_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_19659(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_19659_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_19660_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_19660(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_19660_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_19661_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_19661(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_19661_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_19662_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_19662(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_19662_gshared)(__this /* static, unused */, ___list, method)
