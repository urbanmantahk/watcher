﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_System_Xml_Schema_XmlSchemaParticle.h"

// System.Xml.Schema.XmlSchemaElement
struct  XmlSchemaElement_t4_58  : public XmlSchemaParticle_t4_59
{
};
