﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7/RecipientInfo
struct RecipientInfo_t1_225;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7/RecipientInfo::.ctor()
extern "C" void RecipientInfo__ctor_m1_2508 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/RecipientInfo::.ctor(Mono.Security.ASN1)
extern "C" void RecipientInfo__ctor_m1_2509 (RecipientInfo_t1_225 * __this, ASN1_t1_149 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.PKCS7/RecipientInfo::get_Oid()
extern "C" String_t* RecipientInfo_get_Oid_m1_2510 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/RecipientInfo::get_Key()
extern "C" ByteU5BU5D_t1_109* RecipientInfo_get_Key_m1_2511 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/RecipientInfo::get_SubjectKeyIdentifier()
extern "C" ByteU5BU5D_t1_109* RecipientInfo_get_SubjectKeyIdentifier_m1_2512 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.PKCS7/RecipientInfo::get_Issuer()
extern "C" String_t* RecipientInfo_get_Issuer_m1_2513 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/RecipientInfo::get_Serial()
extern "C" ByteU5BU5D_t1_109* RecipientInfo_get_Serial_m1_2514 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.PKCS7/RecipientInfo::get_Version()
extern "C" int32_t RecipientInfo_get_Version_m1_2515 (RecipientInfo_t1_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
