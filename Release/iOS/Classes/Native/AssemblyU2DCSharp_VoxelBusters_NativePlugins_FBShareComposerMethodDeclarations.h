﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.FBShareComposer
struct FBShareComposer_t8_282;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.FBShareComposer::.ctor()
extern "C" void FBShareComposer__ctor_m8_1672 (FBShareComposer_t8_282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
