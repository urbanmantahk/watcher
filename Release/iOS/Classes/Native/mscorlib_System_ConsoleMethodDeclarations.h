﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Encoding
struct Encoding_t1_406;
// System.IO.TextWriter
struct TextWriter_t1_449;
// System.IO.TextReader
struct TextReader_t1_246;
// System.IO.Stream
struct Stream_t1_405;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_IO_FileAccess.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Console::.cctor()
extern "C" void Console__cctor_m1_13354 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetEncodings(System.Text.Encoding,System.Text.Encoding)
extern "C" void Console_SetEncodings_m1_13355 (Object_t * __this /* static, unused */, Encoding_t1_406 * ___inputEncoding, Encoding_t1_406 * ___outputEncoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextWriter System.Console::get_Error()
extern "C" TextWriter_t1_449 * Console_get_Error_m1_13356 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextWriter System.Console::get_Out()
extern "C" TextWriter_t1_449 * Console_get_Out_m1_13357 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.TextReader System.Console::get_In()
extern "C" TextReader_t1_246 * Console_get_In_m1_13358 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::OpenStandardError()
extern "C" Stream_t1_405 * Console_OpenStandardError_m1_13359 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::Open(System.IntPtr,System.IO.FileAccess,System.Int32)
extern "C" Stream_t1_405 * Console_Open_m1_13360 (Object_t * __this /* static, unused */, IntPtr_t ___handle, int32_t ___access, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::OpenStandardError(System.Int32)
extern "C" Stream_t1_405 * Console_OpenStandardError_m1_13361 (Object_t * __this /* static, unused */, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::OpenStandardInput()
extern "C" Stream_t1_405 * Console_OpenStandardInput_m1_13362 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::OpenStandardInput(System.Int32)
extern "C" Stream_t1_405 * Console_OpenStandardInput_m1_13363 (Object_t * __this /* static, unused */, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::OpenStandardOutput()
extern "C" Stream_t1_405 * Console_OpenStandardOutput_m1_13364 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Console::OpenStandardOutput(System.Int32)
extern "C" Stream_t1_405 * Console_OpenStandardOutput_m1_13365 (Object_t * __this /* static, unused */, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetError(System.IO.TextWriter)
extern "C" void Console_SetError_m1_13366 (Object_t * __this /* static, unused */, TextWriter_t1_449 * ___newError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetIn(System.IO.TextReader)
extern "C" void Console_SetIn_m1_13367 (Object_t * __this /* static, unused */, TextReader_t1_246 * ___newIn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetOut(System.IO.TextWriter)
extern "C" void Console_SetOut_m1_13368 (Object_t * __this /* static, unused */, TextWriter_t1_449 * ___newOut, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Boolean)
extern "C" void Console_Write_m1_13369 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Char)
extern "C" void Console_Write_m1_13370 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Char[])
extern "C" void Console_Write_m1_13371 (Object_t * __this /* static, unused */, CharU5BU5D_t1_16* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Decimal)
extern "C" void Console_Write_m1_13372 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Double)
extern "C" void Console_Write_m1_13373 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Int32)
extern "C" void Console_Write_m1_13374 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Int64)
extern "C" void Console_Write_m1_13375 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Object)
extern "C" void Console_Write_m1_13376 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Single)
extern "C" void Console_Write_m1_13377 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.String)
extern "C" void Console_Write_m1_13378 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.UInt32)
extern "C" void Console_Write_m1_13379 (Object_t * __this /* static, unused */, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.UInt64)
extern "C" void Console_Write_m1_13380 (Object_t * __this /* static, unused */, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.String,System.Object)
extern "C" void Console_Write_m1_13381 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.String,System.Object[])
extern "C" void Console_Write_m1_13382 (Object_t * __this /* static, unused */, String_t* ___format, ObjectU5BU5D_t1_272* ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.Char[],System.Int32,System.Int32)
extern "C" void Console_Write_m1_13383 (Object_t * __this /* static, unused */, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.String,System.Object,System.Object)
extern "C" void Console_Write_m1_13384 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.String,System.Object,System.Object,System.Object)
extern "C" void Console_Write_m1_13385 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::Write(System.String,System.Object,System.Object,System.Object,System.Object)
extern "C" void Console_Write_m1_13386 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, Object_t * ___arg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine()
extern "C" void Console_WriteLine_m1_13387 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Boolean)
extern "C" void Console_WriteLine_m1_13388 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Char)
extern "C" void Console_WriteLine_m1_13389 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Char[])
extern "C" void Console_WriteLine_m1_13390 (Object_t * __this /* static, unused */, CharU5BU5D_t1_16* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Decimal)
extern "C" void Console_WriteLine_m1_13391 (Object_t * __this /* static, unused */, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Double)
extern "C" void Console_WriteLine_m1_13392 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Int32)
extern "C" void Console_WriteLine_m1_13393 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Int64)
extern "C" void Console_WriteLine_m1_13394 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Object)
extern "C" void Console_WriteLine_m1_13395 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Single)
extern "C" void Console_WriteLine_m1_13396 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String)
extern "C" void Console_WriteLine_m1_13397 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.UInt32)
extern "C" void Console_WriteLine_m1_13398 (Object_t * __this /* static, unused */, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.UInt64)
extern "C" void Console_WriteLine_m1_13399 (Object_t * __this /* static, unused */, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String,System.Object)
extern "C" void Console_WriteLine_m1_13400 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String,System.Object[])
extern "C" void Console_WriteLine_m1_13401 (Object_t * __this /* static, unused */, String_t* ___format, ObjectU5BU5D_t1_272* ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.Char[],System.Int32,System.Int32)
extern "C" void Console_WriteLine_m1_13402 (Object_t * __this /* static, unused */, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String,System.Object,System.Object)
extern "C" void Console_WriteLine_m1_13403 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String,System.Object,System.Object,System.Object)
extern "C" void Console_WriteLine_m1_13404 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::WriteLine(System.String,System.Object,System.Object,System.Object,System.Object)
extern "C" void Console_WriteLine_m1_13405 (Object_t * __this /* static, unused */, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, Object_t * ___arg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Console::Read()
extern "C" int32_t Console_Read_m1_13406 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Console::ReadLine()
extern "C" String_t* Console_ReadLine_m1_13407 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Console::get_InputEncoding()
extern "C" Encoding_t1_406 * Console_get_InputEncoding_m1_13408 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::set_InputEncoding(System.Text.Encoding)
extern "C" void Console_set_InputEncoding_m1_13409 (Object_t * __this /* static, unused */, Encoding_t1_406 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Console::get_OutputEncoding()
extern "C" Encoding_t1_406 * Console_get_OutputEncoding_m1_13410 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::set_OutputEncoding(System.Text.Encoding)
extern "C" void Console_set_OutputEncoding_m1_13411 (Object_t * __this /* static, unused */, Encoding_t1_406 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
