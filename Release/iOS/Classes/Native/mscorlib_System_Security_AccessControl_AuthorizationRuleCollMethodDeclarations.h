﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.AuthorizationRuleCollection
struct AuthorizationRuleCollection_t1_1121;
// System.Security.AccessControl.AuthorizationRule[]
struct AuthorizationRuleU5BU5D_t1_1728;
// System.Security.AccessControl.AuthorizationRule
struct AuthorizationRule_t1_1112;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.AccessControl.AuthorizationRuleCollection::.ctor(System.Security.AccessControl.AuthorizationRule[])
extern "C" void AuthorizationRuleCollection__ctor_m1_9722 (AuthorizationRuleCollection_t1_1121 * __this, AuthorizationRuleU5BU5D_t1_1728* ___rules, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuthorizationRule System.Security.AccessControl.AuthorizationRuleCollection::get_Item(System.Int32)
extern "C" AuthorizationRule_t1_1112 * AuthorizationRuleCollection_get_Item_m1_9723 (AuthorizationRuleCollection_t1_1121 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.AuthorizationRuleCollection::CopyTo(System.Security.AccessControl.AuthorizationRule[],System.Int32)
extern "C" void AuthorizationRuleCollection_CopyTo_m1_9724 (AuthorizationRuleCollection_t1_1121 * __this, AuthorizationRuleU5BU5D_t1_1728* ___rules, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
