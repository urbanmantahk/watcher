﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
struct BinaryFormatter_t1_1028;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_959;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t1_1049;
// System.Object
struct Object_t;
// System.IO.Stream
struct Stream_t1_405;
// System.Runtime.Remoting.Messaging.HeaderHandler
struct HeaderHandler_t1_1623;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1_927;
// System.IO.BinaryWriter
struct BinaryWriter_t1_408;
// System.IO.BinaryReader
struct BinaryReader_t1_404;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"

// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor()
extern "C" void BinaryFormatter__ctor_m1_9338 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::.ctor(System.Runtime.Serialization.ISurrogateSelector,System.Runtime.Serialization.StreamingContext)
extern "C" void BinaryFormatter__ctor_m1_9339 (BinaryFormatter_t1_1028 * __this, Object_t * ___selector, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_DefaultSurrogateSelector()
extern "C" Object_t * BinaryFormatter_get_DefaultSurrogateSelector_m1_9340 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_DefaultSurrogateSelector(System.Runtime.Serialization.ISurrogateSelector)
extern "C" void BinaryFormatter_set_DefaultSurrogateSelector_m1_9341 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_AssemblyFormat()
extern "C" int32_t BinaryFormatter_get_AssemblyFormat_m1_9342 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_AssemblyFormat(System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C" void BinaryFormatter_set_AssemblyFormat_m1_9343 (BinaryFormatter_t1_1028 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Binder()
extern "C" SerializationBinder_t1_1049 * BinaryFormatter_get_Binder_m1_9344 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_Binder(System.Runtime.Serialization.SerializationBinder)
extern "C" void BinaryFormatter_set_Binder_m1_9345 (BinaryFormatter_t1_1028 * __this, SerializationBinder_t1_1049 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_Context()
extern "C" StreamingContext_t1_1050  BinaryFormatter_get_Context_m1_9346 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_Context(System.Runtime.Serialization.StreamingContext)
extern "C" void BinaryFormatter_set_Context_m1_9347 (BinaryFormatter_t1_1028 * __this, StreamingContext_t1_1050  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_SurrogateSelector()
extern "C" Object_t * BinaryFormatter_get_SurrogateSelector_m1_9348 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_SurrogateSelector(System.Runtime.Serialization.ISurrogateSelector)
extern "C" void BinaryFormatter_set_SurrogateSelector_m1_9349 (BinaryFormatter_t1_1028 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_TypeFormat()
extern "C" int32_t BinaryFormatter_get_TypeFormat_m1_9350 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_TypeFormat(System.Runtime.Serialization.Formatters.FormatterTypeStyle)
extern "C" void BinaryFormatter_set_TypeFormat_m1_9351 (BinaryFormatter_t1_1028 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.TypeFilterLevel System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::get_FilterLevel()
extern "C" int32_t BinaryFormatter_get_FilterLevel_m1_9352 (BinaryFormatter_t1_1028 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::set_FilterLevel(System.Runtime.Serialization.Formatters.TypeFilterLevel)
extern "C" void BinaryFormatter_set_FilterLevel_m1_9353 (BinaryFormatter_t1_1028 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream)
extern "C" Object_t * BinaryFormatter_Deserialize_m1_9354 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Deserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
extern "C" Object_t * BinaryFormatter_Deserialize_m1_9355 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, HeaderHandler_t1_1623 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::NoCheckDeserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
extern "C" Object_t * BinaryFormatter_NoCheckDeserialize_m1_9356 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, HeaderHandler_t1_1623 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::DeserializeMethodResponse(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" Object_t * BinaryFormatter_DeserializeMethodResponse_m1_9357 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, HeaderHandler_t1_1623 * ___handler, Object_t * ___methodCallMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::NoCheckDeserializeMethodResponse(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" Object_t * BinaryFormatter_NoCheckDeserializeMethodResponse_m1_9358 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, HeaderHandler_t1_1623 * ___handler, Object_t * ___methodCallMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object)
extern "C" void BinaryFormatter_Serialize_m1_9359 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, Object_t * ___graph, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::Serialize(System.IO.Stream,System.Object,System.Runtime.Remoting.Messaging.Header[])
extern "C" void BinaryFormatter_Serialize_m1_9360 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, Object_t * ___graph, HeaderU5BU5D_t1_927* ___headers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::UnsafeDeserialize(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler)
extern "C" Object_t * BinaryFormatter_UnsafeDeserialize_m1_9361 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, HeaderHandler_t1_1623 * ___handler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::UnsafeDeserializeMethodResponse(System.IO.Stream,System.Runtime.Remoting.Messaging.HeaderHandler,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" Object_t * BinaryFormatter_UnsafeDeserializeMethodResponse_m1_9362 (BinaryFormatter_t1_1028 * __this, Stream_t1_405 * ___serializationStream, HeaderHandler_t1_1623 * ___handler, Object_t * ___methodCallMessage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::WriteBinaryHeader(System.IO.BinaryWriter,System.Boolean)
extern "C" void BinaryFormatter_WriteBinaryHeader_m1_9363 (BinaryFormatter_t1_1028 * __this, BinaryWriter_t1_408 * ___writer, bool ___hasHeaders, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.Binary.BinaryFormatter::ReadBinaryHeader(System.IO.BinaryReader,System.Boolean&)
extern "C" void BinaryFormatter_ReadBinaryHeader_m1_9364 (BinaryFormatter_t1_1028 * __this, BinaryReader_t1_404 * ___reader, bool* ___hasHeaders, const MethodInfo* method) IL2CPP_METHOD_ATTR;
