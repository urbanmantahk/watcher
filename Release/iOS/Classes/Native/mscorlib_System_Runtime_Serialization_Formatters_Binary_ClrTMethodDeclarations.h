﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata
struct ClrTypeMetadata_t1_1059;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata::.ctor(System.Type)
extern "C" void ClrTypeMetadata__ctor_m1_9417 (ClrTypeMetadata_t1_1059 * __this, Type_t * ___instanceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.Formatters.Binary.ClrTypeMetadata::get_RequiresTypes()
extern "C" bool ClrTypeMetadata_get_RequiresTypes_m1_9418 (ClrTypeMetadata_t1_1059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
