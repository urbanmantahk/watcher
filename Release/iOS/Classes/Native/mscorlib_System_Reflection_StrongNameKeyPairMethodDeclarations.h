﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_t1_577;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.FileStream
struct FileStream_t1_146;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// Mono.Security.StrongName
struct StrongName_t1_232;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.Byte[])
extern "C" void StrongNameKeyPair__ctor_m1_7281 (StrongNameKeyPair_t1_577 * __this, ByteU5BU5D_t1_109* ___keyPairArray, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.IO.FileStream)
extern "C" void StrongNameKeyPair__ctor_m1_7282 (StrongNameKeyPair_t1_577 * __this, FileStream_t1_146 * ___keyPairFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.String)
extern "C" void StrongNameKeyPair__ctor_m1_7283 (StrongNameKeyPair_t1_577 * __this, String_t* ___keyPairContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StrongNameKeyPair__ctor_m1_7284 (StrongNameKeyPair_t1_577 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m1_7285 (StrongNameKeyPair_t1_577 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_7286 (StrongNameKeyPair_t1_577 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Reflection.StrongNameKeyPair::GetRSA()
extern "C" RSA_t1_175 * StrongNameKeyPair_GetRSA_m1_7287 (StrongNameKeyPair_t1_577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.StrongNameKeyPair::LoadKey(System.Byte[])
extern "C" void StrongNameKeyPair_LoadKey_m1_7288 (StrongNameKeyPair_t1_577 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.StrongNameKeyPair::get_PublicKey()
extern "C" ByteU5BU5D_t1_109* StrongNameKeyPair_get_PublicKey_m1_7289 (StrongNameKeyPair_t1_577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.StrongName System.Reflection.StrongNameKeyPair::StrongName()
extern "C" StrongName_t1_232 * StrongNameKeyPair_StrongName_m1_7290 (StrongNameKeyPair_t1_577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
