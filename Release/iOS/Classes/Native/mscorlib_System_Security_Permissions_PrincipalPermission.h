﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Object.h"

// System.Security.Permissions.PrincipalPermission
struct  PrincipalPermission_t1_1297  : public Object_t
{
	// System.Collections.ArrayList System.Security.Permissions.PrincipalPermission::principals
	ArrayList_t1_170 * ___principals_1;
};
