﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.NativeBinding
struct NativeBinding_t8_59;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.NativeBinding::.ctor()
extern "C" void NativeBinding__ctor_m8_335 (NativeBinding_t8_59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.NativeBinding::utilityBundleVersion()
extern "C" String_t* NativeBinding_utilityBundleVersion_m8_336 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.NativeBinding::utilityBundleIdentifier()
extern "C" String_t* NativeBinding_utilityBundleIdentifier_m8_337 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.NativeBinding::GetBundleVersion()
extern "C" String_t* NativeBinding_GetBundleVersion_m8_338 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.NativeBinding::GetBundleIdentifier()
extern "C" String_t* NativeBinding_GetBundleIdentifier_m8_339 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
