﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifUShort
struct  ExifUShort_t8_117  : public ExifProperty_t8_99
{
	// System.UInt16 ExifLibrary.ExifUShort::mValue
	uint16_t ___mValue_3;
};
