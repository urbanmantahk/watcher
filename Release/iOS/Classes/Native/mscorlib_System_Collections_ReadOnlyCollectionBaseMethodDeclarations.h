﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ReadOnlyCollectionBase
struct ReadOnlyCollectionBase_t1_300;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Array
struct Array_t;
// System.Object
struct Object_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ReadOnlyCollectionBase::.ctor()
extern "C" void ReadOnlyCollectionBase__ctor_m1_3373 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ReadOnlyCollectionBase::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollectionBase_System_Collections_IEnumerable_GetEnumerator_m1_3374 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ReadOnlyCollectionBase::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollectionBase_System_Collections_ICollection_CopyTo_m1_3375 (ReadOnlyCollectionBase_t1_300 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ReadOnlyCollectionBase::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollectionBase_System_Collections_ICollection_get_SyncRoot_m1_3376 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ReadOnlyCollectionBase::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollectionBase_System_Collections_ICollection_get_IsSynchronized_m1_3377 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ReadOnlyCollectionBase::get_Count()
extern "C" int32_t ReadOnlyCollectionBase_get_Count_m1_3378 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ReadOnlyCollectionBase::GetEnumerator()
extern "C" Object_t * ReadOnlyCollectionBase_GetEnumerator_m1_3379 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ReadOnlyCollectionBase::get_InnerList()
extern "C" ArrayList_t1_170 * ReadOnlyCollectionBase_get_InnerList_m1_3380 (ReadOnlyCollectionBase_t1_300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
