﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.OneWayAttribute
struct OneWayAttribute_t1_954;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.OneWayAttribute::.ctor()
extern "C" void OneWayAttribute__ctor_m1_8606 (OneWayAttribute_t1_954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
