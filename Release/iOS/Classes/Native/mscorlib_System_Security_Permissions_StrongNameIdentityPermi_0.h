﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Version
struct Version_t1_578;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Security.Permissions.StrongNameIdentityPermission
struct  StrongNameIdentityPermission_t1_1315  : public CodeAccessPermission_t1_1268
{
	// System.Security.Permissions.PermissionState System.Security.Permissions.StrongNameIdentityPermission::_state
	int32_t ____state_2;
	// System.Collections.ArrayList System.Security.Permissions.StrongNameIdentityPermission::_list
	ArrayList_t1_170 * ____list_3;
};
struct StrongNameIdentityPermission_t1_1315_StaticFields{
	// System.Version System.Security.Permissions.StrongNameIdentityPermission::defaultVersion
	Version_t1_578 * ___defaultVersion_1;
};
