﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_Mono_Security_X509_X520_AttributeTypeAndValue.h"

// Mono.Security.X509.X520/StateOrProvinceName
struct  StateOrProvinceName_t1_205  : public AttributeTypeAndValue_t1_200
{
};
