﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlSignificantWhitespace
struct XmlSignificantWhitespace_t4_165;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeType.h"

// System.Void System.Xml.XmlSignificantWhitespace::.ctor(System.String,System.Xml.XmlDocument)
extern "C" void XmlSignificantWhitespace__ctor_m4_764 (XmlSignificantWhitespace_t4_165 * __this, String_t* ___strData, XmlDocument_t4_123 * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlSignificantWhitespace::get_LocalName()
extern "C" String_t* XmlSignificantWhitespace_get_LocalName_m4_765 (XmlSignificantWhitespace_t4_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlSignificantWhitespace::get_Name()
extern "C" String_t* XmlSignificantWhitespace_get_Name_m4_766 (XmlSignificantWhitespace_t4_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.XmlSignificantWhitespace::get_NodeType()
extern "C" int32_t XmlSignificantWhitespace_get_NodeType_m4_767 (XmlSignificantWhitespace_t4_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlSignificantWhitespace::get_Value()
extern "C" String_t* XmlSignificantWhitespace_get_Value_m4_768 (XmlSignificantWhitespace_t4_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlSignificantWhitespace::set_Value(System.String)
extern "C" void XmlSignificantWhitespace_set_Value_m4_769 (XmlSignificantWhitespace_t4_165 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlSignificantWhitespace::get_ParentNode()
extern "C" XmlNode_t4_116 * XmlSignificantWhitespace_get_ParentNode_m4_770 (XmlSignificantWhitespace_t4_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlSignificantWhitespace::CloneNode(System.Boolean)
extern "C" XmlNode_t4_116 * XmlSignificantWhitespace_CloneNode_m4_771 (XmlSignificantWhitespace_t4_165 * __this, bool ___deep, const MethodInfo* method) IL2CPP_METHOD_ATTR;
