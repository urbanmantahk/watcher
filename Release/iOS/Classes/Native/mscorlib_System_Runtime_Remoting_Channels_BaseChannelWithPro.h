﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Channels.IChannelSinkBase
struct IChannelSinkBase_t1_867;

#include "mscorlib_System_Runtime_Remoting_Channels_BaseChannelObjectW.h"

// System.Runtime.Remoting.Channels.BaseChannelWithProperties
struct  BaseChannelWithProperties_t1_866  : public BaseChannelObjectWithProperties_t1_864
{
	// System.Runtime.Remoting.Channels.IChannelSinkBase System.Runtime.Remoting.Channels.BaseChannelWithProperties::SinksWithProperties
	Object_t * ___SinksWithProperties_1;
};
