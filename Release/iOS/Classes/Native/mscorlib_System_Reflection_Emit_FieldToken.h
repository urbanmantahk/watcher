﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_FieldToken.h"

// System.Reflection.Emit.FieldToken
struct  FieldToken_t1_507 
{
	// System.Int32 System.Reflection.Emit.FieldToken::tokValue
	int32_t ___tokValue_0;
};
struct FieldToken_t1_507_StaticFields{
	// System.Reflection.Emit.FieldToken System.Reflection.Emit.FieldToken::Empty
	FieldToken_t1_507  ___Empty_1;
};
