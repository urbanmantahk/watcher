﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AddressBook
struct AddressBook_t8_196;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.AddressBookContact[]
struct AddressBookContactU5BU5D_t8_177;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion
struct RequestAccessCompletion_t8_193;
// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion
struct ReadContactsCompletion_t8_194;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatio.h"

// System.Void VoxelBusters.NativePlugins.AddressBook::.ctor()
extern "C" void AddressBook__ctor_m8_1128 (AddressBook_t8_196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ABRequestAccessFinished(System.String)
extern "C" void AddressBook_ABRequestAccessFinished_m8_1129 (AddressBook_t8_196 * __this, String_t* ____dataStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ABRequestAccessFinished(VoxelBusters.NativePlugins.eABAuthorizationStatus,System.String)
extern "C" void AddressBook_ABRequestAccessFinished_m8_1130 (AddressBook_t8_196 * __this, int32_t ____authStatus, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ABReadContactsFinished(System.String)
extern "C" void AddressBook_ABReadContactsFinished_m8_1131 (AddressBook_t8_196 * __this, String_t* ____data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ABReadContactsFinished(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBookContact[])
extern "C" void AddressBook_ABReadContactsFinished_m8_1132 (AddressBook_t8_196 * __this, int32_t ____authStatus, AddressBookContactU5BU5D_t8_177* ____contactsList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ParseReadContactsResponseData(System.Collections.IDictionary,VoxelBusters.NativePlugins.eABAuthorizationStatus&,VoxelBusters.NativePlugins.AddressBookContact[]&)
extern "C" void AddressBook_ParseReadContactsResponseData_m8_1133 (AddressBook_t8_196 * __this, Object_t * ____dataDict, int32_t* ____authStatus, AddressBookContactU5BU5D_t8_177** ____contactsList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eABAuthorizationStatus VoxelBusters.NativePlugins.AddressBook::GetAuthorizationStatus()
extern "C" int32_t AddressBook_GetAuthorizationStatus_m8_1134 (AddressBook_t8_196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::RequestAccess(VoxelBusters.NativePlugins.AddressBook/RequestAccessCompletion)
extern "C" void AddressBook_RequestAccess_m8_1135 (AddressBook_t8_196 * __this, RequestAccessCompletion_t8_193 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ReadContacts(VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion)
extern "C" void AddressBook_ReadContacts_m8_1136 (AddressBook_t8_196 * __this, ReadContactsCompletion_t8_194 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook::ReadContacts(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion)
extern "C" void AddressBook_ReadContacts_m8_1137 (AddressBook_t8_196 * __this, int32_t ____status, ReadContactsCompletion_t8_194 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
