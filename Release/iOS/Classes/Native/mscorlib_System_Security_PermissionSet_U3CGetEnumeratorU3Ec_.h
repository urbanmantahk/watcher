﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Security.PermissionSet/<GetEnumerator>c__Iterator1
struct  U3CGetEnumeratorU3Ec__Iterator1_t1_1392  : public Object_t
{
	// System.Int32 System.Security.PermissionSet/<GetEnumerator>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object System.Security.PermissionSet/<GetEnumerator>c__Iterator1::$current
	Object_t * ___U24current_1;
};
