﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ApplicationId
struct ApplicationId_t1_1329;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Version
struct Version_t1_578;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ApplicationId::.ctor(System.Byte[],System.String,System.Version,System.String,System.String)
extern "C" void ApplicationId__ctor_m1_13244 (ApplicationId_t1_1329 * __this, ByteU5BU5D_t1_109* ___publicKeyToken, String_t* ___name, Version_t1_578 * ___version, String_t* ___processorArchitecture, String_t* ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationId::get_Culture()
extern "C" String_t* ApplicationId_get_Culture_m1_13245 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationId::get_Name()
extern "C" String_t* ApplicationId_get_Name_m1_13246 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationId::get_ProcessorArchitecture()
extern "C" String_t* ApplicationId_get_ProcessorArchitecture_m1_13247 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.ApplicationId::get_PublicKeyToken()
extern "C" ByteU5BU5D_t1_109* ApplicationId_get_PublicKeyToken_m1_13248 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.ApplicationId::get_Version()
extern "C" Version_t1_578 * ApplicationId_get_Version_m1_13249 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ApplicationId System.ApplicationId::Copy()
extern "C" ApplicationId_t1_1329 * ApplicationId_Copy_m1_13250 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ApplicationId::Equals(System.Object)
extern "C" bool ApplicationId_Equals_m1_13251 (ApplicationId_t1_1329 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ApplicationId::GetHashCode()
extern "C" int32_t ApplicationId_GetHashCode_m1_13252 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationId::ToString()
extern "C" String_t* ApplicationId_ToString_m1_13253 (ApplicationId_t1_1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
