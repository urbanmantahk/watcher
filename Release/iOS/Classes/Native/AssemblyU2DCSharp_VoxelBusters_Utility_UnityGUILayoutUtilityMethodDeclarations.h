﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.UnityGUILayoutUtility
struct UnityGUILayoutUtility_t8_139;
// UnityEngine.GUIContent
struct GUIContent_t6_171;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.UnityGUILayoutUtility::.ctor()
extern "C" void UnityGUILayoutUtility__ctor_m8_819 (UnityGUILayoutUtility_t8_139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.UnityGUILayoutUtility::Foldout(UnityEngine.GUIContent,System.Boolean)
extern "C" bool UnityGUILayoutUtility_Foldout_m8_820 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ____label, bool ____state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
