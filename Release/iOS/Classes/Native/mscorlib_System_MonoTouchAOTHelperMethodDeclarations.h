﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MonoTouchAOTHelper
struct MonoTouchAOTHelper_t1_1577;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MonoTouchAOTHelper::.ctor()
extern "C" void MonoTouchAOTHelper__ctor_m1_14328 (MonoTouchAOTHelper_t1_1577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTouchAOTHelper::.cctor()
extern "C" void MonoTouchAOTHelper__cctor_m1_14329 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
