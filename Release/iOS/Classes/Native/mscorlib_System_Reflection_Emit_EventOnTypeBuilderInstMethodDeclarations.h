﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.EventOnTypeBuilderInst
struct EventOnTypeBuilderInst_t1_503;
// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.EventBuilder
struct EventBuilder_t1_500;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_EventAttributes.h"

// System.Void System.Reflection.Emit.EventOnTypeBuilderInst::.ctor(System.Reflection.MonoGenericClass,System.Reflection.Emit.EventBuilder)
extern "C" void EventOnTypeBuilderInst__ctor_m1_5811 (EventOnTypeBuilderInst_t1_503 * __this, MonoGenericClass_t1_485 * ___instantiation, EventBuilder_t1_500 * ___evt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventAttributes System.Reflection.Emit.EventOnTypeBuilderInst::get_Attributes()
extern "C" int32_t EventOnTypeBuilderInst_get_Attributes_m1_5812 (EventOnTypeBuilderInst_t1_503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.EventOnTypeBuilderInst::GetAddMethod(System.Boolean)
extern "C" MethodInfo_t * EventOnTypeBuilderInst_GetAddMethod_m1_5813 (EventOnTypeBuilderInst_t1_503 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.EventOnTypeBuilderInst::GetRaiseMethod(System.Boolean)
extern "C" MethodInfo_t * EventOnTypeBuilderInst_GetRaiseMethod_m1_5814 (EventOnTypeBuilderInst_t1_503 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.EventOnTypeBuilderInst::GetRemoveMethod(System.Boolean)
extern "C" MethodInfo_t * EventOnTypeBuilderInst_GetRemoveMethod_m1_5815 (EventOnTypeBuilderInst_t1_503 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.EventOnTypeBuilderInst::GetOtherMethods(System.Boolean)
extern "C" MethodInfoU5BU5D_t1_603* EventOnTypeBuilderInst_GetOtherMethods_m1_5816 (EventOnTypeBuilderInst_t1_503 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EventOnTypeBuilderInst::get_DeclaringType()
extern "C" Type_t * EventOnTypeBuilderInst_get_DeclaringType_m1_5817 (EventOnTypeBuilderInst_t1_503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.EventOnTypeBuilderInst::get_Name()
extern "C" String_t* EventOnTypeBuilderInst_get_Name_m1_5818 (EventOnTypeBuilderInst_t1_503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.EventOnTypeBuilderInst::get_ReflectedType()
extern "C" Type_t * EventOnTypeBuilderInst_get_ReflectedType_m1_5819 (EventOnTypeBuilderInst_t1_503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.EventOnTypeBuilderInst::IsDefined(System.Type,System.Boolean)
extern "C" bool EventOnTypeBuilderInst_IsDefined_m1_5820 (EventOnTypeBuilderInst_t1_503 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.EventOnTypeBuilderInst::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* EventOnTypeBuilderInst_GetCustomAttributes_m1_5821 (EventOnTypeBuilderInst_t1_503 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.EventOnTypeBuilderInst::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* EventOnTypeBuilderInst_GetCustomAttributes_m1_5822 (EventOnTypeBuilderInst_t1_503 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
