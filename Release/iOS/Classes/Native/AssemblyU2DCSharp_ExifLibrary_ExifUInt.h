﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifUInt
struct  ExifUInt_t8_118  : public ExifProperty_t8_99
{
	// System.UInt32 ExifLibrary.ExifUInt::mValue
	uint32_t ___mValue_3;
};
