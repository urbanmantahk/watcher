﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Security_AccessControl_RegistryRights.h"

// System.Security.AccessControl.RegistryAccessRule
struct  RegistryAccessRule_t1_1172  : public AccessRule_t1_1111
{
	// System.Security.AccessControl.RegistryRights System.Security.AccessControl.RegistryAccessRule::rights
	int32_t ___rights_6;
};
