﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Lifetime.LifetimeServices
struct LifetimeServices_t1_913;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1_70;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::.ctor()
extern "C" void LifetimeServices__ctor_m1_8259 (LifetimeServices_t1_913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::.cctor()
extern "C" void LifetimeServices__cctor_m1_8260 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_LeaseManagerPollTime()
extern "C" TimeSpan_t1_368  LifetimeServices_get_LeaseManagerPollTime_m1_8261 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::set_LeaseManagerPollTime(System.TimeSpan)
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m1_8262 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_LeaseTime()
extern "C" TimeSpan_t1_368  LifetimeServices_get_LeaseTime_m1_8263 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::set_LeaseTime(System.TimeSpan)
extern "C" void LifetimeServices_set_LeaseTime_m1_8264 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_RenewOnCallTime()
extern "C" TimeSpan_t1_368  LifetimeServices_get_RenewOnCallTime_m1_8265 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::set_RenewOnCallTime(System.TimeSpan)
extern "C" void LifetimeServices_set_RenewOnCallTime_m1_8266 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.Runtime.Remoting.Lifetime.LifetimeServices::get_SponsorshipTimeout()
extern "C" TimeSpan_t1_368  LifetimeServices_get_SponsorshipTimeout_m1_8267 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::set_SponsorshipTimeout(System.TimeSpan)
extern "C" void LifetimeServices_set_SponsorshipTimeout_m1_8268 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::TrackLifetime(System.Runtime.Remoting.ServerIdentity)
extern "C" void LifetimeServices_TrackLifetime_m1_8269 (Object_t * __this /* static, unused */, ServerIdentity_t1_70 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Lifetime.LifetimeServices::StopTrackingLifetime(System.Runtime.Remoting.ServerIdentity)
extern "C" void LifetimeServices_StopTrackingLifetime_m1_8270 (Object_t * __this /* static, unused */, ServerIdentity_t1_70 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
