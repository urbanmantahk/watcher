﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;

#include "mscorlib_System_Type.h"
#include "mscorlib_System_Reflection_GenericParameterAttributes.h"

// System.Reflection.Emit.GenericTypeParameterBuilder
struct  GenericTypeParameterBuilder_t1_509  : public Type_t
{
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.GenericTypeParameterBuilder::tbuilder
	TypeBuilder_t1_481 * ___tbuilder_8;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.GenericTypeParameterBuilder::mbuilder
	MethodBuilder_t1_501 * ___mbuilder_9;
	// System.String System.Reflection.Emit.GenericTypeParameterBuilder::name
	String_t* ___name_10;
	// System.Int32 System.Reflection.Emit.GenericTypeParameterBuilder::index
	int32_t ___index_11;
	// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::base_type
	Type_t * ___base_type_12;
	// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::iface_constraints
	TypeU5BU5D_t1_31* ___iface_constraints_13;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.GenericTypeParameterBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_14;
	// System.Reflection.GenericParameterAttributes System.Reflection.Emit.GenericTypeParameterBuilder::attrs
	int32_t ___attrs_15;
};
