﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t1_482;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1_529;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.Emit.RefEmitPermissionSet[]
struct RefEmitPermissionSetU5BU5D_t1_473;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_IntPtr.h"

// System.Reflection.Emit.MethodBuilder
struct  MethodBuilder_t1_501  : public MethodInfo_t
{
	// System.RuntimeMethodHandle System.Reflection.Emit.MethodBuilder::mhandle
	RuntimeMethodHandle_t1_479  ___mhandle_0;
	// System.Type System.Reflection.Emit.MethodBuilder::rtype
	Type_t * ___rtype_1;
	// System.Type[] System.Reflection.Emit.MethodBuilder::parameters
	TypeU5BU5D_t1_31* ___parameters_2;
	// System.Reflection.MethodAttributes System.Reflection.Emit.MethodBuilder::attrs
	int32_t ___attrs_3;
	// System.Reflection.MethodImplAttributes System.Reflection.Emit.MethodBuilder::iattrs
	int32_t ___iattrs_4;
	// System.String System.Reflection.Emit.MethodBuilder::name
	String_t* ___name_5;
	// System.Int32 System.Reflection.Emit.MethodBuilder::table_idx
	int32_t ___table_idx_6;
	// System.Byte[] System.Reflection.Emit.MethodBuilder::code
	ByteU5BU5D_t1_109* ___code_7;
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.MethodBuilder::ilgen
	ILGenerator_t1_480 * ___ilgen_8;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.MethodBuilder::type
	TypeBuilder_t1_481 * ___type_9;
	// System.Reflection.Emit.ParameterBuilder[] System.Reflection.Emit.MethodBuilder::pinfo
	ParameterBuilderU5BU5D_t1_482* ___pinfo_10;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.MethodBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_11;
	// System.Reflection.MethodInfo System.Reflection.Emit.MethodBuilder::override_method
	MethodInfo_t * ___override_method_12;
	// System.String System.Reflection.Emit.MethodBuilder::pi_dll
	String_t* ___pi_dll_13;
	// System.String System.Reflection.Emit.MethodBuilder::pi_entry
	String_t* ___pi_entry_14;
	// System.Runtime.InteropServices.CharSet System.Reflection.Emit.MethodBuilder::charset
	int32_t ___charset_15;
	// System.UInt32 System.Reflection.Emit.MethodBuilder::extra_flags
	uint32_t ___extra_flags_16;
	// System.Runtime.InteropServices.CallingConvention System.Reflection.Emit.MethodBuilder::native_cc
	int32_t ___native_cc_17;
	// System.Reflection.CallingConventions System.Reflection.Emit.MethodBuilder::call_conv
	int32_t ___call_conv_18;
	// System.Boolean System.Reflection.Emit.MethodBuilder::init_locals
	bool ___init_locals_19;
	// System.IntPtr System.Reflection.Emit.MethodBuilder::generic_container
	IntPtr_t ___generic_container_20;
	// System.Reflection.Emit.GenericTypeParameterBuilder[] System.Reflection.Emit.MethodBuilder::generic_params
	GenericTypeParameterBuilderU5BU5D_t1_529* ___generic_params_21;
	// System.Type[] System.Reflection.Emit.MethodBuilder::returnModReq
	TypeU5BU5D_t1_31* ___returnModReq_22;
	// System.Type[] System.Reflection.Emit.MethodBuilder::returnModOpt
	TypeU5BU5D_t1_31* ___returnModOpt_23;
	// System.Type[][] System.Reflection.Emit.MethodBuilder::paramModReq
	TypeU5BU5DU5BU5D_t1_483* ___paramModReq_24;
	// System.Type[][] System.Reflection.Emit.MethodBuilder::paramModOpt
	TypeU5BU5DU5BU5D_t1_483* ___paramModOpt_25;
	// System.Reflection.Emit.RefEmitPermissionSet[] System.Reflection.Emit.MethodBuilder::permissions
	RefEmitPermissionSetU5BU5D_t1_473* ___permissions_26;
};
struct MethodBuilder_t1_501_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Reflection.Emit.MethodBuilder::<>f__switch$map1D
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map1D_27;
};
