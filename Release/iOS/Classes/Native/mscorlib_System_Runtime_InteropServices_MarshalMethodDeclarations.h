﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Array
struct Array_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Int16[]
struct Int16U5BU5D_t1_1694;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Int64[]
struct Int64U5BU5D_t1_1665;
// System.Single[]
struct SingleU5BU5D_t1_1695;
// System.Double[]
struct DoubleU5BU5D_t1_1666;
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Module
struct Module_t1_495;
// System.Exception
struct Exception_t1_33;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Threading.Thread
struct Thread_t1_901;
// System.Runtime.InteropServices.UCOMITypeInfo
struct UCOMITypeInfo_t1_1696;
// System.Runtime.InteropServices.ComTypes.ITypeInfo
struct ITypeInfo_t1_1697;
// System.Runtime.InteropServices.UCOMITypeLib
struct UCOMITypeLib_t1_1698;
// System.Runtime.InteropServices.ComTypes.ITypeLib
struct ITypeLib_t1_1699;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.SecureString
struct SecureString_t1_1197;
// System.Delegate
struct Delegate_t1_22;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Runtime_InteropServices_ComMemberType.h"

// System.Void System.Runtime.InteropServices.Marshal::.cctor()
extern "C" void Marshal__cctor_m1_7695 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::AddRefInternal(System.IntPtr)
extern "C" int32_t Marshal_AddRefInternal_m1_7696 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::AddRef(System.IntPtr)
extern "C" int32_t Marshal_AddRef_m1_7697 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocCoTaskMem(System.Int32)
extern "C" IntPtr_t Marshal_AllocCoTaskMem_m1_7698 (Object_t * __this /* static, unused */, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.IntPtr)
extern "C" IntPtr_t Marshal_AllocHGlobal_m1_7699 (Object_t * __this /* static, unused */, IntPtr_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.Int32)
extern "C" IntPtr_t Marshal_AllocHGlobal_m1_7700 (Object_t * __this /* static, unused */, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::BindToMoniker(System.String)
extern "C" Object_t * Marshal_BindToMoniker_m1_7701 (Object_t * __this /* static, unused */, String_t* ___monikerName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ChangeWrapperHandleStrength(System.Object,System.Boolean)
extern "C" void Marshal_ChangeWrapperHandleStrength_m1_7702 (Object_t * __this /* static, unused */, Object_t * ___otp, bool ___fIsWeak, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::copy_to_unmanaged(System.Array,System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_copy_to_unmanaged_m1_7703 (Object_t * __this /* static, unused */, Array_t * ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
extern "C" void Marshal_copy_from_unmanaged_m1_7704 (Object_t * __this /* static, unused */, IntPtr_t ___source, int32_t ___startIndex, Array_t * ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Byte[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7705 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Char[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7706 (Object_t * __this /* static, unused */, CharU5BU5D_t1_16* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Int16[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7707 (Object_t * __this /* static, unused */, Int16U5BU5D_t1_1694* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Int32[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7708 (Object_t * __this /* static, unused */, Int32U5BU5D_t1_275* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Int64[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7709 (Object_t * __this /* static, unused */, Int64U5BU5D_t1_1665* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Single[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7710 (Object_t * __this /* static, unused */, SingleU5BU5D_t1_1695* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Double[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7711 (Object_t * __this /* static, unused */, DoubleU5BU5D_t1_1666* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr[],System.Int32,System.IntPtr,System.Int32)
extern "C" void Marshal_Copy_m1_7712 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1_34* ___source, int32_t ___startIndex, IntPtr_t ___destination, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7713 (Object_t * __this /* static, unused */, IntPtr_t ___source, ByteU5BU5D_t1_109* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7714 (Object_t * __this /* static, unused */, IntPtr_t ___source, CharU5BU5D_t1_16* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Int16[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7715 (Object_t * __this /* static, unused */, IntPtr_t ___source, Int16U5BU5D_t1_1694* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Int32[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7716 (Object_t * __this /* static, unused */, IntPtr_t ___source, Int32U5BU5D_t1_275* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Int64[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7717 (Object_t * __this /* static, unused */, IntPtr_t ___source, Int64U5BU5D_t1_1665* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7718 (Object_t * __this /* static, unused */, IntPtr_t ___source, SingleU5BU5D_t1_1695* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Double[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7719 (Object_t * __this /* static, unused */, IntPtr_t ___source, DoubleU5BU5D_t1_1666* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.IntPtr[],System.Int32,System.Int32)
extern "C" void Marshal_Copy_m1_7720 (Object_t * __this /* static, unused */, IntPtr_t ___source, IntPtrU5BU5D_t1_34* ___destination, int32_t ___startIndex, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::CreateAggregatedObject(System.IntPtr,System.Object)
extern "C" IntPtr_t Marshal_CreateAggregatedObject_m1_7721 (Object_t * __this /* static, unused */, IntPtr_t ___pOuter, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::CreateWrapperOfType(System.Object,System.Type)
extern "C" Object_t * Marshal_CreateWrapperOfType_m1_7722 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::DestroyStructure(System.IntPtr,System.Type)
extern "C" void Marshal_DestroyStructure_m1_7723 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___structuretype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::FreeBSTR(System.IntPtr)
extern "C" void Marshal_FreeBSTR_m1_7724 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::FreeCoTaskMem(System.IntPtr)
extern "C" void Marshal_FreeCoTaskMem_m1_7725 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::FreeHGlobal(System.IntPtr)
extern "C" void Marshal_FreeHGlobal_m1_7726 (Object_t * __this /* static, unused */, IntPtr_t ___hglobal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ClearBSTR(System.IntPtr)
extern "C" void Marshal_ClearBSTR_m1_7727 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeBSTR(System.IntPtr)
extern "C" void Marshal_ZeroFreeBSTR_m1_7728 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ClearAnsi(System.IntPtr)
extern "C" void Marshal_ClearAnsi_m1_7729 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ClearUnicode(System.IntPtr)
extern "C" void Marshal_ClearUnicode_m1_7730 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeCoTaskMemAnsi(System.IntPtr)
extern "C" void Marshal_ZeroFreeCoTaskMemAnsi_m1_7731 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeCoTaskMemUnicode(System.IntPtr)
extern "C" void Marshal_ZeroFreeCoTaskMemUnicode_m1_7732 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeGlobalAllocAnsi(System.IntPtr)
extern "C" void Marshal_ZeroFreeGlobalAllocAnsi_m1_7733 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ZeroFreeGlobalAllocUnicode(System.IntPtr)
extern "C" void Marshal_ZeroFreeGlobalAllocUnicode_m1_7734 (Object_t * __this /* static, unused */, IntPtr_t ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Runtime.InteropServices.Marshal::GenerateGuidForType(System.Type)
extern "C" Guid_t1_319  Marshal_GenerateGuidForType_m1_7735 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::GenerateProgIdForType(System.Type)
extern "C" String_t* Marshal_GenerateProgIdForType_m1_7736 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetActiveObject(System.String)
extern "C" Object_t * Marshal_GetActiveObject_m1_7737 (Object_t * __this /* static, unused */, String_t* ___progID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetCCW(System.Object,System.Type)
extern "C" IntPtr_t Marshal_GetCCW_m1_7738 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___T, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetComInterfaceForObjectInternal(System.Object,System.Type)
extern "C" IntPtr_t Marshal_GetComInterfaceForObjectInternal_m1_7739 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___T, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetComInterfaceForObject(System.Object,System.Type)
extern "C" IntPtr_t Marshal_GetComInterfaceForObject_m1_7740 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___T, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetComInterfaceForObjectInContext(System.Object,System.Type)
extern "C" IntPtr_t Marshal_GetComInterfaceForObjectInContext_m1_7741 (Object_t * __this /* static, unused */, Object_t * ___o, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetComObjectData(System.Object,System.Object)
extern "C" Object_t * Marshal_GetComObjectData_m1_7742 (Object_t * __this /* static, unused */, Object_t * ___obj, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetComSlotForMethodInfoInternal(System.Reflection.MemberInfo)
extern "C" int32_t Marshal_GetComSlotForMethodInfoInternal_m1_7743 (Object_t * __this /* static, unused */, MemberInfo_t * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetComSlotForMethodInfo(System.Reflection.MemberInfo)
extern "C" int32_t Marshal_GetComSlotForMethodInfo_m1_7744 (Object_t * __this /* static, unused */, MemberInfo_t * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetEndComSlot(System.Type)
extern "C" int32_t Marshal_GetEndComSlot_m1_7745 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetExceptionCode()
extern "C" int32_t Marshal_GetExceptionCode_m1_7746 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetExceptionPointers()
extern "C" IntPtr_t Marshal_GetExceptionPointers_m1_7747 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetHINSTANCE(System.Reflection.Module)
extern "C" IntPtr_t Marshal_GetHINSTANCE_m1_7748 (Object_t * __this /* static, unused */, Module_t1_495 * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetHRForException(System.Exception)
extern "C" int32_t Marshal_GetHRForException_m1_7749 (Object_t * __this /* static, unused */, Exception_t1_33 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetHRForLastWin32Error()
extern "C" int32_t Marshal_GetHRForLastWin32Error_m1_7750 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIDispatchForObjectInternal(System.Object)
extern "C" IntPtr_t Marshal_GetIDispatchForObjectInternal_m1_7751 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIDispatchForObject(System.Object)
extern "C" IntPtr_t Marshal_GetIDispatchForObject_m1_7752 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIDispatchForObjectInContext(System.Object)
extern "C" IntPtr_t Marshal_GetIDispatchForObjectInContext_m1_7753 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetITypeInfoForType(System.Type)
extern "C" IntPtr_t Marshal_GetITypeInfoForType_m1_7754 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIUnknownForObjectInternal(System.Object)
extern "C" IntPtr_t Marshal_GetIUnknownForObjectInternal_m1_7755 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIUnknownForObject(System.Object)
extern "C" IntPtr_t Marshal_GetIUnknownForObject_m1_7756 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetIUnknownForObjectInContext(System.Object)
extern "C" IntPtr_t Marshal_GetIUnknownForObjectInContext_m1_7757 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetManagedThunkForUnmanagedMethodPtr(System.IntPtr,System.IntPtr,System.Int32)
extern "C" IntPtr_t Marshal_GetManagedThunkForUnmanagedMethodPtr_m1_7758 (Object_t * __this /* static, unused */, IntPtr_t ___pfnMethodToWrap, IntPtr_t ___pbSignature, int32_t ___cbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Runtime.InteropServices.Marshal::GetMethodInfoForComSlot(System.Type,System.Int32,System.Runtime.InteropServices.ComMemberType&)
extern "C" MemberInfo_t * Marshal_GetMethodInfoForComSlot_m1_7759 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___slot, int32_t* ___memberType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::GetNativeVariantForObject(System.Object,System.IntPtr)
extern "C" void Marshal_GetNativeVariantForObject_m1_7760 (Object_t * __this /* static, unused */, Object_t * ___obj, IntPtr_t ___pDstNativeVariant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetObjectForCCW(System.IntPtr)
extern "C" Object_t * Marshal_GetObjectForCCW_m1_7761 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetObjectForIUnknown(System.IntPtr)
extern "C" Object_t * Marshal_GetObjectForIUnknown_m1_7762 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetObjectForNativeVariant(System.IntPtr)
extern "C" Object_t * Marshal_GetObjectForNativeVariant_m1_7763 (Object_t * __this /* static, unused */, IntPtr_t ___pSrcNativeVariant, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.InteropServices.Marshal::GetObjectsForNativeVariants(System.IntPtr,System.Int32)
extern "C" ObjectU5BU5D_t1_272* Marshal_GetObjectsForNativeVariants_m1_7764 (Object_t * __this /* static, unused */, IntPtr_t ___aSrcNativeVariant, int32_t ___cVars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetStartComSlot(System.Type)
extern "C" int32_t Marshal_GetStartComSlot_m1_7765 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Runtime.InteropServices.Marshal::GetThreadFromFiberCookie(System.Int32)
extern "C" Thread_t1_901 * Marshal_GetThreadFromFiberCookie_m1_7766 (Object_t * __this /* static, unused */, int32_t ___cookie, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetTypedObjectForIUnknown(System.IntPtr,System.Type)
extern "C" Object_t * Marshal_GetTypedObjectForIUnknown_m1_7767 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.InteropServices.Marshal::GetTypeForITypeInfo(System.IntPtr)
extern "C" Type_t * Marshal_GetTypeForITypeInfo_m1_7768 (Object_t * __this /* static, unused */, IntPtr_t ___piTypeInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::GetTypeInfoName(System.Runtime.InteropServices.UCOMITypeInfo)
extern "C" String_t* Marshal_GetTypeInfoName_m1_7769 (Object_t * __this /* static, unused */, Object_t * ___pTI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::GetTypeInfoName(System.Runtime.InteropServices.ComTypes.ITypeInfo)
extern "C" String_t* Marshal_GetTypeInfoName_m1_7770 (Object_t * __this /* static, unused */, Object_t * ___typeInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Runtime.InteropServices.Marshal::GetTypeLibGuid(System.Runtime.InteropServices.UCOMITypeLib)
extern "C" Guid_t1_319  Marshal_GetTypeLibGuid_m1_7771 (Object_t * __this /* static, unused */, Object_t * ___pTLB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Runtime.InteropServices.Marshal::GetTypeLibGuid(System.Runtime.InteropServices.ComTypes.ITypeLib)
extern "C" Guid_t1_319  Marshal_GetTypeLibGuid_m1_7772 (Object_t * __this /* static, unused */, Object_t * ___typelib, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Runtime.InteropServices.Marshal::GetTypeLibGuidForAssembly(System.Reflection.Assembly)
extern "C" Guid_t1_319  Marshal_GetTypeLibGuidForAssembly_m1_7773 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___asm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetTypeLibLcid(System.Runtime.InteropServices.UCOMITypeLib)
extern "C" int32_t Marshal_GetTypeLibLcid_m1_7774 (Object_t * __this /* static, unused */, Object_t * ___pTLB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetTypeLibLcid(System.Runtime.InteropServices.ComTypes.ITypeLib)
extern "C" int32_t Marshal_GetTypeLibLcid_m1_7775 (Object_t * __this /* static, unused */, Object_t * ___typelib, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::GetTypeLibName(System.Runtime.InteropServices.UCOMITypeLib)
extern "C" String_t* Marshal_GetTypeLibName_m1_7776 (Object_t * __this /* static, unused */, Object_t * ___pTLB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::GetTypeLibName(System.Runtime.InteropServices.ComTypes.ITypeLib)
extern "C" String_t* Marshal_GetTypeLibName_m1_7777 (Object_t * __this /* static, unused */, Object_t * ___typelib, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::GetTypeLibVersionForAssembly(System.Reflection.Assembly,System.Int32&,System.Int32&)
extern "C" void Marshal_GetTypeLibVersionForAssembly_m1_7778 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___inputAssembly, int32_t* ___majorVersion, int32_t* ___minorVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::GetUniqueObjectForIUnknown(System.IntPtr)
extern "C" Object_t * Marshal_GetUniqueObjectForIUnknown_m1_7779 (Object_t * __this /* static, unused */, IntPtr_t ___unknown, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetUnmanagedThunkForManagedMethodPtr(System.IntPtr,System.IntPtr,System.Int32)
extern "C" IntPtr_t Marshal_GetUnmanagedThunkForManagedMethodPtr_m1_7780 (Object_t * __this /* static, unused */, IntPtr_t ___pfnMethodToWrap, IntPtr_t ___pbSignature, int32_t ___cbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.Marshal::IsComObject(System.Object)
extern "C" bool Marshal_IsComObject_m1_7781 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.Marshal::IsTypeVisibleFromCom(System.Type)
extern "C" bool Marshal_IsTypeVisibleFromCom_m1_7782 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::NumParamBytes(System.Reflection.MethodInfo)
extern "C" int32_t Marshal_NumParamBytes_m1_7783 (Object_t * __this /* static, unused */, MethodInfo_t * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::GetLastWin32Error()
extern "C" int32_t Marshal_GetLastWin32Error_m1_7784 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::OffsetOf(System.Type,System.String)
extern "C" IntPtr_t Marshal_OffsetOf_m1_7785 (Object_t * __this /* static, unused */, Type_t * ___t, String_t* ___fieldName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Prelink(System.Reflection.MethodInfo)
extern "C" void Marshal_Prelink_m1_7786 (Object_t * __this /* static, unused */, MethodInfo_t * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::PrelinkAll(System.Type)
extern "C" void Marshal_PrelinkAll_m1_7787 (Object_t * __this /* static, unused */, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAnsi(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringAnsi_m1_7788 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAnsi(System.IntPtr,System.Int32)
extern "C" String_t* Marshal_PtrToStringAnsi_m1_7789 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___len, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringAuto_m1_7790 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAuto(System.IntPtr,System.Int32)
extern "C" String_t* Marshal_PtrToStringAuto_m1_7791 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___len, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringUni(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringUni_m1_7792 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringUni(System.IntPtr,System.Int32)
extern "C" String_t* Marshal_PtrToStringUni_m1_7793 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___len, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringBSTR(System.IntPtr)
extern "C" String_t* Marshal_PtrToStringBSTR_m1_7794 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::PtrToStructure(System.IntPtr,System.Object)
extern "C" void Marshal_PtrToStructure_m1_7795 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Object_t * ___structure, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::PtrToStructure(System.IntPtr,System.Type)
extern "C" Object_t * Marshal_PtrToStructure_m1_7796 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___structureType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::QueryInterfaceInternal(System.IntPtr,System.Guid&,System.IntPtr&)
extern "C" int32_t Marshal_QueryInterfaceInternal_m1_7797 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, Guid_t1_319 * ___iid, IntPtr_t* ___ppv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::QueryInterface(System.IntPtr,System.Guid&,System.IntPtr&)
extern "C" int32_t Marshal_QueryInterface_m1_7798 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, Guid_t1_319 * ___iid, IntPtr_t* ___ppv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.IntPtr)
extern "C" uint8_t Marshal_ReadByte_m1_7799 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.IntPtr,System.Int32)
extern "C" uint8_t Marshal_ReadByte_m1_7800 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Runtime.InteropServices.Marshal::ReadByte(System.Object,System.Int32)
extern "C" uint8_t Marshal_ReadByte_m1_7801 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Runtime.InteropServices.Marshal::ReadInt16(System.IntPtr)
extern "C" int16_t Marshal_ReadInt16_m1_7802 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Runtime.InteropServices.Marshal::ReadInt16(System.IntPtr,System.Int32)
extern "C" int16_t Marshal_ReadInt16_m1_7803 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Runtime.InteropServices.Marshal::ReadInt16(System.Object,System.Int32)
extern "C" int16_t Marshal_ReadInt16_m1_7804 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr)
extern "C" int32_t Marshal_ReadInt32_m1_7805 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr,System.Int32)
extern "C" int32_t Marshal_ReadInt32_m1_7806 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.Object,System.Int32)
extern "C" int32_t Marshal_ReadInt32_m1_7807 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.InteropServices.Marshal::ReadInt64(System.IntPtr)
extern "C" int64_t Marshal_ReadInt64_m1_7808 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.InteropServices.Marshal::ReadInt64(System.IntPtr,System.Int32)
extern "C" int64_t Marshal_ReadInt64_m1_7809 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.InteropServices.Marshal::ReadInt64(System.Object,System.Int32)
extern "C" int64_t Marshal_ReadInt64_m1_7810 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::ReadIntPtr(System.IntPtr)
extern "C" IntPtr_t Marshal_ReadIntPtr_m1_7811 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::ReadIntPtr(System.IntPtr,System.Int32)
extern "C" IntPtr_t Marshal_ReadIntPtr_m1_7812 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::ReadIntPtr(System.Object,System.Int32)
extern "C" IntPtr_t Marshal_ReadIntPtr_m1_7813 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::ReAllocCoTaskMem(System.IntPtr,System.Int32)
extern "C" IntPtr_t Marshal_ReAllocCoTaskMem_m1_7814 (Object_t * __this /* static, unused */, IntPtr_t ___pv, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::ReAllocHGlobal(System.IntPtr,System.IntPtr)
extern "C" IntPtr_t Marshal_ReAllocHGlobal_m1_7815 (Object_t * __this /* static, unused */, IntPtr_t ___pv, IntPtr_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReleaseInternal(System.IntPtr)
extern "C" int32_t Marshal_ReleaseInternal_m1_7816 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::Release(System.IntPtr)
extern "C" int32_t Marshal_Release_m1_7817 (Object_t * __this /* static, unused */, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReleaseComObjectInternal(System.Object)
extern "C" int32_t Marshal_ReleaseComObjectInternal_m1_7818 (Object_t * __this /* static, unused */, Object_t * ___co, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReleaseComObject(System.Object)
extern "C" int32_t Marshal_ReleaseComObject_m1_7819 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ReleaseThreadCache()
extern "C" void Marshal_ReleaseThreadCache_m1_7820 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.Marshal::SetComObjectData(System.Object,System.Object,System.Object)
extern "C" bool Marshal_SetComObjectData_m1_7821 (Object_t * __this /* static, unused */, Object_t * ___obj, Object_t * ___key, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Object)
extern "C" int32_t Marshal_SizeOf_m1_7822 (Object_t * __this /* static, unused */, Object_t * ___structure, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Type)
extern "C" int32_t Marshal_SizeOf_m1_7823 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToBSTR(System.String)
extern "C" IntPtr_t Marshal_StringToBSTR_m1_7824 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToCoTaskMemAnsi(System.String)
extern "C" IntPtr_t Marshal_StringToCoTaskMemAnsi_m1_7825 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToCoTaskMemAuto(System.String)
extern "C" IntPtr_t Marshal_StringToCoTaskMemAuto_m1_7826 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToCoTaskMemUni(System.String)
extern "C" IntPtr_t Marshal_StringToCoTaskMemUni_m1_7827 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalAnsi(System.String)
extern "C" IntPtr_t Marshal_StringToHGlobalAnsi_m1_7828 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalAuto(System.String)
extern "C" IntPtr_t Marshal_StringToHGlobalAuto_m1_7829 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalUni(System.String)
extern "C" IntPtr_t Marshal_StringToHGlobalUni_m1_7830 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToBSTR(System.Security.SecureString)
extern "C" IntPtr_t Marshal_SecureStringToBSTR_m1_7831 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToCoTaskMemAnsi(System.Security.SecureString)
extern "C" IntPtr_t Marshal_SecureStringToCoTaskMemAnsi_m1_7832 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToCoTaskMemUnicode(System.Security.SecureString)
extern "C" IntPtr_t Marshal_SecureStringToCoTaskMemUnicode_m1_7833 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToGlobalAllocAnsi(System.Security.SecureString)
extern "C" IntPtr_t Marshal_SecureStringToGlobalAllocAnsi_m1_7834 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::SecureStringToGlobalAllocUnicode(System.Security.SecureString)
extern "C" IntPtr_t Marshal_SecureStringToGlobalAllocUnicode_m1_7835 (Object_t * __this /* static, unused */, SecureString_t1_1197 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::StructureToPtr(System.Object,System.IntPtr,System.Boolean)
extern "C" void Marshal_StructureToPtr_m1_7836 (Object_t * __this /* static, unused */, Object_t * ___structure, IntPtr_t ___ptr, bool ___fDeleteOld, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ThrowExceptionForHR(System.Int32)
extern "C" void Marshal_ThrowExceptionForHR_m1_7837 (Object_t * __this /* static, unused */, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::ThrowExceptionForHR(System.Int32,System.IntPtr)
extern "C" void Marshal_ThrowExceptionForHR_m1_7838 (Object_t * __this /* static, unused */, int32_t ___errorCode, IntPtr_t ___errorInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::UnsafeAddrOfPinnedArrayElement(System.Array,System.Int32)
extern "C" IntPtr_t Marshal_UnsafeAddrOfPinnedArrayElement_m1_7839 (Object_t * __this /* static, unused */, Array_t * ___arr, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.IntPtr,System.Byte)
extern "C" void Marshal_WriteByte_m1_7840 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, uint8_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.IntPtr,System.Int32,System.Byte)
extern "C" void Marshal_WriteByte_m1_7841 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, uint8_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteByte(System.Object,System.Int32,System.Byte)
extern "C" void Marshal_WriteByte_m1_7842 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, uint8_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Int16)
extern "C" void Marshal_WriteInt16_m1_7843 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int16_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Int32,System.Int16)
extern "C" void Marshal_WriteInt16_m1_7844 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, int16_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.Object,System.Int32,System.Int16)
extern "C" void Marshal_WriteInt16_m1_7845 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, int16_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Char)
extern "C" void Marshal_WriteInt16_m1_7846 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, uint16_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.IntPtr,System.Int32,System.Char)
extern "C" void Marshal_WriteInt16_m1_7847 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, uint16_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt16(System.Object,System.Int32,System.Char)
extern "C" void Marshal_WriteInt16_m1_7848 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, uint16_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt32(System.IntPtr,System.Int32)
extern "C" void Marshal_WriteInt32_m1_7849 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt32(System.IntPtr,System.Int32,System.Int32)
extern "C" void Marshal_WriteInt32_m1_7850 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt32(System.Object,System.Int32,System.Int32)
extern "C" void Marshal_WriteInt32_m1_7851 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt64(System.IntPtr,System.Int64)
extern "C" void Marshal_WriteInt64_m1_7852 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int64_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt64(System.IntPtr,System.Int32,System.Int64)
extern "C" void Marshal_WriteInt64_m1_7853 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, int64_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteInt64(System.Object,System.Int32,System.Int64)
extern "C" void Marshal_WriteInt64_m1_7854 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, int64_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteIntPtr(System.IntPtr,System.IntPtr)
extern "C" void Marshal_WriteIntPtr_m1_7855 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, IntPtr_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteIntPtr(System.IntPtr,System.Int32,System.IntPtr)
extern "C" void Marshal_WriteIntPtr_m1_7856 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, int32_t ___ofs, IntPtr_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::WriteIntPtr(System.Object,System.Int32,System.IntPtr)
extern "C" void Marshal_WriteIntPtr_m1_7857 (Object_t * __this /* static, unused */, Object_t * ___ptr, int32_t ___ofs, IntPtr_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.InteropServices.Marshal::GetExceptionForHR(System.Int32)
extern "C" Exception_t1_33 * Marshal_GetExceptionForHR_m1_7858 (Object_t * __this /* static, unused */, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.InteropServices.Marshal::GetExceptionForHR(System.Int32,System.IntPtr)
extern "C" Exception_t1_33 * Marshal_GetExceptionForHR_m1_7859 (Object_t * __this /* static, unused */, int32_t ___errorCode, IntPtr_t ___errorInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::FinalReleaseComObject(System.Object)
extern "C" int32_t Marshal_FinalReleaseComObject_m1_7860 (Object_t * __this /* static, unused */, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Runtime.InteropServices.Marshal::GetDelegateForFunctionPointerInternal(System.IntPtr,System.Type)
extern "C" Delegate_t1_22 * Marshal_GetDelegateForFunctionPointerInternal_m1_7861 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Runtime.InteropServices.Marshal::GetDelegateForFunctionPointer(System.IntPtr,System.Type)
extern "C" Delegate_t1_22 * Marshal_GetDelegateForFunctionPointer_m1_7862 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetFunctionPointerForDelegateInternal(System.Delegate)
extern "C" IntPtr_t Marshal_GetFunctionPointerForDelegateInternal_m1_7863 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::GetFunctionPointerForDelegate(System.Delegate)
extern "C" IntPtr_t Marshal_GetFunctionPointerForDelegate_m1_7864 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
