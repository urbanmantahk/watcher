﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Queue
struct Queue_t1_298;

#include "mscorlib_System_Collections_Queue.h"

// System.Collections.Queue/SyncQueue
struct  SyncQueue_t1_297  : public Queue_t1_298
{
	// System.Collections.Queue System.Collections.Queue/SyncQueue::queue
	Queue_t1_298 * ___queue_6;
};
