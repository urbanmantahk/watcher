﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_CommonAcl.h"

// System.Security.AccessControl.DiscretionaryAcl
struct  DiscretionaryAcl_t1_1134  : public CommonAcl_t1_1124
{
};
