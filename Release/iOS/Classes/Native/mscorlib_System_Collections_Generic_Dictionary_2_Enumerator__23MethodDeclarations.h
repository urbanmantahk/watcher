﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_27716(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2757 *, Dictionary_2_t1_1911 *, const MethodInfo*))Enumerator__ctor_m1_15990_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_27717(__this, method) (( Object_t * (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_27718(__this, method) (( void (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_27719(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_15993_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_27720(__this, method) (( Object_t * (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_15994_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_27721(__this, method) (( Object_t * (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_15995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::MoveNext()
#define Enumerator_MoveNext_m1_27722(__this, method) (( bool (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_MoveNext_m1_15996_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::get_Current()
#define Enumerator_get_Current_m1_27723(__this, method) (( KeyValuePair_2_t1_2754  (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_get_Current_m1_15997_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_27724(__this, method) (( String_t* (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_15998_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_27725(__this, method) (( AlertDialogCompletion_t8_300 * (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_15999_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::Reset()
#define Enumerator_Reset_m1_27726(__this, method) (( void (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_Reset_m1_16000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::VerifyState()
#define Enumerator_VerifyState_m1_27727(__this, method) (( void (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_VerifyState_m1_16001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_27728(__this, method) (( void (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_16002_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>::Dispose()
#define Enumerator_Dispose_m1_27729(__this, method) (( void (*) (Enumerator_t1_2757 *, const MethodInfo*))Enumerator_Dispose_m1_16003_gshared)(__this, method)
