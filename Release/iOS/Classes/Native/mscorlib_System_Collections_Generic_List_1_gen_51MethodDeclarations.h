﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t1_2083;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t1_2782;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_1749;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1_2781;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t1_1753;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t1_1751;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct IComparer_1_t1_2783;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t1_2089;
// System.Action`1<System.Reflection.CustomAttributeNamedArgument>
struct Action_1_t1_2090;
// System.Comparison`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparison_1_t1_2091;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void List_1__ctor_m1_16587_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1__ctor_m1_16587(__this, method) (( void (*) (List_1_t1_2083 *, const MethodInfo*))List_1__ctor_m1_16587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_16588_gshared (List_1_t1_2083 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_16588(__this, ___collection, method) (( void (*) (List_1_t1_2083 *, Object_t*, const MethodInfo*))List_1__ctor_m1_16588_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_16589_gshared (List_1_t1_2083 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_16589(__this, ___capacity, method) (( void (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1__ctor_m1_16589_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_16590_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_16590(__this, ___data, ___size, method) (( void (*) (List_1_t1_2083 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, int32_t, const MethodInfo*))List_1__ctor_m1_16590_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C" void List_1__cctor_m1_16591_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_16591(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_16591_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_16592_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_16592(__this, method) (( Object_t* (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_16592_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_16593_gshared (List_1_t1_2083 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_16593(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_2083 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_16593_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_16594_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_16594(__this, method) (( Object_t * (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_16594_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_16595_gshared (List_1_t1_2083 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_16595(__this, ___item, method) (( int32_t (*) (List_1_t1_2083 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_16595_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_16596_gshared (List_1_t1_2083 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_16596(__this, ___item, method) (( bool (*) (List_1_t1_2083 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_16596_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_16597_gshared (List_1_t1_2083 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_16597(__this, ___item, method) (( int32_t (*) (List_1_t1_2083 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_16597_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_16598_gshared (List_1_t1_2083 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_16598(__this, ___index, ___item, method) (( void (*) (List_1_t1_2083 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_16598_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_16599_gshared (List_1_t1_2083 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_16599(__this, ___item, method) (( void (*) (List_1_t1_2083 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_16599_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16600_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16600(__this, method) (( bool (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16600_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_16601_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_16601(__this, method) (( bool (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_16601_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_16602_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_16602(__this, method) (( Object_t * (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_16602_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_16603_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_16603(__this, method) (( bool (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_16603_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_16604_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_16604(__this, method) (( bool (*) (List_1_t1_2083 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_16604_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_16605_gshared (List_1_t1_2083 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_16605(__this, ___index, method) (( Object_t * (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_16605_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_16606_gshared (List_1_t1_2083 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_16606(__this, ___index, ___value, method) (( void (*) (List_1_t1_2083 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_16606_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void List_1_Add_m1_16607_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_Add_m1_16607(__this, ___item, method) (( void (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_Add_m1_16607_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_16608_gshared (List_1_t1_2083 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_16608(__this, ___newCount, method) (( void (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_16608_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_16609_gshared (List_1_t1_2083 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_16609(__this, ___idx, ___count, method) (( void (*) (List_1_t1_2083 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_16609_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_16610_gshared (List_1_t1_2083 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_16610(__this, ___collection, method) (( void (*) (List_1_t1_2083 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_16610_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_16611_gshared (List_1_t1_2083 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_16611(__this, ___enumerable, method) (( void (*) (List_1_t1_2083 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_16611_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_16612_gshared (List_1_t1_2083 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_16612(__this, ___collection, method) (( void (*) (List_1_t1_2083 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_16612_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_1751 * List_1_AsReadOnly_m1_16613_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_16613(__this, method) (( ReadOnlyCollection_1_t1_1751 * (*) (List_1_t1_2083 *, const MethodInfo*))List_1_AsReadOnly_m1_16613_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_16614_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_16614(__this, ___item, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_BinarySearch_m1_16614_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_16615_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_16615(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_16615_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_16616_gshared (List_1_t1_2083 * __this, int32_t ___index, int32_t ___count, CustomAttributeNamedArgument_t1_593  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_16616(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, int32_t, CustomAttributeNamedArgument_t1_593 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_16616_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void List_1_Clear_m1_16617_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_Clear_m1_16617(__this, method) (( void (*) (List_1_t1_2083 *, const MethodInfo*))List_1_Clear_m1_16617_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool List_1_Contains_m1_16618_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_Contains_m1_16618(__this, ___item, method) (( bool (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_Contains_m1_16618_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_16619_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_16619(__this, ___array, method) (( void (*) (List_1_t1_2083 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, const MethodInfo*))List_1_CopyTo_m1_16619_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_16620_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_16620(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_2083 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, int32_t, const MethodInfo*))List_1_CopyTo_m1_16620_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_16621_gshared (List_1_t1_2083 * __this, int32_t ___index, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_16621(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_2083 *, int32_t, CustomAttributeNamedArgumentU5BU5D_t1_1749*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_16621_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_16622_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_16622(__this, ___match, method) (( bool (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_Exists_m1_16622_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeNamedArgument_t1_593  List_1_Find_m1_16623_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_Find_m1_16623(__this, ___match, method) (( CustomAttributeNamedArgument_t1_593  (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_Find_m1_16623_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_16624_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_16624(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2089 *, const MethodInfo*))List_1_CheckMatch_m1_16624_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_2083 * List_1_FindAll_m1_16625_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_16625(__this, ___match, method) (( List_1_t1_2083 * (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindAll_m1_16625_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_2083 * List_1_FindAllStackBits_m1_16626_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_16626(__this, ___match, method) (( List_1_t1_2083 * (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindAllStackBits_m1_16626_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_2083 * List_1_FindAllList_m1_16627_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_16627(__this, ___match, method) (( List_1_t1_2083 * (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindAllList_m1_16627_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_16628_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_16628(__this, ___match, method) (( int32_t (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindIndex_m1_16628_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_16629_gshared (List_1_t1_2083 * __this, int32_t ___startIndex, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_16629(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindIndex_m1_16629_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_16630_gshared (List_1_t1_2083 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_16630(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, int32_t, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindIndex_m1_16630_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_16631_gshared (List_1_t1_2083 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_16631(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, int32_t, Predicate_1_t1_2089 *, const MethodInfo*))List_1_GetIndex_m1_16631_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindLast(System.Predicate`1<T>)
extern "C" CustomAttributeNamedArgument_t1_593  List_1_FindLast_m1_16632_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_16632(__this, ___match, method) (( CustomAttributeNamedArgument_t1_593  (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindLast_m1_16632_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_16633_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_16633(__this, ___match, method) (( int32_t (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindLastIndex_m1_16633_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_16634_gshared (List_1_t1_2083 * __this, int32_t ___startIndex, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_16634(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindLastIndex_m1_16634_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_16635_gshared (List_1_t1_2083 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_16635(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, int32_t, Predicate_1_t1_2089 *, const MethodInfo*))List_1_FindLastIndex_m1_16635_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_16636_gshared (List_1_t1_2083 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_16636(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2083 *, int32_t, int32_t, Predicate_1_t1_2089 *, const MethodInfo*))List_1_GetLastIndex_m1_16636_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_16637_gshared (List_1_t1_2083 * __this, Action_1_t1_2090 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_16637(__this, ___action, method) (( void (*) (List_1_t1_2083 *, Action_1_t1_2090 *, const MethodInfo*))List_1_ForEach_m1_16637_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Enumerator_t1_2084  List_1_GetEnumerator_m1_16638_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_16638(__this, method) (( Enumerator_t1_2084  (*) (List_1_t1_2083 *, const MethodInfo*))List_1_GetEnumerator_m1_16638_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_2083 * List_1_GetRange_m1_16639_gshared (List_1_t1_2083 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_16639(__this, ___index, ___count, method) (( List_1_t1_2083 * (*) (List_1_t1_2083 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_16639_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_16640_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_16640(__this, ___item, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_IndexOf_m1_16640_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_16641_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_16641(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , int32_t, const MethodInfo*))List_1_IndexOf_m1_16641_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_16642_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_16642(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_16642_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_16643_gshared (List_1_t1_2083 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_16643(__this, ___start, ___delta, method) (( void (*) (List_1_t1_2083 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_16643_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_16644_gshared (List_1_t1_2083 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_16644(__this, ___index, method) (( void (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_16644_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_16645_gshared (List_1_t1_2083 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_Insert_m1_16645(__this, ___index, ___item, method) (( void (*) (List_1_t1_2083 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_Insert_m1_16645_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_16646_gshared (List_1_t1_2083 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_16646(__this, ___collection, method) (( void (*) (List_1_t1_2083 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_16646_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_16647_gshared (List_1_t1_2083 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_16647(__this, ___index, ___collection, method) (( void (*) (List_1_t1_2083 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_16647_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_16648_gshared (List_1_t1_2083 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_16648(__this, ___index, ___collection, method) (( void (*) (List_1_t1_2083 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_16648_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_16649_gshared (List_1_t1_2083 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_16649(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_2083 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_16649_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_16650_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_16650(__this, ___item, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_LastIndexOf_m1_16650_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_16651_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_16651(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_16651_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_16652_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_16652(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_16652_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool List_1_Remove_m1_16653_gshared (List_1_t1_2083 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define List_1_Remove_m1_16653(__this, ___item, method) (( bool (*) (List_1_t1_2083 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_Remove_m1_16653_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_16654_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_16654(__this, ___match, method) (( int32_t (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_RemoveAll_m1_16654_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_16655_gshared (List_1_t1_2083 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_16655(__this, ___index, method) (( void (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_16655_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_16656_gshared (List_1_t1_2083 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_16656(__this, ___index, ___count, method) (( void (*) (List_1_t1_2083 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_16656_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Reverse()
extern "C" void List_1_Reverse_m1_16657_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_16657(__this, method) (( void (*) (List_1_t1_2083 *, const MethodInfo*))List_1_Reverse_m1_16657_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_16658_gshared (List_1_t1_2083 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_16658(__this, ___index, ___count, method) (( void (*) (List_1_t1_2083 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_16658_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort()
extern "C" void List_1_Sort_m1_16659_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_Sort_m1_16659(__this, method) (( void (*) (List_1_t1_2083 *, const MethodInfo*))List_1_Sort_m1_16659_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_16660_gshared (List_1_t1_2083 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_16660(__this, ___comparer, method) (( void (*) (List_1_t1_2083 *, Object_t*, const MethodInfo*))List_1_Sort_m1_16660_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_16661_gshared (List_1_t1_2083 * __this, Comparison_1_t1_2091 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_16661(__this, ___comparison, method) (( void (*) (List_1_t1_2083 *, Comparison_1_t1_2091 *, const MethodInfo*))List_1_Sort_m1_16661_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_16662_gshared (List_1_t1_2083 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_16662(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_2083 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_16662_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C" CustomAttributeNamedArgumentU5BU5D_t1_1749* List_1_ToArray_m1_16663_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_16663(__this, method) (( CustomAttributeNamedArgumentU5BU5D_t1_1749* (*) (List_1_t1_2083 *, const MethodInfo*))List_1_ToArray_m1_16663_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_16664_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_16664(__this, method) (( void (*) (List_1_t1_2083 *, const MethodInfo*))List_1_TrimExcess_m1_16664_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_16665_gshared (List_1_t1_2083 * __this, Predicate_1_t1_2089 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_16665(__this, ___match, method) (( bool (*) (List_1_t1_2083 *, Predicate_1_t1_2089 *, const MethodInfo*))List_1_TrueForAll_m1_16665_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_16666_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_16666(__this, method) (( int32_t (*) (List_1_t1_2083 *, const MethodInfo*))List_1_get_Capacity_m1_16666_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_16667_gshared (List_1_t1_2083 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_16667(__this, ___value, method) (( void (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_16667_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m1_16668_gshared (List_1_t1_2083 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_16668(__this, method) (( int32_t (*) (List_1_t1_2083 *, const MethodInfo*))List_1_get_Count_m1_16668_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_593  List_1_get_Item_m1_16669_gshared (List_1_t1_2083 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_16669(__this, ___index, method) (( CustomAttributeNamedArgument_t1_593  (*) (List_1_t1_2083 *, int32_t, const MethodInfo*))List_1_get_Item_m1_16669_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_16670_gshared (List_1_t1_2083 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_16670(__this, ___index, ___value, method) (( void (*) (List_1_t1_2083 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))List_1_set_Item_m1_16670_gshared)(__this, ___index, ___value, method)
