﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t3_255;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t1_2211;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2186;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1_2777;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Array
struct Array_t;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t1_1937;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "System_System_Collections_Generic_SortedList_2_EnumeratorMod.h"

// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.ctor()
extern "C" void SortedList_2__ctor_m3_1867_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2__ctor_m3_1867(__this, method) (( void (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2__ctor_m3_1867_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.ctor(System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern "C" void SortedList_2__ctor_m3_1868_gshared (SortedList_2_t3_255 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define SortedList_2__ctor_m3_1868(__this, ___capacity, ___comparer, method) (( void (*) (SortedList_2_t3_255 *, int32_t, Object_t*, const MethodInfo*))SortedList_2__ctor_m3_1868_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::.cctor()
extern "C" void SortedList_2__cctor_m3_1869_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SortedList_2__cctor_m3_1869(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SortedList_2__cctor_m3_1869_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_1870_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_1870(__this, method) (( bool (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_1870_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_1871_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_1871(__this, method) (( Object_t * (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_1871_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_1872_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_1872(__this, method) (( bool (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_1872_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_1873_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_1873(__this, method) (( bool (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_1873_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Item_m3_1874_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Item_m3_1874(__this, ___key, method) (( Object_t * (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Item_m3_1874_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_set_Item_m3_1875_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_set_Item_m3_1875(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_255 *, Object_t *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_set_Item_m3_1875_gshared)(__this, ___key, ___value, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Keys_m3_1876_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Keys_m3_1876(__this, method) (( Object_t * (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Keys_m3_1876_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Values_m3_1877_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_get_Values_m3_1877(__this, method) (( Object_t * (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_get_Values_m3_1877_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_1878_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_1878(__this, method) (( Object_t* (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_1878_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_1879_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_1879(__this, method) (( Object_t* (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_1879_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_1880_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_1880(__this, method) (( bool (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_1880_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Clear()
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_1881_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_1881(__this, method) (( void (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_1881_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_1882_gshared (SortedList_2_t3_255 * __this, KeyValuePair_2U5BU5D_t1_2186* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_1882(__this, ___array, ___arrayIndex, method) (( void (*) (SortedList_2_t3_255 *, KeyValuePair_2U5BU5D_t1_2186*, int32_t, const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_1882_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_1883_gshared (SortedList_2_t3_255 * __this, KeyValuePair_2_t1_2015  ___keyValuePair, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_1883(__this, ___keyValuePair, method) (( void (*) (SortedList_2_t3_255 *, KeyValuePair_2_t1_2015 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_1883_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_1884_gshared (SortedList_2_t3_255 * __this, KeyValuePair_2_t1_2015  ___keyValuePair, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_1884(__this, ___keyValuePair, method) (( bool (*) (SortedList_2_t3_255 *, KeyValuePair_2_t1_2015 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_1884_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_1885_gshared (SortedList_2_t3_255 * __this, KeyValuePair_2_t1_2015  ___keyValuePair, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_1885(__this, ___keyValuePair, method) (( bool (*) (SortedList_2_t3_255 *, KeyValuePair_2_t1_2015 , const MethodInfo*))SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_1885_gshared)(__this, ___keyValuePair, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_1886_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_1886(__this, method) (( Object_t* (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_1886_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_1887_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_1887(__this, method) (( Object_t * (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_1887_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_Add_m3_1888_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Add_m3_1888(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_255 *, Object_t *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Add_m3_1888_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool SortedList_2_System_Collections_IDictionary_Contains_m3_1889_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Contains_m3_1889(__this, ___key, method) (( bool (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Contains_m3_1889_gshared)(__this, ___key, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_1890_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_1890(__this, method) (( Object_t * (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_1890_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_Remove_m3_1891_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_System_Collections_IDictionary_Remove_m3_1891(__this, ___key, method) (( void (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_System_Collections_IDictionary_Remove_m3_1891_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void SortedList_2_System_Collections_ICollection_CopyTo_m3_1892_gshared (SortedList_2_t3_255 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define SortedList_2_System_Collections_ICollection_CopyTo_m3_1892(__this, ___array, ___arrayIndex, method) (( void (*) (SortedList_2_t3_255 *, Array_t *, int32_t, const MethodInfo*))SortedList_2_System_Collections_ICollection_CopyTo_m3_1892_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Count()
extern "C" int32_t SortedList_2_get_Count_m3_1893_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_get_Count_m3_1893(__this, method) (( int32_t (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_get_Count_m3_1893_gshared)(__this, method)
// TValue System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * SortedList_2_get_Item_m3_1894_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_get_Item_m3_1894(__this, ___key, method) (( Object_t * (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_get_Item_m3_1894_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void SortedList_2_set_Item_m3_1895_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_set_Item_m3_1895(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_255 *, Object_t *, Object_t *, const MethodInfo*))SortedList_2_set_Item_m3_1895_gshared)(__this, ___key, ___value, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Capacity()
extern "C" int32_t SortedList_2_get_Capacity_m3_1896_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_get_Capacity_m3_1896(__this, method) (( int32_t (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_get_Capacity_m3_1896_gshared)(__this, method)
// System.Collections.Generic.IList`1<TKey> System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* SortedList_2_get_Keys_m3_1897_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_get_Keys_m3_1897(__this, method) (( Object_t* (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_get_Keys_m3_1897_gshared)(__this, method)
// System.Collections.Generic.IList`1<TValue> System.Collections.Generic.SortedList`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* SortedList_2_get_Values_m3_1898_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_get_Values_m3_1898(__this, method) (( Object_t* (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_get_Values_m3_1898_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void SortedList_2_Add_m3_1899_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_Add_m3_1899(__this, ___key, ___value, method) (( void (*) (SortedList_2_t3_255 *, Object_t *, Object_t *, const MethodInfo*))SortedList_2_Add_m3_1899_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C" bool SortedList_2_ContainsKey_m3_1900_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_ContainsKey_m3_1900(__this, ___key, method) (( bool (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_ContainsKey_m3_1900_gshared)(__this, ___key, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* SortedList_2_GetEnumerator_m3_1901_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_GetEnumerator_m3_1901(__this, method) (( Object_t* (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_GetEnumerator_m3_1901_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool SortedList_2_Remove_m3_1902_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_Remove_m3_1902(__this, ___key, method) (( bool (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_Remove_m3_1902_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::Clear()
extern "C" void SortedList_2_Clear_m3_1903_gshared (SortedList_2_t3_255 * __this, const MethodInfo* method);
#define SortedList_2_Clear_m3_1903(__this, method) (( void (*) (SortedList_2_t3_255 *, const MethodInfo*))SortedList_2_Clear_m3_1903_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::RemoveAt(System.Int32)
extern "C" void SortedList_2_RemoveAt_m3_1904_gshared (SortedList_2_t3_255 * __this, int32_t ___index, const MethodInfo* method);
#define SortedList_2_RemoveAt_m3_1904(__this, ___index, method) (( void (*) (SortedList_2_t3_255 *, int32_t, const MethodInfo*))SortedList_2_RemoveAt_m3_1904_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::IndexOfKey(TKey)
extern "C" int32_t SortedList_2_IndexOfKey_m3_1905_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_IndexOfKey_m3_1905(__this, ___key, method) (( int32_t (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_IndexOfKey_m3_1905_gshared)(__this, ___key, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::IndexOfValue(TValue)
extern "C" int32_t SortedList_2_IndexOfValue_m3_1906_gshared (SortedList_2_t3_255 * __this, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_IndexOfValue_m3_1906(__this, ___value, method) (( int32_t (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_IndexOfValue_m3_1906_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.SortedList`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool SortedList_2_TryGetValue_m3_1907_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define SortedList_2_TryGetValue_m3_1907(__this, ___key, ___value, method) (( bool (*) (SortedList_2_t3_255 *, Object_t *, Object_t **, const MethodInfo*))SortedList_2_TryGetValue_m3_1907_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::EnsureCapacity(System.Int32,System.Int32)
extern "C" void SortedList_2_EnsureCapacity_m3_1908_gshared (SortedList_2_t3_255 * __this, int32_t ___n, int32_t ___free, const MethodInfo* method);
#define SortedList_2_EnsureCapacity_m3_1908(__this, ___n, ___free, method) (( void (*) (SortedList_2_t3_255 *, int32_t, int32_t, const MethodInfo*))SortedList_2_EnsureCapacity_m3_1908_gshared)(__this, ___n, ___free, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::PutImpl(TKey,TValue,System.Boolean)
extern "C" void SortedList_2_PutImpl_m3_1909_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, Object_t * ___value, bool ___overwrite, const MethodInfo* method);
#define SortedList_2_PutImpl_m3_1909(__this, ___key, ___value, ___overwrite, method) (( void (*) (SortedList_2_t3_255 *, Object_t *, Object_t *, bool, const MethodInfo*))SortedList_2_PutImpl_m3_1909_gshared)(__this, ___key, ___value, ___overwrite, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::Init(System.Collections.Generic.IComparer`1<TKey>,System.Int32,System.Boolean)
extern "C" void SortedList_2_Init_m3_1910_gshared (SortedList_2_t3_255 * __this, Object_t* ___comparer, int32_t ___capacity, bool ___forceSize, const MethodInfo* method);
#define SortedList_2_Init_m3_1910(__this, ___comparer, ___capacity, ___forceSize, method) (( void (*) (SortedList_2_t3_255 *, Object_t*, int32_t, bool, const MethodInfo*))SortedList_2_Init_m3_1910_gshared)(__this, ___comparer, ___capacity, ___forceSize, method)
// System.Void System.Collections.Generic.SortedList`2<System.Object,System.Object>::CopyToArray(System.Array,System.Int32,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C" void SortedList_2_CopyToArray_m3_1911_gshared (SortedList_2_t3_255 * __this, Array_t * ___arr, int32_t ___i, int32_t ___mode, const MethodInfo* method);
#define SortedList_2_CopyToArray_m3_1911(__this, ___arr, ___i, ___mode, method) (( void (*) (SortedList_2_t3_255 *, Array_t *, int32_t, int32_t, const MethodInfo*))SortedList_2_CopyToArray_m3_1911_gshared)(__this, ___arr, ___i, ___mode, method)
// System.Int32 System.Collections.Generic.SortedList`2<System.Object,System.Object>::Find(TKey)
extern "C" int32_t SortedList_2_Find_m3_1912_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_Find_m3_1912(__this, ___key, method) (( int32_t (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_Find_m3_1912_gshared)(__this, ___key, method)
// TKey System.Collections.Generic.SortedList`2<System.Object,System.Object>::ToKey(System.Object)
extern "C" Object_t * SortedList_2_ToKey_m3_1913_gshared (SortedList_2_t3_255 * __this, Object_t * ___key, const MethodInfo* method);
#define SortedList_2_ToKey_m3_1913(__this, ___key, method) (( Object_t * (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_ToKey_m3_1913_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.SortedList`2<System.Object,System.Object>::ToValue(System.Object)
extern "C" Object_t * SortedList_2_ToValue_m3_1914_gshared (SortedList_2_t3_255 * __this, Object_t * ___value, const MethodInfo* method);
#define SortedList_2_ToValue_m3_1914(__this, ___value, method) (( Object_t * (*) (SortedList_2_t3_255 *, Object_t *, const MethodInfo*))SortedList_2_ToValue_m3_1914_gshared)(__this, ___value, method)
// TKey System.Collections.Generic.SortedList`2<System.Object,System.Object>::KeyAt(System.Int32)
extern "C" Object_t * SortedList_2_KeyAt_m3_1915_gshared (SortedList_2_t3_255 * __this, int32_t ___index, const MethodInfo* method);
#define SortedList_2_KeyAt_m3_1915(__this, ___index, method) (( Object_t * (*) (SortedList_2_t3_255 *, int32_t, const MethodInfo*))SortedList_2_KeyAt_m3_1915_gshared)(__this, ___index, method)
// TValue System.Collections.Generic.SortedList`2<System.Object,System.Object>::ValueAt(System.Int32)
extern "C" Object_t * SortedList_2_ValueAt_m3_1916_gshared (SortedList_2_t3_255 * __this, int32_t ___index, const MethodInfo* method);
#define SortedList_2_ValueAt_m3_1916(__this, ___index, method) (( Object_t * (*) (SortedList_2_t3_255 *, int32_t, const MethodInfo*))SortedList_2_ValueAt_m3_1916_gshared)(__this, ___index, method)
