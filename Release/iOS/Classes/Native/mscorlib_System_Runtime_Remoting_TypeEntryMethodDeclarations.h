﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.TypeEntry
struct TypeEntry_t1_1009;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.TypeEntry::.ctor()
extern "C" void TypeEntry__ctor_m1_9307 (TypeEntry_t1_1009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
extern "C" String_t* TypeEntry_get_AssemblyName_m1_9308 (TypeEntry_t1_1009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.TypeEntry::set_AssemblyName(System.String)
extern "C" void TypeEntry_set_AssemblyName_m1_9309 (TypeEntry_t1_1009 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
extern "C" String_t* TypeEntry_get_TypeName_m1_9310 (TypeEntry_t1_1009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.TypeEntry::set_TypeName(System.String)
extern "C" void TypeEntry_set_TypeName_m1_9311 (TypeEntry_t1_1009 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
