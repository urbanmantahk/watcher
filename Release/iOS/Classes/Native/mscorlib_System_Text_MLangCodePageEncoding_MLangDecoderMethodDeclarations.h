﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.MLangCodePageEncoding/MLangDecoder
struct MLangDecoder_t1_1441;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Text.MLangCodePageEncoding/MLangDecoder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MLangDecoder__ctor_m1_12406 (MLangDecoder_t1_1441 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.MLangCodePageEncoding/MLangDecoder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MLangDecoder_GetObjectData_m1_12407 (MLangDecoder_t1_1441 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.MLangCodePageEncoding/MLangDecoder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern "C" Object_t * MLangDecoder_GetRealObject_m1_12408 (MLangDecoder_t1_1441 * __this, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
