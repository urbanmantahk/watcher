﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Globalization.StringInfo
struct  StringInfo_t1_386  : public Object_t
{
	// System.String System.Globalization.StringInfo::s
	String_t* ___s_0;
	// System.Int32 System.Globalization.StringInfo::length
	int32_t ___length_1;
};
