﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Pointer
struct Pointer_t1_629;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Void.h"

// System.Void System.Reflection.Pointer::.ctor()
extern "C" void Pointer__ctor_m1_7254 (Pointer_t1_629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Pointer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m1_7255 (Pointer_t1_629 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Pointer::Box(System.Void*,System.Type)
extern "C" Object_t * Pointer_Box_m1_7256 (Object_t * __this /* static, unused */, void* ___ptr, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.Reflection.Pointer::Unbox(System.Object)
extern "C" void* Pointer_Unbox_m1_7257 (Object_t * __this /* static, unused */, Object_t * ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
