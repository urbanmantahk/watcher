﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;

#include "mscorlib_System_Globalization_Calendar.h"

// System.Globalization.EastAsianLunisolarCalendar
struct  EastAsianLunisolarCalendar_t1_358  : public Calendar_t1_338
{
	// System.Globalization.CCEastAsianLunisolarEraHandler System.Globalization.EastAsianLunisolarCalendar::M_EraHandler
	CCEastAsianLunisolarEraHandler_t1_355 * ___M_EraHandler_7;
};
