﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
#include "mscorlib_System_Runtime_InteropServices_LayoutKind.h"

// System.Runtime.InteropServices.StructLayoutAttribute
struct  StructLayoutAttribute_t1_64  : public Attribute_t1_2
{
	// System.Runtime.InteropServices.CharSet System.Runtime.InteropServices.StructLayoutAttribute::CharSet
	int32_t ___CharSet_0;
	// System.Int32 System.Runtime.InteropServices.StructLayoutAttribute::Pack
	int32_t ___Pack_1;
	// System.Int32 System.Runtime.InteropServices.StructLayoutAttribute::Size
	int32_t ___Size_2;
	// System.Runtime.InteropServices.LayoutKind System.Runtime.InteropServices.StructLayoutAttribute::lkind
	int32_t ___lkind_3;
};
