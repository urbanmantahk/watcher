﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Collections.IList
struct IList_t1_262;
// System.Security.Policy.CodeGroup
struct CodeGroup_t1_1339;
// System.Security.Policy.StrongName
struct StrongName_t1_1363;
// System.Security.Policy.StrongNameMembershipCondition
struct StrongNameMembershipCondition_t1_1364;
// System.Security.NamedPermissionSet
struct NamedPermissionSet_t1_1344;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Reflection.Assembly
struct Assembly_t1_467;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_PolicyLevelType.h"

// System.Void System.Security.Policy.PolicyLevel::.ctor(System.String,System.Security.PolicyLevelType)
extern "C" void PolicyLevel__ctor_m1_11586 (PolicyLevel_t1_1357 * __this, String_t* ___label, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::LoadFromFile(System.String)
extern "C" void PolicyLevel_LoadFromFile_m1_11587 (PolicyLevel_t1_1357 * __this, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::LoadFromString(System.String)
extern "C" void PolicyLevel_LoadFromString_m1_11588 (PolicyLevel_t1_1357 * __this, String_t* ___xml, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.PolicyLevel::FromString(System.String)
extern "C" SecurityElement_t1_242 * PolicyLevel_FromString_m1_11589 (PolicyLevel_t1_1357 * __this, String_t* ___xml, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Security.Policy.PolicyLevel::get_FullTrustAssemblies()
extern "C" Object_t * PolicyLevel_get_FullTrustAssemblies_m1_11590 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.PolicyLevel::get_Label()
extern "C" String_t* PolicyLevel_get_Label_m1_11591 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Security.Policy.PolicyLevel::get_NamedPermissionSets()
extern "C" Object_t * PolicyLevel_get_NamedPermissionSets_m1_11592 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.PolicyLevel::get_RootCodeGroup()
extern "C" CodeGroup_t1_1339 * PolicyLevel_get_RootCodeGroup_m1_11593 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::set_RootCodeGroup(System.Security.Policy.CodeGroup)
extern "C" void PolicyLevel_set_RootCodeGroup_m1_11594 (PolicyLevel_t1_1357 * __this, CodeGroup_t1_1339 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.PolicyLevel::get_StoreLocation()
extern "C" String_t* PolicyLevel_get_StoreLocation_m1_11595 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PolicyLevelType System.Security.Policy.PolicyLevel::get_Type()
extern "C" int32_t PolicyLevel_get_Type_m1_11596 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::AddFullTrustAssembly(System.Security.Policy.StrongName)
extern "C" void PolicyLevel_AddFullTrustAssembly_m1_11597 (PolicyLevel_t1_1357 * __this, StrongName_t1_1363 * ___sn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::AddFullTrustAssembly(System.Security.Policy.StrongNameMembershipCondition)
extern "C" void PolicyLevel_AddFullTrustAssembly_m1_11598 (PolicyLevel_t1_1357 * __this, StrongNameMembershipCondition_t1_1364 * ___snMC, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::AddNamedPermissionSet(System.Security.NamedPermissionSet)
extern "C" void PolicyLevel_AddNamedPermissionSet_m1_11599 (PolicyLevel_t1_1357 * __this, NamedPermissionSet_t1_1344 * ___permSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.PolicyLevel::ChangeNamedPermissionSet(System.String,System.Security.PermissionSet)
extern "C" NamedPermissionSet_t1_1344 * PolicyLevel_ChangeNamedPermissionSet_m1_11600 (PolicyLevel_t1_1357 * __this, String_t* ___name, PermissionSet_t1_563 * ___pSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.Policy.PolicyLevel::CreateAppDomainLevel()
extern "C" PolicyLevel_t1_1357 * PolicyLevel_CreateAppDomainLevel_m1_11601 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::FromXml(System.Security.SecurityElement)
extern "C" void PolicyLevel_FromXml_m1_11602 (PolicyLevel_t1_1357 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.PolicyLevel::GetNamedPermissionSet(System.String)
extern "C" NamedPermissionSet_t1_1344 * PolicyLevel_GetNamedPermissionSet_m1_11603 (PolicyLevel_t1_1357 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::Recover()
extern "C" void PolicyLevel_Recover_m1_11604 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::RemoveFullTrustAssembly(System.Security.Policy.StrongName)
extern "C" void PolicyLevel_RemoveFullTrustAssembly_m1_11605 (PolicyLevel_t1_1357 * __this, StrongName_t1_1363 * ___sn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::RemoveFullTrustAssembly(System.Security.Policy.StrongNameMembershipCondition)
extern "C" void PolicyLevel_RemoveFullTrustAssembly_m1_11606 (PolicyLevel_t1_1357 * __this, StrongNameMembershipCondition_t1_1364 * ___snMC, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.PolicyLevel::RemoveNamedPermissionSet(System.Security.NamedPermissionSet)
extern "C" NamedPermissionSet_t1_1344 * PolicyLevel_RemoveNamedPermissionSet_m1_11607 (PolicyLevel_t1_1357 * __this, NamedPermissionSet_t1_1344 * ___permSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.NamedPermissionSet System.Security.Policy.PolicyLevel::RemoveNamedPermissionSet(System.String)
extern "C" NamedPermissionSet_t1_1344 * PolicyLevel_RemoveNamedPermissionSet_m1_11608 (PolicyLevel_t1_1357 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::Reset()
extern "C" void PolicyLevel_Reset_m1_11609 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.PolicyLevel::Resolve(System.Security.Policy.Evidence)
extern "C" PolicyStatement_t1_1334 * PolicyLevel_Resolve_m1_11610 (PolicyLevel_t1_1357 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.PolicyLevel::ResolveMatchingCodeGroups(System.Security.Policy.Evidence)
extern "C" CodeGroup_t1_1339 * PolicyLevel_ResolveMatchingCodeGroups_m1_11611 (PolicyLevel_t1_1357 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.PolicyLevel::ToXml()
extern "C" SecurityElement_t1_242 * PolicyLevel_ToXml_m1_11612 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::Save()
extern "C" void PolicyLevel_Save_m1_11613 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::CreateDefaultLevel(System.Security.PolicyLevelType)
extern "C" void PolicyLevel_CreateDefaultLevel_m1_11614 (PolicyLevel_t1_1357 * __this, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::CreateDefaultFullTrustAssemblies()
extern "C" void PolicyLevel_CreateDefaultFullTrustAssemblies_m1_11615 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyLevel::CreateDefaultNamedPermissionSets()
extern "C" void PolicyLevel_CreateDefaultNamedPermissionSets_m1_11616 (PolicyLevel_t1_1357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.PolicyLevel::ResolveClassName(System.String)
extern "C" String_t* PolicyLevel_ResolveClassName_m1_11617 (PolicyLevel_t1_1357 * __this, String_t* ___className, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.PolicyLevel::IsFullTrustAssembly(System.Reflection.Assembly)
extern "C" bool PolicyLevel_IsFullTrustAssembly_m1_11618 (PolicyLevel_t1_1357 * __this, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
