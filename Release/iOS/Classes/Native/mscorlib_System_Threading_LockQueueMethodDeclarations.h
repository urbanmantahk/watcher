﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.LockQueue
struct LockQueue_t1_1467;
// System.Threading.ReaderWriterLock
struct ReaderWriterLock_t1_1468;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.LockQueue::.ctor(System.Threading.ReaderWriterLock)
extern "C" void LockQueue__ctor_m1_12690 (LockQueue_t1_1467 * __this, ReaderWriterLock_t1_1468 * ___rwlock, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.LockQueue::Wait(System.Int32)
extern "C" bool LockQueue_Wait_m1_12691 (LockQueue_t1_1467 * __this, int32_t ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.LockQueue::get_IsEmpty()
extern "C" bool LockQueue_get_IsEmpty_m1_12692 (LockQueue_t1_1467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.LockQueue::Pulse()
extern "C" void LockQueue_Pulse_m1_12693 (LockQueue_t1_1467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.LockQueue::PulseAll()
extern "C" void LockQueue_PulseAll_m1_12694 (LockQueue_t1_1467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
