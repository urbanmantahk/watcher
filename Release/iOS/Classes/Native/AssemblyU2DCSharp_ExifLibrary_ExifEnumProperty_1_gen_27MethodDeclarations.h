﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>
struct ExifEnumProperty_1_t8_366;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSSpeedRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2324_gshared (ExifEnumProperty_1_t8_366 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2324(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_366 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2324_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2003_gshared (ExifEnumProperty_1_t8_366 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2003(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_366 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2003_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2325_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2325(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_366 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2325_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2326_gshared (ExifEnumProperty_1_t8_366 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2326(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_366 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2326_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2327_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2327(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_366 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2327_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2328_gshared (ExifEnumProperty_1_t8_366 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2328(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_366 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2328_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2329_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2329(__this, method) (( bool (*) (ExifEnumProperty_1_t8_366 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2329_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2330_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2330(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_366 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2330_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2331_gshared (ExifEnumProperty_1_t8_366 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2331(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_366 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2331_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSSpeedRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2332_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_366 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2332(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_366 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2332_gshared)(__this /* static, unused */, ___obj, method)
