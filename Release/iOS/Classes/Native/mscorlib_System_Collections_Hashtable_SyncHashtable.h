﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Collections_Hashtable.h"

// System.Collections.Hashtable/SyncHashtable
struct  SyncHashtable_t1_291  : public Hashtable_t1_100
{
	// System.Collections.Hashtable System.Collections.Hashtable/SyncHashtable::host
	Hashtable_t1_100 * ___host_14;
};
