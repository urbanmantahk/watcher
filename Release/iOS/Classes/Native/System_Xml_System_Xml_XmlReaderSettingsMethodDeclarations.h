﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t4_162;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t4_66;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_ConformanceLevel.h"

// System.Boolean System.Xml.XmlReaderSettings::get_CheckCharacters()
extern "C" bool XmlReaderSettings_get_CheckCharacters_m4_758 (XmlReaderSettings_t4_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::get_ConformanceLevel()
extern "C" int32_t XmlReaderSettings_get_ConformanceLevel_m4_759 (XmlReaderSettings_t4_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::get_Schemas()
extern "C" XmlSchemaSet_t4_66 * XmlReaderSettings_get_Schemas_m4_760 (XmlReaderSettings_t4_162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
