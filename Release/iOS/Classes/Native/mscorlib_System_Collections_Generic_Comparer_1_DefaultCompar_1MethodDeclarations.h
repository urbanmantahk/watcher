﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>
struct DefaultComparer_t1_2086;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void DefaultComparer__ctor_m1_16682_gshared (DefaultComparer_t1_2086 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_16682(__this, method) (( void (*) (DefaultComparer_t1_2086 *, const MethodInfo*))DefaultComparer__ctor_m1_16682_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeNamedArgument>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_16683_gshared (DefaultComparer_t1_2086 * __this, CustomAttributeNamedArgument_t1_593  ___x, CustomAttributeNamedArgument_t1_593  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_16683(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_2086 *, CustomAttributeNamedArgument_t1_593 , CustomAttributeNamedArgument_t1_593 , const MethodInfo*))DefaultComparer_Compare_m1_16683_gshared)(__this, ___x, ___y, method)
