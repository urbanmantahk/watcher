﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo
struct  ProductUpdateInfo_t8_21 
{
	// System.Boolean VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::<NewUpdateAvailable>k__BackingField
	bool ___U3CNewUpdateAvailableU3Ek__BackingField_4;
	// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::<VersionNumber>k__BackingField
	String_t* ___U3CVersionNumberU3Ek__BackingField_5;
	// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::<DownloadLink>k__BackingField
	String_t* ___U3CDownloadLinkU3Ek__BackingField_6;
	// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::<AssetStoreLink>k__BackingField
	String_t* ___U3CAssetStoreLinkU3Ek__BackingField_7;
	// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::<ReleaseNote>k__BackingField
	String_t* ___U3CReleaseNoteU3Ek__BackingField_8;
};
// Native definition for marshalling of: VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo
struct ProductUpdateInfo_t8_21_marshaled
{
	int32_t ___U3CNewUpdateAvailableU3Ek__BackingField_4;
	char* ___U3CVersionNumberU3Ek__BackingField_5;
	char* ___U3CDownloadLinkU3Ek__BackingField_6;
	char* ___U3CAssetStoreLinkU3Ek__BackingField_7;
	char* ___U3CReleaseNoteU3Ek__BackingField_8;
};
