﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Version
struct Version_t1_578;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Permissions.StrongNamePublicKeyBlob
struct StrongNamePublicKeyBlob_t1_1314;
// System.Security.NamedPermissionSet
struct NamedPermissionSet_t1_1344;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"

// System.Security.Policy.DefaultPolicies
struct  DefaultPolicies_t1_1343  : public Object_t
{
};
struct DefaultPolicies_t1_1343_StaticFields{
	// System.Version System.Security.Policy.DefaultPolicies::_fxVersion
	Version_t1_578 * ____fxVersion_13;
	// System.Byte[] System.Security.Policy.DefaultPolicies::_ecmaKey
	ByteU5BU5D_t1_109* ____ecmaKey_14;
	// System.Security.Permissions.StrongNamePublicKeyBlob System.Security.Policy.DefaultPolicies::_ecma
	StrongNamePublicKeyBlob_t1_1314 * ____ecma_15;
	// System.Byte[] System.Security.Policy.DefaultPolicies::_msFinalKey
	ByteU5BU5D_t1_109* ____msFinalKey_16;
	// System.Security.Permissions.StrongNamePublicKeyBlob System.Security.Policy.DefaultPolicies::_msFinal
	StrongNamePublicKeyBlob_t1_1314 * ____msFinal_17;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_fullTrust
	NamedPermissionSet_t1_1344 * ____fullTrust_18;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_localIntranet
	NamedPermissionSet_t1_1344 * ____localIntranet_19;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_internet
	NamedPermissionSet_t1_1344 * ____internet_20;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_skipVerification
	NamedPermissionSet_t1_1344 * ____skipVerification_21;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_execution
	NamedPermissionSet_t1_1344 * ____execution_22;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_nothing
	NamedPermissionSet_t1_1344 * ____nothing_23;
	// System.Security.NamedPermissionSet System.Security.Policy.DefaultPolicies::_everything
	NamedPermissionSet_t1_1344 * ____everything_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Policy.DefaultPolicies::<>f__switch$map2B
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2B_25;
};
