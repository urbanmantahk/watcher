﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IList
struct IList_t1_262;
// System.Collections.ICollection
struct ICollection_t1_280;

#include "codegen/il2cpp-codegen.h"

// System.Object[] VoxelBusters.Utility.GenericsExtension::ToArray(System.Collections.IEnumerator)
extern "C" ObjectU5BU5D_t1_272* GenericsExtension_ToArray_m8_198 (Object_t * __this /* static, unused */, Object_t * ____enumerator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] VoxelBusters.Utility.GenericsExtension::ToArray(System.Collections.IList)
extern "C" ObjectU5BU5D_t1_272* GenericsExtension_ToArray_m8_199 (Object_t * __this /* static, unused */, Object_t * ____listObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] VoxelBusters.Utility.GenericsExtension::ToArray(System.Collections.ICollection)
extern "C" ObjectU5BU5D_t1_272* GenericsExtension_ToArray_m8_200 (Object_t * __this /* static, unused */, Object_t * ____collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
