﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdString
struct XsdString_t4_7;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdString::.ctor()
extern "C" void XsdString__ctor_m4_7 (XsdString_t4_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdString::get_TokenizedType()
extern "C" int32_t XsdString_get_TokenizedType_m4_8 (XsdString_t4_7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
