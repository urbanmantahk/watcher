﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1_1824;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t1_2814;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_287;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t1_2815;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t1_1895;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t1_2350;
// System.Collections.Generic.IComparer`1<UnityEngine.UIVertex>
struct IComparer_1_t1_2816;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t1_2356;
// System.Action`1<UnityEngine.UIVertex>
struct Action_1_t1_2357;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t1_2358;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_23.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m1_20243_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1__ctor_m1_20243(__this, method) (( void (*) (List_1_t1_1824 *, const MethodInfo*))List_1__ctor_m1_20243_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_20244_gshared (List_1_t1_1824 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_20244(__this, ___collection, method) (( void (*) (List_1_t1_1824 *, Object_t*, const MethodInfo*))List_1__ctor_m1_20244_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_14953_gshared (List_1_t1_1824 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_14953(__this, ___capacity, method) (( void (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1__ctor_m1_14953_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_20245_gshared (List_1_t1_1824 * __this, UIVertexU5BU5D_t6_287* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_20245(__this, ___data, ___size, method) (( void (*) (List_1_t1_1824 *, UIVertexU5BU5D_t6_287*, int32_t, const MethodInfo*))List_1__ctor_m1_20245_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m1_20246_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_20246(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_20246_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20247_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20247(__this, method) (( Object_t* (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20247_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_20248_gshared (List_1_t1_1824 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_20248(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1824 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_20248_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_20249_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_20249(__this, method) (( Object_t * (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_20249_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_20250_gshared (List_1_t1_1824 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_20250(__this, ___item, method) (( int32_t (*) (List_1_t1_1824 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_20250_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_20251_gshared (List_1_t1_1824 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_20251(__this, ___item, method) (( bool (*) (List_1_t1_1824 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_20251_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_20252_gshared (List_1_t1_1824 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_20252(__this, ___item, method) (( int32_t (*) (List_1_t1_1824 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_20252_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_20253_gshared (List_1_t1_1824 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_20253(__this, ___index, ___item, method) (( void (*) (List_1_t1_1824 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_20253_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_20254_gshared (List_1_t1_1824 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_20254(__this, ___item, method) (( void (*) (List_1_t1_1824 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_20254_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20255_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20255(__this, method) (( bool (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_20256_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_20256(__this, method) (( bool (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_20256_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_20257_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_20257(__this, method) (( Object_t * (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_20257_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_20258_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_20258(__this, method) (( bool (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_20258_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_20259_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_20259(__this, method) (( bool (*) (List_1_t1_1824 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_20259_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_20260_gshared (List_1_t1_1824 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_20260(__this, ___index, method) (( Object_t * (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_20260_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_20261_gshared (List_1_t1_1824 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_20261(__this, ___index, ___value, method) (( void (*) (List_1_t1_1824 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_20261_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m1_20262_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_Add_m1_20262(__this, ___item, method) (( void (*) (List_1_t1_1824 *, UIVertex_t6_158 , const MethodInfo*))List_1_Add_m1_20262_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_20263_gshared (List_1_t1_1824 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_20263(__this, ___newCount, method) (( void (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_20263_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_20264_gshared (List_1_t1_1824 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_20264(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1824 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_20264_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_20265_gshared (List_1_t1_1824 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_20265(__this, ___collection, method) (( void (*) (List_1_t1_1824 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_20265_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_20266_gshared (List_1_t1_1824 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_20266(__this, ___enumerable, method) (( void (*) (List_1_t1_1824 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_20266_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_20267_gshared (List_1_t1_1824 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_20267(__this, ___collection, method) (( void (*) (List_1_t1_1824 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_20267_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2350 * List_1_AsReadOnly_m1_20268_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_20268(__this, method) (( ReadOnlyCollection_1_t1_2350 * (*) (List_1_t1_1824 *, const MethodInfo*))List_1_AsReadOnly_m1_20268_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_20269_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_20269(__this, ___item, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , const MethodInfo*))List_1_BinarySearch_m1_20269_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_20270_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_20270(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_20270_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_20271_gshared (List_1_t1_1824 * __this, int32_t ___index, int32_t ___count, UIVertex_t6_158  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_20271(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, int32_t, UIVertex_t6_158 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_20271_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m1_20272_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_Clear_m1_20272(__this, method) (( void (*) (List_1_t1_1824 *, const MethodInfo*))List_1_Clear_m1_20272_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m1_20273_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_Contains_m1_20273(__this, ___item, method) (( bool (*) (List_1_t1_1824 *, UIVertex_t6_158 , const MethodInfo*))List_1_Contains_m1_20273_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_20274_gshared (List_1_t1_1824 * __this, UIVertexU5BU5D_t6_287* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_20274(__this, ___array, method) (( void (*) (List_1_t1_1824 *, UIVertexU5BU5D_t6_287*, const MethodInfo*))List_1_CopyTo_m1_20274_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_20275_gshared (List_1_t1_1824 * __this, UIVertexU5BU5D_t6_287* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_20275(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1824 *, UIVertexU5BU5D_t6_287*, int32_t, const MethodInfo*))List_1_CopyTo_m1_20275_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_20276_gshared (List_1_t1_1824 * __this, int32_t ___index, UIVertexU5BU5D_t6_287* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_20276(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1824 *, int32_t, UIVertexU5BU5D_t6_287*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_20276_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_20277_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_20277(__this, ___match, method) (( bool (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_Exists_m1_20277_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t6_158  List_1_Find_m1_20278_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_Find_m1_20278(__this, ___match, method) (( UIVertex_t6_158  (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_Find_m1_20278_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_20279_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_20279(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2356 *, const MethodInfo*))List_1_CheckMatch_m1_20279_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1824 * List_1_FindAll_m1_20280_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_20280(__this, ___match, method) (( List_1_t1_1824 * (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindAll_m1_20280_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1824 * List_1_FindAllStackBits_m1_20281_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_20281(__this, ___match, method) (( List_1_t1_1824 * (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindAllStackBits_m1_20281_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1824 * List_1_FindAllList_m1_20282_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_20282(__this, ___match, method) (( List_1_t1_1824 * (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindAllList_m1_20282_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20283_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20283(__this, ___match, method) (( int32_t (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindIndex_m1_20283_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20284_gshared (List_1_t1_1824 * __this, int32_t ___startIndex, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20284(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindIndex_m1_20284_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20285_gshared (List_1_t1_1824 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20285(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, int32_t, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindIndex_m1_20285_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_20286_gshared (List_1_t1_1824 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_20286(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, int32_t, Predicate_1_t1_2356 *, const MethodInfo*))List_1_GetIndex_m1_20286_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindLast(System.Predicate`1<T>)
extern "C" UIVertex_t6_158  List_1_FindLast_m1_20287_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_20287(__this, ___match, method) (( UIVertex_t6_158  (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindLast_m1_20287_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20288_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20288(__this, ___match, method) (( int32_t (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindLastIndex_m1_20288_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20289_gshared (List_1_t1_1824 * __this, int32_t ___startIndex, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20289(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindLastIndex_m1_20289_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20290_gshared (List_1_t1_1824 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20290(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, int32_t, Predicate_1_t1_2356 *, const MethodInfo*))List_1_FindLastIndex_m1_20290_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_20291_gshared (List_1_t1_1824 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_20291(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1824 *, int32_t, int32_t, Predicate_1_t1_2356 *, const MethodInfo*))List_1_GetLastIndex_m1_20291_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_20292_gshared (List_1_t1_1824 * __this, Action_1_t1_2357 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_20292(__this, ___action, method) (( void (*) (List_1_t1_1824 *, Action_1_t1_2357 *, const MethodInfo*))List_1_ForEach_m1_20292_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t1_2349  List_1_GetEnumerator_m1_20293_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_20293(__this, method) (( Enumerator_t1_2349  (*) (List_1_t1_1824 *, const MethodInfo*))List_1_GetEnumerator_m1_20293_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1824 * List_1_GetRange_m1_20294_gshared (List_1_t1_1824 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_20294(__this, ___index, ___count, method) (( List_1_t1_1824 * (*) (List_1_t1_1824 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_20294_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_20295_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_20295(__this, ___item, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , const MethodInfo*))List_1_IndexOf_m1_20295_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_20296_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_20296(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , int32_t, const MethodInfo*))List_1_IndexOf_m1_20296_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_20297_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_20297(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_20297_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_20298_gshared (List_1_t1_1824 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_20298(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1824 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_20298_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_20299_gshared (List_1_t1_1824 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_20299(__this, ___index, method) (( void (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_20299_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_20300_gshared (List_1_t1_1824 * __this, int32_t ___index, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_Insert_m1_20300(__this, ___index, ___item, method) (( void (*) (List_1_t1_1824 *, int32_t, UIVertex_t6_158 , const MethodInfo*))List_1_Insert_m1_20300_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_20301_gshared (List_1_t1_1824 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_20301(__this, ___collection, method) (( void (*) (List_1_t1_1824 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_20301_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_20302_gshared (List_1_t1_1824 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_20302(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1824 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_20302_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_20303_gshared (List_1_t1_1824 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_20303(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1824 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_20303_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_20304_gshared (List_1_t1_1824 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_20304(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1824 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_20304_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_20305_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20305(__this, ___item, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , const MethodInfo*))List_1_LastIndexOf_m1_20305_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_20306_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20306(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_20306_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_20307_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20307(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1824 *, UIVertex_t6_158 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_20307_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m1_20308_gshared (List_1_t1_1824 * __this, UIVertex_t6_158  ___item, const MethodInfo* method);
#define List_1_Remove_m1_20308(__this, ___item, method) (( bool (*) (List_1_t1_1824 *, UIVertex_t6_158 , const MethodInfo*))List_1_Remove_m1_20308_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_20309_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_20309(__this, ___match, method) (( int32_t (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_RemoveAll_m1_20309_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_20310_gshared (List_1_t1_1824 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_20310(__this, ___index, method) (( void (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_20310_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_20311_gshared (List_1_t1_1824 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_20311(__this, ___index, ___count, method) (( void (*) (List_1_t1_1824 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_20311_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m1_20312_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_20312(__this, method) (( void (*) (List_1_t1_1824 *, const MethodInfo*))List_1_Reverse_m1_20312_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_20313_gshared (List_1_t1_1824 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_20313(__this, ___index, ___count, method) (( void (*) (List_1_t1_1824 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_20313_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m1_20314_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_Sort_m1_20314(__this, method) (( void (*) (List_1_t1_1824 *, const MethodInfo*))List_1_Sort_m1_20314_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_20315_gshared (List_1_t1_1824 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_20315(__this, ___comparer, method) (( void (*) (List_1_t1_1824 *, Object_t*, const MethodInfo*))List_1_Sort_m1_20315_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_20316_gshared (List_1_t1_1824 * __this, Comparison_1_t1_2358 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_20316(__this, ___comparison, method) (( void (*) (List_1_t1_1824 *, Comparison_1_t1_2358 *, const MethodInfo*))List_1_Sort_m1_20316_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_20317_gshared (List_1_t1_1824 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_20317(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1824 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_20317_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t6_287* List_1_ToArray_m1_20318_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_20318(__this, method) (( UIVertexU5BU5D_t6_287* (*) (List_1_t1_1824 *, const MethodInfo*))List_1_ToArray_m1_20318_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_20319_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_20319(__this, method) (( void (*) (List_1_t1_1824 *, const MethodInfo*))List_1_TrimExcess_m1_20319_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_20320_gshared (List_1_t1_1824 * __this, Predicate_1_t1_2356 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_20320(__this, ___match, method) (( bool (*) (List_1_t1_1824 *, Predicate_1_t1_2356 *, const MethodInfo*))List_1_TrueForAll_m1_20320_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_15024_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_15024(__this, method) (( int32_t (*) (List_1_t1_1824 *, const MethodInfo*))List_1_get_Capacity_m1_15024_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_15025_gshared (List_1_t1_1824 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_15025(__this, ___value, method) (( void (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_15025_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m1_20321_gshared (List_1_t1_1824 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_20321(__this, method) (( int32_t (*) (List_1_t1_1824 *, const MethodInfo*))List_1_get_Count_m1_20321_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t6_158  List_1_get_Item_m1_20322_gshared (List_1_t1_1824 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_20322(__this, ___index, method) (( UIVertex_t6_158  (*) (List_1_t1_1824 *, int32_t, const MethodInfo*))List_1_get_Item_m1_20322_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_20323_gshared (List_1_t1_1824 * __this, int32_t ___index, UIVertex_t6_158  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_20323(__this, ___index, ___value, method) (( void (*) (List_1_t1_1824 *, int32_t, UIVertex_t6_158 , const MethodInfo*))List_1_set_Item_m1_20323_gshared)(__this, ___index, ___value, method)
