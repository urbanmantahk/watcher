﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHash.h"

// System.Configuration.Assemblies.AssemblyHash
struct  AssemblyHash_t1_311 
{
	// System.Configuration.Assemblies.AssemblyHashAlgorithm System.Configuration.Assemblies.AssemblyHash::_algorithm
	int32_t ____algorithm_0;
	// System.Byte[] System.Configuration.Assemblies.AssemblyHash::_value
	ByteU5BU5D_t1_109* ____value_1;
};
struct AssemblyHash_t1_311_StaticFields{
	// System.Configuration.Assemblies.AssemblyHash System.Configuration.Assemblies.AssemblyHash::Empty
	AssemblyHash_t1_311  ___Empty_2;
};
// Native definition for marshalling of: System.Configuration.Assemblies.AssemblyHash
struct AssemblyHash_t1_311_marshaled
{
	int32_t ____algorithm_0;
	uint8_t* ____value_1;
};
