﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>
struct IEqualityComparer_1_t1_2662;
// System.Collections.Generic.IDictionary`2<ExifLibrary.ExifTag,System.Object>
struct IDictionary_2_t1_2875;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<ExifLibrary.ExifTag>
struct ICollection_1_t1_2876;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2874;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>
struct IEnumerator_1_t1_2877;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>
struct KeyCollection_t1_2668;
// System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>
struct ValueCollection_t1_2672;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m1_26317_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26317(__this, method) (( void (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2__ctor_m1_26317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26319_gshared (Dictionary_2_t1_2663 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26319(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26319_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_26321_gshared (Dictionary_2_t1_2663 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26321(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26321_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_26323_gshared (Dictionary_2_t1_2663 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26323(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_26323_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26325_gshared (Dictionary_2_t1_2663 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26325(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26325_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26327_gshared (Dictionary_2_t1_2663 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26327(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26327_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_26329_gshared (Dictionary_2_t1_2663 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26329(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2663 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_26329_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26331_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26331(__this, method) (( Object_t* (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26331_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26333_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26333(__this, method) (( Object_t* (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26333_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26335_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26335(__this, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26335_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_26337_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_26337(__this, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_26337_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26339_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26339(__this, method) (( bool (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26339_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26341_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26341(__this, method) (( bool (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26341_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_26343_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_26343(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_26343_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_26345_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_26345(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_26345_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_26347_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_26347(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_26347_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_26349_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_26349(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_26349_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_26351_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_26351(__this, ___key, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_26351_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26353_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26353(__this, method) (( bool (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26353_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26355_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26355(__this, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26355_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26357_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26357(__this, method) (( bool (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26357_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26359_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26359(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2_t1_2665 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26359_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26361_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26361(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2663 *, KeyValuePair_2_t1_2665 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26361_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26363_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2U5BU5D_t1_2874* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26363(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26363_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26365_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26365(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2663 *, KeyValuePair_2_t1_2665 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26365_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_26367_gshared (Dictionary_2_t1_2663 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_26367(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_26367_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26369_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26369(__this, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26369_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26371_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26371(__this, method) (( Object_t* (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26371_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26373_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26373(__this, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26373_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_26375_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_26375(__this, method) (( int32_t (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_get_Count_m1_26375_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m1_26377_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_26377(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1_26377_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_26379_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_26379(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m1_26379_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_26381_gshared (Dictionary_2_t1_2663 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_26381(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_26381_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_26383_gshared (Dictionary_2_t1_2663 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_26383(__this, ___size, method) (( void (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_26383_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_26385_gshared (Dictionary_2_t1_2663 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_26385(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_26385_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2665  Dictionary_2_make_pair_m1_26387_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_26387(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_2665  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m1_26387_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m1_26389_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_26389(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m1_26389_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m1_26391_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_26391(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m1_26391_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_26393_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2U5BU5D_t1_2874* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_26393(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_26393_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m1_26395_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_26395(__this, method) (( void (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_Resize_m1_26395_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_26397_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_26397(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m1_26397_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_26399_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_26399(__this, method) (( Object_t* (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_26399_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m1_26401_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_26401(__this, method) (( void (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_Clear_m1_26401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_26403_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_26403(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1_26403_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_26405_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_26405(__this, ___value, method) (( bool (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m1_26405_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_26407_gshared (Dictionary_2_t1_2663 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_26407(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2663 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_26407_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_26409_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_26409(__this, ___sender, method) (( void (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_26409_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_26411_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_26411(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1_26411_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_26413_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_26413(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_2663 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m1_26413_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Keys()
extern "C" KeyCollection_t1_2668 * Dictionary_2_get_Keys_m1_26415_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_26415(__this, method) (( KeyCollection_t1_2668 * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_get_Keys_m1_26415_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Values()
extern "C" ValueCollection_t1_2672 * Dictionary_2_get_Values_m1_26416_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_26416(__this, method) (( ValueCollection_t1_2672 * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_get_Values_m1_26416_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m1_26418_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_26418(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_26418_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m1_26420_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_26420(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_26420_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_26422_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_26422(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_2663 *, KeyValuePair_2_t1_2665 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_26422_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2670  Dictionary_2_GetEnumerator_m1_26423_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_26423(__this, method) (( Enumerator_t1_2670  (*) (Dictionary_2_t1_2663 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_26423_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_26425_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_26425(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_26425_gshared)(__this /* static, unused */, ___key, ___value, method)
