﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DefaultHandler
struct DefaultHandler_t1_244;
// Mono.Xml.SmallXmlParser
struct SmallXmlParser_t1_241;
// System.String
struct String_t;
// Mono.Xml.SmallXmlParser/IAttrList
struct IAttrList_t1_1676;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DefaultHandler::.ctor()
extern "C" void DefaultHandler__ctor_m1_2676 (DefaultHandler_t1_244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnStartParsing(Mono.Xml.SmallXmlParser)
extern "C" void DefaultHandler_OnStartParsing_m1_2677 (DefaultHandler_t1_244 * __this, SmallXmlParser_t1_241 * ___parser, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnEndParsing(Mono.Xml.SmallXmlParser)
extern "C" void DefaultHandler_OnEndParsing_m1_2678 (DefaultHandler_t1_244 * __this, SmallXmlParser_t1_241 * ___parser, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnStartElement(System.String,Mono.Xml.SmallXmlParser/IAttrList)
extern "C" void DefaultHandler_OnStartElement_m1_2679 (DefaultHandler_t1_244 * __this, String_t* ___name, Object_t * ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnEndElement(System.String)
extern "C" void DefaultHandler_OnEndElement_m1_2680 (DefaultHandler_t1_244 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnChars(System.String)
extern "C" void DefaultHandler_OnChars_m1_2681 (DefaultHandler_t1_244 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnIgnorableWhitespace(System.String)
extern "C" void DefaultHandler_OnIgnorableWhitespace_m1_2682 (DefaultHandler_t1_244 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DefaultHandler::OnProcessingInstruction(System.String,System.String)
extern "C" void DefaultHandler_OnProcessingInstruction_m1_2683 (DefaultHandler_t1_244 * __this, String_t* ___name, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
