﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.HostExecutionContext
struct HostExecutionContext_t1_1463;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.HostExecutionContext::.ctor()
extern "C" void HostExecutionContext__ctor_m1_12655 (HostExecutionContext_t1_1463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.HostExecutionContext::.ctor(System.Object)
extern "C" void HostExecutionContext__ctor_m1_12656 (HostExecutionContext_t1_1463 * __this, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.HostExecutionContext System.Threading.HostExecutionContext::CreateCopy()
extern "C" HostExecutionContext_t1_1463 * HostExecutionContext_CreateCopy_m1_12657 (HostExecutionContext_t1_1463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.HostExecutionContext::get_State()
extern "C" Object_t * HostExecutionContext_get_State_m1_12658 (HostExecutionContext_t1_1463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.HostExecutionContext::set_State(System.Object)
extern "C" void HostExecutionContext_set_State_m1_12659 (HostExecutionContext_t1_1463 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
