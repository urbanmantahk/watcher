﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1_1825;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t1_2817;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_288;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t1_2818;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t1_2819;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t1_2361;
// System.Collections.Generic.IComparer`1<UnityEngine.UICharInfo>
struct IComparer_1_t1_2820;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1_2367;
// System.Action`1<UnityEngine.UICharInfo>
struct Action_1_t1_2368;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t1_2369;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_24.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m1_20432_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1__ctor_m1_20432(__this, method) (( void (*) (List_1_t1_1825 *, const MethodInfo*))List_1__ctor_m1_20432_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_20433_gshared (List_1_t1_1825 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_20433(__this, ___collection, method) (( void (*) (List_1_t1_1825 *, Object_t*, const MethodInfo*))List_1__ctor_m1_20433_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_14954_gshared (List_1_t1_1825 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_14954(__this, ___capacity, method) (( void (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1__ctor_m1_14954_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_20434_gshared (List_1_t1_1825 * __this, UICharInfoU5BU5D_t6_288* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_20434(__this, ___data, ___size, method) (( void (*) (List_1_t1_1825 *, UICharInfoU5BU5D_t6_288*, int32_t, const MethodInfo*))List_1__ctor_m1_20434_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m1_20435_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_20435(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_20435_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20436_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20436(__this, method) (( Object_t* (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_20436_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_20437_gshared (List_1_t1_1825 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_20437(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1825 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_20437_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_20438_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_20438(__this, method) (( Object_t * (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_20438_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_20439_gshared (List_1_t1_1825 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_20439(__this, ___item, method) (( int32_t (*) (List_1_t1_1825 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_20439_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_20440_gshared (List_1_t1_1825 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_20440(__this, ___item, method) (( bool (*) (List_1_t1_1825 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_20440_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_20441_gshared (List_1_t1_1825 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_20441(__this, ___item, method) (( int32_t (*) (List_1_t1_1825 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_20441_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_20442_gshared (List_1_t1_1825 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_20442(__this, ___index, ___item, method) (( void (*) (List_1_t1_1825 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_20442_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_20443_gshared (List_1_t1_1825 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_20443(__this, ___item, method) (( void (*) (List_1_t1_1825 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_20443_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20444_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20444(__this, method) (( bool (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20444_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_20445_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_20445(__this, method) (( bool (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_20445_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_20446_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_20446(__this, method) (( Object_t * (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_20446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_20447_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_20447(__this, method) (( bool (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_20447_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_20448_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_20448(__this, method) (( bool (*) (List_1_t1_1825 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_20448_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_20449_gshared (List_1_t1_1825 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_20449(__this, ___index, method) (( Object_t * (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_20449_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_20450_gshared (List_1_t1_1825 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_20450(__this, ___index, ___value, method) (( void (*) (List_1_t1_1825 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_20450_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m1_20451_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Add_m1_20451(__this, ___item, method) (( void (*) (List_1_t1_1825 *, UICharInfo_t6_150 , const MethodInfo*))List_1_Add_m1_20451_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_20452_gshared (List_1_t1_1825 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_20452(__this, ___newCount, method) (( void (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_20452_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_20453_gshared (List_1_t1_1825 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_20453(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1825 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_20453_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_20454_gshared (List_1_t1_1825 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_20454(__this, ___collection, method) (( void (*) (List_1_t1_1825 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_20454_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_20455_gshared (List_1_t1_1825 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_20455(__this, ___enumerable, method) (( void (*) (List_1_t1_1825 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_20455_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_20456_gshared (List_1_t1_1825 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_20456(__this, ___collection, method) (( void (*) (List_1_t1_1825 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_20456_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_2361 * List_1_AsReadOnly_m1_20457_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_20457(__this, method) (( ReadOnlyCollection_1_t1_2361 * (*) (List_1_t1_1825 *, const MethodInfo*))List_1_AsReadOnly_m1_20457_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_20458_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_20458(__this, ___item, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , const MethodInfo*))List_1_BinarySearch_m1_20458_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_20459_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_20459(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_20459_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_20460_gshared (List_1_t1_1825 * __this, int32_t ___index, int32_t ___count, UICharInfo_t6_150  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_20460(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, int32_t, UICharInfo_t6_150 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_20460_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m1_20461_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_Clear_m1_20461(__this, method) (( void (*) (List_1_t1_1825 *, const MethodInfo*))List_1_Clear_m1_20461_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m1_20462_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Contains_m1_20462(__this, ___item, method) (( bool (*) (List_1_t1_1825 *, UICharInfo_t6_150 , const MethodInfo*))List_1_Contains_m1_20462_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_20463_gshared (List_1_t1_1825 * __this, UICharInfoU5BU5D_t6_288* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_20463(__this, ___array, method) (( void (*) (List_1_t1_1825 *, UICharInfoU5BU5D_t6_288*, const MethodInfo*))List_1_CopyTo_m1_20463_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_20464_gshared (List_1_t1_1825 * __this, UICharInfoU5BU5D_t6_288* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_20464(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1825 *, UICharInfoU5BU5D_t6_288*, int32_t, const MethodInfo*))List_1_CopyTo_m1_20464_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_20465_gshared (List_1_t1_1825 * __this, int32_t ___index, UICharInfoU5BU5D_t6_288* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_20465(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1825 *, int32_t, UICharInfoU5BU5D_t6_288*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_20465_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_20466_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_20466(__this, ___match, method) (( bool (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_Exists_m1_20466_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t6_150  List_1_Find_m1_20467_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_Find_m1_20467(__this, ___match, method) (( UICharInfo_t6_150  (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_Find_m1_20467_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_20468_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_20468(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2367 *, const MethodInfo*))List_1_CheckMatch_m1_20468_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_1825 * List_1_FindAll_m1_20469_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_20469(__this, ___match, method) (( List_1_t1_1825 * (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindAll_m1_20469_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_1825 * List_1_FindAllStackBits_m1_20470_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_20470(__this, ___match, method) (( List_1_t1_1825 * (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindAllStackBits_m1_20470_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_1825 * List_1_FindAllList_m1_20471_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_20471(__this, ___match, method) (( List_1_t1_1825 * (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindAllList_m1_20471_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20472_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20472(__this, ___match, method) (( int32_t (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindIndex_m1_20472_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20473_gshared (List_1_t1_1825 * __this, int32_t ___startIndex, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20473(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindIndex_m1_20473_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_20474_gshared (List_1_t1_1825 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_20474(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, int32_t, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindIndex_m1_20474_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_20475_gshared (List_1_t1_1825 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_20475(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, int32_t, Predicate_1_t1_2367 *, const MethodInfo*))List_1_GetIndex_m1_20475_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindLast(System.Predicate`1<T>)
extern "C" UICharInfo_t6_150  List_1_FindLast_m1_20476_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_20476(__this, ___match, method) (( UICharInfo_t6_150  (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindLast_m1_20476_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20477_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20477(__this, ___match, method) (( int32_t (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindLastIndex_m1_20477_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20478_gshared (List_1_t1_1825 * __this, int32_t ___startIndex, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20478(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindLastIndex_m1_20478_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_20479_gshared (List_1_t1_1825 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_20479(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, int32_t, Predicate_1_t1_2367 *, const MethodInfo*))List_1_FindLastIndex_m1_20479_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_20480_gshared (List_1_t1_1825 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_20480(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1825 *, int32_t, int32_t, Predicate_1_t1_2367 *, const MethodInfo*))List_1_GetLastIndex_m1_20480_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_20481_gshared (List_1_t1_1825 * __this, Action_1_t1_2368 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_20481(__this, ___action, method) (( void (*) (List_1_t1_1825 *, Action_1_t1_2368 *, const MethodInfo*))List_1_ForEach_m1_20481_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t1_2360  List_1_GetEnumerator_m1_20482_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_20482(__this, method) (( Enumerator_t1_2360  (*) (List_1_t1_1825 *, const MethodInfo*))List_1_GetEnumerator_m1_20482_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_1825 * List_1_GetRange_m1_20483_gshared (List_1_t1_1825 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_20483(__this, ___index, ___count, method) (( List_1_t1_1825 * (*) (List_1_t1_1825 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_20483_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_20484_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_20484(__this, ___item, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , const MethodInfo*))List_1_IndexOf_m1_20484_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_20485_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_20485(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , int32_t, const MethodInfo*))List_1_IndexOf_m1_20485_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_20486_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_20486(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_20486_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_20487_gshared (List_1_t1_1825 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_20487(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1825 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_20487_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_20488_gshared (List_1_t1_1825 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_20488(__this, ___index, method) (( void (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_20488_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_20489_gshared (List_1_t1_1825 * __this, int32_t ___index, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Insert_m1_20489(__this, ___index, ___item, method) (( void (*) (List_1_t1_1825 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))List_1_Insert_m1_20489_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_20490_gshared (List_1_t1_1825 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_20490(__this, ___collection, method) (( void (*) (List_1_t1_1825 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_20490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_20491_gshared (List_1_t1_1825 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_20491(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1825 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_20491_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_20492_gshared (List_1_t1_1825 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_20492(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1825 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_20492_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_20493_gshared (List_1_t1_1825 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_20493(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1825 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_20493_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_20494_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20494(__this, ___item, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , const MethodInfo*))List_1_LastIndexOf_m1_20494_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_20495_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20495(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_20495_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_20496_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_20496(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1825 *, UICharInfo_t6_150 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_20496_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m1_20497_gshared (List_1_t1_1825 * __this, UICharInfo_t6_150  ___item, const MethodInfo* method);
#define List_1_Remove_m1_20497(__this, ___item, method) (( bool (*) (List_1_t1_1825 *, UICharInfo_t6_150 , const MethodInfo*))List_1_Remove_m1_20497_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_20498_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_20498(__this, ___match, method) (( int32_t (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_RemoveAll_m1_20498_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_20499_gshared (List_1_t1_1825 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_20499(__this, ___index, method) (( void (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_20499_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_20500_gshared (List_1_t1_1825 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_20500(__this, ___index, ___count, method) (( void (*) (List_1_t1_1825 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_20500_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m1_20501_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_20501(__this, method) (( void (*) (List_1_t1_1825 *, const MethodInfo*))List_1_Reverse_m1_20501_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_20502_gshared (List_1_t1_1825 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_20502(__this, ___index, ___count, method) (( void (*) (List_1_t1_1825 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_20502_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m1_20503_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_Sort_m1_20503(__this, method) (( void (*) (List_1_t1_1825 *, const MethodInfo*))List_1_Sort_m1_20503_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_20504_gshared (List_1_t1_1825 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_20504(__this, ___comparer, method) (( void (*) (List_1_t1_1825 *, Object_t*, const MethodInfo*))List_1_Sort_m1_20504_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_20505_gshared (List_1_t1_1825 * __this, Comparison_1_t1_2369 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_20505(__this, ___comparison, method) (( void (*) (List_1_t1_1825 *, Comparison_1_t1_2369 *, const MethodInfo*))List_1_Sort_m1_20505_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_20506_gshared (List_1_t1_1825 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_20506(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1825 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_20506_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t6_288* List_1_ToArray_m1_20507_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_20507(__this, method) (( UICharInfoU5BU5D_t6_288* (*) (List_1_t1_1825 *, const MethodInfo*))List_1_ToArray_m1_20507_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_20508_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_20508(__this, method) (( void (*) (List_1_t1_1825 *, const MethodInfo*))List_1_TrimExcess_m1_20508_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_20509_gshared (List_1_t1_1825 * __this, Predicate_1_t1_2367 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_20509(__this, ___match, method) (( bool (*) (List_1_t1_1825 *, Predicate_1_t1_2367 *, const MethodInfo*))List_1_TrueForAll_m1_20509_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_20510_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_20510(__this, method) (( int32_t (*) (List_1_t1_1825 *, const MethodInfo*))List_1_get_Capacity_m1_20510_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_20511_gshared (List_1_t1_1825 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_20511(__this, ___value, method) (( void (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_20511_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m1_20512_gshared (List_1_t1_1825 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_20512(__this, method) (( int32_t (*) (List_1_t1_1825 *, const MethodInfo*))List_1_get_Count_m1_20512_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t6_150  List_1_get_Item_m1_20513_gshared (List_1_t1_1825 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_20513(__this, ___index, method) (( UICharInfo_t6_150  (*) (List_1_t1_1825 *, int32_t, const MethodInfo*))List_1_get_Item_m1_20513_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_20514_gshared (List_1_t1_1825 * __this, int32_t ___index, UICharInfo_t6_150  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_20514(__this, ___index, ___value, method) (( void (*) (List_1_t1_1825 *, int32_t, UICharInfo_t6_150 , const MethodInfo*))List_1_set_Item_m1_20514_gshared)(__this, ___index, ___value, method)
