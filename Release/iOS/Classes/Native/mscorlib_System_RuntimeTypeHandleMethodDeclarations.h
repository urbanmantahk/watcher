﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_ModuleHandle.h"

// System.Void System.RuntimeTypeHandle::.ctor(System.IntPtr)
extern "C" void RuntimeTypeHandle__ctor_m1_1270 (RuntimeTypeHandle_t1_30 * __this, IntPtr_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.RuntimeTypeHandle::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RuntimeTypeHandle__ctor_m1_1271 (RuntimeTypeHandle_t1_30 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.RuntimeTypeHandle::get_Value()
extern "C" IntPtr_t RuntimeTypeHandle_get_Value_m1_1272 (RuntimeTypeHandle_t1_30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.RuntimeTypeHandle::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RuntimeTypeHandle_GetObjectData_m1_1273 (RuntimeTypeHandle_t1_30 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeTypeHandle::Equals(System.Object)
extern "C" bool RuntimeTypeHandle_Equals_m1_1274 (RuntimeTypeHandle_t1_30 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeTypeHandle::Equals(System.RuntimeTypeHandle)
extern "C" bool RuntimeTypeHandle_Equals_m1_1275 (RuntimeTypeHandle_t1_30 * __this, RuntimeTypeHandle_t1_30  ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.RuntimeTypeHandle::GetHashCode()
extern "C" int32_t RuntimeTypeHandle_GetHashCode_m1_1276 (RuntimeTypeHandle_t1_30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ModuleHandle System.RuntimeTypeHandle::GetModuleHandle()
extern "C" ModuleHandle_t1_1572  RuntimeTypeHandle_GetModuleHandle_m1_1277 (RuntimeTypeHandle_t1_30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeTypeHandle::op_Equality(System.RuntimeTypeHandle,System.Object)
extern "C" bool RuntimeTypeHandle_op_Equality_m1_1278 (Object_t * __this /* static, unused */, RuntimeTypeHandle_t1_30  ___left, Object_t * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeTypeHandle::op_Inequality(System.RuntimeTypeHandle,System.Object)
extern "C" bool RuntimeTypeHandle_op_Inequality_m1_1279 (Object_t * __this /* static, unused */, RuntimeTypeHandle_t1_30  ___left, Object_t * ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeTypeHandle::op_Equality(System.Object,System.RuntimeTypeHandle)
extern "C" bool RuntimeTypeHandle_op_Equality_m1_1280 (Object_t * __this /* static, unused */, Object_t * ___left, RuntimeTypeHandle_t1_30  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.RuntimeTypeHandle::op_Inequality(System.Object,System.RuntimeTypeHandle)
extern "C" bool RuntimeTypeHandle_op_Inequality_m1_1281 (Object_t * __this /* static, unused */, Object_t * ___left, RuntimeTypeHandle_t1_30  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
