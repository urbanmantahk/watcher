﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_26728(__this, ___host, method) (( void (*) (Enumerator_t1_1926 *, Dictionary_2_t1_1904 *, const MethodInfo*))Enumerator__ctor_m1_26496_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_26729(__this, method) (( Object_t * (*) (Enumerator_t1_1926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_26497_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_26730(__this, method) (( void (*) (Enumerator_t1_1926 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_26498_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Dispose()
#define Enumerator_Dispose_m1_26731(__this, method) (( void (*) (Enumerator_t1_1926 *, const MethodInfo*))Enumerator_Dispose_m1_26499_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::MoveNext()
#define Enumerator_MoveNext_m1_15052(__this, method) (( bool (*) (Enumerator_t1_1926 *, const MethodInfo*))Enumerator_MoveNext_m1_26500_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Current()
#define Enumerator_get_Current_m1_15051(__this, method) (( ExifProperty_t8_99 * (*) (Enumerator_t1_1926 *, const MethodInfo*))Enumerator_get_Current_m1_26501_gshared)(__this, method)
