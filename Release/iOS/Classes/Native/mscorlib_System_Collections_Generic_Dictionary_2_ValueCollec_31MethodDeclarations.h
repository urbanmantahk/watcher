﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>
struct ValueCollection_t1_2712;
// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1_2704;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1_2893;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Single[]
struct SingleU5BU5D_t1_1695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_32.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m1_26842_gshared (ValueCollection_t1_2712 * __this, Dictionary_2_t1_2704 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m1_26842(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_2712 *, Dictionary_2_t1_2704 *, const MethodInfo*))ValueCollection__ctor_m1_26842_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26844_gshared (ValueCollection_t1_2712 * __this, float ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26844(__this, ___item, method) (( void (*) (ValueCollection_t1_2712 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26844_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26846_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26846(__this, method) (( void (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26846_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26848_gshared (ValueCollection_t1_2712 * __this, float ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26848(__this, ___item, method) (( bool (*) (ValueCollection_t1_2712 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26848_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26850_gshared (ValueCollection_t1_2712 * __this, float ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26850(__this, ___item, method) (( bool (*) (ValueCollection_t1_2712 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26850_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26852_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26852(__this, method) (( Object_t* (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26852_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_26854_gshared (ValueCollection_t1_2712 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_26854(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2712 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_26854_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26856_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26856(__this, method) (( Object_t * (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26858_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26858(__this, method) (( bool (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26858_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26860_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26860(__this, method) (( bool (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26860_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26862_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26862(__this, method) (( Object_t * (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26862_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_26864_gshared (ValueCollection_t1_2712 * __this, SingleU5BU5D_t1_1695* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m1_26864(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2712 *, SingleU5BU5D_t1_1695*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_26864_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::GetEnumerator()
extern "C" Enumerator_t1_2713  ValueCollection_GetEnumerator_m1_26866_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1_26866(__this, method) (( Enumerator_t1_2713  (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_26866_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_26868_gshared (ValueCollection_t1_2712 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1_26868(__this, method) (( int32_t (*) (ValueCollection_t1_2712 *, const MethodInfo*))ValueCollection_get_Count_m1_26868_gshared)(__this, method)
