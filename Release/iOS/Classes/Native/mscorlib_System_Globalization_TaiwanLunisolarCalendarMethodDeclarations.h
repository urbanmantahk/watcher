﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.TaiwanLunisolarCalendar
struct TaiwanLunisolarCalendar_t1_388;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.TaiwanLunisolarCalendar::.ctor()
extern "C" void TaiwanLunisolarCalendar__ctor_m1_4487 (TaiwanLunisolarCalendar_t1_388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TaiwanLunisolarCalendar::.cctor()
extern "C" void TaiwanLunisolarCalendar__cctor_m1_4488 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.TaiwanLunisolarCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* TaiwanLunisolarCalendar_get_Eras_m1_4489 (TaiwanLunisolarCalendar_t1_388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TaiwanLunisolarCalendar::GetEra(System.DateTime)
extern "C" int32_t TaiwanLunisolarCalendar_GetEra_m1_4490 (TaiwanLunisolarCalendar_t1_388 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanLunisolarCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  TaiwanLunisolarCalendar_get_MinSupportedDateTime_m1_4491 (TaiwanLunisolarCalendar_t1_388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.TaiwanLunisolarCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  TaiwanLunisolarCalendar_get_MaxSupportedDateTime_m1_4492 (TaiwanLunisolarCalendar_t1_388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
