﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSDirectionRef.h"

// ExifLibrary.GPSDirectionRef
struct  GPSDirectionRef_t8_92 
{
	// System.Byte ExifLibrary.GPSDirectionRef::value__
	uint8_t ___value___1;
};
