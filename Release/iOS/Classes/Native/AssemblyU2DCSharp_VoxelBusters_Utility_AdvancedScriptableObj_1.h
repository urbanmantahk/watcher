﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Object_t;

#include "UnityEngine_UnityEngine_ScriptableObject.h"

// VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>
struct  AdvancedScriptableObject_1_t8_375  : public ScriptableObject_t6_15
{
};
struct AdvancedScriptableObject_1_t8_375_StaticFields{
	// T VoxelBusters.Utility.AdvancedScriptableObject`1::instance
	Object_t * ___instance_6;
	// System.String VoxelBusters.Utility.AdvancedScriptableObject`1::assetGroupFolderName
	String_t* ___assetGroupFolderName_7;
};
