﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IMPLTYPEFLA.h"

// System.Runtime.InteropServices.ComTypes.IMPLTYPEFLAGS
struct  IMPLTYPEFLAGS_t1_737 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.IMPLTYPEFLAGS::value__
	int32_t ___value___1;
};
