﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.TypeLibImportClassAttribute
struct TypeLibImportClassAttribute_t1_835;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.TypeLibImportClassAttribute::.ctor(System.Type)
extern "C" void TypeLibImportClassAttribute__ctor_m1_7925 (TypeLibImportClassAttribute_t1_835 * __this, Type_t * ___importClass, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.TypeLibImportClassAttribute::get_Value()
extern "C" String_t* TypeLibImportClassAttribute_get_Value_m1_7926 (TypeLibImportClassAttribute_t1_835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
