﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.SignatureDescription
struct SignatureDescription_t1_1260;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.String
struct String_t;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct AsymmetricSignatureDeformatter_t1_1185;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Security.Cryptography.AsymmetricSignatureFormatter
struct AsymmetricSignatureFormatter_t1_1186;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.SignatureDescription::.ctor()
extern "C" void SignatureDescription__ctor_m1_10708 (SignatureDescription_t1_1260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::.ctor(System.Security.SecurityElement)
extern "C" void SignatureDescription__ctor_m1_10709 (SignatureDescription_t1_1260 * __this, SecurityElement_t1_242 * ___el, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.SignatureDescription::get_DeformatterAlgorithm()
extern "C" String_t* SignatureDescription_get_DeformatterAlgorithm_m1_10710 (SignatureDescription_t1_1260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DeformatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m1_10711 (SignatureDescription_t1_1260 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.SignatureDescription::get_DigestAlgorithm()
extern "C" String_t* SignatureDescription_get_DigestAlgorithm_m1_10712 (SignatureDescription_t1_1260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DigestAlgorithm(System.String)
extern "C" void SignatureDescription_set_DigestAlgorithm_m1_10713 (SignatureDescription_t1_1260 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.SignatureDescription::get_FormatterAlgorithm()
extern "C" String_t* SignatureDescription_get_FormatterAlgorithm_m1_10714 (SignatureDescription_t1_1260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_FormatterAlgorithm(System.String)
extern "C" void SignatureDescription_set_FormatterAlgorithm_m1_10715 (SignatureDescription_t1_1260 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.SignatureDescription::get_KeyAlgorithm()
extern "C" String_t* SignatureDescription_get_KeyAlgorithm_m1_10716 (SignatureDescription_t1_1260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_KeyAlgorithm(System.String)
extern "C" void SignatureDescription_set_KeyAlgorithm_m1_10717 (SignatureDescription_t1_1260 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricSignatureDeformatter System.Security.Cryptography.SignatureDescription::CreateDeformatter(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" AsymmetricSignatureDeformatter_t1_1185 * SignatureDescription_CreateDeformatter_m1_10718 (SignatureDescription_t1_1260 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm System.Security.Cryptography.SignatureDescription::CreateDigest()
extern "C" HashAlgorithm_t1_162 * SignatureDescription_CreateDigest_m1_10719 (SignatureDescription_t1_1260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricSignatureFormatter System.Security.Cryptography.SignatureDescription::CreateFormatter(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" AsymmetricSignatureFormatter_t1_1186 * SignatureDescription_CreateFormatter_m1_10720 (SignatureDescription_t1_1260 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
