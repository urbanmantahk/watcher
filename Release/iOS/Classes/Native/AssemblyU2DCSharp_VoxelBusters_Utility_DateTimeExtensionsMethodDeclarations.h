﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToDateTimeUTC(System.String,System.String)
extern "C" DateTime_t1_150  DateTimeExtensions_ToDateTimeUTC_m8_183 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToDateTimeLocal(System.String,System.String)
extern "C" DateTime_t1_150  DateTimeExtensions_ToDateTimeLocal_m8_184 (Object_t * __this /* static, unused */, String_t* ____string, String_t* ____format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToZuluFormatDateTimeUTC(System.String)
extern "C" DateTime_t1_150  DateTimeExtensions_ToZuluFormatDateTimeUTC_m8_185 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToZuluFormatDateTimeLocal(System.String)
extern "C" DateTime_t1_150  DateTimeExtensions_ToZuluFormatDateTimeLocal_m8_186 (Object_t * __this /* static, unused */, String_t* ____string, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.DateTimeExtensions::ToStringUsingZuluFormat(System.DateTime)
extern "C" String_t* DateTimeExtensions_ToStringUsingZuluFormat_m8_187 (Object_t * __this /* static, unused */, DateTime_t1_150  ____dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.Utility.DateTimeExtensions::ToDateTimeFromJavaTime(System.Int64)
extern "C" DateTime_t1_150  DateTimeExtensions_ToDateTimeFromJavaTime_m8_188 (Object_t * __this /* static, unused */, int64_t ____time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 VoxelBusters.Utility.DateTimeExtensions::ToJavaTimeFromDateTime(System.DateTime)
extern "C" int64_t DateTimeExtensions_ToJavaTimeFromDateTime_m8_189 (Object_t * __this /* static, unused */, DateTime_t1_150  ____dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
