﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_IO_IOException.h"

// System.IO.FileLoadException
struct  FileLoadException_t1_423  : public IOException_t1_413
{
	// System.String System.IO.FileLoadException::msg
	String_t* ___msg_13;
	// System.String System.IO.FileLoadException::fileName
	String_t* ___fileName_14;
	// System.String System.IO.FileLoadException::fusionLog
	String_t* ___fusionLog_15;
};
