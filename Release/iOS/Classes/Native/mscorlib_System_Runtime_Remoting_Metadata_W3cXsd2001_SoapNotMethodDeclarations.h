﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation
struct SoapNotation_t1_990;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::.ctor()
extern "C" void SoapNotation__ctor_m1_8864 (SoapNotation_t1_990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::.ctor(System.String)
extern "C" void SoapNotation__ctor_m1_8865 (SoapNotation_t1_990 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::get_Value()
extern "C" String_t* SoapNotation_get_Value_m1_8866 (SoapNotation_t1_990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::set_Value(System.String)
extern "C" void SoapNotation_set_Value_m1_8867 (SoapNotation_t1_990 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::get_XsdType()
extern "C" String_t* SoapNotation_get_XsdType_m1_8868 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::GetXsdType()
extern "C" String_t* SoapNotation_GetXsdType_m1_8869 (SoapNotation_t1_990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::Parse(System.String)
extern "C" SoapNotation_t1_990 * SoapNotation_Parse_m1_8870 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation::ToString()
extern "C" String_t* SoapNotation_ToString_m1_8871 (SoapNotation_t1_990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
