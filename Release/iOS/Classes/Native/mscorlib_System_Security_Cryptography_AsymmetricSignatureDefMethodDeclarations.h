﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct AsymmetricSignatureDeformatter_t1_1185;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::.ctor()
extern "C" void AsymmetricSignatureDeformatter__ctor_m1_10168 (AsymmetricSignatureDeformatter_t1_1185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.AsymmetricSignatureDeformatter::VerifySignature(System.Security.Cryptography.HashAlgorithm,System.Byte[])
extern "C" bool AsymmetricSignatureDeformatter_VerifySignature_m1_10169 (AsymmetricSignatureDeformatter_t1_1185 * __this, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
