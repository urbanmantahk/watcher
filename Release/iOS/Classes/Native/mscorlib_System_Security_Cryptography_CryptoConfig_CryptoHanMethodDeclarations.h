﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.CryptoConfig/CryptoHandler
struct CryptoHandler_t1_1190;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// Mono.Xml.SmallXmlParser
struct SmallXmlParser_t1_241;
// System.String
struct String_t;
// Mono.Xml.SmallXmlParser/IAttrList
struct IAttrList_t1_1676;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::.ctor(System.Collections.Hashtable,System.Collections.Hashtable)
extern "C" void CryptoHandler__ctor_m1_10185 (CryptoHandler_t1_1190 * __this, Hashtable_t1_100 * ___algorithms, Hashtable_t1_100 * ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnStartParsing(Mono.Xml.SmallXmlParser)
extern "C" void CryptoHandler_OnStartParsing_m1_10186 (CryptoHandler_t1_1190 * __this, SmallXmlParser_t1_241 * ___parser, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnEndParsing(Mono.Xml.SmallXmlParser)
extern "C" void CryptoHandler_OnEndParsing_m1_10187 (CryptoHandler_t1_1190 * __this, SmallXmlParser_t1_241 * ___parser, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.CryptoConfig/CryptoHandler::Get(Mono.Xml.SmallXmlParser/IAttrList,System.String)
extern "C" String_t* CryptoHandler_Get_m1_10188 (CryptoHandler_t1_1190 * __this, Object_t * ___attrs, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnStartElement(System.String,Mono.Xml.SmallXmlParser/IAttrList)
extern "C" void CryptoHandler_OnStartElement_m1_10189 (CryptoHandler_t1_1190 * __this, String_t* ___name, Object_t * ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnEndElement(System.String)
extern "C" void CryptoHandler_OnEndElement_m1_10190 (CryptoHandler_t1_1190 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnProcessingInstruction(System.String,System.String)
extern "C" void CryptoHandler_OnProcessingInstruction_m1_10191 (CryptoHandler_t1_1190 * __this, String_t* ___name, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnChars(System.String)
extern "C" void CryptoHandler_OnChars_m1_10192 (CryptoHandler_t1_1190 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.CryptoConfig/CryptoHandler::OnIgnorableWhitespace(System.String)
extern "C" void CryptoHandler_OnIgnorableWhitespace_m1_10193 (CryptoHandler_t1_1190 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
