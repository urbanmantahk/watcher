﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Xml.NameTable/Entry[]
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t4_109  : public Array_t { };
// System.Xml.XmlNode[]
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_t4_195  : public Array_t { };
// System.Xml.XPath.IXPathNavigable[]
// System.Xml.XPath.IXPathNavigable[]
struct IXPathNavigableU5BU5D_t4_196  : public Array_t { };
// System.Xml.XmlNamespaceManager/NsDecl[]
// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_t4_144  : public Array_t { };
// System.Xml.XmlNamespaceManager/NsScope[]
// System.Xml.XmlNamespaceManager/NsScope[]
struct NsScopeU5BU5D_t4_145  : public Array_t { };
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[]
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[]
struct XmlAttributeTokenInfoU5BU5D_t4_174  : public Array_t { };
// Mono.Xml2.XmlTextReader/XmlTokenInfo[]
// Mono.Xml2.XmlTextReader/XmlTokenInfo[]
struct XmlTokenInfoU5BU5D_t4_175  : public Array_t { };
// Mono.Xml2.XmlTextReader/TagName[]
// Mono.Xml2.XmlTextReader/TagName[]
struct TagNameU5BU5D_t4_176  : public Array_t { };
// System.Xml.XmlTextWriter/XmlNodeInfo[]
// System.Xml.XmlTextWriter/XmlNodeInfo[]
struct XmlNodeInfoU5BU5D_t4_183  : public Array_t { };
