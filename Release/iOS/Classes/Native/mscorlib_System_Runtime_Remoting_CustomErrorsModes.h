﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Remoting_CustomErrorsModes.h"

// System.Runtime.Remoting.CustomErrorsModes
struct  CustomErrorsModes_t1_1011 
{
	// System.Int32 System.Runtime.Remoting.CustomErrorsModes::value__
	int32_t ___value___1;
};
