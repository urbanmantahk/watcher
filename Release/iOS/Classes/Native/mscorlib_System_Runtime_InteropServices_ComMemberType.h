﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComMemberType.h"

// System.Runtime.InteropServices.ComMemberType
struct  ComMemberType_t1_773 
{
	// System.Int32 System.Runtime.InteropServices.ComMemberType::value__
	int32_t ___value___1;
};
