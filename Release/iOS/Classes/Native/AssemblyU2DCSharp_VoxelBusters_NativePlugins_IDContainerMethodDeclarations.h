﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.IDContainer
struct IDContainer_t8_328;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.PlatformID[]
struct PlatformIDU5BU5D_t8_329;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.IDContainer::.ctor()
extern "C" void IDContainer__ctor_m8_1912 (IDContainer_t8_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.IDContainer::.ctor(System.String,VoxelBusters.NativePlugins.PlatformID[])
extern "C" void IDContainer__ctor_m8_1913 (IDContainer_t8_328 * __this, String_t* ____globalID, PlatformIDU5BU5D_t8_329* ____platformIDs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.IDContainer::CompareGlobalID(System.String)
extern "C" bool IDContainer_CompareGlobalID_m8_1914 (IDContainer_t8_328 * __this, String_t* ____identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.IDContainer::GetGlobalID()
extern "C" String_t* IDContainer_GetGlobalID_m8_1915 (IDContainer_t8_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.IDContainer::CompareCurrentPlatformID(System.String)
extern "C" bool IDContainer_CompareCurrentPlatformID_m8_1916 (IDContainer_t8_328 * __this, String_t* ____identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.IDContainer::GetCurrentPlatformID()
extern "C" String_t* IDContainer_GetCurrentPlatformID_m8_1917 (IDContainer_t8_328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
