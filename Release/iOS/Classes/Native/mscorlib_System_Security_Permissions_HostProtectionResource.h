﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"

// System.Security.Permissions.HostProtectionResource
struct  HostProtectionResource_t1_1281 
{
	// System.Int32 System.Security.Permissions.HostProtectionResource::value__
	int32_t ___value___1;
};
