﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1_105;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Array
struct Array_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Globalization.Unicode.CodePointIndexer::.ctor(System.Int32[],System.Int32[],System.Int32,System.Int32)
extern "C" void CodePointIndexer__ctor_m1_1565 (CodePointIndexer_t1_105 * __this, Int32U5BU5D_t1_275* ___starts, Int32U5BU5D_t1_275* ___ends, int32_t ___defaultIndex, int32_t ___defaultCP, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array Mono.Globalization.Unicode.CodePointIndexer::CompressArray(System.Array,System.Type,Mono.Globalization.Unicode.CodePointIndexer)
extern "C" Array_t * CodePointIndexer_CompressArray_m1_1566 (Object_t * __this /* static, unused */, Array_t * ___source, Type_t * ___type, CodePointIndexer_t1_105 * ___indexer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.CodePointIndexer::ToIndex(System.Int32)
extern "C" int32_t CodePointIndexer_ToIndex_m1_1567 (CodePointIndexer_t1_105 * __this, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.CodePointIndexer::ToCodePoint(System.Int32)
extern "C" int32_t CodePointIndexer_ToCodePoint_m1_1568 (CodePointIndexer_t1_105 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
