﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void Context_t1_119_marshal ();
extern "C" void Context_t1_119_marshal_back ();
extern "C" void Context_t1_119_marshal_cleanup ();
extern "C" void PreviousInfo_t1_120_marshal ();
extern "C" void PreviousInfo_t1_120_marshal_back ();
extern "C" void PreviousInfo_t1_120_marshal_cleanup ();
extern "C" void Escape_t1_121_marshal ();
extern "C" void Escape_t1_121_marshal_back ();
extern "C" void Escape_t1_121_marshal_cleanup ();
extern "C" void UriScheme_t1_236_marshal ();
extern "C" void UriScheme_t1_236_marshal_back ();
extern "C" void UriScheme_t1_236_marshal_cleanup ();
extern "C" void AssemblyHash_t1_311_marshal ();
extern "C" void AssemblyHash_t1_311_marshal_back ();
extern "C" void AssemblyHash_t1_311_marshal_cleanup ();
extern "C" void MonoIOStat_t1_432_marshal ();
extern "C" void MonoIOStat_t1_432_marshal_back ();
extern "C" void MonoIOStat_t1_432_marshal_cleanup ();
extern "C" void RefEmitPermissionSet_t1_463_marshal ();
extern "C" void RefEmitPermissionSet_t1_463_marshal_back ();
extern "C" void RefEmitPermissionSet_t1_463_marshal_cleanup ();
extern "C" void MonoWin32Resource_t1_465_marshal ();
extern "C" void MonoWin32Resource_t1_465_marshal_back ();
extern "C" void MonoWin32Resource_t1_465_marshal_cleanup ();
extern "C" void ParameterModifier_t1_628_marshal ();
extern "C" void ParameterModifier_t1_628_marshal_back ();
extern "C" void ParameterModifier_t1_628_marshal_cleanup ();
extern "C" void ResourceInfo_t1_647_marshal ();
extern "C" void ResourceInfo_t1_647_marshal_back ();
extern "C" void ResourceInfo_t1_647_marshal_cleanup ();
extern "C" void DESCUNION_t1_726_marshal ();
extern "C" void DESCUNION_t1_726_marshal_back ();
extern "C" void DESCUNION_t1_726_marshal_cleanup ();
extern "C" void ELEMDESC_t1_729_marshal ();
extern "C" void ELEMDESC_t1_729_marshal_back ();
extern "C" void ELEMDESC_t1_729_marshal_cleanup ();
extern "C" void EXCEPINFO_t1_731_marshal ();
extern "C" void EXCEPINFO_t1_731_marshal_back ();
extern "C" void EXCEPINFO_t1_731_marshal_cleanup ();
extern "C" void FUNCDESC_t1_733_marshal ();
extern "C" void FUNCDESC_t1_733_marshal_back ();
extern "C" void FUNCDESC_t1_733_marshal_cleanup ();
extern "C" void IDLDESC_t1_727_marshal ();
extern "C" void IDLDESC_t1_727_marshal_back ();
extern "C" void IDLDESC_t1_727_marshal_cleanup ();
extern "C" void PARAMDESC_t1_728_marshal ();
extern "C" void PARAMDESC_t1_728_marshal_back ();
extern "C" void PARAMDESC_t1_728_marshal_cleanup ();
extern "C" void STATSTG_t1_741_marshal ();
extern "C" void STATSTG_t1_741_marshal_back ();
extern "C" void STATSTG_t1_741_marshal_cleanup ();
extern "C" void TYPEATTR_t1_743_marshal ();
extern "C" void TYPEATTR_t1_743_marshal_back ();
extern "C" void TYPEATTR_t1_743_marshal_cleanup ();
extern "C" void TYPELIBATTR_t1_746_marshal ();
extern "C" void TYPELIBATTR_t1_746_marshal_back ();
extern "C" void TYPELIBATTR_t1_746_marshal_cleanup ();
extern "C" void VARDESC_t1_748_marshal ();
extern "C" void VARDESC_t1_748_marshal_back ();
extern "C" void VARDESC_t1_748_marshal_cleanup ();
extern "C" void DESCUNION_t1_782_marshal ();
extern "C" void DESCUNION_t1_782_marshal_back ();
extern "C" void DESCUNION_t1_782_marshal_cleanup ();
extern "C" void ELEMDESC_t1_785_marshal ();
extern "C" void ELEMDESC_t1_785_marshal_back ();
extern "C" void ELEMDESC_t1_785_marshal_cleanup ();
extern "C" void EXCEPINFO_t1_787_marshal ();
extern "C" void EXCEPINFO_t1_787_marshal_back ();
extern "C" void EXCEPINFO_t1_787_marshal_cleanup ();
extern "C" void FUNCDESC_t1_792_marshal ();
extern "C" void FUNCDESC_t1_792_marshal_back ();
extern "C" void FUNCDESC_t1_792_marshal_cleanup ();
extern "C" void IDLDESC_t1_783_marshal ();
extern "C" void IDLDESC_t1_783_marshal_back ();
extern "C" void IDLDESC_t1_783_marshal_cleanup ();
extern "C" void PARAMDESC_t1_784_marshal ();
extern "C" void PARAMDESC_t1_784_marshal_back ();
extern "C" void PARAMDESC_t1_784_marshal_cleanup ();
extern "C" void STATSTG_t1_822_marshal ();
extern "C" void STATSTG_t1_822_marshal_back ();
extern "C" void STATSTG_t1_822_marshal_cleanup ();
extern "C" void TYPEATTR_t1_827_marshal ();
extern "C" void TYPEATTR_t1_827_marshal_back ();
extern "C" void TYPEATTR_t1_827_marshal_cleanup ();
extern "C" void TYPELIBATTR_t1_830_marshal ();
extern "C" void TYPELIBATTR_t1_830_marshal_back ();
extern "C" void TYPELIBATTR_t1_830_marshal_cleanup ();
extern "C" void VARDESC_t1_846_marshal ();
extern "C" void VARDESC_t1_846_marshal_back ();
extern "C" void VARDESC_t1_846_marshal_cleanup ();
extern "C" void DSAParameters_t1_1204_marshal ();
extern "C" void DSAParameters_t1_1204_marshal_back ();
extern "C" void DSAParameters_t1_1204_marshal_cleanup ();
extern "C" void RSAParameters_t1_1242_marshal ();
extern "C" void RSAParameters_t1_1242_marshal_back ();
extern "C" void RSAParameters_t1_1242_marshal_cleanup ();
extern "C" void ConsoleKeyInfo_t1_1516_marshal ();
extern "C" void ConsoleKeyInfo_t1_1516_marshal_back ();
extern "C" void ConsoleKeyInfo_t1_1516_marshal_cleanup ();
extern "C" void NsDecl_t4_141_marshal ();
extern "C" void NsDecl_t4_141_marshal_back ();
extern "C" void NsDecl_t4_141_marshal_cleanup ();
extern "C" void NsScope_t4_142_marshal ();
extern "C" void NsScope_t4_142_marshal_back ();
extern "C" void NsScope_t4_142_marshal_cleanup ();
extern "C" void TagName_t4_171_marshal ();
extern "C" void TagName_t4_171_marshal_back ();
extern "C" void TagName_t4_171_marshal_cleanup ();
extern "C" void X509ChainStatus_t3_157_marshal ();
extern "C" void X509ChainStatus_t3_157_marshal_back ();
extern "C" void X509ChainStatus_t3_157_marshal_cleanup ();
extern "C" void IntStack_t3_200_marshal ();
extern "C" void IntStack_t3_200_marshal_back ();
extern "C" void IntStack_t3_200_marshal_cleanup ();
extern "C" void Interval_t3_206_marshal ();
extern "C" void Interval_t3_206_marshal_back ();
extern "C" void Interval_t3_206_marshal_cleanup ();
extern "C" void UriScheme_t3_234_marshal ();
extern "C" void UriScheme_t3_234_marshal_back ();
extern "C" void UriScheme_t3_234_marshal_cleanup ();
extern "C" void WaitForSeconds_t6_10_marshal ();
extern "C" void WaitForSeconds_t6_10_marshal_back ();
extern "C" void WaitForSeconds_t6_10_marshal_cleanup ();
extern "C" void Coroutine_t6_14_marshal ();
extern "C" void Coroutine_t6_14_marshal_back ();
extern "C" void Coroutine_t6_14_marshal_cleanup ();
extern "C" void ScriptableObject_t6_15_marshal ();
extern "C" void ScriptableObject_t6_15_marshal_back ();
extern "C" void ScriptableObject_t6_15_marshal_cleanup ();
extern "C" void Gradient_t6_42_marshal ();
extern "C" void Gradient_t6_42_marshal_back ();
extern "C" void Gradient_t6_42_marshal_cleanup ();
extern "C" void CacheIndex_t6_81_marshal ();
extern "C" void CacheIndex_t6_81_marshal_back ();
extern "C" void CacheIndex_t6_81_marshal_cleanup ();
extern "C" void AsyncOperation_t6_2_marshal ();
extern "C" void AsyncOperation_t6_2_marshal_back ();
extern "C" void AsyncOperation_t6_2_marshal_cleanup ();
extern "C" void Touch_t6_94_marshal ();
extern "C" void Touch_t6_94_marshal_back ();
extern "C" void Touch_t6_94_marshal_cleanup ();
extern "C" void Object_t6_5_marshal ();
extern "C" void Object_t6_5_marshal_back ();
extern "C" void Object_t6_5_marshal_cleanup ();
extern "C" void YieldInstruction_t6_11_marshal ();
extern "C" void YieldInstruction_t6_11_marshal_back ();
extern "C" void YieldInstruction_t6_11_marshal_cleanup ();
extern "C" void WebCamDevice_t6_129_marshal ();
extern "C" void WebCamDevice_t6_129_marshal_back ();
extern "C" void WebCamDevice_t6_129_marshal_cleanup ();
extern "C" void AnimationCurve_t6_136_marshal ();
extern "C" void AnimationCurve_t6_136_marshal_back ();
extern "C" void AnimationCurve_t6_136_marshal_cleanup ();
extern "C" void AnimatorTransitionInfo_t6_138_marshal ();
extern "C" void AnimatorTransitionInfo_t6_138_marshal_back ();
extern "C" void AnimatorTransitionInfo_t6_138_marshal_cleanup ();
extern "C" void SkeletonBone_t6_140_marshal ();
extern "C" void SkeletonBone_t6_140_marshal_back ();
extern "C" void SkeletonBone_t6_140_marshal_cleanup ();
extern "C" void HumanBone_t6_142_marshal ();
extern "C" void HumanBone_t6_142_marshal_back ();
extern "C" void HumanBone_t6_142_marshal_cleanup ();
extern "C" void CharacterInfo_t6_147_marshal ();
extern "C" void CharacterInfo_t6_147_marshal_back ();
extern "C" void CharacterInfo_t6_147_marshal_cleanup ();
extern "C" void Event_t6_162_marshal ();
extern "C" void Event_t6_162_marshal_back ();
extern "C" void Event_t6_162_marshal_cleanup ();
extern "C" void GcAchievementData_t6_210_marshal ();
extern "C" void GcAchievementData_t6_210_marshal_back ();
extern "C" void GcAchievementData_t6_210_marshal_cleanup ();
extern "C" void GcScoreData_t6_211_marshal ();
extern "C" void GcScoreData_t6_211_marshal_back ();
extern "C" void GcScoreData_t6_211_marshal_cleanup ();
extern "C" void TrackedReference_t6_137_marshal ();
extern "C" void TrackedReference_t6_137_marshal_back ();
extern "C" void TrackedReference_t6_137_marshal_cleanup ();
extern "C" void ProductUpdateInfo_t8_21_marshal ();
extern "C" void ProductUpdateInfo_t8_21_marshal_back ();
extern "C" void ProductUpdateInfo_t8_21_marshal_cleanup ();
extern "C" void Feature_t8_24_marshal ();
extern "C" void Feature_t8_24_marshal_back ();
extern "C" void Feature_t8_24_marshal_cleanup ();
extern "C" void JSONString_t8_55_marshal ();
extern "C" void JSONString_t8_55_marshal_back ();
extern "C" void JSONString_t8_55_marshal_cleanup ();
extern "C" void ExifInterOperability_t8_113_marshal ();
extern "C" void ExifInterOperability_t8_113_marshal_back ();
extern "C" void ExifInterOperability_t8_113_marshal_cleanup ();
extern "C" void Fraction32_t8_127_marshal ();
extern "C" void Fraction32_t8_127_marshal_back ();
extern "C" void Fraction32_t8_127_marshal_cleanup ();
extern "C" void URL_t8_156_marshal ();
extern "C" void URL_t8_156_marshal_back ();
extern "C" void URL_t8_156_marshal_cleanup ();
extern "C" void ConsoleLog_t8_170_marshal ();
extern "C" void ConsoleLog_t8_170_marshal_back ();
extern "C" void ConsoleLog_t8_170_marshal_cleanup ();
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[67] = 
{
	{ Context_t1_119_marshal, Context_t1_119_marshal_back, Context_t1_119_marshal_cleanup },
	{ PreviousInfo_t1_120_marshal, PreviousInfo_t1_120_marshal_back, PreviousInfo_t1_120_marshal_cleanup },
	{ Escape_t1_121_marshal, Escape_t1_121_marshal_back, Escape_t1_121_marshal_cleanup },
	{ UriScheme_t1_236_marshal, UriScheme_t1_236_marshal_back, UriScheme_t1_236_marshal_cleanup },
	{ AssemblyHash_t1_311_marshal, AssemblyHash_t1_311_marshal_back, AssemblyHash_t1_311_marshal_cleanup },
	{ MonoIOStat_t1_432_marshal, MonoIOStat_t1_432_marshal_back, MonoIOStat_t1_432_marshal_cleanup },
	{ RefEmitPermissionSet_t1_463_marshal, RefEmitPermissionSet_t1_463_marshal_back, RefEmitPermissionSet_t1_463_marshal_cleanup },
	{ MonoWin32Resource_t1_465_marshal, MonoWin32Resource_t1_465_marshal_back, MonoWin32Resource_t1_465_marshal_cleanup },
	{ ParameterModifier_t1_628_marshal, ParameterModifier_t1_628_marshal_back, ParameterModifier_t1_628_marshal_cleanup },
	{ ResourceInfo_t1_647_marshal, ResourceInfo_t1_647_marshal_back, ResourceInfo_t1_647_marshal_cleanup },
	{ DESCUNION_t1_726_marshal, DESCUNION_t1_726_marshal_back, DESCUNION_t1_726_marshal_cleanup },
	{ ELEMDESC_t1_729_marshal, ELEMDESC_t1_729_marshal_back, ELEMDESC_t1_729_marshal_cleanup },
	{ EXCEPINFO_t1_731_marshal, EXCEPINFO_t1_731_marshal_back, EXCEPINFO_t1_731_marshal_cleanup },
	{ FUNCDESC_t1_733_marshal, FUNCDESC_t1_733_marshal_back, FUNCDESC_t1_733_marshal_cleanup },
	{ IDLDESC_t1_727_marshal, IDLDESC_t1_727_marshal_back, IDLDESC_t1_727_marshal_cleanup },
	{ PARAMDESC_t1_728_marshal, PARAMDESC_t1_728_marshal_back, PARAMDESC_t1_728_marshal_cleanup },
	{ STATSTG_t1_741_marshal, STATSTG_t1_741_marshal_back, STATSTG_t1_741_marshal_cleanup },
	{ TYPEATTR_t1_743_marshal, TYPEATTR_t1_743_marshal_back, TYPEATTR_t1_743_marshal_cleanup },
	{ TYPELIBATTR_t1_746_marshal, TYPELIBATTR_t1_746_marshal_back, TYPELIBATTR_t1_746_marshal_cleanup },
	{ VARDESC_t1_748_marshal, VARDESC_t1_748_marshal_back, VARDESC_t1_748_marshal_cleanup },
	{ DESCUNION_t1_782_marshal, DESCUNION_t1_782_marshal_back, DESCUNION_t1_782_marshal_cleanup },
	{ ELEMDESC_t1_785_marshal, ELEMDESC_t1_785_marshal_back, ELEMDESC_t1_785_marshal_cleanup },
	{ EXCEPINFO_t1_787_marshal, EXCEPINFO_t1_787_marshal_back, EXCEPINFO_t1_787_marshal_cleanup },
	{ FUNCDESC_t1_792_marshal, FUNCDESC_t1_792_marshal_back, FUNCDESC_t1_792_marshal_cleanup },
	{ IDLDESC_t1_783_marshal, IDLDESC_t1_783_marshal_back, IDLDESC_t1_783_marshal_cleanup },
	{ PARAMDESC_t1_784_marshal, PARAMDESC_t1_784_marshal_back, PARAMDESC_t1_784_marshal_cleanup },
	{ STATSTG_t1_822_marshal, STATSTG_t1_822_marshal_back, STATSTG_t1_822_marshal_cleanup },
	{ TYPEATTR_t1_827_marshal, TYPEATTR_t1_827_marshal_back, TYPEATTR_t1_827_marshal_cleanup },
	{ TYPELIBATTR_t1_830_marshal, TYPELIBATTR_t1_830_marshal_back, TYPELIBATTR_t1_830_marshal_cleanup },
	{ VARDESC_t1_846_marshal, VARDESC_t1_846_marshal_back, VARDESC_t1_846_marshal_cleanup },
	{ DSAParameters_t1_1204_marshal, DSAParameters_t1_1204_marshal_back, DSAParameters_t1_1204_marshal_cleanup },
	{ RSAParameters_t1_1242_marshal, RSAParameters_t1_1242_marshal_back, RSAParameters_t1_1242_marshal_cleanup },
	{ ConsoleKeyInfo_t1_1516_marshal, ConsoleKeyInfo_t1_1516_marshal_back, ConsoleKeyInfo_t1_1516_marshal_cleanup },
	{ NsDecl_t4_141_marshal, NsDecl_t4_141_marshal_back, NsDecl_t4_141_marshal_cleanup },
	{ NsScope_t4_142_marshal, NsScope_t4_142_marshal_back, NsScope_t4_142_marshal_cleanup },
	{ TagName_t4_171_marshal, TagName_t4_171_marshal_back, TagName_t4_171_marshal_cleanup },
	{ X509ChainStatus_t3_157_marshal, X509ChainStatus_t3_157_marshal_back, X509ChainStatus_t3_157_marshal_cleanup },
	{ IntStack_t3_200_marshal, IntStack_t3_200_marshal_back, IntStack_t3_200_marshal_cleanup },
	{ Interval_t3_206_marshal, Interval_t3_206_marshal_back, Interval_t3_206_marshal_cleanup },
	{ UriScheme_t3_234_marshal, UriScheme_t3_234_marshal_back, UriScheme_t3_234_marshal_cleanup },
	{ WaitForSeconds_t6_10_marshal, WaitForSeconds_t6_10_marshal_back, WaitForSeconds_t6_10_marshal_cleanup },
	{ Coroutine_t6_14_marshal, Coroutine_t6_14_marshal_back, Coroutine_t6_14_marshal_cleanup },
	{ ScriptableObject_t6_15_marshal, ScriptableObject_t6_15_marshal_back, ScriptableObject_t6_15_marshal_cleanup },
	{ Gradient_t6_42_marshal, Gradient_t6_42_marshal_back, Gradient_t6_42_marshal_cleanup },
	{ CacheIndex_t6_81_marshal, CacheIndex_t6_81_marshal_back, CacheIndex_t6_81_marshal_cleanup },
	{ AsyncOperation_t6_2_marshal, AsyncOperation_t6_2_marshal_back, AsyncOperation_t6_2_marshal_cleanup },
	{ Touch_t6_94_marshal, Touch_t6_94_marshal_back, Touch_t6_94_marshal_cleanup },
	{ Object_t6_5_marshal, Object_t6_5_marshal_back, Object_t6_5_marshal_cleanup },
	{ YieldInstruction_t6_11_marshal, YieldInstruction_t6_11_marshal_back, YieldInstruction_t6_11_marshal_cleanup },
	{ WebCamDevice_t6_129_marshal, WebCamDevice_t6_129_marshal_back, WebCamDevice_t6_129_marshal_cleanup },
	{ AnimationCurve_t6_136_marshal, AnimationCurve_t6_136_marshal_back, AnimationCurve_t6_136_marshal_cleanup },
	{ AnimatorTransitionInfo_t6_138_marshal, AnimatorTransitionInfo_t6_138_marshal_back, AnimatorTransitionInfo_t6_138_marshal_cleanup },
	{ SkeletonBone_t6_140_marshal, SkeletonBone_t6_140_marshal_back, SkeletonBone_t6_140_marshal_cleanup },
	{ HumanBone_t6_142_marshal, HumanBone_t6_142_marshal_back, HumanBone_t6_142_marshal_cleanup },
	{ CharacterInfo_t6_147_marshal, CharacterInfo_t6_147_marshal_back, CharacterInfo_t6_147_marshal_cleanup },
	{ Event_t6_162_marshal, Event_t6_162_marshal_back, Event_t6_162_marshal_cleanup },
	{ GcAchievementData_t6_210_marshal, GcAchievementData_t6_210_marshal_back, GcAchievementData_t6_210_marshal_cleanup },
	{ GcScoreData_t6_211_marshal, GcScoreData_t6_211_marshal_back, GcScoreData_t6_211_marshal_cleanup },
	{ TrackedReference_t6_137_marshal, TrackedReference_t6_137_marshal_back, TrackedReference_t6_137_marshal_cleanup },
	{ ProductUpdateInfo_t8_21_marshal, ProductUpdateInfo_t8_21_marshal_back, ProductUpdateInfo_t8_21_marshal_cleanup },
	{ Feature_t8_24_marshal, Feature_t8_24_marshal_back, Feature_t8_24_marshal_cleanup },
	{ JSONString_t8_55_marshal, JSONString_t8_55_marshal_back, JSONString_t8_55_marshal_cleanup },
	{ ExifInterOperability_t8_113_marshal, ExifInterOperability_t8_113_marshal_back, ExifInterOperability_t8_113_marshal_cleanup },
	{ Fraction32_t8_127_marshal, Fraction32_t8_127_marshal_back, Fraction32_t8_127_marshal_cleanup },
	{ URL_t8_156_marshal, URL_t8_156_marshal_back, URL_t8_156_marshal_cleanup },
	{ ConsoleLog_t8_170_marshal, ConsoleLog_t8_170_marshal_back, ConsoleLog_t8_170_marshal_cleanup },
	NULL,
};
