﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode
struct CleanupCode_t1_72;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode::.ctor(System.Object,System.IntPtr)
extern "C" void CleanupCode__ctor_m1_1382 (CleanupCode_t1_72 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode::Invoke(System.Object,System.Boolean)
extern "C" void CleanupCode_Invoke_m1_1383 (CleanupCode_t1_72 * __this, Object_t * ___userData, bool ___exceptionThrown, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CleanupCode_t1_72(Il2CppObject* delegate, Object_t * ___userData, bool ___exceptionThrown);
// System.IAsyncResult System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode::BeginInvoke(System.Object,System.Boolean,System.AsyncCallback,System.Object)
extern "C" Object_t * CleanupCode_BeginInvoke_m1_1384 (CleanupCode_t1_72 * __this, Object_t * ___userData, bool ___exceptionThrown, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers/CleanupCode::EndInvoke(System.IAsyncResult)
extern "C" void CleanupCode_EndInvoke_m1_1385 (CleanupCode_t1_72 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
