﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdNMToken.h"

// Mono.Xml.Schema.XsdNMTokens
struct  XsdNMTokens_t4_12  : public XsdNMToken_t4_11
{
};
