﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.DispatchWrapper
struct DispatchWrapper_t1_781;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.DispatchWrapper::.ctor(System.Object)
extern "C" void DispatchWrapper__ctor_m1_7633 (DispatchWrapper_t1_781 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.DispatchWrapper::get_WrappedObject()
extern "C" Object_t * DispatchWrapper_get_WrappedObject_m1_7634 (DispatchWrapper_t1_781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
