﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t4_187;
// System.Text.Encoding
struct Encoding_t1_406;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_ConformanceLevel.h"
#include "System_Xml_System_Xml_NewLineHandling.h"
#include "System_Xml_System_Xml_NamespaceHandling.h"

// System.Void System.Xml.XmlWriterSettings::.ctor()
extern "C" void XmlWriterSettings__ctor_m4_1033 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::Reset()
extern "C" void XmlWriterSettings_Reset_m4_1034 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_CheckCharacters()
extern "C" bool XmlWriterSettings_get_CheckCharacters_m4_1035 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_CloseOutput()
extern "C" bool XmlWriterSettings_get_CloseOutput_m4_1036 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::get_ConformanceLevel()
extern "C" int32_t XmlWriterSettings_get_ConformanceLevel_m4_1037 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::set_ConformanceLevel(System.Xml.ConformanceLevel)
extern "C" void XmlWriterSettings_set_ConformanceLevel_m4_1038 (XmlWriterSettings_t4_187 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Xml.XmlWriterSettings::get_Encoding()
extern "C" Encoding_t1_406 * XmlWriterSettings_get_Encoding_m4_1039 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::set_Encoding(System.Text.Encoding)
extern "C" void XmlWriterSettings_set_Encoding_m4_1040 (XmlWriterSettings_t4_187 * __this, Encoding_t1_406 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_Indent()
extern "C" bool XmlWriterSettings_get_Indent_m4_1041 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriterSettings::set_Indent(System.Boolean)
extern "C" void XmlWriterSettings_set_Indent_m4_1042 (XmlWriterSettings_t4_187 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWriterSettings::get_IndentChars()
extern "C" String_t* XmlWriterSettings_get_IndentChars_m4_1043 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWriterSettings::get_NewLineChars()
extern "C" String_t* XmlWriterSettings_get_NewLineChars_m4_1044 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_NewLineOnAttributes()
extern "C" bool XmlWriterSettings_get_NewLineOnAttributes_m4_1045 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.NewLineHandling System.Xml.XmlWriterSettings::get_NewLineHandling()
extern "C" int32_t XmlWriterSettings_get_NewLineHandling_m4_1046 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Xml.XmlWriterSettings::get_OmitXmlDeclaration()
extern "C" bool XmlWriterSettings_get_OmitXmlDeclaration_m4_1047 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.NamespaceHandling System.Xml.XmlWriterSettings::get_NamespaceHandling()
extern "C" int32_t XmlWriterSettings_get_NamespaceHandling_m4_1048 (XmlWriterSettings_t4_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
