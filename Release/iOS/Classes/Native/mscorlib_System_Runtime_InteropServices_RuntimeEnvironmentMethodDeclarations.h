﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.RuntimeEnvironment
struct RuntimeEnvironment_t1_820;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1_467;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.RuntimeEnvironment::.ctor()
extern "C" void RuntimeEnvironment__ctor_m1_7886 (RuntimeEnvironment_t1_820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.RuntimeEnvironment::get_SystemConfigurationFile()
extern "C" String_t* RuntimeEnvironment_get_SystemConfigurationFile_m1_7887 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.RuntimeEnvironment::FromGlobalAccessCache(System.Reflection.Assembly)
extern "C" bool RuntimeEnvironment_FromGlobalAccessCache_m1_7888 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.RuntimeEnvironment::GetRuntimeDirectory()
extern "C" String_t* RuntimeEnvironment_GetRuntimeDirectory_m1_7889 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.RuntimeEnvironment::GetSystemVersion()
extern "C" String_t* RuntimeEnvironment_GetSystemVersion_m1_7890 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
