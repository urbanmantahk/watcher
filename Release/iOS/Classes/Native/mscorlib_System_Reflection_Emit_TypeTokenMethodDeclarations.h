﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_TypeToken.h"

// System.Void System.Reflection.Emit.TypeToken::.ctor(System.Int32)
extern "C" void TypeToken__ctor_m1_6536 (TypeToken_t1_558 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeToken::.cctor()
extern "C" void TypeToken__cctor_m1_6537 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeToken::Equals(System.Object)
extern "C" bool TypeToken_Equals_m1_6538 (TypeToken_t1_558 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeToken::Equals(System.Reflection.Emit.TypeToken)
extern "C" bool TypeToken_Equals_m1_6539 (TypeToken_t1_558 * __this, TypeToken_t1_558  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.TypeToken::GetHashCode()
extern "C" int32_t TypeToken_GetHashCode_m1_6540 (TypeToken_t1_558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.TypeToken::get_Token()
extern "C" int32_t TypeToken_get_Token_m1_6541 (TypeToken_t1_558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeToken::op_Equality(System.Reflection.Emit.TypeToken,System.Reflection.Emit.TypeToken)
extern "C" bool TypeToken_op_Equality_m1_6542 (Object_t * __this /* static, unused */, TypeToken_t1_558  ___a, TypeToken_t1_558  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeToken::op_Inequality(System.Reflection.Emit.TypeToken,System.Reflection.Emit.TypeToken)
extern "C" bool TypeToken_op_Inequality_m1_6543 (Object_t * __this /* static, unused */, TypeToken_t1_558  ___a, TypeToken_t1_558  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
