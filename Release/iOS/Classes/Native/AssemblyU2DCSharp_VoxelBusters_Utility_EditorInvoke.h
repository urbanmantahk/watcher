﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>
struct Dictionary_2_t1_1907;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.EditorInvoke
struct  EditorInvoke_t8_150  : public Object_t
{
};
struct EditorInvoke_t8_150_StaticFields{
	// System.Double VoxelBusters.Utility.EditorInvoke::m_cachedTimeSinceStartup
	double ___m_cachedTimeSinceStartup_3;
	// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>> VoxelBusters.Utility.EditorInvoke::invokeMethodsContainer
	Dictionary_2_t1_1907 * ___invokeMethodsContainer_4;
};
