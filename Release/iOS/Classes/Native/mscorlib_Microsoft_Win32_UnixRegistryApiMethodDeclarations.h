﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.UnixRegistryApi
struct UnixRegistryApi_t1_101;
// System.String
struct String_t;
// Microsoft.Win32.RegistryKey
struct RegistryKey_t1_91;
// System.Object
struct Object_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Microsoft_Win32_RegistryHive.h"
#include "mscorlib_Microsoft_Win32_RegistryValueOptions.h"
#include "mscorlib_Microsoft_Win32_RegistryValueKind.h"

// System.Void Microsoft.Win32.UnixRegistryApi::.ctor()
extern "C" void UnixRegistryApi__ctor_m1_1508 (UnixRegistryApi_t1_101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.UnixRegistryApi::ToUnix(System.String)
extern "C" String_t* UnixRegistryApi_ToUnix_m1_1509 (Object_t * __this /* static, unused */, String_t* ___keyname, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.UnixRegistryApi::IsWellKnownKey(System.String,System.String)
extern "C" bool UnixRegistryApi_IsWellKnownKey_m1_1510 (Object_t * __this /* static, unused */, String_t* ___parentKeyName, String_t* ___keyname, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.UnixRegistryApi::CreateSubKey(Microsoft.Win32.RegistryKey,System.String)
extern "C" RegistryKey_t1_91 * UnixRegistryApi_CreateSubKey_m1_1511 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyname, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.UnixRegistryApi::OpenRemoteBaseKey(Microsoft.Win32.RegistryHive,System.String)
extern "C" RegistryKey_t1_91 * UnixRegistryApi_OpenRemoteBaseKey_m1_1512 (UnixRegistryApi_t1_101 * __this, int32_t ___hKey, String_t* ___machineName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.UnixRegistryApi::OpenSubKey(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" RegistryKey_t1_91 * UnixRegistryApi_OpenSubKey_m1_1513 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyname, bool ___writable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.UnixRegistryApi::Flush(Microsoft.Win32.RegistryKey)
extern "C" void UnixRegistryApi_Flush_m1_1514 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.UnixRegistryApi::Close(Microsoft.Win32.RegistryKey)
extern "C" void UnixRegistryApi_Close_m1_1515 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.UnixRegistryApi::GetValue(Microsoft.Win32.RegistryKey,System.String,System.Object,Microsoft.Win32.RegistryValueOptions)
extern "C" Object_t * UnixRegistryApi_GetValue_m1_1516 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, Object_t * ___default_value, int32_t ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.UnixRegistryApi::SetValue(Microsoft.Win32.RegistryKey,System.String,System.Object)
extern "C" void UnixRegistryApi_SetValue_m1_1517 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.UnixRegistryApi::SetValue(Microsoft.Win32.RegistryKey,System.String,System.Object,Microsoft.Win32.RegistryValueKind)
extern "C" void UnixRegistryApi_SetValue_m1_1518 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, Object_t * ___value, int32_t ___valueKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.UnixRegistryApi::SubKeyCount(Microsoft.Win32.RegistryKey)
extern "C" int32_t UnixRegistryApi_SubKeyCount_m1_1519 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.UnixRegistryApi::ValueCount(Microsoft.Win32.RegistryKey)
extern "C" int32_t UnixRegistryApi_ValueCount_m1_1520 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.UnixRegistryApi::DeleteValue(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" void UnixRegistryApi_DeleteValue_m1_1521 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, bool ___throw_if_missing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.UnixRegistryApi::DeleteKey(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" void UnixRegistryApi_DeleteKey_m1_1522 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyname, bool ___throw_if_missing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Microsoft.Win32.UnixRegistryApi::GetSubKeyNames(Microsoft.Win32.RegistryKey)
extern "C" StringU5BU5D_t1_238* UnixRegistryApi_GetSubKeyNames_m1_1523 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Microsoft.Win32.UnixRegistryApi::GetValueNames(Microsoft.Win32.RegistryKey)
extern "C" StringU5BU5D_t1_238* UnixRegistryApi_GetValueNames_m1_1524 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.UnixRegistryApi::ToString(Microsoft.Win32.RegistryKey)
extern "C" String_t* UnixRegistryApi_ToString_m1_1525 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.UnixRegistryApi::CreateSubKey(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" RegistryKey_t1_91 * UnixRegistryApi_CreateSubKey_m1_1526 (UnixRegistryApi_t1_101 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyname, bool ___writable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
