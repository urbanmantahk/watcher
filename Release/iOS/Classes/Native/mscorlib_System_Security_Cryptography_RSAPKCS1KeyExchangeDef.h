﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;

#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeD.h"

// System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter
struct  RSAPKCS1KeyExchangeDeformatter_t1_1238  : public AsymmetricKeyExchangeDeformatter_t1_1183
{
	// System.Security.Cryptography.RSA System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::rsa
	RSA_t1_175 * ___rsa_0;
	// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::random
	RandomNumberGenerator_t1_143 * ___random_1;
};
