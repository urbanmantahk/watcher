﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.MonoTrustManager
struct MonoTrustManager_t1_1353;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t1_1333;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t1_1365;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.MonoTrustManager::.ctor()
extern "C" void MonoTrustManager__ctor_m1_11552 (MonoTrustManager_t1_1353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.Policy.MonoTrustManager::DetermineApplicationTrust(System.ActivationContext,System.Security.Policy.TrustManagerContext)
extern "C" ApplicationTrust_t1_1333 * MonoTrustManager_DetermineApplicationTrust_m1_11553 (MonoTrustManager_t1_1353 * __this, ActivationContext_t1_717 * ___activationContext, TrustManagerContext_t1_1365 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.MonoTrustManager::FromXml(System.Security.SecurityElement)
extern "C" void MonoTrustManager_FromXml_m1_11554 (MonoTrustManager_t1_1353 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.MonoTrustManager::ToXml()
extern "C" SecurityElement_t1_242 * MonoTrustManager_ToXml_m1_11555 (MonoTrustManager_t1_1353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
