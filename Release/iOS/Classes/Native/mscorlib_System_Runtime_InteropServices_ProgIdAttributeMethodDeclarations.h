﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ProgIdAttribute
struct ProgIdAttribute_t1_816;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ProgIdAttribute::.ctor(System.String)
extern "C" void ProgIdAttribute__ctor_m1_7873 (ProgIdAttribute_t1_816 * __this, String_t* ___progId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.ProgIdAttribute::get_Value()
extern "C" String_t* ProgIdAttribute_get_Value_m1_7874 (ProgIdAttribute_t1_816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
