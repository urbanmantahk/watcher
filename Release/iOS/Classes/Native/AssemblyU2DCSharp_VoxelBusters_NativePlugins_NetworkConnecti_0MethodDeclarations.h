﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings
struct AndroidSettings_t8_255;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::.ctor()
extern "C" void AndroidSettings__ctor_m8_1407 (AndroidSettings_t8_255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_Port()
extern "C" int32_t AndroidSettings_get_Port_m8_1408 (AndroidSettings_t8_255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_Port(System.Int32)
extern "C" void AndroidSettings_set_Port_m8_1409 (AndroidSettings_t8_255 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_TimeOutPeriod()
extern "C" int32_t AndroidSettings_get_TimeOutPeriod_m8_1410 (AndroidSettings_t8_255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_TimeOutPeriod(System.Int32)
extern "C" void AndroidSettings_set_TimeOutPeriod_m8_1411 (AndroidSettings_t8_255 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_MaxRetryCount()
extern "C" int32_t AndroidSettings_get_MaxRetryCount_m8_1412 (AndroidSettings_t8_255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_MaxRetryCount(System.Int32)
extern "C" void AndroidSettings_set_MaxRetryCount_m8_1413 (AndroidSettings_t8_255 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::get_TimeGapBetweenPolling()
extern "C" float AndroidSettings_get_TimeGapBetweenPolling_m8_1414 (AndroidSettings_t8_255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::set_TimeGapBetweenPolling(System.Single)
extern "C" void AndroidSettings_set_TimeGapBetweenPolling_m8_1415 (AndroidSettings_t8_255 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
