﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Security_AccessControl_EventWaitHandleRights.h"

// System.Security.AccessControl.EventWaitHandleAccessRule
struct  EventWaitHandleAccessRule_t1_1149  : public AccessRule_t1_1111
{
	// System.Security.AccessControl.EventWaitHandleRights System.Security.AccessControl.EventWaitHandleAccessRule::rights
	int32_t ___rights_6;
};
