﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ExceptionFilterSink
struct ExceptionFilterSink_t1_873;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.ExceptionFilterSink::.ctor(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void ExceptionFilterSink__ctor_m1_8059 (ExceptionFilterSink_t1_873 * __this, Object_t * ___call, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.ExceptionFilterSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ExceptionFilterSink_SyncProcessMessage_m1_8060 (ExceptionFilterSink_t1_873 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Channels.ExceptionFilterSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * ExceptionFilterSink_AsyncProcessMessage_m1_8061 (ExceptionFilterSink_t1_873 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ExceptionFilterSink::get_NextSink()
extern "C" Object_t * ExceptionFilterSink_get_NextSink_m1_8062 (ExceptionFilterSink_t1_873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
