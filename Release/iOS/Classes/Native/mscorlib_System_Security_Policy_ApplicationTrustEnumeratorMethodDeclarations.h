﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ApplicationTrustEnumerator
struct ApplicationTrustEnumerator_t1_1336;
// System.Security.Policy.ApplicationTrustCollection
struct ApplicationTrustCollection_t1_1331;
// System.Object
struct Object_t;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t1_1333;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.ApplicationTrustEnumerator::.ctor(System.Security.Policy.ApplicationTrustCollection)
extern "C" void ApplicationTrustEnumerator__ctor_m1_11392 (ApplicationTrustEnumerator_t1_1336 * __this, ApplicationTrustCollection_t1_1331 * ___collection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.ApplicationTrustEnumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * ApplicationTrustEnumerator_System_Collections_IEnumerator_get_Current_m1_11393 (ApplicationTrustEnumerator_t1_1336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.Policy.ApplicationTrustEnumerator::get_Current()
extern "C" ApplicationTrust_t1_1333 * ApplicationTrustEnumerator_get_Current_m1_11394 (ApplicationTrustEnumerator_t1_1336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationTrustEnumerator::MoveNext()
extern "C" bool ApplicationTrustEnumerator_MoveNext_m1_11395 (ApplicationTrustEnumerator_t1_1336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustEnumerator::Reset()
extern "C" void ApplicationTrustEnumerator_Reset_m1_11396 (ApplicationTrustEnumerator_t1_1336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
