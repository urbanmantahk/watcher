﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7/SignedData
struct SignedData_t1_226;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Mono.Security.ASN1
struct ASN1_t1_149;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1_148;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1_222;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.String
struct String_t;
// Mono.Security.PKCS7/SignerInfo
struct SignerInfo_t1_227;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7/SignedData::.ctor()
extern "C" void SignedData__ctor_m1_2516 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignedData::.ctor(System.Byte[])
extern "C" void SignedData__ctor_m1_2517 (SignedData_t1_226 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignedData::.ctor(Mono.Security.ASN1)
extern "C" void SignedData__ctor_m1_2518 (SignedData_t1_226 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/SignedData::get_ASN1()
extern "C" ASN1_t1_149 * SignedData_get_ASN1_m1_2519 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.PKCS7/SignedData::get_Certificates()
extern "C" X509CertificateCollection_t1_148 * SignedData_get_Certificates_m1_2520 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.PKCS7/SignedData::get_ContentInfo()
extern "C" ContentInfo_t1_222 * SignedData_get_ContentInfo_m1_2521 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.PKCS7/SignedData::get_Crls()
extern "C" ArrayList_t1_170 * SignedData_get_Crls_m1_2522 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.PKCS7/SignedData::get_HashName()
extern "C" String_t* SignedData_get_HashName_m1_2523 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignedData::set_HashName(System.String)
extern "C" void SignedData_set_HashName_m1_2524 (SignedData_t1_226 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/SignerInfo Mono.Security.PKCS7/SignedData::get_SignerInfo()
extern "C" SignerInfo_t1_227 * SignedData_get_SignerInfo_m1_2525 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.PKCS7/SignedData::get_Version()
extern "C" uint8_t SignedData_get_Version_m1_2526 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignedData::set_Version(System.Byte)
extern "C" void SignedData_set_Version_m1_2527 (SignedData_t1_226 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.PKCS7/SignedData::get_UseAuthenticatedAttributes()
extern "C" bool SignedData_get_UseAuthenticatedAttributes_m1_2528 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignedData::set_UseAuthenticatedAttributes(System.Boolean)
extern "C" void SignedData_set_UseAuthenticatedAttributes_m1_2529 (SignedData_t1_226 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.PKCS7/SignedData::VerifySignature(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" bool SignedData_VerifySignature_m1_2530 (SignedData_t1_226 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.PKCS7/SignedData::OidToName(System.String)
extern "C" String_t* SignedData_OidToName_m1_2531 (SignedData_t1_226 * __this, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/SignedData::GetASN1()
extern "C" ASN1_t1_149 * SignedData_GetASN1_m1_2532 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/SignedData::GetBytes()
extern "C" ByteU5BU5D_t1_109* SignedData_GetBytes_m1_2533 (SignedData_t1_226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
