﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CompoundAce
struct CompoundAce_t1_1135;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"
#include "mscorlib_System_Security_AccessControl_CompoundAceType.h"

// System.Void System.Security.AccessControl.CompoundAce::.ctor(System.Security.AccessControl.AceFlags,System.Int32,System.Security.AccessControl.CompoundAceType,System.Security.Principal.SecurityIdentifier)
extern "C" void CompoundAce__ctor_m1_9779 (CompoundAce_t1_1135 * __this, uint8_t ___flags, int32_t ___accessMask, int32_t ___compoundAceType, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CompoundAce::get_BinaryLength()
extern "C" int32_t CompoundAce_get_BinaryLength_m1_9780 (CompoundAce_t1_1135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.CompoundAceType System.Security.AccessControl.CompoundAce::get_CompoundAceType()
extern "C" int32_t CompoundAce_get_CompoundAceType_m1_9781 (CompoundAce_t1_1135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CompoundAce::set_CompoundAceType(System.Security.AccessControl.CompoundAceType)
extern "C" void CompoundAce_set_CompoundAceType_m1_9782 (CompoundAce_t1_1135 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CompoundAce::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void CompoundAce_GetBinaryForm_m1_9783 (CompoundAce_t1_1135 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
