﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t1_2071;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t1_2779;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_1748;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1_2778;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t1_1752;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t1_1750;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct IComparer_1_t1_2780;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t1_2077;
// System.Action`1<System.Reflection.CustomAttributeTypedArgument>
struct Action_1_t1_2078;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t1_2079;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m1_16379_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1__ctor_m1_16379(__this, method) (( void (*) (List_1_t1_2071 *, const MethodInfo*))List_1__ctor_m1_16379_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m1_16380_gshared (List_1_t1_2071 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m1_16380(__this, ___collection, method) (( void (*) (List_1_t1_2071 *, Object_t*, const MethodInfo*))List_1__ctor_m1_16380_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m1_16381_gshared (List_1_t1_2071 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m1_16381(__this, ___capacity, method) (( void (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1__ctor_m1_16381_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[],System.Int32)
extern "C" void List_1__ctor_m1_16382_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___data, int32_t ___size, const MethodInfo* method);
#define List_1__ctor_m1_16382(__this, ___data, ___size, method) (( void (*) (List_1_t1_2071 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, int32_t, const MethodInfo*))List_1__ctor_m1_16382_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m1_16383_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1_16383(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_16383_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_16384_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_16384(__this, method) (( Object_t* (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_16384_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m1_16385_gshared (List_1_t1_2071 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1_16385(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_2071 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_16385_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m1_16386_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_16386(__this, method) (( Object_t * (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_16386_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m1_16387_gshared (List_1_t1_2071 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1_16387(__this, ___item, method) (( int32_t (*) (List_1_t1_2071 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_16387_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m1_16388_gshared (List_1_t1_2071 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1_16388(__this, ___item, method) (( bool (*) (List_1_t1_2071 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_16388_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m1_16389_gshared (List_1_t1_2071 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1_16389(__this, ___item, method) (( int32_t (*) (List_1_t1_2071 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_16389_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m1_16390_gshared (List_1_t1_2071 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1_16390(__this, ___index, ___item, method) (( void (*) (List_1_t1_2071 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_16390_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m1_16391_gshared (List_1_t1_2071 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1_16391(__this, ___item, method) (( void (*) (List_1_t1_2071 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_16391_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16392_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16392(__this, method) (( bool (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16392_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m1_16393_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_16393(__this, method) (( bool (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_16393_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m1_16394_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_16394(__this, method) (( Object_t * (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_16394_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m1_16395_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1_16395(__this, method) (( bool (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_16395_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m1_16396_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1_16396(__this, method) (( bool (*) (List_1_t1_2071 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_16396_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m1_16397_gshared (List_1_t1_2071 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1_16397(__this, ___index, method) (( Object_t * (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_16397_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m1_16398_gshared (List_1_t1_2071 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1_16398(__this, ___index, ___value, method) (( void (*) (List_1_t1_2071 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_16398_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m1_16399_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_Add_m1_16399(__this, ___item, method) (( void (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_Add_m1_16399_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m1_16400_gshared (List_1_t1_2071 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1_16400(__this, ___newCount, method) (( void (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_16400_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckRange(System.Int32,System.Int32)
extern "C" void List_1_CheckRange_m1_16401_gshared (List_1_t1_2071 * __this, int32_t ___idx, int32_t ___count, const MethodInfo* method);
#define List_1_CheckRange_m1_16401(__this, ___idx, ___count, method) (( void (*) (List_1_t1_2071 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_16401_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m1_16402_gshared (List_1_t1_2071 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m1_16402(__this, ___collection, method) (( void (*) (List_1_t1_2071 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_16402_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m1_16403_gshared (List_1_t1_2071 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m1_16403(__this, ___enumerable, method) (( void (*) (List_1_t1_2071 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_16403_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m1_16404_gshared (List_1_t1_2071 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m1_16404(__this, ___collection, method) (( void (*) (List_1_t1_2071 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_16404_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1_1750 * List_1_AsReadOnly_m1_16405_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1_16405(__this, method) (( ReadOnlyCollection_1_t1_1750 * (*) (List_1_t1_2071 *, const MethodInfo*))List_1_AsReadOnly_m1_16405_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::BinarySearch(T)
extern "C" int32_t List_1_BinarySearch_m1_16406_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_BinarySearch_m1_16406(__this, ___item, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_BinarySearch_m1_16406_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_16407_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_16407(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_16407_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
extern "C" int32_t List_1_BinarySearch_m1_16408_gshared (List_1_t1_2071 * __this, int32_t ___index, int32_t ___count, CustomAttributeTypedArgument_t1_594  ___item, Object_t* ___comparer, const MethodInfo* method);
#define List_1_BinarySearch_m1_16408(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, int32_t, CustomAttributeTypedArgument_t1_594 , Object_t*, const MethodInfo*))List_1_BinarySearch_m1_16408_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m1_16409_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_Clear_m1_16409(__this, method) (( void (*) (List_1_t1_2071 *, const MethodInfo*))List_1_Clear_m1_16409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m1_16410_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_Contains_m1_16410(__this, ___item, method) (( bool (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_Contains_m1_16410_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[])
extern "C" void List_1_CopyTo_m1_16411_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, const MethodInfo* method);
#define List_1_CopyTo_m1_16411(__this, ___array, method) (( void (*) (List_1_t1_2071 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, const MethodInfo*))List_1_CopyTo_m1_16411_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m1_16412_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m1_16412(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_2071 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, int32_t, const MethodInfo*))List_1_CopyTo_m1_16412_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
extern "C" void List_1_CopyTo_m1_16413_gshared (List_1_t1_2071 * __this, int32_t ___index, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method);
#define List_1_CopyTo_m1_16413(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_2071 *, int32_t, CustomAttributeTypedArgumentU5BU5D_t1_1748*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_16413_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Exists(System.Predicate`1<T>)
extern "C" bool List_1_Exists_m1_16414_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_Exists_m1_16414(__this, ___match, method) (( bool (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_Exists_m1_16414_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeTypedArgument_t1_594  List_1_Find_m1_16415_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_Find_m1_16415(__this, ___match, method) (( CustomAttributeTypedArgument_t1_594  (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_Find_m1_16415_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m1_16416_gshared (Object_t * __this /* static, unused */, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m1_16416(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2077 *, const MethodInfo*))List_1_CheckMatch_m1_16416_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindAll(System.Predicate`1<T>)
extern "C" List_1_t1_2071 * List_1_FindAll_m1_16417_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindAll_m1_16417(__this, ___match, method) (( List_1_t1_2071 * (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindAll_m1_16417_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindAllStackBits(System.Predicate`1<T>)
extern "C" List_1_t1_2071 * List_1_FindAllStackBits_m1_16418_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindAllStackBits_m1_16418(__this, ___match, method) (( List_1_t1_2071 * (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindAllStackBits_m1_16418_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindAllList(System.Predicate`1<T>)
extern "C" List_1_t1_2071 * List_1_FindAllList_m1_16419_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindAllList_m1_16419(__this, ___match, method) (( List_1_t1_2071 * (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindAllList_m1_16419_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_16420_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_16420(__this, ___match, method) (( int32_t (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindIndex_m1_16420_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_16421_gshared (List_1_t1_2071 * __this, int32_t ___startIndex, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_16421(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindIndex_m1_16421_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindIndex_m1_16422_gshared (List_1_t1_2071 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindIndex_m1_16422(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, int32_t, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindIndex_m1_16422_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m1_16423_gshared (List_1_t1_2071 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m1_16423(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, int32_t, Predicate_1_t1_2077 *, const MethodInfo*))List_1_GetIndex_m1_16423_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindLast(System.Predicate`1<T>)
extern "C" CustomAttributeTypedArgument_t1_594  List_1_FindLast_m1_16424_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindLast_m1_16424(__this, ___match, method) (( CustomAttributeTypedArgument_t1_594  (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindLast_m1_16424_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindLastIndex(System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_16425_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_16425(__this, ___match, method) (( int32_t (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindLastIndex_m1_16425_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindLastIndex(System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_16426_gshared (List_1_t1_2071 * __this, int32_t ___startIndex, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_16426(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindLastIndex_m1_16426_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_FindLastIndex_m1_16427_gshared (List_1_t1_2071 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_FindLastIndex_m1_16427(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, int32_t, Predicate_1_t1_2077 *, const MethodInfo*))List_1_FindLastIndex_m1_16427_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetLastIndex_m1_16428_gshared (List_1_t1_2071 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_GetLastIndex_m1_16428(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_2071 *, int32_t, int32_t, Predicate_1_t1_2077 *, const MethodInfo*))List_1_GetLastIndex_m1_16428_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ForEach(System.Action`1<T>)
extern "C" void List_1_ForEach_m1_16429_gshared (List_1_t1_2071 * __this, Action_1_t1_2078 * ___action, const MethodInfo* method);
#define List_1_ForEach_m1_16429(__this, ___action, method) (( void (*) (List_1_t1_2071 *, Action_1_t1_2078 *, const MethodInfo*))List_1_ForEach_m1_16429_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t1_2072  List_1_GetEnumerator_m1_16430_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1_16430(__this, method) (( Enumerator_t1_2072  (*) (List_1_t1_2071 *, const MethodInfo*))List_1_GetEnumerator_m1_16430_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetRange(System.Int32,System.Int32)
extern "C" List_1_t1_2071 * List_1_GetRange_m1_16431_gshared (List_1_t1_2071 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_GetRange_m1_16431(__this, ___index, ___count, method) (( List_1_t1_2071 * (*) (List_1_t1_2071 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_16431_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m1_16432_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_IndexOf_m1_16432(__this, ___item, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_IndexOf_m1_16432_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_16433_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_IndexOf_m1_16433(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , int32_t, const MethodInfo*))List_1_IndexOf_m1_16433_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_IndexOf_m1_16434_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_IndexOf_m1_16434(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_16434_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m1_16435_gshared (List_1_t1_2071 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m1_16435(__this, ___start, ___delta, method) (( void (*) (List_1_t1_2071 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_16435_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m1_16436_gshared (List_1_t1_2071 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m1_16436(__this, ___index, method) (( void (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_16436_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m1_16437_gshared (List_1_t1_2071 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_Insert_m1_16437(__this, ___index, ___item, method) (( void (*) (List_1_t1_2071 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_Insert_m1_16437_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m1_16438_gshared (List_1_t1_2071 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m1_16438(__this, ___collection, method) (( void (*) (List_1_t1_2071 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_16438_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertRange_m1_16439_gshared (List_1_t1_2071 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertRange_m1_16439(__this, ___index, ___collection, method) (( void (*) (List_1_t1_2071 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_16439_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_InsertCollection_m1_16440_gshared (List_1_t1_2071 * __this, int32_t ___index, Object_t* ___collection, const MethodInfo* method);
#define List_1_InsertCollection_m1_16440(__this, ___index, ___collection, method) (( void (*) (List_1_t1_2071 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_16440_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_InsertEnumeration_m1_16441_gshared (List_1_t1_2071 * __this, int32_t ___index, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_InsertEnumeration_m1_16441(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_2071 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_16441_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::LastIndexOf(T)
extern "C" int32_t List_1_LastIndexOf_m1_16442_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_LastIndexOf_m1_16442(__this, ___item, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_LastIndexOf_m1_16442_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::LastIndexOf(T,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_16443_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, int32_t ___index, const MethodInfo* method);
#define List_1_LastIndexOf_m1_16443(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , int32_t, const MethodInfo*))List_1_LastIndexOf_m1_16443_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::LastIndexOf(T,System.Int32,System.Int32)
extern "C" int32_t List_1_LastIndexOf_m1_16444_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_LastIndexOf_m1_16444(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_16444_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m1_16445_gshared (List_1_t1_2071 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define List_1_Remove_m1_16445(__this, ___item, method) (( bool (*) (List_1_t1_2071 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_Remove_m1_16445_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m1_16446_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m1_16446(__this, ___match, method) (( int32_t (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_RemoveAll_m1_16446_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m1_16447_gshared (List_1_t1_2071 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m1_16447(__this, ___index, method) (( void (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_16447_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveRange(System.Int32,System.Int32)
extern "C" void List_1_RemoveRange_m1_16448_gshared (List_1_t1_2071 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_RemoveRange_m1_16448(__this, ___index, ___count, method) (( void (*) (List_1_t1_2071 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_16448_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse()
extern "C" void List_1_Reverse_m1_16449_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_Reverse_m1_16449(__this, method) (( void (*) (List_1_t1_2071 *, const MethodInfo*))List_1_Reverse_m1_16449_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse(System.Int32,System.Int32)
extern "C" void List_1_Reverse_m1_16450_gshared (List_1_t1_2071 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define List_1_Reverse_m1_16450(__this, ___index, ___count, method) (( void (*) (List_1_t1_2071 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_16450_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort()
extern "C" void List_1_Sort_m1_16451_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_Sort_m1_16451(__this, method) (( void (*) (List_1_t1_2071 *, const MethodInfo*))List_1_Sort_m1_16451_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_16452_gshared (List_1_t1_2071 * __this, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_16452(__this, ___comparer, method) (( void (*) (List_1_t1_2071 *, Object_t*, const MethodInfo*))List_1_Sort_m1_16452_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1_16453_gshared (List_1_t1_2071 * __this, Comparison_1_t1_2079 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1_16453(__this, ___comparison, method) (( void (*) (List_1_t1_2071 *, Comparison_1_t1_2079 *, const MethodInfo*))List_1_Sort_m1_16453_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern "C" void List_1_Sort_m1_16454_gshared (List_1_t1_2071 * __this, int32_t ___index, int32_t ___count, Object_t* ___comparer, const MethodInfo* method);
#define List_1_Sort_m1_16454(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_2071 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_16454_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t1_1748* List_1_ToArray_m1_16455_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_ToArray_m1_16455(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t1_1748* (*) (List_1_t1_2071 *, const MethodInfo*))List_1_ToArray_m1_16455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m1_16456_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1_16456(__this, method) (( void (*) (List_1_t1_2071 *, const MethodInfo*))List_1_TrimExcess_m1_16456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrueForAll(System.Predicate`1<T>)
extern "C" bool List_1_TrueForAll_m1_16457_gshared (List_1_t1_2071 * __this, Predicate_1_t1_2077 * ___match, const MethodInfo* method);
#define List_1_TrueForAll_m1_16457(__this, ___match, method) (( bool (*) (List_1_t1_2071 *, Predicate_1_t1_2077 *, const MethodInfo*))List_1_TrueForAll_m1_16457_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1_16458_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1_16458(__this, method) (( int32_t (*) (List_1_t1_2071 *, const MethodInfo*))List_1_get_Capacity_m1_16458_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1_16459_gshared (List_1_t1_2071 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1_16459(__this, ___value, method) (( void (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_16459_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m1_16460_gshared (List_1_t1_2071 * __this, const MethodInfo* method);
#define List_1_get_Count_m1_16460(__this, method) (( int32_t (*) (List_1_t1_2071 *, const MethodInfo*))List_1_get_Count_m1_16460_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_594  List_1_get_Item_m1_16461_gshared (List_1_t1_2071 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m1_16461(__this, ___index, method) (( CustomAttributeTypedArgument_t1_594  (*) (List_1_t1_2071 *, int32_t, const MethodInfo*))List_1_get_Item_m1_16461_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m1_16462_gshared (List_1_t1_2071 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___value, const MethodInfo* method);
#define List_1_set_Item_m1_16462(__this, ___index, ___value, method) (( void (*) (List_1_t1_2071 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))List_1_set_Item_m1_16462_gshared)(__this, ___index, ___value, method)
