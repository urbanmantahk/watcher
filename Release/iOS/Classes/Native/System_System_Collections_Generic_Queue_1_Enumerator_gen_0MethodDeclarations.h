﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m3_2258(__this, ___q, method) (( void (*) (Enumerator_t3_289 *, Queue_1_t3_249 *, const MethodInfo*))Enumerator__ctor_m3_1861_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3_2259(__this, method) (( void (*) (Enumerator_t3_289 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1862_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_2260(__this, method) (( Object_t * (*) (Enumerator_t3_289 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1863_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>::Dispose()
#define Enumerator_Dispose_m3_2261(__this, method) (( void (*) (Enumerator_t3_289 *, const MethodInfo*))Enumerator_Dispose_m3_1864_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>::MoveNext()
#define Enumerator_MoveNext_m3_2262(__this, method) (( bool (*) (Enumerator_t3_289 *, const MethodInfo*))Enumerator_MoveNext_m3_1865_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<ExifLibrary.ExifProperty>::get_Current()
#define Enumerator_get_Current_m3_2263(__this, method) (( ExifProperty_t8_99 * (*) (Enumerator_t3_289 *, const MethodInfo*))Enumerator_get_Current_m3_1866_gshared)(__this, method)
