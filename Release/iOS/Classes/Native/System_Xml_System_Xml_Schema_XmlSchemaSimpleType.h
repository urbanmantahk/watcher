﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t4_63;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t4_69;

#include "System_Xml_System_Xml_Schema_XmlSchemaType.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMethod.h"

// System.Xml.Schema.XmlSchemaSimpleType
struct  XmlSchemaSimpleType_t4_63  : public XmlSchemaType_t4_64
{
	// System.Xml.Schema.XmlSchemaSimpleTypeContent System.Xml.Schema.XmlSchemaSimpleType::content
	XmlSchemaSimpleTypeContent_t4_69 * ___content_10;
	// System.Boolean System.Xml.Schema.XmlSchemaSimpleType::islocal
	bool ___islocal_11;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaSimpleType::variety
	int32_t ___variety_12;
};
struct XmlSchemaSimpleType_t4_63_StaticFields{
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::schemaLocationType
	XmlSchemaSimpleType_t4_63 * ___schemaLocationType_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnySimpleType
	XmlSchemaSimpleType_t4_63 * ___XsAnySimpleType_13;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsString
	XmlSchemaSimpleType_t4_63 * ___XsString_14;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBoolean
	XmlSchemaSimpleType_t4_63 * ___XsBoolean_15;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDecimal
	XmlSchemaSimpleType_t4_63 * ___XsDecimal_16;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsFloat
	XmlSchemaSimpleType_t4_63 * ___XsFloat_17;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDouble
	XmlSchemaSimpleType_t4_63 * ___XsDouble_18;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDuration
	XmlSchemaSimpleType_t4_63 * ___XsDuration_19;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDateTime
	XmlSchemaSimpleType_t4_63 * ___XsDateTime_20;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsTime
	XmlSchemaSimpleType_t4_63 * ___XsTime_21;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDate
	XmlSchemaSimpleType_t4_63 * ___XsDate_22;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYearMonth
	XmlSchemaSimpleType_t4_63 * ___XsGYearMonth_23;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYear
	XmlSchemaSimpleType_t4_63 * ___XsGYear_24;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonthDay
	XmlSchemaSimpleType_t4_63 * ___XsGMonthDay_25;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGDay
	XmlSchemaSimpleType_t4_63 * ___XsGDay_26;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonth
	XmlSchemaSimpleType_t4_63 * ___XsGMonth_27;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsHexBinary
	XmlSchemaSimpleType_t4_63 * ___XsHexBinary_28;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBase64Binary
	XmlSchemaSimpleType_t4_63 * ___XsBase64Binary_29;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnyUri
	XmlSchemaSimpleType_t4_63 * ___XsAnyUri_30;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsQName
	XmlSchemaSimpleType_t4_63 * ___XsQName_31;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNotation
	XmlSchemaSimpleType_t4_63 * ___XsNotation_32;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNormalizedString
	XmlSchemaSimpleType_t4_63 * ___XsNormalizedString_33;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsToken
	XmlSchemaSimpleType_t4_63 * ___XsToken_34;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLanguage
	XmlSchemaSimpleType_t4_63 * ___XsLanguage_35;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMToken
	XmlSchemaSimpleType_t4_63 * ___XsNMToken_36;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMTokens
	XmlSchemaSimpleType_t4_63 * ___XsNMTokens_37;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsName
	XmlSchemaSimpleType_t4_63 * ___XsName_38;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNCName
	XmlSchemaSimpleType_t4_63 * ___XsNCName_39;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsID
	XmlSchemaSimpleType_t4_63 * ___XsID_40;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRef
	XmlSchemaSimpleType_t4_63 * ___XsIDRef_41;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRefs
	XmlSchemaSimpleType_t4_63 * ___XsIDRefs_42;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntity
	XmlSchemaSimpleType_t4_63 * ___XsEntity_43;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntities
	XmlSchemaSimpleType_t4_63 * ___XsEntities_44;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInteger
	XmlSchemaSimpleType_t4_63 * ___XsInteger_45;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonPositiveInteger
	XmlSchemaSimpleType_t4_63 * ___XsNonPositiveInteger_46;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNegativeInteger
	XmlSchemaSimpleType_t4_63 * ___XsNegativeInteger_47;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLong
	XmlSchemaSimpleType_t4_63 * ___XsLong_48;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInt
	XmlSchemaSimpleType_t4_63 * ___XsInt_49;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsShort
	XmlSchemaSimpleType_t4_63 * ___XsShort_50;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsByte
	XmlSchemaSimpleType_t4_63 * ___XsByte_51;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonNegativeInteger
	XmlSchemaSimpleType_t4_63 * ___XsNonNegativeInteger_52;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedLong
	XmlSchemaSimpleType_t4_63 * ___XsUnsignedLong_53;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedInt
	XmlSchemaSimpleType_t4_63 * ___XsUnsignedInt_54;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedShort
	XmlSchemaSimpleType_t4_63 * ___XsUnsignedShort_55;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedByte
	XmlSchemaSimpleType_t4_63 * ___XsUnsignedByte_56;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsPositiveInteger
	XmlSchemaSimpleType_t4_63 * ___XsPositiveInteger_57;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtUntypedAtomic
	XmlSchemaSimpleType_t4_63 * ___XdtUntypedAtomic_58;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtAnyAtomicType
	XmlSchemaSimpleType_t4_63 * ___XdtAnyAtomicType_59;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtYearMonthDuration
	XmlSchemaSimpleType_t4_63 * ___XdtYearMonthDuration_60;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtDayTimeDuration
	XmlSchemaSimpleType_t4_63 * ___XdtDayTimeDuration_61;
};
