﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.WeakReference
struct WeakReference_t1_1014;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_GCCollectionMode.h"

// System.Int32 System.GC::get_MaxGeneration()
extern "C" int32_t GC_get_MaxGeneration_m1_14103 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::InternalCollect(System.Int32)
extern "C" void GC_InternalCollect_m1_14104 (Object_t * __this /* static, unused */, int32_t ___generation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::Collect()
extern "C" void GC_Collect_m1_14105 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::Collect(System.Int32)
extern "C" void GC_Collect_m1_14106 (Object_t * __this /* static, unused */, int32_t ___generation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::Collect(System.Int32,System.GCCollectionMode)
extern "C" void GC_Collect_m1_14107 (Object_t * __this /* static, unused */, int32_t ___generation, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.GC::GetGeneration(System.Object)
extern "C" int32_t GC_GetGeneration_m1_14108 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.GC::GetGeneration(System.WeakReference)
extern "C" int32_t GC_GetGeneration_m1_14109 (Object_t * __this /* static, unused */, WeakReference_t1_1014 * ___wo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.GC::GetTotalMemory(System.Boolean)
extern "C" int64_t GC_GetTotalMemory_m1_14110 (Object_t * __this /* static, unused */, bool ___forceFullCollection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::KeepAlive(System.Object)
extern "C" void GC_KeepAlive_m1_14111 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::ReRegisterForFinalize(System.Object)
extern "C" void GC_ReRegisterForFinalize_m1_14112 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C" void GC_SuppressFinalize_m1_14113 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::WaitForPendingFinalizers()
extern "C" void GC_WaitForPendingFinalizers_m1_14114 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.GC::CollectionCount(System.Int32)
extern "C" int32_t GC_CollectionCount_m1_14115 (Object_t * __this /* static, unused */, int32_t ___generation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::RecordPressure(System.Int64)
extern "C" void GC_RecordPressure_m1_14116 (Object_t * __this /* static, unused */, int64_t ___bytesAllocated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::AddMemoryPressure(System.Int64)
extern "C" void GC_AddMemoryPressure_m1_14117 (Object_t * __this /* static, unused */, int64_t ___bytesAllocated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::RemoveMemoryPressure(System.Int64)
extern "C" void GC_RemoveMemoryPressure_m1_14118 (Object_t * __this /* static, unused */, int64_t ___bytesAllocated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
