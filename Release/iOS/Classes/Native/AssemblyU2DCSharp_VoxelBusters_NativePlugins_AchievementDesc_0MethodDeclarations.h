﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AchievementDescription
struct AchievementDescription_t8_225;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.NativePlugins.AchievementDescription::.ctor()
extern "C" void AchievementDescription__ctor_m8_1267 (AchievementDescription_t8_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::add_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void AchievementDescription_add_DownloadImageFinishedEvent_m8_1268 (AchievementDescription_t8_225 * __this, Completion_t8_161 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::remove_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void AchievementDescription_remove_DownloadImageFinishedEvent_m8_1269 (AchievementDescription_t8_225 * __this, Completion_t8_161 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AchievementDescription::get_GlobalIdentifier()
extern "C" String_t* AchievementDescription_get_GlobalIdentifier_m8_1270 (AchievementDescription_t8_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::set_GlobalIdentifier(System.String)
extern "C" void AchievementDescription_set_GlobalIdentifier_m8_1271 (AchievementDescription_t8_225 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AchievementDescription::get_InstanceID()
extern "C" String_t* AchievementDescription_get_InstanceID_m8_1272 (AchievementDescription_t8_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::set_InstanceID(System.String)
extern "C" void AchievementDescription_set_InstanceID_m8_1273 (AchievementDescription_t8_225 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::GetImageAsync(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void AchievementDescription_GetImageAsync_m8_1274 (AchievementDescription_t8_225 * __this, Completion_t8_161 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImage()
extern "C" void AchievementDescription_RequestForImage_m8_1275 (AchievementDescription_t8_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.AchievementDescription::ToString()
extern "C" String_t* AchievementDescription_ToString_m8_1276 (AchievementDescription_t8_225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImageFinished(System.Collections.IDictionary)
extern "C" void AchievementDescription_RequestForImageFinished_m8_1277 (AchievementDescription_t8_225 * __this, Object_t * ____dataDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::RequestForImageFinished(VoxelBusters.Utility.URL,System.String)
extern "C" void AchievementDescription_RequestForImageFinished_m8_1278 (AchievementDescription_t8_225 * __this, URL_t8_156  ____imagePathURL, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::DownloadImageFinished(UnityEngine.Texture2D,System.String)
extern "C" void AchievementDescription_DownloadImageFinished_m8_1279 (AchievementDescription_t8_225 * __this, Texture2D_t6_33 * ____image, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AchievementDescription::<RequestForImageFinished>m__A(UnityEngine.Texture2D,System.String)
extern "C" void AchievementDescription_U3CRequestForImageFinishedU3Em__A_m8_1280 (AchievementDescription_t8_225 * __this, Texture2D_t6_33 * ____image, String_t* ____downloadError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
