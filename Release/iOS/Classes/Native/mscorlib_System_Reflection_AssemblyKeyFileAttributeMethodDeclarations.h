﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyKeyFileAttribute
struct AssemblyKeyFileAttribute_t1_574;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
extern "C" void AssemblyKeyFileAttribute__ctor_m1_6691 (AssemblyKeyFileAttribute_t1_574 * __this, String_t* ___keyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyKeyFileAttribute::get_KeyFile()
extern "C" String_t* AssemblyKeyFileAttribute_get_KeyFile_m1_6692 (AssemblyKeyFileAttribute_t1_574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
