﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.GPSTimeStamp
struct GPSTimeStamp_t8_108;
// ExifLibrary.MathEx/UFraction32[]
struct UFraction32U5BU5D_t8_122;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"

// System.Void ExifLibrary.GPSTimeStamp::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSTimeStamp__ctor_m8_466 (GPSTimeStamp_t8_108 * __this, int32_t ___tag, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32[] ExifLibrary.GPSTimeStamp::get_Value()
extern "C" UFraction32U5BU5D_t8_122* GPSTimeStamp_get_Value_m8_467 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSTimeStamp::set_Value(ExifLibrary.MathEx/UFraction32[])
extern "C" void GPSTimeStamp_set_Value_m8_468 (GPSTimeStamp_t8_108 * __this, UFraction32U5BU5D_t8_122* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSTimeStamp::get_Hour()
extern "C" UFraction32_t8_121  GPSTimeStamp_get_Hour_m8_469 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSTimeStamp::set_Hour(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSTimeStamp_set_Hour_m8_470 (GPSTimeStamp_t8_108 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSTimeStamp::get_Minute()
extern "C" UFraction32_t8_121  GPSTimeStamp_get_Minute_m8_471 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSTimeStamp::set_Minute(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSTimeStamp_set_Minute_m8_472 (GPSTimeStamp_t8_108 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.GPSTimeStamp::get_Second()
extern "C" UFraction32_t8_121  GPSTimeStamp_get_Second_m8_473 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.GPSTimeStamp::set_Second(ExifLibrary.MathEx/UFraction32)
extern "C" void GPSTimeStamp_set_Second_m8_474 (GPSTimeStamp_t8_108 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.GPSTimeStamp::ToString()
extern "C" String_t* GPSTimeStamp_ToString_m8_475 (GPSTimeStamp_t8_108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
