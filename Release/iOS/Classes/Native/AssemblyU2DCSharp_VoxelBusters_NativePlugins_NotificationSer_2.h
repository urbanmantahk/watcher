﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion
struct RegisterForRemoteNotificationCompletion_t8_257;
// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse
struct ReceivedNotificationResponse_t8_258;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.NotificationService
struct  NotificationService_t8_261  : public MonoBehaviour_t6_91
{
	// System.String VoxelBusters.NativePlugins.NotificationService::notificationDefaultSoundName
	String_t* ___notificationDefaultSoundName_2;
	// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.NotificationService::m_launchLocalNotification
	CrossPlatformNotification_t8_259 * ___m_launchLocalNotification_3;
	// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.NotificationService::m_launchRemoteNotification
	CrossPlatformNotification_t8_259 * ___m_launchRemoteNotification_4;
	// System.Boolean VoxelBusters.NativePlugins.NotificationService::m_receivedAppLaunchInfo
	bool ___m_receivedAppLaunchInfo_5;
};
struct NotificationService_t8_261_StaticFields{
	// VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion VoxelBusters.NativePlugins.NotificationService::DidFinishRegisterForRemoteNotificationEvent
	RegisterForRemoteNotificationCompletion_t8_257 * ___DidFinishRegisterForRemoteNotificationEvent_6;
	// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse VoxelBusters.NativePlugins.NotificationService::DidLaunchWithLocalNotificationEvent
	ReceivedNotificationResponse_t8_258 * ___DidLaunchWithLocalNotificationEvent_7;
	// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse VoxelBusters.NativePlugins.NotificationService::DidLaunchWithRemoteNotificationEvent
	ReceivedNotificationResponse_t8_258 * ___DidLaunchWithRemoteNotificationEvent_8;
	// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse VoxelBusters.NativePlugins.NotificationService::DidReceiveLocalNotificationEvent
	ReceivedNotificationResponse_t8_258 * ___DidReceiveLocalNotificationEvent_9;
	// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse VoxelBusters.NativePlugins.NotificationService::DidReceiveRemoteNotificationEvent
	ReceivedNotificationResponse_t8_258 * ___DidReceiveRemoteNotificationEvent_10;
};
