﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ImportedFromTypeLibAttribute
struct ImportedFromTypeLibAttribute_t1_803;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ImportedFromTypeLibAttribute::.ctor(System.String)
extern "C" void ImportedFromTypeLibAttribute__ctor_m1_7680 (ImportedFromTypeLibAttribute_t1_803 * __this, String_t* ___tlbFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.ImportedFromTypeLibAttribute::get_Value()
extern "C" String_t* ImportedFromTypeLibAttribute_get_Value_m1_7681 (ImportedFromTypeLibAttribute_t1_803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
