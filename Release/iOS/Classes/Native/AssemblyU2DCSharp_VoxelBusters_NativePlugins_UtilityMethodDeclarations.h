﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Utility
struct Utility_t8_306;
// VoxelBusters.NativePlugins.RateMyApp
struct RateMyApp_t8_307;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Utility::.ctor()
extern "C" void Utility__ctor_m8_1792 (Utility_t8_306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.RateMyApp VoxelBusters.NativePlugins.Utility::get_RateMyApp()
extern "C" RateMyApp_t8_307 * Utility_get_RateMyApp_m8_1793 (Utility_t8_306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Utility::set_RateMyApp(VoxelBusters.NativePlugins.RateMyApp)
extern "C" void Utility_set_RateMyApp_m8_1794 (Utility_t8_306 * __this, RateMyApp_t8_307 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Utility::Awake()
extern "C" void Utility_Awake_m8_1795 (Utility_t8_306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Utility::GetUUID()
extern "C" String_t* Utility_GetUUID_m8_1796 (Utility_t8_306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Utility::OpenStoreLink(System.String)
extern "C" void Utility_OpenStoreLink_m8_1797 (Utility_t8_306 * __this, String_t* ____applicationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Utility::SetApplicationIconBadgeNumber(System.Int32)
extern "C" void Utility_SetApplicationIconBadgeNumber_m8_1798 (Utility_t8_306 * __this, int32_t ____badgeNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Utility::GetBundleVersion()
extern "C" String_t* Utility_GetBundleVersion_m8_1799 (Utility_t8_306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Utility::GetBundleIdentifier()
extern "C" String_t* Utility_GetBundleIdentifier_m8_1800 (Utility_t8_306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
