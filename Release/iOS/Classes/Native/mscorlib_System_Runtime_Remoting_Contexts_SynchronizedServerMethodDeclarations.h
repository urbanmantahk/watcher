﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.SynchronizedServerContextSink
struct SynchronizedServerContextSink_t1_903;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
struct SynchronizationAttribute_t1_900;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.SynchronizedServerContextSink::.ctor(System.Runtime.Remoting.Messaging.IMessageSink,System.Runtime.Remoting.Contexts.SynchronizationAttribute)
extern "C" void SynchronizedServerContextSink__ctor_m1_8208 (SynchronizedServerContextSink_t1_903 * __this, Object_t * ___next, SynchronizationAttribute_t1_900 * ___att, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.SynchronizedServerContextSink::get_NextSink()
extern "C" Object_t * SynchronizedServerContextSink_get_NextSink_m1_8209 (SynchronizedServerContextSink_t1_903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Contexts.SynchronizedServerContextSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * SynchronizedServerContextSink_AsyncProcessMessage_m1_8210 (SynchronizedServerContextSink_t1_903 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Contexts.SynchronizedServerContextSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * SynchronizedServerContextSink_SyncProcessMessage_m1_8211 (SynchronizedServerContextSink_t1_903 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
