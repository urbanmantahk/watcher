﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.MessageShareComposer
struct  MessageShareComposer_t8_281  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.MessageShareComposer::<Body>k__BackingField
	String_t* ___U3CBodyU3Ek__BackingField_0;
	// System.String[] VoxelBusters.NativePlugins.MessageShareComposer::<ToRecipients>k__BackingField
	StringU5BU5D_t1_238* ___U3CToRecipientsU3Ek__BackingField_1;
};
