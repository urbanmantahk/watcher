﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Globalization.Unicode.ContractionComparer
struct ContractionComparer_t1_110;

#include "mscorlib_System_Object.h"

// Mono.Globalization.Unicode.ContractionComparer
struct  ContractionComparer_t1_110  : public Object_t
{
};
struct ContractionComparer_t1_110_StaticFields{
	// Mono.Globalization.Unicode.ContractionComparer Mono.Globalization.Unicode.ContractionComparer::Instance
	ContractionComparer_t1_110 * ___Instance_0;
};
