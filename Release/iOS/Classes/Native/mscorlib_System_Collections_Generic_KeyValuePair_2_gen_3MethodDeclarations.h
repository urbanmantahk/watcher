﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_26534(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1924 *, int32_t, ExifProperty_t8_99 *, const MethodInfo*))KeyValuePair_2__ctor_m1_26432_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Key()
#define KeyValuePair_2_get_Key_m1_15047(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1924 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_26433_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_26535(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1924 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_26434_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Value()
#define KeyValuePair_2_get_Value_m1_15046(__this, method) (( ExifProperty_t8_99 * (*) (KeyValuePair_2_t1_1924 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_26435_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_26536(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1924 *, ExifProperty_t8_99 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_26436_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::ToString()
#define KeyValuePair_2_ToString_m1_26537(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1924 *, const MethodInfo*))KeyValuePair_2_ToString_m1_26437_gshared)(__this, method)
