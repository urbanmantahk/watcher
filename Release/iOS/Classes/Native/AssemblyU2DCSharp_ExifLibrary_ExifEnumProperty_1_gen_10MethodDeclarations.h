﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>
struct ExifEnumProperty_1_t8_349;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_SensingMethod.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1986_gshared (ExifEnumProperty_1_t8_349 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1986(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_349 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1986_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2171_gshared (ExifEnumProperty_1_t8_349 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2171(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_349 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2171_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2172_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2172(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_349 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2172_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2173_gshared (ExifEnumProperty_1_t8_349 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2173(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_349 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2173_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2174_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2174(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_349 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2174_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2175_gshared (ExifEnumProperty_1_t8_349 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2175(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_349 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2175_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2176_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2176(__this, method) (( bool (*) (ExifEnumProperty_1_t8_349 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2176_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2177_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2177(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_349 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2177_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2178_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2178(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_349 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2178_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2179_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_349 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2179(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_349 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2179_gshared)(__this /* static, unused */, ___obj, method)
