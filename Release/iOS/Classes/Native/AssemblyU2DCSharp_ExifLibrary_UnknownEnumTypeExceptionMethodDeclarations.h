﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.UnknownEnumTypeException
struct UnknownEnumTypeException_t8_96;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLibrary.UnknownEnumTypeException::.ctor()
extern "C" void UnknownEnumTypeException__ctor_m8_407 (UnknownEnumTypeException_t8_96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.UnknownEnumTypeException::.ctor(System.String)
extern "C" void UnknownEnumTypeException__ctor_m8_408 (UnknownEnumTypeException_t8_96 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
