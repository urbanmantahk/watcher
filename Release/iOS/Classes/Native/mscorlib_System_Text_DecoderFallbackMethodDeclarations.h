﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.DecoderFallback
struct DecoderFallback_t1_1420;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.DecoderFallback::.ctor()
extern "C" void DecoderFallback__ctor_m1_12224 (DecoderFallback_t1_1420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallback::.cctor()
extern "C" void DecoderFallback__cctor_m1_12225 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ExceptionFallback()
extern "C" DecoderFallback_t1_1420 * DecoderFallback_get_ExceptionFallback_m1_12226 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ReplacementFallback()
extern "C" DecoderFallback_t1_1420 * DecoderFallback_get_ReplacementFallback_m1_12227 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_StandardSafeFallback()
extern "C" DecoderFallback_t1_1420 * DecoderFallback_get_StandardSafeFallback_m1_12228 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
