﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>
struct  ValueEnumerator_t3_284 
{
	// System.Collections.Generic.SortedList`2<TKey,TValue> System.Collections.Generic.SortedList`2/ValueEnumerator::l
	SortedList_2_t3_248 * ___l_0;
	// System.Int32 System.Collections.Generic.SortedList`2/ValueEnumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.SortedList`2/ValueEnumerator::ver
	int32_t ___ver_2;
};
