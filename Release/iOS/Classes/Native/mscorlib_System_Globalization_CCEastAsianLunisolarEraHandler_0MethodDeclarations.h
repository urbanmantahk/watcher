﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.CCEastAsianLunisolarEraHandler::.ctor()
extern "C" void CCEastAsianLunisolarEraHandler__ctor_m1_3813 (CCEastAsianLunisolarEraHandler_t1_355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.CCEastAsianLunisolarEraHandler::get_Eras()
extern "C" Int32U5BU5D_t1_275* CCEastAsianLunisolarEraHandler_get_Eras_m1_3814 (CCEastAsianLunisolarEraHandler_t1_355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCEastAsianLunisolarEraHandler::appendEra(System.Int32,System.Int32,System.Int32)
extern "C" void CCEastAsianLunisolarEraHandler_appendEra_m1_3815 (CCEastAsianLunisolarEraHandler_t1_355 * __this, int32_t ___nr, int32_t ___rd_start, int32_t ___rd_end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCEastAsianLunisolarEraHandler::appendEra(System.Int32,System.Int32)
extern "C" void CCEastAsianLunisolarEraHandler_appendEra_m1_3816 (CCEastAsianLunisolarEraHandler_t1_355 * __this, int32_t ___nr, int32_t ___rd_start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarEraHandler::GregorianYear(System.Int32,System.Int32)
extern "C" int32_t CCEastAsianLunisolarEraHandler_GregorianYear_m1_3817 (CCEastAsianLunisolarEraHandler_t1_355 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarEraHandler::EraYear(System.Int32&,System.Int32)
extern "C" int32_t CCEastAsianLunisolarEraHandler_EraYear_m1_3818 (CCEastAsianLunisolarEraHandler_t1_355 * __this, int32_t* ___era, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCEastAsianLunisolarEraHandler::CheckDateTime(System.DateTime)
extern "C" void CCEastAsianLunisolarEraHandler_CheckDateTime_m1_3819 (CCEastAsianLunisolarEraHandler_t1_355 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCEastAsianLunisolarEraHandler::ValidDate(System.Int32)
extern "C" bool CCEastAsianLunisolarEraHandler_ValidDate_m1_3820 (CCEastAsianLunisolarEraHandler_t1_355 * __this, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCEastAsianLunisolarEraHandler::ValidEra(System.Int32)
extern "C" bool CCEastAsianLunisolarEraHandler_ValidEra_m1_3821 (CCEastAsianLunisolarEraHandler_t1_355 * __this, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
