﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TypedReference.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Boolean System.TypedReference::Equals(System.Object)
extern "C" bool TypedReference_Equals_m1_1350 (TypedReference_t1_67 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TypedReference::GetHashCode()
extern "C" int32_t TypedReference_GetHashCode_m1_1351 (TypedReference_t1_67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.TypedReference::GetTargetType(System.TypedReference)
extern "C" Type_t * TypedReference_GetTargetType_m1_1352 (Object_t * __this /* static, unused */, TypedReference_t1_67  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypedReference System.TypedReference::MakeTypedReference(System.Object,System.Reflection.FieldInfo[])
extern "C" TypedReference_t1_67  TypedReference_MakeTypedReference_m1_1353 (Object_t * __this /* static, unused */, Object_t * ___target, FieldInfoU5BU5D_t1_1668* ___flds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.TypedReference::SetTypedReference(System.TypedReference,System.Object)
extern "C" void TypedReference_SetTypedReference_m1_1354 (Object_t * __this /* static, unused */, TypedReference_t1_67  ___target, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.TypedReference::TargetTypeToken(System.TypedReference)
extern "C" RuntimeTypeHandle_t1_30  TypedReference_TargetTypeToken_m1_1355 (Object_t * __this /* static, unused */, TypedReference_t1_67  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.TypedReference::ToObject(System.TypedReference)
extern "C" Object_t * TypedReference_ToObject_m1_1356 (Object_t * __this /* static, unused */, TypedReference_t1_67  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
