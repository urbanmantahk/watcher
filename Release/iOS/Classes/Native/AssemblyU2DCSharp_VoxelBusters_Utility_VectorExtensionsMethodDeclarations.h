﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector2 VoxelBusters.Utility.VectorExtensions::Rotate(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t6_47  VectorExtensions_Rotate_m8_248 (Object_t * __this /* static, unused */, Vector2_t6_47  ____vec, float ____angleDeg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
