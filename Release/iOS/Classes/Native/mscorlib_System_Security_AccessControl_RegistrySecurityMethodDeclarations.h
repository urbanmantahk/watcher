﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.RegistrySecurity
struct RegistrySecurity_t1_1175;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.RegistryAccessRule
struct RegistryAccessRule_t1_1172;
// System.Security.AccessControl.RegistryAuditRule
struct RegistryAuditRule_t1_1173;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.RegistrySecurity::.ctor()
extern "C" void RegistrySecurity__ctor_m1_10072 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.RegistrySecurity::get_AccessRightType()
extern "C" Type_t * RegistrySecurity_get_AccessRightType_m1_10073 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.RegistrySecurity::get_AccessRuleType()
extern "C" Type_t * RegistrySecurity_get_AccessRuleType_m1_10074 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.RegistrySecurity::get_AuditRuleType()
extern "C" Type_t * RegistrySecurity_get_AuditRuleType_m1_10075 (RegistrySecurity_t1_1175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessRule System.Security.AccessControl.RegistrySecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" AccessRule_t1_1111 * RegistrySecurity_AccessRuleFactory_m1_10076 (RegistrySecurity_t1_1175 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::AddAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern "C" void RegistrySecurity_AddAccessRule_m1_10077 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::AddAuditRule(System.Security.AccessControl.RegistryAuditRule)
extern "C" void RegistrySecurity_AddAuditRule_m1_10078 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditRule System.Security.AccessControl.RegistrySecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" AuditRule_t1_1119 * RegistrySecurity_AuditRuleFactory_m1_10079 (RegistrySecurity_t1_1175 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.RegistrySecurity::RemoveAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern "C" bool RegistrySecurity_RemoveAccessRule_m1_10080 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAccessRuleAll(System.Security.AccessControl.RegistryAccessRule)
extern "C" void RegistrySecurity_RemoveAccessRuleAll_m1_10081 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.RegistryAccessRule)
extern "C" void RegistrySecurity_RemoveAccessRuleSpecific_m1_10082 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.RegistrySecurity::RemoveAuditRule(System.Security.AccessControl.RegistryAuditRule)
extern "C" bool RegistrySecurity_RemoveAuditRule_m1_10083 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAuditRuleAll(System.Security.AccessControl.RegistryAuditRule)
extern "C" void RegistrySecurity_RemoveAuditRuleAll_m1_10084 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.RegistryAuditRule)
extern "C" void RegistrySecurity_RemoveAuditRuleSpecific_m1_10085 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::ResetAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern "C" void RegistrySecurity_ResetAccessRule_m1_10086 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::SetAccessRule(System.Security.AccessControl.RegistryAccessRule)
extern "C" void RegistrySecurity_SetAccessRule_m1_10087 (RegistrySecurity_t1_1175 * __this, RegistryAccessRule_t1_1172 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.RegistrySecurity::SetAuditRule(System.Security.AccessControl.RegistryAuditRule)
extern "C" void RegistrySecurity_SetAuditRule_m1_10088 (RegistrySecurity_t1_1175 * __this, RegistryAuditRule_t1_1173 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
