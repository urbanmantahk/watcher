﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_FILETIME.h"
#include "mscorlib_System_Guid.h"

// System.Runtime.InteropServices.STATSTG
struct  STATSTG_t1_822 
{
	// System.String System.Runtime.InteropServices.STATSTG::pwcsName
	String_t* ___pwcsName_0;
	// System.Int32 System.Runtime.InteropServices.STATSTG::type
	int32_t ___type_1;
	// System.Int64 System.Runtime.InteropServices.STATSTG::cbSize
	int64_t ___cbSize_2;
	// System.Runtime.InteropServices.FILETIME System.Runtime.InteropServices.STATSTG::mtime
	FILETIME_t1_791  ___mtime_3;
	// System.Runtime.InteropServices.FILETIME System.Runtime.InteropServices.STATSTG::ctime
	FILETIME_t1_791  ___ctime_4;
	// System.Runtime.InteropServices.FILETIME System.Runtime.InteropServices.STATSTG::atime
	FILETIME_t1_791  ___atime_5;
	// System.Int32 System.Runtime.InteropServices.STATSTG::grfMode
	int32_t ___grfMode_6;
	// System.Int32 System.Runtime.InteropServices.STATSTG::grfLocksSupported
	int32_t ___grfLocksSupported_7;
	// System.Guid System.Runtime.InteropServices.STATSTG::clsid
	Guid_t1_319  ___clsid_8;
	// System.Int32 System.Runtime.InteropServices.STATSTG::grfStateBits
	int32_t ___grfStateBits_9;
	// System.Int32 System.Runtime.InteropServices.STATSTG::reserved
	int32_t ___reserved_10;
};
// Native definition for marshalling of: System.Runtime.InteropServices.STATSTG
struct STATSTG_t1_822_marshaled
{
	char* ___pwcsName_0;
	int32_t ___type_1;
	int64_t ___cbSize_2;
	FILETIME_t1_791  ___mtime_3;
	FILETIME_t1_791  ___ctime_4;
	FILETIME_t1_791  ___atime_5;
	int32_t ___grfMode_6;
	int32_t ___grfLocksSupported_7;
	Guid_t1_319  ___clsid_8;
	int32_t ___grfStateBits_9;
	int32_t ___reserved_10;
};
