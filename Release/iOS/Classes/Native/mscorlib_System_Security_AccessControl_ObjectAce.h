﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_QualifiedAce.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"

// System.Security.AccessControl.ObjectAce
struct  ObjectAce_t1_1164  : public QualifiedAce_t1_1123
{
	// System.Guid System.Security.AccessControl.ObjectAce::object_ace_type
	Guid_t1_319  ___object_ace_type_9;
	// System.Guid System.Security.AccessControl.ObjectAce::inherited_object_type
	Guid_t1_319  ___inherited_object_type_10;
	// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAce::object_ace_flags
	int32_t ___object_ace_flags_11;
};
