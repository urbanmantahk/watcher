﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IComparer
struct IComparer_t1_295;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList::.ctor()
extern "C" void ArrayList__ctor_m1_3049 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor(System.Collections.ICollection)
extern "C" void ArrayList__ctor_m1_3050 (ArrayList_t1_170 * __this, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor(System.Int32)
extern "C" void ArrayList__ctor_m1_3051 (ArrayList_t1_170 * __this, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor(System.Int32,System.Boolean)
extern "C" void ArrayList__ctor_m1_3052 (ArrayList_t1_170 * __this, int32_t ___initialCapacity, bool ___forceZeroSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.ctor(System.Object[],System.Int32,System.Int32)
extern "C" void ArrayList__ctor_m1_3053 (ArrayList_t1_170 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::.cctor()
extern "C" void ArrayList__cctor_m1_3054 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList::get_Item(System.Int32)
extern "C" Object_t * ArrayList_get_Item_m1_3055 (ArrayList_t1_170 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::set_Item(System.Int32,System.Object)
extern "C" void ArrayList_set_Item_m1_3056 (ArrayList_t1_170 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::get_Count()
extern "C" int32_t ArrayList_get_Count_m1_3057 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::get_Capacity()
extern "C" int32_t ArrayList_get_Capacity_m1_3058 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::set_Capacity(System.Int32)
extern "C" void ArrayList_set_Capacity_m1_3059 (ArrayList_t1_170 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList::get_IsFixedSize()
extern "C" bool ArrayList_get_IsFixedSize_m1_3060 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList::get_IsReadOnly()
extern "C" bool ArrayList_get_IsReadOnly_m1_3061 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList::get_IsSynchronized()
extern "C" bool ArrayList_get_IsSynchronized_m1_3062 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList::get_SyncRoot()
extern "C" Object_t * ArrayList_get_SyncRoot_m1_3063 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::EnsureCapacity(System.Int32)
extern "C" void ArrayList_EnsureCapacity_m1_3064 (ArrayList_t1_170 * __this, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Shift(System.Int32,System.Int32)
extern "C" void ArrayList_Shift_m1_3065 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::Add(System.Object)
extern "C" int32_t ArrayList_Add_m1_3066 (ArrayList_t1_170 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Clear()
extern "C" void ArrayList_Clear_m1_3067 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList::Contains(System.Object)
extern "C" bool ArrayList_Contains_m1_3068 (ArrayList_t1_170 * __this, Object_t * ___item, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList::Contains(System.Object,System.Int32,System.Int32)
extern "C" bool ArrayList_Contains_m1_3069 (ArrayList_t1_170 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::IndexOf(System.Object)
extern "C" int32_t ArrayList_IndexOf_m1_3070 (ArrayList_t1_170 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::IndexOf(System.Object,System.Int32)
extern "C" int32_t ArrayList_IndexOf_m1_3071 (ArrayList_t1_170 * __this, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::IndexOf(System.Object,System.Int32,System.Int32)
extern "C" int32_t ArrayList_IndexOf_m1_3072 (ArrayList_t1_170 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::LastIndexOf(System.Object)
extern "C" int32_t ArrayList_LastIndexOf_m1_3073 (ArrayList_t1_170 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::LastIndexOf(System.Object,System.Int32)
extern "C" int32_t ArrayList_LastIndexOf_m1_3074 (ArrayList_t1_170 * __this, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::LastIndexOf(System.Object,System.Int32,System.Int32)
extern "C" int32_t ArrayList_LastIndexOf_m1_3075 (ArrayList_t1_170 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Insert(System.Int32,System.Object)
extern "C" void ArrayList_Insert_m1_3076 (ArrayList_t1_170 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::InsertRange(System.Int32,System.Collections.ICollection)
extern "C" void ArrayList_InsertRange_m1_3077 (ArrayList_t1_170 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Remove(System.Object)
extern "C" void ArrayList_Remove_m1_3078 (ArrayList_t1_170 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::RemoveAt(System.Int32)
extern "C" void ArrayList_RemoveAt_m1_3079 (ArrayList_t1_170 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::RemoveRange(System.Int32,System.Int32)
extern "C" void ArrayList_RemoveRange_m1_3080 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Reverse()
extern "C" void ArrayList_Reverse_m1_3081 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Reverse(System.Int32,System.Int32)
extern "C" void ArrayList_Reverse_m1_3082 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::CopyTo(System.Array)
extern "C" void ArrayList_CopyTo_m1_3083 (ArrayList_t1_170 * __this, Array_t * ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::CopyTo(System.Array,System.Int32)
extern "C" void ArrayList_CopyTo_m1_3084 (ArrayList_t1_170 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::CopyTo(System.Int32,System.Array,System.Int32,System.Int32)
extern "C" void ArrayList_CopyTo_m1_3085 (ArrayList_t1_170 * __this, int32_t ___index, Array_t * ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator()
extern "C" Object_t * ArrayList_GetEnumerator_m1_3086 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList::GetEnumerator(System.Int32,System.Int32)
extern "C" Object_t * ArrayList_GetEnumerator_m1_3087 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::AddRange(System.Collections.ICollection)
extern "C" void ArrayList_AddRange_m1_3088 (ArrayList_t1_170 * __this, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::BinarySearch(System.Object)
extern "C" int32_t ArrayList_BinarySearch_m1_3089 (ArrayList_t1_170 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::BinarySearch(System.Object,System.Collections.IComparer)
extern "C" int32_t ArrayList_BinarySearch_m1_3090 (ArrayList_t1_170 * __this, Object_t * ___value, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList::BinarySearch(System.Int32,System.Int32,System.Object,System.Collections.IComparer)
extern "C" int32_t ArrayList_BinarySearch_m1_3091 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, Object_t * ___value, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::GetRange(System.Int32,System.Int32)
extern "C" ArrayList_t1_170 * ArrayList_GetRange_m1_3092 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::SetRange(System.Int32,System.Collections.ICollection)
extern "C" void ArrayList_SetRange_m1_3093 (ArrayList_t1_170 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::TrimToSize()
extern "C" void ArrayList_TrimToSize_m1_3094 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Sort()
extern "C" void ArrayList_Sort_m1_3095 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Sort(System.Collections.IComparer)
extern "C" void ArrayList_Sort_m1_3096 (ArrayList_t1_170 * __this, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::Sort(System.Int32,System.Int32,System.Collections.IComparer)
extern "C" void ArrayList_Sort_m1_3097 (ArrayList_t1_170 * __this, int32_t ___index, int32_t ___count, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Collections.ArrayList::ToArray()
extern "C" ObjectU5BU5D_t1_272* ArrayList_ToArray_m1_3098 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Collections.ArrayList::ToArray(System.Type)
extern "C" Array_t * ArrayList_ToArray_m1_3099 (ArrayList_t1_170 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList::Clone()
extern "C" Object_t * ArrayList_Clone_m1_3100 (ArrayList_t1_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::CheckRange(System.Int32,System.Int32,System.Int32)
extern "C" void ArrayList_CheckRange_m1_3101 (Object_t * __this /* static, unused */, int32_t ___index, int32_t ___count, int32_t ___listCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList::ThrowNewArgumentOutOfRangeException(System.String,System.Object,System.String)
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m1_3102 (Object_t * __this /* static, unused */, String_t* ___name, Object_t * ___actual, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::Adapter(System.Collections.IList)
extern "C" ArrayList_t1_170 * ArrayList_Adapter_m1_3103 (Object_t * __this /* static, unused */, Object_t * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::Synchronized(System.Collections.ArrayList)
extern "C" ArrayList_t1_170 * ArrayList_Synchronized_m1_3104 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Collections.ArrayList::Synchronized(System.Collections.IList)
extern "C" Object_t * ArrayList_Synchronized_m1_3105 (Object_t * __this /* static, unused */, Object_t * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::ReadOnly(System.Collections.ArrayList)
extern "C" ArrayList_t1_170 * ArrayList_ReadOnly_m1_3106 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Collections.ArrayList::ReadOnly(System.Collections.IList)
extern "C" Object_t * ArrayList_ReadOnly_m1_3107 (Object_t * __this /* static, unused */, Object_t * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::FixedSize(System.Collections.ArrayList)
extern "C" ArrayList_t1_170 * ArrayList_FixedSize_m1_3108 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Collections.ArrayList::FixedSize(System.Collections.IList)
extern "C" Object_t * ArrayList_FixedSize_m1_3109 (Object_t * __this /* static, unused */, Object_t * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList::Repeat(System.Object,System.Int32)
extern "C" ArrayList_t1_170 * ArrayList_Repeat_m1_3110 (Object_t * __this /* static, unused */, Object_t * ___value, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
