﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;

#include "System_Xml_System_Xml_XmlNode.h"

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t4_133  : public XmlNode_t4_116
{
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastLinkedChild
	XmlLinkedNode_t4_118 * ___lastLinkedChild_5;
};
