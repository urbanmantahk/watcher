﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities
struct  SoapEntities_t1_971  : public Object_t
{
	// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::_value
	String_t* ____value_0;
};
