﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t4_38;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdHexBinary::.ctor()
extern "C" void XsdHexBinary__ctor_m4_54 (XsdHexBinary_t4_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdHexBinary::get_TokenizedType()
extern "C" int32_t XsdHexBinary_get_TokenizedType_m4_55 (XsdHexBinary_t4_38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
