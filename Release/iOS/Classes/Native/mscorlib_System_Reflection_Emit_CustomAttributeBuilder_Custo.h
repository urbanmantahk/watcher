﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_ValueType.h"

// System.Reflection.Emit.CustomAttributeBuilder/CustomAttributeInfo
struct  CustomAttributeInfo_t1_486 
{
	// System.Reflection.ConstructorInfo System.Reflection.Emit.CustomAttributeBuilder/CustomAttributeInfo::ctor
	ConstructorInfo_t1_478 * ___ctor_0;
	// System.Object[] System.Reflection.Emit.CustomAttributeBuilder/CustomAttributeInfo::ctorArgs
	ObjectU5BU5D_t1_272* ___ctorArgs_1;
	// System.String[] System.Reflection.Emit.CustomAttributeBuilder/CustomAttributeInfo::namedParamNames
	StringU5BU5D_t1_238* ___namedParamNames_2;
	// System.Object[] System.Reflection.Emit.CustomAttributeBuilder/CustomAttributeInfo::namedParamValues
	ObjectU5BU5D_t1_272* ___namedParamValues_3;
};
