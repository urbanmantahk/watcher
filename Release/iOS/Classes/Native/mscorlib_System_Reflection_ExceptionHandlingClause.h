﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Reflection_ExceptionHandlingClauseOptions.h"

// System.Reflection.ExceptionHandlingClause
struct  ExceptionHandlingClause_t1_597  : public Object_t
{
	// System.Type System.Reflection.ExceptionHandlingClause::catch_type
	Type_t * ___catch_type_0;
	// System.Int32 System.Reflection.ExceptionHandlingClause::filter_offset
	int32_t ___filter_offset_1;
	// System.Reflection.ExceptionHandlingClauseOptions System.Reflection.ExceptionHandlingClause::flags
	int32_t ___flags_2;
	// System.Int32 System.Reflection.ExceptionHandlingClause::try_offset
	int32_t ___try_offset_3;
	// System.Int32 System.Reflection.ExceptionHandlingClause::try_length
	int32_t ___try_length_4;
	// System.Int32 System.Reflection.ExceptionHandlingClause::handler_offset
	int32_t ___handler_offset_5;
	// System.Int32 System.Reflection.ExceptionHandlingClause::handler_length
	int32_t ___handler_length_6;
};
