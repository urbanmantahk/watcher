﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.RateMyApp/Settings
struct Settings_t8_310;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.UtilitySettings
struct  UtilitySettings_t8_309  : public Object_t
{
	// VoxelBusters.NativePlugins.RateMyApp/Settings VoxelBusters.NativePlugins.UtilitySettings::m_rateMyApp
	Settings_t8_310 * ___m_rateMyApp_0;
};
