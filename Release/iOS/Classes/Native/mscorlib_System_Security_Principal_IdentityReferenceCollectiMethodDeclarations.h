﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Principal.IdentityReferenceCollection
struct IdentityReferenceCollection_t1_1376;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.Principal.IdentityReference[]
struct IdentityReferenceU5BU5D_t1_1734;
// System.Collections.Generic.IEnumerator`1<System.Security.Principal.IdentityReference>
struct IEnumerator_1_t1_1735;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Principal.IdentityReferenceCollection::.ctor()
extern "C" void IdentityReferenceCollection__ctor_m1_11800 (IdentityReferenceCollection_t1_1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityReferenceCollection::.ctor(System.Int32)
extern "C" void IdentityReferenceCollection__ctor_m1_11801 (IdentityReferenceCollection_t1_1376 * __this, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Principal.IdentityReferenceCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IdentityReferenceCollection_System_Collections_IEnumerable_GetEnumerator_m1_11802 (IdentityReferenceCollection_t1_1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Principal.IdentityReferenceCollection::get_Count()
extern "C" int32_t IdentityReferenceCollection_get_Count_m1_11803 (IdentityReferenceCollection_t1_1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.IdentityReferenceCollection::get_IsReadOnly()
extern "C" bool IdentityReferenceCollection_get_IsReadOnly_m1_11804 (IdentityReferenceCollection_t1_1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReference System.Security.Principal.IdentityReferenceCollection::get_Item(System.Int32)
extern "C" IdentityReference_t1_1120 * IdentityReferenceCollection_get_Item_m1_11805 (IdentityReferenceCollection_t1_1376 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityReferenceCollection::set_Item(System.Int32,System.Security.Principal.IdentityReference)
extern "C" void IdentityReferenceCollection_set_Item_m1_11806 (IdentityReferenceCollection_t1_1376 * __this, int32_t ___index, IdentityReference_t1_1120 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityReferenceCollection::Add(System.Security.Principal.IdentityReference)
extern "C" void IdentityReferenceCollection_Add_m1_11807 (IdentityReferenceCollection_t1_1376 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityReferenceCollection::Clear()
extern "C" void IdentityReferenceCollection_Clear_m1_11808 (IdentityReferenceCollection_t1_1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.IdentityReferenceCollection::Contains(System.Security.Principal.IdentityReference)
extern "C" bool IdentityReferenceCollection_Contains_m1_11809 (IdentityReferenceCollection_t1_1376 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Principal.IdentityReferenceCollection::CopyTo(System.Security.Principal.IdentityReference[],System.Int32)
extern "C" void IdentityReferenceCollection_CopyTo_m1_11810 (IdentityReferenceCollection_t1_1376 * __this, IdentityReferenceU5BU5D_t1_1734* ___array, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Security.Principal.IdentityReference> System.Security.Principal.IdentityReferenceCollection::GetEnumerator()
extern "C" Object_t* IdentityReferenceCollection_GetEnumerator_m1_11811 (IdentityReferenceCollection_t1_1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Principal.IdentityReferenceCollection::Remove(System.Security.Principal.IdentityReference)
extern "C" bool IdentityReferenceCollection_Remove_m1_11812 (IdentityReferenceCollection_t1_1376 * __this, IdentityReference_t1_1120 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReferenceCollection System.Security.Principal.IdentityReferenceCollection::Translate(System.Type)
extern "C" IdentityReferenceCollection_t1_1376 * IdentityReferenceCollection_Translate_m1_11813 (IdentityReferenceCollection_t1_1376 * __this, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReferenceCollection System.Security.Principal.IdentityReferenceCollection::Translate(System.Type,System.Boolean)
extern "C" IdentityReferenceCollection_t1_1376 * IdentityReferenceCollection_Translate_m1_11814 (IdentityReferenceCollection_t1_1376 * __this, Type_t * ___targetType, bool ___forceSuccess, const MethodInfo* method) IL2CPP_METHOD_ATTR;
