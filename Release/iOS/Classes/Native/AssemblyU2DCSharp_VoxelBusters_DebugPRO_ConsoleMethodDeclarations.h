﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DebugPRO.Console
struct Console_t8_169;
// System.String
struct String_t;
// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;
// System.Object
struct Object_t;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LogType.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void VoxelBusters.DebugPRO.Console::.ctor()
extern "C" void Console__ctor_m8_976 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::.cctor()
extern "C" void Console__cctor_m8_977 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console::get_IsDebugMode()
extern "C" bool Console_get_IsDebugMode_m8_978 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console::get_ShowInfoLogs()
extern "C" bool Console_get_ShowInfoLogs_m8_979 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::set_ShowInfoLogs(System.Boolean)
extern "C" void Console_set_ShowInfoLogs_m8_980 (Console_t8_169 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console::get_ShowWarningLogs()
extern "C" bool Console_get_ShowWarningLogs_m8_981 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::set_ShowWarningLogs(System.Boolean)
extern "C" void Console_set_ShowWarningLogs_m8_982 (Console_t8_169 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console::get_ShowErrorLogs()
extern "C" bool Console_get_ShowErrorLogs_m8_983 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::set_ShowErrorLogs(System.Boolean)
extern "C" void Console_set_ShowErrorLogs_m8_984 (Console_t8_169 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.DebugPRO.Console::get_InfoLogsCounter()
extern "C" int32_t Console_get_InfoLogsCounter_m8_985 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::set_InfoLogsCounter(System.Int32)
extern "C" void Console_set_InfoLogsCounter_m8_986 (Console_t8_169 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.DebugPRO.Console::get_WarningLogsCounter()
extern "C" int32_t Console_get_WarningLogsCounter_m8_987 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::set_WarningLogsCounter(System.Int32)
extern "C" void Console_set_WarningLogsCounter_m8_988 (Console_t8_169 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.DebugPRO.Console::get_ErrorLogsCounter()
extern "C" int32_t Console_get_ErrorLogsCounter_m8_989 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::set_ErrorLogsCounter(System.Int32)
extern "C" void Console_set_ErrorLogsCounter_m8_990 (Console_t8_169 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.DebugPRO.Console VoxelBusters.DebugPRO.Console::get_Instance()
extern "C" Console_t8_169 * Console_get_Instance_m8_991 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::OnEnable()
extern "C" void Console_OnEnable_m8_992 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::OnDisable()
extern "C" void Console_OnDisable_m8_993 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::HandleUnityLog(System.String,System.String,UnityEngine.LogType)
extern "C" void Console_HandleUnityLog_m8_994 (Object_t * __this /* static, unused */, String_t* ____message, String_t* ____stackTrace, int32_t ____logType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::Clear()
extern "C" void Console_Clear_m8_995 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::RebuildDisplayableLogs()
extern "C" void Console_RebuildDisplayableLogs_m8_996 (Console_t8_169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console::AddToDisplayableLogList(VoxelBusters.DebugPRO.Internal.ConsoleLog)
extern "C" bool Console_AddToDisplayableLogList_m8_997 (Console_t8_169 * __this, ConsoleLog_t8_170  ____consoleLog, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.DebugPRO.Internal.ConsoleTag VoxelBusters.DebugPRO.Console::GetConsoleTag(System.String)
extern "C" ConsoleTag_t8_171 * Console_GetConsoleTag_m8_998 (Console_t8_169 * __this, String_t* ____tagName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.DebugPRO.Console::GetIndexOfConsoleTag(System.String)
extern "C" int32_t Console_GetIndexOfConsoleTag_m8_999 (Console_t8_169 * __this, String_t* ____tagName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console::IgnoreConsoleLog(System.String)
extern "C" bool Console_IgnoreConsoleLog_m8_1000 (Console_t8_169 * __this, String_t* ____tagName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::Log(System.String,System.Object,VoxelBusters.DebugPRO.Internal.eConsoleLogType,UnityEngine.Object,System.Int32)
extern "C" void Console_Log_m8_1001 (Console_t8_169 * __this, String_t* ____tagName, Object_t * ____message, int32_t ____logType, Object_t6_5 * ____context, int32_t ____skipStackFrameCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::ClearConsole()
extern "C" void Console_ClearConsole_m8_1002 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::AddIgnoreTag(System.String)
extern "C" void Console_AddIgnoreTag_m8_1003 (Object_t * __this /* static, unused */, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::RemoveIgnoreTag(System.String)
extern "C" void Console_RemoveIgnoreTag_m8_1004 (Object_t * __this /* static, unused */, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::DrawLine(System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C" void Console_DrawLine_m8_1005 (Object_t * __this /* static, unused */, String_t* ____tag, Vector3_t6_48  ____start, Vector3_t6_48  ____end, Color_t6_40  ____color, float ____duration, bool ____depthTest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::DrawRay(System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern "C" void Console_DrawRay_m8_1006 (Object_t * __this /* static, unused */, String_t* ____tag, Vector3_t6_48  ____start, Vector3_t6_48  ____direction, Color_t6_40  ____color, float ____duration, bool ____depthTest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::Log(System.String,System.Object,UnityEngine.Object)
extern "C" void Console_Log_m8_1007 (Object_t * __this /* static, unused */, String_t* ____tag, Object_t * ____message, Object_t6_5 * ____context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::LogWarning(System.String,System.Object,UnityEngine.Object)
extern "C" void Console_LogWarning_m8_1008 (Object_t * __this /* static, unused */, String_t* ____tag, Object_t * ____message, Object_t6_5 * ____context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::LogError(System.String,System.Object,UnityEngine.Object)
extern "C" void Console_LogError_m8_1009 (Object_t * __this /* static, unused */, String_t* ____tag, Object_t * ____message, Object_t6_5 * ____context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Console::LogException(System.String,System.Exception,UnityEngine.Object)
extern "C" void Console_LogException_m8_1010 (Object_t * __this /* static, unused */, String_t* ____tag, Exception_t1_33 * ____exception, Object_t6_5 * ____context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
