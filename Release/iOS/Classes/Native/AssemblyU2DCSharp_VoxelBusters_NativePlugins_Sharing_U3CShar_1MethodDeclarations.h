﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing/<ShareImageAtPath>c__AnonStorey11
struct U3CShareImageAtPathU3Ec__AnonStorey11_t8_278;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Sharing/<ShareImageAtPath>c__AnonStorey11::.ctor()
extern "C" void U3CShareImageAtPathU3Ec__AnonStorey11__ctor_m8_1584 (U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareImageAtPath>c__AnonStorey11::<>m__12(UnityEngine.Texture2D,System.String)
extern "C" void U3CShareImageAtPathU3Ec__AnonStorey11_U3CU3Em__12_m8_1585 (U3CShareImageAtPathU3Ec__AnonStorey11_t8_278 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
