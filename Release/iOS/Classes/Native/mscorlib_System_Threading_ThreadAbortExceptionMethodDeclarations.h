﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ThreadAbortException
struct ThreadAbortException_t1_1478;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadAbortException::.ctor()
extern "C" void ThreadAbortException__ctor_m1_12913 (ThreadAbortException_t1_1478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadAbortException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ThreadAbortException__ctor_m1_12914 (ThreadAbortException_t1_1478 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
