﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings
struct  iOSSettings_t8_268  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.NotificationServiceSettings/iOSSettings::m_userInfoKey
	String_t* ___m_userInfoKey_0;
};
