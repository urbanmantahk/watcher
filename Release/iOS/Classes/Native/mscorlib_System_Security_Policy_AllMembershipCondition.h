﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// System.Security.Policy.AllMembershipCondition
struct  AllMembershipCondition_t1_1325  : public Object_t
{
	// System.Int32 System.Security.Policy.AllMembershipCondition::version
	int32_t ___version_0;
};
