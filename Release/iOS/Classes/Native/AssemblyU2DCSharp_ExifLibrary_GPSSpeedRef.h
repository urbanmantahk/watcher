﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSSpeedRef.h"

// ExifLibrary.GPSSpeedRef
struct  GPSSpeedRef_t8_91 
{
	// System.Byte ExifLibrary.GPSSpeedRef::value__
	uint8_t ___value___1;
};
