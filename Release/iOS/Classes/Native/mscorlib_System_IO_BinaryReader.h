﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1_405;
// System.Text.Encoding
struct Encoding_t1_406;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Text.Decoder
struct Decoder_t1_407;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "mscorlib_System_Object.h"

// System.IO.BinaryReader
struct  BinaryReader_t1_404  : public Object_t
{
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_t1_405 * ___m_stream_1;
	// System.Text.Encoding System.IO.BinaryReader::m_encoding
	Encoding_t1_406 * ___m_encoding_2;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_t1_109* ___m_buffer_3;
	// System.Text.Decoder System.IO.BinaryReader::decoder
	Decoder_t1_407 * ___decoder_4;
	// System.Char[] System.IO.BinaryReader::charBuffer
	CharU5BU5D_t1_16* ___charBuffer_5;
	// System.Boolean System.IO.BinaryReader::m_disposed
	bool ___m_disposed_6;
};
