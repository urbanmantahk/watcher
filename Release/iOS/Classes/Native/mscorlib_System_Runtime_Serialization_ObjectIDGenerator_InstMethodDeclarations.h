﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer
struct InstanceComparer_t1_1077;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::.ctor()
extern "C" void InstanceComparer__ctor_m1_9535 (InstanceComparer_t1_1077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t InstanceComparer_System_Collections_IComparer_Compare_m1_9536 (InstanceComparer_t1_1077 * __this, Object_t * ___o1, Object_t * ___o2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.ObjectIDGenerator/InstanceComparer::System.Collections.IHashCodeProvider.GetHashCode(System.Object)
extern "C" int32_t InstanceComparer_System_Collections_IHashCodeProvider_GetHashCode_m1_9537 (InstanceComparer_t1_1077 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
