﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_HostSecurityManagerOptions.h"

// System.Security.HostSecurityManagerOptions
struct  HostSecurityManagerOptions_t1_1390 
{
	// System.Int32 System.Security.HostSecurityManagerOptions::value__
	int32_t ___value___1;
};
