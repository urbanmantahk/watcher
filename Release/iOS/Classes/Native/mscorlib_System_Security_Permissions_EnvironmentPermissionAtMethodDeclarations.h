﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.EnvironmentPermissionAttribute
struct EnvironmentPermissionAttribute_t1_1270;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.EnvironmentPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void EnvironmentPermissionAttribute__ctor_m1_10795 (EnvironmentPermissionAttribute_t1_1270 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.EnvironmentPermissionAttribute::get_All()
extern "C" String_t* EnvironmentPermissionAttribute_get_All_m1_10796 (EnvironmentPermissionAttribute_t1_1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermissionAttribute::set_All(System.String)
extern "C" void EnvironmentPermissionAttribute_set_All_m1_10797 (EnvironmentPermissionAttribute_t1_1270 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.EnvironmentPermissionAttribute::get_Read()
extern "C" String_t* EnvironmentPermissionAttribute_get_Read_m1_10798 (EnvironmentPermissionAttribute_t1_1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermissionAttribute::set_Read(System.String)
extern "C" void EnvironmentPermissionAttribute_set_Read_m1_10799 (EnvironmentPermissionAttribute_t1_1270 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.EnvironmentPermissionAttribute::get_Write()
extern "C" String_t* EnvironmentPermissionAttribute_get_Write_m1_10800 (EnvironmentPermissionAttribute_t1_1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermissionAttribute::set_Write(System.String)
extern "C" void EnvironmentPermissionAttribute_set_Write_m1_10801 (EnvironmentPermissionAttribute_t1_1270 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.EnvironmentPermissionAttribute::CreatePermission()
extern "C" Object_t * EnvironmentPermissionAttribute_CreatePermission_m1_10802 (EnvironmentPermissionAttribute_t1_1270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
