﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.AuthorizationRule
struct AuthorizationRule_t1_1112;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"

// System.Void System.Security.AccessControl.AuthorizationRule::.ctor()
extern "C" void AuthorizationRule__ctor_m1_9715 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.AuthorizationRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void AuthorizationRule__ctor_m1_9716 (AuthorizationRule_t1_1112 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IdentityReference System.Security.AccessControl.AuthorizationRule::get_IdentityReference()
extern "C" IdentityReference_t1_1120 * AuthorizationRule_get_IdentityReference_m1_9717 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.InheritanceFlags System.Security.AccessControl.AuthorizationRule::get_InheritanceFlags()
extern "C" int32_t AuthorizationRule_get_InheritanceFlags_m1_9718 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.AuthorizationRule::get_IsInherited()
extern "C" bool AuthorizationRule_get_IsInherited_m1_9719 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.PropagationFlags System.Security.AccessControl.AuthorizationRule::get_PropagationFlags()
extern "C" int32_t AuthorizationRule_get_PropagationFlags_m1_9720 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.AuthorizationRule::get_AccessMask()
extern "C" int32_t AuthorizationRule_get_AccessMask_m1_9721 (AuthorizationRule_t1_1112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
