﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion>
struct Dictionary_2_t1_1911;
// VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion
struct SingleFieldPromptCompletion_t8_301;
// VoxelBusters.NativePlugins.UI/LoginPromptCompletion
struct LoginPromptCompletion_t8_302;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.UI
struct  UI_t8_303  : public MonoBehaviour_t6_91
{
	// System.Collections.Generic.Dictionary`2<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion> VoxelBusters.NativePlugins.UI::m_alertDialogCallbackCollection
	Dictionary_2_t1_1911 * ___m_alertDialogCallbackCollection_3;
	// VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion VoxelBusters.NativePlugins.UI::OnSingleFieldPromptClosed
	SingleFieldPromptCompletion_t8_301 * ___OnSingleFieldPromptClosed_4;
	// VoxelBusters.NativePlugins.UI/LoginPromptCompletion VoxelBusters.NativePlugins.UI::OnLoginPromptClosed
	LoginPromptCompletion_t8_302 * ___OnLoginPromptClosed_5;
};
