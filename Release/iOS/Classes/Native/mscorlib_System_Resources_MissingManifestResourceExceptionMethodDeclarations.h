﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.MissingManifestResourceException
struct MissingManifestResourceException_t1_642;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Resources.MissingManifestResourceException::.ctor()
extern "C" void MissingManifestResourceException__ctor_m1_7346 (MissingManifestResourceException_t1_642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingManifestResourceException::.ctor(System.String)
extern "C" void MissingManifestResourceException__ctor_m1_7347 (MissingManifestResourceException_t1_642 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingManifestResourceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MissingManifestResourceException__ctor_m1_7348 (MissingManifestResourceException_t1_642 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.MissingManifestResourceException::.ctor(System.String,System.Exception)
extern "C" void MissingManifestResourceException__ctor_m1_7349 (MissingManifestResourceException_t1_642 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
