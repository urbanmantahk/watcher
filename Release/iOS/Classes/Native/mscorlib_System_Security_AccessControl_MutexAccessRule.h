﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Security_AccessControl_MutexRights.h"

// System.Security.AccessControl.MutexAccessRule
struct  MutexAccessRule_t1_1158  : public AccessRule_t1_1111
{
	// System.Security.AccessControl.MutexRights System.Security.AccessControl.MutexAccessRule::rights
	int32_t ___rights_6;
};
