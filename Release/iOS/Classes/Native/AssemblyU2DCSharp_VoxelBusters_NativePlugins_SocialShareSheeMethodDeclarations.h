﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.SocialShareSheet
struct SocialShareSheet_t8_290;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.SocialShareSheet::.ctor()
extern "C" void SocialShareSheet__ctor_m8_1704 (SocialShareSheet_t8_290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.SocialShareSheet::get_ExcludedShareOptions()
extern "C" eShareOptionsU5BU5D_t8_186* SocialShareSheet_get_ExcludedShareOptions_m8_1705 (SocialShareSheet_t8_290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareSheet::set_ExcludedShareOptions(VoxelBusters.NativePlugins.eShareOptions[])
extern "C" void SocialShareSheet_set_ExcludedShareOptions_m8_1706 (SocialShareSheet_t8_290 * __this, eShareOptionsU5BU5D_t8_186* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
