﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void TYPELIBATTR_t1_746_marshal(const TYPELIBATTR_t1_746& unmarshaled, TYPELIBATTR_t1_746_marshaled& marshaled);
extern "C" void TYPELIBATTR_t1_746_marshal_back(const TYPELIBATTR_t1_746_marshaled& marshaled, TYPELIBATTR_t1_746& unmarshaled);
extern "C" void TYPELIBATTR_t1_746_marshal_cleanup(TYPELIBATTR_t1_746_marshaled& marshaled);
