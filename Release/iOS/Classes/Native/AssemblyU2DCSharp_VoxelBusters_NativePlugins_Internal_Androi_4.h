﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_WebViewMessage.h"

// VoxelBusters.NativePlugins.Internal.AndroidWebViewMessage
struct  AndroidWebViewMessage_t8_313  : public WebViewMessage_t8_314
{
};
