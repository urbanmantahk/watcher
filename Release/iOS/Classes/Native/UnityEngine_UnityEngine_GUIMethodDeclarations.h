﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUISkin
struct GUISkin_t6_169;
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t6_176;
// UnityEngine.GUIContent
struct GUIContent_t6_171;
// UnityEngine.TextEditor
struct TextEditor_t6_254;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t6_167;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_FocusType.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.GUI::.cctor()
extern "C" void GUI__cctor_m6_1196 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.GUI::get_nextScrollStepTime()
extern "C" DateTime_t1_150  GUI_get_nextScrollStepTime_m6_1197 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern "C" void GUI_set_nextScrollStepTime_m6_1198 (Object_t * __this /* static, unused */, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GUI::get_scrollTroughSide()
extern "C" int32_t GUI_get_scrollTroughSide_m6_1199 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_scrollTroughSide(System.Int32)
extern "C" void GUI_set_scrollTroughSide_m6_1200 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C" void GUI_set_skin_m6_1201 (Object_t * __this /* static, unused */, GUISkin_t6_169 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C" GUISkin_t6_169 * GUI_get_skin_m6_1202 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoSetSkin(UnityEngine.GUISkin)
extern "C" void GUI_DoSetSkin_m6_1203 (Object_t * __this /* static, unused */, GUISkin_t6_169 * ___newSkin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m6_1204 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, String_t* ___text, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_Label_m6_1205 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_Box_m6_1206 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C" bool GUI_Button_m6_1207 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, String_t* ___text, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" bool GUI_Button_m6_1208 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoRepeatButton(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.FocusType)
extern "C" bool GUI_DoRepeatButton_m6_1209 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, int32_t ___focusType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUI::PasswordFieldGetStrToShow(System.String,System.Char)
extern "C" String_t* GUI_PasswordFieldGetStrToShow_m6_1210 (Object_t * __this /* static, unused */, String_t* ___password, uint16_t ___maskChar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle)
extern "C" void GUI_DoTextField_m6_1211 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String)
extern "C" void GUI_DoTextField_m6_1212 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, String_t* ___secureText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char)
extern "C" void GUI_DoTextField_m6_1213 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, String_t* ___secureText, uint16_t ___maskChar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::HandleTextFieldEventForTouchscreen(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char,UnityEngine.TextEditor)
extern "C" void GUI_HandleTextFieldEventForTouchscreen_m6_1214 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, String_t* ___secureText, uint16_t ___maskChar, TextEditor_t6_254 * ___editor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::HandleTextFieldEventForDesktop(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,UnityEngine.TextEditor)
extern "C" void GUI_HandleTextFieldEventForDesktop_m6_1215 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, TextEditor_t6_254 * ___editor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Toggle(UnityEngine.Rect,System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" bool GUI_Toggle_m6_1216 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, bool ___value, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::Slider(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C" float GUI_Slider_m6_1217 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___start, float ___end, GUIStyle_t6_176 * ___slider, GUIStyle_t6_176 * ___thumb, bool ___horiz, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::HorizontalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" float GUI_HorizontalScrollbar_m6_1218 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::ScrollerRepeatButton(System.Int32,UnityEngine.Rect,UnityEngine.GUIStyle)
extern "C" bool GUI_ScrollerRepeatButton_m6_1219 (Object_t * __this /* static, unused */, int32_t ___scrollerID, Rect_t6_51  ___rect, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::VerticalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" float GUI_VerticalScrollbar_m6_1220 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___topValue, float ___bottomValue, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUI::Scroller(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean)
extern "C" float GUI_Scroller_m6_1221 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_176 * ___slider, GUIStyle_t6_176 * ___thumb, GUIStyle_t6_176 * ___leftButton, GUIStyle_t6_176 * ___rightButton, bool ___horiz, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern "C" void GUI_BeginGroup_m6_1222 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::EndGroup()
extern "C" void GUI_EndGroup_m6_1223 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUI::BeginScrollView(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern "C" Vector2_t6_47  GUI_BeginScrollView_m6_1224 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, Vector2_t6_47  ___scrollPosition, Rect_t6_51  ___viewRect, bool ___alwaysShowHorizontal, bool ___alwaysShowVertical, GUIStyle_t6_176 * ___horizontalScrollbar, GUIStyle_t6_176 * ___verticalScrollbar, GUIStyle_t6_176 * ___background, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::EndScrollView(System.Boolean)
extern "C" void GUI_EndScrollView_m6_1225 (Object_t * __this /* static, unused */, bool ___handleScrollWheel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
extern "C" Rect_t6_51  GUI_Window_m6_1226 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_51  ___clientRect, WindowFunction_t6_167 * ___func, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern "C" void GUI_CallWindowDelegate_m6_1227 (Object_t * __this /* static, unused */, WindowFunction_t6_167 * ___func, int32_t ___id, GUISkin_t6_169 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m6_1228 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_DoLabel_m6_1229 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m6_1230 (Object_t * __this /* static, unused */, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_DoButton_m6_1231 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m6_1232 (Object_t * __this /* static, unused */, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoToggle(UnityEngine.Rect,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_DoToggle_m6_1233 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, bool ___value, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoToggle_m6_1234 (Object_t * __this /* static, unused */, Rect_t6_51 * ___position, int32_t ___id, bool ___value, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::get_usePageScrollbars()
extern "C" bool GUI_get_usePageScrollbars_m6_1235 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
extern "C" void GUI_InternalRepaintEditorWindow_m6_1236 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t6_51  GUI_DoWindow_m6_1237 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_51  ___clientRect, WindowFunction_t6_167 * ___func, GUIContent_t6_171 * ___title, GUIStyle_t6_176 * ___style, GUISkin_t6_169 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t6_51  GUI_INTERNAL_CALL_DoWindow_m6_1238 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_51 * ___clientRect, WindowFunction_t6_167 * ___func, GUIContent_t6_171 * ___title, GUIStyle_t6_176 * ___style, GUISkin_t6_169 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
