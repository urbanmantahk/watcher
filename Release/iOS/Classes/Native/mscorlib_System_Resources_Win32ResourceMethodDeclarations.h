﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32Resource
struct Win32Resource_t1_664;
// System.Resources.NameOrId
struct NameOrId_t1_663;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Resources_Win32ResourceType.h"

// System.Void System.Resources.Win32Resource::.ctor(System.Resources.NameOrId,System.Resources.NameOrId,System.Int32)
extern "C" void Win32Resource__ctor_m1_7470 (Win32Resource_t1_664 * __this, NameOrId_t1_663 * ___type, NameOrId_t1_663 * ___name, int32_t ___language, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32Resource::.ctor(System.Resources.Win32ResourceType,System.Int32,System.Int32)
extern "C" void Win32Resource__ctor_m1_7471 (Win32Resource_t1_664 * __this, int32_t ___type, int32_t ___name, int32_t ___language, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.Win32ResourceType System.Resources.Win32Resource::get_ResourceType()
extern "C" int32_t Win32Resource_get_ResourceType_m1_7472 (Win32Resource_t1_664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.NameOrId System.Resources.Win32Resource::get_Name()
extern "C" NameOrId_t1_663 * Win32Resource_get_Name_m1_7473 (Win32Resource_t1_664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.NameOrId System.Resources.Win32Resource::get_Type()
extern "C" NameOrId_t1_663 * Win32Resource_get_Type_m1_7474 (Win32Resource_t1_664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.Win32Resource::get_Language()
extern "C" int32_t Win32Resource_get_Language_m1_7475 (Win32Resource_t1_664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32Resource::ToString()
extern "C" String_t* Win32Resource_ToString_m1_7476 (Win32Resource_t1_664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
