﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime
struct SoapDateTime_t1_968;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime::.ctor()
extern "C" void SoapDateTime__ctor_m1_8694 (SoapDateTime_t1_968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime::.cctor()
extern "C" void SoapDateTime__cctor_m1_8695 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime::get_XsdType()
extern "C" String_t* SoapDateTime_get_XsdType_m1_8696 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime::Parse(System.String)
extern "C" DateTime_t1_150  SoapDateTime_Parse_m1_8697 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime::ToString(System.DateTime)
extern "C" String_t* SoapDateTime_ToString_m1_8698 (Object_t * __this /* static, unused */, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
