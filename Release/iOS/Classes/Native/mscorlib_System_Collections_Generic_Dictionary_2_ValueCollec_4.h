﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct Dictionary_2_t1_1904;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>
struct  ValueCollection_t1_1927  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t1_1904 * ___dictionary_0;
};
