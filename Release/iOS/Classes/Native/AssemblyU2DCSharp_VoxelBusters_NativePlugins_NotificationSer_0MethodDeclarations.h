﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse
struct ReceivedNotificationResponse_t8_258;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::.ctor(System.Object,System.IntPtr)
extern "C" void ReceivedNotificationResponse__ctor_m8_1427 (ReceivedNotificationResponse_t8_258 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::Invoke(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void ReceivedNotificationResponse_Invoke_m8_1428 (ReceivedNotificationResponse_t8_258 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReceivedNotificationResponse_t8_258(Il2CppObject* delegate, CrossPlatformNotification_t8_259 * ____notification);
// System.IAsyncResult VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::BeginInvoke(VoxelBusters.NativePlugins.CrossPlatformNotification,System.AsyncCallback,System.Object)
extern "C" Object_t * ReceivedNotificationResponse_BeginInvoke_m8_1429 (ReceivedNotificationResponse_t8_258 * __this, CrossPlatformNotification_t8_259 * ____notification, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse::EndInvoke(System.IAsyncResult)
extern "C" void ReceivedNotificationResponse_EndInvoke_m8_1430 (ReceivedNotificationResponse_t8_258 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
