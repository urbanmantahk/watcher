﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AuditRule.h"
#include "mscorlib_System_Security_AccessControl_MutexRights.h"

// System.Security.AccessControl.MutexAuditRule
struct  MutexAuditRule_t1_1159  : public AuditRule_t1_1119
{
	// System.Security.AccessControl.MutexRights System.Security.AccessControl.MutexAuditRule::rights
	int32_t ___rights_6;
};
