﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.CaseInsensitiveHashCodeProvider
struct CaseInsensitiveHashCodeProvider_t1_278;
// System.Object
struct Object_t;
// System.Globalization.TextInfo
struct TextInfo_t1_124;

#include "mscorlib_System_Object.h"

// System.Collections.CaseInsensitiveHashCodeProvider
struct  CaseInsensitiveHashCodeProvider_t1_278  : public Object_t
{
	// System.Globalization.TextInfo System.Collections.CaseInsensitiveHashCodeProvider::m_text
	TextInfo_t1_124 * ___m_text_3;
};
struct CaseInsensitiveHashCodeProvider_t1_278_StaticFields{
	// System.Collections.CaseInsensitiveHashCodeProvider System.Collections.CaseInsensitiveHashCodeProvider::singletonInvariant
	CaseInsensitiveHashCodeProvider_t1_278 * ___singletonInvariant_0;
	// System.Collections.CaseInsensitiveHashCodeProvider System.Collections.CaseInsensitiveHashCodeProvider::singleton
	CaseInsensitiveHashCodeProvider_t1_278 * ___singleton_1;
	// System.Object System.Collections.CaseInsensitiveHashCodeProvider::sync
	Object_t * ___sync_2;
};
