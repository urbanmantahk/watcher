﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.SoapAttribute
struct SoapAttribute_t1_997;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.SoapAttribute::.ctor()
extern "C" void SoapAttribute__ctor_m1_8935 (SoapAttribute_t1_997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapAttribute::get_Embedded()
extern "C" bool SoapAttribute_get_Embedded_m1_8936 (SoapAttribute_t1_997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapAttribute::set_Embedded(System.Boolean)
extern "C" void SoapAttribute_set_Embedded_m1_8937 (SoapAttribute_t1_997 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapAttribute::get_UseAttribute()
extern "C" bool SoapAttribute_get_UseAttribute_m1_8938 (SoapAttribute_t1_997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapAttribute::set_UseAttribute(System.Boolean)
extern "C" void SoapAttribute_set_UseAttribute_m1_8939 (SoapAttribute_t1_997 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapAttribute::get_XmlNamespace()
extern "C" String_t* SoapAttribute_get_XmlNamespace_m1_8940 (SoapAttribute_t1_997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapAttribute::set_XmlNamespace(System.String)
extern "C" void SoapAttribute_set_XmlNamespace_m1_8941 (SoapAttribute_t1_997 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapAttribute::SetReflectionObject(System.Object)
extern "C" void SoapAttribute_SetReflectionObject_m1_8942 (SoapAttribute_t1_997 * __this, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
