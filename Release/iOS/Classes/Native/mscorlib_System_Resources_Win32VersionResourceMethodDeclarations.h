﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32VersionResource
struct Win32VersionResource_t1_474;
// System.String
struct String_t;
// System.IO.BinaryWriter
struct BinaryWriter_t1_408;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.Win32VersionResource::.ctor(System.Int32,System.Int32,System.Boolean)
extern "C" void Win32VersionResource__ctor_m1_7485 (Win32VersionResource_t1_474 * __this, int32_t ___id, int32_t ___language, bool ___compilercontext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_Version()
extern "C" String_t* Win32VersionResource_get_Version_m1_7486 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_Version(System.String)
extern "C" void Win32VersionResource_set_Version_m1_7487 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_Item(System.String,System.String)
extern "C" void Win32VersionResource_set_Item_m1_7488 (Win32VersionResource_t1_474 * __this, String_t* ___key, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_Comments()
extern "C" String_t* Win32VersionResource_get_Comments_m1_7489 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_Comments(System.String)
extern "C" void Win32VersionResource_set_Comments_m1_7490 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_CompanyName()
extern "C" String_t* Win32VersionResource_get_CompanyName_m1_7491 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_CompanyName(System.String)
extern "C" void Win32VersionResource_set_CompanyName_m1_7492 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_LegalCopyright()
extern "C" String_t* Win32VersionResource_get_LegalCopyright_m1_7493 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_LegalCopyright(System.String)
extern "C" void Win32VersionResource_set_LegalCopyright_m1_7494 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_LegalTrademarks()
extern "C" String_t* Win32VersionResource_get_LegalTrademarks_m1_7495 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_LegalTrademarks(System.String)
extern "C" void Win32VersionResource_set_LegalTrademarks_m1_7496 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_OriginalFilename()
extern "C" String_t* Win32VersionResource_get_OriginalFilename_m1_7497 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_OriginalFilename(System.String)
extern "C" void Win32VersionResource_set_OriginalFilename_m1_7498 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_ProductName()
extern "C" String_t* Win32VersionResource_get_ProductName_m1_7499 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_ProductName(System.String)
extern "C" void Win32VersionResource_set_ProductName_m1_7500 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_ProductVersion()
extern "C" String_t* Win32VersionResource_get_ProductVersion_m1_7501 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_ProductVersion(System.String)
extern "C" void Win32VersionResource_set_ProductVersion_m1_7502 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_InternalName()
extern "C" String_t* Win32VersionResource_get_InternalName_m1_7503 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_InternalName(System.String)
extern "C" void Win32VersionResource_set_InternalName_m1_7504 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_FileDescription()
extern "C" String_t* Win32VersionResource_get_FileDescription_m1_7505 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_FileDescription(System.String)
extern "C" void Win32VersionResource_set_FileDescription_m1_7506 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.Win32VersionResource::get_FileLanguage()
extern "C" int32_t Win32VersionResource_get_FileLanguage_m1_7507 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_FileLanguage(System.Int32)
extern "C" void Win32VersionResource_set_FileLanguage_m1_7508 (Win32VersionResource_t1_474 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.Win32VersionResource::get_FileVersion()
extern "C" String_t* Win32VersionResource_get_FileVersion_m1_7509 (Win32VersionResource_t1_474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::set_FileVersion(System.String)
extern "C" void Win32VersionResource_set_FileVersion_m1_7510 (Win32VersionResource_t1_474 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::emit_padding(System.IO.BinaryWriter)
extern "C" void Win32VersionResource_emit_padding_m1_7511 (Win32VersionResource_t1_474 * __this, BinaryWriter_t1_408 * ___w, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::patch_length(System.IO.BinaryWriter,System.Int64)
extern "C" void Win32VersionResource_patch_length_m1_7512 (Win32VersionResource_t1_474 * __this, BinaryWriter_t1_408 * ___w, int64_t ___len_pos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32VersionResource::WriteTo(System.IO.Stream)
extern "C" void Win32VersionResource_WriteTo_m1_7513 (Win32VersionResource_t1_474 * __this, Stream_t1_405 * ___ms, const MethodInfo* method) IL2CPP_METHOD_ATTR;
