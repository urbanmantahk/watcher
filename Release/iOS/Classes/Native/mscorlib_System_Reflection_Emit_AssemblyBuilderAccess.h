﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderAccess.h"

// System.Reflection.Emit.AssemblyBuilderAccess
struct  AssemblyBuilderAccess_t1_476 
{
	// System.Int32 System.Reflection.Emit.AssemblyBuilderAccess::value__
	int32_t ___value___1;
};
