﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_SystemException.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"

// System.Security.HostProtectionException
struct  HostProtectionException_t1_1388  : public SystemException_t1_250
{
	// System.Security.Permissions.HostProtectionResource System.Security.HostProtectionException::_protected
	int32_t ____protected_12;
	// System.Security.Permissions.HostProtectionResource System.Security.HostProtectionException::_demanded
	int32_t ____demanded_13;
};
