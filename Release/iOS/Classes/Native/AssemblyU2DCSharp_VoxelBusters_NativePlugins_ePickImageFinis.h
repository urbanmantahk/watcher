﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickImageFinis.h"

// VoxelBusters.NativePlugins.ePickImageFinishReason
struct  ePickImageFinishReason_t8_249 
{
	// System.Int32 VoxelBusters.NativePlugins.ePickImageFinishReason::value__
	int32_t ___value___1;
};
