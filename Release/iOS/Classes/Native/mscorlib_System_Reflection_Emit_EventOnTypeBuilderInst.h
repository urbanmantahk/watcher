﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.EventBuilder
struct EventBuilder_t1_500;

#include "mscorlib_System_Reflection_EventInfo.h"

// System.Reflection.Emit.EventOnTypeBuilderInst
struct  EventOnTypeBuilderInst_t1_503  : public EventInfo_t
{
	// System.Reflection.MonoGenericClass System.Reflection.Emit.EventOnTypeBuilderInst::instantiation
	MonoGenericClass_t1_485 * ___instantiation_1;
	// System.Reflection.Emit.EventBuilder System.Reflection.Emit.EventOnTypeBuilderInst::evt
	EventBuilder_t1_500 * ___evt_2;
};
