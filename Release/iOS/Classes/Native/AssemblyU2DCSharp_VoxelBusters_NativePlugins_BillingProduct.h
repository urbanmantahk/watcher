﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.BillingProduct
struct  BillingProduct_t8_207  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.BillingProduct::m_name
	String_t* ___m_name_0;
	// System.String VoxelBusters.NativePlugins.BillingProduct::m_description
	String_t* ___m_description_1;
	// System.Boolean VoxelBusters.NativePlugins.BillingProduct::m_isConsumable
	bool ___m_isConsumable_2;
	// System.Single VoxelBusters.NativePlugins.BillingProduct::m_price
	float ___m_price_3;
	// System.String VoxelBusters.NativePlugins.BillingProduct::m_iosProductId
	String_t* ___m_iosProductId_4;
	// System.String VoxelBusters.NativePlugins.BillingProduct::m_androidProductId
	String_t* ___m_androidProductId_5;
	// System.String VoxelBusters.NativePlugins.BillingProduct::<LocalizedPrice>k__BackingField
	String_t* ___U3CLocalizedPriceU3Ek__BackingField_6;
	// System.String VoxelBusters.NativePlugins.BillingProduct::<CurrencyCode>k__BackingField
	String_t* ___U3CCurrencyCodeU3Ek__BackingField_7;
	// System.String VoxelBusters.NativePlugins.BillingProduct::<CurrencySymbol>k__BackingField
	String_t* ___U3CCurrencySymbolU3Ek__BackingField_8;
};
