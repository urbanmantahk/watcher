﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.TypeDelegator
struct TypeDelegator_t1_641;
// System.Type
struct Type_t;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.String
struct String_t;
// System.Reflection.Module
struct Module_t1_495;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_1671;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1_1667;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Object
struct Object_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Reflection_InterfaceMapping.h"
#include "mscorlib_System_Reflection_MemberTypes.h"

// System.Void System.Reflection.TypeDelegator::.ctor()
extern "C" void TypeDelegator__ctor_m1_7302 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.TypeDelegator::.ctor(System.Type)
extern "C" void TypeDelegator__ctor_m1_7303 (TypeDelegator_t1_641 * __this, Type_t * ___delegatingType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.TypeDelegator::get_Assembly()
extern "C" Assembly_t1_467 * TypeDelegator_get_Assembly_m1_7304 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.TypeDelegator::get_AssemblyQualifiedName()
extern "C" String_t* TypeDelegator_get_AssemblyQualifiedName_m1_7305 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.TypeDelegator::get_BaseType()
extern "C" Type_t * TypeDelegator_get_BaseType_m1_7306 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.TypeDelegator::get_FullName()
extern "C" String_t* TypeDelegator_get_FullName_m1_7307 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.TypeDelegator::get_GUID()
extern "C" Guid_t1_319  TypeDelegator_get_GUID_m1_7308 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.TypeDelegator::get_Module()
extern "C" Module_t1_495 * TypeDelegator_get_Module_m1_7309 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.TypeDelegator::get_Name()
extern "C" String_t* TypeDelegator_get_Name_m1_7310 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.TypeDelegator::get_Namespace()
extern "C" String_t* TypeDelegator_get_Namespace_m1_7311 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.Reflection.TypeDelegator::get_TypeHandle()
extern "C" RuntimeTypeHandle_t1_30  TypeDelegator_get_TypeHandle_m1_7312 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.TypeDelegator::get_UnderlyingSystemType()
extern "C" Type_t * TypeDelegator_get_UnderlyingSystemType_m1_7313 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.TypeDelegator::GetAttributeFlagsImpl()
extern "C" int32_t TypeDelegator_GetAttributeFlagsImpl_m1_7314 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.TypeDelegator::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" ConstructorInfo_t1_478 * TypeDelegator_GetConstructorImpl_m1_7315 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.TypeDelegator::GetConstructors(System.Reflection.BindingFlags)
extern "C" ConstructorInfoU5BU5D_t1_1671* TypeDelegator_GetConstructors_m1_7316 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.TypeDelegator::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* TypeDelegator_GetCustomAttributes_m1_7317 (TypeDelegator_t1_641 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.TypeDelegator::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* TypeDelegator_GetCustomAttributes_m1_7318 (TypeDelegator_t1_641 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.TypeDelegator::GetElementType()
extern "C" Type_t * TypeDelegator_GetElementType_m1_7319 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.TypeDelegator::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C" EventInfo_t * TypeDelegator_GetEvent_m1_7320 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.TypeDelegator::GetEvents()
extern "C" EventInfoU5BU5D_t1_1667* TypeDelegator_GetEvents_m1_7321 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.TypeDelegator::GetEvents(System.Reflection.BindingFlags)
extern "C" EventInfoU5BU5D_t1_1667* TypeDelegator_GetEvents_m1_7322 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.TypeDelegator::GetField(System.String,System.Reflection.BindingFlags)
extern "C" FieldInfo_t * TypeDelegator_GetField_m1_7323 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.TypeDelegator::GetFields(System.Reflection.BindingFlags)
extern "C" FieldInfoU5BU5D_t1_1668* TypeDelegator_GetFields_m1_7324 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.TypeDelegator::GetInterface(System.String,System.Boolean)
extern "C" Type_t * TypeDelegator_GetInterface_m1_7325 (TypeDelegator_t1_641 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.InterfaceMapping System.Reflection.TypeDelegator::GetInterfaceMap(System.Type)
extern "C" InterfaceMapping_t1_602  TypeDelegator_GetInterfaceMap_m1_7326 (TypeDelegator_t1_641 * __this, Type_t * ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.TypeDelegator::GetInterfaces()
extern "C" TypeU5BU5D_t1_31* TypeDelegator_GetInterfaces_m1_7327 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.TypeDelegator::GetMember(System.String,System.Reflection.MemberTypes,System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* TypeDelegator_GetMember_m1_7328 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___type, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.TypeDelegator::GetMembers(System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* TypeDelegator_GetMembers_m1_7329 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.TypeDelegator::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * TypeDelegator_GetMethodImpl_m1_7330 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.TypeDelegator::GetMethods(System.Reflection.BindingFlags)
extern "C" MethodInfoU5BU5D_t1_603* TypeDelegator_GetMethods_m1_7331 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.TypeDelegator::GetNestedType(System.String,System.Reflection.BindingFlags)
extern "C" Type_t * TypeDelegator_GetNestedType_m1_7332 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.TypeDelegator::GetNestedTypes(System.Reflection.BindingFlags)
extern "C" TypeU5BU5D_t1_31* TypeDelegator_GetNestedTypes_m1_7333 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Reflection.TypeDelegator::GetProperties(System.Reflection.BindingFlags)
extern "C" PropertyInfoU5BU5D_t1_1670* TypeDelegator_GetProperties_m1_7334 (TypeDelegator_t1_641 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.TypeDelegator::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C" PropertyInfo_t * TypeDelegator_GetPropertyImpl_m1_7335 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, Type_t * ___returnType, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::HasElementTypeImpl()
extern "C" bool TypeDelegator_HasElementTypeImpl_m1_7336 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.TypeDelegator::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C" Object_t * TypeDelegator_InvokeMember_m1_7337 (TypeDelegator_t1_641 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1_585 * ___binder, Object_t * ___target, ObjectU5BU5D_t1_272* ___args, ParameterModifierU5BU5D_t1_1669* ___modifiers, CultureInfo_t1_277 * ___culture, StringU5BU5D_t1_238* ___namedParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsArrayImpl()
extern "C" bool TypeDelegator_IsArrayImpl_m1_7338 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsByRefImpl()
extern "C" bool TypeDelegator_IsByRefImpl_m1_7339 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsCOMObjectImpl()
extern "C" bool TypeDelegator_IsCOMObjectImpl_m1_7340 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsDefined(System.Type,System.Boolean)
extern "C" bool TypeDelegator_IsDefined_m1_7341 (TypeDelegator_t1_641 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsPointerImpl()
extern "C" bool TypeDelegator_IsPointerImpl_m1_7342 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsPrimitiveImpl()
extern "C" bool TypeDelegator_IsPrimitiveImpl_m1_7343 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.TypeDelegator::IsValueTypeImpl()
extern "C" bool TypeDelegator_IsValueTypeImpl_m1_7344 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.TypeDelegator::get_MetadataToken()
extern "C" int32_t TypeDelegator_get_MetadataToken_m1_7345 (TypeDelegator_t1_641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
