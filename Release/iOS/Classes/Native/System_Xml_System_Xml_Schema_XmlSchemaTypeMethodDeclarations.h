﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4_64;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t4_71;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t4_63;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Schema.XmlSchemaType::.ctor()
extern "C" void XmlSchemaType__ctor_m4_106 (XmlSchemaType_t4_64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::get_QualifiedName()
extern "C" XmlQualifiedName_t4_71 * XmlSchemaType_get_QualifiedName_m4_107 (XmlSchemaType_t4_64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaType::GetBuiltInSimpleType(System.Xml.XmlQualifiedName)
extern "C" XmlSchemaSimpleType_t4_63 * XmlSchemaType_GetBuiltInSimpleType_m4_108 (Object_t * __this /* static, unused */, XmlQualifiedName_t4_71 * ___qualifiedName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
