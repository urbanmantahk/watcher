﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Stack
struct Stack_t1_243;

#include "mscorlib_System_Object.h"

// System.Collections.Stack/Enumerator
struct  Enumerator_t1_310  : public Object_t
{
	// System.Collections.Stack System.Collections.Stack/Enumerator::stack
	Stack_t1_243 * ___stack_2;
	// System.Int32 System.Collections.Stack/Enumerator::modCount
	int32_t ___modCount_3;
	// System.Int32 System.Collections.Stack/Enumerator::current
	int32_t ___current_4;
};
