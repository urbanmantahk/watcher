﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.ActivationContext
struct ActivationContext_t1_717;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.ApplicationId
struct ApplicationId_t1_1329;
// System.Security.PermissionSet
struct PermissionSet_t1_563;

#include "mscorlib_System_Object.h"

// System.Security.Policy.ApplicationSecurityInfo
struct  ApplicationSecurityInfo_t1_1328  : public Object_t
{
	// System.ActivationContext System.Security.Policy.ApplicationSecurityInfo::_context
	ActivationContext_t1_717 * ____context_0;
	// System.Security.Policy.Evidence System.Security.Policy.ApplicationSecurityInfo::_evidence
	Evidence_t1_398 * ____evidence_1;
	// System.ApplicationId System.Security.Policy.ApplicationSecurityInfo::_appid
	ApplicationId_t1_1329 * ____appid_2;
	// System.Security.PermissionSet System.Security.Policy.ApplicationSecurityInfo::_defaultSet
	PermissionSet_t1_563 * ____defaultSet_3;
	// System.ApplicationId System.Security.Policy.ApplicationSecurityInfo::_deployid
	ApplicationId_t1_1329 * ____deployid_4;
};
