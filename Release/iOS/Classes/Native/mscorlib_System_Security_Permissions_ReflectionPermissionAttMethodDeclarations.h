﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.ReflectionPermissionAttribute
struct ReflectionPermissionAttribute_t1_1302;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_ReflectionPermissionFla.h"

// System.Void System.Security.Permissions.ReflectionPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void ReflectionPermissionAttribute__ctor_m1_11096 (ReflectionPermissionAttribute_t1_1302 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.ReflectionPermissionFlag System.Security.Permissions.ReflectionPermissionAttribute::get_Flags()
extern "C" int32_t ReflectionPermissionAttribute_get_Flags_m1_11097 (ReflectionPermissionAttribute_t1_1302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermissionAttribute::set_Flags(System.Security.Permissions.ReflectionPermissionFlag)
extern "C" void ReflectionPermissionAttribute_set_Flags_m1_11098 (ReflectionPermissionAttribute_t1_1302 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::get_MemberAccess()
extern "C" bool ReflectionPermissionAttribute_get_MemberAccess_m1_11099 (ReflectionPermissionAttribute_t1_1302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermissionAttribute::set_MemberAccess(System.Boolean)
extern "C" void ReflectionPermissionAttribute_set_MemberAccess_m1_11100 (ReflectionPermissionAttribute_t1_1302 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::get_ReflectionEmit()
extern "C" bool ReflectionPermissionAttribute_get_ReflectionEmit_m1_11101 (ReflectionPermissionAttribute_t1_1302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermissionAttribute::set_ReflectionEmit(System.Boolean)
extern "C" void ReflectionPermissionAttribute_set_ReflectionEmit_m1_11102 (ReflectionPermissionAttribute_t1_1302 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::get_RestrictedMemberAccess()
extern "C" bool ReflectionPermissionAttribute_get_RestrictedMemberAccess_m1_11103 (ReflectionPermissionAttribute_t1_1302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermissionAttribute::set_RestrictedMemberAccess(System.Boolean)
extern "C" void ReflectionPermissionAttribute_set_RestrictedMemberAccess_m1_11104 (ReflectionPermissionAttribute_t1_1302 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ReflectionPermissionAttribute::get_TypeInformation()
extern "C" bool ReflectionPermissionAttribute_get_TypeInformation_m1_11105 (ReflectionPermissionAttribute_t1_1302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ReflectionPermissionAttribute::set_TypeInformation(System.Boolean)
extern "C" void ReflectionPermissionAttribute_set_TypeInformation_m1_11106 (ReflectionPermissionAttribute_t1_1302 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ReflectionPermissionAttribute::CreatePermission()
extern "C" Object_t * ReflectionPermissionAttribute_CreatePermission_m1_11107 (ReflectionPermissionAttribute_t1_1302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
