﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.BitConverterLE
struct BitConverterLE_t1_220;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.BitConverterLE::.ctor()
extern "C" void BitConverterLE__ctor_m1_2446 (BitConverterLE_t1_220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetUShortBytes(System.Byte*)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetUShortBytes_m1_2447 (Object_t * __this /* static, unused */, uint8_t* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetUIntBytes(System.Byte*)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetUIntBytes_m1_2448 (Object_t * __this /* static, unused */, uint8_t* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetULongBytes(System.Byte*)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetULongBytes_m1_2449 (Object_t * __this /* static, unused */, uint8_t* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Boolean)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2450 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Char)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2451 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Int16)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2452 (Object_t * __this /* static, unused */, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2453 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Int64)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2454 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.UInt16)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2455 (Object_t * __this /* static, unused */, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.UInt32)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2456 (Object_t * __this /* static, unused */, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.UInt64)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2457 (Object_t * __this /* static, unused */, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Single)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2458 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.BitConverterLE::GetBytes(System.Double)
extern "C" ByteU5BU5D_t1_109* BitConverterLE_GetBytes_m1_2459 (Object_t * __this /* static, unused */, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.BitConverterLE::UShortFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" void BitConverterLE_UShortFromBytes_m1_2460 (Object_t * __this /* static, unused */, uint8_t* ___dst, ByteU5BU5D_t1_109* ___src, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.BitConverterLE::UIntFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" void BitConverterLE_UIntFromBytes_m1_2461 (Object_t * __this /* static, unused */, uint8_t* ___dst, ByteU5BU5D_t1_109* ___src, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.BitConverterLE::ULongFromBytes(System.Byte*,System.Byte[],System.Int32)
extern "C" void BitConverterLE_ULongFromBytes_m1_2462 (Object_t * __this /* static, unused */, uint8_t* ___dst, ByteU5BU5D_t1_109* ___src, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.BitConverterLE::ToBoolean(System.Byte[],System.Int32)
extern "C" bool BitConverterLE_ToBoolean_m1_2463 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Mono.Security.BitConverterLE::ToChar(System.Byte[],System.Int32)
extern "C" uint16_t BitConverterLE_ToChar_m1_2464 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Mono.Security.BitConverterLE::ToInt16(System.Byte[],System.Int32)
extern "C" int16_t BitConverterLE_ToInt16_m1_2465 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.BitConverterLE::ToInt32(System.Byte[],System.Int32)
extern "C" int32_t BitConverterLE_ToInt32_m1_2466 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Mono.Security.BitConverterLE::ToInt64(System.Byte[],System.Int32)
extern "C" int64_t BitConverterLE_ToInt64_m1_2467 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Mono.Security.BitConverterLE::ToUInt16(System.Byte[],System.Int32)
extern "C" uint16_t BitConverterLE_ToUInt16_m1_2468 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Security.BitConverterLE::ToUInt32(System.Byte[],System.Int32)
extern "C" uint32_t BitConverterLE_ToUInt32_m1_2469 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Mono.Security.BitConverterLE::ToUInt64(System.Byte[],System.Int32)
extern "C" uint64_t BitConverterLE_ToUInt64_m1_2470 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mono.Security.BitConverterLE::ToSingle(System.Byte[],System.Int32)
extern "C" float BitConverterLE_ToSingle_m1_2471 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Mono.Security.BitConverterLE::ToDouble(System.Byte[],System.Int32)
extern "C" double BitConverterLE_ToDouble_m1_2472 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
