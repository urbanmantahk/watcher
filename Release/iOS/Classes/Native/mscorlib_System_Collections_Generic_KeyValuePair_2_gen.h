﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mono.Xml.DTDNode
struct DTDNode_t4_89;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>
struct  KeyValuePair_2_t1_1805 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	DTDNode_t4_89 * ___value_1;
};
