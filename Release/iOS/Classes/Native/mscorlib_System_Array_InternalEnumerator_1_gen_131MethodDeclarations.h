﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_131.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18344_gshared (InternalEnumerator_1_t1_2224 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18344(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2224 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18344_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18345_gshared (InternalEnumerator_1_t1_2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18345(__this, method) (( void (*) (InternalEnumerator_1_t1_2224 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18345_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18346_gshared (InternalEnumerator_1_t1_2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18346(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2224 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18346_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18347_gshared (InternalEnumerator_1_t1_2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18347(__this, method) (( void (*) (InternalEnumerator_1_t1_2224 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18347_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18348_gshared (InternalEnumerator_1_t1_2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18348(__this, method) (( bool (*) (InternalEnumerator_1_t1_2224 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18348_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C" KeyValuePair_2_t1_2223  InternalEnumerator_1_get_Current_m1_18349_gshared (InternalEnumerator_1_t1_2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18349(__this, method) (( KeyValuePair_2_t1_2223  (*) (InternalEnumerator_1_t1_2224 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18349_gshared)(__this, method)
