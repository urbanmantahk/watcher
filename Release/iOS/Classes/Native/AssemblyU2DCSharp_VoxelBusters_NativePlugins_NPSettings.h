﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;
// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct
struct AssetStoreProduct_t8_22;
// VoxelBusters.NativePlugins.ApplicationSettings
struct ApplicationSettings_t8_320;
// VoxelBusters.NativePlugins.UtilitySettings
struct UtilitySettings_t8_309;
// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj_0.h"

// VoxelBusters.NativePlugins.NPSettings
struct  NPSettings_t8_335  : public AdvancedScriptableObject_1_t8_336
{
	// System.Char[] VoxelBusters.NativePlugins.NPSettings::defineSeperators
	CharU5BU5D_t1_16* ___defineSeperators_23;
	// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct VoxelBusters.NativePlugins.NPSettings::m_assetStoreProduct
	AssetStoreProduct_t8_22 * ___m_assetStoreProduct_24;
	// VoxelBusters.NativePlugins.ApplicationSettings VoxelBusters.NativePlugins.NPSettings::m_applicationSettings
	ApplicationSettings_t8_320 * ___m_applicationSettings_25;
	// VoxelBusters.NativePlugins.UtilitySettings VoxelBusters.NativePlugins.NPSettings::m_utilitySettings
	UtilitySettings_t8_309 * ___m_utilitySettings_26;
	// VoxelBusters.NativePlugins.NotificationServiceSettings VoxelBusters.NativePlugins.NPSettings::m_notificationSettings
	NotificationServiceSettings_t8_270 * ___m_notificationSettings_27;
};
