﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion
struct LoadAchievementsCompletion_t8_218;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.Achievement[]
struct AchievementU5BU5D_t8_219;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadAchievementsCompletion__ctor_m8_1242 (LoadAchievementsCompletion_t8_218 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::Invoke(VoxelBusters.NativePlugins.Achievement[],System.String)
extern "C" void LoadAchievementsCompletion_Invoke_m8_1243 (LoadAchievementsCompletion_t8_218 * __this, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoadAchievementsCompletion_t8_218(Il2CppObject* delegate, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::BeginInvoke(VoxelBusters.NativePlugins.Achievement[],System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadAchievementsCompletion_BeginInvoke_m8_1244 (LoadAchievementsCompletion_t8_218 * __this, AchievementU5BU5D_t8_219* ____achievements, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Achievement/LoadAchievementsCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadAchievementsCompletion_EndInvoke_m8_1245 (LoadAchievementsCompletion_t8_218 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
