﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Globalization.Unicode.NormalizationTableUtil
struct NormalizationTableUtil_t1_118;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Globalization.Unicode.NormalizationTableUtil::.ctor()
extern "C" void NormalizationTableUtil__ctor_m1_1632 (NormalizationTableUtil_t1_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Globalization.Unicode.NormalizationTableUtil::.cctor()
extern "C" void NormalizationTableUtil__cctor_m1_1633 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::PropIdx(System.Int32)
extern "C" int32_t NormalizationTableUtil_PropIdx_m1_1634 (Object_t * __this /* static, unused */, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::PropCP(System.Int32)
extern "C" int32_t NormalizationTableUtil_PropCP_m1_1635 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::get_PropCount()
extern "C" int32_t NormalizationTableUtil_get_PropCount_m1_1636 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::MapIdx(System.Int32)
extern "C" int32_t NormalizationTableUtil_MapIdx_m1_1637 (Object_t * __this /* static, unused */, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::MapCP(System.Int32)
extern "C" int32_t NormalizationTableUtil_MapCP_m1_1638 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::CbIdx(System.Int32)
extern "C" int32_t NormalizationTableUtil_CbIdx_m1_1639 (Object_t * __this /* static, unused */, int32_t ___cp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::CbCP(System.Int32)
extern "C" int32_t NormalizationTableUtil_CbCP_m1_1640 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Globalization.Unicode.NormalizationTableUtil::get_MapCount()
extern "C" int32_t NormalizationTableUtil_get_MapCount_m1_1641 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
