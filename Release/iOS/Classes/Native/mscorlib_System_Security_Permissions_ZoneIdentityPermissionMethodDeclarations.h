﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.ZoneIdentityPermission
struct ZoneIdentityPermission_t1_1323;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Void System.Security.Permissions.ZoneIdentityPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void ZoneIdentityPermission__ctor_m1_11298 (ZoneIdentityPermission_t1_1323 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ZoneIdentityPermission::.ctor(System.Security.SecurityZone)
extern "C" void ZoneIdentityPermission__ctor_m1_11299 (ZoneIdentityPermission_t1_1323 * __this, int32_t ___zone, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.ZoneIdentityPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t ZoneIdentityPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11300 (ZoneIdentityPermission_t1_1323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ZoneIdentityPermission::Copy()
extern "C" Object_t * ZoneIdentityPermission_Copy_m1_11301 (ZoneIdentityPermission_t1_1323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.ZoneIdentityPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool ZoneIdentityPermission_IsSubsetOf_m1_11302 (ZoneIdentityPermission_t1_1323 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ZoneIdentityPermission::Union(System.Security.IPermission)
extern "C" Object_t * ZoneIdentityPermission_Union_m1_11303 (ZoneIdentityPermission_t1_1323 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.ZoneIdentityPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * ZoneIdentityPermission_Intersect_m1_11304 (ZoneIdentityPermission_t1_1323 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ZoneIdentityPermission::FromXml(System.Security.SecurityElement)
extern "C" void ZoneIdentityPermission_FromXml_m1_11305 (ZoneIdentityPermission_t1_1323 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.ZoneIdentityPermission::ToXml()
extern "C" SecurityElement_t1_242 * ZoneIdentityPermission_ToXml_m1_11306 (ZoneIdentityPermission_t1_1323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityZone System.Security.Permissions.ZoneIdentityPermission::get_SecurityZone()
extern "C" int32_t ZoneIdentityPermission_get_SecurityZone_m1_11307 (ZoneIdentityPermission_t1_1323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.ZoneIdentityPermission::set_SecurityZone(System.Security.SecurityZone)
extern "C" void ZoneIdentityPermission_set_SecurityZone_m1_11308 (ZoneIdentityPermission_t1_1323 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.ZoneIdentityPermission System.Security.Permissions.ZoneIdentityPermission::Cast(System.Security.IPermission)
extern "C" ZoneIdentityPermission_t1_1323 * ZoneIdentityPermission_Cast_m1_11309 (ZoneIdentityPermission_t1_1323 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
