﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.FieldOffsetAttribute
struct FieldOffsetAttribute_t1_65;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.FieldOffsetAttribute::.ctor(System.Int32)
extern "C" void FieldOffsetAttribute__ctor_m1_1344 (FieldOffsetAttribute_t1_65 * __this, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.FieldOffsetAttribute::get_Value()
extern "C" int32_t FieldOffsetAttribute_get_Value_m1_1345 (FieldOffsetAttribute_t1_65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
