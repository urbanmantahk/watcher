﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CallConvStdcall
struct CallConvStdcall_t1_675;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CallConvStdcall::.ctor()
extern "C" void CallConvStdcall__ctor_m1_7528 (CallConvStdcall_t1_675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
