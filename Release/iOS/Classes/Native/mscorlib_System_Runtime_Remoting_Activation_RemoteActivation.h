﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IList
struct IList_t1_262;

#include "mscorlib_System_Attribute.h"

// System.Runtime.Remoting.Activation.RemoteActivationAttribute
struct  RemoteActivationAttribute_t1_856  : public Attribute_t1_2
{
	// System.Collections.IList System.Runtime.Remoting.Activation.RemoteActivationAttribute::_contextProperties
	Object_t * ____contextProperties_0;
};
