﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.IShareView
struct IShareView_t8_274;
// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.Sharing
struct Sharing_t8_273;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6
struct  U3CShowViewCoroutineU3Ec__Iterator6_t8_272  : public Object_t
{
	// VoxelBusters.NativePlugins.IShareView VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::_shareView
	Object_t * ____shareView_0;
	// VoxelBusters.NativePlugins.Sharing/SharingCompletion VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::_onCompletion
	SharingCompletion_t8_271 * ____onCompletion_1;
	// System.Int32 VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::$current
	Object_t * ___U24current_3;
	// VoxelBusters.NativePlugins.IShareView VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::<$>_shareView
	Object_t * ___U3CU24U3E_shareView_4;
	// VoxelBusters.NativePlugins.Sharing/SharingCompletion VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::<$>_onCompletion
	SharingCompletion_t8_271 * ___U3CU24U3E_onCompletion_5;
	// VoxelBusters.NativePlugins.Sharing VoxelBusters.NativePlugins.Sharing/<ShowViewCoroutine>c__Iterator6::<>f__this
	Sharing_t8_273 * ___U3CU3Ef__this_6;
};
