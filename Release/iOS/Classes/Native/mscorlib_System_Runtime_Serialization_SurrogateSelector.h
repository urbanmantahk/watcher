﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1_959;

#include "mscorlib_System_Object.h"

// System.Runtime.Serialization.SurrogateSelector
struct  SurrogateSelector_t1_1099  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Serialization.SurrogateSelector::Surrogates
	Hashtable_t1_100 * ___Surrogates_0;
	// System.Runtime.Serialization.ISurrogateSelector System.Runtime.Serialization.SurrogateSelector::nextSelector
	Object_t * ___nextSelector_1;
};
