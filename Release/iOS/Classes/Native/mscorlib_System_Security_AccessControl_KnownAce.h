﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;

#include "mscorlib_System_Security_AccessControl_GenericAce.h"

// System.Security.AccessControl.KnownAce
struct  KnownAce_t1_1136  : public GenericAce_t1_1145
{
	// System.Int32 System.Security.AccessControl.KnownAce::access_mask
	int32_t ___access_mask_4;
	// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.KnownAce::identifier
	SecurityIdentifier_t1_1132 * ___identifier_5;
};
