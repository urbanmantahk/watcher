﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.KeyContainerPermission
struct KeyContainerPermission_t1_1288;
// System.Security.Permissions.KeyContainerPermissionAccessEntry[]
struct KeyContainerPermissionAccessEntryU5BU5D_t1_1730;
// System.Security.Permissions.KeyContainerPermissionAccessEntryCollection
struct KeyContainerPermissionAccessEntryCollection_t1_1289;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPermissionF.h"

// System.Void System.Security.Permissions.KeyContainerPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void KeyContainerPermission__ctor_m1_10952 (KeyContainerPermission_t1_1288 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermission::.ctor(System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermission__ctor_m1_10953 (KeyContainerPermission_t1_1288 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermission::.ctor(System.Security.Permissions.KeyContainerPermissionFlags,System.Security.Permissions.KeyContainerPermissionAccessEntry[])
extern "C" void KeyContainerPermission__ctor_m1_10954 (KeyContainerPermission_t1_1288 * __this, int32_t ___flags, KeyContainerPermissionAccessEntryU5BU5D_t1_1730* ___accessList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.KeyContainerPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t KeyContainerPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_10955 (KeyContainerPermission_t1_1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionAccessEntryCollection System.Security.Permissions.KeyContainerPermission::get_AccessEntries()
extern "C" KeyContainerPermissionAccessEntryCollection_t1_1289 * KeyContainerPermission_get_AccessEntries_m1_10956 (KeyContainerPermission_t1_1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermissionFlags System.Security.Permissions.KeyContainerPermission::get_Flags()
extern "C" int32_t KeyContainerPermission_get_Flags_m1_10957 (KeyContainerPermission_t1_1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.KeyContainerPermission::Copy()
extern "C" Object_t * KeyContainerPermission_Copy_m1_10958 (KeyContainerPermission_t1_1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermission::FromXml(System.Security.SecurityElement)
extern "C" void KeyContainerPermission_FromXml_m1_10959 (KeyContainerPermission_t1_1288 * __this, SecurityElement_t1_242 * ___securityElement, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.KeyContainerPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * KeyContainerPermission_Intersect_m1_10960 (KeyContainerPermission_t1_1288 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.KeyContainerPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool KeyContainerPermission_IsSubsetOf_m1_10961 (KeyContainerPermission_t1_1288 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.KeyContainerPermission::IsUnrestricted()
extern "C" bool KeyContainerPermission_IsUnrestricted_m1_10962 (KeyContainerPermission_t1_1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.KeyContainerPermission::ToXml()
extern "C" SecurityElement_t1_242 * KeyContainerPermission_ToXml_m1_10963 (KeyContainerPermission_t1_1288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.KeyContainerPermission::Union(System.Security.IPermission)
extern "C" Object_t * KeyContainerPermission_Union_m1_10964 (KeyContainerPermission_t1_1288 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.KeyContainerPermission::SetFlags(System.Security.Permissions.KeyContainerPermissionFlags)
extern "C" void KeyContainerPermission_SetFlags_m1_10965 (KeyContainerPermission_t1_1288 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.KeyContainerPermission System.Security.Permissions.KeyContainerPermission::Cast(System.Security.IPermission)
extern "C" KeyContainerPermission_t1_1288 * KeyContainerPermission_Cast_m1_10966 (KeyContainerPermission_t1_1288 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
