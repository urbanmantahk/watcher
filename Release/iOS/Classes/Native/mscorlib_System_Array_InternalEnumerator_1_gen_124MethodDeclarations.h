﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_124.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope.h"

// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18050_gshared (InternalEnumerator_1_t1_2206 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18050(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2206 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18050_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18051_gshared (InternalEnumerator_1_t1_2206 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18051(__this, method) (( void (*) (InternalEnumerator_1_t1_2206 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18051_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18052_gshared (InternalEnumerator_1_t1_2206 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18052(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2206 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18052_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18053_gshared (InternalEnumerator_1_t1_2206 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18053(__this, method) (( void (*) (InternalEnumerator_1_t1_2206 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18053_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18054_gshared (InternalEnumerator_1_t1_2206 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18054(__this, method) (( bool (*) (InternalEnumerator_1_t1_2206 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18054_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::get_Current()
extern "C" NsScope_t4_142  InternalEnumerator_1_get_Current_m1_18055_gshared (InternalEnumerator_1_t1_2206 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18055(__this, method) (( NsScope_t4_142  (*) (InternalEnumerator_1_t1_2206 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18055_gshared)(__this, method)
