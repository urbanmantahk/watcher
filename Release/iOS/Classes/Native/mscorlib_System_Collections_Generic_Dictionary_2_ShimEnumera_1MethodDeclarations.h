﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t1_2233;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1_2221;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_18426_gshared (ShimEnumerator_t1_2233 * __this, Dictionary_2_t1_2221 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_18426(__this, ___host, method) (( void (*) (ShimEnumerator_t1_2233 *, Dictionary_2_t1_2221 *, const MethodInfo*))ShimEnumerator__ctor_m1_18426_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_18427_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_18427(__this, method) (( void (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_Dispose_m1_18427_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_18428_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_18428(__this, method) (( bool (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_18428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_18429_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_18429(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_18429_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_18430_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_18430(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_get_Key_m1_18430_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_18431_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_18431(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_get_Value_m1_18431_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_18432_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_18432(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_get_Current_m1_18432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C" void ShimEnumerator_Reset_m1_18433_gshared (ShimEnumerator_t1_2233 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_18433(__this, method) (( void (*) (ShimEnumerator_t1_2233 *, const MethodInfo*))ShimEnumerator_Reset_m1_18433_gshared)(__this, method)
