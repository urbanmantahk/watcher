﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.RegistryPermission
struct RegistryPermission_t1_1304;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_RegistryPermissionAcces.h"
#include "mscorlib_System_Security_AccessControl_AccessControlActions.h"

// System.Void System.Security.Permissions.RegistryPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void RegistryPermission__ctor_m1_11108 (RegistryPermission_t1_1304 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::.ctor(System.Security.Permissions.RegistryPermissionAccess,System.String)
extern "C" void RegistryPermission__ctor_m1_11109 (RegistryPermission_t1_1304 * __this, int32_t ___access, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::.ctor(System.Security.Permissions.RegistryPermissionAccess,System.Security.AccessControl.AccessControlActions,System.String)
extern "C" void RegistryPermission__ctor_m1_11110 (RegistryPermission_t1_1304 * __this, int32_t ___access, int32_t ___control, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.RegistryPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t RegistryPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11111 (RegistryPermission_t1_1304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::AddPathList(System.Security.Permissions.RegistryPermissionAccess,System.String)
extern "C" void RegistryPermission_AddPathList_m1_11112 (RegistryPermission_t1_1304 * __this, int32_t ___access, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::AddPathList(System.Security.Permissions.RegistryPermissionAccess,System.Security.AccessControl.AccessControlActions,System.String)
extern "C" void RegistryPermission_AddPathList_m1_11113 (RegistryPermission_t1_1304 * __this, int32_t ___access, int32_t ___control, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermission::GetPathList(System.Security.Permissions.RegistryPermissionAccess)
extern "C" String_t* RegistryPermission_GetPathList_m1_11114 (RegistryPermission_t1_1304 * __this, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::SetPathList(System.Security.Permissions.RegistryPermissionAccess,System.String)
extern "C" void RegistryPermission_SetPathList_m1_11115 (RegistryPermission_t1_1304 * __this, int32_t ___access, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.RegistryPermission::Copy()
extern "C" Object_t * RegistryPermission_Copy_m1_11116 (RegistryPermission_t1_1304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::FromXml(System.Security.SecurityElement)
extern "C" void RegistryPermission_FromXml_m1_11117 (RegistryPermission_t1_1304 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.RegistryPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * RegistryPermission_Intersect_m1_11118 (RegistryPermission_t1_1304 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.RegistryPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool RegistryPermission_IsSubsetOf_m1_11119 (RegistryPermission_t1_1304 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.RegistryPermission::IsUnrestricted()
extern "C" bool RegistryPermission_IsUnrestricted_m1_11120 (RegistryPermission_t1_1304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.RegistryPermission::ToXml()
extern "C" SecurityElement_t1_242 * RegistryPermission_ToXml_m1_11121 (RegistryPermission_t1_1304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.RegistryPermission::Union(System.Security.IPermission)
extern "C" Object_t * RegistryPermission_Union_m1_11122 (RegistryPermission_t1_1304 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.RegistryPermission::IsEmpty()
extern "C" bool RegistryPermission_IsEmpty_m1_11123 (RegistryPermission_t1_1304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.RegistryPermission System.Security.Permissions.RegistryPermission::Cast(System.Security.IPermission)
extern "C" RegistryPermission_t1_1304 * RegistryPermission_Cast_m1_11124 (RegistryPermission_t1_1304 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::ThrowInvalidFlag(System.Security.Permissions.RegistryPermissionAccess,System.Boolean)
extern "C" void RegistryPermission_ThrowInvalidFlag_m1_11125 (RegistryPermission_t1_1304 * __this, int32_t ___flag, bool ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.RegistryPermission::GetPathList(System.Collections.ArrayList)
extern "C" String_t* RegistryPermission_GetPathList_m1_11126 (RegistryPermission_t1_1304 * __this, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.RegistryPermission::KeyIsSubsetOf(System.Collections.IList,System.Collections.IList)
extern "C" bool RegistryPermission_KeyIsSubsetOf_m1_11127 (RegistryPermission_t1_1304 * __this, Object_t * ___local, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::AddWithUnionKey(System.Collections.IList,System.String)
extern "C" void RegistryPermission_AddWithUnionKey_m1_11128 (RegistryPermission_t1_1304 * __this, Object_t * ___list, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.RegistryPermission::IntersectKeys(System.Collections.IList,System.Collections.IList,System.Collections.IList)
extern "C" void RegistryPermission_IntersectKeys_m1_11129 (RegistryPermission_t1_1304 * __this, Object_t * ___local, Object_t * ___target, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
