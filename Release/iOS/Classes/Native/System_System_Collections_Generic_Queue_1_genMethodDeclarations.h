﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::.ctor()
#define Queue_1__ctor_m3_1813(__this, method) (( void (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1__ctor_m3_1848_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3_2248(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_249 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3_1849_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3_2249(__this, method) (( bool (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m3_1850_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3_2250(__this, method) (( Object_t * (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m3_1851_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_2251(__this, method) (( Object_t* (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1852_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_2252(__this, method) (( Object_t * (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3_1853_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3_2253(__this, ___array, ___idx, method) (( void (*) (Queue_1_t3_249 *, ExifPropertyU5BU5D_t8_380*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3_1854_gshared)(__this, ___array, ___idx, method)
// T System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::Dequeue()
#define Queue_1_Dequeue_m3_1815(__this, method) (( ExifProperty_t8_99 * (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_Dequeue_m3_1855_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::Peek()
#define Queue_1_Peek_m3_2254(__this, method) (( ExifProperty_t8_99 * (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_Peek_m3_1856_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::Enqueue(T)
#define Queue_1_Enqueue_m3_1814(__this, ___item, method) (( void (*) (Queue_1_t3_249 *, ExifProperty_t8_99 *, const MethodInfo*))Queue_1_Enqueue_m3_1857_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3_2255(__this, ___new_size, method) (( void (*) (Queue_1_t3_249 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3_1858_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::get_Count()
#define Queue_1_get_Count_m3_2256(__this, method) (( int32_t (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_get_Count_m3_1859_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>::GetEnumerator()
#define Queue_1_GetEnumerator_m3_2257(__this, method) (( Enumerator_t3_289  (*) (Queue_1_t3_249 *, const MethodInfo*))Queue_1_GetEnumerator_m3_1860_gshared)(__this, method)
