﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_145.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18728_gshared (InternalEnumerator_1_t1_2262 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18728(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2262 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18728_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18729_gshared (InternalEnumerator_1_t1_2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18729(__this, method) (( void (*) (InternalEnumerator_1_t1_2262 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18729_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18730_gshared (InternalEnumerator_1_t1_2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18730(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2262 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18730_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18731_gshared (InternalEnumerator_1_t1_2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18731(__this, method) (( void (*) (InternalEnumerator_1_t1_2262 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18731_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18732_gshared (InternalEnumerator_1_t1_2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18732(__this, method) (( bool (*) (InternalEnumerator_1_t1_2262 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18732_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t6_211  InternalEnumerator_1_get_Current_m1_18733_gshared (InternalEnumerator_1_t1_2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18733(__this, method) (( GcScoreData_t6_211  (*) (InternalEnumerator_1_t1_2262 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18733_gshared)(__this, method)
