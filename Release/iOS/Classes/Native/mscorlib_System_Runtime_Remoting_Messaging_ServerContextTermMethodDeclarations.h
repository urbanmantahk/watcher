﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.ServerContextTerminatorSink
struct ServerContextTerminatorSink_t1_961;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.ServerContextTerminatorSink::.ctor()
extern "C" void ServerContextTerminatorSink__ctor_m1_8648 (ServerContextTerminatorSink_t1_961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.ServerContextTerminatorSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ServerContextTerminatorSink_SyncProcessMessage_m1_8649 (ServerContextTerminatorSink_t1_961 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.ServerContextTerminatorSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * ServerContextTerminatorSink_AsyncProcessMessage_m1_8650 (ServerContextTerminatorSink_t1_961 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.ServerContextTerminatorSink::get_NextSink()
extern "C" Object_t * ServerContextTerminatorSink_get_NextSink_m1_8651 (ServerContextTerminatorSink_t1_961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
