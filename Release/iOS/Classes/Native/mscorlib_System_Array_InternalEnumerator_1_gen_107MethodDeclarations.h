﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.SerializationEntry>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16978_gshared (InternalEnumerator_1_t1_2127 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16978(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2127 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16978_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.SerializationEntry>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16979_gshared (InternalEnumerator_1_t1_2127 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16979(__this, method) (( void (*) (InternalEnumerator_1_t1_2127 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16979_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.SerializationEntry>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16980_gshared (InternalEnumerator_1_t1_2127 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16980(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2127 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16980_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.SerializationEntry>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16981_gshared (InternalEnumerator_1_t1_2127 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16981(__this, method) (( void (*) (InternalEnumerator_1_t1_2127 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16981_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.SerializationEntry>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16982_gshared (InternalEnumerator_1_t1_2127 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16982(__this, method) (( bool (*) (InternalEnumerator_1_t1_2127 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16982_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.SerializationEntry>::get_Current()
extern "C" SerializationEntry_t1_1093  InternalEnumerator_1_get_Current_m1_16983_gshared (InternalEnumerator_1_t1_2127 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16983(__this, method) (( SerializationEntry_t1_1093  (*) (InternalEnumerator_1_t1_2127 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16983_gshared)(__this, method)
