﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibFuncFlags.h"

// System.Runtime.InteropServices.TypeLibFuncFlags
struct  TypeLibFuncFlags_t1_834 
{
	// System.Int32 System.Runtime.InteropServices.TypeLibFuncFlags::value__
	int32_t ___value___1;
};
