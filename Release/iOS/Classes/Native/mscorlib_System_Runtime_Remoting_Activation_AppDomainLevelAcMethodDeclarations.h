﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Activation.AppDomainLevelActivator
struct AppDomainLevelActivator_t1_853;
// System.String
struct String_t;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1_851;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage
struct IConstructionReturnMessage_t1_1703;
// System.Runtime.Remoting.Activation.IConstructionCallMessage
struct IConstructionCallMessage_t1_1702;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Activation_ActivatorLevel.h"

// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
extern "C" void AppDomainLevelActivator__ctor_m1_7952 (AppDomainLevelActivator_t1_853 * __this, String_t* ___activationUrl, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.ActivatorLevel System.Runtime.Remoting.Activation.AppDomainLevelActivator::get_Level()
extern "C" int32_t AppDomainLevelActivator_get_Level_m1_7953 (AppDomainLevelActivator_t1_853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.AppDomainLevelActivator::get_NextActivator()
extern "C" Object_t * AppDomainLevelActivator_get_NextActivator_m1_7954 (AppDomainLevelActivator_t1_853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::set_NextActivator(System.Runtime.Remoting.Activation.IActivator)
extern "C" void AppDomainLevelActivator_set_NextActivator_m1_7955 (AppDomainLevelActivator_t1_853 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Activation.IConstructionReturnMessage System.Runtime.Remoting.Activation.AppDomainLevelActivator::Activate(System.Runtime.Remoting.Activation.IConstructionCallMessage)
extern "C" Object_t * AppDomainLevelActivator_Activate_m1_7956 (AppDomainLevelActivator_t1_853 * __this, Object_t * ___ctorCall, const MethodInfo* method) IL2CPP_METHOD_ATTR;
