﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.HijriCalendar
struct  HijriCalendar_t1_374  : public Calendar_t1_338
{
	// System.Int32 System.Globalization.HijriCalendar::M_AddHijriDate
	int32_t ___M_AddHijriDate_10;
};
struct HijriCalendar_t1_374_StaticFields{
	// System.Int32 System.Globalization.HijriCalendar::HijriEra
	int32_t ___HijriEra_7;
	// System.Int32 System.Globalization.HijriCalendar::M_MinFixed
	int32_t ___M_MinFixed_8;
	// System.Int32 System.Globalization.HijriCalendar::M_MaxFixed
	int32_t ___M_MaxFixed_9;
	// System.DateTime System.Globalization.HijriCalendar::Min
	DateTime_t1_150  ___Min_11;
	// System.DateTime System.Globalization.HijriCalendar::Max
	DateTime_t1_150  ___Max_12;
};
