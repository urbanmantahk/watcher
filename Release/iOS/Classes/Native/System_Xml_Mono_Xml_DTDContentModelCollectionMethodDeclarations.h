﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t4_93;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t4_92;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDContentModelCollection::.ctor()
extern "C" void DTDContentModelCollection__ctor_m4_182 (DTDContentModelCollection_t4_93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDContentModel Mono.Xml.DTDContentModelCollection::get_Item(System.Int32)
extern "C" DTDContentModel_t4_92 * DTDContentModelCollection_get_Item_m4_183 (DTDContentModelCollection_t4_93 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Xml.DTDContentModelCollection::get_Count()
extern "C" int32_t DTDContentModelCollection_get_Count_m4_184 (DTDContentModelCollection_t4_93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDContentModelCollection::Add(Mono.Xml.DTDContentModel)
extern "C" void DTDContentModelCollection_Add_m4_185 (DTDContentModelCollection_t4_93 * __this, DTDContentModel_t4_92 * ___model, const MethodInfo* method) IL2CPP_METHOD_ATTR;
