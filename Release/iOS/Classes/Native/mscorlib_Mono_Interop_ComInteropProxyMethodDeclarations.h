﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Interop.ComInteropProxy
struct ComInteropProxy_t1_129;
// System.Type
struct Type_t;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void Mono.Interop.ComInteropProxy::.ctor(System.Type)
extern "C" void ComInteropProxy__ctor_m1_1732 (ComInteropProxy_t1_129 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Interop.ComInteropProxy::.ctor(System.IntPtr)
extern "C" void ComInteropProxy__ctor_m1_1733 (ComInteropProxy_t1_129 * __this, IntPtr_t ___pUnk, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Interop.ComInteropProxy::.ctor(System.IntPtr,System.Type)
extern "C" void ComInteropProxy__ctor_m1_1734 (ComInteropProxy_t1_129 * __this, IntPtr_t ___pUnk, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Interop.ComInteropProxy::AddProxy(System.IntPtr,Mono.Interop.ComInteropProxy)
extern "C" void ComInteropProxy_AddProxy_m1_1735 (Object_t * __this /* static, unused */, IntPtr_t ___pItf, ComInteropProxy_t1_129 * ___proxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Interop.ComInteropProxy Mono.Interop.ComInteropProxy::FindProxy(System.IntPtr)
extern "C" ComInteropProxy_t1_129 * ComInteropProxy_FindProxy_m1_1736 (Object_t * __this /* static, unused */, IntPtr_t ___pItf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Interop.ComInteropProxy::CacheProxy()
extern "C" void ComInteropProxy_CacheProxy_m1_1737 (ComInteropProxy_t1_129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Interop.ComInteropProxy Mono.Interop.ComInteropProxy::GetProxy(System.IntPtr,System.Type)
extern "C" ComInteropProxy_t1_129 * ComInteropProxy_GetProxy_m1_1738 (Object_t * __this /* static, unused */, IntPtr_t ___pItf, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Interop.ComInteropProxy Mono.Interop.ComInteropProxy::CreateProxy(System.Type)
extern "C" ComInteropProxy_t1_129 * ComInteropProxy_CreateProxy_m1_1739 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage Mono.Interop.ComInteropProxy::Invoke(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ComInteropProxy_Invoke_m1_1740 (ComInteropProxy_t1_129 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Interop.ComInteropProxy::get_TypeName()
extern "C" String_t* ComInteropProxy_get_TypeName_m1_1741 (ComInteropProxy_t1_129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Interop.ComInteropProxy::set_TypeName(System.String)
extern "C" void ComInteropProxy_set_TypeName_m1_1742 (ComInteropProxy_t1_129 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Interop.ComInteropProxy::CanCastTo(System.Type,System.Object)
extern "C" bool ComInteropProxy_CanCastTo_m1_1743 (ComInteropProxy_t1_129 * __this, Type_t * ___fromType, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
