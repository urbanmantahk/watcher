﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterUser.h"

// VoxelBusters.NativePlugins.Internal.iOSTwitterUser
struct  iOSTwitterUser_t8_299  : public TwitterUser_t8_298
{
};
