﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger
struct SoapPositiveInteger_t1_991;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::.ctor()
extern "C" void SoapPositiveInteger__ctor_m1_8872 (SoapPositiveInteger_t1_991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::.ctor(System.Decimal)
extern "C" void SoapPositiveInteger__ctor_m1_8873 (SoapPositiveInteger_t1_991 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::get_Value()
extern "C" Decimal_t1_19  SoapPositiveInteger_get_Value_m1_8874 (SoapPositiveInteger_t1_991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::set_Value(System.Decimal)
extern "C" void SoapPositiveInteger_set_Value_m1_8875 (SoapPositiveInteger_t1_991 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::get_XsdType()
extern "C" String_t* SoapPositiveInteger_get_XsdType_m1_8876 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::GetXsdType()
extern "C" String_t* SoapPositiveInteger_GetXsdType_m1_8877 (SoapPositiveInteger_t1_991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::Parse(System.String)
extern "C" SoapPositiveInteger_t1_991 * SoapPositiveInteger_Parse_m1_8878 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger::ToString()
extern "C" String_t* SoapPositiveInteger_ToString_m1_8879 (SoapPositiveInteger_t1_991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
