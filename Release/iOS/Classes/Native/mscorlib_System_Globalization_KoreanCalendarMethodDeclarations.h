﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.KoreanCalendar
struct KoreanCalendar_t1_381;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Void System.Globalization.KoreanCalendar::.ctor()
extern "C" void KoreanCalendar__ctor_m1_4281 (KoreanCalendar_t1_381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.KoreanCalendar::.cctor()
extern "C" void KoreanCalendar__cctor_m1_4282 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.KoreanCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* KoreanCalendar_get_Eras_m1_4283 (KoreanCalendar_t1_381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::get_TwoDigitYearMax()
extern "C" int32_t KoreanCalendar_get_TwoDigitYearMax_m1_4284 (KoreanCalendar_t1_381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.KoreanCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void KoreanCalendar_set_TwoDigitYearMax_m1_4285 (KoreanCalendar_t1_381 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.KoreanCalendar::M_CheckEra(System.Int32&)
extern "C" void KoreanCalendar_M_CheckEra_m1_4286 (KoreanCalendar_t1_381 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::M_CheckYEG(System.Int32,System.Int32&)
extern "C" int32_t KoreanCalendar_M_CheckYEG_m1_4287 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.KoreanCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void KoreanCalendar_M_CheckYE_m1_4288 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::M_CheckYMEG(System.Int32,System.Int32,System.Int32&)
extern "C" int32_t KoreanCalendar_M_CheckYMEG_m1_4289 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::M_CheckYMDEG(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" int32_t KoreanCalendar_M_CheckYMDEG_m1_4290 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  KoreanCalendar_AddMonths_m1_4291 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  KoreanCalendar_AddYears_m1_4292 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t KoreanCalendar_GetDayOfMonth_m1_4293 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.KoreanCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t KoreanCalendar_GetDayOfWeek_m1_4294 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t KoreanCalendar_GetDayOfYear_m1_4295 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t KoreanCalendar_GetDaysInMonth_m1_4296 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t KoreanCalendar_GetDaysInYear_m1_4297 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetEra(System.DateTime)
extern "C" int32_t KoreanCalendar_GetEra_m1_4298 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t KoreanCalendar_GetLeapMonth_m1_4299 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetMonth(System.DateTime)
extern "C" int32_t KoreanCalendar_GetMonth_m1_4300 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t KoreanCalendar_GetMonthsInYear_m1_4301 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetWeekOfYear(System.DateTime,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" int32_t KoreanCalendar_GetWeekOfYear_m1_4302 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::GetYear(System.DateTime)
extern "C" int32_t KoreanCalendar_GetYear_m1_4303 (KoreanCalendar_t1_381 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.KoreanCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool KoreanCalendar_IsLeapDay_m1_4304 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.KoreanCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool KoreanCalendar_IsLeapMonth_m1_4305 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.KoreanCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool KoreanCalendar_IsLeapYear_m1_4306 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  KoreanCalendar_ToDateTime_m1_4307 (KoreanCalendar_t1_381 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.KoreanCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t KoreanCalendar_ToFourDigitYear_m1_4308 (KoreanCalendar_t1_381 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  KoreanCalendar_get_MinSupportedDateTime_m1_4309 (KoreanCalendar_t1_381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.KoreanCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  KoreanCalendar_get_MaxSupportedDateTime_m1_4310 (KoreanCalendar_t1_381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
