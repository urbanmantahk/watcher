﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_UnauthorizedAccessException.h"

// System.Security.AccessControl.PrivilegeNotHeldException
struct  PrivilegeNotHeldException_t1_1167  : public UnauthorizedAccessException_t1_1168
{
};
