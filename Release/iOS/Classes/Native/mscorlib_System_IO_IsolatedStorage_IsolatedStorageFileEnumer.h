﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageScope.h"

// System.IO.IsolatedStorage.IsolatedStorageFileEnumerator
struct  IsolatedStorageFileEnumerator_t1_401  : public Object_t
{
	// System.IO.IsolatedStorage.IsolatedStorageScope System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::_scope
	int32_t ____scope_0;
	// System.String[] System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::_storages
	StringU5BU5D_t1_238* ____storages_1;
	// System.Int32 System.IO.IsolatedStorage.IsolatedStorageFileEnumerator::_pos
	int32_t ____pos_2;
};
