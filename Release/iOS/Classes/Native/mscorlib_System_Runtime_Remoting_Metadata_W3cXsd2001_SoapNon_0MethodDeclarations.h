﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger
struct SoapNonPositiveInteger_t1_988;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::.ctor()
extern "C" void SoapNonPositiveInteger__ctor_m1_8848 (SoapNonPositiveInteger_t1_988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::.ctor(System.Decimal)
extern "C" void SoapNonPositiveInteger__ctor_m1_8849 (SoapNonPositiveInteger_t1_988 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::get_Value()
extern "C" Decimal_t1_19  SoapNonPositiveInteger_get_Value_m1_8850 (SoapNonPositiveInteger_t1_988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::set_Value(System.Decimal)
extern "C" void SoapNonPositiveInteger_set_Value_m1_8851 (SoapNonPositiveInteger_t1_988 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::get_XsdType()
extern "C" String_t* SoapNonPositiveInteger_get_XsdType_m1_8852 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::GetXsdType()
extern "C" String_t* SoapNonPositiveInteger_GetXsdType_m1_8853 (SoapNonPositiveInteger_t1_988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::Parse(System.String)
extern "C" SoapNonPositiveInteger_t1_988 * SoapNonPositiveInteger_Parse_m1_8854 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonPositiveInteger::ToString()
extern "C" String_t* SoapNonPositiveInteger_ToString_m1_8855 (SoapNonPositiveInteger_t1_988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
