﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Runtime.Remoting.Messaging.CADArgHolder
struct CADArgHolder_t1_921;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.CADMessageBase
struct  CADMessageBase_t1_924  : public Object_t
{
	// System.Object[] System.Runtime.Remoting.Messaging.CADMessageBase::_args
	ObjectU5BU5D_t1_272* ____args_0;
	// System.Byte[] System.Runtime.Remoting.Messaging.CADMessageBase::_serializedArgs
	ByteU5BU5D_t1_109* ____serializedArgs_1;
	// System.Int32 System.Runtime.Remoting.Messaging.CADMessageBase::_propertyCount
	int32_t ____propertyCount_2;
	// System.Runtime.Remoting.Messaging.CADArgHolder System.Runtime.Remoting.Messaging.CADMessageBase::_callContext
	CADArgHolder_t1_921 * ____callContext_3;
};
