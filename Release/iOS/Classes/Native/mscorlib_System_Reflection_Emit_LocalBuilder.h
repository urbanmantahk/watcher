﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_t1_480;

#include "mscorlib_System_Reflection_LocalVariableInfo.h"

// System.Reflection.Emit.LocalBuilder
struct  LocalBuilder_t1_527  : public LocalVariableInfo_t1_528
{
	// System.String System.Reflection.Emit.LocalBuilder::name
	String_t* ___name_3;
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.LocalBuilder::ilgen
	ILGenerator_t1_480 * ___ilgen_4;
	// System.Int32 System.Reflection.Emit.LocalBuilder::startOffset
	int32_t ___startOffset_5;
	// System.Int32 System.Reflection.Emit.LocalBuilder::endOffset
	int32_t ___endOffset_6;
};
