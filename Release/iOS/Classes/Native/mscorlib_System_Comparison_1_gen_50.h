﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.DesignPatterns.IObserver
struct IObserver_t8_374;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Comparison`1<VoxelBusters.DesignPatterns.IObserver>
struct  Comparison_1_t1_2638  : public MulticastDelegate_t1_21
{
};
