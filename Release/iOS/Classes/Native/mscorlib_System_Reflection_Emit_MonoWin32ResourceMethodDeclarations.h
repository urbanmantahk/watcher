﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_MonoWin32Resource.h"

// System.Void System.Reflection.Emit.MonoWin32Resource::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[])
extern "C" void MonoWin32Resource__ctor_m1_5452 (MonoWin32Resource_t1_465 * __this, int32_t ___res_type, int32_t ___res_id, int32_t ___lang_id, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void MonoWin32Resource_t1_465_marshal(const MonoWin32Resource_t1_465& unmarshaled, MonoWin32Resource_t1_465_marshaled& marshaled);
extern "C" void MonoWin32Resource_t1_465_marshal_back(const MonoWin32Resource_t1_465_marshaled& marshaled, MonoWin32Resource_t1_465& unmarshaled);
extern "C" void MonoWin32Resource_t1_465_marshal_cleanup(MonoWin32Resource_t1_465_marshaled& marshaled);
