﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.RegistrationServices
struct RegistrationServices_t1_819;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Assembly
struct Assembly_t1_467;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Runtime_InteropServices_AssemblyRegistration.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationClassCon.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationConnecti.h"

// System.Void System.Runtime.InteropServices.RegistrationServices::.ctor()
extern "C" void RegistrationServices__ctor_m1_7875 (RegistrationServices_t1_819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Runtime.InteropServices.RegistrationServices::GetManagedCategoryGuid()
extern "C" Guid_t1_319  RegistrationServices_GetManagedCategoryGuid_m1_7876 (RegistrationServices_t1_819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.RegistrationServices::GetProgIdForType(System.Type)
extern "C" String_t* RegistrationServices_GetProgIdForType_m1_7877 (RegistrationServices_t1_819 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Runtime.InteropServices.RegistrationServices::GetRegistrableTypesInAssembly(System.Reflection.Assembly)
extern "C" TypeU5BU5D_t1_31* RegistrationServices_GetRegistrableTypesInAssembly_m1_7878 (RegistrationServices_t1_819 * __this, Assembly_t1_467 * ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.RegistrationServices::RegisterAssembly(System.Reflection.Assembly,System.Runtime.InteropServices.AssemblyRegistrationFlags)
extern "C" bool RegistrationServices_RegisterAssembly_m1_7879 (RegistrationServices_t1_819 * __this, Assembly_t1_467 * ___assembly, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.RegistrationServices::RegisterTypeForComClients(System.Type,System.Guid&)
extern "C" void RegistrationServices_RegisterTypeForComClients_m1_7880 (RegistrationServices_t1_819 * __this, Type_t * ___type, Guid_t1_319 * ___g, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.RegistrationServices::TypeRepresentsComType(System.Type)
extern "C" bool RegistrationServices_TypeRepresentsComType_m1_7881 (RegistrationServices_t1_819 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.RegistrationServices::TypeRequiresRegistration(System.Type)
extern "C" bool RegistrationServices_TypeRequiresRegistration_m1_7882 (RegistrationServices_t1_819 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.RegistrationServices::UnregisterAssembly(System.Reflection.Assembly)
extern "C" bool RegistrationServices_UnregisterAssembly_m1_7883 (RegistrationServices_t1_819 * __this, Assembly_t1_467 * ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.RegistrationServices::RegisterTypeForComClients(System.Type,System.Runtime.InteropServices.RegistrationClassContext,System.Runtime.InteropServices.RegistrationConnectionType)
extern "C" int32_t RegistrationServices_RegisterTypeForComClients_m1_7884 (RegistrationServices_t1_819 * __this, Type_t * ___type, int32_t ___classContext, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.RegistrationServices::UnregisterTypeForComClients(System.Int32)
extern "C" void RegistrationServices_UnregisterTypeForComClients_m1_7885 (RegistrationServices_t1_819 * __this, int32_t ___cookie, const MethodInfo* method) IL2CPP_METHOD_ATTR;
