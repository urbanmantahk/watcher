﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"

// System.Security.Permissions.HostProtectionPermission
struct  HostProtectionPermission_t1_1280  : public CodeAccessPermission_t1_1268
{
	// System.Security.Permissions.HostProtectionResource System.Security.Permissions.HostProtectionPermission::_resources
	int32_t ____resources_1;
};
