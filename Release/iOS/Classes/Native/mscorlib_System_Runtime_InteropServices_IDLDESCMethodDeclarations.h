﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void IDLDESC_t1_783_marshal(const IDLDESC_t1_783& unmarshaled, IDLDESC_t1_783_marshaled& marshaled);
extern "C" void IDLDESC_t1_783_marshal_back(const IDLDESC_t1_783_marshaled& marshaled, IDLDESC_t1_783& unmarshaled);
extern "C" void IDLDESC_t1_783_marshal_cleanup(IDLDESC_t1_783_marshaled& marshaled);
