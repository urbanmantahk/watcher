﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifPointSubjectArea.h"

// ExifLibrary.ExifCircularSubjectArea
struct  ExifCircularSubjectArea_t8_104  : public ExifPointSubjectArea_t8_102
{
};
