﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.SymbolStore.SymDocumentType
struct SymDocumentType_t1_318;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.SymbolStore.SymDocumentType::.ctor()
extern "C" void SymDocumentType__ctor_m1_3563 (SymDocumentType_t1_318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
