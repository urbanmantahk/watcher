﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.DiscardableAttribute
struct DiscardableAttribute_t1_685;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.DiscardableAttribute::.ctor()
extern "C" void DiscardableAttribute__ctor_m1_7543 (DiscardableAttribute_t1_685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
