﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter
struct RSAPKCS1KeyExchangeDeformatter_t1_1238;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.String
struct String_t;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::.ctor()
extern "C" void RSAPKCS1KeyExchangeDeformatter__ctor_m1_10568 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1KeyExchangeDeformatter__ctor_m1_10569 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::get_Parameters()
extern "C" String_t* RSAPKCS1KeyExchangeDeformatter_get_Parameters_m1_10570 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::set_Parameters(System.String)
extern "C" void RSAPKCS1KeyExchangeDeformatter_set_Parameters_m1_10571 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::get_RNG()
extern "C" RandomNumberGenerator_t1_143 * RSAPKCS1KeyExchangeDeformatter_get_RNG_m1_10572 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::set_RNG(System.Security.Cryptography.RandomNumberGenerator)
extern "C" void RSAPKCS1KeyExchangeDeformatter_set_RNG_m1_10573 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, RandomNumberGenerator_t1_143 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::DecryptKeyExchange(System.Byte[])
extern "C" ByteU5BU5D_t1_109* RSAPKCS1KeyExchangeDeformatter_DecryptKeyExchange_m1_10574 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, ByteU5BU5D_t1_109* ___rgbIn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1KeyExchangeDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1KeyExchangeDeformatter_SetKey_m1_10575 (RSAPKCS1KeyExchangeDeformatter_t1_1238 * __this, AsymmetricAlgorithm_t1_228 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
