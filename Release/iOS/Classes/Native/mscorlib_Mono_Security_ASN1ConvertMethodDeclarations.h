﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.ASN1
struct ASN1_t1_149;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromDateTime(System.DateTime)
extern "C" ASN1_t1_149 * ASN1Convert_FromDateTime_m1_2439 (Object_t * __this /* static, unused */, DateTime_t1_150  ___dt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromInt32(System.Int32)
extern "C" ASN1_t1_149 * ASN1Convert_FromInt32_m1_2440 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromOid(System.String)
extern "C" ASN1_t1_149 * ASN1Convert_FromOid_m1_2441 (Object_t * __this /* static, unused */, String_t* ___oid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.ASN1Convert::FromUnsignedBigInteger(System.Byte[])
extern "C" ASN1_t1_149 * ASN1Convert_FromUnsignedBigInteger_m1_2442 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___big, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.ASN1Convert::ToInt32(Mono.Security.ASN1)
extern "C" int32_t ASN1Convert_ToInt32_m1_2443 (Object_t * __this /* static, unused */, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.ASN1Convert::ToOid(Mono.Security.ASN1)
extern "C" String_t* ASN1Convert_ToOid_m1_2444 (Object_t * __this /* static, unused */, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.ASN1Convert::ToDateTime(Mono.Security.ASN1)
extern "C" DateTime_t1_150  ASN1Convert_ToDateTime_m1_2445 (Object_t * __this /* static, unused */, ASN1_t1_149 * ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
