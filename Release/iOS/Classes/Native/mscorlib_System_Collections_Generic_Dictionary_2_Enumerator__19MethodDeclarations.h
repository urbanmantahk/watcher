﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26464_gshared (Enumerator_t1_2670 * __this, Dictionary_2_t1_2663 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_26464(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2670 *, Dictionary_2_t1_2663 *, const MethodInfo*))Enumerator__ctor_m1_26464_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26465_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_26465(__this, method) (( Object_t * (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_26465_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26466_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_26466(__this, method) (( void (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_26466_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26467_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26467(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26467_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26468_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26468(__this, method) (( Object_t * (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26468_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26469_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26469(__this, method) (( Object_t * (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26469_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26470_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_26470(__this, method) (( bool (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_MoveNext_m1_26470_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1_2665  Enumerator_get_Current_m1_26471_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_26471(__this, method) (( KeyValuePair_2_t1_2665  (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_get_Current_m1_26471_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m1_26472_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_26472(__this, method) (( int32_t (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_26472_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m1_26473_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_26473(__this, method) (( Object_t * (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_26473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::Reset()
extern "C" void Enumerator_Reset_m1_26474_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_26474(__this, method) (( void (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_Reset_m1_26474_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_26475_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_26475(__this, method) (( void (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_VerifyState_m1_26475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_26476_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_26476(__this, method) (( void (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_26476_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_26477_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_26477(__this, method) (( void (*) (Enumerator_t1_2670 *, const MethodInfo*))Enumerator_Dispose_m1_26477_gshared)(__this, method)
