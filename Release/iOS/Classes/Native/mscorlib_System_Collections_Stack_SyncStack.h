﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Stack
struct Stack_t1_243;

#include "mscorlib_System_Collections_Stack.h"

// System.Collections.Stack/SyncStack
struct  SyncStack_t1_309  : public Stack_t1_243
{
	// System.Collections.Stack System.Collections.Stack/SyncStack::stack
	Stack_t1_243 * ___stack_6;
};
