﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1_26552(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_1927 *, Dictionary_2_t1_1904 *, const MethodInfo*))ValueCollection__ctor_m1_26482_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26553(__this, ___item, method) (( void (*) (ValueCollection_t1_1927 *, ExifProperty_t8_99 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26483_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26554(__this, method) (( void (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26484_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26555(__this, ___item, method) (( bool (*) (ValueCollection_t1_1927 *, ExifProperty_t8_99 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26485_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26556(__this, ___item, method) (( bool (*) (ValueCollection_t1_1927 *, ExifProperty_t8_99 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26486_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26557(__this, method) (( Object_t* (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26487_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_26558(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1927 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_26488_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26559(__this, method) (( Object_t * (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26560(__this, method) (( bool (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26490_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26561(__this, method) (( bool (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26491_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26562(__this, method) (( Object_t * (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1_26563(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_1927 *, ExifPropertyU5BU5D_t8_380*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_26493_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1_15050(__this, method) (( Enumerator_t1_1926  (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_26494_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Count()
#define ValueCollection_get_Count_m1_26564(__this, method) (( int32_t (*) (ValueCollection_t1_1927 *, const MethodInfo*))ValueCollection_get_Count_m1_26495_gshared)(__this, method)
