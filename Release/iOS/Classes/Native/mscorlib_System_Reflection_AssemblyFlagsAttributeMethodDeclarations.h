﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyFlagsAttribute
struct AssemblyFlagsAttribute_t1_572;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"

// System.Void System.Reflection.AssemblyFlagsAttribute::.ctor(System.UInt32)
extern "C" void AssemblyFlagsAttribute__ctor_m1_6684 (AssemblyFlagsAttribute_t1_572 * __this, uint32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyFlagsAttribute::.ctor(System.Int32)
extern "C" void AssemblyFlagsAttribute__ctor_m1_6685 (AssemblyFlagsAttribute_t1_572 * __this, int32_t ___assemblyFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.AssemblyFlagsAttribute::.ctor(System.Reflection.AssemblyNameFlags)
extern "C" void AssemblyFlagsAttribute__ctor_m1_6686 (AssemblyFlagsAttribute_t1_572 * __this, int32_t ___assemblyFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Reflection.AssemblyFlagsAttribute::get_Flags()
extern "C" uint32_t AssemblyFlagsAttribute_get_Flags_m1_6687 (AssemblyFlagsAttribute_t1_572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.AssemblyFlagsAttribute::get_AssemblyFlags()
extern "C" int32_t AssemblyFlagsAttribute_get_AssemblyFlags_m1_6688 (AssemblyFlagsAttribute_t1_572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
