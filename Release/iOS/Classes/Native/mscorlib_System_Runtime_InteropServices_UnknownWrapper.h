﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Runtime.InteropServices.UnknownWrapper
struct  UnknownWrapper_t1_842  : public Object_t
{
	// System.Object System.Runtime.InteropServices.UnknownWrapper::InternalObject
	Object_t * ___InternalObject_0;
};
