﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Reflection_Emit_DerivedType.h"

// System.Reflection.Emit.ArrayType
struct  ArrayType_t1_490  : public DerivedType_t1_489
{
	// System.Int32 System.Reflection.Emit.ArrayType::rank
	int32_t ___rank_9;
};
