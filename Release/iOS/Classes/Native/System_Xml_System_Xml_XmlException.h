﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_SystemException.h"

// System.Xml.XmlException
struct  XmlException_t4_137  : public SystemException_t1_250
{
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_13;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_14;
	// System.String System.Xml.XmlException::res
	String_t* ___res_15;
	// System.String[] System.Xml.XmlException::messages
	StringU5BU5D_t1_238* ___messages_16;
};
