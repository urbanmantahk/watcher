﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatio.h"

// VoxelBusters.NativePlugins.eABAuthorizationStatus
struct  eABAuthorizationStatus_t8_202 
{
	// System.Int32 VoxelBusters.NativePlugins.eABAuthorizationStatus::value__
	int32_t ___value___1;
};
