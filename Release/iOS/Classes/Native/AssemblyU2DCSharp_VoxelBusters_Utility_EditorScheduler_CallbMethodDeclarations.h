﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.EditorScheduler/CallbackFunction
struct CallbackFunction_t8_151;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void VoxelBusters.Utility.EditorScheduler/CallbackFunction::.ctor(System.Object,System.IntPtr)
extern "C" void CallbackFunction__ctor_m8_874 (CallbackFunction_t8_151 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorScheduler/CallbackFunction::Invoke()
extern "C" void CallbackFunction_Invoke_m8_875 (CallbackFunction_t8_151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CallbackFunction_t8_151(Il2CppObject* delegate);
// System.IAsyncResult VoxelBusters.Utility.EditorScheduler/CallbackFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * CallbackFunction_BeginInvoke_m8_876 (CallbackFunction_t8_151 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.EditorScheduler/CallbackFunction::EndInvoke(System.IAsyncResult)
extern "C" void CallbackFunction_EndInvoke_m8_877 (CallbackFunction_t8_151 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
