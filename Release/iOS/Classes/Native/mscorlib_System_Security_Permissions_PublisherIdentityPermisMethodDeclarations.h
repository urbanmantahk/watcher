﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.PublisherIdentityPermission
struct PublisherIdentityPermission_t1_1299;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.Permissions.PublisherIdentityPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void PublisherIdentityPermission__ctor_m1_11063 (PublisherIdentityPermission_t1_1299 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PublisherIdentityPermission::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" void PublisherIdentityPermission__ctor_m1_11064 (PublisherIdentityPermission_t1_1299 * __this, X509Certificate_t1_1179 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.PublisherIdentityPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t PublisherIdentityPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11065 (PublisherIdentityPermission_t1_1299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Permissions.PublisherIdentityPermission::get_Certificate()
extern "C" X509Certificate_t1_1179 * PublisherIdentityPermission_get_Certificate_m1_11066 (PublisherIdentityPermission_t1_1299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PublisherIdentityPermission::set_Certificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" void PublisherIdentityPermission_set_Certificate_m1_11067 (PublisherIdentityPermission_t1_1299 * __this, X509Certificate_t1_1179 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PublisherIdentityPermission::Copy()
extern "C" Object_t * PublisherIdentityPermission_Copy_m1_11068 (PublisherIdentityPermission_t1_1299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PublisherIdentityPermission::FromXml(System.Security.SecurityElement)
extern "C" void PublisherIdentityPermission_FromXml_m1_11069 (PublisherIdentityPermission_t1_1299 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PublisherIdentityPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * PublisherIdentityPermission_Intersect_m1_11070 (PublisherIdentityPermission_t1_1299 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PublisherIdentityPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool PublisherIdentityPermission_IsSubsetOf_m1_11071 (PublisherIdentityPermission_t1_1299 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.PublisherIdentityPermission::ToXml()
extern "C" SecurityElement_t1_242 * PublisherIdentityPermission_ToXml_m1_11072 (PublisherIdentityPermission_t1_1299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PublisherIdentityPermission::Union(System.Security.IPermission)
extern "C" Object_t * PublisherIdentityPermission_Union_m1_11073 (PublisherIdentityPermission_t1_1299 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.PublisherIdentityPermission System.Security.Permissions.PublisherIdentityPermission::Cast(System.Security.IPermission)
extern "C" PublisherIdentityPermission_t1_1299 * PublisherIdentityPermission_Cast_m1_11074 (PublisherIdentityPermission_t1_1299 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
