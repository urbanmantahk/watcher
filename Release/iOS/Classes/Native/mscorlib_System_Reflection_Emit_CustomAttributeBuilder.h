﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"

// System.Reflection.Emit.CustomAttributeBuilder
struct  CustomAttributeBuilder_t1_487  : public Object_t
{
	// System.Reflection.ConstructorInfo System.Reflection.Emit.CustomAttributeBuilder::ctor
	ConstructorInfo_t1_478 * ___ctor_0;
	// System.Byte[] System.Reflection.Emit.CustomAttributeBuilder::data
	ByteU5BU5D_t1_109* ___data_1;
};
struct CustomAttributeBuilder_t1_487_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Reflection.Emit.CustomAttributeBuilder::<>f__switch$map1C
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map1C_2;
};
