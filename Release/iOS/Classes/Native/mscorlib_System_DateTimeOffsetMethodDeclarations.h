﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Calendar
struct Calendar_t1_338;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IFormatProvider
struct IFormatProvider_t1_455;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t1_362;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Globalization_DateTimeStyles.h"
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.DateTimeOffset::.ctor(System.DateTime)
extern "C" void DateTimeOffset__ctor_m1_13911 (DateTimeOffset_t1_1527 * __this, DateTime_t1_150  ___dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.ctor(System.DateTime,System.TimeSpan)
extern "C" void DateTimeOffset__ctor_m1_13912 (DateTimeOffset_t1_1527 * __this, DateTime_t1_150  ___dateTime, TimeSpan_t1_368  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.ctor(System.Int64,System.TimeSpan)
extern "C" void DateTimeOffset__ctor_m1_13913 (DateTimeOffset_t1_1527 * __this, int64_t ___ticks, TimeSpan_t1_368  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.TimeSpan)
extern "C" void DateTimeOffset__ctor_m1_13914 (DateTimeOffset_t1_1527 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, TimeSpan_t1_368  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.TimeSpan)
extern "C" void DateTimeOffset__ctor_m1_13915 (DateTimeOffset_t1_1527 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, TimeSpan_t1_368  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Globalization.Calendar,System.TimeSpan)
extern "C" void DateTimeOffset__ctor_m1_13916 (DateTimeOffset_t1_1527 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, Calendar_t1_338 * ___calendar, TimeSpan_t1_368  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DateTimeOffset__ctor_m1_13917 (DateTimeOffset_t1_1527 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::.cctor()
extern "C" void DateTimeOffset__cctor_m1_13918 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::System.IComparable.CompareTo(System.Object)
extern "C" int32_t DateTimeOffset_System_IComparable_CompareTo_m1_13919 (DateTimeOffset_t1_1527 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m1_13920 (DateTimeOffset_t1_1527 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DateTimeOffset::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_13921 (DateTimeOffset_t1_1527 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::Add(System.TimeSpan)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_Add_m1_13922 (DateTimeOffset_t1_1527 * __this, TimeSpan_t1_368  ___timeSpan, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddDays(System.Double)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddDays_m1_13923 (DateTimeOffset_t1_1527 * __this, double ___days, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddHours(System.Double)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddHours_m1_13924 (DateTimeOffset_t1_1527 * __this, double ___hours, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddMilliseconds(System.Double)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddMilliseconds_m1_13925 (DateTimeOffset_t1_1527 * __this, double ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddMinutes(System.Double)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddMinutes_m1_13926 (DateTimeOffset_t1_1527 * __this, double ___minutes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddMonths(System.Int32)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddMonths_m1_13927 (DateTimeOffset_t1_1527 * __this, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddSeconds(System.Double)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddSeconds_m1_13928 (DateTimeOffset_t1_1527 * __this, double ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddTicks(System.Int64)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddTicks_m1_13929 (DateTimeOffset_t1_1527 * __this, int64_t ___ticks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::AddYears(System.Int32)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_AddYears_m1_13930 (DateTimeOffset_t1_1527 * __this, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::Compare(System.DateTimeOffset,System.DateTimeOffset)
extern "C" int32_t DateTimeOffset_Compare_m1_13931 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___first, DateTimeOffset_t1_1527  ___second, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::CompareTo(System.DateTimeOffset)
extern "C" int32_t DateTimeOffset_CompareTo_m1_13932 (DateTimeOffset_t1_1527 * __this, DateTimeOffset_t1_1527  ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.DateTimeOffset)
extern "C" bool DateTimeOffset_Equals_m1_13933 (DateTimeOffset_t1_1527 * __this, DateTimeOffset_t1_1527  ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.Object)
extern "C" bool DateTimeOffset_Equals_m1_13934 (DateTimeOffset_t1_1527 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::Equals(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_Equals_m1_13935 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___first, DateTimeOffset_t1_1527  ___second, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::EqualsExact(System.DateTimeOffset)
extern "C" bool DateTimeOffset_EqualsExact_m1_13936 (DateTimeOffset_t1_1527 * __this, DateTimeOffset_t1_1527  ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::FromFileTime(System.Int64)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_FromFileTime_m1_13937 (Object_t * __this /* static, unused */, int64_t ___fileTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::GetHashCode()
extern "C" int32_t DateTimeOffset_GetHashCode_m1_13938 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::Parse(System.String)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_Parse_m1_13939 (Object_t * __this /* static, unused */, String_t* ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::Parse(System.String,System.IFormatProvider)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_Parse_m1_13940 (Object_t * __this /* static, unused */, String_t* ___input, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::Parse(System.String,System.IFormatProvider,System.Globalization.DateTimeStyles)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_Parse_m1_13941 (Object_t * __this /* static, unused */, String_t* ___input, Object_t * ___formatProvider, int32_t ___styles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ParseExact(System.String,System.String,System.IFormatProvider)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_ParseExact_m1_13942 (Object_t * __this /* static, unused */, String_t* ___input, String_t* ___format, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ParseExact(System.String,System.String,System.IFormatProvider,System.Globalization.DateTimeStyles)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_ParseExact_m1_13943 (Object_t * __this /* static, unused */, String_t* ___input, String_t* ___format, Object_t * ___formatProvider, int32_t ___styles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ParseExact(System.String,System.String[],System.IFormatProvider,System.Globalization.DateTimeStyles)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_ParseExact_m1_13944 (Object_t * __this /* static, unused */, String_t* ___input, StringU5BU5D_t1_238* ___formats, Object_t * ___formatProvider, int32_t ___styles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::ParseExact(System.String,System.String[],System.Globalization.DateTimeFormatInfo,System.Globalization.DateTimeStyles,System.DateTimeOffset&)
extern "C" bool DateTimeOffset_ParseExact_m1_13945 (Object_t * __this /* static, unused */, String_t* ___input, StringU5BU5D_t1_238* ___formats, DateTimeFormatInfo_t1_362 * ___dfi, int32_t ___styles, DateTimeOffset_t1_1527 * ___ret, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::DoParse(System.String,System.String,System.Boolean,System.DateTimeOffset&,System.Globalization.DateTimeFormatInfo,System.Globalization.DateTimeStyles)
extern "C" bool DateTimeOffset_DoParse_m1_13946 (Object_t * __this /* static, unused */, String_t* ___input, String_t* ___format, bool ___exact, DateTimeOffset_t1_1527 * ___result, DateTimeFormatInfo_t1_362 * ___dfi, int32_t ___styles, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::ParseNumber(System.String,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Int32&)
extern "C" int32_t DateTimeOffset_ParseNumber_m1_13947 (Object_t * __this /* static, unused */, String_t* ___input, int32_t ___pos, int32_t ___digits, bool ___leading_zero, bool ___allow_leading_white, int32_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::ParseNumber(System.String,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Int32&,System.Int32&)
extern "C" int32_t DateTimeOffset_ParseNumber_m1_13948 (Object_t * __this /* static, unused */, String_t* ___input, int32_t ___pos, int32_t ___digits, bool ___leading_zero, bool ___allow_leading_white, int32_t* ___result, int32_t* ___digit_parsed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::ParseEnum(System.String,System.Int32,System.String[],System.Boolean,System.Int32&)
extern "C" int32_t DateTimeOffset_ParseEnum_m1_13949 (Object_t * __this /* static, unused */, String_t* ___input, int32_t ___pos, StringU5BU5D_t1_238* ___enums, bool ___allow_leading_white, int32_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::ParseChar(System.String,System.Int32,System.Char,System.Boolean,System.Int32&)
extern "C" int32_t DateTimeOffset_ParseChar_m1_13950 (Object_t * __this /* static, unused */, String_t* ___input, int32_t ___pos, uint16_t ___c, bool ___allow_leading_white, int32_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.DateTimeOffset::Subtract(System.DateTimeOffset)
extern "C" TimeSpan_t1_368  DateTimeOffset_Subtract_m1_13951 (DateTimeOffset_t1_1527 * __this, DateTimeOffset_t1_1527  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::Subtract(System.TimeSpan)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_Subtract_m1_13952 (DateTimeOffset_t1_1527 * __this, TimeSpan_t1_368  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTimeOffset::ToFileTime()
extern "C" int64_t DateTimeOffset_ToFileTime_m1_13953 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ToLocalTime()
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_ToLocalTime_m1_13954 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ToOffset(System.TimeSpan)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_ToOffset_m1_13955 (DateTimeOffset_t1_1527 * __this, TimeSpan_t1_368  ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTimeOffset::ToString()
extern "C" String_t* DateTimeOffset_ToString_m1_13956 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTimeOffset::ToString(System.IFormatProvider)
extern "C" String_t* DateTimeOffset_ToString_m1_13957 (DateTimeOffset_t1_1527 * __this, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTimeOffset::ToString(System.String)
extern "C" String_t* DateTimeOffset_ToString_m1_13958 (DateTimeOffset_t1_1527 * __this, String_t* ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.DateTimeOffset::ToString(System.String,System.IFormatProvider)
extern "C" String_t* DateTimeOffset_ToString_m1_13959 (DateTimeOffset_t1_1527 * __this, String_t* ___format, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::ToUniversalTime()
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_ToUniversalTime_m1_13960 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::TryParse(System.String,System.DateTimeOffset&)
extern "C" bool DateTimeOffset_TryParse_m1_13961 (Object_t * __this /* static, unused */, String_t* ___input, DateTimeOffset_t1_1527 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::TryParse(System.String,System.IFormatProvider,System.Globalization.DateTimeStyles,System.DateTimeOffset&)
extern "C" bool DateTimeOffset_TryParse_m1_13962 (Object_t * __this /* static, unused */, String_t* ___input, Object_t * ___formatProvider, int32_t ___styles, DateTimeOffset_t1_1527 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::TryParseExact(System.String,System.String,System.IFormatProvider,System.Globalization.DateTimeStyles,System.DateTimeOffset&)
extern "C" bool DateTimeOffset_TryParseExact_m1_13963 (Object_t * __this /* static, unused */, String_t* ___input, String_t* ___format, Object_t * ___formatProvider, int32_t ___styles, DateTimeOffset_t1_1527 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::TryParseExact(System.String,System.String[],System.IFormatProvider,System.Globalization.DateTimeStyles,System.DateTimeOffset&)
extern "C" bool DateTimeOffset_TryParseExact_m1_13964 (Object_t * __this /* static, unused */, String_t* ___input, StringU5BU5D_t1_238* ___formats, Object_t * ___formatProvider, int32_t ___styles, DateTimeOffset_t1_1527 * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTimeOffset::get_Date()
extern "C" DateTime_t1_150  DateTimeOffset_get_Date_m1_13965 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTimeOffset::get_DateTime()
extern "C" DateTime_t1_150  DateTimeOffset_get_DateTime_m1_13966 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Day()
extern "C" int32_t DateTimeOffset_get_Day_m1_13967 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.DateTimeOffset::get_DayOfWeek()
extern "C" int32_t DateTimeOffset_get_DayOfWeek_m1_13968 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_DayOfYear()
extern "C" int32_t DateTimeOffset_get_DayOfYear_m1_13969 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Hour()
extern "C" int32_t DateTimeOffset_get_Hour_m1_13970 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTimeOffset::get_LocalDateTime()
extern "C" DateTime_t1_150  DateTimeOffset_get_LocalDateTime_m1_13971 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Millisecond()
extern "C" int32_t DateTimeOffset_get_Millisecond_m1_13972 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Minute()
extern "C" int32_t DateTimeOffset_get_Minute_m1_13973 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Month()
extern "C" int32_t DateTimeOffset_get_Month_m1_13974 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::get_Now()
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_get_Now_m1_13975 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.DateTimeOffset::get_Offset()
extern "C" TimeSpan_t1_368  DateTimeOffset_get_Offset_m1_13976 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Second()
extern "C" int32_t DateTimeOffset_get_Second_m1_13977 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTimeOffset::get_Ticks()
extern "C" int64_t DateTimeOffset_get_Ticks_m1_13978 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.DateTimeOffset::get_TimeOfDay()
extern "C" TimeSpan_t1_368  DateTimeOffset_get_TimeOfDay_m1_13979 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.DateTimeOffset::get_UtcDateTime()
extern "C" DateTime_t1_150  DateTimeOffset_get_UtcDateTime_m1_13980 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::get_UtcNow()
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_get_UtcNow_m1_13981 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.DateTimeOffset::get_UtcTicks()
extern "C" int64_t DateTimeOffset_get_UtcTicks_m1_13982 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.DateTimeOffset::get_Year()
extern "C" int32_t DateTimeOffset_get_Year_m1_13983 (DateTimeOffset_t1_1527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::op_Addition(System.DateTimeOffset,System.TimeSpan)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_op_Addition_m1_13984 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___dateTimeTz, TimeSpan_t1_368  ___timeSpan, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::op_Equality(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_op_Equality_m1_13985 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::op_GreaterThan(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_op_GreaterThan_m1_13986 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::op_GreaterThanOrEqual(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_op_GreaterThanOrEqual_m1_13987 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::op_Implicit(System.DateTime)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_op_Implicit_m1_13988 (Object_t * __this /* static, unused */, DateTime_t1_150  ___dateTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::op_Inequality(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_op_Inequality_m1_13989 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::op_LessThan(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_op_LessThan_m1_13990 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.DateTimeOffset::op_LessThanOrEqual(System.DateTimeOffset,System.DateTimeOffset)
extern "C" bool DateTimeOffset_op_LessThanOrEqual_m1_13991 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan System.DateTimeOffset::op_Subtraction(System.DateTimeOffset,System.DateTimeOffset)
extern "C" TimeSpan_t1_368  DateTimeOffset_op_Subtraction_m1_13992 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___left, DateTimeOffset_t1_1527  ___right, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset System.DateTimeOffset::op_Subtraction(System.DateTimeOffset,System.TimeSpan)
extern "C" DateTimeOffset_t1_1527  DateTimeOffset_op_Subtraction_m1_13993 (Object_t * __this /* static, unused */, DateTimeOffset_t1_1527  ___dateTimeTz, TimeSpan_t1_368  ___timeSpan, const MethodInfo* method) IL2CPP_METHOD_ATTR;
