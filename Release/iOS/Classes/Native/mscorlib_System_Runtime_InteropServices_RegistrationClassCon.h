﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_RegistrationClassCon.h"

// System.Runtime.InteropServices.RegistrationClassContext
struct  RegistrationClassContext_t1_817 
{
	// System.Int32 System.Runtime.InteropServices.RegistrationClassContext::value__
	int32_t ___value___1;
};
