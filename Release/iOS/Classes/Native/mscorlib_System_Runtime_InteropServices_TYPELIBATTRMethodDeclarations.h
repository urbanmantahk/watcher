﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void TYPELIBATTR_t1_830_marshal(const TYPELIBATTR_t1_830& unmarshaled, TYPELIBATTR_t1_830_marshaled& marshaled);
extern "C" void TYPELIBATTR_t1_830_marshal_back(const TYPELIBATTR_t1_830_marshaled& marshaled, TYPELIBATTR_t1_830& unmarshaled);
extern "C" void TYPELIBATTR_t1_830_marshal_cleanup(TYPELIBATTR_t1_830_marshaled& marshaled);
