﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ResourceReader/ResourceEnumerator
struct ResourceEnumerator_t1_649;
// System.Resources.ResourceReader
struct ResourceReader_t1_650;
// System.Object
struct Object_t;
// System.IO.UnmanagedMemoryStream
struct UnmanagedMemoryStream_t1_460;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Resources.ResourceReader/ResourceEnumerator::.ctor(System.Resources.ResourceReader)
extern "C" void ResourceEnumerator__ctor_m1_7393 (ResourceEnumerator_t1_649 * __this, ResourceReader_t1_650 * ___readerToEnumerate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.ResourceReader/ResourceEnumerator::get_Index()
extern "C" int32_t ResourceEnumerator_get_Index_m1_7394 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Resources.ResourceReader/ResourceEnumerator::get_Entry()
extern "C" DictionaryEntry_t1_284  ResourceEnumerator_get_Entry_m1_7395 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceReader/ResourceEnumerator::get_Key()
extern "C" Object_t * ResourceEnumerator_get_Key_m1_7396 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceReader/ResourceEnumerator::get_Value()
extern "C" Object_t * ResourceEnumerator_get_Value_m1_7397 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.UnmanagedMemoryStream System.Resources.ResourceReader/ResourceEnumerator::get_ValueAsStream()
extern "C" UnmanagedMemoryStream_t1_460 * ResourceEnumerator_get_ValueAsStream_m1_7398 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Resources.ResourceReader/ResourceEnumerator::get_Current()
extern "C" Object_t * ResourceEnumerator_get_Current_m1_7399 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Resources.ResourceReader/ResourceEnumerator::MoveNext()
extern "C" bool ResourceEnumerator_MoveNext_m1_7400 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceReader/ResourceEnumerator::Reset()
extern "C" void ResourceEnumerator_Reset_m1_7401 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.ResourceReader/ResourceEnumerator::FillCache()
extern "C" void ResourceEnumerator_FillCache_m1_7402 (ResourceEnumerator_t1_649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
