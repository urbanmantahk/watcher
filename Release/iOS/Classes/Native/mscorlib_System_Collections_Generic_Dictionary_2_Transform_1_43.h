﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.UI/AlertDialogCompletion
struct AlertDialogCompletion_t8_300;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.String,VoxelBusters.NativePlugins.UI/AlertDialogCompletion,System.Collections.DictionaryEntry>
struct  Transform_1_t1_2753  : public MulticastDelegate_t1_21
{
};
