﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object
struct Object_t;
// System.IFormatProvider
struct IFormatProvider_t1_455;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"

// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.UInt32,System.UInt32)
extern "C" void UFraction32__ctor_m8_726 (UFraction32_t8_121 * __this, uint32_t ___numerator, uint32_t ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.UInt32)
extern "C" void UFraction32__ctor_m8_727 (UFraction32_t8_121 * __this, uint32_t ___numerator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(ExifLibrary.MathEx/UFraction32)
extern "C" void UFraction32__ctor_m8_728 (UFraction32_t8_121 * __this, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.Single)
extern "C" void UFraction32__ctor_m8_729 (UFraction32_t8_121 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.Double)
extern "C" void UFraction32__ctor_m8_730 (UFraction32_t8_121 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::.ctor(System.String)
extern "C" void UFraction32__ctor_m8_731 (UFraction32_t8_121 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::.cctor()
extern "C" void UFraction32__cctor_m8_732 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.MathEx/UFraction32::get_Numerator()
extern "C" uint32_t UFraction32_get_Numerator_m8_733 (UFraction32_t8_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::set_Numerator(System.UInt32)
extern "C" void UFraction32_set_Numerator_m8_734 (UFraction32_t8_121 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.MathEx/UFraction32::get_Denominator()
extern "C" uint32_t UFraction32_get_Denominator_m8_735 (UFraction32_t8_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::set_Denominator(System.UInt32)
extern "C" void UFraction32_set_Denominator_m8_736 (UFraction32_t8_121 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::IsNan(ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_IsNan_m8_737 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::IsInfinity(ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_IsInfinity_m8_738 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::Parse(System.String)
extern "C" UFraction32_t8_121  UFraction32_Parse_m8_739 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::TryParse(System.String,ExifLibrary.MathEx/UFraction32&)
extern "C" bool UFraction32_TryParse_m8_740 (Object_t * __this /* static, unused */, String_t* ___s, UFraction32_t8_121 * ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::Set(System.UInt32,System.UInt32)
extern "C" void UFraction32_Set_m8_741 (UFraction32_t8_121 * __this, uint32_t ___numerator, uint32_t ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::Inverse(ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_Inverse_m8_742 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::Equals(System.Object)
extern "C" bool UFraction32_Equals_m8_743 (UFraction32_t8_121 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::Equals(ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_Equals_m8_744 (UFraction32_t8_121 * __this, UFraction32_t8_121  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/UFraction32::GetHashCode()
extern "C" int32_t UFraction32_GetHashCode_m8_745 (UFraction32_t8_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/UFraction32::ToString(System.String,System.IFormatProvider)
extern "C" String_t* UFraction32_ToString_m8_746 (UFraction32_t8_121 * __this, String_t* ___format, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/UFraction32::ToString(System.String)
extern "C" String_t* UFraction32_ToString_m8_747 (UFraction32_t8_121 * __this, String_t* ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/UFraction32::ToString(System.IFormatProvider)
extern "C" String_t* UFraction32_ToString_m8_748 (UFraction32_t8_121 * __this, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.MathEx/UFraction32::ToString()
extern "C" String_t* UFraction32_ToString_m8_749 (UFraction32_t8_121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/UFraction32::CompareTo(System.Object)
extern "C" int32_t UFraction32_CompareTo_m8_750 (UFraction32_t8_121 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.MathEx/UFraction32::CompareTo(ExifLibrary.MathEx/UFraction32)
extern "C" int32_t UFraction32_CompareTo_m8_751 (UFraction32_t8_121 * __this, UFraction32_t8_121  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::FromDouble(System.Double,System.Int32)
extern "C" UFraction32_t8_121  UFraction32_FromDouble_m8_752 (Object_t * __this /* static, unused */, double ___value, int32_t ___precision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::FromString(System.String)
extern "C" UFraction32_t8_121  UFraction32_FromString_m8_753 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.MathEx/UFraction32::Reduce(System.UInt32&,System.UInt32&)
extern "C" void UFraction32_Reduce_m8_754 (Object_t * __this /* static, unused */, uint32_t* ___numerator, uint32_t* ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_755 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(System.UInt32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_756 (Object_t * __this /* static, unused */, uint32_t ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,System.Single)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_757 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(System.Single,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_758 (Object_t * __this /* static, unused */, float ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,System.Double)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_759 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(System.Double,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_760 (Object_t * __this /* static, unused */, double ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Multiply(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Multiply_m8_761 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_762 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,System.Single)
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_763 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,System.Double)
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_764 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Division(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Division_m8_765 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_766 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(System.UInt32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_767 (Object_t * __this /* static, unused */, uint32_t ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,System.Single)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_768 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(System.Single,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_769 (Object_t * __this /* static, unused */, float ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,System.Double)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_770 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(System.Double,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_771 (Object_t * __this /* static, unused */, double ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Addition(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Addition_m8_772 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,System.UInt32)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_773 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, uint32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(System.UInt32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_774 (Object_t * __this /* static, unused */, uint32_t ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,System.Single)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_775 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(System.Single,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_776 (Object_t * __this /* static, unused */, float ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,System.Double)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_777 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, double ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(System.Double,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_778 (Object_t * __this /* static, unused */, double ___n, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Subtraction(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Subtraction_m8_779 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Increment(ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Increment_m8_780 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::op_Decrement(ExifLibrary.MathEx/UFraction32)
extern "C" UFraction32_t8_121  UFraction32_op_Decrement_m8_781 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.MathEx/UFraction32::op_Explicit(ExifLibrary.MathEx/UFraction32)
extern "C" uint32_t UFraction32_op_Explicit_m8_782 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.MathEx/UFraction32::op_Explicit(ExifLibrary.MathEx/UFraction32)
extern "C" float UFraction32_op_Explicit_m8_783 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLibrary.MathEx/UFraction32::op_Explicit(ExifLibrary.MathEx/UFraction32)
extern "C" double UFraction32_op_Explicit_m8_784 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::op_Equality(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_Equality_m8_785 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::op_Inequality(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_Inequality_m8_786 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::op_LessThan(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_LessThan_m8_787 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ExifLibrary.MathEx/UFraction32::op_GreaterThan(ExifLibrary.MathEx/UFraction32,ExifLibrary.MathEx/UFraction32)
extern "C" bool UFraction32_op_GreaterThan_m8_788 (Object_t * __this /* static, unused */, UFraction32_t8_121  ___f1, UFraction32_t8_121  ___f2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
