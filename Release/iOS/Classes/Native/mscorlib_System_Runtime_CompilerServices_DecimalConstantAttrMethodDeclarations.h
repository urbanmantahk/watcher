﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.DecimalConstantAttribute
struct DecimalConstantAttribute_t1_63;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.CompilerServices.DecimalConstantAttribute::.ctor(System.Byte,System.Byte,System.UInt32,System.UInt32,System.UInt32)
extern "C" void DecimalConstantAttribute__ctor_m1_1338 (DecimalConstantAttribute_t1_63 * __this, uint8_t ___scale, uint8_t ___sign, uint32_t ___hi, uint32_t ___mid, uint32_t ___low, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.DecimalConstantAttribute::.ctor(System.Byte,System.Byte,System.Int32,System.Int32,System.Int32)
extern "C" void DecimalConstantAttribute__ctor_m1_1339 (DecimalConstantAttribute_t1_63 * __this, uint8_t ___scale, uint8_t ___sign, int32_t ___hi, int32_t ___mid, int32_t ___low, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.CompilerServices.DecimalConstantAttribute::get_Value()
extern "C" Decimal_t1_19  DecimalConstantAttribute_get_Value_m1_1340 (DecimalConstantAttribute_t1_63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
