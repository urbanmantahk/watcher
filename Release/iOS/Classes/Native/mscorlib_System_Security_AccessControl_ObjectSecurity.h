﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// System.Security.AccessControl.ObjectSecurity
struct  ObjectSecurity_t1_1127  : public Object_t
{
	// System.Boolean System.Security.AccessControl.ObjectSecurity::is_container
	bool ___is_container_0;
	// System.Boolean System.Security.AccessControl.ObjectSecurity::is_ds
	bool ___is_ds_1;
	// System.Boolean System.Security.AccessControl.ObjectSecurity::access_rules_modified
	bool ___access_rules_modified_2;
	// System.Boolean System.Security.AccessControl.ObjectSecurity::audit_rules_modified
	bool ___audit_rules_modified_3;
	// System.Boolean System.Security.AccessControl.ObjectSecurity::group_modified
	bool ___group_modified_4;
	// System.Boolean System.Security.AccessControl.ObjectSecurity::owner_modified
	bool ___owner_modified_5;
};
