﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ApplicationException.h"

// System.Threading.WaitHandleCannotBeOpenedException
struct  WaitHandleCannotBeOpenedException_t1_1489  : public ApplicationException_t1_605
{
};
