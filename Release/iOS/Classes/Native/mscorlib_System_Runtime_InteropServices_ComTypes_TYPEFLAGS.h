﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_TYPEFLAGS.h"

// System.Runtime.InteropServices.ComTypes.TYPEFLAGS
struct  TYPEFLAGS_t1_744 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.TYPEFLAGS::value__
	int32_t ___value___1;
};
