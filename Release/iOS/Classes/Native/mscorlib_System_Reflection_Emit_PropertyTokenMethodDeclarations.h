﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_PropertyToken.h"

// System.Void System.Reflection.Emit.PropertyToken::.ctor(System.Int32)
extern "C" void PropertyToken__ctor_m1_6348 (PropertyToken_t1_549 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyToken::.cctor()
extern "C" void PropertyToken__cctor_m1_6349 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyToken::Equals(System.Object)
extern "C" bool PropertyToken_Equals_m1_6350 (PropertyToken_t1_549 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyToken::Equals(System.Reflection.Emit.PropertyToken)
extern "C" bool PropertyToken_Equals_m1_6351 (PropertyToken_t1_549 * __this, PropertyToken_t1_549  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.PropertyToken::GetHashCode()
extern "C" int32_t PropertyToken_GetHashCode_m1_6352 (PropertyToken_t1_549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.PropertyToken::get_Token()
extern "C" int32_t PropertyToken_get_Token_m1_6353 (PropertyToken_t1_549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyToken::op_Equality(System.Reflection.Emit.PropertyToken,System.Reflection.Emit.PropertyToken)
extern "C" bool PropertyToken_op_Equality_m1_6354 (Object_t * __this /* static, unused */, PropertyToken_t1_549  ___a, PropertyToken_t1_549  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyToken::op_Inequality(System.Reflection.Emit.PropertyToken,System.Reflection.Emit.PropertyToken)
extern "C" bool PropertyToken_op_Inequality_m1_6355 (Object_t * __this /* static, unused */, PropertyToken_t1_549  ___a, PropertyToken_t1_549  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
