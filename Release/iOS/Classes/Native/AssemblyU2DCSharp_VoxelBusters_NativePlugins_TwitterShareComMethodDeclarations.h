﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.TwitterShareComposer
struct TwitterShareComposer_t8_284;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.TwitterShareComposer::.ctor()
extern "C" void TwitterShareComposer__ctor_m8_1685 (TwitterShareComposer_t8_284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
