﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_CodeAccessPermission.h"

// System.Security.Permissions.UrlIdentityPermission
struct  UrlIdentityPermission_t1_1321  : public CodeAccessPermission_t1_1268
{
	// System.String System.Security.Permissions.UrlIdentityPermission::url
	String_t* ___url_1;
};
