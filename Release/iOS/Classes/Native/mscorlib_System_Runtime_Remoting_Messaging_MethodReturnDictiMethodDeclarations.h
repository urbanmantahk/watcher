﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.MethodReturnDictionary
struct MethodReturnDictionary_t1_950;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t1_1718;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern "C" void MethodReturnDictionary__ctor_m1_8543 (MethodReturnDictionary_t1_950 * __this, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern "C" void MethodReturnDictionary__cctor_m1_8544 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
