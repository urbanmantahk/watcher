﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1_499;

#include "mscorlib_System_Reflection_FieldInfo.h"

// System.Reflection.Emit.FieldOnTypeBuilderInst
struct  FieldOnTypeBuilderInst_t1_506  : public FieldInfo_t
{
	// System.Reflection.MonoGenericClass System.Reflection.Emit.FieldOnTypeBuilderInst::instantiation
	MonoGenericClass_t1_485 * ___instantiation_0;
	// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.FieldOnTypeBuilderInst::fb
	FieldBuilder_t1_499 * ___fb_1;
};
