﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>
struct DefaultComparer_t1_2295;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::.ctor()
extern "C" void DefaultComparer__ctor_m1_19297_gshared (DefaultComparer_t1_2295 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_19297(__this, method) (( void (*) (DefaultComparer_t1_2295 *, const MethodInfo*))DefaultComparer__ctor_m1_19297_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Vector2>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m1_19298_gshared (DefaultComparer_t1_2295 * __this, Vector2_t6_47  ___x, Vector2_t6_47  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m1_19298(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1_2295 *, Vector2_t6_47 , Vector2_t6_47 , const MethodInfo*))DefaultComparer_Compare_m1_19298_gshared)(__this, ___x, ___y, method)
