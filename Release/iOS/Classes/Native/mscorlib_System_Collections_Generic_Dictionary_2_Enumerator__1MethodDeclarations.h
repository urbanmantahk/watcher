﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_26565(__this, ___dictionary, method) (( void (*) (Enumerator_t1_1925 *, Dictionary_2_t1_1904 *, const MethodInfo*))Enumerator__ctor_m1_26464_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_26566(__this, method) (( Object_t * (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_26465_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_26567(__this, method) (( void (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_26466_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26568(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26467_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26569(__this, method) (( Object_t * (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26468_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26570(__this, method) (( Object_t * (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26469_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::MoveNext()
#define Enumerator_MoveNext_m1_15048(__this, method) (( bool (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_MoveNext_m1_26470_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_Current()
#define Enumerator_get_Current_m1_15045(__this, method) (( KeyValuePair_2_t1_1924  (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_get_Current_m1_26471_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_26571(__this, method) (( int32_t (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_26472_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_26572(__this, method) (( ExifProperty_t8_99 * (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_26473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Reset()
#define Enumerator_Reset_m1_26573(__this, method) (( void (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_Reset_m1_26474_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::VerifyState()
#define Enumerator_VerifyState_m1_26574(__this, method) (( void (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_VerifyState_m1_26475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_26575(__this, method) (( void (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_26476_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,ExifLibrary.ExifProperty>::Dispose()
#define Enumerator_Dispose_m1_26576(__this, method) (( void (*) (Enumerator_t1_1925 *, const MethodInfo*))Enumerator_Dispose_m1_26477_gshared)(__this, method)
