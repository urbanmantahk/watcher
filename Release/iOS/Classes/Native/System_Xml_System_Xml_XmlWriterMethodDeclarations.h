﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlWriter
struct XmlWriter_t4_182;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1_405;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_t4_187;
// System.IO.TextWriter
struct TextWriter_t1_449;
// System.Xml.XmlReader
struct XmlReader_t4_160;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlSpace.h"

// System.Void System.Xml.XmlWriter::.ctor()
extern "C" void XmlWriter__ctor_m4_1018 (XmlWriter_t4_182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::System.IDisposable.Dispose()
extern "C" void XmlWriter_System_IDisposable_Dispose_m4_1019 (XmlWriter_t4_182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWriter::get_XmlLang()
extern "C" String_t* XmlWriter_get_XmlLang_m4_1020 (XmlWriter_t4_182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlSpace System.Xml.XmlWriter::get_XmlSpace()
extern "C" int32_t XmlWriter_get_XmlSpace_m4_1021 (XmlWriter_t4_182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlWriter System.Xml.XmlWriter::Create(System.IO.Stream,System.Xml.XmlWriterSettings)
extern "C" XmlWriter_t4_182 * XmlWriter_Create_m4_1022 (Object_t * __this /* static, unused */, Stream_t1_405 * ___stream, XmlWriterSettings_t4_187 * ___settings, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlWriter System.Xml.XmlWriter::Create(System.IO.TextWriter,System.Xml.XmlWriterSettings)
extern "C" XmlWriter_t4_182 * XmlWriter_Create_m4_1023 (Object_t * __this /* static, unused */, TextWriter_t1_449 * ___writer, XmlWriterSettings_t4_187 * ___settings, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlWriter System.Xml.XmlWriter::Create(System.Xml.XmlWriter,System.Xml.XmlWriterSettings)
extern "C" XmlWriter_t4_182 * XmlWriter_Create_m4_1024 (Object_t * __this /* static, unused */, XmlWriter_t4_182 * ___writer, XmlWriterSettings_t4_187 * ___settings, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlWriter System.Xml.XmlWriter::CreateTextWriter(System.IO.TextWriter,System.Xml.XmlWriterSettings,System.Boolean)
extern "C" XmlWriter_t4_182 * XmlWriter_CreateTextWriter_m4_1025 (Object_t * __this /* static, unused */, TextWriter_t1_449 * ___writer, XmlWriterSettings_t4_187 * ___settings, bool ___closeOutput, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::Dispose(System.Boolean)
extern "C" void XmlWriter_Dispose_m4_1026 (XmlWriter_t4_182 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::WriteAttribute(System.Xml.XmlReader,System.Boolean)
extern "C" void XmlWriter_WriteAttribute_m4_1027 (XmlWriter_t4_182 * __this, XmlReader_t4_160 * ___reader, bool ___defattr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::WriteAttributeString(System.String,System.String)
extern "C" void XmlWriter_WriteAttributeString_m4_1028 (XmlWriter_t4_182 * __this, String_t* ___localName, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::WriteAttributeString(System.String,System.String,System.String,System.String)
extern "C" void XmlWriter_WriteAttributeString_m4_1029 (XmlWriter_t4_182 * __this, String_t* ___prefix, String_t* ___localName, String_t* ___ns, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::WriteElementString(System.String,System.String)
extern "C" void XmlWriter_WriteElementString_m4_1030 (XmlWriter_t4_182 * __this, String_t* ___localName, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::WriteNode(System.Xml.XmlReader,System.Boolean)
extern "C" void XmlWriter_WriteNode_m4_1031 (XmlWriter_t4_182 * __this, XmlReader_t4_160 * ___reader, bool ___defattr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWriter::WriteStartElement(System.String)
extern "C" void XmlWriter_WriteStartElement_m4_1032 (XmlWriter_t4_182 * __this, String_t* ___localName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
