﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"

// System.Runtime.Remoting.Messaging.MethodCallMessageWrapper/DictionaryWrapper
struct  DictionaryWrapper_t1_945  : public MethodCallDictionary_t1_944
{
	// System.Collections.IDictionary System.Runtime.Remoting.Messaging.MethodCallMessageWrapper/DictionaryWrapper::_wrappedDictionary
	Object_t * ____wrappedDictionary_7;
};
struct DictionaryWrapper_t1_945_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallMessageWrapper/DictionaryWrapper::_keys
	StringU5BU5D_t1_238* ____keys_8;
};
