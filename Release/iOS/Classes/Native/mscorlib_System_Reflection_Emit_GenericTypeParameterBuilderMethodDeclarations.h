﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.GenericTypeParameterBuilder
struct GenericTypeParameterBuilder_t1_509;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_1671;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1_1667;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.Module
struct Module_t1_495;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_GenericParameterAttributes.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Reflection_InterfaceMapping.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.Reflection.Emit.MethodBuilder,System.String,System.Int32)
extern "C" void GenericTypeParameterBuilder__ctor_m1_5881 (GenericTypeParameterBuilder_t1_509 * __this, TypeBuilder_t1_481 * ___tbuilder, MethodBuilder_t1_501 * ___mbuilder, String_t* ___name, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::SetBaseTypeConstraint(System.Type)
extern "C" void GenericTypeParameterBuilder_SetBaseTypeConstraint_m1_5882 (GenericTypeParameterBuilder_t1_509 * __this, Type_t * ___baseTypeConstraint, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::SetInterfaceConstraints(System.Type[])
extern "C" void GenericTypeParameterBuilder_SetInterfaceConstraints_m1_5883 (GenericTypeParameterBuilder_t1_509 * __this, TypeU5BU5D_t1_31* ___interfaceConstraints, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::SetGenericParameterAttributes(System.Reflection.GenericParameterAttributes)
extern "C" void GenericTypeParameterBuilder_SetGenericParameterAttributes_m1_5884 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___genericParameterAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::initialize()
extern "C" void GenericTypeParameterBuilder_initialize_m1_5885 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsSubclassOf(System.Type)
extern "C" bool GenericTypeParameterBuilder_IsSubclassOf_m1_5886 (GenericTypeParameterBuilder_t1_509 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.Emit.GenericTypeParameterBuilder::GetAttributeFlagsImpl()
extern "C" int32_t GenericTypeParameterBuilder_GetAttributeFlagsImpl_m1_5887 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" ConstructorInfo_t1_478 * GenericTypeParameterBuilder_GetConstructorImpl_m1_5888 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetConstructors(System.Reflection.BindingFlags)
extern "C" ConstructorInfoU5BU5D_t1_1671* GenericTypeParameterBuilder_GetConstructors_m1_5889 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C" EventInfo_t * GenericTypeParameterBuilder_GetEvent_m1_5890 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetEvents()
extern "C" EventInfoU5BU5D_t1_1667* GenericTypeParameterBuilder_GetEvents_m1_5891 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetEvents(System.Reflection.BindingFlags)
extern "C" EventInfoU5BU5D_t1_1667* GenericTypeParameterBuilder_GetEvents_m1_5892 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetField(System.String,System.Reflection.BindingFlags)
extern "C" FieldInfo_t * GenericTypeParameterBuilder_GetField_m1_5893 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetFields(System.Reflection.BindingFlags)
extern "C" FieldInfoU5BU5D_t1_1668* GenericTypeParameterBuilder_GetFields_m1_5894 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::GetInterface(System.String,System.Boolean)
extern "C" Type_t * GenericTypeParameterBuilder_GetInterface_m1_5895 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::GetInterfaces()
extern "C" TypeU5BU5D_t1_31* GenericTypeParameterBuilder_GetInterfaces_m1_5896 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetMembers(System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* GenericTypeParameterBuilder_GetMembers_m1_5897 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetMember(System.String,System.Reflection.MemberTypes,System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* GenericTypeParameterBuilder_GetMember_m1_5898 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___type, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetMethods(System.Reflection.BindingFlags)
extern "C" MethodInfoU5BU5D_t1_603* GenericTypeParameterBuilder_GetMethods_m1_5899 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * GenericTypeParameterBuilder_GetMethodImpl_m1_5900 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::GetNestedType(System.String,System.Reflection.BindingFlags)
extern "C" Type_t * GenericTypeParameterBuilder_GetNestedType_m1_5901 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::GetNestedTypes(System.Reflection.BindingFlags)
extern "C" TypeU5BU5D_t1_31* GenericTypeParameterBuilder_GetNestedTypes_m1_5902 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Reflection.Emit.GenericTypeParameterBuilder::GetProperties(System.Reflection.BindingFlags)
extern "C" PropertyInfoU5BU5D_t1_1670* GenericTypeParameterBuilder_GetProperties_m1_5903 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.Emit.GenericTypeParameterBuilder::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C" PropertyInfo_t * GenericTypeParameterBuilder_GetPropertyImpl_m1_5904 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, Type_t * ___returnType, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::HasElementTypeImpl()
extern "C" bool GenericTypeParameterBuilder_HasElementTypeImpl_m1_5905 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsAssignableFrom(System.Type)
extern "C" bool GenericTypeParameterBuilder_IsAssignableFrom_m1_5906 (GenericTypeParameterBuilder_t1_509 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsInstanceOfType(System.Object)
extern "C" bool GenericTypeParameterBuilder_IsInstanceOfType_m1_5907 (GenericTypeParameterBuilder_t1_509 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsArrayImpl()
extern "C" bool GenericTypeParameterBuilder_IsArrayImpl_m1_5908 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsByRefImpl()
extern "C" bool GenericTypeParameterBuilder_IsByRefImpl_m1_5909 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsCOMObjectImpl()
extern "C" bool GenericTypeParameterBuilder_IsCOMObjectImpl_m1_5910 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsPointerImpl()
extern "C" bool GenericTypeParameterBuilder_IsPointerImpl_m1_5911 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsPrimitiveImpl()
extern "C" bool GenericTypeParameterBuilder_IsPrimitiveImpl_m1_5912 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsValueTypeImpl()
extern "C" bool GenericTypeParameterBuilder_IsValueTypeImpl_m1_5913 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.GenericTypeParameterBuilder::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C" Object_t * GenericTypeParameterBuilder_InvokeMember_m1_5914 (GenericTypeParameterBuilder_t1_509 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1_585 * ___binder, Object_t * ___target, ObjectU5BU5D_t1_272* ___args, ParameterModifierU5BU5D_t1_1669* ___modifiers, CultureInfo_t1_277 * ___culture, StringU5BU5D_t1_238* ___namedParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::GetElementType()
extern "C" Type_t * GenericTypeParameterBuilder_GetElementType_m1_5915 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_UnderlyingSystemType()
extern "C" Type_t * GenericTypeParameterBuilder_get_UnderlyingSystemType_m1_5916 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Emit.GenericTypeParameterBuilder::get_Assembly()
extern "C" Assembly_t1_467 * GenericTypeParameterBuilder_get_Assembly_m1_5917 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_AssemblyQualifiedName()
extern "C" String_t* GenericTypeParameterBuilder_get_AssemblyQualifiedName_m1_5918 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_BaseType()
extern "C" Type_t * GenericTypeParameterBuilder_get_BaseType_m1_5919 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_FullName()
extern "C" String_t* GenericTypeParameterBuilder_get_FullName_m1_5920 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Emit.GenericTypeParameterBuilder::get_GUID()
extern "C" Guid_t1_319  GenericTypeParameterBuilder_get_GUID_m1_5921 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::IsDefined(System.Type,System.Boolean)
extern "C" bool GenericTypeParameterBuilder_IsDefined_m1_5922 (GenericTypeParameterBuilder_t1_509 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.GenericTypeParameterBuilder::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* GenericTypeParameterBuilder_GetCustomAttributes_m1_5923 (GenericTypeParameterBuilder_t1_509 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.GenericTypeParameterBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* GenericTypeParameterBuilder_GetCustomAttributes_m1_5924 (GenericTypeParameterBuilder_t1_509 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.InterfaceMapping System.Reflection.Emit.GenericTypeParameterBuilder::GetInterfaceMap(System.Type)
extern "C" InterfaceMapping_t1_602  GenericTypeParameterBuilder_GetInterfaceMap_m1_5925 (GenericTypeParameterBuilder_t1_509 * __this, Type_t * ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_Name()
extern "C" String_t* GenericTypeParameterBuilder_get_Name_m1_5926 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::get_Namespace()
extern "C" String_t* GenericTypeParameterBuilder_get_Namespace_m1_5927 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.GenericTypeParameterBuilder::get_Module()
extern "C" Module_t1_495 * GenericTypeParameterBuilder_get_Module_m1_5928 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_DeclaringType()
extern "C" Type_t * GenericTypeParameterBuilder_get_DeclaringType_m1_5929 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::get_ReflectedType()
extern "C" Type_t * GenericTypeParameterBuilder_get_ReflectedType_m1_5930 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.Reflection.Emit.GenericTypeParameterBuilder::get_TypeHandle()
extern "C" RuntimeTypeHandle_t1_30  GenericTypeParameterBuilder_get_TypeHandle_m1_5931 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* GenericTypeParameterBuilder_GetGenericArguments_m1_5932 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::GetGenericTypeDefinition()
extern "C" Type_t * GenericTypeParameterBuilder_GetGenericTypeDefinition_m1_5933 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_ContainsGenericParameters()
extern "C" bool GenericTypeParameterBuilder_get_ContainsGenericParameters_m1_5934 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_IsGenericParameter()
extern "C" bool GenericTypeParameterBuilder_get_IsGenericParameter_m1_5935 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_IsGenericType()
extern "C" bool GenericTypeParameterBuilder_get_IsGenericType_m1_5936 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::get_IsGenericTypeDefinition()
extern "C" bool GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m1_5937 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.GenericParameterAttributes System.Reflection.Emit.GenericTypeParameterBuilder::get_GenericParameterAttributes()
extern "C" int32_t GenericTypeParameterBuilder_get_GenericParameterAttributes_m1_5938 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.GenericTypeParameterBuilder::get_GenericParameterPosition()
extern "C" int32_t GenericTypeParameterBuilder_get_GenericParameterPosition_m1_5939 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.GenericTypeParameterBuilder::GetGenericParameterConstraints()
extern "C" TypeU5BU5D_t1_31* GenericTypeParameterBuilder_GetGenericParameterConstraints_m1_5940 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Emit.GenericTypeParameterBuilder::get_DeclaringMethod()
extern "C" MethodBase_t1_335 * GenericTypeParameterBuilder_get_DeclaringMethod_m1_5941 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void GenericTypeParameterBuilder_SetCustomAttribute_m1_5942 (GenericTypeParameterBuilder_t1_509 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.GenericTypeParameterBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void GenericTypeParameterBuilder_SetCustomAttribute_m1_5943 (GenericTypeParameterBuilder_t1_509 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.GenericTypeParameterBuilder::not_supported()
extern "C" Exception_t1_33 * GenericTypeParameterBuilder_not_supported_m1_5944 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.GenericTypeParameterBuilder::ToString()
extern "C" String_t* GenericTypeParameterBuilder_ToString_m1_5945 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.GenericTypeParameterBuilder::Equals(System.Object)
extern "C" bool GenericTypeParameterBuilder_Equals_m1_5946 (GenericTypeParameterBuilder_t1_509 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.GenericTypeParameterBuilder::GetHashCode()
extern "C" int32_t GenericTypeParameterBuilder_GetHashCode_m1_5947 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::MakeArrayType()
extern "C" Type_t * GenericTypeParameterBuilder_MakeArrayType_m1_5948 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::MakeArrayType(System.Int32)
extern "C" Type_t * GenericTypeParameterBuilder_MakeArrayType_m1_5949 (GenericTypeParameterBuilder_t1_509 * __this, int32_t ___rank, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::MakeByRefType()
extern "C" Type_t * GenericTypeParameterBuilder_MakeByRefType_m1_5950 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::MakeGenericType(System.Type[])
extern "C" Type_t * GenericTypeParameterBuilder_MakeGenericType_m1_5951 (GenericTypeParameterBuilder_t1_509 * __this, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.GenericTypeParameterBuilder::MakePointerType()
extern "C" Type_t * GenericTypeParameterBuilder_MakePointerType_m1_5952 (GenericTypeParameterBuilder_t1_509 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
