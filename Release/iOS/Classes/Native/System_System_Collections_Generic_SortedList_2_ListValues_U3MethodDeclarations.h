﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>
struct GetEnumeratorU3Ec__Iterator3_t3_260;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator3__ctor_m3_1965_gshared (GetEnumeratorU3Ec__Iterator3_t3_260 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3__ctor_m3_1965(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator3_t3_260 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3__ctor_m3_1965_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_1966_gshared (GetEnumeratorU3Ec__Iterator3_t3_260 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_1966(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator3_t3_260 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_1966_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_1967_gshared (GetEnumeratorU3Ec__Iterator3_t3_260 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_1967(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator3_t3_260 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_1967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator3_MoveNext_m3_1968_gshared (GetEnumeratorU3Ec__Iterator3_t3_260 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_MoveNext_m3_1968(__this, method) (( bool (*) (GetEnumeratorU3Ec__Iterator3_t3_260 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_MoveNext_m3_1968_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator3_Dispose_m3_1969_gshared (GetEnumeratorU3Ec__Iterator3_t3_260 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Dispose_m3_1969(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator3_t3_260 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Dispose_m3_1969_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Object,System.Object>::Reset()
extern "C" void GetEnumeratorU3Ec__Iterator3_Reset_m3_1970_gshared (GetEnumeratorU3Ec__Iterator3_t3_260 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator3_Reset_m3_1970(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator3_t3_260 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator3_Reset_m3_1970_gshared)(__this, method)
