﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.FileWebResponse
struct FileWebResponse_t3_95;
// System.Uri
struct Uri_t3_3;
// System.IO.FileStream
struct FileStream_t1_146;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3_80;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Net.FileWebResponse::.ctor(System.Uri,System.IO.FileStream)
extern "C" void FileWebResponse__ctor_m3_564 (FileWebResponse_t3_95 * __this, Uri_t3_3 * ___responseUri, FileStream_t1_146 * ___fileStream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void FileWebResponse__ctor_m3_565 (FileWebResponse_t3_95 * __this, SerializationInfo_t1_293 * ___serializationInfo, StreamingContext_t1_1050  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void FileWebResponse_System_Runtime_Serialization_ISerializable_GetObjectData_m3_566 (FileWebResponse_t3_95 * __this, SerializationInfo_t1_293 * ___serializationInfo, StreamingContext_t1_1050  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::System.IDisposable.Dispose()
extern "C" void FileWebResponse_System_IDisposable_Dispose_m3_567 (FileWebResponse_t3_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.FileWebResponse::get_Headers()
extern "C" WebHeaderCollection_t3_80 * FileWebResponse_get_Headers_m3_568 (FileWebResponse_t3_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void FileWebResponse_GetObjectData_m3_569 (FileWebResponse_t3_95 * __this, SerializationInfo_t1_293 * ___serializationInfo, StreamingContext_t1_1050  ___streamingContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Net.FileWebResponse::GetResponseStream()
extern "C" Stream_t1_405 * FileWebResponse_GetResponseStream_m3_570 (FileWebResponse_t3_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::Finalize()
extern "C" void FileWebResponse_Finalize_m3_571 (FileWebResponse_t3_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::Close()
extern "C" void FileWebResponse_Close_m3_572 (FileWebResponse_t3_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::Dispose(System.Boolean)
extern "C" void FileWebResponse_Dispose_m3_573 (FileWebResponse_t3_95 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebResponse::CheckDisposed()
extern "C" void FileWebResponse_CheckDisposed_m3_574 (FileWebResponse_t3_95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
