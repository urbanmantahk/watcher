﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.User
struct User_t8_235;
// System.String
struct String_t;
// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.NativePlugins.User::.ctor()
extern "C" void User__ctor_m8_1319 (User_t8_235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::.ctor(System.String,System.String)
extern "C" void User__ctor_m8_1320 (User_t8_235 * __this, String_t* ____id, String_t* ____name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::add_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void User_add_DownloadImageFinishedEvent_m8_1321 (User_t8_235 * __this, Completion_t8_161 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::remove_DownloadImageFinishedEvent(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void User_remove_DownloadImageFinishedEvent_m8_1322 (User_t8_235 * __this, Completion_t8_161 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::GetImageAsync(VoxelBusters.Utility.DownloadTexture/Completion)
extern "C" void User_GetImageAsync_m8_1323 (User_t8_235 * __this, Completion_t8_161 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::RequestForImage()
extern "C" void User_RequestForImage_m8_1324 (User_t8_235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.User::ToString()
extern "C" String_t* User_ToString_m8_1325 (User_t8_235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::RequestForImageFinished(System.Collections.IDictionary)
extern "C" void User_RequestForImageFinished_m8_1326 (User_t8_235 * __this, Object_t * ____dataDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::RequestForImageFinished(VoxelBusters.Utility.URL,System.String)
extern "C" void User_RequestForImageFinished_m8_1327 (User_t8_235 * __this, URL_t8_156  ____imagePathURL, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::DownloadImageFinished(UnityEngine.Texture2D,System.String)
extern "C" void User_DownloadImageFinished_m8_1328 (User_t8_235 * __this, Texture2D_t6_33 * ____image, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.User::<RequestForImageFinished>m__B(UnityEngine.Texture2D,System.String)
extern "C" void User_U3CRequestForImageFinishedU3Em__B_m8_1329 (User_t8_235 * __this, Texture2D_t6_33 * ____image, String_t* ____downloadError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
