﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.OnSerializedAttribute
struct OnSerializedAttribute_t1_1088;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.OnSerializedAttribute::.ctor()
extern "C" void OnSerializedAttribute__ctor_m1_9585 (OnSerializedAttribute_t1_1088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
