﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.ObjectAccessRule
struct ObjectAccessRule_t1_1163;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_ObjectAceFlags.h"

// System.Void System.Security.AccessControl.ObjectAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Guid,System.Guid,System.Security.AccessControl.AccessControlType)
extern "C" void ObjectAccessRule__ctor_m1_9970 (ObjectAccessRule_t1_1163 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, Guid_t1_319  ___objectType, Guid_t1_319  ___inheritedObjectType, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Security.AccessControl.ObjectAccessRule::get_InheritedObjectType()
extern "C" Guid_t1_319  ObjectAccessRule_get_InheritedObjectType_m1_9971 (ObjectAccessRule_t1_1163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.ObjectAceFlags System.Security.AccessControl.ObjectAccessRule::get_ObjectFlags()
extern "C" int32_t ObjectAccessRule_get_ObjectFlags_m1_9972 (ObjectAccessRule_t1_1163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Security.AccessControl.ObjectAccessRule::get_ObjectType()
extern "C" Guid_t1_319  ObjectAccessRule_get_ObjectType_m1_9973 (ObjectAccessRule_t1_1163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
