﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.Win32RegistryApi
struct Win32RegistryApi_t1_102;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Microsoft.Win32.RegistryKey
struct RegistryKey_t1_91;
// System.Object
struct Object_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_Microsoft_Win32_RegistryValueKind.h"
#include "mscorlib_Microsoft_Win32_RegistryValueOptions.h"
#include "mscorlib_Microsoft_Win32_RegistryHive.h"

// System.Void Microsoft.Win32.Win32RegistryApi::.ctor()
extern "C" void Win32RegistryApi__ctor_m1_1527 (Win32RegistryApi_t1_102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegCreateKey(System.IntPtr,System.String,System.IntPtr&)
extern "C" int32_t Win32RegistryApi_RegCreateKey_m1_1528 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___keyName, IntPtr_t* ___keyHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegCloseKey(System.IntPtr)
extern "C" int32_t Win32RegistryApi_RegCloseKey_m1_1529 (Object_t * __this /* static, unused */, IntPtr_t ___keyHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegConnectRegistry(System.String,System.IntPtr,System.IntPtr&)
extern "C" int32_t Win32RegistryApi_RegConnectRegistry_m1_1530 (Object_t * __this /* static, unused */, String_t* ___machineName, IntPtr_t ___hKey, IntPtr_t* ___keyHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegFlushKey(System.IntPtr)
extern "C" int32_t Win32RegistryApi_RegFlushKey_m1_1531 (Object_t * __this /* static, unused */, IntPtr_t ___keyHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegOpenKeyEx(System.IntPtr,System.String,System.IntPtr,System.Int32,System.IntPtr&)
extern "C" int32_t Win32RegistryApi_RegOpenKeyEx_m1_1532 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___keyName, IntPtr_t ___reserved, int32_t ___access, IntPtr_t* ___keyHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegDeleteKey(System.IntPtr,System.String)
extern "C" int32_t Win32RegistryApi_RegDeleteKey_m1_1533 (Object_t * __this /* static, unused */, IntPtr_t ___keyHandle, String_t* ___valueName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegDeleteValue(System.IntPtr,System.String)
extern "C" int32_t Win32RegistryApi_RegDeleteValue_m1_1534 (Object_t * __this /* static, unused */, IntPtr_t ___keyHandle, String_t* ___valueName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegEnumKey(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t Win32RegistryApi_RegEnumKey_m1_1535 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, int32_t ___index, StringBuilder_t1_247 * ___nameBuffer, int32_t ___bufferLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegEnumValue(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32&,System.IntPtr,Microsoft.Win32.RegistryValueKind&,System.IntPtr,System.IntPtr)
extern "C" int32_t Win32RegistryApi_RegEnumValue_m1_1536 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, int32_t ___index, StringBuilder_t1_247 * ___nameBuffer, int32_t* ___nameLength, IntPtr_t ___reserved, int32_t* ___type, IntPtr_t ___data, IntPtr_t ___dataLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegSetValueEx(System.IntPtr,System.String,System.IntPtr,Microsoft.Win32.RegistryValueKind,System.String,System.Int32)
extern "C" int32_t Win32RegistryApi_RegSetValueEx_m1_1537 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___valueName, IntPtr_t ___reserved, int32_t ___type, String_t* ___data, int32_t ___rawDataLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegSetValueEx(System.IntPtr,System.String,System.IntPtr,Microsoft.Win32.RegistryValueKind,System.Byte[],System.Int32)
extern "C" int32_t Win32RegistryApi_RegSetValueEx_m1_1538 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___valueName, IntPtr_t ___reserved, int32_t ___type, ByteU5BU5D_t1_109* ___rawData, int32_t ___rawDataLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegSetValueEx(System.IntPtr,System.String,System.IntPtr,Microsoft.Win32.RegistryValueKind,System.Int32&,System.Int32)
extern "C" int32_t Win32RegistryApi_RegSetValueEx_m1_1539 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___valueName, IntPtr_t ___reserved, int32_t ___type, int32_t* ___data, int32_t ___rawDataLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegQueryValueEx(System.IntPtr,System.String,System.IntPtr,Microsoft.Win32.RegistryValueKind&,System.IntPtr,System.Int32&)
extern "C" int32_t Win32RegistryApi_RegQueryValueEx_m1_1540 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___valueName, IntPtr_t ___reserved, int32_t* ___type, IntPtr_t ___zero, int32_t* ___dataSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegQueryValueEx(System.IntPtr,System.String,System.IntPtr,Microsoft.Win32.RegistryValueKind&,System.Byte[],System.Int32&)
extern "C" int32_t Win32RegistryApi_RegQueryValueEx_m1_1541 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___valueName, IntPtr_t ___reserved, int32_t* ___type, ByteU5BU5D_t1_109* ___data, int32_t* ___dataSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::RegQueryValueEx(System.IntPtr,System.String,System.IntPtr,Microsoft.Win32.RegistryValueKind&,System.Int32&,System.Int32&)
extern "C" int32_t Win32RegistryApi_RegQueryValueEx_m1_1542 (Object_t * __this /* static, unused */, IntPtr_t ___keyBase, String_t* ___valueName, IntPtr_t ___reserved, int32_t* ___type, int32_t* ___data, int32_t* ___dataSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Microsoft.Win32.Win32RegistryApi::GetHandle(Microsoft.Win32.RegistryKey)
extern "C" IntPtr_t Win32RegistryApi_GetHandle_m1_1543 (Object_t * __this /* static, unused */, RegistryKey_t1_91 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.Win32RegistryApi::IsHandleValid(Microsoft.Win32.RegistryKey)
extern "C" bool Win32RegistryApi_IsHandleValid_m1_1544 (Object_t * __this /* static, unused */, RegistryKey_t1_91 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Microsoft.Win32.Win32RegistryApi::GetValue(Microsoft.Win32.RegistryKey,System.String,System.Object,Microsoft.Win32.RegistryValueOptions)
extern "C" Object_t * Win32RegistryApi_GetValue_m1_1545 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, Object_t * ___defaultValue, int32_t ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::SetValue(Microsoft.Win32.RegistryKey,System.String,System.Object,Microsoft.Win32.RegistryValueKind)
extern "C" void Win32RegistryApi_SetValue_m1_1546 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, Object_t * ___value, int32_t ___valueKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::SetValue(Microsoft.Win32.RegistryKey,System.String,System.Object)
extern "C" void Win32RegistryApi_SetValue_m1_1547 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::GetBinaryValue(Microsoft.Win32.RegistryKey,System.String,Microsoft.Win32.RegistryValueKind,System.Byte[]&,System.Int32)
extern "C" int32_t Win32RegistryApi_GetBinaryValue_m1_1548 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___name, int32_t ___type, ByteU5BU5D_t1_109** ___data, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::SubKeyCount(Microsoft.Win32.RegistryKey)
extern "C" int32_t Win32RegistryApi_SubKeyCount_m1_1549 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Microsoft.Win32.Win32RegistryApi::ValueCount(Microsoft.Win32.RegistryKey)
extern "C" int32_t Win32RegistryApi_ValueCount_m1_1550 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.Win32RegistryApi::OpenRemoteBaseKey(Microsoft.Win32.RegistryHive,System.String)
extern "C" RegistryKey_t1_91 * Win32RegistryApi_OpenRemoteBaseKey_m1_1551 (Win32RegistryApi_t1_102 * __this, int32_t ___hKey, String_t* ___machineName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.Win32RegistryApi::OpenSubKey(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" RegistryKey_t1_91 * Win32RegistryApi_OpenSubKey_m1_1552 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyName, bool ___writable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::Flush(Microsoft.Win32.RegistryKey)
extern "C" void Win32RegistryApi_Flush_m1_1553 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::Close(Microsoft.Win32.RegistryKey)
extern "C" void Win32RegistryApi_Close_m1_1554 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Microsoft.Win32.RegistryKey Microsoft.Win32.Win32RegistryApi::CreateSubKey(Microsoft.Win32.RegistryKey,System.String)
extern "C" RegistryKey_t1_91 * Win32RegistryApi_CreateSubKey_m1_1555 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::DeleteKey(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" void Win32RegistryApi_DeleteKey_m1_1556 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___keyName, bool ___shouldThrowWhenKeyMissing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::DeleteValue(Microsoft.Win32.RegistryKey,System.String,System.Boolean)
extern "C" void Win32RegistryApi_DeleteValue_m1_1557 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, String_t* ___value, bool ___shouldThrowWhenKeyMissing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Microsoft.Win32.Win32RegistryApi::GetSubKeyNames(Microsoft.Win32.RegistryKey)
extern "C" StringU5BU5D_t1_238* Win32RegistryApi_GetSubKeyNames_m1_1558 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Microsoft.Win32.Win32RegistryApi::GetValueNames(Microsoft.Win32.RegistryKey)
extern "C" StringU5BU5D_t1_238* Win32RegistryApi_GetValueNames_m1_1559 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Microsoft.Win32.Win32RegistryApi::GenerateException(System.Int32)
extern "C" void Win32RegistryApi_GenerateException_m1_1560 (Win32RegistryApi_t1_102 * __this, int32_t ___errorCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.Win32RegistryApi::ToString(Microsoft.Win32.RegistryKey)
extern "C" String_t* Win32RegistryApi_ToString_m1_1561 (Win32RegistryApi_t1_102 * __this, RegistryKey_t1_91 * ___rkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Microsoft.Win32.Win32RegistryApi::CombineName(Microsoft.Win32.RegistryKey,System.String)
extern "C" String_t* Win32RegistryApi_CombineName_m1_1562 (Object_t * __this /* static, unused */, RegistryKey_t1_91 * ___rkey, String_t* ___localName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
