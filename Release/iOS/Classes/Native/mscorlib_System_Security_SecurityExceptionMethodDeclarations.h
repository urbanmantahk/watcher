﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityException
struct SecurityException_t1_1399;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;
// System.Type
struct Type_t;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;
// System.Security.Policy.Evidence
struct Evidence_t1_398;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Void System.Security.SecurityException::.ctor()
extern "C" void SecurityException__ctor_m1_12062 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String)
extern "C" void SecurityException__ctor_m1_12063 (SecurityException_t1_1399 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SecurityException__ctor_m1_12064 (SecurityException_t1_1399 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Exception)
extern "C" void SecurityException__ctor_m1_12065 (SecurityException_t1_1399 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Type)
extern "C" void SecurityException__ctor_m1_12066 (SecurityException_t1_1399 * __this, String_t* ___message, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Type,System.String)
extern "C" void SecurityException__ctor_m1_12067 (SecurityException_t1_1399 * __this, String_t* ___message, Type_t * ___type, String_t* ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" void SecurityException__ctor_m1_12068 (SecurityException_t1_1399 * __this, String_t* ___message, PermissionSet_t1_563 * ___granted, PermissionSet_t1_563 * ___refused, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Object,System.Object,System.Reflection.MethodInfo,System.Object,System.Security.IPermission)
extern "C" void SecurityException__ctor_m1_12069 (SecurityException_t1_1399 * __this, String_t* ___message, Object_t * ___deny, Object_t * ___permitOnly, MethodInfo_t * ___method, Object_t * ___demanded, Object_t * ___permThatFailed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::.ctor(System.String,System.Reflection.AssemblyName,System.Security.PermissionSet,System.Security.PermissionSet,System.Reflection.MethodInfo,System.Security.Permissions.SecurityAction,System.Object,System.Security.IPermission,System.Security.Policy.Evidence)
extern "C" void SecurityException__ctor_m1_12070 (SecurityException_t1_1399 * __this, String_t* ___message, AssemblyName_t1_576 * ___assemblyName, PermissionSet_t1_563 * ___grant, PermissionSet_t1_563 * ___refused, MethodInfo_t * ___method, int32_t ___action, Object_t * ___demanded, Object_t * ___permThatFailed, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.SecurityAction System.Security.SecurityException::get_Action()
extern "C" int32_t SecurityException_get_Action_m1_12071 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Action(System.Security.Permissions.SecurityAction)
extern "C" void SecurityException_set_Action_m1_12072 (SecurityException_t1_1399 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.SecurityException::get_DenySetInstance()
extern "C" Object_t * SecurityException_get_DenySetInstance_m1_12073 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_DenySetInstance(System.Object)
extern "C" void SecurityException_set_DenySetInstance_m1_12074 (SecurityException_t1_1399 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Security.SecurityException::get_FailedAssemblyInfo()
extern "C" AssemblyName_t1_576 * SecurityException_get_FailedAssemblyInfo_m1_12075 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_FailedAssemblyInfo(System.Reflection.AssemblyName)
extern "C" void SecurityException_set_FailedAssemblyInfo_m1_12076 (SecurityException_t1_1399 * __this, AssemblyName_t1_576 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Security.SecurityException::get_Method()
extern "C" MethodInfo_t * SecurityException_get_Method_m1_12077 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Method(System.Reflection.MethodInfo)
extern "C" void SecurityException_set_Method_m1_12078 (SecurityException_t1_1399 * __this, MethodInfo_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.SecurityException::get_PermitOnlySetInstance()
extern "C" Object_t * SecurityException_get_PermitOnlySetInstance_m1_12079 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_PermitOnlySetInstance(System.Object)
extern "C" void SecurityException_set_PermitOnlySetInstance_m1_12080 (SecurityException_t1_1399 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_Url()
extern "C" String_t* SecurityException_get_Url_m1_12081 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Url(System.String)
extern "C" void SecurityException_set_Url_m1_12082 (SecurityException_t1_1399 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityZone System.Security.SecurityException::get_Zone()
extern "C" int32_t SecurityException_get_Zone_m1_12083 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Zone(System.Security.SecurityZone)
extern "C" void SecurityException_set_Zone_m1_12084 (SecurityException_t1_1399 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.SecurityException::get_Demanded()
extern "C" Object_t * SecurityException_get_Demanded_m1_12085 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_Demanded(System.Object)
extern "C" void SecurityException_set_Demanded_m1_12086 (SecurityException_t1_1399 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityException::get_FirstPermissionThatFailed()
extern "C" Object_t * SecurityException_get_FirstPermissionThatFailed_m1_12087 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_FirstPermissionThatFailed(System.Security.IPermission)
extern "C" void SecurityException_set_FirstPermissionThatFailed_m1_12088 (SecurityException_t1_1399 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_PermissionState()
extern "C" String_t* SecurityException_get_PermissionState_m1_12089 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_PermissionState(System.String)
extern "C" void SecurityException_set_PermissionState_m1_12090 (SecurityException_t1_1399 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.SecurityException::get_PermissionType()
extern "C" Type_t * SecurityException_get_PermissionType_m1_12091 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_PermissionType(System.Type)
extern "C" void SecurityException_set_PermissionType_m1_12092 (SecurityException_t1_1399 * __this, Type_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_GrantedSet()
extern "C" String_t* SecurityException_get_GrantedSet_m1_12093 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_GrantedSet(System.String)
extern "C" void SecurityException_set_GrantedSet_m1_12094 (SecurityException_t1_1399 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::get_RefusedSet()
extern "C" String_t* SecurityException_get_RefusedSet_m1_12095 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::set_RefusedSet(System.String)
extern "C" void SecurityException_set_RefusedSet_m1_12096 (SecurityException_t1_1399 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SecurityException_GetObjectData_m1_12097 (SecurityException_t1_1399 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.SecurityException::ToString()
extern "C" String_t* SecurityException_ToString_m1_12098 (SecurityException_t1_1399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
