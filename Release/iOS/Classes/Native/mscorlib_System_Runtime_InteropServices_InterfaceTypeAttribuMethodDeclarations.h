﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.InterfaceTypeAttribute
struct InterfaceTypeAttribute_t1_805;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"

// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Runtime.InteropServices.ComInterfaceType)
extern "C" void InterfaceTypeAttribute__ctor_m1_7682 (InterfaceTypeAttribute_t1_805 * __this, int32_t ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.InterfaceTypeAttribute::.ctor(System.Int16)
extern "C" void InterfaceTypeAttribute__ctor_m1_7683 (InterfaceTypeAttribute_t1_805 * __this, int16_t ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.ComInterfaceType System.Runtime.InteropServices.InterfaceTypeAttribute::get_Value()
extern "C" int32_t InterfaceTypeAttribute_get_Value_m1_7684 (InterfaceTypeAttribute_t1_805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
