﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.Formatters.ServerFault
struct ServerFault_t1_1070;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.Formatters.ServerFault::.ctor(System.String,System.String,System.String)
extern "C" void ServerFault__ctor_m1_9470 (ServerFault_t1_1070 * __this, String_t* ___exceptionType, String_t* ___message, String_t* ___stackTrace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.ServerFault::get_ExceptionType()
extern "C" String_t* ServerFault_get_ExceptionType_m1_9471 (ServerFault_t1_1070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.ServerFault::set_ExceptionType(System.String)
extern "C" void ServerFault_set_ExceptionType_m1_9472 (ServerFault_t1_1070 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.ServerFault::get_ExceptionMessage()
extern "C" String_t* ServerFault_get_ExceptionMessage_m1_9473 (ServerFault_t1_1070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.ServerFault::set_ExceptionMessage(System.String)
extern "C" void ServerFault_set_ExceptionMessage_m1_9474 (ServerFault_t1_1070 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.Formatters.ServerFault::get_StackTrace()
extern "C" String_t* ServerFault_get_StackTrace_m1_9475 (ServerFault_t1_1070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.Formatters.ServerFault::set_StackTrace(System.String)
extern "C" void ServerFault_set_StackTrace_m1_9476 (ServerFault_t1_1070 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
