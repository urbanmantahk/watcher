﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.String
struct String_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Policy_PolicyStatementAttribute.h"

// System.Void System.Security.Policy.PolicyStatement::.ctor(System.Security.PermissionSet)
extern "C" void PolicyStatement__ctor_m1_11619 (PolicyStatement_t1_1334 * __this, PermissionSet_t1_563 * ___permSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyStatement::.ctor(System.Security.PermissionSet,System.Security.Policy.PolicyStatementAttribute)
extern "C" void PolicyStatement__ctor_m1_11620 (PolicyStatement_t1_1334 * __this, PermissionSet_t1_563 * ___permSet, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Policy.PolicyStatement::get_PermissionSet()
extern "C" PermissionSet_t1_563 * PolicyStatement_get_PermissionSet_m1_11621 (PolicyStatement_t1_1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyStatement::set_PermissionSet(System.Security.PermissionSet)
extern "C" void PolicyStatement_set_PermissionSet_m1_11622 (PolicyStatement_t1_1334 * __this, PermissionSet_t1_563 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatementAttribute System.Security.Policy.PolicyStatement::get_Attributes()
extern "C" int32_t PolicyStatement_get_Attributes_m1_11623 (PolicyStatement_t1_1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyStatement::set_Attributes(System.Security.Policy.PolicyStatementAttribute)
extern "C" void PolicyStatement_set_Attributes_m1_11624 (PolicyStatement_t1_1334 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.PolicyStatement::get_AttributeString()
extern "C" String_t* PolicyStatement_get_AttributeString_m1_11625 (PolicyStatement_t1_1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.PolicyStatement::Copy()
extern "C" PolicyStatement_t1_1334 * PolicyStatement_Copy_m1_11626 (PolicyStatement_t1_1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyStatement::FromXml(System.Security.SecurityElement)
extern "C" void PolicyStatement_FromXml_m1_11627 (PolicyStatement_t1_1334 * __this, SecurityElement_t1_242 * ___et, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PolicyStatement::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void PolicyStatement_FromXml_m1_11628 (PolicyStatement_t1_1334 * __this, SecurityElement_t1_242 * ___et, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.PolicyStatement::ToXml()
extern "C" SecurityElement_t1_242 * PolicyStatement_ToXml_m1_11629 (PolicyStatement_t1_1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.PolicyStatement::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * PolicyStatement_ToXml_m1_11630 (PolicyStatement_t1_1334 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.PolicyStatement::Equals(System.Object)
extern "C" bool PolicyStatement_Equals_m1_11631 (PolicyStatement_t1_1334 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.PolicyStatement::GetHashCode()
extern "C" int32_t PolicyStatement_GetHashCode_m1_11632 (PolicyStatement_t1_1334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.PolicyStatement::Empty()
extern "C" PolicyStatement_t1_1334 * PolicyStatement_Empty_m1_11633 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
