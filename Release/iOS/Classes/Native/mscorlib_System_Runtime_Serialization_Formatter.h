﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Serialization.ObjectIDGenerator
struct ObjectIDGenerator_t1_1064;
// System.Collections.Queue
struct Queue_t1_298;

#include "mscorlib_System_Object.h"

// System.Runtime.Serialization.Formatter
struct  Formatter_t1_1074  : public Object_t
{
	// System.Runtime.Serialization.ObjectIDGenerator System.Runtime.Serialization.Formatter::m_idGenerator
	ObjectIDGenerator_t1_1064 * ___m_idGenerator_0;
	// System.Collections.Queue System.Runtime.Serialization.Formatter::m_objectQueue
	Queue_t1_298 * ___m_objectQueue_1;
};
