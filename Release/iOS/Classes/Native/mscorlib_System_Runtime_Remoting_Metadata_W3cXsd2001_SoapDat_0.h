﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime
struct  SoapDateTime_t1_968  : public Object_t
{
};
struct SoapDateTime_t1_968_StaticFields{
	// System.String[] System.Runtime.Remoting.Metadata.W3cXsd2001.SoapDateTime::_datetimeFormats
	StringU5BU5D_t1_238* ____datetimeFormats_0;
};
