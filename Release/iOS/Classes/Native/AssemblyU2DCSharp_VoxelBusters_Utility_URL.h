﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// VoxelBusters.Utility.URL
struct  URL_t8_156 
{
	// System.String VoxelBusters.Utility.URL::<URLString>k__BackingField
	String_t* ___U3CURLStringU3Ek__BackingField_3;
};
// Native definition for marshalling of: VoxelBusters.Utility.URL
struct URL_t8_156_marshaled
{
	char* ___U3CURLStringU3Ek__BackingField_3;
};
