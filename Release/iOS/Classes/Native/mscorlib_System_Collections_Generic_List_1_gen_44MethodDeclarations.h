﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::.ctor()
#define List_1__ctor_m1_15033(__this, method) (( void (*) (List_1_t1_1901 *, const MethodInfo*))List_1__ctor_m1_15034_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1_25918(__this, ___collection, method) (( void (*) (List_1_t1_1901 *, Object_t*, const MethodInfo*))List_1__ctor_m1_15095_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::.ctor(System.Int32)
#define List_1__ctor_m1_25919(__this, ___capacity, method) (( void (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1__ctor_m1_15097_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::.ctor(T[],System.Int32)
#define List_1__ctor_m1_25920(__this, ___data, ___size, method) (( void (*) (List_1_t1_1901 *, ShaderInfoU5BU5D_t8_376*, int32_t, const MethodInfo*))List_1__ctor_m1_15099_gshared)(__this, ___data, ___size, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::.cctor()
#define List_1__cctor_m1_25921(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m1_15101_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_25922(__this, method) (( Object_t* (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1_15103_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1_25923(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1901 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1_15105_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1_25924(__this, method) (( Object_t * (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1_15107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m1_25925(__this, ___item, method) (( int32_t (*) (List_1_t1_1901 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m1_15109_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1_25926(__this, ___item, method) (( bool (*) (List_1_t1_1901 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1_15111_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1_25927(__this, ___item, method) (( int32_t (*) (List_1_t1_1901 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1_15113_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1_25928(__this, ___index, ___item, method) (( void (*) (List_1_t1_1901 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1_15115_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1_25929(__this, ___item, method) (( void (*) (List_1_t1_1901 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1_15117_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_25930(__this, method) (( bool (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15119_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1_25931(__this, method) (( bool (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1_15121_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1_25932(__this, method) (( Object_t * (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1_15123_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1_25933(__this, method) (( bool (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1_15125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1_25934(__this, method) (( bool (*) (List_1_t1_1901 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1_15127_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1_25935(__this, ___index, method) (( Object_t * (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1_15129_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1_25936(__this, ___index, ___value, method) (( void (*) (List_1_t1_1901 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1_15131_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Add(T)
#define List_1_Add_m1_25937(__this, ___item, method) (( void (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, const MethodInfo*))List_1_Add_m1_15133_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1_25938(__this, ___newCount, method) (( void (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1_15135_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m1_25939(__this, ___idx, ___count, method) (( void (*) (List_1_t1_1901 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1_15137_gshared)(__this, ___idx, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1_25940(__this, ___collection, method) (( void (*) (List_1_t1_1901 *, Object_t*, const MethodInfo*))List_1_AddCollection_m1_15139_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1_25941(__this, ___enumerable, method) (( void (*) (List_1_t1_1901 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m1_15141_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m1_25942(__this, ___collection, method) (( void (*) (List_1_t1_1901 *, Object_t*, const MethodInfo*))List_1_AddRange_m1_15143_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::AsReadOnly()
#define List_1_AsReadOnly_m1_25943(__this, method) (( ReadOnlyCollection_1_t1_2639 * (*) (List_1_t1_1901 *, const MethodInfo*))List_1_AsReadOnly_m1_15145_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::BinarySearch(T)
#define List_1_BinarySearch_m1_25944(__this, ___item, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, const MethodInfo*))List_1_BinarySearch_m1_15147_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::BinarySearch(T,System.Collections.Generic.IComparer`1<T>)
#define List_1_BinarySearch_m1_25945(__this, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15149_gshared)(__this, ___item, ___comparer, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::BinarySearch(System.Int32,System.Int32,T,System.Collections.Generic.IComparer`1<T>)
#define List_1_BinarySearch_m1_25946(__this, ___index, ___count, ___item, ___comparer, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, int32_t, ShaderInfo_t8_26 *, Object_t*, const MethodInfo*))List_1_BinarySearch_m1_15151_gshared)(__this, ___index, ___count, ___item, ___comparer, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Clear()
#define List_1_Clear_m1_25947(__this, method) (( void (*) (List_1_t1_1901 *, const MethodInfo*))List_1_Clear_m1_15153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Contains(T)
#define List_1_Contains_m1_25948(__this, ___item, method) (( bool (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, const MethodInfo*))List_1_Contains_m1_15155_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CopyTo(T[])
#define List_1_CopyTo_m1_25949(__this, ___array, method) (( void (*) (List_1_t1_1901 *, ShaderInfoU5BU5D_t8_376*, const MethodInfo*))List_1_CopyTo_m1_15157_gshared)(__this, ___array, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1_25950(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1_1901 *, ShaderInfoU5BU5D_t8_376*, int32_t, const MethodInfo*))List_1_CopyTo_m1_15159_gshared)(__this, ___array, ___arrayIndex, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CopyTo(System.Int32,T[],System.Int32,System.Int32)
#define List_1_CopyTo_m1_25951(__this, ___index, ___array, ___arrayIndex, ___count, method) (( void (*) (List_1_t1_1901 *, int32_t, ShaderInfoU5BU5D_t8_376*, int32_t, int32_t, const MethodInfo*))List_1_CopyTo_m1_15161_gshared)(__this, ___index, ___array, ___arrayIndex, ___count, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Exists(System.Predicate`1<T>)
#define List_1_Exists_m1_25952(__this, ___match, method) (( bool (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_Exists_m1_15163_gshared)(__this, ___match, method)
// T System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m1_25953(__this, ___match, method) (( ShaderInfo_t8_26 * (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_Find_m1_15165_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1_25954(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1_2641 *, const MethodInfo*))List_1_CheckMatch_m1_15167_gshared)(__this /* static, unused */, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindAll(System.Predicate`1<T>)
#define List_1_FindAll_m1_25955(__this, ___match, method) (( List_1_t1_1901 * (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindAll_m1_15169_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindAllStackBits(System.Predicate`1<T>)
#define List_1_FindAllStackBits_m1_25956(__this, ___match, method) (( List_1_t1_1901 * (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindAllStackBits_m1_15171_gshared)(__this, ___match, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindAllList(System.Predicate`1<T>)
#define List_1_FindAllList_m1_25957(__this, ___match, method) (( List_1_t1_1901 * (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindAllList_m1_15173_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindIndex(System.Predicate`1<T>)
#define List_1_FindIndex_m1_25958(__this, ___match, method) (( int32_t (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindIndex_m1_15175_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindIndex(System.Int32,System.Predicate`1<T>)
#define List_1_FindIndex_m1_25959(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindIndex_m1_15177_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_FindIndex_m1_25960(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, int32_t, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindIndex_m1_15179_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1_25961(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, int32_t, Predicate_1_t1_2641 *, const MethodInfo*))List_1_GetIndex_m1_15181_gshared)(__this, ___startIndex, ___count, ___match, method)
// T System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindLast(System.Predicate`1<T>)
#define List_1_FindLast_m1_25962(__this, ___match, method) (( ShaderInfo_t8_26 * (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindLast_m1_15183_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindLastIndex(System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_25963(__this, ___match, method) (( int32_t (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindLastIndex_m1_15185_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindLastIndex(System.Int32,System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_25964(__this, ___startIndex, ___match, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindLastIndex_m1_15187_gshared)(__this, ___startIndex, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::FindLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_FindLastIndex_m1_25965(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, int32_t, Predicate_1_t1_2641 *, const MethodInfo*))List_1_FindLastIndex_m1_15189_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::GetLastIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetLastIndex_m1_25966(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1_1901 *, int32_t, int32_t, Predicate_1_t1_2641 *, const MethodInfo*))List_1_GetLastIndex_m1_15191_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::ForEach(System.Action`1<T>)
#define List_1_ForEach_m1_25967(__this, ___action, method) (( void (*) (List_1_t1_1901 *, Action_1_t1_2642 *, const MethodInfo*))List_1_ForEach_m1_15193_gshared)(__this, ___action, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::GetEnumerator()
#define List_1_GetEnumerator_m1_25968(__this, method) (( Enumerator_t1_2643  (*) (List_1_t1_1901 *, const MethodInfo*))List_1_GetEnumerator_m1_15195_gshared)(__this, method)
// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::GetRange(System.Int32,System.Int32)
#define List_1_GetRange_m1_25969(__this, ___index, ___count, method) (( List_1_t1_1901 * (*) (List_1_t1_1901 *, int32_t, int32_t, const MethodInfo*))List_1_GetRange_m1_15197_gshared)(__this, ___index, ___count, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::IndexOf(T)
#define List_1_IndexOf_m1_25970(__this, ___item, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, const MethodInfo*))List_1_IndexOf_m1_15199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::IndexOf(T,System.Int32)
#define List_1_IndexOf_m1_25971(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, int32_t, const MethodInfo*))List_1_IndexOf_m1_15201_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::IndexOf(T,System.Int32,System.Int32)
#define List_1_IndexOf_m1_25972(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, int32_t, int32_t, const MethodInfo*))List_1_IndexOf_m1_15203_gshared)(__this, ___item, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1_25973(__this, ___start, ___delta, method) (( void (*) (List_1_t1_1901 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1_15205_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1_25974(__this, ___index, method) (( void (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1_15207_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Insert(System.Int32,T)
#define List_1_Insert_m1_25975(__this, ___index, ___item, method) (( void (*) (List_1_t1_1901 *, int32_t, ShaderInfo_t8_26 *, const MethodInfo*))List_1_Insert_m1_15209_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1_25976(__this, ___collection, method) (( void (*) (List_1_t1_1901 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m1_15211_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
#define List_1_InsertRange_m1_25977(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1901 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertRange_m1_15213_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::InsertCollection(System.Int32,System.Collections.Generic.ICollection`1<T>)
#define List_1_InsertCollection_m1_25978(__this, ___index, ___collection, method) (( void (*) (List_1_t1_1901 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertCollection_m1_15215_gshared)(__this, ___index, ___collection, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::InsertEnumeration(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
#define List_1_InsertEnumeration_m1_25979(__this, ___index, ___enumerable, method) (( void (*) (List_1_t1_1901 *, int32_t, Object_t*, const MethodInfo*))List_1_InsertEnumeration_m1_15217_gshared)(__this, ___index, ___enumerable, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::LastIndexOf(T)
#define List_1_LastIndexOf_m1_25980(__this, ___item, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, const MethodInfo*))List_1_LastIndexOf_m1_15219_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::LastIndexOf(T,System.Int32)
#define List_1_LastIndexOf_m1_25981(__this, ___item, ___index, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15221_gshared)(__this, ___item, ___index, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::LastIndexOf(T,System.Int32,System.Int32)
#define List_1_LastIndexOf_m1_25982(__this, ___item, ___index, ___count, method) (( int32_t (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, int32_t, int32_t, const MethodInfo*))List_1_LastIndexOf_m1_15223_gshared)(__this, ___item, ___index, ___count, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Remove(T)
#define List_1_Remove_m1_25983(__this, ___item, method) (( bool (*) (List_1_t1_1901 *, ShaderInfo_t8_26 *, const MethodInfo*))List_1_Remove_m1_15225_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1_25984(__this, ___match, method) (( int32_t (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_RemoveAll_m1_15227_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1_25985(__this, ___index, method) (( void (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1_15229_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1_25986(__this, ___index, ___count, method) (( void (*) (List_1_t1_1901 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1_15231_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Reverse()
#define List_1_Reverse_m1_25987(__this, method) (( void (*) (List_1_t1_1901 *, const MethodInfo*))List_1_Reverse_m1_15233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Reverse(System.Int32,System.Int32)
#define List_1_Reverse_m1_25988(__this, ___index, ___count, method) (( void (*) (List_1_t1_1901 *, int32_t, int32_t, const MethodInfo*))List_1_Reverse_m1_15235_gshared)(__this, ___index, ___count, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Sort()
#define List_1_Sort_m1_25989(__this, method) (( void (*) (List_1_t1_1901 *, const MethodInfo*))List_1_Sort_m1_15237_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1_25990(__this, ___comparer, method) (( void (*) (List_1_t1_1901 *, Object_t*, const MethodInfo*))List_1_Sort_m1_15239_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1_25991(__this, ___comparison, method) (( void (*) (List_1_t1_1901 *, Comparison_1_t1_2644 *, const MethodInfo*))List_1_Sort_m1_15241_gshared)(__this, ___comparison, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::Sort(System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1_25992(__this, ___index, ___count, ___comparer, method) (( void (*) (List_1_t1_1901 *, int32_t, int32_t, Object_t*, const MethodInfo*))List_1_Sort_m1_15243_gshared)(__this, ___index, ___count, ___comparer, method)
// T[] System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::ToArray()
#define List_1_ToArray_m1_25993(__this, method) (( ShaderInfoU5BU5D_t8_376* (*) (List_1_t1_1901 *, const MethodInfo*))List_1_ToArray_m1_15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::TrimExcess()
#define List_1_TrimExcess_m1_25994(__this, method) (( void (*) (List_1_t1_1901 *, const MethodInfo*))List_1_TrimExcess_m1_15245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::TrueForAll(System.Predicate`1<T>)
#define List_1_TrueForAll_m1_25995(__this, ___match, method) (( bool (*) (List_1_t1_1901 *, Predicate_1_t1_2641 *, const MethodInfo*))List_1_TrueForAll_m1_15247_gshared)(__this, ___match, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Capacity()
#define List_1_get_Capacity_m1_25996(__this, method) (( int32_t (*) (List_1_t1_1901 *, const MethodInfo*))List_1_get_Capacity_m1_15249_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1_25997(__this, ___value, method) (( void (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1_15251_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Count()
#define List_1_get_Count_m1_25998(__this, method) (( int32_t (*) (List_1_t1_1901 *, const MethodInfo*))List_1_get_Count_m1_15253_gshared)(__this, method)
// T System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::get_Item(System.Int32)
#define List_1_get_Item_m1_25999(__this, ___index, method) (( ShaderInfo_t8_26 * (*) (List_1_t1_1901 *, int32_t, const MethodInfo*))List_1_get_Item_m1_15255_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m1_26000(__this, ___index, ___value, method) (( void (*) (List_1_t1_1901 *, int32_t, ShaderInfo_t8_26 *, const MethodInfo*))List_1_set_Item_m1_15257_gshared)(__this, ___index, ___value, method)
