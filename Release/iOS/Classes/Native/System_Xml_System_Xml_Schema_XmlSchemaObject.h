﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t4_65;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Guid.h"

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_t4_54  : public Object_t
{
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_t4_65 * ___namespaces_0;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaObject::unhandledAttributeList
	ArrayList_t1_170 * ___unhandledAttributeList_1;
	// System.Guid System.Xml.Schema.XmlSchemaObject::CompilationId
	Guid_t1_319  ___CompilationId_2;
};
