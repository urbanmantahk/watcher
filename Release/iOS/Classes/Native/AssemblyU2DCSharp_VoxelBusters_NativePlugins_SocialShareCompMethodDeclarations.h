﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.SocialShareComposerBase
struct SocialShareComposerBase_t8_283;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_eSocia.h"

// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::.ctor()
extern "C" void SocialShareComposerBase__ctor_m8_1673 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::.ctor(VoxelBusters.NativePlugins.Internal.eSocialServiceType)
extern "C" void SocialShareComposerBase__ctor_m8_1674 (SocialShareComposerBase_t8_283 * __this, int32_t ____serviceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.Internal.eSocialServiceType VoxelBusters.NativePlugins.SocialShareComposerBase::get_ServiceType()
extern "C" int32_t SocialShareComposerBase_get_ServiceType_m8_1675 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_ServiceType(VoxelBusters.NativePlugins.Internal.eSocialServiceType)
extern "C" void SocialShareComposerBase_set_ServiceType_m8_1676 (SocialShareComposerBase_t8_283 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.SocialShareComposerBase::get_Text()
extern "C" String_t* SocialShareComposerBase_get_Text_m8_1677 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_Text(System.String)
extern "C" void SocialShareComposerBase_set_Text_m8_1678 (SocialShareComposerBase_t8_283 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.SocialShareComposerBase::get_URL()
extern "C" String_t* SocialShareComposerBase_get_URL_m8_1679 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_URL(System.String)
extern "C" void SocialShareComposerBase_set_URL_m8_1680 (SocialShareComposerBase_t8_283 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] VoxelBusters.NativePlugins.SocialShareComposerBase::get_ImageData()
extern "C" ByteU5BU5D_t1_109* SocialShareComposerBase_get_ImageData_m8_1681 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::set_ImageData(System.Byte[])
extern "C" void SocialShareComposerBase_set_ImageData_m8_1682 (SocialShareComposerBase_t8_283 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.SocialShareComposerBase::get_IsReadyToShowView()
extern "C" bool SocialShareComposerBase_get_IsReadyToShowView_m8_1683 (SocialShareComposerBase_t8_283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.SocialShareComposerBase::AttachImage(UnityEngine.Texture2D)
extern "C" void SocialShareComposerBase_AttachImage_m8_1684 (SocialShareComposerBase_t8_283 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
