﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SliderState
struct SliderState_t6_244;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m6_1629 (SliderState_t6_244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
