﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifUndefined
struct ExifUndefined_t8_123;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifUndefined::.ctor(ExifLibrary.ExifTag,System.Byte[])
extern "C" void ExifUndefined__ctor_m8_584 (ExifUndefined_t8_123 * __this, int32_t ___tag, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifUndefined::get__Value()
extern "C" Object_t * ExifUndefined_get__Value_m8_585 (ExifUndefined_t8_123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUndefined::set__Value(System.Object)
extern "C" void ExifUndefined_set__Value_m8_586 (ExifUndefined_t8_123 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifUndefined::get_Value()
extern "C" ByteU5BU5D_t1_109* ExifUndefined_get_Value_m8_587 (ExifUndefined_t8_123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifUndefined::set_Value(System.Byte[])
extern "C" void ExifUndefined_set_Value_m8_588 (ExifUndefined_t8_123 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifUndefined::ToString()
extern "C" String_t* ExifUndefined_ToString_m8_589 (ExifUndefined_t8_123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifUndefined::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifUndefined_get_Interoperability_m8_590 (ExifUndefined_t8_123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.ExifUndefined::op_Implicit(ExifLibrary.ExifUndefined)
extern "C" ByteU5BU5D_t1_109* ExifUndefined_op_Implicit_m8_591 (Object_t * __this /* static, unused */, ExifUndefined_t8_123 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
