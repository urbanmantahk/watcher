﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.ApplicationSettings
struct ApplicationSettings_t8_320;
// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings
struct iOSSettings_t8_319;
// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings
struct AndroidSettings_t8_317;
// VoxelBusters.NativePlugins.ApplicationSettings/Features
struct Features_t8_318;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.ApplicationSettings::.ctor()
extern "C" void ApplicationSettings__ctor_m8_1879 (ApplicationSettings_t8_320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings::get_IsDebugMode()
extern "C" bool ApplicationSettings_get_IsDebugMode_m8_1880 (ApplicationSettings_t8_320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings VoxelBusters.NativePlugins.ApplicationSettings::get_IOS()
extern "C" iOSSettings_t8_319 * ApplicationSettings_get_IOS_m8_1881 (ApplicationSettings_t8_320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::set_IOS(VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings)
extern "C" void ApplicationSettings_set_IOS_m8_1882 (ApplicationSettings_t8_320 * __this, iOSSettings_t8_319 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings VoxelBusters.NativePlugins.ApplicationSettings::get_Android()
extern "C" AndroidSettings_t8_317 * ApplicationSettings_get_Android_m8_1883 (ApplicationSettings_t8_320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::set_Android(VoxelBusters.NativePlugins.ApplicationSettings/AndroidSettings)
extern "C" void ApplicationSettings_set_Android_m8_1884 (ApplicationSettings_t8_320 * __this, AndroidSettings_t8_317 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.ApplicationSettings/Features VoxelBusters.NativePlugins.ApplicationSettings::get_SupportedFeatures()
extern "C" Features_t8_318 * ApplicationSettings_get_SupportedFeatures_m8_1885 (ApplicationSettings_t8_320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ApplicationSettings::set_SupportedFeatures(VoxelBusters.NativePlugins.ApplicationSettings/Features)
extern "C" void ApplicationSettings_set_SupportedFeatures_m8_1886 (ApplicationSettings_t8_320 * __this, Features_t8_318 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.ApplicationSettings::get_StoreIdentifier()
extern "C" String_t* ApplicationSettings_get_StoreIdentifier_m8_1887 (ApplicationSettings_t8_320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
