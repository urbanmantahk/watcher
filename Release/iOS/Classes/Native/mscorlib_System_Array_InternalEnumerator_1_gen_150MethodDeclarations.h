﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_150.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18758_gshared (InternalEnumerator_1_t1_2267 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18758(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2267 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18758_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18759_gshared (InternalEnumerator_1_t1_2267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18759(__this, method) (( void (*) (InternalEnumerator_1_t1_2267 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18759_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18760_gshared (InternalEnumerator_1_t1_2267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18760(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2267 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18760_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18761_gshared (InternalEnumerator_1_t1_2267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18761(__this, method) (( void (*) (InternalEnumerator_1_t1_2267 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18761_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18762_gshared (InternalEnumerator_1_t1_2267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18762(__this, method) (( bool (*) (InternalEnumerator_1_t1_2267 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18762_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern "C" Color32_t6_49  InternalEnumerator_1_get_Current_m1_18763_gshared (InternalEnumerator_1_t1_2267 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18763(__this, method) (( Color32_t6_49  (*) (InternalEnumerator_1_t1_2267 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18763_gshared)(__this, method)
