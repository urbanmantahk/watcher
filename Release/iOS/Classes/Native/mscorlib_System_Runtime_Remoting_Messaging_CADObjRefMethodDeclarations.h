﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CADObjRef
struct CADObjRef_t1_922;
// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CADObjRef::.ctor(System.Runtime.Remoting.ObjRef,System.Int32)
extern "C" void CADObjRef__ctor_m1_8294 (CADObjRef_t1_922 * __this, ObjRef_t1_923 * ___o, int32_t ___sourceDomain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.CADObjRef::get_TypeName()
extern "C" String_t* CADObjRef_get_TypeName_m1_8295 (CADObjRef_t1_922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.CADObjRef::get_URI()
extern "C" String_t* CADObjRef_get_URI_m1_8296 (CADObjRef_t1_922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
