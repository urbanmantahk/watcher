﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.IdnMapping
struct IdnMapping_t1_375;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.IdnMapping::.ctor()
extern "C" void IdnMapping__ctor_m1_4187 (IdnMapping_t1_375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.IdnMapping::get_AllowUnassigned()
extern "C" bool IdnMapping_get_AllowUnassigned_m1_4188 (IdnMapping_t1_375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.IdnMapping::set_AllowUnassigned(System.Boolean)
extern "C" void IdnMapping_set_AllowUnassigned_m1_4189 (IdnMapping_t1_375 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.IdnMapping::get_UseStd3AsciiRules()
extern "C" bool IdnMapping_get_UseStd3AsciiRules_m1_4190 (IdnMapping_t1_375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.IdnMapping::set_UseStd3AsciiRules(System.Boolean)
extern "C" void IdnMapping_set_UseStd3AsciiRules_m1_4191 (IdnMapping_t1_375 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.IdnMapping::Equals(System.Object)
extern "C" bool IdnMapping_Equals_m1_4192 (IdnMapping_t1_375 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.IdnMapping::GetHashCode()
extern "C" int32_t IdnMapping_GetHashCode_m1_4193 (IdnMapping_t1_375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::GetAscii(System.String)
extern "C" String_t* IdnMapping_GetAscii_m1_4194 (IdnMapping_t1_375 * __this, String_t* ___unicode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::GetAscii(System.String,System.Int32)
extern "C" String_t* IdnMapping_GetAscii_m1_4195 (IdnMapping_t1_375 * __this, String_t* ___unicode, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::GetAscii(System.String,System.Int32,System.Int32)
extern "C" String_t* IdnMapping_GetAscii_m1_4196 (IdnMapping_t1_375 * __this, String_t* ___unicode, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::Convert(System.String,System.Int32,System.Int32,System.Boolean)
extern "C" String_t* IdnMapping_Convert_m1_4197 (IdnMapping_t1_375 * __this, String_t* ___input, int32_t ___index, int32_t ___count, bool ___toAscii, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::ToAscii(System.String,System.Int32)
extern "C" String_t* IdnMapping_ToAscii_m1_4198 (IdnMapping_t1_375 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.IdnMapping::VerifyLength(System.String,System.Int32)
extern "C" void IdnMapping_VerifyLength_m1_4199 (IdnMapping_t1_375 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::NamePrep(System.String,System.Int32)
extern "C" String_t* IdnMapping_NamePrep_m1_4200 (IdnMapping_t1_375 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.IdnMapping::VerifyProhibitedCharacters(System.String,System.Int32)
extern "C" void IdnMapping_VerifyProhibitedCharacters_m1_4201 (IdnMapping_t1_375 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.IdnMapping::VerifyStd3AsciiRules(System.String,System.Int32)
extern "C" void IdnMapping_VerifyStd3AsciiRules_m1_4202 (IdnMapping_t1_375 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::GetUnicode(System.String)
extern "C" String_t* IdnMapping_GetUnicode_m1_4203 (IdnMapping_t1_375 * __this, String_t* ___ascii, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::GetUnicode(System.String,System.Int32)
extern "C" String_t* IdnMapping_GetUnicode_m1_4204 (IdnMapping_t1_375 * __this, String_t* ___ascii, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::GetUnicode(System.String,System.Int32,System.Int32)
extern "C" String_t* IdnMapping_GetUnicode_m1_4205 (IdnMapping_t1_375 * __this, String_t* ___ascii, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.IdnMapping::ToUnicode(System.String,System.Int32)
extern "C" String_t* IdnMapping_ToUnicode_m1_4206 (IdnMapping_t1_375 * __this, String_t* ___s, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
