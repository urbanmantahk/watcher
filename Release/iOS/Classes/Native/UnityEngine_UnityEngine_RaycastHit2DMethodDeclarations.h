﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider2D
struct Collider2D_t6_119;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t6_120;
// UnityEngine.Transform
struct Transform_t6_65;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t6_47  RaycastHit2D_get_point_m6_872 (RaycastHit2D_t6_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t6_47  RaycastHit2D_get_normal_m6_873 (RaycastHit2D_t6_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m6_874 (RaycastHit2D_t6_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t6_119 * RaycastHit2D_get_collider_m6_875 (RaycastHit2D_t6_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t6_120 * RaycastHit2D_get_rigidbody_m6_876 (RaycastHit2D_t6_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t6_65 * RaycastHit2D_get_transform_m6_877 (RaycastHit2D_t6_118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
