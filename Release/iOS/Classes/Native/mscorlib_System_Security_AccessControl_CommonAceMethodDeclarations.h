﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CommonAce
struct CommonAce_t1_1122;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"
#include "mscorlib_System_Security_AccessControl_AceQualifier.h"

// System.Void System.Security.AccessControl.CommonAce::.ctor(System.Security.AccessControl.AceFlags,System.Security.AccessControl.AceQualifier,System.Int32,System.Security.Principal.SecurityIdentifier,System.Boolean,System.Byte[])
extern "C" void CommonAce__ctor_m1_9725 (CommonAce_t1_1122 * __this, uint8_t ___flags, int32_t ___qualifier, int32_t ___accessMask, SecurityIdentifier_t1_1132 * ___sid, bool ___isCallback, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CommonAce::get_BinaryLength()
extern "C" int32_t CommonAce_get_BinaryLength_m1_9726 (CommonAce_t1_1122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonAce::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void CommonAce_GetBinaryForm_m1_9727 (CommonAce_t1_1122 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CommonAce::MaxOpaqueLength(System.Boolean)
extern "C" int32_t CommonAce_MaxOpaqueLength_m1_9728 (Object_t * __this /* static, unused */, bool ___isCallback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
