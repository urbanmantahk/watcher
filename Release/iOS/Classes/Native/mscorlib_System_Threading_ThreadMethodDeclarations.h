﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Thread
struct Thread_t1_901;
// System.Threading.ThreadStart
struct ThreadStart_t1_1628;
// System.Threading.ParameterizedThreadStart
struct ParameterizedThreadStart_t1_1626;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Security.Principal.IPrincipal
struct IPrincipal_t1_1477;
// System.LocalDataStoreSlot
struct LocalDataStoreSlot_t1_1564;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.AppDomain
struct AppDomain_t1_1403;
// System.MulticastDelegate
struct MulticastDelegate_t1_21;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Threading.ExecutionContext
struct ExecutionContext_t1_918;
// System.Threading.CompressedStack
struct CompressedStack_t1_1395;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_Threading_ApartmentState.h"
#include "mscorlib_System_Threading_ThreadPriority.h"
#include "mscorlib_System_Threading_ThreadState.h"
#include "mscorlib_System_UIntPtr.h"

// System.Void System.Threading.Thread::.ctor(System.Threading.ThreadStart)
extern "C" void Thread__ctor_m1_12790 (Thread_t1_901 * __this, ThreadStart_t1_1628 * ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::.ctor(System.Threading.ThreadStart,System.Int32)
extern "C" void Thread__ctor_m1_12791 (Thread_t1_901 * __this, ThreadStart_t1_1628 * ___start, int32_t ___maxStackSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::.ctor(System.Threading.ParameterizedThreadStart)
extern "C" void Thread__ctor_m1_12792 (Thread_t1_901 * __this, ParameterizedThreadStart_t1_1626 * ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::.ctor(System.Threading.ParameterizedThreadStart,System.Int32)
extern "C" void Thread__ctor_m1_12793 (Thread_t1_901 * __this, ParameterizedThreadStart_t1_1626 * ___start, int32_t ___maxStackSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::.cctor()
extern "C" void Thread__cctor_m1_12794 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::System.Runtime.InteropServices._Thread.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Thread_System_Runtime_InteropServices__Thread_GetIDsOfNames_m1_12795 (Thread_t1_901 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::System.Runtime.InteropServices._Thread.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Thread_System_Runtime_InteropServices__Thread_GetTypeInfo_m1_12796 (Thread_t1_901 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::System.Runtime.InteropServices._Thread.GetTypeInfoCount(System.UInt32&)
extern "C" void Thread_System_Runtime_InteropServices__Thread_GetTypeInfoCount_m1_12797 (Thread_t1_901 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::System.Runtime.InteropServices._Thread.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void Thread_System_Runtime_InteropServices__Thread_Invoke_m1_12798 (Thread_t1_901 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.Threading.Thread::get_CurrentContext()
extern "C" Context_t1_891 * Thread_get_CurrentContext_m1_12799 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.IPrincipal System.Threading.Thread::get_CurrentPrincipal()
extern "C" Object_t * Thread_get_CurrentPrincipal_m1_12800 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_CurrentPrincipal(System.Security.Principal.IPrincipal)
extern "C" void Thread_set_CurrentPrincipal_m1_12801 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Threading.Thread::CurrentThread_internal()
extern "C" Thread_t1_901 * Thread_CurrentThread_internal_m1_12802 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C" Thread_t1_901 * Thread_get_CurrentThread_m1_12803 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_CurrentThreadId()
extern "C" int32_t Thread_get_CurrentThreadId_m1_12804 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::InitDataStoreHash()
extern "C" void Thread_InitDataStoreHash_m1_12805 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.LocalDataStoreSlot System.Threading.Thread::AllocateNamedDataSlot(System.String)
extern "C" LocalDataStoreSlot_t1_1564 * Thread_AllocateNamedDataSlot_m1_12806 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::FreeNamedDataSlot(System.String)
extern "C" void Thread_FreeNamedDataSlot_m1_12807 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.LocalDataStoreSlot System.Threading.Thread::AllocateDataSlot()
extern "C" LocalDataStoreSlot_t1_1564 * Thread_AllocateDataSlot_m1_12808 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.Thread::GetData(System.LocalDataStoreSlot)
extern "C" Object_t * Thread_GetData_m1_12809 (Object_t * __this /* static, unused */, LocalDataStoreSlot_t1_1564 * ___slot, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetData(System.LocalDataStoreSlot,System.Object)
extern "C" void Thread_SetData_m1_12810 (Object_t * __this /* static, unused */, LocalDataStoreSlot_t1_1564 * ___slot, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::FreeLocalSlotValues(System.Int32,System.Boolean)
extern "C" void Thread_FreeLocalSlotValues_m1_12811 (Object_t * __this /* static, unused */, int32_t ___slot, bool ___thread_local, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.LocalDataStoreSlot System.Threading.Thread::GetNamedDataSlot(System.String)
extern "C" LocalDataStoreSlot_t1_1564 * Thread_GetNamedDataSlot_m1_12812 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.Threading.Thread::GetDomain()
extern "C" AppDomain_t1_1403 * Thread_GetDomain_m1_12813 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::GetDomainID()
extern "C" int32_t Thread_GetDomainID_m1_12814 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::ResetAbort_internal()
extern "C" void Thread_ResetAbort_internal_m1_12815 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::ResetAbort()
extern "C" void Thread_ResetAbort_m1_12816 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Sleep_internal(System.Int32)
extern "C" void Thread_Sleep_internal_m1_12817 (Object_t * __this /* static, unused */, int32_t ___ms, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Sleep(System.Int32)
extern "C" void Thread_Sleep_m1_12818 (Object_t * __this /* static, unused */, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Sleep(System.TimeSpan)
extern "C" void Thread_Sleep_m1_12819 (Object_t * __this /* static, unused */, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Threading.Thread::Thread_internal(System.MulticastDelegate)
extern "C" IntPtr_t Thread_Thread_internal_m1_12820 (Thread_t1_901 * __this, MulticastDelegate_t1_21 * ___start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Thread_init()
extern "C" void Thread_Thread_init_m1_12821 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ApartmentState System.Threading.Thread::get_ApartmentState()
extern "C" int32_t Thread_get_ApartmentState_m1_12822 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_ApartmentState(System.Threading.ApartmentState)
extern "C" void Thread_set_ApartmentState_m1_12823 (Thread_t1_901 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Threading.Thread::GetCachedCurrentCulture()
extern "C" CultureInfo_t1_277 * Thread_GetCachedCurrentCulture_m1_12824 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Threading.Thread::GetSerializedCurrentCulture()
extern "C" ByteU5BU5D_t1_109* Thread_GetSerializedCurrentCulture_m1_12825 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetCachedCurrentCulture(System.Globalization.CultureInfo)
extern "C" void Thread_SetCachedCurrentCulture_m1_12826 (Thread_t1_901 * __this, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetSerializedCurrentCulture(System.Byte[])
extern "C" void Thread_SetSerializedCurrentCulture_m1_12827 (Thread_t1_901 * __this, ByteU5BU5D_t1_109* ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Threading.Thread::GetCachedCurrentUICulture()
extern "C" CultureInfo_t1_277 * Thread_GetCachedCurrentUICulture_m1_12828 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Threading.Thread::GetSerializedCurrentUICulture()
extern "C" ByteU5BU5D_t1_109* Thread_GetSerializedCurrentUICulture_m1_12829 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetCachedCurrentUICulture(System.Globalization.CultureInfo)
extern "C" void Thread_SetCachedCurrentUICulture_m1_12830 (Thread_t1_901 * __this, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetSerializedCurrentUICulture(System.Byte[])
extern "C" void Thread_SetSerializedCurrentUICulture_m1_12831 (Thread_t1_901 * __this, ByteU5BU5D_t1_109* ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Threading.Thread::get_CurrentCulture()
extern "C" CultureInfo_t1_277 * Thread_get_CurrentCulture_m1_12832 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_CurrentCulture(System.Globalization.CultureInfo)
extern "C" void Thread_set_CurrentCulture_m1_12833 (Thread_t1_901 * __this, CultureInfo_t1_277 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Threading.Thread::get_CurrentUICulture()
extern "C" CultureInfo_t1_277 * Thread_get_CurrentUICulture_m1_12834 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_CurrentUICulture(System.Globalization.CultureInfo)
extern "C" void Thread_set_CurrentUICulture_m1_12835 (Thread_t1_901 * __this, CultureInfo_t1_277 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::get_IsThreadPoolThread()
extern "C" bool Thread_get_IsThreadPoolThread_m1_12836 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::get_IsThreadPoolThreadInternal()
extern "C" bool Thread_get_IsThreadPoolThreadInternal_m1_12837 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_IsThreadPoolThreadInternal(System.Boolean)
extern "C" void Thread_set_IsThreadPoolThreadInternal_m1_12838 (Thread_t1_901 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::get_IsAlive()
extern "C" bool Thread_get_IsAlive_m1_12839 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::get_IsBackground()
extern "C" bool Thread_get_IsBackground_m1_12840 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_IsBackground(System.Boolean)
extern "C" void Thread_set_IsBackground_m1_12841 (Thread_t1_901 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Threading.Thread::GetName_internal()
extern "C" String_t* Thread_GetName_internal_m1_12842 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetName_internal(System.String)
extern "C" void Thread_SetName_internal_m1_12843 (Thread_t1_901 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Threading.Thread::get_Name()
extern "C" String_t* Thread_get_Name_m1_12844 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_Name(System.String)
extern "C" void Thread_set_Name_m1_12845 (Thread_t1_901 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ThreadPriority System.Threading.Thread::get_Priority()
extern "C" int32_t Thread_get_Priority_m1_12846 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::set_Priority(System.Threading.ThreadPriority)
extern "C" void Thread_set_Priority_m1_12847 (Thread_t1_901 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ThreadState System.Threading.Thread::get_ThreadState()
extern "C" int32_t Thread_get_ThreadState_m1_12848 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Abort_internal(System.Object)
extern "C" void Thread_Abort_internal_m1_12849 (Thread_t1_901 * __this, Object_t * ___stateInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Abort()
extern "C" void Thread_Abort_m1_12850 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Abort(System.Object)
extern "C" void Thread_Abort_m1_12851 (Thread_t1_901 * __this, Object_t * ___stateInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.Thread::GetAbortExceptionState()
extern "C" Object_t * Thread_GetAbortExceptionState_m1_12852 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Interrupt_internal()
extern "C" void Thread_Interrupt_internal_m1_12853 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Interrupt()
extern "C" void Thread_Interrupt_m1_12854 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::Join_internal(System.Int32,System.IntPtr)
extern "C" bool Thread_Join_internal_m1_12855 (Thread_t1_901 * __this, int32_t ___ms, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Join()
extern "C" void Thread_Join_m1_12856 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::Join(System.Int32)
extern "C" bool Thread_Join_m1_12857 (Thread_t1_901 * __this, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::Join(System.TimeSpan)
extern "C" bool Thread_Join_m1_12858 (Thread_t1_901 * __this, TimeSpan_t1_368  ___timeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::MemoryBarrier()
extern "C" void Thread_MemoryBarrier_m1_12859 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Resume_internal()
extern "C" void Thread_Resume_internal_m1_12860 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Resume()
extern "C" void Thread_Resume_m1_12861 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SpinWait_nop()
extern "C" void Thread_SpinWait_nop_m1_12862 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SpinWait(System.Int32)
extern "C" void Thread_SpinWait_m1_12863 (Object_t * __this /* static, unused */, int32_t ___iterations, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Start()
extern "C" void Thread_Start_m1_12864 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Suspend_internal()
extern "C" void Thread_Suspend_internal_m1_12865 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Suspend()
extern "C" void Thread_Suspend_m1_12866 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Thread_free_internal(System.IntPtr)
extern "C" void Thread_Thread_free_internal_m1_12867 (Thread_t1_901 * __this, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Finalize()
extern "C" void Thread_Finalize_m1_12868 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetState(System.Threading.ThreadState)
extern "C" void Thread_SetState_m1_12869 (Thread_t1_901 * __this, int32_t ___set, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::ClrState(System.Threading.ThreadState)
extern "C" void Thread_ClrState_m1_12870 (Thread_t1_901 * __this, int32_t ___clr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ThreadState System.Threading.Thread::GetState()
extern "C" int32_t Thread_GetState_m1_12871 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Threading.Thread::VolatileRead(System.Byte&)
extern "C" uint8_t Thread_VolatileRead_m1_12872 (Object_t * __this /* static, unused */, uint8_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Threading.Thread::VolatileRead(System.Double&)
extern "C" double Thread_VolatileRead_m1_12873 (Object_t * __this /* static, unused */, double* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Threading.Thread::VolatileRead(System.Int16&)
extern "C" int16_t Thread_VolatileRead_m1_12874 (Object_t * __this /* static, unused */, int16_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::VolatileRead(System.Int32&)
extern "C" int32_t Thread_VolatileRead_m1_12875 (Object_t * __this /* static, unused */, int32_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Threading.Thread::VolatileRead(System.Int64&)
extern "C" int64_t Thread_VolatileRead_m1_12876 (Object_t * __this /* static, unused */, int64_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Threading.Thread::VolatileRead(System.IntPtr&)
extern "C" IntPtr_t Thread_VolatileRead_m1_12877 (Object_t * __this /* static, unused */, IntPtr_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.Thread::VolatileRead(System.Object&)
extern "C" Object_t * Thread_VolatileRead_m1_12878 (Object_t * __this /* static, unused */, Object_t ** ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Threading.Thread::VolatileRead(System.SByte&)
extern "C" int8_t Thread_VolatileRead_m1_12879 (Object_t * __this /* static, unused */, int8_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Threading.Thread::VolatileRead(System.Single&)
extern "C" float Thread_VolatileRead_m1_12880 (Object_t * __this /* static, unused */, float* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Threading.Thread::VolatileRead(System.UInt16&)
extern "C" uint16_t Thread_VolatileRead_m1_12881 (Object_t * __this /* static, unused */, uint16_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Threading.Thread::VolatileRead(System.UInt32&)
extern "C" uint32_t Thread_VolatileRead_m1_12882 (Object_t * __this /* static, unused */, uint32_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Threading.Thread::VolatileRead(System.UInt64&)
extern "C" uint64_t Thread_VolatileRead_m1_12883 (Object_t * __this /* static, unused */, uint64_t* ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr System.Threading.Thread::VolatileRead(System.UIntPtr&)
extern "C" UIntPtr_t  Thread_VolatileRead_m1_12884 (Object_t * __this /* static, unused */, UIntPtr_t * ___address, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Byte&,System.Byte)
extern "C" void Thread_VolatileWrite_m1_12885 (Object_t * __this /* static, unused */, uint8_t* ___address, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Double&,System.Double)
extern "C" void Thread_VolatileWrite_m1_12886 (Object_t * __this /* static, unused */, double* ___address, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Int16&,System.Int16)
extern "C" void Thread_VolatileWrite_m1_12887 (Object_t * __this /* static, unused */, int16_t* ___address, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Int32&,System.Int32)
extern "C" void Thread_VolatileWrite_m1_12888 (Object_t * __this /* static, unused */, int32_t* ___address, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Int64&,System.Int64)
extern "C" void Thread_VolatileWrite_m1_12889 (Object_t * __this /* static, unused */, int64_t* ___address, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.IntPtr&,System.IntPtr)
extern "C" void Thread_VolatileWrite_m1_12890 (Object_t * __this /* static, unused */, IntPtr_t* ___address, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Object&,System.Object)
extern "C" void Thread_VolatileWrite_m1_12891 (Object_t * __this /* static, unused */, Object_t ** ___address, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.SByte&,System.SByte)
extern "C" void Thread_VolatileWrite_m1_12892 (Object_t * __this /* static, unused */, int8_t* ___address, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.Single&,System.Single)
extern "C" void Thread_VolatileWrite_m1_12893 (Object_t * __this /* static, unused */, float* ___address, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.UInt16&,System.UInt16)
extern "C" void Thread_VolatileWrite_m1_12894 (Object_t * __this /* static, unused */, uint16_t* ___address, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.UInt32&,System.UInt32)
extern "C" void Thread_VolatileWrite_m1_12895 (Object_t * __this /* static, unused */, uint32_t* ___address, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.UInt64&,System.UInt64)
extern "C" void Thread_VolatileWrite_m1_12896 (Object_t * __this /* static, unused */, uint64_t* ___address, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::VolatileWrite(System.UIntPtr&,System.UIntPtr)
extern "C" void Thread_VolatileWrite_m1_12897 (Object_t * __this /* static, unused */, UIntPtr_t * ___address, UIntPtr_t  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::GetNewManagedId()
extern "C" int32_t Thread_GetNewManagedId_m1_12898 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::GetNewManagedId_internal()
extern "C" int32_t Thread_GetNewManagedId_internal_m1_12899 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ExecutionContext System.Threading.Thread::get_ExecutionContext()
extern "C" ExecutionContext_t1_918 * Thread_get_ExecutionContext_m1_12900 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C" int32_t Thread_get_ManagedThreadId_m1_12901 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::BeginCriticalRegion()
extern "C" void Thread_BeginCriticalRegion_m1_12902 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::EndCriticalRegion()
extern "C" void Thread_EndCriticalRegion_m1_12903 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::BeginThreadAffinity()
extern "C" void Thread_BeginThreadAffinity_m1_12904 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::EndThreadAffinity()
extern "C" void Thread_EndThreadAffinity_m1_12905 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.ApartmentState System.Threading.Thread::GetApartmentState()
extern "C" int32_t Thread_GetApartmentState_m1_12906 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetApartmentState(System.Threading.ApartmentState)
extern "C" void Thread_SetApartmentState_m1_12907 (Thread_t1_901 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.Thread::TrySetApartmentState(System.Threading.ApartmentState)
extern "C" bool Thread_TrySetApartmentState_m1_12908 (Thread_t1_901 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::GetHashCode()
extern "C" int32_t Thread_GetHashCode_m1_12909 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::Start(System.Object)
extern "C" void Thread_Start_m1_12910 (Thread_t1_901 * __this, Object_t * ___parameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Threading.Thread::GetCompressedStack()
extern "C" CompressedStack_t1_1395 * Thread_GetCompressedStack_m1_12911 (Thread_t1_901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Thread::SetCompressedStack(System.Threading.CompressedStack)
extern "C" void Thread_SetCompressedStack_m1_12912 (Thread_t1_901 * __this, CompressedStack_t1_1395 * ___stack, const MethodInfo* method) IL2CPP_METHOD_ATTR;
