﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Guid.h"

// System.Diagnostics.SymbolStore.SymLanguageVendor
struct  SymLanguageVendor_t1_321  : public Object_t
{
};
struct SymLanguageVendor_t1_321_StaticFields{
	// System.Guid System.Diagnostics.SymbolStore.SymLanguageVendor::Microsoft
	Guid_t1_319  ___Microsoft_0;
};
