﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t5_11;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1_1929;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>,System.Collections.DictionaryEntry>
struct  Transform_1_t1_2693  : public MulticastDelegate_t1_21
{
};
