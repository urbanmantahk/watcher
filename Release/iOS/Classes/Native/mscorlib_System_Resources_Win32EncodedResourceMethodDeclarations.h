﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32EncodedResource
struct Win32EncodedResource_t1_665;
// System.Resources.NameOrId
struct NameOrId_t1_663;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.Stream
struct Stream_t1_405;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.Win32EncodedResource::.ctor(System.Resources.NameOrId,System.Resources.NameOrId,System.Int32,System.Byte[])
extern "C" void Win32EncodedResource__ctor_m1_7477 (Win32EncodedResource_t1_665 * __this, NameOrId_t1_663 * ___type, NameOrId_t1_663 * ___name, int32_t ___language, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Resources.Win32EncodedResource::get_Data()
extern "C" ByteU5BU5D_t1_109* Win32EncodedResource_get_Data_m1_7478 (Win32EncodedResource_t1_665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32EncodedResource::WriteTo(System.IO.Stream)
extern "C" void Win32EncodedResource_WriteTo_m1_7479 (Win32EncodedResource_t1_665 * __this, Stream_t1_405 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
