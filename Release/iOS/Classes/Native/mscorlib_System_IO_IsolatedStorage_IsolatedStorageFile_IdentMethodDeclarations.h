﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageFile_Ident.h"

// System.Void System.IO.IsolatedStorage.IsolatedStorageFile/Identities::.ctor(System.Object,System.Object,System.Object)
extern "C" void Identities__ctor_m1_4603 (Identities_t1_396 * __this, Object_t * ___application, Object_t * ___assembly, Object_t * ___domain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
