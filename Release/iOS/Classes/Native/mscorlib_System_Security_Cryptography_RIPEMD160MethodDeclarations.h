﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.RIPEMD160
struct RIPEMD160_t1_1232;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.RIPEMD160::.ctor()
extern "C" void RIPEMD160__ctor_m1_10475 (RIPEMD160_t1_1232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RIPEMD160 System.Security.Cryptography.RIPEMD160::Create()
extern "C" RIPEMD160_t1_1232 * RIPEMD160_Create_m1_10476 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RIPEMD160 System.Security.Cryptography.RIPEMD160::Create(System.String)
extern "C" RIPEMD160_t1_1232 * RIPEMD160_Create_m1_10477 (Object_t * __this /* static, unused */, String_t* ___hashName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
