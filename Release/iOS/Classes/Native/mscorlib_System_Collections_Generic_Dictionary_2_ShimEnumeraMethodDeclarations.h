﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t1_1993;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1_1978;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_15765_gshared (ShimEnumerator_t1_1993 * __this, Dictionary_2_t1_1978 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_15765(__this, ___host, method) (( void (*) (ShimEnumerator_t1_1993 *, Dictionary_2_t1_1978 *, const MethodInfo*))ShimEnumerator__ctor_m1_15765_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_15766_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_15766(__this, method) (( void (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_Dispose_m1_15766_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_15767_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_15767(__this, method) (( bool (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_15767_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_15768_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_15768(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_15768_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_15769_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_15769(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_get_Key_m1_15769_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_15770_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_15770(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_get_Value_m1_15770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_15771_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_15771(__this, method) (( Object_t * (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_get_Current_m1_15771_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Reset()
extern "C" void ShimEnumerator_Reset_m1_15772_gshared (ShimEnumerator_t1_1993 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_15772(__this, method) (( void (*) (ShimEnumerator_t1_1993 *, const MethodInfo*))ShimEnumerator_Reset_m1_15772_gshared)(__this, method)
