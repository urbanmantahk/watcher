﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1_25736(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_2628 *, Dictionary_2_t1_1919 *, const MethodInfo*))KeyCollection__ctor_m1_15970_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_25737(__this, ___item, method) (( void (*) (KeyCollection_t1_2628 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_15971_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_25738(__this, method) (( void (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_15972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_25739(__this, ___item, method) (( bool (*) (KeyCollection_t1_2628 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_15973_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_25740(__this, ___item, method) (( bool (*) (KeyCollection_t1_2628 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_15974_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_25741(__this, method) (( Object_t* (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_15975_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_25742(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_2628 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_15976_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_25743(__this, method) (( Object_t * (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_15977_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_25744(__this, method) (( bool (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_15978_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_25745(__this, method) (( bool (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_15979_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_25746(__this, method) (( Object_t * (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_15980_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1_25747(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_2628 *, StringU5BU5D_t1_238*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_15981_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1_25748(__this, method) (( Enumerator_t1_2868  (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_15982_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Boomlagoon.JSON.JSONValue>::get_Count()
#define KeyCollection_get_Count_m1_25749(__this, method) (( int32_t (*) (KeyCollection_t1_2628 *, const MethodInfo*))KeyCollection_get_Count_m1_15983_gshared)(__this, method)
