﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.RemotingServices
struct RemotingServices_t1_1027;
// System.Object
struct Object_t;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type
struct Type_t;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t1_1718;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.String
struct String_t;
// System.Runtime.Remoting.ObjRef
struct ObjRef_t1_923;
// System.Runtime.Remoting.Proxies.RealProxy
struct RealProxy_t1_130;
// System.Runtime.Remoting.Messaging.IMethodMessage
struct IMethodMessage_t1_948;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.ActivatedClientTypeEntry
struct ActivatedClientTypeEntry_t1_1008;
// System.Runtime.Remoting.WellKnownClientTypeEntry
struct WellKnownClientTypeEntry_t1_1039;
// System.Runtime.Remoting.Identity
struct Identity_t1_943;
// System.Runtime.Remoting.ClientIdentity
struct ClientIdentity_t1_1013;
// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t1_1031;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1_70;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Exception
struct Exception_t1_33;
// System.AppDomain
struct AppDomain_t1_1403;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1_627;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"

// System.Void System.Runtime.Remoting.RemotingServices::.ctor()
extern "C" void RemotingServices__ctor_m1_9178 (RemotingServices_t1_1027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::.cctor()
extern "C" void RemotingServices__cctor_m1_9179 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::InternalExecute(System.Reflection.MethodBase,System.Object,System.Object[],System.Object[]&)
extern "C" Object_t * RemotingServices_InternalExecute_m1_9180 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___method, Object_t * ___obj, ObjectU5BU5D_t1_272* ___parameters, ObjectU5BU5D_t1_272** ___out_args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetVirtualMethod(System.Type,System.Reflection.MethodBase)
extern "C" MethodBase_t1_335 * RemotingServices_GetVirtualMethod_m1_9181 (Object_t * __this /* static, unused */, Type_t * ___type, MethodBase_t1_335 * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::IsTransparentProxy(System.Object)
extern "C" bool RemotingServices_IsTransparentProxy_m1_9182 (Object_t * __this /* static, unused */, Object_t * ___proxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage System.Runtime.Remoting.RemotingServices::InternalExecuteMessage(System.MarshalByRefObject,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" Object_t * RemotingServices_InternalExecuteMessage_m1_9183 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___target, Object_t * ___reqMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage System.Runtime.Remoting.RemotingServices::ExecuteMessage(System.MarshalByRefObject,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" Object_t * RemotingServices_ExecuteMessage_m1_9184 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___target, Object_t * ___reqMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::Connect(System.Type,System.String)
extern "C" Object_t * RemotingServices_Connect_m1_9185 (Object_t * __this /* static, unused */, Type_t * ___classToProxy, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::Connect(System.Type,System.String,System.Object)
extern "C" Object_t * RemotingServices_Connect_m1_9186 (Object_t * __this /* static, unused */, Type_t * ___classToProxy, String_t* ___url, Object_t * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::Disconnect(System.MarshalByRefObject)
extern "C" bool RemotingServices_Disconnect_m1_9187 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.RemotingServices::GetServerTypeForUri(System.String)
extern "C" Type_t * RemotingServices_GetServerTypeForUri_m1_9188 (Object_t * __this /* static, unused */, String_t* ___URI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingServices::GetObjectUri(System.MarshalByRefObject)
extern "C" String_t* RemotingServices_GetObjectUri_m1_9189 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef)
extern "C" Object_t * RemotingServices_Unmarshal_m1_9190 (Object_t * __this /* static, unused */, ObjRef_t1_923 * ___objectRef, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::Unmarshal(System.Runtime.Remoting.ObjRef,System.Boolean)
extern "C" Object_t * RemotingServices_Unmarshal_m1_9191 (Object_t * __this /* static, unused */, ObjRef_t1_923 * ___objectRef, bool ___fRefine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.RemotingServices::Marshal(System.MarshalByRefObject)
extern "C" ObjRef_t1_923 * RemotingServices_Marshal_m1_9192 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___Obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.RemotingServices::Marshal(System.MarshalByRefObject,System.String)
extern "C" ObjRef_t1_923 * RemotingServices_Marshal_m1_9193 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___Obj, String_t* ___URI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.RemotingServices::Marshal(System.MarshalByRefObject,System.String,System.Type)
extern "C" ObjRef_t1_923 * RemotingServices_Marshal_m1_9194 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___Obj, String_t* ___ObjURI, Type_t * ___RequestedType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingServices::NewUri()
extern "C" String_t* RemotingServices_NewUri_m1_9195 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Proxies.RealProxy System.Runtime.Remoting.RemotingServices::GetRealProxy(System.Object)
extern "C" RealProxy_t1_130 * RemotingServices_GetRealProxy_m1_9196 (Object_t * __this /* static, unused */, Object_t * ___proxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromMethodMessage(System.Runtime.Remoting.Messaging.IMethodMessage)
extern "C" MethodBase_t1_335 * RemotingServices_GetMethodBaseFromMethodMessage_m1_9197 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::GetMethodBaseFromName(System.Type,System.String,System.Type[])
extern "C" MethodBase_t1_335 * RemotingServices_GetMethodBaseFromName_m1_9198 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___methodName, TypeU5BU5D_t1_31* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Runtime.Remoting.RemotingServices::FindInterfaceMethod(System.Type,System.String,System.Type[])
extern "C" MethodBase_t1_335 * RemotingServices_FindInterfaceMethod_m1_9199 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___methodName, TypeU5BU5D_t1_31* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::GetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void RemotingServices_GetObjectData_m1_9200 (Object_t * __this /* static, unused */, Object_t * ___obj, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjRef System.Runtime.Remoting.RemotingServices::GetObjRefForProxy(System.MarshalByRefObject)
extern "C" ObjRef_t1_923 * RemotingServices_GetObjRefForProxy_m1_9201 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::GetLifetimeService(System.MarshalByRefObject)
extern "C" Object_t * RemotingServices_GetLifetimeService_m1_9202 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.RemotingServices::GetEnvoyChainForProxy(System.MarshalByRefObject)
extern "C" Object_t * RemotingServices_GetEnvoyChainForProxy_m1_9203 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::LogRemotingStage(System.Int32)
extern "C" void RemotingServices_LogRemotingStage_m1_9204 (Object_t * __this /* static, unused */, int32_t ___stage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingServices::GetSessionIdForMethodMessage(System.Runtime.Remoting.Messaging.IMethodMessage)
extern "C" String_t* RemotingServices_GetSessionIdForMethodMessage_m1_9205 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::IsMethodOverloaded(System.Runtime.Remoting.Messaging.IMethodMessage)
extern "C" bool RemotingServices_IsMethodOverloaded_m1_9206 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::IsObjectOutOfAppDomain(System.Object)
extern "C" bool RemotingServices_IsObjectOutOfAppDomain_m1_9207 (Object_t * __this /* static, unused */, Object_t * ___tp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::IsObjectOutOfContext(System.Object)
extern "C" bool RemotingServices_IsObjectOutOfContext_m1_9208 (Object_t * __this /* static, unused */, Object_t * ___tp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::IsOneWay(System.Reflection.MethodBase)
extern "C" bool RemotingServices_IsOneWay_m1_9209 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::IsAsyncMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" bool RemotingServices_IsAsyncMessage_m1_9210 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::SetObjectUriForMarshal(System.MarshalByRefObject,System.String)
extern "C" void RemotingServices_SetObjectUriForMarshal_m1_9211 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Runtime.Remoting.ActivatedClientTypeEntry,System.Object[])
extern "C" Object_t * RemotingServices_CreateClientProxy_m1_9212 (Object_t * __this /* static, unused */, ActivatedClientTypeEntry_t1_1008 * ___entry, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Type,System.String,System.Object[])
extern "C" Object_t * RemotingServices_CreateClientProxy_m1_9213 (Object_t * __this /* static, unused */, Type_t * ___objectType, String_t* ___url, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxy(System.Runtime.Remoting.WellKnownClientTypeEntry)
extern "C" Object_t * RemotingServices_CreateClientProxy_m1_9214 (Object_t * __this /* static, unused */, WellKnownClientTypeEntry_t1_1039 * ___entry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::CreateClientProxyForContextBound(System.Type,System.Object[])
extern "C" Object_t * RemotingServices_CreateClientProxyForContextBound_m1_9215 (Object_t * __this /* static, unused */, Type_t * ___type, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetIdentityForUri(System.String)
extern "C" Identity_t1_943 * RemotingServices_GetIdentityForUri_m1_9216 (Object_t * __this /* static, unused */, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingServices::RemoveAppNameFromUri(System.String)
extern "C" String_t* RemotingServices_RemoveAppNameFromUri_m1_9217 (Object_t * __this /* static, unused */, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetObjectIdentity(System.MarshalByRefObject)
extern "C" Identity_t1_943 * RemotingServices_GetObjectIdentity_m1_9218 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ClientIdentity System.Runtime.Remoting.RemotingServices::GetOrCreateClientIdentity(System.Runtime.Remoting.ObjRef,System.Type,System.Object&)
extern "C" ClientIdentity_t1_1013 * RemotingServices_GetOrCreateClientIdentity_m1_9219 (Object_t * __this /* static, unused */, ObjRef_t1_923 * ___objRef, Type_t * ___proxyType, Object_t ** ___clientProxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.RemotingServices::GetClientChannelSinkChain(System.String,System.Object,System.String&)
extern "C" Object_t * RemotingServices_GetClientChannelSinkChain_m1_9220 (Object_t * __this /* static, unused */, String_t* ___url, Object_t * ___channelData, String_t** ___objectUri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ClientActivatedIdentity System.Runtime.Remoting.RemotingServices::CreateContextBoundObjectIdentity(System.Type)
extern "C" ClientActivatedIdentity_t1_1031 * RemotingServices_CreateContextBoundObjectIdentity_m1_9221 (Object_t * __this /* static, unused */, Type_t * ___objectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ClientActivatedIdentity System.Runtime.Remoting.RemotingServices::CreateClientActivatedServerIdentity(System.MarshalByRefObject,System.Type,System.String)
extern "C" ClientActivatedIdentity_t1_1031 * RemotingServices_CreateClientActivatedServerIdentity_m1_9222 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___realObject, Type_t * ___objectType, String_t* ___objectUri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ServerIdentity System.Runtime.Remoting.RemotingServices::CreateWellKnownServerIdentity(System.Type,System.String,System.Runtime.Remoting.WellKnownObjectMode)
extern "C" ServerIdentity_t1_70 * RemotingServices_CreateWellKnownServerIdentity_m1_9223 (Object_t * __this /* static, unused */, Type_t * ___objectType, String_t* ___objectUri, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::RegisterServerIdentity(System.Runtime.Remoting.ServerIdentity)
extern "C" void RemotingServices_RegisterServerIdentity_m1_9224 (Object_t * __this /* static, unused */, ServerIdentity_t1_70 * ___identity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::GetProxyForRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern "C" Object_t * RemotingServices_GetProxyForRemoteObject_m1_9225 (Object_t * __this /* static, unused */, ObjRef_t1_923 * ___objref, Type_t * ___classToProxy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::GetRemoteObject(System.Runtime.Remoting.ObjRef,System.Type)
extern "C" Object_t * RemotingServices_GetRemoteObject_m1_9226 (Object_t * __this /* static, unused */, ObjRef_t1_923 * ___objRef, Type_t * ___proxyType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::GetServerObject(System.String)
extern "C" Object_t * RemotingServices_GetServerObject_m1_9227 (Object_t * __this /* static, unused */, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Runtime.Remoting.RemotingServices::SerializeCallData(System.Object)
extern "C" ByteU5BU5D_t1_109* RemotingServices_SerializeCallData_m1_9228 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::DeserializeCallData(System.Byte[])
extern "C" Object_t * RemotingServices_DeserializeCallData_m1_9229 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Runtime.Remoting.RemotingServices::SerializeExceptionData(System.Exception)
extern "C" ByteU5BU5D_t1_109* RemotingServices_SerializeExceptionData_m1_9230 (Object_t * __this /* static, unused */, Exception_t1_33 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.RemotingServices::GetDomainProxy(System.AppDomain)
extern "C" Object_t * RemotingServices_GetDomainProxy_m1_9231 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::RegisterInternalChannels()
extern "C" void RemotingServices_RegisterInternalChannels_m1_9232 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::DisposeIdentity(System.Runtime.Remoting.Identity)
extern "C" void RemotingServices_DisposeIdentity_m1_9233 (Object_t * __this /* static, unused */, Identity_t1_943 * ___ident, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Identity System.Runtime.Remoting.RemotingServices::GetMessageTargetIdentity(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Identity_t1_943 * RemotingServices_GetMessageTargetIdentity_m1_9234 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingServices::SetMessageTargetIdentity(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Identity)
extern "C" void RemotingServices_SetMessageTargetIdentity_m1_9235 (Object_t * __this /* static, unused */, Object_t * ___msg, Identity_t1_943 * ___ident, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingServices::UpdateOutArgObject(System.Reflection.ParameterInfo,System.Object,System.Object)
extern "C" bool RemotingServices_UpdateOutArgObject_m1_9236 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___pi, Object_t * ___local, Object_t * ___remote, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingServices::GetNormalizedUri(System.String)
extern "C" String_t* RemotingServices_GetNormalizedUri_m1_9237 (Object_t * __this /* static, unused */, String_t* ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
