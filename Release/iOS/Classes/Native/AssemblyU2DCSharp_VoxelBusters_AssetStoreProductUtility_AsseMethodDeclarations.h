﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.AssetStoreProductUtility.AssetStoreProduct
struct AssetStoreProduct_t8_22;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::.ctor()
extern "C" void AssetStoreProduct__ctor_m8_150 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::.ctor(System.String,System.String,System.String)
extern "C" void AssetStoreProduct__ctor_m8_151 (AssetStoreProduct_t8_22 * __this, String_t* ____pName, String_t* ____pVersion, String_t* ____logoPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::get_ProductName()
extern "C" String_t* AssetStoreProduct_get_ProductName_m8_152 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::set_ProductName(System.String)
extern "C" void AssetStoreProduct_set_ProductName_m8_153 (AssetStoreProduct_t8_22 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::get_ProductVersion()
extern "C" String_t* AssetStoreProduct_get_ProductVersion_m8_154 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::set_ProductVersion(System.String)
extern "C" void AssetStoreProduct_set_ProductVersion_m8_155 (AssetStoreProduct_t8_22 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::get_LogoTexture()
extern "C" Texture2D_t6_33 * AssetStoreProduct_get_LogoTexture_m8_156 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::set_LogoTexture(UnityEngine.Texture2D)
extern "C" void AssetStoreProduct_set_LogoTexture_m8_157 (AssetStoreProduct_t8_22 * __this, Texture2D_t6_33 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.AssetStoreProduct::Finalize()
extern "C" void AssetStoreProduct_Finalize_m8_158 (AssetStoreProduct_t8_22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
