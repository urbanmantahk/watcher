﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Runtime.Remoting.ActivatedClientTypeEntry[]
struct ActivatedClientTypeEntryU5BU5D_t1_1723;
// System.Runtime.Remoting.ActivatedServiceTypeEntry[]
struct ActivatedServiceTypeEntryU5BU5D_t1_1724;
// System.Runtime.Remoting.WellKnownClientTypeEntry[]
struct WellKnownClientTypeEntryU5BU5D_t1_1725;
// System.Runtime.Remoting.WellKnownServiceTypeEntry[]
struct WellKnownServiceTypeEntryU5BU5D_t1_1726;
// System.Type
struct Type_t;
// System.Runtime.Remoting.ActivatedClientTypeEntry
struct ActivatedClientTypeEntry_t1_1008;
// System.Runtime.Remoting.WellKnownClientTypeEntry
struct WellKnownClientTypeEntry_t1_1039;
// System.Runtime.Remoting.ActivatedServiceTypeEntry
struct ActivatedServiceTypeEntry_t1_1010;
// System.Runtime.Remoting.WellKnownServiceTypeEntry
struct WellKnownServiceTypeEntry_t1_1041;
// System.Runtime.Remoting.ChannelData
struct ChannelData_t1_1022;
// System.Runtime.Remoting.ProviderData
struct ProviderData_t1_1023;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_CustomErrorsModes.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"

// System.Void System.Runtime.Remoting.RemotingConfiguration::.cctor()
extern "C" void RemotingConfiguration__cctor_m1_9106 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ApplicationId()
extern "C" String_t* RemotingConfiguration_get_ApplicationId_m1_9107 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ApplicationName()
extern "C" String_t* RemotingConfiguration_get_ApplicationName_m1_9108 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::set_ApplicationName(System.String)
extern "C" void RemotingConfiguration_set_ApplicationName_m1_9109 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.CustomErrorsModes System.Runtime.Remoting.RemotingConfiguration::get_CustomErrorsMode()
extern "C" int32_t RemotingConfiguration_get_CustomErrorsMode_m1_9110 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::set_CustomErrorsMode(System.Runtime.Remoting.CustomErrorsModes)
extern "C" void RemotingConfiguration_set_CustomErrorsMode_m1_9111 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.RemotingConfiguration::get_ProcessId()
extern "C" String_t* RemotingConfiguration_get_ProcessId_m1_9112 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::Configure(System.String,System.Boolean)
extern "C" void RemotingConfiguration_Configure_m1_9113 (Object_t * __this /* static, unused */, String_t* ___filename, bool ___ensureSecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::Configure(System.String)
extern "C" void RemotingConfiguration_Configure_m1_9114 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::ReadConfigFile(System.String)
extern "C" void RemotingConfiguration_ReadConfigFile_m1_9115 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::LoadDefaultDelayedChannels()
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m1_9116 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ActivatedClientTypeEntry[] System.Runtime.Remoting.RemotingConfiguration::GetRegisteredActivatedClientTypes()
extern "C" ActivatedClientTypeEntryU5BU5D_t1_1723* RemotingConfiguration_GetRegisteredActivatedClientTypes_m1_9117 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ActivatedServiceTypeEntry[] System.Runtime.Remoting.RemotingConfiguration::GetRegisteredActivatedServiceTypes()
extern "C" ActivatedServiceTypeEntryU5BU5D_t1_1724* RemotingConfiguration_GetRegisteredActivatedServiceTypes_m1_9118 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.WellKnownClientTypeEntry[] System.Runtime.Remoting.RemotingConfiguration::GetRegisteredWellKnownClientTypes()
extern "C" WellKnownClientTypeEntryU5BU5D_t1_1725* RemotingConfiguration_GetRegisteredWellKnownClientTypes_m1_9119 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.WellKnownServiceTypeEntry[] System.Runtime.Remoting.RemotingConfiguration::GetRegisteredWellKnownServiceTypes()
extern "C" WellKnownServiceTypeEntryU5BU5D_t1_1726* RemotingConfiguration_GetRegisteredWellKnownServiceTypes_m1_9120 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingConfiguration::IsActivationAllowed(System.Type)
extern "C" bool RemotingConfiguration_IsActivationAllowed_m1_9121 (Object_t * __this /* static, unused */, Type_t * ___svrType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ActivatedClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsRemotelyActivatedClientType(System.Type)
extern "C" ActivatedClientTypeEntry_t1_1008 * RemotingConfiguration_IsRemotelyActivatedClientType_m1_9122 (Object_t * __this /* static, unused */, Type_t * ___svrType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ActivatedClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsRemotelyActivatedClientType(System.String,System.String)
extern "C" ActivatedClientTypeEntry_t1_1008 * RemotingConfiguration_IsRemotelyActivatedClientType_m1_9123 (Object_t * __this /* static, unused */, String_t* ___typeName, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.WellKnownClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsWellKnownClientType(System.Type)
extern "C" WellKnownClientTypeEntry_t1_1039 * RemotingConfiguration_IsWellKnownClientType_m1_9124 (Object_t * __this /* static, unused */, Type_t * ___svrType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.WellKnownClientTypeEntry System.Runtime.Remoting.RemotingConfiguration::IsWellKnownClientType(System.String,System.String)
extern "C" WellKnownClientTypeEntry_t1_1039 * RemotingConfiguration_IsWellKnownClientType_m1_9125 (Object_t * __this /* static, unused */, String_t* ___typeName, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterActivatedClientType(System.Runtime.Remoting.ActivatedClientTypeEntry)
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m1_9126 (Object_t * __this /* static, unused */, ActivatedClientTypeEntry_t1_1008 * ___entry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterActivatedClientType(System.Type,System.String)
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m1_9127 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___appUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterActivatedServiceType(System.Runtime.Remoting.ActivatedServiceTypeEntry)
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m1_9128 (Object_t * __this /* static, unused */, ActivatedServiceTypeEntry_t1_1010 * ___entry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterActivatedServiceType(System.Type)
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m1_9129 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterWellKnownClientType(System.Type,System.String)
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m1_9130 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___objectUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterWellKnownClientType(System.Runtime.Remoting.WellKnownClientTypeEntry)
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m1_9131 (Object_t * __this /* static, unused */, WellKnownClientTypeEntry_t1_1039 * ___entry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterWellKnownServiceType(System.Type,System.String,System.Runtime.Remoting.WellKnownObjectMode)
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m1_9132 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___objectUri, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterWellKnownServiceType(System.Runtime.Remoting.WellKnownServiceTypeEntry)
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m1_9133 (Object_t * __this /* static, unused */, WellKnownServiceTypeEntry_t1_1041 * ___entry, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterChannelTemplate(System.Runtime.Remoting.ChannelData)
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m1_9134 (Object_t * __this /* static, unused */, ChannelData_t1_1022 * ___channel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterClientProviderTemplate(System.Runtime.Remoting.ProviderData)
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m1_9135 (Object_t * __this /* static, unused */, ProviderData_t1_1023 * ___prov, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterServerProviderTemplate(System.Runtime.Remoting.ProviderData)
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m1_9136 (Object_t * __this /* static, unused */, ProviderData_t1_1023 * ___prov, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterChannels(System.Collections.ArrayList,System.Boolean)
extern "C" void RemotingConfiguration_RegisterChannels_m1_9137 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ___channels, bool ___onlyDelayed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::RegisterTypes(System.Collections.ArrayList)
extern "C" void RemotingConfiguration_RegisterTypes_m1_9138 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ___types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.RemotingConfiguration::CustomErrorsEnabled(System.Boolean)
extern "C" bool RemotingConfiguration_CustomErrorsEnabled_m1_9139 (Object_t * __this /* static, unused */, bool ___isLocalRequest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.RemotingConfiguration::SetCustomErrorsMode(System.String)
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m1_9140 (Object_t * __this /* static, unused */, String_t* ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
