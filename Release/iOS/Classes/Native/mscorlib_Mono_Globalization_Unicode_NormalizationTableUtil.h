﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1_105;

#include "mscorlib_System_Object.h"

// Mono.Globalization.Unicode.NormalizationTableUtil
struct  NormalizationTableUtil_t1_118  : public Object_t
{
};
struct NormalizationTableUtil_t1_118_StaticFields{
	// Mono.Globalization.Unicode.CodePointIndexer Mono.Globalization.Unicode.NormalizationTableUtil::Prop
	CodePointIndexer_t1_105 * ___Prop_0;
	// Mono.Globalization.Unicode.CodePointIndexer Mono.Globalization.Unicode.NormalizationTableUtil::Map
	CodePointIndexer_t1_105 * ___Map_1;
	// Mono.Globalization.Unicode.CodePointIndexer Mono.Globalization.Unicode.NormalizationTableUtil::Combining
	CodePointIndexer_t1_105 * ___Combining_2;
	// Mono.Globalization.Unicode.CodePointIndexer Mono.Globalization.Unicode.NormalizationTableUtil::Composite
	CodePointIndexer_t1_105 * ___Composite_3;
	// Mono.Globalization.Unicode.CodePointIndexer Mono.Globalization.Unicode.NormalizationTableUtil::Helper
	CodePointIndexer_t1_105 * ___Helper_4;
};
