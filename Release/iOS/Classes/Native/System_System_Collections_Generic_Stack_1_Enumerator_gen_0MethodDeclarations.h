﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.String>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m3_1842(__this, ___t, method) (( void (*) (Enumerator_t3_252 *, Stack_1_t3_17 *, const MethodInfo*))Enumerator__ctor_m3_1836_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1843(__this, method) (( void (*) (Enumerator_t3_252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1837_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1844(__this, method) (( Object_t * (*) (Enumerator_t3_252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1838_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m3_1845(__this, method) (( void (*) (Enumerator_t3_252 *, const MethodInfo*))Enumerator_Dispose_m3_1839_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m3_1846(__this, method) (( bool (*) (Enumerator_t3_252 *, const MethodInfo*))Enumerator_MoveNext_m3_1840_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m3_1847(__this, method) (( String_t* (*) (Enumerator_t3_252 *, const MethodInfo*))Enumerator_get_Current_m3_1841_gshared)(__this, method)
