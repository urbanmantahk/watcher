﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.TextElementEnumerator
struct TextElementEnumerator_t1_389;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.TextElementEnumerator::.ctor(System.String,System.Int32)
extern "C" void TextElementEnumerator__ctor_m1_4493 (TextElementEnumerator_t1_389 * __this, String_t* ___str, int32_t ___startpos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.TextElementEnumerator::get_Current()
extern "C" Object_t * TextElementEnumerator_get_Current_m1_4494 (TextElementEnumerator_t1_389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextElementEnumerator::get_ElementIndex()
extern "C" int32_t TextElementEnumerator_get_ElementIndex_m1_4495 (TextElementEnumerator_t1_389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextElementEnumerator::GetTextElement()
extern "C" String_t* TextElementEnumerator_GetTextElement_m1_4496 (TextElementEnumerator_t1_389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TextElementEnumerator::MoveNext()
extern "C" bool TextElementEnumerator_MoveNext_m1_4497 (TextElementEnumerator_t1_389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TextElementEnumerator::Reset()
extern "C" void TextElementEnumerator_Reset_m1_4498 (TextElementEnumerator_t1_389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
