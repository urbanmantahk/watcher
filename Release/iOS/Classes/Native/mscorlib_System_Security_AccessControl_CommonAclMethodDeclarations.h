﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CommonAcl
struct CommonAcl_t1_1124;
// System.Security.AccessControl.GenericAce
struct GenericAce_t1_1145;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.AccessControl.CommonAcl::.ctor(System.Boolean,System.Boolean,System.Byte)
extern "C" void CommonAcl__ctor_m1_9729 (CommonAcl_t1_1124 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonAcl::.ctor(System.Boolean,System.Boolean,System.Byte,System.Int32)
extern "C" void CommonAcl__ctor_m1_9730 (CommonAcl_t1_1124 * __this, bool ___isContainer, bool ___isDS, uint8_t ___revision, int32_t ___capacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CommonAcl::get_BinaryLength()
extern "C" int32_t CommonAcl_get_BinaryLength_m1_9731 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CommonAcl::get_Count()
extern "C" int32_t CommonAcl_get_Count_m1_9732 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonAcl::get_IsCanonical()
extern "C" bool CommonAcl_get_IsCanonical_m1_9733 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonAcl::get_IsContainer()
extern "C" bool CommonAcl_get_IsContainer_m1_9734 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonAcl::get_IsDS()
extern "C" bool CommonAcl_get_IsDS_m1_9735 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.GenericAce System.Security.AccessControl.CommonAcl::get_Item(System.Int32)
extern "C" GenericAce_t1_1145 * CommonAcl_get_Item_m1_9736 (CommonAcl_t1_1124 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonAcl::set_Item(System.Int32,System.Security.AccessControl.GenericAce)
extern "C" void CommonAcl_set_Item_m1_9737 (CommonAcl_t1_1124 * __this, int32_t ___index, GenericAce_t1_1145 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Security.AccessControl.CommonAcl::get_Revision()
extern "C" uint8_t CommonAcl_get_Revision_m1_9738 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonAcl::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void CommonAcl_GetBinaryForm_m1_9739 (CommonAcl_t1_1124 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonAcl::Purge(System.Security.Principal.SecurityIdentifier)
extern "C" void CommonAcl_Purge_m1_9740 (CommonAcl_t1_1124 * __this, SecurityIdentifier_t1_1132 * ___sid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonAcl::RemoveInheritedAces()
extern "C" void CommonAcl_RemoveInheritedAces_m1_9741 (CommonAcl_t1_1124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
