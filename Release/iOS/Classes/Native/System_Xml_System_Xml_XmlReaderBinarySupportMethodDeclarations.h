﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t4_161;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlReaderBinarySupport::Reset()
extern "C" void XmlReaderBinarySupport_Reset_m4_757 (XmlReaderBinarySupport_t4_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
