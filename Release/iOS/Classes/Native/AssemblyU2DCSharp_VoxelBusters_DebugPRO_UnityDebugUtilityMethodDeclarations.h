﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DebugPRO.UnityDebugUtility
struct UnityDebugUtility_t8_173;
// UnityEngine.Application/LogCallback
struct LogCallback_t6_83;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LogType.h"

// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::.ctor()
extern "C" void UnityDebugUtility__ctor_m8_1040 (UnityDebugUtility_t8_173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::.cctor()
extern "C" void UnityDebugUtility__cctor_m8_1041 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::add_LogCallback(UnityEngine.Application/LogCallback)
extern "C" void UnityDebugUtility_add_LogCallback_m8_1042 (Object_t * __this /* static, unused */, LogCallback_t6_83 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::remove_LogCallback(UnityEngine.Application/LogCallback)
extern "C" void UnityDebugUtility_remove_LogCallback_m8_1043 (Object_t * __this /* static, unused */, LogCallback_t6_83 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::EditorUpdate()
extern "C" void UnityDebugUtility_EditorUpdate_m8_1044 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.UnityDebugUtility::HandleLog(System.String,System.String,UnityEngine.LogType)
extern "C" void UnityDebugUtility_HandleLog_m8_1045 (UnityDebugUtility_t8_173 * __this, String_t* ____message, String_t* ____stackTrace, int32_t ____logType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
