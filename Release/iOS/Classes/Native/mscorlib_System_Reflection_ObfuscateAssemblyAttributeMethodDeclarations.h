﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ObfuscateAssemblyAttribute
struct ObfuscateAssemblyAttribute_t1_624;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.ObfuscateAssemblyAttribute::.ctor(System.Boolean)
extern "C" void ObfuscateAssemblyAttribute__ctor_m1_7208 (ObfuscateAssemblyAttribute_t1_624 * __this, bool ___assemblyIsPrivate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ObfuscateAssemblyAttribute::get_AssemblyIsPrivate()
extern "C" bool ObfuscateAssemblyAttribute_get_AssemblyIsPrivate_m1_7209 (ObfuscateAssemblyAttribute_t1_624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.ObfuscateAssemblyAttribute::get_StripAfterObfuscation()
extern "C" bool ObfuscateAssemblyAttribute_get_StripAfterObfuscation_m1_7210 (ObfuscateAssemblyAttribute_t1_624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ObfuscateAssemblyAttribute::set_StripAfterObfuscation(System.Boolean)
extern "C" void ObfuscateAssemblyAttribute_set_StripAfterObfuscation_m1_7211 (ObfuscateAssemblyAttribute_t1_624 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
