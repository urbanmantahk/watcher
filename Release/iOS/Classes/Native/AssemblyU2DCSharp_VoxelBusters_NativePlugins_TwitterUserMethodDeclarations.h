﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.TwitterUser
struct TwitterUser_t8_298;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.TwitterUser::.ctor()
extern "C" void TwitterUser__ctor_m8_1730 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterUser::get_UserID()
extern "C" String_t* TwitterUser_get_UserID_m8_1731 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_UserID(System.String)
extern "C" void TwitterUser_set_UserID_m8_1732 (TwitterUser_t8_298 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterUser::get_Name()
extern "C" String_t* TwitterUser_get_Name_m8_1733 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_Name(System.String)
extern "C" void TwitterUser_set_Name_m8_1734 (TwitterUser_t8_298 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.TwitterUser::get_IsVerified()
extern "C" bool TwitterUser_get_IsVerified_m8_1735 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_IsVerified(System.Boolean)
extern "C" void TwitterUser_set_IsVerified_m8_1736 (TwitterUser_t8_298 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.TwitterUser::get_IsProtected()
extern "C" bool TwitterUser_get_IsProtected_m8_1737 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_IsProtected(System.Boolean)
extern "C" void TwitterUser_set_IsProtected_m8_1738 (TwitterUser_t8_298 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterUser::get_ProfileImageURL()
extern "C" String_t* TwitterUser_get_ProfileImageURL_m8_1739 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.TwitterUser::set_ProfileImageURL(System.String)
extern "C" void TwitterUser_set_ProfileImageURL_m8_1740 (TwitterUser_t8_298 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.TwitterUser::ToString()
extern "C" String_t* TwitterUser_ToString_m8_1741 (TwitterUser_t8_298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
