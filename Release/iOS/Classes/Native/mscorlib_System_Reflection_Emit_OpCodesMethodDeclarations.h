﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.OpCodes
struct OpCodes_t1_541;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"

// System.Void System.Reflection.Emit.OpCodes::.ctor()
extern "C" void OpCodes__ctor_m1_6271 (OpCodes_t1_541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.OpCodes::.cctor()
extern "C" void OpCodes__cctor_m1_6272 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.OpCodes::TakesSingleByteArgument(System.Reflection.Emit.OpCode)
extern "C" bool OpCodes_TakesSingleByteArgument_m1_6273 (Object_t * __this /* static, unused */, OpCode_t1_538  ___inst, const MethodInfo* method) IL2CPP_METHOD_ATTR;
