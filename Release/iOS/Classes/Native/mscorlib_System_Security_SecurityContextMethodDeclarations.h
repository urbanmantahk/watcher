﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityContext
struct SecurityContext_t1_1394;
// System.Threading.CompressedStack
struct CompressedStack_t1_1395;
// System.Threading.ContextCallback
struct ContextCallback_t1_1624;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Threading_AsyncFlowControl.h"

// System.Void System.Security.SecurityContext::.ctor()
extern "C" void SecurityContext__ctor_m1_12011 (SecurityContext_t1_1394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::.ctor(System.Security.SecurityContext)
extern "C" void SecurityContext__ctor_m1_12012 (SecurityContext_t1_1394 * __this, SecurityContext_t1_1394 * ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityContext System.Security.SecurityContext::CreateCopy()
extern "C" SecurityContext_t1_1394 * SecurityContext_CreateCopy_m1_12013 (SecurityContext_t1_1394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityContext System.Security.SecurityContext::Capture()
extern "C" SecurityContext_t1_1394 * SecurityContext_Capture_m1_12014 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityContext::get_FlowSuppressed()
extern "C" bool SecurityContext_get_FlowSuppressed_m1_12015 (SecurityContext_t1_1394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::set_FlowSuppressed(System.Boolean)
extern "C" void SecurityContext_set_FlowSuppressed_m1_12016 (SecurityContext_t1_1394 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityContext::get_WindowsIdentityFlowSuppressed()
extern "C" bool SecurityContext_get_WindowsIdentityFlowSuppressed_m1_12017 (SecurityContext_t1_1394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::set_WindowsIdentityFlowSuppressed(System.Boolean)
extern "C" void SecurityContext_set_WindowsIdentityFlowSuppressed_m1_12018 (SecurityContext_t1_1394 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Security.SecurityContext::get_CompressedStack()
extern "C" CompressedStack_t1_1395 * SecurityContext_get_CompressedStack_m1_12019 (SecurityContext_t1_1394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::set_CompressedStack(System.Threading.CompressedStack)
extern "C" void SecurityContext_set_CompressedStack_m1_12020 (SecurityContext_t1_1394 * __this, CompressedStack_t1_1395 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Security.SecurityContext::get_IdentityToken()
extern "C" IntPtr_t SecurityContext_get_IdentityToken_m1_12021 (SecurityContext_t1_1394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::set_IdentityToken(System.IntPtr)
extern "C" void SecurityContext_set_IdentityToken_m1_12022 (SecurityContext_t1_1394 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityContext::IsFlowSuppressed()
extern "C" bool SecurityContext_IsFlowSuppressed_m1_12023 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityContext::IsWindowsIdentityFlowSuppressed()
extern "C" bool SecurityContext_IsWindowsIdentityFlowSuppressed_m1_12024 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::RestoreFlow()
extern "C" void SecurityContext_RestoreFlow_m1_12025 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::Run(System.Security.SecurityContext,System.Threading.ContextCallback,System.Object)
extern "C" void SecurityContext_Run_m1_12026 (Object_t * __this /* static, unused */, SecurityContext_t1_1394 * ___securityContext, ContextCallback_t1_1624 * ___callback, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.AsyncFlowControl System.Security.SecurityContext::SuppressFlow()
extern "C" AsyncFlowControl_t1_1459  SecurityContext_SuppressFlow_m1_12027 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.AsyncFlowControl System.Security.SecurityContext::SuppressFlowWindowsIdentity()
extern "C" AsyncFlowControl_t1_1459  SecurityContext_SuppressFlowWindowsIdentity_m1_12028 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
