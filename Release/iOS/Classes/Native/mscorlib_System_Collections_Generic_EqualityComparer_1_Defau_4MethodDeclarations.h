﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t1_2168;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m1_17598_gshared (DefaultComparer_t1_2168 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1_17598(__this, method) (( void (*) (DefaultComparer_t1_2168 *, const MethodInfo*))DefaultComparer__ctor_m1_17598_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_17599_gshared (DefaultComparer_t1_2168 * __this, DateTimeOffset_t1_1527  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1_17599(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1_2168 *, DateTimeOffset_t1_1527 , const MethodInfo*))DefaultComparer_GetHashCode_m1_17599_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTimeOffset>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_17600_gshared (DefaultComparer_t1_2168 * __this, DateTimeOffset_t1_1527  ___x, DateTimeOffset_t1_1527  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m1_17600(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1_2168 *, DateTimeOffset_t1_1527 , DateTimeOffset_t1_1527 , const MethodInfo*))DefaultComparer_Equals_m1_17600_gshared)(__this, ___x, ___y, method)
