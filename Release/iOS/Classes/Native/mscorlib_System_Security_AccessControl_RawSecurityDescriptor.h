﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_GenericSecurityDescri.h"

// System.Security.AccessControl.RawSecurityDescriptor
struct  RawSecurityDescriptor_t1_1171  : public GenericSecurityDescriptor_t1_1131
{
};
