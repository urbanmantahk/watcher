﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARDESC_DES.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_ELEMDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_VARKIND.h"

// System.Runtime.InteropServices.ComTypes.VARDESC
struct  VARDESC_t1_748 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.VARDESC::memid
	int32_t ___memid_0;
	// System.String System.Runtime.InteropServices.ComTypes.VARDESC::lpstrSchema
	String_t* ___lpstrSchema_1;
	// System.Runtime.InteropServices.ComTypes.VARDESC/DESCUNION System.Runtime.InteropServices.ComTypes.VARDESC::desc
	DESCUNION_t1_747  ___desc_2;
	// System.Runtime.InteropServices.ComTypes.ELEMDESC System.Runtime.InteropServices.ComTypes.VARDESC::elemdescVar
	ELEMDESC_t1_729  ___elemdescVar_3;
	// System.Int16 System.Runtime.InteropServices.ComTypes.VARDESC::wVarFlags
	int16_t ___wVarFlags_4;
	// System.Runtime.InteropServices.ComTypes.VARKIND System.Runtime.InteropServices.ComTypes.VARDESC::varkind
	int32_t ___varkind_5;
};
// Native definition for marshalling of: System.Runtime.InteropServices.ComTypes.VARDESC
struct VARDESC_t1_748_marshaled
{
	int32_t ___memid_0;
	char* ___lpstrSchema_1;
	DESCUNION_t1_747  ___desc_2;
	ELEMDESC_t1_729_marshaled ___elemdescVar_3;
	int16_t ___wVarFlags_4;
	int32_t ___varkind_5;
};
