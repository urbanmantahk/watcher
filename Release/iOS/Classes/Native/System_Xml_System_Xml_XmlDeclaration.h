﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "System_Xml_System_Xml_XmlLinkedNode.h"

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t4_129  : public XmlLinkedNode_t4_118
{
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_6;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_7;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_8;
};
struct XmlDeclaration_t4_129_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switch$map4A
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map4A_9;
};
