﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"

// System.Void System.Comparison`1<UnityEngine.Events.PersistentCall>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m1_21862(__this, ___object, ___method, method) (( void (*) (Comparison_1_t1_2446 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1_15374_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Events.PersistentCall>::Invoke(T,T)
#define Comparison_1_Invoke_m1_21863(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t1_2446 *, PersistentCall_t6_261 *, PersistentCall_t6_261 *, const MethodInfo*))Comparison_1_Invoke_m1_15375_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Events.PersistentCall>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1_21864(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t1_2446 *, PersistentCall_t6_261 *, PersistentCall_t6_261 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m1_15376_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Events.PersistentCall>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m1_21865(__this, ___result, method) (( int32_t (*) (Comparison_1_t1_2446 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m1_15377_gshared)(__this, ___result, method)
