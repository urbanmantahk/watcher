﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ObjectCreationDelegate
struct ObjectCreationDelegate_t1_1621;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.InteropServices.ObjectCreationDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void ObjectCreationDelegate__ctor_m1_14799 (ObjectCreationDelegate_t1_1621 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.ObjectCreationDelegate::Invoke(System.IntPtr)
extern "C" IntPtr_t ObjectCreationDelegate_Invoke_m1_14800 (ObjectCreationDelegate_t1_1621 * __this, IntPtr_t ___aggregator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" IntPtr_t pinvoke_delegate_wrapper_ObjectCreationDelegate_t1_1621(Il2CppObject* delegate, IntPtr_t ___aggregator);
// System.IAsyncResult System.Runtime.InteropServices.ObjectCreationDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern "C" Object_t * ObjectCreationDelegate_BeginInvoke_m1_14801 (ObjectCreationDelegate_t1_1621 * __this, IntPtr_t ___aggregator, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.ObjectCreationDelegate::EndInvoke(System.IAsyncResult)
extern "C" IntPtr_t ObjectCreationDelegate_EndInvoke_m1_14802 (ObjectCreationDelegate_t1_1621 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
