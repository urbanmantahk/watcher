﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_AccessRule.h"
#include "mscorlib_System_Security_AccessControl_FileSystemRights.h"

// System.Security.AccessControl.FileSystemAccessRule
struct  FileSystemAccessRule_t1_1154  : public AccessRule_t1_1111
{
	// System.Security.AccessControl.FileSystemRights System.Security.AccessControl.FileSystemAccessRule::rights
	int32_t ___rights_6;
};
