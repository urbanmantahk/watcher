﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage
struct SoapLanguage_t1_979;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::.ctor()
extern "C" void SoapLanguage__ctor_m1_8774 (SoapLanguage_t1_979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::.ctor(System.String)
extern "C" void SoapLanguage__ctor_m1_8775 (SoapLanguage_t1_979 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::get_Value()
extern "C" String_t* SoapLanguage_get_Value_m1_8776 (SoapLanguage_t1_979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::set_Value(System.String)
extern "C" void SoapLanguage_set_Value_m1_8777 (SoapLanguage_t1_979 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::get_XsdType()
extern "C" String_t* SoapLanguage_get_XsdType_m1_8778 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::GetXsdType()
extern "C" String_t* SoapLanguage_GetXsdType_m1_8779 (SoapLanguage_t1_979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::Parse(System.String)
extern "C" SoapLanguage_t1_979 * SoapLanguage_Parse_m1_8780 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage::ToString()
extern "C" String_t* SoapLanguage_ToString_m1_8781 (SoapLanguage_t1_979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
