﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifSInt
struct  ExifSInt_t8_124  : public ExifProperty_t8_99
{
	// System.Int32 ExifLibrary.ExifSInt::mValue
	int32_t ___mValue_3;
};
