﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion
struct SingleFieldPromptCompletion_t8_301;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void SingleFieldPromptCompletion__ctor_m8_1752 (SingleFieldPromptCompletion_t8_301 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::Invoke(System.String,System.String)
extern "C" void SingleFieldPromptCompletion_Invoke_m8_1753 (SingleFieldPromptCompletion_t8_301 * __this, String_t* ____buttonPressed, String_t* ____inputText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SingleFieldPromptCompletion_t8_301(Il2CppObject* delegate, String_t* ____buttonPressed, String_t* ____inputText);
// System.IAsyncResult VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * SingleFieldPromptCompletion_BeginInvoke_m8_1754 (SingleFieldPromptCompletion_t8_301 * __this, String_t* ____buttonPressed, String_t* ____inputText, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UI/SingleFieldPromptCompletion::EndInvoke(System.IAsyncResult)
extern "C" void SingleFieldPromptCompletion_EndInvoke_m8_1755 (SingleFieldPromptCompletion_t8_301 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
