﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t4_121;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;

#include "System_Xml_System_Xml_XmlLinkedNode.h"

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t4_134  : public XmlLinkedNode_t4_118
{
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t4_121 * ___entities_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t4_121 * ___notations_7;
	// Mono.Xml.DTDObjectModel System.Xml.XmlDocumentType::dtd
	DTDObjectModel_t4_82 * ___dtd_8;
};
