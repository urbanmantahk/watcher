﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.DerivedType
struct DerivedType_t1_489;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1_1667;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_1671;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Runtime.InteropServices.StructLayoutAttribute
struct StructLayoutAttribute_t1_64;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.Module
struct Module_t1_495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_InterfaceMapping.h"
#include "mscorlib_System_Reflection_GenericParameterAttributes.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_RuntimeTypeHandle.h"

// System.Void System.Reflection.Emit.DerivedType::.ctor(System.Type)
extern "C" void DerivedType__ctor_m1_5605 (DerivedType_t1_489 * __this, Type_t * ___elementType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.DerivedType::create_unmanaged_type(System.Type)
extern "C" void DerivedType_create_unmanaged_type_m1_5606 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::GetInterface(System.String,System.Boolean)
extern "C" Type_t * DerivedType_GetInterface_m1_5607 (DerivedType_t1_489 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.DerivedType::GetInterfaces()
extern "C" TypeU5BU5D_t1_31* DerivedType_GetInterfaces_m1_5608 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::GetElementType()
extern "C" Type_t * DerivedType_GetElementType_m1_5609 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.Emit.DerivedType::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C" EventInfo_t * DerivedType_GetEvent_m1_5610 (DerivedType_t1_489 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.Emit.DerivedType::GetEvents(System.Reflection.BindingFlags)
extern "C" EventInfoU5BU5D_t1_1667* DerivedType_GetEvents_m1_5611 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Emit.DerivedType::GetField(System.String,System.Reflection.BindingFlags)
extern "C" FieldInfo_t * DerivedType_GetField_m1_5612 (DerivedType_t1_489 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.Emit.DerivedType::GetFields(System.Reflection.BindingFlags)
extern "C" FieldInfoU5BU5D_t1_1668* DerivedType_GetFields_m1_5613 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.Emit.DerivedType::GetMembers(System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* DerivedType_GetMembers_m1_5614 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.DerivedType::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * DerivedType_GetMethodImpl_m1_5615 (DerivedType_t1_489 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.DerivedType::GetMethods(System.Reflection.BindingFlags)
extern "C" MethodInfoU5BU5D_t1_603* DerivedType_GetMethods_m1_5616 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::GetNestedType(System.String,System.Reflection.BindingFlags)
extern "C" Type_t * DerivedType_GetNestedType_m1_5617 (DerivedType_t1_489 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.DerivedType::GetNestedTypes(System.Reflection.BindingFlags)
extern "C" TypeU5BU5D_t1_31* DerivedType_GetNestedTypes_m1_5618 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Reflection.Emit.DerivedType::GetProperties(System.Reflection.BindingFlags)
extern "C" PropertyInfoU5BU5D_t1_1670* DerivedType_GetProperties_m1_5619 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.Emit.DerivedType::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C" PropertyInfo_t * DerivedType_GetPropertyImpl_m1_5620 (DerivedType_t1_489 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, Type_t * ___returnType, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.DerivedType::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" ConstructorInfo_t1_478 * DerivedType_GetConstructorImpl_m1_5621 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.Emit.DerivedType::GetAttributeFlagsImpl()
extern "C" int32_t DerivedType_GetAttributeFlagsImpl_m1_5622 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::HasElementTypeImpl()
extern "C" bool DerivedType_HasElementTypeImpl_m1_5623 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsArrayImpl()
extern "C" bool DerivedType_IsArrayImpl_m1_5624 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsByRefImpl()
extern "C" bool DerivedType_IsByRefImpl_m1_5625 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsCOMObjectImpl()
extern "C" bool DerivedType_IsCOMObjectImpl_m1_5626 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsPointerImpl()
extern "C" bool DerivedType_IsPointerImpl_m1_5627 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsPrimitiveImpl()
extern "C" bool DerivedType_IsPrimitiveImpl_m1_5628 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.DerivedType::GetConstructors(System.Reflection.BindingFlags)
extern "C" ConstructorInfoU5BU5D_t1_1671* DerivedType_GetConstructors_m1_5629 (DerivedType_t1_489 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.DerivedType::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C" Object_t * DerivedType_InvokeMember_m1_5630 (DerivedType_t1_489 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1_585 * ___binder, Object_t * ___target, ObjectU5BU5D_t1_272* ___args, ParameterModifierU5BU5D_t1_1669* ___modifiers, CultureInfo_t1_277 * ___culture, StringU5BU5D_t1_238* ___namedParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.InterfaceMapping System.Reflection.Emit.DerivedType::GetInterfaceMap(System.Type)
extern "C" InterfaceMapping_t1_602  DerivedType_GetInterfaceMap_m1_5631 (DerivedType_t1_489 * __this, Type_t * ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsInstanceOfType(System.Object)
extern "C" bool DerivedType_IsInstanceOfType_m1_5632 (DerivedType_t1_489 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsAssignableFrom(System.Type)
extern "C" bool DerivedType_IsAssignableFrom_m1_5633 (DerivedType_t1_489 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::get_ContainsGenericParameters()
extern "C" bool DerivedType_get_ContainsGenericParameters_m1_5634 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::MakeGenericType(System.Type[])
extern "C" Type_t * DerivedType_MakeGenericType_m1_5635 (DerivedType_t1_489 * __this, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::MakeArrayType()
extern "C" Type_t * DerivedType_MakeArrayType_m1_5636 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::MakeArrayType(System.Int32)
extern "C" Type_t * DerivedType_MakeArrayType_m1_5637 (DerivedType_t1_489 * __this, int32_t ___rank, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::MakeByRefType()
extern "C" Type_t * DerivedType_MakeByRefType_m1_5638 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::MakePointerType()
extern "C" Type_t * DerivedType_MakePointerType_m1_5639 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DerivedType::ToString()
extern "C" String_t* DerivedType_ToString_m1_5640 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.GenericParameterAttributes System.Reflection.Emit.DerivedType::get_GenericParameterAttributes()
extern "C" int32_t DerivedType_get_GenericParameterAttributes_m1_5641 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.StructLayoutAttribute System.Reflection.Emit.DerivedType::get_StructLayoutAttribute()
extern "C" StructLayoutAttribute_t1_64 * DerivedType_get_StructLayoutAttribute_m1_5642 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Emit.DerivedType::get_Assembly()
extern "C" Assembly_t1_467 * DerivedType_get_Assembly_m1_5643 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DerivedType::get_AssemblyQualifiedName()
extern "C" String_t* DerivedType_get_AssemblyQualifiedName_m1_5644 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DerivedType::get_FullName()
extern "C" String_t* DerivedType_get_FullName_m1_5645 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DerivedType::get_Name()
extern "C" String_t* DerivedType_get_Name_m1_5646 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Emit.DerivedType::get_GUID()
extern "C" Guid_t1_319  DerivedType_get_GUID_m1_5647 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.DerivedType::get_Module()
extern "C" Module_t1_495 * DerivedType_get_Module_m1_5648 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.DerivedType::get_Namespace()
extern "C" String_t* DerivedType_get_Namespace_m1_5649 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.Reflection.Emit.DerivedType::get_TypeHandle()
extern "C" RuntimeTypeHandle_t1_30  DerivedType_get_TypeHandle_m1_5650 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.DerivedType::get_UnderlyingSystemType()
extern "C" Type_t * DerivedType_get_UnderlyingSystemType_m1_5651 (DerivedType_t1_489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.DerivedType::IsDefined(System.Type,System.Boolean)
extern "C" bool DerivedType_IsDefined_m1_5652 (DerivedType_t1_489 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.DerivedType::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* DerivedType_GetCustomAttributes_m1_5653 (DerivedType_t1_489 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.DerivedType::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* DerivedType_GetCustomAttributes_m1_5654 (DerivedType_t1_489 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
