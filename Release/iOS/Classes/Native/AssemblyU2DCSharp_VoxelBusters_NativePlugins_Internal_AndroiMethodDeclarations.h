﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.AndroidAddressBookContact
struct AndroidAddressBookContact_t8_199;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.AndroidAddressBookContact::.ctor(System.Collections.IDictionary)
extern "C" void AndroidAddressBookContact__ctor_m8_1155 (AndroidAddressBookContact_t8_199 * __this, Object_t * ____contactInfoJsontDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
