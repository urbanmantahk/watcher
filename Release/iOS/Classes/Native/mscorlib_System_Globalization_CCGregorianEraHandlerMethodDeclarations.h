﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCGregorianEraHandler
struct CCGregorianEraHandler_t1_353;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.CCGregorianEraHandler::.ctor()
extern "C" void CCGregorianEraHandler__ctor_m1_3799 (CCGregorianEraHandler_t1_353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.CCGregorianEraHandler::get_Eras()
extern "C" Int32U5BU5D_t1_275* CCGregorianEraHandler_get_Eras_m1_3800 (CCGregorianEraHandler_t1_353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCGregorianEraHandler::appendEra(System.Int32,System.Int32,System.Int32)
extern "C" void CCGregorianEraHandler_appendEra_m1_3801 (CCGregorianEraHandler_t1_353 * __this, int32_t ___nr, int32_t ___rd_start, int32_t ___rd_end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCGregorianEraHandler::appendEra(System.Int32,System.Int32)
extern "C" void CCGregorianEraHandler_appendEra_m1_3802 (CCGregorianEraHandler_t1_353 * __this, int32_t ___nr, int32_t ___rd_start, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianEraHandler::GregorianYear(System.Int32,System.Int32)
extern "C" int32_t CCGregorianEraHandler_GregorianYear_m1_3803 (CCGregorianEraHandler_t1_353 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianEraHandler::EraYear(System.Int32&,System.Int32)
extern "C" int32_t CCGregorianEraHandler_EraYear_m1_3804 (CCGregorianEraHandler_t1_353 * __this, int32_t* ___era, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCGregorianEraHandler::CheckDateTime(System.DateTime)
extern "C" void CCGregorianEraHandler_CheckDateTime_m1_3805 (CCGregorianEraHandler_t1_353 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCGregorianEraHandler::ValidDate(System.Int32)
extern "C" bool CCGregorianEraHandler_ValidDate_m1_3806 (CCGregorianEraHandler_t1_353 * __this, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCGregorianEraHandler::ValidEra(System.Int32)
extern "C" bool CCGregorianEraHandler_ValidEra_m1_3807 (CCGregorianEraHandler_t1_353 * __this, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
