﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.Plist
struct Plist_t8_51;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.Utility.Demo.PlistDemo
struct  PlistDemo_t8_50  : public MonoBehaviour_t6_91
{
	// VoxelBusters.Utility.Plist VoxelBusters.Utility.Demo.PlistDemo::m_plist
	Plist_t8_51 * ___m_plist_2;
	// System.String VoxelBusters.Utility.Demo.PlistDemo::m_input
	String_t* ___m_input_3;
	// System.String VoxelBusters.Utility.Demo.PlistDemo::m_keyPath
	String_t* ___m_keyPath_4;
	// System.String VoxelBusters.Utility.Demo.PlistDemo::m_result
	String_t* ___m_result_5;
	// System.Collections.ArrayList VoxelBusters.Utility.Demo.PlistDemo::m_GUIButtons
	ArrayList_t1_170 * ___m_GUIButtons_6;
};
