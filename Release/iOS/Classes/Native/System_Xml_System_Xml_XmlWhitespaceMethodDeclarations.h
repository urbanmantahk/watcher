﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlWhitespace
struct XmlWhitespace_t4_186;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeType.h"

// System.Void System.Xml.XmlWhitespace::.ctor(System.String,System.Xml.XmlDocument)
extern "C" void XmlWhitespace__ctor_m4_1010 (XmlWhitespace_t4_186 * __this, String_t* ___strData, XmlDocument_t4_123 * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWhitespace::get_LocalName()
extern "C" String_t* XmlWhitespace_get_LocalName_m4_1011 (XmlWhitespace_t4_186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWhitespace::get_Name()
extern "C" String_t* XmlWhitespace_get_Name_m4_1012 (XmlWhitespace_t4_186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.XmlWhitespace::get_NodeType()
extern "C" int32_t XmlWhitespace_get_NodeType_m4_1013 (XmlWhitespace_t4_186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlWhitespace::get_Value()
extern "C" String_t* XmlWhitespace_get_Value_m4_1014 (XmlWhitespace_t4_186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlWhitespace::set_Value(System.String)
extern "C" void XmlWhitespace_set_Value_m4_1015 (XmlWhitespace_t4_186 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlWhitespace::get_ParentNode()
extern "C" XmlNode_t4_116 * XmlWhitespace_get_ParentNode_m4_1016 (XmlWhitespace_t4_186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlWhitespace::CloneNode(System.Boolean)
extern "C" XmlNode_t4_116 * XmlWhitespace_CloneNode_m4_1017 (XmlWhitespace_t4_186 * __this, bool ___deep, const MethodInfo* method) IL2CPP_METHOD_ATTR;
