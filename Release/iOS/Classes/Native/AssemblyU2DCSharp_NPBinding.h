﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.NotificationService
struct NotificationService_t8_261;
// VoxelBusters.NativePlugins.UI
struct UI_t8_303;
// VoxelBusters.NativePlugins.Utility
struct Utility_t8_306;

#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatte.h"

// NPBinding
struct  NPBinding_t8_333  : public SingletonPattern_1_t8_334
{
};
struct NPBinding_t8_333_StaticFields{
	// VoxelBusters.NativePlugins.NotificationService NPBinding::notificationService
	NotificationService_t8_261 * ___notificationService_9;
	// VoxelBusters.NativePlugins.UI NPBinding::userInterface
	UI_t8_303 * ___userInterface_10;
	// VoxelBusters.NativePlugins.Utility NPBinding::utility
	Utility_t8_306 * ___utility_11;
};
