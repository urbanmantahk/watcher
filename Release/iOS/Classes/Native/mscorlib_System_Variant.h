﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_BRECORD.h"

// System.Variant
#pragma pack(push, tp, 1)
struct  Variant_t1_1617 
{
	union
	{
		// System.Int16 System.Variant::vt
		struct
		{
			int16_t ___vt_0;
		};
		// System.UInt16 System.Variant::wReserved1
		struct
		{
			char ___wReserved1_1_OffsetPadding[2];
			uint16_t ___wReserved1_1;
		};
		// System.UInt16 System.Variant::wReserved2
		struct
		{
			char ___wReserved2_2_OffsetPadding[4];
			uint16_t ___wReserved2_2;
		};
		// System.UInt16 System.Variant::wReserved3
		struct
		{
			char ___wReserved3_3_OffsetPadding[6];
			uint16_t ___wReserved3_3;
		};
		// System.Int64 System.Variant::llVal
		struct
		{
			char ___llVal_4_OffsetPadding[8];
			int64_t ___llVal_4;
		};
		// System.Int32 System.Variant::lVal
		struct
		{
			char ___lVal_5_OffsetPadding[8];
			int32_t ___lVal_5;
		};
		// System.Byte System.Variant::bVal
		struct
		{
			char ___bVal_6_OffsetPadding[8];
			uint8_t ___bVal_6;
		};
		// System.Int16 System.Variant::iVal
		struct
		{
			char ___iVal_7_OffsetPadding[8];
			int16_t ___iVal_7;
		};
		// System.Single System.Variant::fltVal
		struct
		{
			char ___fltVal_8_OffsetPadding[8];
			float ___fltVal_8;
		};
		// System.Double System.Variant::dblVal
		struct
		{
			char ___dblVal_9_OffsetPadding[8];
			double ___dblVal_9;
		};
		// System.Int16 System.Variant::boolVal
		struct
		{
			char ___boolVal_10_OffsetPadding[8];
			int16_t ___boolVal_10;
		};
		// System.IntPtr System.Variant::bstrVal
		struct
		{
			char ___bstrVal_11_OffsetPadding[8];
			IntPtr_t ___bstrVal_11;
		};
		// System.SByte System.Variant::cVal
		struct
		{
			char ___cVal_12_OffsetPadding[8];
			int8_t ___cVal_12;
		};
		// System.UInt16 System.Variant::uiVal
		struct
		{
			char ___uiVal_13_OffsetPadding[8];
			uint16_t ___uiVal_13;
		};
		// System.UInt32 System.Variant::ulVal
		struct
		{
			char ___ulVal_14_OffsetPadding[8];
			uint32_t ___ulVal_14;
		};
		// System.UInt64 System.Variant::ullVal
		struct
		{
			char ___ullVal_15_OffsetPadding[8];
			uint64_t ___ullVal_15;
		};
		// System.Int32 System.Variant::intVal
		struct
		{
			char ___intVal_16_OffsetPadding[8];
			int32_t ___intVal_16;
		};
		// System.UInt32 System.Variant::uintVal
		struct
		{
			char ___uintVal_17_OffsetPadding[8];
			uint32_t ___uintVal_17;
		};
		// System.IntPtr System.Variant::pdispVal
		struct
		{
			char ___pdispVal_18_OffsetPadding[8];
			IntPtr_t ___pdispVal_18;
		};
		// System.BRECORD System.Variant::bRecord
		struct
		{
			char ___bRecord_19_OffsetPadding[8];
			BRECORD_t1_1618  ___bRecord_19;
		};
	};
};
#pragma pack(pop, tp)
