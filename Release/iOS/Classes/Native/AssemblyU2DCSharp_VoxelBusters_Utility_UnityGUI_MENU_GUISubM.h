﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t6_97;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_UnityGUI_MENU_GUIMenu.h"

// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu
struct  GUISubMenu_t8_143  : public GUIMenuBase_t8_141
{
	// UnityEngine.GameObject VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::m_gameObject
	GameObject_t6_97 * ___m_gameObject_9;
};
