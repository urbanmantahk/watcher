﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_ValueType.h"

// System.Runtime.InteropServices.ArrayWithOffset
struct  ArrayWithOffset_t1_752 
{
	// System.Object System.Runtime.InteropServices.ArrayWithOffset::array
	Object_t * ___array_0;
	// System.Int32 System.Runtime.InteropServices.ArrayWithOffset::offset
	int32_t ___offset_1;
};
