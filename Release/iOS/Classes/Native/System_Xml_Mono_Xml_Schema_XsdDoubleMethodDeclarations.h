﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t4_36;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdDouble::.ctor()
extern "C" void XsdDouble__ctor_m4_51 (XsdDouble_t4_36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
