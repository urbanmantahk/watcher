﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_AddressBookCont_0.h"

// VoxelBusters.NativePlugins.Internal.AndroidAddressBookContact
struct  AndroidAddressBookContact_t8_199  : public AddressBookContact_t8_198
{
};
