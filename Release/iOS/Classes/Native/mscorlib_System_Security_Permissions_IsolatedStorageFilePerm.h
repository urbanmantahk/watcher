﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_IsolatedStoragePermissi.h"

// System.Security.Permissions.IsolatedStorageFilePermission
struct  IsolatedStorageFilePermission_t1_1284  : public IsolatedStoragePermission_t1_1285
{
};
