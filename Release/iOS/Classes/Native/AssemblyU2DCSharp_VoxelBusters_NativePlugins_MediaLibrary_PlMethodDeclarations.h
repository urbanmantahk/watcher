﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion
struct PlayVideoCompletion_t8_243;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePlayVideoFinis.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void PlayVideoCompletion__ctor_m8_1359 (PlayVideoCompletion_t8_243 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::Invoke(VoxelBusters.NativePlugins.ePlayVideoFinishReason)
extern "C" void PlayVideoCompletion_Invoke_m8_1360 (PlayVideoCompletion_t8_243 * __this, int32_t ____reason, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_PlayVideoCompletion_t8_243(Il2CppObject* delegate, int32_t ____reason);
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::BeginInvoke(VoxelBusters.NativePlugins.ePlayVideoFinishReason,System.AsyncCallback,System.Object)
extern "C" Object_t * PlayVideoCompletion_BeginInvoke_m8_1361 (PlayVideoCompletion_t8_243 * __this, int32_t ____reason, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/PlayVideoCompletion::EndInvoke(System.IAsyncResult)
extern "C" void PlayVideoCompletion_EndInvoke_m8_1362 (PlayVideoCompletion_t8_243 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
