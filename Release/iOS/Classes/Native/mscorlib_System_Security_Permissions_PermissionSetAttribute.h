﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.PermissionSetAttribute
struct  PermissionSetAttribute_t1_1294  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.String System.Security.Permissions.PermissionSetAttribute::file
	String_t* ___file_2;
	// System.String System.Security.Permissions.PermissionSetAttribute::name
	String_t* ___name_3;
	// System.Boolean System.Security.Permissions.PermissionSetAttribute::isUnicodeEncoded
	bool ___isUnicodeEncoded_4;
	// System.String System.Security.Permissions.PermissionSetAttribute::xml
	String_t* ___xml_5;
	// System.String System.Security.Permissions.PermissionSetAttribute::hex
	String_t* ___hex_6;
};
