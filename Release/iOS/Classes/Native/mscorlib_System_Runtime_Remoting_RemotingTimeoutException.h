﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_Remoting_RemotingException.h"

// System.Runtime.Remoting.RemotingTimeoutException
struct  RemotingTimeoutException_t1_1029  : public RemotingException_t1_1025
{
};
