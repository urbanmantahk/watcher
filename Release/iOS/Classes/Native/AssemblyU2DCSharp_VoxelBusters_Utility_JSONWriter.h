﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.StringBuilder
struct StringBuilder_t1_247;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.JSONWriter
struct  JSONWriter_t8_58  : public Object_t
{
	// System.Text.StringBuilder VoxelBusters.Utility.JSONWriter::<StringBuilder>k__BackingField
	StringBuilder_t1_247 * ___U3CStringBuilderU3Ek__BackingField_1;
};
