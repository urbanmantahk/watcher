﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.DownloadTextureDemo
struct DownloadTextureDemo_t8_153;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.DownloadTextureDemo::.ctor()
extern "C" void DownloadTextureDemo__ctor_m8_883 (DownloadTextureDemo_t8_153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextureDemo::StartDownload()
extern "C" void DownloadTextureDemo_StartDownload_m8_884 (DownloadTextureDemo_t8_153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextureDemo::<StartDownload>m__4(UnityEngine.Texture2D,System.String)
extern "C" void DownloadTextureDemo_U3CStartDownloadU3Em__4_m8_885 (DownloadTextureDemo_t8_153 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
