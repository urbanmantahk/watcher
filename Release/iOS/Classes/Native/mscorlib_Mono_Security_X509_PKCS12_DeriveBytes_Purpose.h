﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytes_Purpose.h"

// Mono.Security.X509.PKCS12/DeriveBytes/Purpose
struct  Purpose_t1_185 
{
	// System.Int32 Mono.Security.X509.PKCS12/DeriveBytes/Purpose::value__
	int32_t ___value___1;
};
