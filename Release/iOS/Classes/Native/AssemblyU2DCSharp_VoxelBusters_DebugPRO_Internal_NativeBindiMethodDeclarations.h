﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DebugPRO.Internal.NativeBinding
struct NativeBinding_t8_166;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void VoxelBusters.DebugPRO.Internal.NativeBinding::.ctor()
extern "C" void NativeBinding__ctor_m8_969 (NativeBinding_t8_166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.NativeBinding::debugProLogMessage(System.String,VoxelBusters.DebugPRO.Internal.eConsoleLogType,System.String)
extern "C" void NativeBinding_debugProLogMessage_m8_970 (Object_t * __this /* static, unused */, String_t* ____message, int32_t ____type, String_t* ____stackTrace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.NativeBinding::Log(VoxelBusters.DebugPRO.Internal.ConsoleLog)
extern "C" void NativeBinding_Log_m8_971 (Object_t * __this /* static, unused */, ConsoleLog_t8_170  ____log, const MethodInfo* method) IL2CPP_METHOD_ATTR;
