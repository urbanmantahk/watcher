﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_TwitterSession.h"

// VoxelBusters.NativePlugins.Internal.AndroidTwitterSession
struct  AndroidTwitterSession_t8_294  : public TwitterSession_t8_295
{
};
