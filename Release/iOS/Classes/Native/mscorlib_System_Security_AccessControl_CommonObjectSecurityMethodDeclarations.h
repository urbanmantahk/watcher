﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CommonObjectSecurity
struct CommonObjectSecurity_t1_1126;
// System.Security.AccessControl.AuthorizationRuleCollection
struct AuthorizationRuleCollection_t1_1121;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlModifica.h"

// System.Void System.Security.AccessControl.CommonObjectSecurity::.ctor(System.Boolean)
extern "C" void CommonObjectSecurity__ctor_m1_9742 (CommonObjectSecurity_t1_1126 * __this, bool ___isContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.CommonObjectSecurity::GetAccessRules(System.Boolean,System.Boolean,System.Type)
extern "C" AuthorizationRuleCollection_t1_1121 * CommonObjectSecurity_GetAccessRules_m1_9743 (CommonObjectSecurity_t1_1126 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuthorizationRuleCollection System.Security.AccessControl.CommonObjectSecurity::GetAuditRules(System.Boolean,System.Boolean,System.Type)
extern "C" AuthorizationRuleCollection_t1_1121 * CommonObjectSecurity_GetAuditRules_m1_9744 (CommonObjectSecurity_t1_1126 * __this, bool ___includeExplicit, bool ___includeInherited, Type_t * ___targetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::AddAccessRule(System.Security.AccessControl.AccessRule)
extern "C" void CommonObjectSecurity_AddAccessRule_m1_9745 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::RemoveAccessRule(System.Security.AccessControl.AccessRule)
extern "C" bool CommonObjectSecurity_RemoveAccessRule_m1_9746 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAccessRuleAll(System.Security.AccessControl.AccessRule)
extern "C" void CommonObjectSecurity_RemoveAccessRuleAll_m1_9747 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.AccessRule)
extern "C" void CommonObjectSecurity_RemoveAccessRuleSpecific_m1_9748 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::ResetAccessRule(System.Security.AccessControl.AccessRule)
extern "C" void CommonObjectSecurity_ResetAccessRule_m1_9749 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::SetAccessRule(System.Security.AccessControl.AccessRule)
extern "C" void CommonObjectSecurity_SetAccessRule_m1_9750 (CommonObjectSecurity_t1_1126 * __this, AccessRule_t1_1111 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::ModifyAccess(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AccessRule,System.Boolean&)
extern "C" bool CommonObjectSecurity_ModifyAccess_m1_9751 (CommonObjectSecurity_t1_1126 * __this, int32_t ___modification, AccessRule_t1_1111 * ___rule, bool* ___modified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::AddAuditRule(System.Security.AccessControl.AuditRule)
extern "C" void CommonObjectSecurity_AddAuditRule_m1_9752 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::RemoveAuditRule(System.Security.AccessControl.AuditRule)
extern "C" bool CommonObjectSecurity_RemoveAuditRule_m1_9753 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAuditRuleAll(System.Security.AccessControl.AuditRule)
extern "C" void CommonObjectSecurity_RemoveAuditRuleAll_m1_9754 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.AuditRule)
extern "C" void CommonObjectSecurity_RemoveAuditRuleSpecific_m1_9755 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CommonObjectSecurity::SetAuditRule(System.Security.AccessControl.AuditRule)
extern "C" void CommonObjectSecurity_SetAuditRule_m1_9756 (CommonObjectSecurity_t1_1126 * __this, AuditRule_t1_1119 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.CommonObjectSecurity::ModifyAudit(System.Security.AccessControl.AccessControlModification,System.Security.AccessControl.AuditRule,System.Boolean&)
extern "C" bool CommonObjectSecurity_ModifyAudit_m1_9757 (CommonObjectSecurity_t1_1126 * __this, int32_t ___modification, AuditRule_t1_1119 * ___rule, bool* ___modified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
