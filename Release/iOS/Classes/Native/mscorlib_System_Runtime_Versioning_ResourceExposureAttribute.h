﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_Versioning_ResourceScope.h"

// System.Runtime.Versioning.ResourceExposureAttribute
struct  ResourceExposureAttribute_t1_1101  : public Attribute_t1_2
{
	// System.Runtime.Versioning.ResourceScope System.Runtime.Versioning.ResourceExposureAttribute::exposure
	int32_t ___exposure_0;
};
