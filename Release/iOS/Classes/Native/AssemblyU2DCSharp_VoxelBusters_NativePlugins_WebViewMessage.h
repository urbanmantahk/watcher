﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1_1839;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.WebViewMessage
struct  WebViewMessage_t8_314  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.WebViewMessage::<SchemeName>k__BackingField
	String_t* ___U3CSchemeNameU3Ek__BackingField_0;
	// System.String VoxelBusters.NativePlugins.WebViewMessage::<Host>k__BackingField
	String_t* ___U3CHostU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> VoxelBusters.NativePlugins.WebViewMessage::<Arguments>k__BackingField
	Dictionary_2_t1_1839 * ___U3CArgumentsU3Ek__BackingField_2;
};
