﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.PlatformID
struct PlatformID_t8_331;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID_ePla.h"

// System.Void VoxelBusters.NativePlugins.PlatformID::.ctor()
extern "C" void PlatformID__ctor_m8_1918 (PlatformID_t8_331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.PlatformID::.ctor(VoxelBusters.NativePlugins.PlatformID/ePlatform,System.String)
extern "C" void PlatformID__ctor_m8_1919 (PlatformID_t8_331 * __this, int32_t ____platform, String_t* ____identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.PlatformID/ePlatform VoxelBusters.NativePlugins.PlatformID::get_Platform()
extern "C" int32_t PlatformID_get_Platform_m8_1920 (PlatformID_t8_331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.PlatformID::set_Platform(VoxelBusters.NativePlugins.PlatformID/ePlatform)
extern "C" void PlatformID_set_Platform_m8_1921 (PlatformID_t8_331 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.PlatformID::get_Value()
extern "C" String_t* PlatformID_get_Value_m8_1922 (PlatformID_t8_331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.PlatformID::set_Value(System.String)
extern "C" void PlatformID_set_Value_m8_1923 (PlatformID_t8_331 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.PlatformID VoxelBusters.NativePlugins.PlatformID::IOS(System.String)
extern "C" PlatformID_t8_331 * PlatformID_IOS_m8_1924 (Object_t * __this /* static, unused */, String_t* ____identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.PlatformID VoxelBusters.NativePlugins.PlatformID::Android(System.String)
extern "C" PlatformID_t8_331 * PlatformID_Android_m8_1925 (Object_t * __this /* static, unused */, String_t* ____identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.PlatformID VoxelBusters.NativePlugins.PlatformID::Editor(System.String)
extern "C" PlatformID_t8_331 * PlatformID_Editor_m8_1926 (Object_t * __this /* static, unused */, String_t* ____identifier, const MethodInfo* method) IL2CPP_METHOD_ATTR;
