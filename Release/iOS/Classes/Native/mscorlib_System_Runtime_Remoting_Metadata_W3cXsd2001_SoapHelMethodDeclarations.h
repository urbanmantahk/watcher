﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHelper
struct SoapHelper_t1_973;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Remoting.Metadata.W3cXsd2001.ISoapXsd
struct ISoapXsd_t1_1719;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHelper::.ctor()
extern "C" void SoapHelper__ctor_m1_8728 (SoapHelper_t1_973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHelper::GetException(System.Runtime.Remoting.Metadata.W3cXsd2001.ISoapXsd,System.String)
extern "C" Exception_t1_33 * SoapHelper_GetException_m1_8729 (Object_t * __this /* static, unused */, Object_t * ___type, String_t* ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHelper::Normalize(System.String)
extern "C" String_t* SoapHelper_Normalize_m1_8730 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
