﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_PropertyAttribute.h"

// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t6_239  : public PropertyAttribute_t6_236
{
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;
};
