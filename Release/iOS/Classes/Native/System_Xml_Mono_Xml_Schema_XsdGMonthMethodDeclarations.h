﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t4_51;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdGMonth::.ctor()
extern "C" void XsdGMonth__ctor_m4_76 (XsdGMonth_t4_51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
