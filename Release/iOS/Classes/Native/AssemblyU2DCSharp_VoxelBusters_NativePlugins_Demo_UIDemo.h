﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDemoBase.h"

// VoxelBusters.NativePlugins.Demo.UIDemo
struct  UIDemo_t8_188  : public NPDemoBase_t8_174
{
	// System.String VoxelBusters.NativePlugins.Demo.UIDemo::m_title
	String_t* ___m_title_13;
	// System.String VoxelBusters.NativePlugins.Demo.UIDemo::m_message
	String_t* ___m_message_14;
	// System.String VoxelBusters.NativePlugins.Demo.UIDemo::m_usernamePlaceHolder
	String_t* ___m_usernamePlaceHolder_15;
	// System.String VoxelBusters.NativePlugins.Demo.UIDemo::m_passwordPlaceHolder
	String_t* ___m_passwordPlaceHolder_16;
	// System.String VoxelBusters.NativePlugins.Demo.UIDemo::m_button
	String_t* ___m_button_17;
	// System.String[] VoxelBusters.NativePlugins.Demo.UIDemo::m_buttons
	StringU5BU5D_t1_238* ___m_buttons_18;
};
