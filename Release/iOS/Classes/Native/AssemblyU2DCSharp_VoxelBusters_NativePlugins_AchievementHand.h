﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t8_224;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.AchievementHandler
struct  AchievementHandler_t8_327  : public Object_t
{
};
struct AchievementHandler_t8_327_StaticFields{
	// VoxelBusters.NativePlugins.AchievementDescription[] VoxelBusters.NativePlugins.AchievementHandler::cachedAchievementDescriptionList
	AchievementDescriptionU5BU5D_t8_224* ___cachedAchievementDescriptionList_0;
	// System.Int32 VoxelBusters.NativePlugins.AchievementHandler::cachedAchievementDescriptionCount
	int32_t ___cachedAchievementDescriptionCount_1;
};
