﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.EncoderFallbackException
struct EncoderFallbackException_t1_1432;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.EncoderFallbackException::.ctor()
extern "C" void EncoderFallbackException__ctor_m1_12278 (EncoderFallbackException_t1_1432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallbackException::.ctor(System.String)
extern "C" void EncoderFallbackException__ctor_m1_12279 (EncoderFallbackException_t1_1432 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallbackException::.ctor(System.String,System.Exception)
extern "C" void EncoderFallbackException__ctor_m1_12280 (EncoderFallbackException_t1_1432 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallbackException::.ctor(System.Char,System.Int32)
extern "C" void EncoderFallbackException__ctor_m1_12281 (EncoderFallbackException_t1_1432 * __this, uint16_t ___charUnknown, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallbackException::.ctor(System.Char,System.Char,System.Int32)
extern "C" void EncoderFallbackException__ctor_m1_12282 (EncoderFallbackException_t1_1432 * __this, uint16_t ___charUnknownHigh, uint16_t ___charUnknownLow, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Text.EncoderFallbackException::get_CharUnknown()
extern "C" uint16_t EncoderFallbackException_get_CharUnknown_m1_12283 (EncoderFallbackException_t1_1432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Text.EncoderFallbackException::get_CharUnknownHigh()
extern "C" uint16_t EncoderFallbackException_get_CharUnknownHigh_m1_12284 (EncoderFallbackException_t1_1432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Text.EncoderFallbackException::get_CharUnknownLow()
extern "C" uint16_t EncoderFallbackException_get_CharUnknownLow_m1_12285 (EncoderFallbackException_t1_1432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderFallbackException::get_Index()
extern "C" int32_t EncoderFallbackException_get_Index_m1_12286 (EncoderFallbackException_t1_1432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderFallbackException::IsUnknownSurrogate()
extern "C" bool EncoderFallbackException_IsUnknownSurrogate_m1_12287 (EncoderFallbackException_t1_1432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
