﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Xml.DTDNode
struct DTDNode_t4_89;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t4_90;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// Mono.Xml.DictionaryBase/<>c__Iterator3
struct  U3CU3Ec__Iterator3_t4_88  : public Object_t
{
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>> Mono.Xml.DictionaryBase/<>c__Iterator3::<$s_431>__0
	Enumerator_t1_1804  ___U3CU24s_431U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::<p>__1
	KeyValuePair_2_t1_1805  ___U3CpU3E__1_1;
	// System.Int32 Mono.Xml.DictionaryBase/<>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::$current
	DTDNode_t4_89 * ___U24current_3;
	// Mono.Xml.DictionaryBase Mono.Xml.DictionaryBase/<>c__Iterator3::<>f__this
	DictionaryBase_t4_90 * ___U3CU3Ef__this_4;
};
