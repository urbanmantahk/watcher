﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.SafeHandles.CriticalHandleMinusOneIsInvalid
struct CriticalHandleMinusOneIsInvalid_t1_82;

#include "codegen/il2cpp-codegen.h"

// System.Void Microsoft.Win32.SafeHandles.CriticalHandleMinusOneIsInvalid::.ctor()
extern "C" void CriticalHandleMinusOneIsInvalid__ctor_m1_1414 (CriticalHandleMinusOneIsInvalid_t1_82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.CriticalHandleMinusOneIsInvalid::get_IsInvalid()
extern "C" bool CriticalHandleMinusOneIsInvalid_get_IsInvalid_m1_1415 (CriticalHandleMinusOneIsInvalid_t1_82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
