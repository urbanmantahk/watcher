﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_6.h"

// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15426_gshared (InternalEnumerator_1_t1_1954 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15426(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1954 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15426_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15427_gshared (InternalEnumerator_1_t1_1954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15427(__this, method) (( void (*) (InternalEnumerator_1_t1_1954 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15427_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15428_gshared (InternalEnumerator_1_t1_1954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15428(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1954 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15428_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15429_gshared (InternalEnumerator_1_t1_1954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15429(__this, method) (( void (*) (InternalEnumerator_1_t1_1954 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15429_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15430_gshared (InternalEnumerator_1_t1_1954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15430(__this, method) (( bool (*) (InternalEnumerator_1_t1_1954 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15430_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m1_15431_gshared (InternalEnumerator_1_t1_1954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15431(__this, method) (( uint8_t (*) (InternalEnumerator_1_t1_1954 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15431_gshared)(__this, method)
