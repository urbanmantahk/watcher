﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_25673(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1915 *, String_t*, JSONValue_t8_4 *, const MethodInfo*))KeyValuePair_2__ctor_m1_15906_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>::get_Key()
#define KeyValuePair_2_get_Key_m1_15031(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1915 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_15907_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_25674(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1915 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1_15908_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>::get_Value()
#define KeyValuePair_2_get_Value_m1_15032(__this, method) (( JSONValue_t8_4 * (*) (KeyValuePair_2_t1_1915 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_15909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_25675(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1915 *, JSONValue_t8_4 *, const MethodInfo*))KeyValuePair_2_set_Value_m1_15910_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Boomlagoon.JSON.JSONValue>::ToString()
#define KeyValuePair_2_ToString_m1_25676(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1915 *, const MethodInfo*))KeyValuePair_2_ToString_m1_15911_gshared)(__this, method)
