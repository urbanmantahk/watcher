﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.HMACAlgorithm
struct HMACAlgorithm_t1_161;
// System.String
struct String_t;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Cryptography.HMACAlgorithm::.ctor(System.String)
extern "C" void HMACAlgorithm__ctor_m1_1948 (HMACAlgorithm_t1_161 * __this, String_t* ___algoName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Finalize()
extern "C" void HMACAlgorithm_Finalize_m1_1949 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::CreateHash(System.String)
extern "C" void HMACAlgorithm_CreateHash_m1_1950 (HMACAlgorithm_t1_161 * __this, String_t* ___algoName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Dispose()
extern "C" void HMACAlgorithm_Dispose_m1_1951 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.HMACAlgorithm::get_Algo()
extern "C" HashAlgorithm_t1_162 * HMACAlgorithm_get_Algo_m1_1952 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.Cryptography.HMACAlgorithm::get_HashName()
extern "C" String_t* HMACAlgorithm_get_HashName_m1_1953 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::set_HashName(System.String)
extern "C" void HMACAlgorithm_set_HashName_m1_1954 (HMACAlgorithm_t1_161 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::get_Key()
extern "C" ByteU5BU5D_t1_109* HMACAlgorithm_get_Key_m1_1955 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::set_Key(System.Byte[])
extern "C" void HMACAlgorithm_set_Key_m1_1956 (HMACAlgorithm_t1_161 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Initialize()
extern "C" void HMACAlgorithm_Initialize_m1_1957 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::KeySetup(System.Byte[],System.Byte)
extern "C" ByteU5BU5D_t1_109* HMACAlgorithm_KeySetup_m1_1958 (HMACAlgorithm_t1_161 * __this, ByteU5BU5D_t1_109* ___key, uint8_t ___padding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.HMACAlgorithm::Core(System.Byte[],System.Int32,System.Int32)
extern "C" void HMACAlgorithm_Core_m1_1959 (HMACAlgorithm_t1_161 * __this, ByteU5BU5D_t1_109* ___rgb, int32_t ___ib, int32_t ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.HMACAlgorithm::Final()
extern "C" ByteU5BU5D_t1_109* HMACAlgorithm_Final_m1_1960 (HMACAlgorithm_t1_161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
