﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.CodeConnectAccess
struct CodeConnectAccess_t1_1338;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.CodeConnectAccess::.ctor(System.String,System.Int32)
extern "C" void CodeConnectAccess__ctor_m1_11397 (CodeConnectAccess_t1_1338 * __this, String_t* ___allowScheme, int32_t ___allowPort, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.CodeConnectAccess::.cctor()
extern "C" void CodeConnectAccess__cctor_m1_11398 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.CodeConnectAccess::get_Port()
extern "C" int32_t CodeConnectAccess_get_Port_m1_11399 (CodeConnectAccess_t1_1338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.CodeConnectAccess::get_Scheme()
extern "C" String_t* CodeConnectAccess_get_Scheme_m1_11400 (CodeConnectAccess_t1_1338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.CodeConnectAccess::Equals(System.Object)
extern "C" bool CodeConnectAccess_Equals_m1_11401 (CodeConnectAccess_t1_1338 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.CodeConnectAccess::GetHashCode()
extern "C" int32_t CodeConnectAccess_GetHashCode_m1_11402 (CodeConnectAccess_t1_1338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeConnectAccess System.Security.Policy.CodeConnectAccess::CreateAnySchemeAccess(System.Int32)
extern "C" CodeConnectAccess_t1_1338 * CodeConnectAccess_CreateAnySchemeAccess_m1_11403 (Object_t * __this /* static, unused */, int32_t ___allowPort, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeConnectAccess System.Security.Policy.CodeConnectAccess::CreateOriginSchemeAccess(System.Int32)
extern "C" CodeConnectAccess_t1_1338 * CodeConnectAccess_CreateOriginSchemeAccess_m1_11404 (Object_t * __this /* static, unused */, int32_t ___allowPort, const MethodInfo* method) IL2CPP_METHOD_ATTR;
