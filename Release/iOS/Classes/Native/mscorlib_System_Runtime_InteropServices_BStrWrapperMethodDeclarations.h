﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.BStrWrapper
struct BStrWrapper_t1_757;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.BStrWrapper::.ctor(System.String)
extern "C" void BStrWrapper__ctor_m1_7586 (BStrWrapper_t1_757 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.BStrWrapper::get_WrappedObject()
extern "C" String_t* BStrWrapper_get_WrappedObject_m1_7587 (BStrWrapper_t1_757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
