﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger.h"

// Mono.Xml.Schema.XsdNegativeInteger
struct  XsdNegativeInteger_t4_34  : public XsdNonPositiveInteger_t4_33
{
};
