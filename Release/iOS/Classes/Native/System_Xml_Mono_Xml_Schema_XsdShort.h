﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdInt.h"

// Mono.Xml.Schema.XsdShort
struct  XsdShort_t4_25  : public XsdInt_t4_24
{
};
