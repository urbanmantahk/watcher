﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.StrongNameIdentityPermissionAttribute
struct StrongNameIdentityPermissionAttribute_t1_1316;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.StrongNameIdentityPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void StrongNameIdentityPermissionAttribute__ctor_m1_11242 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.StrongNameIdentityPermissionAttribute::get_Name()
extern "C" String_t* StrongNameIdentityPermissionAttribute_get_Name_m1_11243 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.StrongNameIdentityPermissionAttribute::set_Name(System.String)
extern "C" void StrongNameIdentityPermissionAttribute_set_Name_m1_11244 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.StrongNameIdentityPermissionAttribute::get_PublicKey()
extern "C" String_t* StrongNameIdentityPermissionAttribute_get_PublicKey_m1_11245 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.StrongNameIdentityPermissionAttribute::set_PublicKey(System.String)
extern "C" void StrongNameIdentityPermissionAttribute_set_PublicKey_m1_11246 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.StrongNameIdentityPermissionAttribute::get_Version()
extern "C" String_t* StrongNameIdentityPermissionAttribute_get_Version_m1_11247 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.StrongNameIdentityPermissionAttribute::set_Version(System.String)
extern "C" void StrongNameIdentityPermissionAttribute_set_Version_m1_11248 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.StrongNameIdentityPermissionAttribute::CreatePermission()
extern "C" Object_t * StrongNameIdentityPermissionAttribute_CreatePermission_m1_11249 (StrongNameIdentityPermissionAttribute_t1_1316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
