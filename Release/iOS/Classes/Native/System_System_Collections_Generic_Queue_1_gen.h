﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLibrary.ExifProperty[]
struct ExifPropertyU5BU5D_t8_380;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.Queue`1<ExifLibrary.ExifProperty>
struct  Queue_1_t3_249  : public Object_t
{
	// T[] System.Collections.Generic.Queue`1::_array
	ExifPropertyU5BU5D_t8_380* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
};
