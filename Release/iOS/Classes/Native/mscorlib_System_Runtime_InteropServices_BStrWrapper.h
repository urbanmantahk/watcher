﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Runtime.InteropServices.BStrWrapper
struct  BStrWrapper_t1_757  : public Object_t
{
	// System.String System.Runtime.InteropServices.BStrWrapper::_value
	String_t* ____value_0;
};
