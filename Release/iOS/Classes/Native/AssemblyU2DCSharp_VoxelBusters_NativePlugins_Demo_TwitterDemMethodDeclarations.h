﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.TwitterDemo
struct TwitterDemo_t8_187;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.TwitterDemo::.ctor()
extern "C" void TwitterDemo__ctor_m8_1093 (TwitterDemo_t8_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
