﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Thread
struct Thread_t1_901;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t1_361;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.NumberFormatter
struct NumberFormatter_t1_1587;

#include "mscorlib_System_Object.h"

// System.NumberFormatter
struct  NumberFormatter_t1_1587  : public Object_t
{
	// System.Threading.Thread System.NumberFormatter::_thread
	Thread_t1_901 * ____thread_29;
	// System.Globalization.NumberFormatInfo System.NumberFormatter::_nfi
	NumberFormatInfo_t1_361 * ____nfi_30;
	// System.Boolean System.NumberFormatter::_NaN
	bool ____NaN_31;
	// System.Boolean System.NumberFormatter::_infinity
	bool ____infinity_32;
	// System.Boolean System.NumberFormatter::_isCustomFormat
	bool ____isCustomFormat_33;
	// System.Boolean System.NumberFormatter::_specifierIsUpper
	bool ____specifierIsUpper_34;
	// System.Boolean System.NumberFormatter::_positive
	bool ____positive_35;
	// System.Char System.NumberFormatter::_specifier
	uint16_t ____specifier_36;
	// System.Int32 System.NumberFormatter::_precision
	int32_t ____precision_37;
	// System.Int32 System.NumberFormatter::_defPrecision
	int32_t ____defPrecision_38;
	// System.Int32 System.NumberFormatter::_digitsLen
	int32_t ____digitsLen_39;
	// System.Int32 System.NumberFormatter::_offset
	int32_t ____offset_40;
	// System.Int32 System.NumberFormatter::_decPointPos
	int32_t ____decPointPos_41;
	// System.UInt32 System.NumberFormatter::_val1
	uint32_t ____val1_42;
	// System.UInt32 System.NumberFormatter::_val2
	uint32_t ____val2_43;
	// System.UInt32 System.NumberFormatter::_val3
	uint32_t ____val3_44;
	// System.UInt32 System.NumberFormatter::_val4
	uint32_t ____val4_45;
	// System.Char[] System.NumberFormatter::_cbuf
	CharU5BU5D_t1_16* ____cbuf_46;
	// System.Int32 System.NumberFormatter::_ind
	int32_t ____ind_47;
};
struct NumberFormatter_t1_1587_StaticFields{
	// System.UInt64* System.NumberFormatter::MantissaBitsTable
	uint64_t* ___MantissaBitsTable_23;
	// System.Int32* System.NumberFormatter::TensExponentTable
	int32_t* ___TensExponentTable_24;
	// System.Char* System.NumberFormatter::DigitLowerTable
	uint16_t* ___DigitLowerTable_25;
	// System.Char* System.NumberFormatter::DigitUpperTable
	uint16_t* ___DigitUpperTable_26;
	// System.Int64* System.NumberFormatter::TenPowersList
	int64_t* ___TenPowersList_27;
	// System.Int32* System.NumberFormatter::DecHexDigits
	int32_t* ___DecHexDigits_28;
};
struct NumberFormatter_t1_1587_ThreadStaticFields{
	// System.NumberFormatter System.NumberFormatter::threadNumberFormatter
	NumberFormatter_t1_1587 * ___threadNumberFormatter_48;
};
