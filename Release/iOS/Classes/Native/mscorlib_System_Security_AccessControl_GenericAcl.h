﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// System.Security.AccessControl.GenericAcl
struct  GenericAcl_t1_1114  : public Object_t
{
};
struct GenericAcl_t1_1114_StaticFields{
	// System.Byte System.Security.AccessControl.GenericAcl::AclRevision
	uint8_t ___AclRevision_0;
	// System.Byte System.Security.AccessControl.GenericAcl::AclRevisionDS
	uint8_t ___AclRevisionDS_1;
	// System.Int32 System.Security.AccessControl.GenericAcl::MaxBinaryLength
	int32_t ___MaxBinaryLength_2;
};
