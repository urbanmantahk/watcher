﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t4_52;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdGDay::.ctor()
extern "C" void XsdGDay__ctor_m4_77 (XsdGDay_t4_52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
