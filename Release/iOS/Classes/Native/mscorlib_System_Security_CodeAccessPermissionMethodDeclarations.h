﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.CodeAccessPermission
struct CodeAccessPermission_t1_1268;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_SecurityFrame.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.CodeAccessPermission::.ctor()
extern "C" void CodeAccessPermission__ctor_m1_11900 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::Assert()
extern "C" void CodeAccessPermission_Assert_m1_11901 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::CheckAssert(System.Security.CodeAccessPermission)
extern "C" bool CodeAccessPermission_CheckAssert_m1_11902 (CodeAccessPermission_t1_1268 * __this, CodeAccessPermission_t1_1268 * ___asserted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::CheckDemand(System.Security.CodeAccessPermission)
extern "C" bool CodeAccessPermission_CheckDemand_m1_11903 (CodeAccessPermission_t1_1268 * __this, CodeAccessPermission_t1_1268 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::CheckDeny(System.Security.CodeAccessPermission)
extern "C" bool CodeAccessPermission_CheckDeny_m1_11904 (CodeAccessPermission_t1_1268 * __this, CodeAccessPermission_t1_1268 * ___denied, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::CheckPermitOnly(System.Security.CodeAccessPermission)
extern "C" bool CodeAccessPermission_CheckPermitOnly_m1_11905 (CodeAccessPermission_t1_1268 * __this, CodeAccessPermission_t1_1268 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::Demand()
extern "C" void CodeAccessPermission_Demand_m1_11906 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::Deny()
extern "C" void CodeAccessPermission_Deny_m1_11907 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::Equals(System.Object)
extern "C" bool CodeAccessPermission_Equals_m1_11908 (CodeAccessPermission_t1_1268 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.CodeAccessPermission::GetHashCode()
extern "C" int32_t CodeAccessPermission_GetHashCode_m1_11909 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.CodeAccessPermission::ToString()
extern "C" String_t* CodeAccessPermission_ToString_m1_11910 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.CodeAccessPermission::Union(System.Security.IPermission)
extern "C" Object_t * CodeAccessPermission_Union_m1_11911 (CodeAccessPermission_t1_1268 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::PermitOnly()
extern "C" void CodeAccessPermission_PermitOnly_m1_11912 (CodeAccessPermission_t1_1268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::RevertAll()
extern "C" void CodeAccessPermission_RevertAll_m1_11913 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::RevertAssert()
extern "C" void CodeAccessPermission_RevertAssert_m1_11914 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::RevertDeny()
extern "C" void CodeAccessPermission_RevertDeny_m1_11915 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::RevertPermitOnly()
extern "C" void CodeAccessPermission_RevertPermitOnly_m1_11916 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.CodeAccessPermission::Element(System.Int32)
extern "C" SecurityElement_t1_242 * CodeAccessPermission_Element_m1_11917 (CodeAccessPermission_t1_1268 * __this, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.PermissionState System.Security.CodeAccessPermission::CheckPermissionState(System.Security.Permissions.PermissionState,System.Boolean)
extern "C" int32_t CodeAccessPermission_CheckPermissionState_m1_11918 (Object_t * __this /* static, unused */, int32_t ___state, bool ___allowUnrestricted, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.CodeAccessPermission::CheckSecurityElement(System.Security.SecurityElement,System.String,System.Int32,System.Int32)
extern "C" int32_t CodeAccessPermission_CheckSecurityElement_m1_11919 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___se, String_t* ___parameterName, int32_t ___minimumVersion, int32_t ___maximumVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::IsUnrestricted(System.Security.SecurityElement)
extern "C" bool CodeAccessPermission_IsUnrestricted_m1_11920 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___se, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.CodeAccessPermission::ProcessFrame(System.Security.SecurityFrame)
extern "C" bool CodeAccessPermission_ProcessFrame_m1_11921 (CodeAccessPermission_t1_1268 * __this, SecurityFrame_t1_1404  ___frame, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::ThrowInvalidPermission(System.Security.IPermission,System.Type)
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m1_11922 (Object_t * __this /* static, unused */, Object_t * ___target, Type_t * ___expected, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::ThrowExecutionEngineException(System.Security.Permissions.SecurityAction)
extern "C" void CodeAccessPermission_ThrowExecutionEngineException_m1_11923 (Object_t * __this /* static, unused */, int32_t ___stackmod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.CodeAccessPermission::ThrowSecurityException(System.Object,System.String,System.Security.SecurityFrame,System.Security.Permissions.SecurityAction,System.Security.IPermission)
extern "C" void CodeAccessPermission_ThrowSecurityException_m1_11924 (Object_t * __this /* static, unused */, Object_t * ___demanded, String_t* ___message, SecurityFrame_t1_1404  ___frame, int32_t ___action, Object_t * ___failed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
