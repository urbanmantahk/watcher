﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/RangedArrayList
struct RangedArrayList_t1_267;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.IComparer
struct IComparer_t1_295;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/RangedArrayList::.ctor(System.Collections.ArrayList,System.Int32,System.Int32)
extern "C" void RangedArrayList__ctor_m1_2960 (RangedArrayList_t1_267 * __this, ArrayList_t1_170 * ___innerList, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/RangedArrayList::get_IsSynchronized()
extern "C" bool RangedArrayList_get_IsSynchronized_m1_2961 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/RangedArrayList::get_Item(System.Int32)
extern "C" Object_t * RangedArrayList_get_Item_m1_2962 (RangedArrayList_t1_267 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::set_Item(System.Int32,System.Object)
extern "C" void RangedArrayList_set_Item_m1_2963 (RangedArrayList_t1_267 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::get_Count()
extern "C" int32_t RangedArrayList_get_Count_m1_2964 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::get_Capacity()
extern "C" int32_t RangedArrayList_get_Capacity_m1_2965 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::set_Capacity(System.Int32)
extern "C" void RangedArrayList_set_Capacity_m1_2966 (RangedArrayList_t1_267 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::VerifyStateChanges()
extern "C" void RangedArrayList_VerifyStateChanges_m1_2967 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::Add(System.Object)
extern "C" int32_t RangedArrayList_Add_m1_2968 (RangedArrayList_t1_267 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Clear()
extern "C" void RangedArrayList_Clear_m1_2969 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/RangedArrayList::Contains(System.Object)
extern "C" bool RangedArrayList_Contains_m1_2970 (RangedArrayList_t1_267 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::IndexOf(System.Object)
extern "C" int32_t RangedArrayList_IndexOf_m1_2971 (RangedArrayList_t1_267 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::IndexOf(System.Object,System.Int32)
extern "C" int32_t RangedArrayList_IndexOf_m1_2972 (RangedArrayList_t1_267 * __this, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::IndexOf(System.Object,System.Int32,System.Int32)
extern "C" int32_t RangedArrayList_IndexOf_m1_2973 (RangedArrayList_t1_267 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::LastIndexOf(System.Object)
extern "C" int32_t RangedArrayList_LastIndexOf_m1_2974 (RangedArrayList_t1_267 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::LastIndexOf(System.Object,System.Int32)
extern "C" int32_t RangedArrayList_LastIndexOf_m1_2975 (RangedArrayList_t1_267 * __this, Object_t * ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::LastIndexOf(System.Object,System.Int32,System.Int32)
extern "C" int32_t RangedArrayList_LastIndexOf_m1_2976 (RangedArrayList_t1_267 * __this, Object_t * ___value, int32_t ___startIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Insert(System.Int32,System.Object)
extern "C" void RangedArrayList_Insert_m1_2977 (RangedArrayList_t1_267 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::InsertRange(System.Int32,System.Collections.ICollection)
extern "C" void RangedArrayList_InsertRange_m1_2978 (RangedArrayList_t1_267 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Remove(System.Object)
extern "C" void RangedArrayList_Remove_m1_2979 (RangedArrayList_t1_267 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::RemoveAt(System.Int32)
extern "C" void RangedArrayList_RemoveAt_m1_2980 (RangedArrayList_t1_267 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::RemoveRange(System.Int32,System.Int32)
extern "C" void RangedArrayList_RemoveRange_m1_2981 (RangedArrayList_t1_267 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Reverse()
extern "C" void RangedArrayList_Reverse_m1_2982 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Reverse(System.Int32,System.Int32)
extern "C" void RangedArrayList_Reverse_m1_2983 (RangedArrayList_t1_267 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::SetRange(System.Int32,System.Collections.ICollection)
extern "C" void RangedArrayList_SetRange_m1_2984 (RangedArrayList_t1_267 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::CopyTo(System.Array)
extern "C" void RangedArrayList_CopyTo_m1_2985 (RangedArrayList_t1_267 * __this, Array_t * ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::CopyTo(System.Array,System.Int32)
extern "C" void RangedArrayList_CopyTo_m1_2986 (RangedArrayList_t1_267 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::CopyTo(System.Int32,System.Array,System.Int32,System.Int32)
extern "C" void RangedArrayList_CopyTo_m1_2987 (RangedArrayList_t1_267 * __this, int32_t ___index, Array_t * ___array, int32_t ___arrayIndex, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList/RangedArrayList::GetEnumerator()
extern "C" Object_t * RangedArrayList_GetEnumerator_m1_2988 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList/RangedArrayList::GetEnumerator(System.Int32,System.Int32)
extern "C" Object_t * RangedArrayList_GetEnumerator_m1_2989 (RangedArrayList_t1_267 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::AddRange(System.Collections.ICollection)
extern "C" void RangedArrayList_AddRange_m1_2990 (RangedArrayList_t1_267 * __this, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::BinarySearch(System.Object)
extern "C" int32_t RangedArrayList_BinarySearch_m1_2991 (RangedArrayList_t1_267 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::BinarySearch(System.Object,System.Collections.IComparer)
extern "C" int32_t RangedArrayList_BinarySearch_m1_2992 (RangedArrayList_t1_267 * __this, Object_t * ___value, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/RangedArrayList::BinarySearch(System.Int32,System.Int32,System.Object,System.Collections.IComparer)
extern "C" int32_t RangedArrayList_BinarySearch_m1_2993 (RangedArrayList_t1_267 * __this, int32_t ___index, int32_t ___count, Object_t * ___value, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/RangedArrayList::Clone()
extern "C" Object_t * RangedArrayList_Clone_m1_2994 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Collections.ArrayList/RangedArrayList::GetRange(System.Int32,System.Int32)
extern "C" ArrayList_t1_170 * RangedArrayList_GetRange_m1_2995 (RangedArrayList_t1_267 * __this, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::TrimToSize()
extern "C" void RangedArrayList_TrimToSize_m1_2996 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Sort()
extern "C" void RangedArrayList_Sort_m1_2997 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Sort(System.Collections.IComparer)
extern "C" void RangedArrayList_Sort_m1_2998 (RangedArrayList_t1_267 * __this, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/RangedArrayList::Sort(System.Int32,System.Int32,System.Collections.IComparer)
extern "C" void RangedArrayList_Sort_m1_2999 (RangedArrayList_t1_267 * __this, int32_t ___index, int32_t ___count, Object_t * ___comparer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Collections.ArrayList/RangedArrayList::ToArray()
extern "C" ObjectU5BU5D_t1_272* RangedArrayList_ToArray_m1_3000 (RangedArrayList_t1_267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array System.Collections.ArrayList/RangedArrayList::ToArray(System.Type)
extern "C" Array_t * RangedArrayList_ToArray_m1_3001 (RangedArrayList_t1_267 * __this, Type_t * ___elementType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
