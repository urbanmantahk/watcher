﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.DataConverter
struct DataConverter_t1_252;

#include "mscorlib_System_Object.h"

// Mono.DataConverter
struct  DataConverter_t1_252  : public Object_t
{
};
struct DataConverter_t1_252_StaticFields{
	// Mono.DataConverter Mono.DataConverter::SwapConv
	DataConverter_t1_252 * ___SwapConv_0;
	// Mono.DataConverter Mono.DataConverter::CopyConv
	DataConverter_t1_252 * ___CopyConv_1;
	// System.Boolean Mono.DataConverter::IsLittleEndian
	bool ___IsLittleEndian_2;
};
