﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ContextMarshalException
struct ContextMarshalException_t1_1519;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ContextMarshalException::.ctor()
extern "C" void ContextMarshalException__ctor_m1_13431 (ContextMarshalException_t1_1519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ContextMarshalException::.ctor(System.String)
extern "C" void ContextMarshalException__ctor_m1_13432 (ContextMarshalException_t1_1519 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ContextMarshalException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ContextMarshalException__ctor_m1_13433 (ContextMarshalException_t1_1519 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ContextMarshalException::.ctor(System.String,System.Exception)
extern "C" void ContextMarshalException__ctor_m1_13434 (ContextMarshalException_t1_1519 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
