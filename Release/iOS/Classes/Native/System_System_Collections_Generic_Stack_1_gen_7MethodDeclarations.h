﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor()
#define Stack_1__ctor_m3_2049(__this, method) (( void (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1__ctor_m3_1816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_2050(__this, method) (( bool (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3_2051(__this, method) (( Object_t * (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m3_2052(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3_273 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_2053(__this, method) (( Object_t* (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_2054(__this, method) (( Object_t * (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Contains(T)
#define Stack_1_Contains_m3_2055(__this, ___t, method) (( bool (*) (Stack_1_t3_273 *, List_1_t1_1834 *, const MethodInfo*))Stack_1_Contains_m3_1827_gshared)(__this, ___t, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Peek()
#define Stack_1_Peek_m3_2056(__this, method) (( List_1_t1_1834 * (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_Peek_m3_1829_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Pop()
#define Stack_1_Pop_m3_2057(__this, method) (( List_1_t1_1834 * (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_Pop_m3_1830_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Push(T)
#define Stack_1_Push_m3_2058(__this, ___t, method) (( void (*) (Stack_1_t3_273 *, List_1_t1_1834 *, const MethodInfo*))Stack_1_Push_m3_1831_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::get_Count()
#define Stack_1_get_Count_m3_2059(__this, method) (( int32_t (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_get_Count_m3_1833_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::GetEnumerator()
#define Stack_1_GetEnumerator_m3_2060(__this, method) (( Enumerator_t3_293  (*) (Stack_1_t3_273 *, const MethodInfo*))Stack_1_GetEnumerator_m3_1835_gshared)(__this, method)
