﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.SortedList
struct SortedList_t1_304;

#include "mscorlib_System_Collections_SortedList.h"

// System.Collections.SortedList/SynchedSortedList
struct  SynchedSortedList_t1_307  : public SortedList_t1_304
{
	// System.Collections.SortedList System.Collections.SortedList/SynchedSortedList::host
	SortedList_t1_304 * ___host_6;
};
