﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.FieldOnTypeBuilderInst
struct FieldOnTypeBuilderInst_t1_506;
// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1_499;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Emit.FieldOnTypeBuilderInst::.ctor(System.Reflection.MonoGenericClass,System.Reflection.Emit.FieldBuilder)
extern "C" void FieldOnTypeBuilderInst__ctor_m1_5859 (FieldOnTypeBuilderInst_t1_506 * __this, MonoGenericClass_t1_485 * ___instantiation, FieldBuilder_t1_499 * ___fb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.FieldOnTypeBuilderInst::get_DeclaringType()
extern "C" Type_t * FieldOnTypeBuilderInst_get_DeclaringType_m1_5860 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.FieldOnTypeBuilderInst::get_Name()
extern "C" String_t* FieldOnTypeBuilderInst_get_Name_m1_5861 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.FieldOnTypeBuilderInst::get_ReflectedType()
extern "C" Type_t * FieldOnTypeBuilderInst_get_ReflectedType_m1_5862 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.FieldOnTypeBuilderInst::IsDefined(System.Type,System.Boolean)
extern "C" bool FieldOnTypeBuilderInst_IsDefined_m1_5863 (FieldOnTypeBuilderInst_t1_506 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.FieldOnTypeBuilderInst::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* FieldOnTypeBuilderInst_GetCustomAttributes_m1_5864 (FieldOnTypeBuilderInst_t1_506 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.FieldOnTypeBuilderInst::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* FieldOnTypeBuilderInst_GetCustomAttributes_m1_5865 (FieldOnTypeBuilderInst_t1_506 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.FieldOnTypeBuilderInst::ToString()
extern "C" String_t* FieldOnTypeBuilderInst_ToString_m1_5866 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldAttributes System.Reflection.Emit.FieldOnTypeBuilderInst::get_Attributes()
extern "C" int32_t FieldOnTypeBuilderInst_get_Attributes_m1_5867 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeFieldHandle System.Reflection.Emit.FieldOnTypeBuilderInst::get_FieldHandle()
extern "C" RuntimeFieldHandle_t1_36  FieldOnTypeBuilderInst_get_FieldHandle_m1_5868 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.FieldOnTypeBuilderInst::get_MetadataToken()
extern "C" int32_t FieldOnTypeBuilderInst_get_MetadataToken_m1_5869 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.FieldOnTypeBuilderInst::get_FieldType()
extern "C" Type_t * FieldOnTypeBuilderInst_get_FieldType_m1_5870 (FieldOnTypeBuilderInst_t1_506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.FieldOnTypeBuilderInst::GetValue(System.Object)
extern "C" Object_t * FieldOnTypeBuilderInst_GetValue_m1_5871 (FieldOnTypeBuilderInst_t1_506 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldOnTypeBuilderInst::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern "C" void FieldOnTypeBuilderInst_SetValue_m1_5872 (FieldOnTypeBuilderInst_t1_506 * __this, Object_t * ___obj, Object_t * ___value, int32_t ___invokeAttr, Binder_t1_585 * ___binder, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
