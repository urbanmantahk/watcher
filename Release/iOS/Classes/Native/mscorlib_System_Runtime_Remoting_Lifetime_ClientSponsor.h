﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_MarshalByRefObject.h"
#include "mscorlib_System_TimeSpan.h"

// System.Runtime.Remoting.Lifetime.ClientSponsor
struct  ClientSponsor_t1_905  : public MarshalByRefObject_t1_69
{
	// System.TimeSpan System.Runtime.Remoting.Lifetime.ClientSponsor::renewal_time
	TimeSpan_t1_368  ___renewal_time_1;
	// System.Collections.Hashtable System.Runtime.Remoting.Lifetime.ClientSponsor::registered_objects
	Hashtable_t1_100 * ___registered_objects_2;
};
