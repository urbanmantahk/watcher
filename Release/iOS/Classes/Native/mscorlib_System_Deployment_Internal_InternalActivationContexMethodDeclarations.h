﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Object System.Deployment.Internal.InternalActivationContextHelper::GetActivationContextData(System.ActivationContext)
extern "C" Object_t * InternalActivationContextHelper_GetActivationContextData_m1_3543 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Deployment.Internal.InternalActivationContextHelper::GetApplicationComponentManifest(System.ActivationContext)
extern "C" Object_t * InternalActivationContextHelper_GetApplicationComponentManifest_m1_3544 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Deployment.Internal.InternalActivationContextHelper::GetApplicationManifestBytes(System.ActivationContext)
extern "C" ByteU5BU5D_t1_109* InternalActivationContextHelper_GetApplicationManifestBytes_m1_3545 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Deployment.Internal.InternalActivationContextHelper::GetDeploymentComponentManifest(System.ActivationContext)
extern "C" Object_t * InternalActivationContextHelper_GetDeploymentComponentManifest_m1_3546 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Deployment.Internal.InternalActivationContextHelper::GetDeploymentManifestBytes(System.ActivationContext)
extern "C" ByteU5BU5D_t1_109* InternalActivationContextHelper_GetDeploymentManifestBytes_m1_3547 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Deployment.Internal.InternalActivationContextHelper::IsFirstRun(System.ActivationContext)
extern "C" bool InternalActivationContextHelper_IsFirstRun_m1_3548 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Deployment.Internal.InternalActivationContextHelper::PrepareForExecution(System.ActivationContext)
extern "C" void InternalActivationContextHelper_PrepareForExecution_m1_3549 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___appInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
