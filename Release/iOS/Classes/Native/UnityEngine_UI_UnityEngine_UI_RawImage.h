﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t6_32;

#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
#include "UnityEngine_UnityEngine_Rect.h"

// UnityEngine.UI.RawImage
struct  RawImage_t7_107  : public MaskableGraphic_t7_88
{
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t6_32 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t6_51  ___m_UVRect_29;
};
