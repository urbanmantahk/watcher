﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_171.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_21580_gshared (InternalEnumerator_1_t1_2425 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_21580(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2425 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_21580_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_21581_gshared (InternalEnumerator_1_t1_2425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_21581(__this, method) (( void (*) (InternalEnumerator_1_t1_2425 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_21581_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_21582_gshared (InternalEnumerator_1_t1_2425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_21582(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2425 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_21582_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_21583_gshared (InternalEnumerator_1_t1_2425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_21583(__this, method) (( void (*) (InternalEnumerator_1_t1_2425 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_21583_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_21584_gshared (InternalEnumerator_1_t1_2425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_21584(__this, method) (( bool (*) (InternalEnumerator_1_t1_2425 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_21584_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern "C" KeyValuePair_2_t1_2424  InternalEnumerator_1_get_Current_m1_21585_gshared (InternalEnumerator_1_t1_2425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_21585(__this, method) (( KeyValuePair_2_t1_2424  (*) (InternalEnumerator_1_t1_2425 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_21585_gshared)(__this, method)
