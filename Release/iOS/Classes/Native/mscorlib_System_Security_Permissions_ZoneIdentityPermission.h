﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Security.Permissions.ZoneIdentityPermission
struct  ZoneIdentityPermission_t1_1323  : public CodeAccessPermission_t1_1268
{
	// System.Security.SecurityZone System.Security.Permissions.ZoneIdentityPermission::zone
	int32_t ___zone_1;
};
