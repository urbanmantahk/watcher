﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.StreamReader
struct StreamReader_t1_447;
// System.IO.Stream
struct Stream_t1_405;
// System.Text.Encoding
struct Encoding_t1_406;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "codegen/il2cpp-codegen.h"

// System.Void System.IO.StreamReader::.ctor()
extern "C" void StreamReader__ctor_m1_5209 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream)
extern "C" void StreamReader__ctor_m1_5210 (StreamReader_t1_447 * __this, Stream_t1_405 * ___stream, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Boolean)
extern "C" void StreamReader__ctor_m1_5211 (StreamReader_t1_447 * __this, Stream_t1_405 * ___stream, bool ___detectEncodingFromByteOrderMarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding)
extern "C" void StreamReader__ctor_m1_5212 (StreamReader_t1_447 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean)
extern "C" void StreamReader__ctor_m1_5213 (StreamReader_t1_447 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, bool ___detectEncodingFromByteOrderMarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.IO.Stream,System.Text.Encoding,System.Boolean,System.Int32)
extern "C" void StreamReader__ctor_m1_5214 (StreamReader_t1_447 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, bool ___detectEncodingFromByteOrderMarks, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.String)
extern "C" void StreamReader__ctor_m1_5215 (StreamReader_t1_447 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Boolean)
extern "C" void StreamReader__ctor_m1_5216 (StreamReader_t1_447 * __this, String_t* ___path, bool ___detectEncodingFromByteOrderMarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Text.Encoding)
extern "C" void StreamReader__ctor_m1_5217 (StreamReader_t1_447 * __this, String_t* ___path, Encoding_t1_406 * ___encoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean)
extern "C" void StreamReader__ctor_m1_5218 (StreamReader_t1_447 * __this, String_t* ___path, Encoding_t1_406 * ___encoding, bool ___detectEncodingFromByteOrderMarks, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.ctor(System.String,System.Text.Encoding,System.Boolean,System.Int32)
extern "C" void StreamReader__ctor_m1_5219 (StreamReader_t1_447 * __this, String_t* ___path, Encoding_t1_406 * ___encoding, bool ___detectEncodingFromByteOrderMarks, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::.cctor()
extern "C" void StreamReader__cctor_m1_5220 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::Initialize(System.IO.Stream,System.Text.Encoding,System.Boolean,System.Int32)
extern "C" void StreamReader_Initialize_m1_5221 (StreamReader_t1_447 * __this, Stream_t1_405 * ___stream, Encoding_t1_406 * ___encoding, bool ___detectEncodingFromByteOrderMarks, int32_t ___bufferSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.IO.StreamReader::get_BaseStream()
extern "C" Stream_t1_405 * StreamReader_get_BaseStream_m1_5222 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.IO.StreamReader::get_CurrentEncoding()
extern "C" Encoding_t1_406 * StreamReader_get_CurrentEncoding_m1_5223 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.StreamReader::get_EndOfStream()
extern "C" bool StreamReader_get_EndOfStream_m1_5224 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::Close()
extern "C" void StreamReader_Close_m1_5225 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::Dispose(System.Boolean)
extern "C" void StreamReader_Dispose_m1_5226 (StreamReader_t1_447 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader::DoChecks(System.Int32)
extern "C" int32_t StreamReader_DoChecks_m1_5227 (StreamReader_t1_447 * __this, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.StreamReader::DiscardBufferedData()
extern "C" void StreamReader_DiscardBufferedData_m1_5228 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader::ReadBuffer()
extern "C" int32_t StreamReader_ReadBuffer_m1_5229 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader::Peek()
extern "C" int32_t StreamReader_Peek_m1_5230 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.StreamReader::DataAvailable()
extern "C" bool StreamReader_DataAvailable_m1_5231 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader::Read()
extern "C" int32_t StreamReader_Read_m1_5232 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader::Read(System.Char[],System.Int32,System.Int32)
extern "C" int32_t StreamReader_Read_m1_5233 (StreamReader_t1_447 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IO.StreamReader::FindNextEOL()
extern "C" int32_t StreamReader_FindNextEOL_m1_5234 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.StreamReader::ReadLine()
extern "C" String_t* StreamReader_ReadLine_m1_5235 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.StreamReader::ReadToEnd()
extern "C" String_t* StreamReader_ReadToEnd_m1_5236 (StreamReader_t1_447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
