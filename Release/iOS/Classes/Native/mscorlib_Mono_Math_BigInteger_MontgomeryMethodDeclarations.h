﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.BigInteger/Montgomery
struct Montgomery_t1_140;
// Mono.Math.BigInteger
struct BigInteger_t1_139;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Math.BigInteger/Montgomery::.ctor()
extern "C" void Montgomery__ctor_m1_1767 (Montgomery_t1_140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Montgomery::Inverse(System.UInt32)
extern "C" uint32_t Montgomery_Inverse_m1_1768 (Object_t * __this /* static, unused */, uint32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Montgomery::ToMont(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1_139 * Montgomery_ToMont_m1_1769 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___n, BigInteger_t1_139 * ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Montgomery::Reduce(Mono.Math.BigInteger,Mono.Math.BigInteger,System.UInt32)
extern "C" BigInteger_t1_139 * Montgomery_Reduce_m1_1770 (Object_t * __this /* static, unused */, BigInteger_t1_139 * ___n, BigInteger_t1_139 * ___m, uint32_t ___mPrime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
