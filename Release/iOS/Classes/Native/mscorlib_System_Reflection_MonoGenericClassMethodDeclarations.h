﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_1671;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670;
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1_1667;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.String
struct String_t;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Void System.Reflection.MonoGenericClass::.ctor()
extern "C" void MonoGenericClass__ctor_m1_7057 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MonoGenericClass::.ctor(System.Reflection.Emit.TypeBuilder,System.Type[])
extern "C" void MonoGenericClass__ctor_m1_7058 (MonoGenericClass_t1_485 * __this, TypeBuilder_t1_481 * ___tb, TypeU5BU5D_t1_31* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MonoGenericClass::initialize(System.Reflection.MethodInfo[],System.Reflection.ConstructorInfo[],System.Reflection.FieldInfo[],System.Reflection.PropertyInfo[],System.Reflection.EventInfo[])
extern "C" void MonoGenericClass_initialize_m1_7059 (MonoGenericClass_t1_485 * __this, MethodInfoU5BU5D_t1_603* ___methods, ConstructorInfoU5BU5D_t1_1671* ___ctors, FieldInfoU5BU5D_t1_1668* ___fields, PropertyInfoU5BU5D_t1_1670* ___properties, EventInfoU5BU5D_t1_1667* ___events, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MonoGenericClass::initialize()
extern "C" void MonoGenericClass_initialize_m1_7060 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::GetParentType()
extern "C" Type_t * MonoGenericClass_GetParentType_m1_7061 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::InflateType(System.Type)
extern "C" Type_t * MonoGenericClass_InflateType_m1_7062 (MonoGenericClass_t1_485 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::InflateType(System.Type,System.Type[])
extern "C" Type_t * MonoGenericClass_InflateType_m1_7063 (MonoGenericClass_t1_485 * __this, Type_t * ___type, TypeU5BU5D_t1_31* ___method_args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::get_BaseType()
extern "C" Type_t * MonoGenericClass_get_BaseType_m1_7064 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.MonoGenericClass::GetInterfacesInternal()
extern "C" TypeU5BU5D_t1_31* MonoGenericClass_GetInterfacesInternal_m1_7065 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.MonoGenericClass::GetInterfaces()
extern "C" TypeU5BU5D_t1_31* MonoGenericClass_GetInterfaces_m1_7066 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MonoGenericClass::IsValueTypeImpl()
extern "C" bool MonoGenericClass_IsValueTypeImpl_m1_7067 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.MonoGenericClass::GetMethod(System.Reflection.MethodInfo)
extern "C" MethodInfo_t * MonoGenericClass_GetMethod_m1_7068 (MonoGenericClass_t1_485 * __this, MethodInfo_t * ___fromNoninstanciated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.MonoGenericClass::GetConstructor(System.Reflection.ConstructorInfo)
extern "C" ConstructorInfo_t1_478 * MonoGenericClass_GetConstructor_m1_7069 (MonoGenericClass_t1_485 * __this, ConstructorInfo_t1_478 * ___fromNoninstanciated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.MonoGenericClass::GetField(System.Reflection.FieldInfo)
extern "C" FieldInfo_t * MonoGenericClass_GetField_m1_7070 (MonoGenericClass_t1_485 * __this, FieldInfo_t * ___fromNoninstanciated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.MonoGenericClass::GetMethods(System.Reflection.BindingFlags)
extern "C" MethodInfoU5BU5D_t1_603* MonoGenericClass_GetMethods_m1_7071 (MonoGenericClass_t1_485 * __this, int32_t ___bf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.MonoGenericClass::GetMethodsInternal(System.Reflection.BindingFlags,System.Reflection.MonoGenericClass)
extern "C" MethodInfoU5BU5D_t1_603* MonoGenericClass_GetMethodsInternal_m1_7072 (MonoGenericClass_t1_485 * __this, int32_t ___bf, MonoGenericClass_t1_485 * ___reftype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.MonoGenericClass::GetConstructors(System.Reflection.BindingFlags)
extern "C" ConstructorInfoU5BU5D_t1_1671* MonoGenericClass_GetConstructors_m1_7073 (MonoGenericClass_t1_485 * __this, int32_t ___bf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.MonoGenericClass::GetConstructorsInternal(System.Reflection.BindingFlags,System.Reflection.MonoGenericClass)
extern "C" ConstructorInfoU5BU5D_t1_1671* MonoGenericClass_GetConstructorsInternal_m1_7074 (MonoGenericClass_t1_485 * __this, int32_t ___bf, MonoGenericClass_t1_485 * ___reftype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.MonoGenericClass::GetFields(System.Reflection.BindingFlags)
extern "C" FieldInfoU5BU5D_t1_1668* MonoGenericClass_GetFields_m1_7075 (MonoGenericClass_t1_485 * __this, int32_t ___bf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.MonoGenericClass::GetFieldsInternal(System.Reflection.BindingFlags,System.Reflection.MonoGenericClass)
extern "C" FieldInfoU5BU5D_t1_1668* MonoGenericClass_GetFieldsInternal_m1_7076 (MonoGenericClass_t1_485 * __this, int32_t ___bf, MonoGenericClass_t1_485 * ___reftype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Reflection.MonoGenericClass::GetProperties(System.Reflection.BindingFlags)
extern "C" PropertyInfoU5BU5D_t1_1670* MonoGenericClass_GetProperties_m1_7077 (MonoGenericClass_t1_485 * __this, int32_t ___bf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Reflection.MonoGenericClass::GetPropertiesInternal(System.Reflection.BindingFlags,System.Reflection.MonoGenericClass)
extern "C" PropertyInfoU5BU5D_t1_1670* MonoGenericClass_GetPropertiesInternal_m1_7078 (MonoGenericClass_t1_485 * __this, int32_t ___bf, MonoGenericClass_t1_485 * ___reftype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.MonoGenericClass::GetEvents(System.Reflection.BindingFlags)
extern "C" EventInfoU5BU5D_t1_1667* MonoGenericClass_GetEvents_m1_7079 (MonoGenericClass_t1_485 * __this, int32_t ___bf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.MonoGenericClass::GetEventsInternal(System.Reflection.BindingFlags,System.Reflection.MonoGenericClass)
extern "C" EventInfoU5BU5D_t1_1667* MonoGenericClass_GetEventsInternal_m1_7080 (MonoGenericClass_t1_485 * __this, int32_t ___bf, MonoGenericClass_t1_485 * ___reftype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.MonoGenericClass::GetNestedTypes(System.Reflection.BindingFlags)
extern "C" TypeU5BU5D_t1_31* MonoGenericClass_GetNestedTypes_m1_7081 (MonoGenericClass_t1_485 * __this, int32_t ___bf, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MonoGenericClass::IsAssignableFrom(System.Type)
extern "C" bool MonoGenericClass_IsAssignableFrom_m1_7082 (MonoGenericClass_t1_485 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::get_UnderlyingSystemType()
extern "C" Type_t * MonoGenericClass_get_UnderlyingSystemType_m1_7083 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoGenericClass::get_Name()
extern "C" String_t* MonoGenericClass_get_Name_m1_7084 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoGenericClass::get_Namespace()
extern "C" String_t* MonoGenericClass_get_Namespace_m1_7085 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoGenericClass::get_FullName()
extern "C" String_t* MonoGenericClass_get_FullName_m1_7086 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoGenericClass::get_AssemblyQualifiedName()
extern "C" String_t* MonoGenericClass_get_AssemblyQualifiedName_m1_7087 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.MonoGenericClass::get_GUID()
extern "C" Guid_t1_319  MonoGenericClass_get_GUID_m1_7088 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoGenericClass::format_name(System.Boolean,System.Boolean)
extern "C" String_t* MonoGenericClass_format_name_m1_7089 (MonoGenericClass_t1_485 * __this, bool ___full_name, bool ___assembly_qualified, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.MonoGenericClass::ToString()
extern "C" String_t* MonoGenericClass_ToString_m1_7090 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::MakeArrayType()
extern "C" Type_t * MonoGenericClass_MakeArrayType_m1_7091 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::MakeArrayType(System.Int32)
extern "C" Type_t * MonoGenericClass_MakeArrayType_m1_7092 (MonoGenericClass_t1_485 * __this, int32_t ___rank, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::MakeByRefType()
extern "C" Type_t * MonoGenericClass_MakeByRefType_m1_7093 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::MakePointerType()
extern "C" Type_t * MonoGenericClass_MakePointerType_m1_7094 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MonoGenericClass::IsCOMObjectImpl()
extern "C" bool MonoGenericClass_IsCOMObjectImpl_m1_7095 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MonoGenericClass::IsPrimitiveImpl()
extern "C" bool MonoGenericClass_IsPrimitiveImpl_m1_7096 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.MonoGenericClass::GetAttributeFlagsImpl()
extern "C" int32_t MonoGenericClass_GetAttributeFlagsImpl_m1_7097 (MonoGenericClass_t1_485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::GetInterface(System.String,System.Boolean)
extern "C" Type_t * MonoGenericClass_GetInterface_m1_7098 (MonoGenericClass_t1_485 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.MonoGenericClass::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C" EventInfo_t * MonoGenericClass_GetEvent_m1_7099 (MonoGenericClass_t1_485 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.MonoGenericClass::GetField(System.String,System.Reflection.BindingFlags)
extern "C" FieldInfo_t * MonoGenericClass_GetField_m1_7100 (MonoGenericClass_t1_485 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.MonoGenericClass::GetMembers(System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* MonoGenericClass_GetMembers_m1_7101 (MonoGenericClass_t1_485 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericClass::GetNestedType(System.String,System.Reflection.BindingFlags)
extern "C" Type_t * MonoGenericClass_GetNestedType_m1_7102 (MonoGenericClass_t1_485 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.MonoGenericClass::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C" Object_t * MonoGenericClass_InvokeMember_m1_7103 (MonoGenericClass_t1_485 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1_585 * ___binder, Object_t * ___target, ObjectU5BU5D_t1_272* ___args, ParameterModifierU5BU5D_t1_1669* ___modifiers, CultureInfo_t1_277 * ___culture, StringU5BU5D_t1_238* ___namedParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.MonoGenericClass::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * MonoGenericClass_GetMethodImpl_m1_7104 (MonoGenericClass_t1_485 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.MonoGenericClass::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C" PropertyInfo_t * MonoGenericClass_GetPropertyImpl_m1_7105 (MonoGenericClass_t1_485 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, Type_t * ___returnType, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.MonoGenericClass::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" ConstructorInfo_t1_478 * MonoGenericClass_GetConstructorImpl_m1_7106 (MonoGenericClass_t1_485 * __this, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MonoGenericClass::IsDefined(System.Type,System.Boolean)
extern "C" bool MonoGenericClass_IsDefined_m1_7107 (MonoGenericClass_t1_485 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.MonoGenericClass::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MonoGenericClass_GetCustomAttributes_m1_7108 (MonoGenericClass_t1_485 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.MonoGenericClass::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MonoGenericClass_GetCustomAttributes_m1_7109 (MonoGenericClass_t1_485 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
