﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.STAThreadAttribute
struct STAThreadAttribute_t1_1596;

#include "codegen/il2cpp-codegen.h"

// System.Void System.STAThreadAttribute::.ctor()
extern "C" void STAThreadAttribute__ctor_m1_14579 (STAThreadAttribute_t1_1596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
