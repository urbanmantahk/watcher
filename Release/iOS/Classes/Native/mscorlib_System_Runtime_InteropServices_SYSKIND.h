﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_SYSKIND.h"

// System.Runtime.InteropServices.SYSKIND
struct  SYSKIND_t1_823 
{
	// System.Int32 System.Runtime.InteropServices.SYSKIND::value__
	int32_t ___value___1;
};
