﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.NotFiniteNumberException
struct NotFiniteNumberException_t1_1581;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.NotFiniteNumberException::.ctor()
extern "C" void NotFiniteNumberException__ctor_m1_14403 (NotFiniteNumberException_t1_1581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::.ctor(System.Double)
extern "C" void NotFiniteNumberException__ctor_m1_14404 (NotFiniteNumberException_t1_1581 * __this, double ___offendingNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::.ctor(System.String)
extern "C" void NotFiniteNumberException__ctor_m1_14405 (NotFiniteNumberException_t1_1581 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::.ctor(System.String,System.Double)
extern "C" void NotFiniteNumberException__ctor_m1_14406 (NotFiniteNumberException_t1_1581 * __this, String_t* ___message, double ___offendingNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::.ctor(System.String,System.Double,System.Exception)
extern "C" void NotFiniteNumberException__ctor_m1_14407 (NotFiniteNumberException_t1_1581 * __this, String_t* ___message, double ___offendingNumber, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void NotFiniteNumberException__ctor_m1_14408 (NotFiniteNumberException_t1_1581 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::.ctor(System.String,System.Exception)
extern "C" void NotFiniteNumberException__ctor_m1_14409 (NotFiniteNumberException_t1_1581 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.NotFiniteNumberException::get_OffendingNumber()
extern "C" double NotFiniteNumberException_get_OffendingNumber_m1_14410 (NotFiniteNumberException_t1_1581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotFiniteNumberException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void NotFiniteNumberException_GetObjectData_m1_14411 (NotFiniteNumberException_t1_1581 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
