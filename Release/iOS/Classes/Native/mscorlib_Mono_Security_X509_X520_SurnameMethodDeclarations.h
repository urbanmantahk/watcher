﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/Surname
struct Surname_t1_215;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/Surname::.ctor()
extern "C" void Surname__ctor_m1_2415 (Surname_t1_215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
