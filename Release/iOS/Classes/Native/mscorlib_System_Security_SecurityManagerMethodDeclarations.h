﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.AppDomain
struct AppDomain_t1_1403;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.Evidence[]
struct EvidenceU5BU5D_t1_1736;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Type
struct Type_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_PolicyLevelType.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityActions.h"

// System.Void System.Security.SecurityManager::.cctor()
extern "C" void SecurityManager__cctor_m1_12115 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::get_CheckExecutionRights()
extern "C" bool SecurityManager_get_CheckExecutionRights_m1_12116 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::set_CheckExecutionRights(System.Boolean)
extern "C" void SecurityManager_set_CheckExecutionRights_m1_12117 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::get_SecurityEnabled()
extern "C" bool SecurityManager_get_SecurityEnabled_m1_12118 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::set_SecurityEnabled(System.Boolean)
extern "C" void SecurityManager_set_SecurityEnabled_m1_12119 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::GetZoneAndOrigin(System.Collections.ArrayList&,System.Collections.ArrayList&)
extern "C" void SecurityManager_GetZoneAndOrigin_m1_12120 (Object_t * __this /* static, unused */, ArrayList_t1_170 ** ___zone, ArrayList_t1_170 ** ___origin, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::IsGranted(System.Security.IPermission)
extern "C" bool SecurityManager_IsGranted_m1_12121 (Object_t * __this /* static, unused */, Object_t * ___perm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::IsGranted(System.Reflection.Assembly,System.Security.IPermission)
extern "C" bool SecurityManager_IsGranted_m1_12122 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, Object_t * ___perm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityManager::CheckPermissionSet(System.Reflection.Assembly,System.Security.PermissionSet,System.Boolean)
extern "C" Object_t * SecurityManager_CheckPermissionSet_m1_12123 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, PermissionSet_t1_563 * ___ps, bool ___noncas, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityManager::CheckPermissionSet(System.AppDomain,System.Security.PermissionSet)
extern "C" Object_t * SecurityManager_CheckPermissionSet_m1_12124 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___ad, PermissionSet_t1_563 * ___ps, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.SecurityManager::LoadPolicyLevelFromFile(System.String,System.Security.PolicyLevelType)
extern "C" PolicyLevel_t1_1357 * SecurityManager_LoadPolicyLevelFromFile_m1_12125 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.SecurityManager::LoadPolicyLevelFromString(System.String,System.Security.PolicyLevelType)
extern "C" PolicyLevel_t1_1357 * SecurityManager_LoadPolicyLevelFromString_m1_12126 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.SecurityManager::PolicyHierarchy()
extern "C" Object_t * SecurityManager_PolicyHierarchy_m1_12127 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolvePolicy(System.Security.Policy.Evidence)
extern "C" PermissionSet_t1_563 * SecurityManager_ResolvePolicy_m1_12128 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolvePolicy(System.Security.Policy.Evidence[])
extern "C" PermissionSet_t1_563 * SecurityManager_ResolvePolicy_m1_12129 (Object_t * __this /* static, unused */, EvidenceU5BU5D_t1_1736* ___evidences, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolveSystemPolicy(System.Security.Policy.Evidence)
extern "C" PermissionSet_t1_563 * SecurityManager_ResolveSystemPolicy_m1_12130 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::ResolvePolicy(System.Security.Policy.Evidence,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet&)
extern "C" PermissionSet_t1_563 * SecurityManager_ResolvePolicy_m1_12131 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___evidence, PermissionSet_t1_563 * ___reqdPset, PermissionSet_t1_563 * ___optPset, PermissionSet_t1_563 * ___denyPset, PermissionSet_t1_563 ** ___denied, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.SecurityManager::ResolvePolicyGroups(System.Security.Policy.Evidence)
extern "C" Object_t * SecurityManager_ResolvePolicyGroups_m1_12132 (Object_t * __this /* static, unused */, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::SavePolicy()
extern "C" void SecurityManager_SavePolicy_m1_12133 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::SavePolicyLevel(System.Security.Policy.PolicyLevel)
extern "C" void SecurityManager_SavePolicyLevel_m1_12134 (Object_t * __this /* static, unused */, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.SecurityManager::get_Hierarchy()
extern "C" Object_t * SecurityManager_get_Hierarchy_m1_12135 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InitializePolicyHierarchy()
extern "C" void SecurityManager_InitializePolicyHierarchy_m1_12136 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::ResolvePolicyLevel(System.Security.PermissionSet&,System.Security.Policy.PolicyLevel,System.Security.Policy.Evidence)
extern "C" bool SecurityManager_ResolvePolicyLevel_m1_12137 (Object_t * __this /* static, unused */, PermissionSet_t1_563 ** ___ps, PolicyLevel_t1_1357 * ___pl, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::ResolveIdentityPermissions(System.Security.PermissionSet,System.Security.Policy.Evidence)
extern "C" void SecurityManager_ResolveIdentityPermissions_m1_12138 (Object_t * __this /* static, unused */, PermissionSet_t1_563 * ___ps, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.SecurityManager::get_ResolvingPolicyLevel()
extern "C" PolicyLevel_t1_1357 * SecurityManager_get_ResolvingPolicyLevel_m1_12139 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::set_ResolvingPolicyLevel(System.Security.Policy.PolicyLevel)
extern "C" void SecurityManager_set_ResolvingPolicyLevel_m1_12140 (Object_t * __this /* static, unused */, PolicyLevel_t1_1357 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::Decode(System.IntPtr,System.Int32)
extern "C" PermissionSet_t1_563 * SecurityManager_Decode_m1_12141 (Object_t * __this /* static, unused */, IntPtr_t ___permissions, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.SecurityManager::Decode(System.Byte[])
extern "C" PermissionSet_t1_563 * SecurityManager_Decode_m1_12142 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___encodedPermissions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.SecurityManager::get_UnmanagedCode()
extern "C" Object_t * SecurityManager_get_UnmanagedCode_m1_12143 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::GetLinkDemandSecurity(System.Reflection.MethodBase,System.Security.RuntimeDeclSecurityActions*,System.Security.RuntimeDeclSecurityActions*)
extern "C" bool SecurityManager_GetLinkDemandSecurity_m1_12144 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___method, RuntimeDeclSecurityActions_t1_1405 * ___cdecl, RuntimeDeclSecurityActions_t1_1405 * ___mdecl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::ReflectedLinkDemandInvoke(System.Reflection.MethodBase)
extern "C" void SecurityManager_ReflectedLinkDemandInvoke_m1_12145 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::ReflectedLinkDemandQuery(System.Reflection.MethodBase)
extern "C" bool SecurityManager_ReflectedLinkDemandQuery_m1_12146 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::LinkDemand(System.Reflection.Assembly,System.Security.RuntimeDeclSecurityActions*,System.Security.RuntimeDeclSecurityActions*)
extern "C" bool SecurityManager_LinkDemand_m1_12147 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, RuntimeDeclSecurityActions_t1_1405 * ___klass, RuntimeDeclSecurityActions_t1_1405 * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::LinkDemandFullTrust(System.Reflection.Assembly)
extern "C" bool SecurityManager_LinkDemandFullTrust_m1_12148 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::LinkDemandUnmanaged(System.Reflection.Assembly)
extern "C" bool SecurityManager_LinkDemandUnmanaged_m1_12149 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::LinkDemandSecurityException(System.Int32,System.IntPtr)
extern "C" void SecurityManager_LinkDemandSecurityException_m1_12150 (Object_t * __this /* static, unused */, int32_t ___securityViolation, IntPtr_t ___methodHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InheritanceDemandSecurityException(System.Int32,System.Reflection.Assembly,System.Type,System.Reflection.MethodInfo)
extern "C" void SecurityManager_InheritanceDemandSecurityException_m1_12151 (Object_t * __this /* static, unused */, int32_t ___securityViolation, Assembly_t1_467 * ___a, Type_t * ___t, MethodInfo_t * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::ThrowException(System.Exception)
extern "C" void SecurityManager_ThrowException_m1_12152 (Object_t * __this /* static, unused */, Exception_t1_33 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityManager::InheritanceDemand(System.AppDomain,System.Reflection.Assembly,System.Security.RuntimeDeclSecurityActions*)
extern "C" bool SecurityManager_InheritanceDemand_m1_12153 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___ad, Assembly_t1_467 * ___a, RuntimeDeclSecurityActions_t1_1405 * ___actions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::DemandUnmanaged()
extern "C" void SecurityManager_DemandUnmanaged_m1_12154 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InternalDemand(System.IntPtr,System.Int32)
extern "C" void SecurityManager_InternalDemand_m1_12155 (Object_t * __this /* static, unused */, IntPtr_t ___permissions, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityManager::InternalDemandChoice(System.IntPtr,System.Int32)
extern "C" void SecurityManager_InternalDemandChoice_m1_12156 (Object_t * __this /* static, unused */, IntPtr_t ___permissions, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
