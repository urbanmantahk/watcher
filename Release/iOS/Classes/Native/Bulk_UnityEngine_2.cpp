﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Object_t;
// System.Type
struct Type_t;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t6_20;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t6_222;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t6_221;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t6_223;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t6_224;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t6_225;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t6_22;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t6_228;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t6_226;
// System.String[]
struct StringU5BU5D_t1_238;
// UnityEngine.GUILayer
struct GUILayer_t6_31;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t6_236;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t6_237;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t6_238;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t6_239;
// UnityEngine.RangeAttribute
struct RangeAttribute_t6_240;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_t6_241;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t6_242;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t6_243;
// UnityEngine.SliderState
struct SliderState_t6_244;
// UnityEngine.GUIStyle
struct GUIStyle_t6_176;
// UnityEngine.Event
struct Event_t6_162;
struct Event_t6_162_marshaled;
// UnityEngine.StackTraceUtility
struct StackTraceUtility_t6_246;
// System.Diagnostics.StackTrace
struct StackTrace_t1_336;
// UnityEngine.UnityException
struct UnityException_t6_247;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t6_248;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t6_249;
// UnityEngine.TextEditor
struct TextEditor_t6_254;
// UnityEngine.TrackedReference
struct TrackedReference_t6_137;
struct TrackedReference_t6_137_marshaled;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t6_256;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t6_257;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Delegate
struct Delegate_t1_22;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t6_258;
// UnityEngine.Events.UnityAction
struct UnityAction_t6_259;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t6_261;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t6_264;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6_262;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t6_263;
// System.Type[]
struct TypeU5BU5D_t1_31;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t6_265;
// UnityEngine.Internal.DefaultValueAttribute
struct DefaultValueAttribute_t6_266;
// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t6_267;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t6_268;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t6_270;
// UnityEngineInternal.GenericStack
struct GenericStack_t6_170;
// UnityEngine.Advertisements.UnityAdsDelegate
struct UnityAdsDelegate_t6_106;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMaskMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOpMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStateObjects.h"
#include "UnityEngine_UnityEngine_GUIStateObjectsMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_4.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_GUILayer.h"
#include "UnityEngine_UnityEngine_GUIElement.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_RenderTexture.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HeaderAttribute.h"
#include "UnityEngine_UnityEngine_HeaderAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RangeAttribute.h"
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MultilineAttribute.h"
#include "UnityEngine_UnityEngine_MultilineAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState.h"
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler.h"
#include "UnityEngine_UnityEngine_SliderHandlerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClockMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackFrame.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_ParameterInfo.h"
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException.h"
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "UnityEngine_UnityEngine_SystemClock.h"
#include "mscorlib_System_DateTimeKind.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor.h"
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5MethodDeclarations.h"
#include "mscorlib_System_CharMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_TrackedReference.h"
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_Delegate.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtensionsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2.h"
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_20.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_21MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_21.h"
#include "mscorlib_System_Predicate_1_genMethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_Binder.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
#include "mscorlib_System_Collections_Stack.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtensions.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegateMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m6_1840_gshared (Component_t6_26 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m6_1840(__this, method) (( Object_t * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1840_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t6_31_m6_1835(__this, method) (( GUILayer_t6_31 * (*) (Component_t6_26 *, const MethodInfo*))Component_GetComponent_TisObject_t_m6_1840_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.GUIStateObjects::.cctor()
extern TypeInfo* Dictionary_2_t1_1830_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStateObjects_t6_220_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14966_MethodInfo_var;
extern "C" void GUIStateObjects__cctor_m6_1552 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1830_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1682);
		GUIStateObjects_t6_220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1670);
		Dictionary_2__ctor_m1_14966_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483810);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1830 * L_0 = (Dictionary_2_t1_1830 *)il2cpp_codegen_object_new (Dictionary_2_t1_1830_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14966(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14966_MethodInfo_var);
		((GUIStateObjects_t6_220_StaticFields*)GUIStateObjects_t6_220_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.GUIStateObjects::GetStateObject(System.Type,System.Int32)
extern TypeInfo* GUIStateObjects_t6_220_il2cpp_TypeInfo_var;
extern "C" Object_t * GUIStateObjects_GetStateObject_m6_1553 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___controlID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStateObjects_t6_220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1670);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t6_220_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1830 * L_0 = ((GUIStateObjects_t6_220_StaticFields*)GUIStateObjects_t6_220_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0;
		int32_t L_1 = ___controlID;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, int32_t, Object_t ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Object_t * L_3 = V_0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = ___t;
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0031;
		}
	}

IL_001e:
	{
		Type_t * L_6 = ___t;
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t6_220_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1830 * L_8 = ((GUIStateObjects_t6_220_StaticFields*)GUIStateObjects_t6_220_il2cpp_TypeInfo_var->static_fields)->___s_StateCache_0;
		int32_t L_9 = ___controlID;
		Object_t * L_10 = V_0;
		NullCheck(L_8);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1) */, L_8, L_9, L_10);
	}

IL_0031:
	{
		Object_t * L_11 = V_0;
		return L_11;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern TypeInfo* UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var;
extern "C" void LocalUser__ctor_m6_1554 (LocalUser_t6_20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1561);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m6_1559(__this, /*hidden argument*/NULL);
		__this->___m_Friends_5 = (IUserProfileU5BU5D_t6_222*)((UserProfileU5BU5D_t6_19*)SZArrayNew(UserProfileU5BU5D_t6_19_il2cpp_TypeInfo_var, 0));
		__this->___m_Authenticated_6 = 0;
		__this->___m_Underage_7 = 0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C" void LocalUser_SetFriends_m6_1555 (LocalUser_t6_20 * __this, IUserProfileU5BU5D_t6_222* ___friends, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t6_222* L_0 = ___friends;
		__this->___m_Friends_5 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C" void LocalUser_SetAuthenticated_m6_1556 (LocalUser_t6_20 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_Authenticated_6 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C" void LocalUser_SetUnderage_m6_1557 (LocalUser_t6_20 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___m_Underage_7 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C" bool LocalUser_get_authenticated_m6_1558 (LocalUser_t6_20 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Authenticated_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern TypeInfo* Texture2D_t6_33_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4860;
extern Il2CppCodeGenString* _stringLiteral166;
extern "C" void UserProfile__ctor_m6_1559 (UserProfile_t6_221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t6_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1581);
		_stringLiteral4860 = il2cpp_codegen_string_literal_from_index(4860);
		_stringLiteral166 = il2cpp_codegen_string_literal_from_index(166);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		__this->___m_UserName_0 = _stringLiteral4860;
		__this->___m_ID_1 = _stringLiteral166;
		__this->___m_IsFriend_2 = 0;
		__this->___m_State_3 = 3;
		Texture2D_t6_33 * L_0 = (Texture2D_t6_33 *)il2cpp_codegen_object_new (Texture2D_t6_33_il2cpp_TypeInfo_var);
		Texture2D__ctor_m6_155(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C" void UserProfile__ctor_m6_1560 (UserProfile_t6_221 * __this, String_t* ___name, String_t* ___id, bool ___friend, int32_t ___state, Texture2D_t6_33 * ___image, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		__this->___m_UserName_0 = L_0;
		String_t* L_1 = ___id;
		__this->___m_ID_1 = L_1;
		bool L_2 = ___friend;
		__this->___m_IsFriend_2 = L_2;
		int32_t L_3 = ___state;
		__this->___m_State_3 = L_3;
		Texture2D_t6_33 * L_4 = ___image;
		__this->___m_Image_4 = L_4;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* UserState_t6_233_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4861;
extern "C" String_t* UserProfile_ToString_m6_1561 (UserProfile_t6_221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		UserState_t6_233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1683);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4861 = il2cpp_codegen_string_literal_from_index(4861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 7));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_272* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_6 = L_5;
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend() */, __this);
		bool L_8 = L_7;
		Object_t * L_9 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_11 = L_10;
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state() */, __this);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(UserState_t6_233_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6, sizeof(Object_t *))) = (Object_t *)L_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1_562(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C" void UserProfile_SetUserName_m6_1562 (UserProfile_t6_221 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		__this->___m_UserName_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C" void UserProfile_SetUserID_m6_1563 (UserProfile_t6_221 * __this, String_t* ___id, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id;
		__this->___m_ID_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C" void UserProfile_SetImage_m6_1564 (UserProfile_t6_221 * __this, Texture2D_t6_33 * ___image, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___image;
		__this->___m_Image_4 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C" String_t* UserProfile_get_userName_m6_1565 (UserProfile_t6_221 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_UserName_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C" String_t* UserProfile_get_id_m6_1566 (UserProfile_t6_221 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_ID_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C" bool UserProfile_get_isFriend_m6_1567 (UserProfile_t6_221 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_IsFriend_2);
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C" int32_t UserProfile_get_state_m6_1568 (UserProfile_t6_221 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_State_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C" void Achievement__ctor_m6_1569 (Achievement_t6_223 * __this, String_t* ___id, double ___percentCompleted, bool ___completed, bool ___hidden, DateTime_t1_150  ___lastReportedDate, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String) */, __this, L_0);
		double L_1 = ___percentCompleted;
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double) */, __this, L_1);
		bool L_2 = ___completed;
		__this->___m_Completed_0 = L_2;
		bool L_3 = ___hidden;
		__this->___m_Hidden_1 = L_3;
		DateTime_t1_150  L_4 = ___lastReportedDate;
		__this->___m_LastReportedDate_2 = L_4;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" void Achievement__ctor_m6_1570 (Achievement_t6_223 * __this, String_t* ___id, double ___percent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String) */, __this, L_0);
		double L_1 = ___percent;
		VirtActionInvoker1< double >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double) */, __this, L_1);
		__this->___m_Hidden_1 = 0;
		__this->___m_Completed_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_2 = ((DateTime_t1_150_StaticFields*)DateTime_t1_150_il2cpp_TypeInfo_var->static_fields)->___MinValue_13;
		__this->___m_LastReportedDate_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral2192;
extern "C" void Achievement__ctor_m6_1571 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral2192 = il2cpp_codegen_string_literal_from_index(2192);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m6_1570(__this, _stringLiteral2192, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t1_18_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4861;
extern "C" String_t* Achievement_ToString_m6_1572 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Double_t1_18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4861 = il2cpp_codegen_string_literal_from_index(4861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)9)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_3 = L_2;
		double L_4 = (double)VirtFuncInvoker0< double >::Invoke(6 /* System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted() */, __this);
		double L_5 = L_4;
		Object_t * L_6 = Box(Double_t1_18_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_272* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_8 = L_7;
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed() */, __this);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 4, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_13 = L_12;
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden() */, __this);
		bool L_15 = L_14;
		Object_t * L_16 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_272* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_18 = L_17;
		DateTime_t1_150  L_19 = (DateTime_t1_150 )VirtFuncInvoker0< DateTime_t1_150  >::Invoke(10 /* System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate() */, __this);
		DateTime_t1_150  L_20 = L_19;
		Object_t * L_21 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 8, sizeof(Object_t *))) = (Object_t *)L_21;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_562(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C" String_t* Achievement_get_id_m6_1573 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C" void Achievement_set_id_m6_1574 (Achievement_t6_223 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C" double Achievement_get_percentCompleted_m6_1575 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	{
		double L_0 = (__this->___U3CpercentCompletedU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C" void Achievement_set_percentCompleted_m6_1576 (Achievement_t6_223 * __this, double ___value, const MethodInfo* method)
{
	{
		double L_0 = ___value;
		__this->___U3CpercentCompletedU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C" bool Achievement_get_completed_m6_1577 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Completed_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C" bool Achievement_get_hidden_m6_1578 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Hidden_1);
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C" DateTime_t1_150  Achievement_get_lastReportedDate_m6_1579 (Achievement_t6_223 * __this, const MethodInfo* method)
{
	{
		DateTime_t1_150  L_0 = (__this->___m_LastReportedDate_2);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C" void AchievementDescription__ctor_m6_1580 (AchievementDescription_t6_224 * __this, String_t* ___id, String_t* ___title, Texture2D_t6_33 * ___image, String_t* ___achievedDescription, String_t* ___unachievedDescription, bool ___hidden, int32_t ___points, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String) */, __this, L_0);
		String_t* L_1 = ___title;
		__this->___m_Title_0 = L_1;
		Texture2D_t6_33 * L_2 = ___image;
		__this->___m_Image_1 = L_2;
		String_t* L_3 = ___achievedDescription;
		__this->___m_AchievedDescription_2 = L_3;
		String_t* L_4 = ___unachievedDescription;
		__this->___m_UnachievedDescription_3 = L_4;
		bool L_5 = ___hidden;
		__this->___m_Hidden_4 = L_5;
		int32_t L_6 = ___points;
		__this->___m_Points_5 = L_6;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4861;
extern "C" String_t* AchievementDescription_ToString_m6_1581 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4861 = il2cpp_codegen_string_literal_from_index(4861);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)11)));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id() */, __this);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t1_272* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_3 = L_2;
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title() */, __this);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_272* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_6 = L_5;
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription() */, __this);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 5, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_9 = L_8;
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription() */, __this);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 6, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_272* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_12 = L_11;
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points() */, __this);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 8, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_272* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral4861);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral4861;
		ObjectU5BU5D_t1_272* L_17 = L_16;
		bool L_18 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden() */, __this);
		bool L_19 = L_18;
		Object_t * L_20 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_562(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C" void AchievementDescription_SetImage_m6_1582 (AchievementDescription_t6_224 * __this, Texture2D_t6_33 * ___image, const MethodInfo* method)
{
	{
		Texture2D_t6_33 * L_0 = ___image;
		__this->___m_Image_1 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C" String_t* AchievementDescription_get_id_m6_1583 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C" void AchievementDescription_set_id_m6_1584 (AchievementDescription_t6_224 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C" String_t* AchievementDescription_get_title_m6_1585 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Title_0);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C" String_t* AchievementDescription_get_achievedDescription_m6_1586 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_AchievedDescription_2);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C" String_t* AchievementDescription_get_unachievedDescription_m6_1587 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_UnachievedDescription_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C" bool AchievementDescription_get_hidden_m6_1588 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Hidden_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C" int32_t AchievementDescription_get_points_m6_1589 (AchievementDescription_t6_224 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Points_5);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral166;
extern "C" void Score__ctor_m6_1590 (Score_t6_225 * __this, String_t* ___leaderboardID, int64_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral166 = il2cpp_codegen_string_literal_from_index(166);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int64_t L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_2 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		Score__ctor_m6_1591(__this, L_0, L_1, _stringLiteral166, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C" void Score__ctor_m6_1591 (Score_t6_225 * __this, String_t* ___leaderboardID, int64_t ___value, String_t* ___userID, DateTime_t1_150  ___date, String_t* ___formattedValue, int32_t ___rank, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String) */, __this, L_0);
		int64_t L_1 = ___value;
		VirtActionInvoker1< int64_t >::Invoke(7 /* System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64) */, __this, L_1);
		String_t* L_2 = ___userID;
		__this->___m_UserID_2 = L_2;
		DateTime_t1_150  L_3 = ___date;
		__this->___m_Date_0 = L_3;
		String_t* L_4 = ___formattedValue;
		__this->___m_FormattedValue_1 = L_4;
		int32_t L_5 = ___rank;
		__this->___m_Rank_3 = L_5;
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t1_7_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4862;
extern Il2CppCodeGenString* _stringLiteral4863;
extern Il2CppCodeGenString* _stringLiteral4864;
extern Il2CppCodeGenString* _stringLiteral4865;
extern Il2CppCodeGenString* _stringLiteral4866;
extern "C" String_t* Score_ToString_m6_1592 (Score_t6_225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int64_t1_7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4862 = il2cpp_codegen_string_literal_from_index(4862);
		_stringLiteral4863 = il2cpp_codegen_string_literal_from_index(4863);
		_stringLiteral4864 = il2cpp_codegen_string_literal_from_index(4864);
		_stringLiteral4865 = il2cpp_codegen_string_literal_from_index(4865);
		_stringLiteral4866 = il2cpp_codegen_string_literal_from_index(4866);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral4862);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4862;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		int32_t L_2 = (__this->___m_Rank_3);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_4;
		ObjectU5BU5D_t1_272* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral4863);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4863;
		ObjectU5BU5D_t1_272* L_6 = L_5;
		int64_t L_7 = (int64_t)VirtFuncInvoker0< int64_t >::Invoke(6 /* System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value() */, __this);
		int64_t L_8 = L_7;
		Object_t * L_9 = Box(Int64_t1_7_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral4864);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4864;
		ObjectU5BU5D_t1_272* L_11 = L_10;
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID() */, __this);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t1_272* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral4865);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral4865;
		ObjectU5BU5D_t1_272* L_14 = L_13;
		String_t* L_15 = (__this->___m_UserID_2);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 7, sizeof(Object_t *))) = (Object_t *)L_15;
		ObjectU5BU5D_t1_272* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral4866);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral4866;
		ObjectU5BU5D_t1_272* L_17 = L_16;
		DateTime_t1_150  L_18 = (__this->___m_Date_0);
		DateTime_t1_150  L_19 = L_18;
		Object_t * L_20 = Box(DateTime_t1_150_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_20;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_562(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C" String_t* Score_get_leaderboardID_m6_1593 (Score_t6_225 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CleaderboardIDU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C" void Score_set_leaderboardID_m6_1594 (Score_t6_225 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CleaderboardIDU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C" int64_t Score_get_value_m6_1595 (Score_t6_225 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (__this->___U3CvalueU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C" void Score_set_value_m6_1596 (Score_t6_225 * __this, int64_t ___value, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		__this->___U3CvalueU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern TypeInfo* Score_t6_225_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4867;
extern "C" void Leaderboard__ctor_m6_1597 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t6_225_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1573);
		ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1572);
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		_stringLiteral4867 = il2cpp_codegen_string_literal_from_index(4867);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String) */, __this, _stringLiteral4867);
		Range_t6_227  L_0 = {0};
		Range__ctor_m6_1619(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		VirtActionInvoker1< Range_t6_227  >::Invoke(10 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range) */, __this, L_0);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope) */, __this, 0);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope) */, __this, 2);
		__this->___m_Loading_0 = 0;
		Score_t6_225 * L_1 = (Score_t6_225 *)il2cpp_codegen_object_new (Score_t6_225_il2cpp_TypeInfo_var);
		Score__ctor_m6_1590(L_1, _stringLiteral4867, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->___m_LocalUserScore_1 = L_1;
		__this->___m_MaxRange_2 = 0;
		__this->___m_Scores_3 = (IScoreU5BU5D_t6_226*)((ScoreU5BU5D_t6_294*)SZArrayNew(ScoreU5BU5D_t6_294_il2cpp_TypeInfo_var, 0));
		__this->___m_Title_4 = _stringLiteral4867;
		__this->___m_UserIDs_5 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t1_8_il2cpp_TypeInfo_var;
extern TypeInfo* UserScope_t6_234_il2cpp_TypeInfo_var;
extern TypeInfo* TimeScope_t6_235_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4868;
extern Il2CppCodeGenString* _stringLiteral4869;
extern Il2CppCodeGenString* _stringLiteral4870;
extern Il2CppCodeGenString* _stringLiteral4871;
extern Il2CppCodeGenString* _stringLiteral265;
extern Il2CppCodeGenString* _stringLiteral4872;
extern Il2CppCodeGenString* _stringLiteral4873;
extern Il2CppCodeGenString* _stringLiteral4874;
extern Il2CppCodeGenString* _stringLiteral4875;
extern Il2CppCodeGenString* _stringLiteral4876;
extern "C" String_t* Leaderboard_ToString_m6_1598 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		UInt32_t1_8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		UserScope_t6_234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1684);
		TimeScope_t6_235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1685);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4868 = il2cpp_codegen_string_literal_from_index(4868);
		_stringLiteral4869 = il2cpp_codegen_string_literal_from_index(4869);
		_stringLiteral4870 = il2cpp_codegen_string_literal_from_index(4870);
		_stringLiteral4871 = il2cpp_codegen_string_literal_from_index(4871);
		_stringLiteral265 = il2cpp_codegen_string_literal_from_index(265);
		_stringLiteral4872 = il2cpp_codegen_string_literal_from_index(4872);
		_stringLiteral4873 = il2cpp_codegen_string_literal_from_index(4873);
		_stringLiteral4874 = il2cpp_codegen_string_literal_from_index(4874);
		_stringLiteral4875 = il2cpp_codegen_string_literal_from_index(4875);
		_stringLiteral4876 = il2cpp_codegen_string_literal_from_index(4876);
		s_Il2CppMethodIntialized = true;
	}
	Range_t6_227  V_0 = {0};
	Range_t6_227  V_1 = {0};
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral4868);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4868;
		ObjectU5BU5D_t1_272* L_1 = L_0;
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id() */, __this);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1, sizeof(Object_t *))) = (Object_t *)L_2;
		ObjectU5BU5D_t1_272* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral4869);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4869;
		ObjectU5BU5D_t1_272* L_4 = L_3;
		String_t* L_5 = (__this->___m_Title_4);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 3, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral4870);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4870;
		ObjectU5BU5D_t1_272* L_7 = L_6;
		bool L_8 = (__this->___m_Loading_0);
		bool L_9 = L_8;
		Object_t * L_10 = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 5, sizeof(Object_t *))) = (Object_t *)L_10;
		ObjectU5BU5D_t1_272* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral4871);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral4871;
		ObjectU5BU5D_t1_272* L_12 = L_11;
		Range_t6_227  L_13 = (Range_t6_227 )VirtFuncInvoker0< Range_t6_227  >::Invoke(6 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range() */, __this);
		V_0 = L_13;
		int32_t L_14 = ((&V_0)->___from_0);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 7, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_272* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral265);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 8, sizeof(Object_t *))) = (Object_t *)_stringLiteral265;
		ObjectU5BU5D_t1_272* L_18 = L_17;
		Range_t6_227  L_19 = (Range_t6_227 )VirtFuncInvoker0< Range_t6_227  >::Invoke(6 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range() */, __this);
		V_1 = L_19;
		int32_t L_20 = ((&V_1)->___count_1);
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t1_272* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral4872);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)_stringLiteral4872;
		ObjectU5BU5D_t1_272* L_24 = L_23;
		uint32_t L_25 = (__this->___m_MaxRange_2);
		uint32_t L_26 = L_25;
		Object_t * L_27 = Box(UInt32_t1_8_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, ((int32_t)11), sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_272* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral4873);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, ((int32_t)12), sizeof(Object_t *))) = (Object_t *)_stringLiteral4873;
		ObjectU5BU5D_t1_272* L_29 = L_28;
		IScoreU5BU5D_t6_226* L_30 = (__this->___m_Scores_3);
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Array_t *)L_30)->max_length))));
		Object_t * L_32 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, ((int32_t)13), sizeof(Object_t *))) = (Object_t *)L_32;
		ObjectU5BU5D_t1_272* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral4874);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, ((int32_t)14), sizeof(Object_t *))) = (Object_t *)_stringLiteral4874;
		ObjectU5BU5D_t1_272* L_34 = L_33;
		int32_t L_35 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope() */, __this);
		int32_t L_36 = L_35;
		Object_t * L_37 = Box(UserScope_t6_234_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_34, ((int32_t)15), sizeof(Object_t *))) = (Object_t *)L_37;
		ObjectU5BU5D_t1_272* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral4875);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, ((int32_t)16), sizeof(Object_t *))) = (Object_t *)_stringLiteral4875;
		ObjectU5BU5D_t1_272* L_39 = L_38;
		int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(7 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope() */, __this);
		int32_t L_41 = L_40;
		Object_t * L_42 = Box(TimeScope_t6_235_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_39, ((int32_t)17), sizeof(Object_t *))) = (Object_t *)L_42;
		ObjectU5BU5D_t1_272* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral4876);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, ((int32_t)18), sizeof(Object_t *))) = (Object_t *)_stringLiteral4876;
		ObjectU5BU5D_t1_272* L_44 = L_43;
		StringU5BU5D_t1_238* L_45 = (__this->___m_UserIDs_5);
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Array_t *)L_45)->max_length))));
		Object_t * L_47 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)19), sizeof(Object_t *))) = (Object_t *)L_47;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m1_562(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C" void Leaderboard_SetLocalUserScore_m6_1599 (Leaderboard_t6_22 * __this, Object_t * ___score, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___score;
		__this->___m_LocalUserScore_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C" void Leaderboard_SetMaxRange_m6_1600 (Leaderboard_t6_22 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange;
		__this->___m_MaxRange_2 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C" void Leaderboard_SetScores_m6_1601 (Leaderboard_t6_22 * __this, IScoreU5BU5D_t6_226* ___scores, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t6_226* L_0 = ___scores;
		__this->___m_Scores_3 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C" void Leaderboard_SetTitle_m6_1602 (Leaderboard_t6_22 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title;
		__this->___m_Title_4 = L_0;
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C" StringU5BU5D_t1_238* Leaderboard_GetUserFilter_m6_1603 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1_238* L_0 = (__this->___m_UserIDs_5);
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C" String_t* Leaderboard_get_id_m6_1604 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CidU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C" void Leaderboard_set_id_m6_1605 (Leaderboard_t6_22 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CidU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C" int32_t Leaderboard_get_userScope_m6_1606 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CuserScopeU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C" void Leaderboard_set_userScope_m6_1607 (Leaderboard_t6_22 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CuserScopeU3Ek__BackingField_7 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C" Range_t6_227  Leaderboard_get_range_m6_1608 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	{
		Range_t6_227  L_0 = (__this->___U3CrangeU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C" void Leaderboard_set_range_m6_1609 (Leaderboard_t6_22 * __this, Range_t6_227  ___value, const MethodInfo* method)
{
	{
		Range_t6_227  L_0 = ___value;
		__this->___U3CrangeU3Ek__BackingField_8 = L_0;
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C" int32_t Leaderboard_get_timeScope_m6_1610 (Leaderboard_t6_22 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CtimeScopeU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C" void Leaderboard_set_timeScope_m6_1611 (Leaderboard_t6_22 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CtimeScopeU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C" void HitInfo_SendMessage_m6_1612 (HitInfo_t6_229 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = (__this->___target_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		GameObject_SendMessage_m6_745(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern "C" bool HitInfo_Compare_m6_1613 (Object_t * __this /* static, unused */, HitInfo_t6_229  ___lhs, HitInfo_t6_229  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t6_97 * L_0 = ((&___lhs)->___target_0);
		GameObject_t6_97 * L_1 = ((&___rhs)->___target_0);
		bool L_2 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t6_86 * L_3 = ((&___lhs)->___camera_1);
		Camera_t6_86 * L_4 = ((&___rhs)->___camera_1);
		bool L_5 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern "C" bool HitInfo_op_Implicit_m6_1614 (Object_t * __this /* static, unused */, HitInfo_t6_229  ___exists, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		GameObject_t6_97 * L_0 = ((&___exists)->___target_0);
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t6_86 * L_2 = ((&___exists)->___camera_1);
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_2, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern TypeInfo* SendMouseEvents_t6_230_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfoU5BU5D_t6_231_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t6_229_il2cpp_TypeInfo_var;
extern "C" void SendMouseEvents__cctor_m6_1615 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SendMouseEvents_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1686);
		HitInfoU5BU5D_t6_231_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1687);
		HitInfo_t6_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1688);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t6_229  V_0 = {0};
	HitInfo_t6_229  V_1 = {0};
	HitInfo_t6_229  V_2 = {0};
	HitInfo_t6_229  V_3 = {0};
	HitInfo_t6_229  V_4 = {0};
	HitInfo_t6_229  V_5 = {0};
	HitInfo_t6_229  V_6 = {0};
	HitInfo_t6_229  V_7 = {0};
	HitInfo_t6_229  V_8 = {0};
	{
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3 = 0;
		HitInfoU5BU5D_t6_231* L_0 = ((HitInfoU5BU5D_t6_231*)SZArrayNew(HitInfoU5BU5D_t6_231_il2cpp_TypeInfo_var, 3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t6_229  L_1 = V_0;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_0, 0, sizeof(HitInfo_t6_229 )))) = L_1;
		HitInfoU5BU5D_t6_231* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t6_229  L_3 = V_1;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_2, 1, sizeof(HitInfo_t6_229 )))) = L_3;
		HitInfoU5BU5D_t6_231* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t6_229  L_5 = V_2;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_4, 2, sizeof(HitInfo_t6_229 )))) = L_5;
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4 = L_4;
		HitInfoU5BU5D_t6_231* L_6 = ((HitInfoU5BU5D_t6_231*)SZArrayNew(HitInfoU5BU5D_t6_231_il2cpp_TypeInfo_var, 3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t6_229  L_7 = V_3;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_6, 0, sizeof(HitInfo_t6_229 )))) = L_7;
		HitInfoU5BU5D_t6_231* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t6_229  L_9 = V_4;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_8, 1, sizeof(HitInfo_t6_229 )))) = L_9;
		HitInfoU5BU5D_t6_231* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t6_229  L_11 = V_5;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_10, 2, sizeof(HitInfo_t6_229 )))) = L_11;
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5 = L_10;
		HitInfoU5BU5D_t6_231* L_12 = ((HitInfoU5BU5D_t6_231*)SZArrayNew(HitInfoU5BU5D_t6_231_il2cpp_TypeInfo_var, 3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t6_229  L_13 = V_6;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_12, 0, sizeof(HitInfo_t6_229 )))) = L_13;
		HitInfoU5BU5D_t6_231* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t6_229  L_15 = V_7;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_14, 1, sizeof(HitInfo_t6_229 )))) = L_15;
		HitInfoU5BU5D_t6_231* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t6_229  L_17 = V_8;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_16, 2, sizeof(HitInfo_t6_229 )))) = L_17;
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6 = L_16;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern TypeInfo* SendMouseEvents_t6_230_il2cpp_TypeInfo_var;
extern "C" void SendMouseEvents_SetMouseMoved_m6_1616 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SendMouseEvents_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1686);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3 = 1;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t6_230_il2cpp_TypeInfo_var;
extern TypeInfo* CameraU5BU5D_t6_232_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t6_229_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t6_31_m6_1835_MethodInfo_var;
extern "C" void SendMouseEvents_DoSendMouseEvents_m6_1617 (Object_t * __this /* static, unused */, int32_t ___skipRTCameras, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		SendMouseEvents_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1686);
		CameraU5BU5D_t6_232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1690);
		HitInfo_t6_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1688);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		Component_GetComponent_TisGUILayer_t6_31_m6_1835_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483811);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_48  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t6_86 * V_3 = {0};
	CameraU5BU5D_t6_232* V_4 = {0};
	int32_t V_5 = 0;
	Rect_t6_51  V_6 = {0};
	GUILayer_t6_31 * V_7 = {0};
	GUIElement_t6_29 * V_8 = {0};
	Ray_t6_55  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t6_97 * V_12 = {0};
	GameObject_t6_97 * V_13 = {0};
	int32_t V_14 = 0;
	HitInfo_t6_229  V_15 = {0};
	Vector3_t6_48  V_16 = {0};
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Vector3_t6_48  L_0 = Input_get_mousePosition_m6_688(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m6_617(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_232* L_2 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_232* L_3 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7 = ((CameraU5BU5D_t6_232*)SZArrayNew(CameraU5BU5D_t6_232_il2cpp_TypeInfo_var, L_5));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_232* L_6 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		Camera_GetAllCameras_m6_618(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_7 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t6_229  L_9 = V_15;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_7, L_8, sizeof(HitInfo_t6_229 )))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_12 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3;
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		CameraU5BU5D_t6_232* L_14 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_Cameras_7;
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t6_232* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		V_3 = (*(Camera_t6_86 **)(Camera_t6_86 **)SZArrayLdElema(L_15, L_17, sizeof(Camera_t6_86 *)));
		Camera_t6_86 * L_18 = V_3;
		bool L_19 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_18, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_20 = ___skipRTCameras;
		if (!L_20)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t6_86 * L_21 = V_3;
		NullCheck(L_21);
		RenderTexture_t6_34 * L_22 = Camera_get_targetTexture_m6_610(L_21, /*hidden argument*/NULL);
		bool L_23 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_22, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t6_86 * L_24 = V_3;
		NullCheck(L_24);
		Rect_t6_51  L_25 = Camera_get_pixelRect_m6_608(L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		Vector3_t6_48  L_26 = V_0;
		bool L_27 = Rect_Contains_m6_321((&V_6), L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t6_86 * L_28 = V_3;
		NullCheck(L_28);
		GUILayer_t6_31 * L_29 = Component_GetComponent_TisGUILayer_t6_31_m6_1835(L_28, /*hidden argument*/Component_GetComponent_TisGUILayer_t6_31_m6_1835_MethodInfo_var);
		V_7 = L_29;
		GUILayer_t6_31 * L_30 = V_7;
		bool L_31 = Object_op_Implicit_m6_720(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t6_31 * L_32 = V_7;
		Vector3_t6_48  L_33 = V_0;
		NullCheck(L_32);
		GUIElement_t6_29 * L_34 = GUILayer_HitTest_m6_148(L_32, L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		GUIElement_t6_29 * L_35 = V_8;
		bool L_36 = Object_op_Implicit_m6_720(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_37 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		GUIElement_t6_29 * L_38 = V_8;
		NullCheck(L_38);
		GameObject_t6_97 * L_39 = Component_get_gameObject_m6_725(L_38, /*hidden argument*/NULL);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_37, 0, sizeof(HitInfo_t6_229 )))->___target_0 = L_39;
		HitInfoU5BU5D_t6_231* L_40 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		Camera_t6_86 * L_41 = V_3;
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_40, 0, sizeof(HitInfo_t6_229 )))->___camera_1 = L_41;
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_42 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_42, 0, sizeof(HitInfo_t6_229 )))->___target_0 = (GameObject_t6_97 *)NULL;
		HitInfoU5BU5D_t6_231* L_43 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_43, 0, sizeof(HitInfo_t6_229 )))->___camera_1 = (Camera_t6_86 *)NULL;
	}

IL_0145:
	{
		Camera_t6_86 * L_44 = V_3;
		NullCheck(L_44);
		int32_t L_45 = Camera_get_eventMask_m6_607(L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t6_86 * L_46 = V_3;
		Vector3_t6_48  L_47 = V_0;
		NullCheck(L_46);
		Ray_t6_55  L_48 = Camera_ScreenPointToRay_m6_614(L_46, L_47, /*hidden argument*/NULL);
		V_9 = L_48;
		Vector3_t6_48  L_49 = Ray_get_direction_m6_414((&V_9), /*hidden argument*/NULL);
		V_16 = L_49;
		float L_50 = ((&V_16)->___z_3);
		V_10 = L_50;
		float L_51 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_52 = Mathf_Approximately_m6_443(NULL /*static, unused*/, (0.0f), L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t6_86 * L_53 = V_3;
		NullCheck(L_53);
		float L_54 = Camera_get_farClipPlane_m6_604(L_53, /*hidden argument*/NULL);
		Camera_t6_86 * L_55 = V_3;
		NullCheck(L_55);
		float L_56 = Camera_get_nearClipPlane_m6_603(L_55, /*hidden argument*/NULL);
		float L_57 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_58 = fabsf(((float)((float)((float)((float)L_54-(float)L_56))/(float)L_57)));
		G_B23_0 = L_58;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t6_86 * L_59 = V_3;
		Ray_t6_55  L_60 = V_9;
		float L_61 = V_11;
		Camera_t6_86 * L_62 = V_3;
		NullCheck(L_62);
		int32_t L_63 = Camera_get_cullingMask_m6_606(L_62, /*hidden argument*/NULL);
		Camera_t6_86 * L_64 = V_3;
		NullCheck(L_64);
		int32_t L_65 = Camera_get_eventMask_m6_607(L_64, /*hidden argument*/NULL);
		NullCheck(L_59);
		GameObject_t6_97 * L_66 = Camera_RaycastTry_m6_622(L_59, L_60, L_61, ((int32_t)((int32_t)L_63&(int32_t)L_65)), /*hidden argument*/NULL);
		V_12 = L_66;
		GameObject_t6_97 * L_67 = V_12;
		bool L_68 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_67, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_69 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 1);
		GameObject_t6_97 * L_70 = V_12;
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_69, 1, sizeof(HitInfo_t6_229 )))->___target_0 = L_70;
		HitInfoU5BU5D_t6_231* L_71 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, 1);
		Camera_t6_86 * L_72 = V_3;
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_71, 1, sizeof(HitInfo_t6_229 )))->___camera_1 = L_72;
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t6_86 * L_73 = V_3;
		NullCheck(L_73);
		int32_t L_74 = Camera_get_clearFlags_m6_611(L_73, /*hidden argument*/NULL);
		if ((((int32_t)L_74) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t6_86 * L_75 = V_3;
		NullCheck(L_75);
		int32_t L_76 = Camera_get_clearFlags_m6_611(L_75, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_76) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_77 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 1);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_77, 1, sizeof(HitInfo_t6_229 )))->___target_0 = (GameObject_t6_97 *)NULL;
		HitInfoU5BU5D_t6_231* L_78 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_78, 1, sizeof(HitInfo_t6_229 )))->___camera_1 = (Camera_t6_86 *)NULL;
	}

IL_022a:
	{
		Camera_t6_86 * L_79 = V_3;
		Ray_t6_55  L_80 = V_9;
		float L_81 = V_11;
		Camera_t6_86 * L_82 = V_3;
		NullCheck(L_82);
		int32_t L_83 = Camera_get_cullingMask_m6_606(L_82, /*hidden argument*/NULL);
		Camera_t6_86 * L_84 = V_3;
		NullCheck(L_84);
		int32_t L_85 = Camera_get_eventMask_m6_607(L_84, /*hidden argument*/NULL);
		NullCheck(L_79);
		GameObject_t6_97 * L_86 = Camera_RaycastTry2D_m6_624(L_79, L_80, L_81, ((int32_t)((int32_t)L_83&(int32_t)L_85)), /*hidden argument*/NULL);
		V_13 = L_86;
		GameObject_t6_97 * L_87 = V_13;
		bool L_88 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_87, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_89 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 2);
		GameObject_t6_97 * L_90 = V_13;
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_89, 2, sizeof(HitInfo_t6_229 )))->___target_0 = L_90;
		HitInfoU5BU5D_t6_231* L_91 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 2);
		Camera_t6_86 * L_92 = V_3;
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_91, 2, sizeof(HitInfo_t6_229 )))->___camera_1 = L_92;
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t6_86 * L_93 = V_3;
		NullCheck(L_93);
		int32_t L_94 = Camera_get_clearFlags_m6_611(L_93, /*hidden argument*/NULL);
		if ((((int32_t)L_94) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t6_86 * L_95 = V_3;
		NullCheck(L_95);
		int32_t L_96 = Camera_get_clearFlags_m6_611(L_95, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_96) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_97 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 2);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_97, 2, sizeof(HitInfo_t6_229 )))->___target_0 = (GameObject_t6_97 *)NULL;
		HitInfoU5BU5D_t6_231* L_98 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_98, 2, sizeof(HitInfo_t6_229 )))->___camera_1 = (Camera_t6_86 *)NULL;
	}

IL_02b2:
	{
		int32_t L_99 = V_5;
		V_5 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_100 = V_5;
		CameraU5BU5D_t6_232* L_101 = V_4;
		NullCheck(L_101);
		if ((((int32_t)L_100) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_101)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_102 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_103 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		int32_t L_104 = V_14;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		SendMouseEvents_SendEvents_m6_1618(NULL /*static, unused*/, L_102, (*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_103, L_104, sizeof(HitInfo_t6_229 )))), /*hidden argument*/NULL);
		int32_t L_105 = V_14;
		V_14 = ((int32_t)((int32_t)L_105+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_106 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_107 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_CurrentHit_6;
		NullCheck(L_107);
		if ((((int32_t)L_106) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_107)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___s_MouseUsed_3 = 0;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern TypeInfo* SendMouseEvents_t6_230_il2cpp_TypeInfo_var;
extern TypeInfo* HitInfo_t6_229_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4877;
extern Il2CppCodeGenString* _stringLiteral4878;
extern Il2CppCodeGenString* _stringLiteral4879;
extern Il2CppCodeGenString* _stringLiteral4880;
extern Il2CppCodeGenString* _stringLiteral4881;
extern Il2CppCodeGenString* _stringLiteral4882;
extern Il2CppCodeGenString* _stringLiteral4883;
extern "C" void SendMouseEvents_SendEvents_m6_1618 (Object_t * __this /* static, unused */, int32_t ___i, HitInfo_t6_229  ___hit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		SendMouseEvents_t6_230_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1686);
		HitInfo_t6_229_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1688);
		_stringLiteral4877 = il2cpp_codegen_string_literal_from_index(4877);
		_stringLiteral4878 = il2cpp_codegen_string_literal_from_index(4878);
		_stringLiteral4879 = il2cpp_codegen_string_literal_from_index(4879);
		_stringLiteral4880 = il2cpp_codegen_string_literal_from_index(4880);
		_stringLiteral4881 = il2cpp_codegen_string_literal_from_index(4881);
		_stringLiteral4882 = il2cpp_codegen_string_literal_from_index(4882);
		_stringLiteral4883 = il2cpp_codegen_string_literal_from_index(4883);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t6_229  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m6_686(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m6_685(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t6_229  L_3 = ___hit;
		bool L_4 = HitInfo_op_Implicit_m6_1614(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_5 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_6 = ___i;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t6_229  L_7 = ___hit;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_5, L_6, sizeof(HitInfo_t6_229 )))) = L_7;
		HitInfoU5BU5D_t6_231* L_8 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_9 = ___i;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m6_1612(((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_8, L_9, sizeof(HitInfo_t6_229 ))), _stringLiteral4877, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_11 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_12 = ___i;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m6_1614(NULL /*static, unused*/, (*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_11, L_12, sizeof(HitInfo_t6_229 )))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t6_229  L_14 = ___hit;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_15 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_16 = ___i;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m6_1613(NULL /*static, unused*/, L_14, (*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_15, L_16, sizeof(HitInfo_t6_229 )))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_18 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_19 = ___i;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m6_1612(((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_18, L_19, sizeof(HitInfo_t6_229 ))), _stringLiteral4878, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_20 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_21 = ___i;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m6_1612(((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_20, L_21, sizeof(HitInfo_t6_229 ))), _stringLiteral4879, /*hidden argument*/NULL);
		HitInfoU5BU5D_t6_231* L_22 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_23 = ___i;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t6_229_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t6_229  L_24 = V_2;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_22, L_23, sizeof(HitInfo_t6_229 )))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_25 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_26 = ___i;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m6_1614(NULL /*static, unused*/, (*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_25, L_26, sizeof(HitInfo_t6_229 )))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_28 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_MouseDownHit_5;
		int32_t L_29 = ___i;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m6_1612(((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_28, L_29, sizeof(HitInfo_t6_229 ))), _stringLiteral4880, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t6_229  L_30 = ___hit;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_31 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_32 = ___i;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m6_1613(NULL /*static, unused*/, L_30, (*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_31, L_32, sizeof(HitInfo_t6_229 )))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t6_229  L_34 = ___hit;
		bool L_35 = HitInfo_op_Implicit_m6_1614(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m6_1612((&___hit), _stringLiteral4881, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_36 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_37 = ___i;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m6_1614(NULL /*static, unused*/, (*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_36, L_37, sizeof(HitInfo_t6_229 )))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_39 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_40 = ___i;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m6_1612(((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_39, L_40, sizeof(HitInfo_t6_229 ))), _stringLiteral4882, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t6_229  L_41 = ___hit;
		bool L_42 = HitInfo_op_Implicit_m6_1614(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m6_1612((&___hit), _stringLiteral4883, /*hidden argument*/NULL);
		HitInfo_SendMessage_m6_1612((&___hit), _stringLiteral4881, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t6_230_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t6_231* L_43 = ((SendMouseEvents_t6_230_StaticFields*)SendMouseEvents_t6_230_il2cpp_TypeInfo_var->static_fields)->___m_LastHit_4;
		int32_t L_44 = ___i;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t6_229  L_45 = ___hit;
		(*(HitInfo_t6_229 *)((HitInfo_t6_229 *)(HitInfo_t6_229 *)SZArrayLdElema(L_43, L_44, sizeof(HitInfo_t6_229 )))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C" void Range__ctor_m6_1619 (Range_t6_227 * __this, int32_t ___fromValue, int32_t ___valueCount, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fromValue;
		__this->___from_0 = L_0;
		int32_t L_1 = ___valueCount;
		__this->___count_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C" void PropertyAttribute__ctor_m6_1620 (PropertyAttribute_t6_236 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C" void TooltipAttribute__ctor_m6_1621 (TooltipAttribute_t6_237 * __this, String_t* ___tooltip, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip;
		__this->___tooltip_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C" void SpaceAttribute__ctor_m6_1622 (SpaceAttribute_t6_238 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		__this->___height_0 = (8.0f);
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C" void SpaceAttribute__ctor_m6_1623 (SpaceAttribute_t6_238 * __this, float ___height, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		float L_0 = ___height;
		__this->___height_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
extern "C" void HeaderAttribute__ctor_m6_1624 (HeaderAttribute_t6_239 * __this, String_t* ___header, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___header;
		__this->___header_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C" void RangeAttribute__ctor_m6_1625 (RangeAttribute_t6_240 * __this, float ___min, float ___max, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		float L_0 = ___min;
		__this->___min_0 = L_0;
		float L_1 = ___max;
		__this->___max_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.MultilineAttribute::.ctor(System.Int32)
extern "C" void MultilineAttribute__ctor_m6_1626 (MultilineAttribute_t6_241 * __this, int32_t ___lines, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___lines;
		__this->___lines_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C" void TextAreaAttribute__ctor_m6_1627 (TextAreaAttribute_t6_242 * __this, int32_t ___minLines, int32_t ___maxLines, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m6_1620(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines;
		__this->___minLines_0 = L_0;
		int32_t L_1 = ___maxLines;
		__this->___maxLines_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C" void SelectionBaseAttribute__ctor_m6_1628 (SelectionBaseAttribute_t6_243 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C" void SliderState__ctor_m6_1629 (SliderState_t6_244 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C" void SliderHandler__ctor_m6_1630 (SliderHandler_t6_245 * __this, Rect_t6_51  ___position, float ___currentValue, float ___size, float ___start, float ___end, GUIStyle_t6_176 * ___slider, GUIStyle_t6_176 * ___thumb, bool ___horiz, int32_t ___id, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = ___position;
		__this->___position_0 = L_0;
		float L_1 = ___currentValue;
		__this->___currentValue_1 = L_1;
		float L_2 = ___size;
		__this->___size_2 = L_2;
		float L_3 = ___start;
		__this->___start_3 = L_3;
		float L_4 = ___end;
		__this->___end_4 = L_4;
		GUIStyle_t6_176 * L_5 = ___slider;
		__this->___slider_5 = L_5;
		GUIStyle_t6_176 * L_6 = ___thumb;
		__this->___thumb_6 = L_6;
		bool L_7 = ___horiz;
		__this->___horiz_7 = L_7;
		int32_t L_8 = ___id;
		__this->___id_8 = L_8;
		return;
	}
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C" float SliderHandler_Handle_m6_1631 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		GUIStyle_t6_176 * L_0 = (__this->___slider_5);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (__this->___thumb_6);
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		float L_2 = (__this->___currentValue_1);
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m6_1636(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004f;
		}
		if (L_4 == 1)
		{
			goto IL_005d;
		}
		if (L_4 == 2)
		{
			goto IL_006b;
		}
		if (L_4 == 3)
		{
			goto IL_0056;
		}
		if (L_4 == 4)
		{
			goto IL_006b;
		}
		if (L_4 == 5)
		{
			goto IL_006b;
		}
		if (L_4 == 6)
		{
			goto IL_006b;
		}
		if (L_4 == 7)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_006b;
	}

IL_004f:
	{
		float L_5 = SliderHandler_OnMouseDown_m6_1632(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0056:
	{
		float L_6 = SliderHandler_OnMouseDrag_m6_1633(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_005d:
	{
		float L_7 = SliderHandler_OnMouseUp_m6_1634(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0064:
	{
		float L_8 = SliderHandler_OnRepaint_m6_1635(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_006b:
	{
		float L_9 = (__this->___currentValue_1);
		return L_9;
	}
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* SystemClock_t6_250_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnMouseDown_m6_1632 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		SystemClock_t6_250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1691);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Rect_t6_51  V_1 = {0};
	Rect_t6_51  V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	{
		Rect_t6_51  L_0 = (__this->___position_0);
		V_1 = L_0;
		Event_t6_162 * L_1 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_47  L_2 = Event_get_mousePosition_m6_1131(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m6_320((&V_1), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m6_1638(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		float L_5 = (__this->___currentValue_1);
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m6_1200(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = (__this->___id_8);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t6_162 * L_7 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m6_1188(L_7, /*hidden argument*/NULL);
		Rect_t6_51  L_8 = SliderHandler_ThumbSelectionRect_m6_1645(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t6_162 * L_9 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t6_47  L_10 = Event_get_mousePosition_m6_1131(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m6_320((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m6_1651(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m6_1646(__this, L_12, /*hidden argument*/NULL);
		float L_13 = (__this->___currentValue_1);
		return L_13;
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1228(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m6_1639(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c7;
		}
	}
	{
		SliderState_t6_244 * L_15 = SliderHandler_SliderState_m6_1647(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->___isDragging_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t6_250_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_16 = SystemClock_get_now_m6_1673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t1_150  L_17 = DateTime_AddMilliseconds_m1_13836((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1198(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m6_1637(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m6_1200(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m6_1640(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_00c7:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m6_1643(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = V_0;
		SliderHandler_StartDraggingWithValue_m6_1646(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_0;
		float L_23 = SliderHandler_Clamp_m6_1644(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnMouseDrag_m6_1633 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	SliderState_t6_244 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		float L_2 = (__this->___currentValue_1);
		return L_2;
	}

IL_0017:
	{
		SliderState_t6_244 * L_3 = SliderHandler_SliderState_m6_1647(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		SliderState_t6_244 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (L_4->___isDragging_2);
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6 = (__this->___currentValue_1);
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1228(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		Event_t6_162 * L_7 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m6_1188(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m6_1652(__this, /*hidden argument*/NULL);
		SliderState_t6_244 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = (L_9->___dragStartPos_0);
		V_1 = ((float)((float)L_8-(float)L_10));
		SliderState_t6_244 * L_11 = V_0;
		NullCheck(L_11);
		float L_12 = (L_11->___dragStartValue_1);
		float L_13 = V_1;
		float L_14 = SliderHandler_ValuesPerPixel_m6_1653(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_2;
		float L_16 = SliderHandler_Clamp_m6_1644(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnMouseUp_m6_1634 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Event_t6_162 * L_2 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m6_1188(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		float L_3 = (__this->___currentValue_1);
		return L_3;
	}
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* SystemClock_t6_250_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_OnRepaint_m6_1635 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		SystemClock_t6_250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1691);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t6_51  V_0 = {0};
	Rect_t6_51  V_1 = {0};
	DateTime_t1_150  V_2 = {0};
	{
		GUIStyle_t6_176 * L_0 = (__this->___slider_5);
		Rect_t6_51  L_1 = (__this->___position_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_2 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		int32_t L_3 = (__this->___id_8);
		NullCheck(L_0);
		GUIStyle_Draw_m6_1439(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m6_1638(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GUIStyle_t6_176 * L_5 = (__this->___thumb_6);
		Rect_t6_51  L_6 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_7 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		int32_t L_8 = (__this->___id_8);
		NullCheck(L_5);
		GUIStyle_Draw_m6_1439(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___id_8);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007c;
		}
	}
	{
		Rect_t6_51  L_11 = (__this->___position_0);
		V_0 = L_11;
		Event_t6_162 * L_12 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t6_47  L_13 = Event_get_mousePosition_m6_1131(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m6_320((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m6_1638(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		float L_16 = (__this->___currentValue_1);
		return L_16;
	}

IL_0083:
	{
		Rect_t6_51  L_17 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Event_t6_162 * L_18 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t6_47  L_19 = Event_get_mousePosition_m6_1131(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m6_320((&V_1), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m6_1199(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		float L_22 = (__this->___currentValue_1);
		return L_22;
	}

IL_00b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m6_1236(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t6_250_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_23 = SystemClock_get_now_m6_1673(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t1_150  L_24 = GUI_get_nextScrollStepTime_m6_1197(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m1_13907(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		float L_26 = (__this->___currentValue_1);
		return L_26;
	}

IL_00d8:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m6_1637(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m6_1199(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00ef;
		}
	}
	{
		float L_29 = (__this->___currentValue_1);
		return L_29;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t6_250_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_30 = SystemClock_get_now_m6_1673(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_30;
		DateTime_t1_150  L_31 = DateTime_AddMilliseconds_m1_13836((&V_2), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1198(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m6_1639(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_012e;
		}
	}
	{
		SliderState_t6_244 * L_33 = SliderHandler_SliderState_m6_1647(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->___isDragging_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1228(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m6_1640(__this, /*hidden argument*/NULL);
		return L_34;
	}

IL_012e:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m6_1651(__this, /*hidden argument*/NULL);
		return L_35;
	}
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C" int32_t SliderHandler_CurrentEventType_m6_1636 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	{
		Event_t6_162 * L_0 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m6_1166(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C" int32_t SliderHandler_CurrentScrollTroughSide_m6_1637 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t6_47  V_2 = {0};
	Vector2_t6_47  V_3 = {0};
	Rect_t6_51  V_4 = {0};
	Rect_t6_51  V_5 = {0};
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Event_t6_162 * L_1 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_47  L_2 = Event_get_mousePosition_m6_1131(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = ((&V_2)->___x_1);
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0023:
	{
		Event_t6_162 * L_4 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t6_47  L_5 = Event_get_mousePosition_m6_1131(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = ((&V_3)->___y_2);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		V_0 = G_B3_0;
		bool L_7 = (__this->___horiz_7);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Rect_t6_51  L_8 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m6_298((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0065;
	}

IL_0056:
	{
		Rect_t6_51  L_10 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m6_300((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0065:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0073;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = (-1);
	}

IL_0074:
	{
		return G_B9_0;
	}
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C" bool SliderHandler_IsEmptySlider_m6_1638 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___start_3);
		float L_1 = (__this->___end_4);
		return ((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool SliderHandler_SupportsPageMovements_m6_1639 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = (__this->___size_2);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m6_1235(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C" float SliderHandler_PageMovementValue_m6_1640 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		float L_0 = (__this->___currentValue_1);
		V_0 = L_0;
		float L_1 = (__this->___start_3);
		float L_2 = (__this->___end_4);
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m6_1652(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m6_1641(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = (__this->___size_2);
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005a;
	}

IL_0048:
	{
		float L_8 = V_0;
		float L_9 = (__this->___size_2);
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005a:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m6_1644(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C" float SliderHandler_PageUpMovementBound_m6_1641 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	Rect_t6_51  V_1 = {0};
	Rect_t6_51  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Rect_t6_51  L_1 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m6_315((&V_0), /*hidden argument*/NULL);
		Rect_t6_51  L_3 = (__this->___position_0);
		V_1 = L_3;
		float L_4 = Rect_get_x_m6_298((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_2-(float)L_4));
	}

IL_0029:
	{
		Rect_t6_51  L_5 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_yMax_m6_317((&V_2), /*hidden argument*/NULL);
		Rect_t6_51  L_7 = (__this->___position_0);
		V_3 = L_7;
		float L_8 = Rect_get_y_m6_300((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_6-(float)L_8));
	}
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C" Event_t6_162 * SliderHandler_CurrentEvent_m6_1642 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C" float SliderHandler_ValueForCurrentMousePosition_m6_1643 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	Rect_t6_51  V_1 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m6_1652(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_2 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m6_1653(__this, /*hidden argument*/NULL);
		float L_5 = (__this->___start_3);
		float L_6 = (__this->___size_2);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
	}

IL_0042:
	{
		float L_7 = SliderHandler_MousePosition_m6_1652(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_8 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m6_1653(__this, /*hidden argument*/NULL);
		float L_11 = (__this->___start_3);
		float L_12 = (__this->___size_2);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
	}
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_Clamp_m6_1644 (SliderHandler_t6_245 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		float L_1 = SliderHandler_MinValue_m6_1656(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m6_1655(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C" Rect_t6_51  SliderHandler_ThumbSelectionRect_m6_1645 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	int32_t V_1 = 0;
	{
		Rect_t6_51  L_0 = SliderHandler_ThumbRect_m6_1648(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_003f;
		}
	}
	{
		Rect_t6_51 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m6_298(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m6_299(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m6_307((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_003f:
	{
		float L_8 = Rect_get_height_m6_308((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t6_51 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m6_300(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m6_308((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m6_301(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m6_309((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rect_t6_51  L_15 = V_0;
		return L_15;
	}
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C" void SliderHandler_StartDraggingWithValue_m6_1646 (SliderHandler_t6_245 * __this, float ___dragStartValue, const MethodInfo* method)
{
	SliderState_t6_244 * V_0 = {0};
	{
		SliderState_t6_244 * L_0 = SliderHandler_SliderState_m6_1647(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t6_244 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m6_1652(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___dragStartPos_0 = L_2;
		SliderState_t6_244 * L_3 = V_0;
		float L_4 = ___dragStartValue;
		NullCheck(L_3);
		L_3->___dragStartValue_1 = L_4;
		SliderState_t6_244 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___isDragging_2 = 1;
		return;
	}
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t6_244_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* SliderState_t6_244_il2cpp_TypeInfo_var;
extern "C" SliderState_t6_244 * SliderHandler_SliderState_m6_1647 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SliderState_t6_244_0_0_0_var = il2cpp_codegen_type_from_index(1692);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		SliderState_t6_244_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1692);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(SliderState_t6_244_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = (__this->___id_8);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		Object_t * L_2 = GUIUtility_GetStateObject_m6_1498(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((SliderState_t6_244 *)CastclassClass(L_2, SliderState_t6_244_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C" Rect_t6_51  SliderHandler_ThumbRect_m6_1648 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	Rect_t6_51  G_B3_0 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Rect_t6_51  L_1 = SliderHandler_HorizontalThumbRect_m6_1650(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Rect_t6_51  L_2 = SliderHandler_VerticalThumbRect_m6_1649(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C" Rect_t6_51  SliderHandler_VerticalThumbRect_m6_1649 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t6_51  V_1 = {0};
	Rect_t6_51  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	Rect_t6_51  V_4 = {0};
	Rect_t6_51  V_5 = {0};
	Rect_t6_51  V_6 = {0};
	{
		float L_0 = SliderHandler_ValuesPerPixel_m6_1653(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (__this->___start_3);
		float L_2 = (__this->___end_4);
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		Rect_t6_51  L_3 = (__this->___position_0);
		V_1 = L_3;
		float L_4 = Rect_get_x_m6_298((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_5 = (__this->___slider_5);
		NullCheck(L_5);
		RectOffset_t6_178 * L_6 = GUIStyle_get_padding_m6_1434(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m6_1416(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m6_1651(__this, /*hidden argument*/NULL);
		float L_9 = (__this->___start_3);
		float L_10 = V_0;
		Rect_t6_51  L_11 = (__this->___position_0);
		V_2 = L_11;
		float L_12 = Rect_get_y_m6_300((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_13 = (__this->___slider_5);
		NullCheck(L_13);
		RectOffset_t6_178 * L_14 = GUIStyle_get_padding_m6_1434(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m6_1420(L_14, /*hidden argument*/NULL);
		Rect_t6_51  L_16 = (__this->___position_0);
		V_3 = L_16;
		float L_17 = Rect_get_width_m6_306((&V_3), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_18 = (__this->___slider_5);
		NullCheck(L_18);
		RectOffset_t6_178 * L_19 = GUIStyle_get_padding_m6_1434(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m6_1424(L_19, /*hidden argument*/NULL);
		float L_21 = (__this->___size_2);
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m6_1654(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_24 = {0};
		Rect__ctor_m6_295(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		Rect_t6_51  L_25 = (__this->___position_0);
		V_4 = L_25;
		float L_26 = Rect_get_x_m6_298((&V_4), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_27 = (__this->___slider_5);
		NullCheck(L_27);
		RectOffset_t6_178 * L_28 = GUIStyle_get_padding_m6_1434(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m6_1416(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m6_1651(__this, /*hidden argument*/NULL);
		float L_31 = (__this->___size_2);
		float L_32 = (__this->___start_3);
		float L_33 = V_0;
		Rect_t6_51  L_34 = (__this->___position_0);
		V_5 = L_34;
		float L_35 = Rect_get_y_m6_300((&V_5), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_36 = (__this->___slider_5);
		NullCheck(L_36);
		RectOffset_t6_178 * L_37 = GUIStyle_get_padding_m6_1434(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m6_1420(L_37, /*hidden argument*/NULL);
		Rect_t6_51  L_39 = (__this->___position_0);
		V_6 = L_39;
		float L_40 = Rect_get_width_m6_306((&V_6), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_41 = (__this->___slider_5);
		NullCheck(L_41);
		RectOffset_t6_178 * L_42 = GUIStyle_get_padding_m6_1434(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m6_1424(L_42, /*hidden argument*/NULL);
		float L_44 = (__this->___size_2);
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m6_1654(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_47 = {0};
		Rect__ctor_m6_295(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		return L_47;
	}
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C" Rect_t6_51  SliderHandler_HorizontalThumbRect_m6_1650 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t6_51  V_1 = {0};
	Rect_t6_51  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	Rect_t6_51  V_4 = {0};
	Rect_t6_51  V_5 = {0};
	Rect_t6_51  V_6 = {0};
	{
		float L_0 = SliderHandler_ValuesPerPixel_m6_1653(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (__this->___start_3);
		float L_2 = (__this->___end_4);
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m6_1651(__this, /*hidden argument*/NULL);
		float L_4 = (__this->___start_3);
		float L_5 = V_0;
		Rect_t6_51  L_6 = (__this->___position_0);
		V_1 = L_6;
		float L_7 = Rect_get_x_m6_298((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_8 = (__this->___slider_5);
		NullCheck(L_8);
		RectOffset_t6_178 * L_9 = GUIStyle_get_padding_m6_1434(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m6_1416(L_9, /*hidden argument*/NULL);
		Rect_t6_51  L_11 = (__this->___position_0);
		V_2 = L_11;
		float L_12 = Rect_get_y_m6_300((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_13 = (__this->___slider_5);
		NullCheck(L_13);
		RectOffset_t6_178 * L_14 = GUIStyle_get_padding_m6_1434(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m6_1420(L_14, /*hidden argument*/NULL);
		float L_16 = (__this->___size_2);
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m6_1654(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_19 = (__this->___position_0);
		V_3 = L_19;
		float L_20 = Rect_get_height_m6_308((&V_3), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_21 = (__this->___slider_5);
		NullCheck(L_21);
		RectOffset_t6_178 * L_22 = GUIStyle_get_padding_m6_1434(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m6_1425(L_22, /*hidden argument*/NULL);
		Rect_t6_51  L_24 = {0};
		Rect__ctor_m6_295(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m6_1651(__this, /*hidden argument*/NULL);
		float L_26 = (__this->___size_2);
		float L_27 = (__this->___start_3);
		float L_28 = V_0;
		Rect_t6_51  L_29 = (__this->___position_0);
		V_4 = L_29;
		float L_30 = Rect_get_x_m6_298((&V_4), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_31 = (__this->___slider_5);
		NullCheck(L_31);
		RectOffset_t6_178 * L_32 = GUIStyle_get_padding_m6_1434(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m6_1416(L_32, /*hidden argument*/NULL);
		Rect_t6_51  L_34 = (__this->___position_0);
		V_5 = L_34;
		float L_35 = Rect_get_y_m6_300((&V_5), /*hidden argument*/NULL);
		float L_36 = (__this->___size_2);
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m6_1654(__this, /*hidden argument*/NULL);
		Rect_t6_51  L_39 = (__this->___position_0);
		V_6 = L_39;
		float L_40 = Rect_get_height_m6_308((&V_6), /*hidden argument*/NULL);
		Rect_t6_51  L_41 = {0};
		Rect__ctor_m6_295(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C" float SliderHandler_ClampedCurrentValue_m6_1651 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___currentValue_1);
		float L_1 = SliderHandler_Clamp_m6_1644(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C" float SliderHandler_MousePosition_m6_1652 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	Rect_t6_51  V_1 = {0};
	Vector2_t6_47  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Event_t6_162 * L_1 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_47  L_2 = Event_get_mousePosition_m6_1131(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___x_1);
		Rect_t6_51  L_4 = (__this->___position_0);
		V_1 = L_4;
		float L_5 = Rect_get_x_m6_298((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_3-(float)L_5));
	}

IL_002e:
	{
		Event_t6_162 * L_6 = SliderHandler_CurrentEvent_m6_1642(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t6_47  L_7 = Event_get_mousePosition_m6_1131(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = ((&V_2)->___y_2);
		Rect_t6_51  L_9 = (__this->___position_0);
		V_3 = L_9;
		float L_10 = Rect_get_y_m6_300((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_8-(float)L_10));
	}
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C" float SliderHandler_ValuesPerPixel_m6_1653 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	Rect_t6_51  V_1 = {0};
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Rect_t6_51  L_1 = (__this->___position_0);
		V_0 = L_1;
		float L_2 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_3 = (__this->___slider_5);
		NullCheck(L_3);
		RectOffset_t6_178 * L_4 = GUIStyle_get_padding_m6_1434(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m6_1424(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m6_1654(__this, /*hidden argument*/NULL);
		float L_7 = (__this->___end_4);
		float L_8 = (__this->___start_3);
		return ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
	}

IL_0041:
	{
		Rect_t6_51  L_9 = (__this->___position_0);
		V_1 = L_9;
		float L_10 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_11 = (__this->___slider_5);
		NullCheck(L_11);
		RectOffset_t6_178 * L_12 = GUIStyle_get_padding_m6_1434(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m6_1425(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m6_1654(__this, /*hidden argument*/NULL);
		float L_15 = (__this->___end_4);
		float L_16 = (__this->___start_3);
		return ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
	}
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C" float SliderHandler_ThumbSize_m6_1654 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = (__this->___horiz_7);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (__this->___thumb_6);
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m6_1468(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0030;
		}
	}
	{
		GUIStyle_t6_176 * L_3 = (__this->___thumb_6);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m6_1468(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0041;
	}

IL_0030:
	{
		GUIStyle_t6_176 * L_5 = (__this->___thumb_6);
		NullCheck(L_5);
		RectOffset_t6_178 * L_6 = GUIStyle_get_padding_m6_1434(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m6_1424(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0041:
	{
		return G_B4_0;
	}

IL_0042:
	{
		GUIStyle_t6_176 * L_8 = (__this->___thumb_6);
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m6_1469(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		GUIStyle_t6_176 * L_10 = (__this->___thumb_6);
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m6_1469(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0078;
	}

IL_0067:
	{
		GUIStyle_t6_176 * L_12 = (__this->___thumb_6);
		NullCheck(L_12);
		RectOffset_t6_178 * L_13 = GUIStyle_get_padding_m6_1434(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m6_1425(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_0078:
	{
		return G_B8_0;
	}
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_MaxValue_m6_1655 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___start_3);
		float L_1 = (__this->___end_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m6_429(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (__this->___size_2);
		return ((float)((float)L_2-(float)L_3));
	}
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float SliderHandler_MinValue_m6_1656 (SliderHandler_t6_245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___start_3);
		float L_1 = (__this->___end_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m6_427(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern "C" void StackTraceUtility__ctor_m6_1657 (StackTraceUtility_t6_246 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern "C" void StackTraceUtility__cctor_m6_1658 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((StackTraceUtility_t6_246_StaticFields*)StackTraceUtility_t6_246_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern "C" void StackTraceUtility_SetProjectFolder_m6_1659 (Object_t * __this /* static, unused */, String_t* ___folder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___folder;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		((StackTraceUtility_t6_246_StaticFields*)StackTraceUtility_t6_246_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0 = L_0;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern TypeInfo* StackTrace_t1_336_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern "C" String_t* StackTraceUtility_ExtractStackTrace_m6_1660 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StackTrace_t1_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t1_336 * V_0 = {0};
	String_t* V_1 = {0};
	{
		StackTrace_t1_336 * L_0 = (StackTrace_t1_336 *)il2cpp_codegen_object_new (StackTrace_t1_336_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1_3641(L_0, 1, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t1_336 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m6_1665(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_ToString_m1_546(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4884;
extern Il2CppCodeGenString* _stringLiteral4885;
extern Il2CppCodeGenString* _stringLiteral4886;
extern Il2CppCodeGenString* _stringLiteral4887;
extern Il2CppCodeGenString* _stringLiteral4888;
extern Il2CppCodeGenString* _stringLiteral4889;
extern "C" bool StackTraceUtility_IsSystemStacktraceType_m6_1661 (Object_t * __this /* static, unused */, Object_t * ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4884 = il2cpp_codegen_string_literal_from_index(4884);
		_stringLiteral4885 = il2cpp_codegen_string_literal_from_index(4885);
		_stringLiteral4886 = il2cpp_codegen_string_literal_from_index(4886);
		_stringLiteral4887 = il2cpp_codegen_string_literal_from_index(4887);
		_stringLiteral4888 = il2cpp_codegen_string_literal_from_index(4888);
		_stringLiteral4889 = il2cpp_codegen_string_literal_from_index(4889);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___name;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1_531(L_1, _stringLiteral4884, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1_531(L_3, _stringLiteral4885, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1_531(L_5, _stringLiteral4886, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1_531(L_7, _stringLiteral4887, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1_531(L_9, _stringLiteral4888, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1_531(L_11, _stringLiteral4889, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4828;
extern "C" String_t* StackTraceUtility_ExtractStringFromException_m6_1662 (Object_t * __this /* static, unused */, Object_t * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		Object_t * L_2 = ___exception;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		StackTraceUtility_ExtractStringFromExceptionInternal_m6_1663(NULL /*static, unused*/, L_2, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		String_t* L_4 = V_1;
		String_t* L_5 = String_Concat_m1_560(NULL /*static, unused*/, L_3, _stringLiteral4828, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTrace_t1_336_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4890;
extern Il2CppCodeGenString* _stringLiteral4891;
extern Il2CppCodeGenString* _stringLiteral4828;
extern Il2CppCodeGenString* _stringLiteral259;
extern Il2CppCodeGenString* _stringLiteral4892;
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m6_1663 (Object_t * __this /* static, unused */, Object_t * ___exceptiono, String_t** ___message, String_t** ___stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StackTrace_t1_336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		_stringLiteral4890 = il2cpp_codegen_string_literal_from_index(4890);
		_stringLiteral4891 = il2cpp_codegen_string_literal_from_index(4891);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		_stringLiteral259 = il2cpp_codegen_string_literal_from_index(259);
		_stringLiteral4892 = il2cpp_codegen_string_literal_from_index(4892);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * V_0 = {0};
	StringBuilder_t1_247 * V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	StackTrace_t1_336 * V_5 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___exceptiono;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_1, _stringLiteral4890, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___exceptiono;
		V_0 = ((Exception_t1_33 *)IsInstClass(L_2, Exception_t1_33_il2cpp_TypeInfo_var));
		Exception_t1_33 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t1_1425 * L_4 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_4, _stringLiteral4891, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0029:
	{
		Exception_t1_33 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		Exception_t1_33 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_571(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t1_247 * L_10 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12415(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		*((Object_t **)(L_11)) = (Object_t *)L_12;
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_2 = L_13;
		goto IL_00ff;
	}

IL_0063:
	{
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1_571(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		Exception_t1_33 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Exception::get_StackTrace() */, L_16);
		V_2 = L_17;
		goto IL_008c;
	}

IL_007a:
	{
		Exception_t1_33 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Exception::get_StackTrace() */, L_18);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m1_560(NULL /*static, unused*/, L_19, _stringLiteral4828, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_008c:
	{
		Exception_t1_33 * L_22 = V_0;
		NullCheck(L_22);
		Type_t * L_23 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Exception::GetType() */, L_22);
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		V_3 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_4 = L_25;
		Exception_t1_33 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_26);
		if (!L_27)
		{
			goto IL_00b2;
		}
	}
	{
		Exception_t1_33 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Exception::get_Message() */, L_28);
		V_4 = L_29;
	}

IL_00b2:
	{
		String_t* L_30 = V_4;
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m1_457(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m1_571(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_559(NULL /*static, unused*/, L_33, _stringLiteral259, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		String_t* L_36 = V_4;
		String_t* L_37 = String_Concat_m1_559(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
	}

IL_00d8:
	{
		String_t** L_38 = ___message;
		String_t* L_39 = V_3;
		*((Object_t **)(L_38)) = (Object_t *)L_39;
		Exception_t1_33 * L_40 = V_0;
		NullCheck(L_40);
		Exception_t1_33 * L_41 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, L_40);
		if (!L_41)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m1_561(NULL /*static, unused*/, _stringLiteral4892, L_42, _stringLiteral4828, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
	}

IL_00f8:
	{
		Exception_t1_33 * L_45 = V_0;
		NullCheck(L_45);
		Exception_t1_33 * L_46 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, L_45);
		V_0 = L_46;
	}

IL_00ff:
	{
		Exception_t1_33 * L_47 = V_0;
		if (L_47)
		{
			goto IL_0063;
		}
	}
	{
		StringBuilder_t1_247 * L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m1_559(NULL /*static, unused*/, L_49, _stringLiteral4828, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_Append_m1_12438(L_48, L_50, /*hidden argument*/NULL);
		StackTrace_t1_336 * L_51 = (StackTrace_t1_336 *)il2cpp_codegen_object_new (StackTrace_t1_336_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1_3641(L_51, 1, 1, /*hidden argument*/NULL);
		V_5 = L_51;
		StringBuilder_t1_247 * L_52 = V_1;
		StackTrace_t1_336 * L_53 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		String_t* L_54 = StackTraceUtility_ExtractFormattedStackTrace_m6_1665(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m1_12438(L_52, L_54, /*hidden argument*/NULL);
		String_t** L_55 = ___stackTrace;
		StringBuilder_t1_247 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m1_12428(L_56, /*hidden argument*/NULL);
		*((Object_t **)(L_55)) = (Object_t *)L_57;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4893;
extern Il2CppCodeGenString* _stringLiteral4894;
extern Il2CppCodeGenString* _stringLiteral4895;
extern Il2CppCodeGenString* _stringLiteral4896;
extern Il2CppCodeGenString* _stringLiteral4897;
extern Il2CppCodeGenString* _stringLiteral4898;
extern Il2CppCodeGenString* _stringLiteral264;
extern Il2CppCodeGenString* _stringLiteral266;
extern Il2CppCodeGenString* _stringLiteral4899;
extern Il2CppCodeGenString* _stringLiteral4900;
extern Il2CppCodeGenString* _stringLiteral4901;
extern Il2CppCodeGenString* _stringLiteral4902;
extern Il2CppCodeGenString* _stringLiteral4903;
extern Il2CppCodeGenString* _stringLiteral106;
extern Il2CppCodeGenString* _stringLiteral4828;
extern "C" String_t* StackTraceUtility_PostprocessStacktrace_m6_1664 (Object_t * __this /* static, unused */, String_t* ___oldString, bool ___stripEngineInternalInformation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		_stringLiteral4893 = il2cpp_codegen_string_literal_from_index(4893);
		_stringLiteral4894 = il2cpp_codegen_string_literal_from_index(4894);
		_stringLiteral4895 = il2cpp_codegen_string_literal_from_index(4895);
		_stringLiteral4896 = il2cpp_codegen_string_literal_from_index(4896);
		_stringLiteral4897 = il2cpp_codegen_string_literal_from_index(4897);
		_stringLiteral4898 = il2cpp_codegen_string_literal_from_index(4898);
		_stringLiteral264 = il2cpp_codegen_string_literal_from_index(264);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		_stringLiteral4899 = il2cpp_codegen_string_literal_from_index(4899);
		_stringLiteral4900 = il2cpp_codegen_string_literal_from_index(4900);
		_stringLiteral4901 = il2cpp_codegen_string_literal_from_index(4901);
		_stringLiteral4902 = il2cpp_codegen_string_literal_from_index(4902);
		_stringLiteral4903 = il2cpp_codegen_string_literal_from_index(4903);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1_238* V_0 = {0};
	StringBuilder_t1_247 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___oldString;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		String_t* L_2 = ___oldString;
		CharU5BU5D_t1_16* L_3 = ((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)10);
		NullCheck(L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___oldString;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_7 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12415(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		StringU5BU5D_t1_238* L_8 = V_0;
		int32_t L_9 = V_2;
		StringU5BU5D_t1_238* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12, sizeof(String_t*))));
		String_t* L_13 = String_Trim_m1_457((*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12, sizeof(String_t*))), /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_13);
		*((String_t**)(String_t**)SZArrayLdElema(L_8, L_9, sizeof(String_t*))) = (String_t*)L_13;
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_15 = V_2;
		StringU5BU5D_t1_238* L_16 = V_0;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		StringU5BU5D_t1_238* L_17 = V_0;
		int32_t L_18 = V_3;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		V_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_17, L_19, sizeof(String_t*)));
		String_t* L_20 = V_4;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m1_571(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_22 = V_4;
		NullCheck(L_22);
		uint16_t L_23 = String_get_Chars_m1_442(L_22, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		bool L_25 = String_StartsWith_m1_531(L_24, _stringLiteral4893, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		bool L_26 = ___stripEngineInternalInformation;
		if (!L_26)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_27 = V_4;
		NullCheck(L_27);
		bool L_28 = String_StartsWith_m1_531(L_27, _stringLiteral4894, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		bool L_29 = ___stripEngineInternalInformation;
		if (!L_29)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_30 = V_3;
		StringU5BU5D_t1_238* L_31 = V_0;
		NullCheck(L_31);
		if ((((int32_t)L_30) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_31)->max_length))))-(int32_t)1)))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_32 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		bool L_33 = StackTraceUtility_IsSystemStacktraceType_m6_1661(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t1_238* L_34 = V_0;
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)((int32_t)L_35+(int32_t)1)));
		int32_t L_36 = ((int32_t)((int32_t)L_35+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		bool L_37 = StackTraceUtility_IsSystemStacktraceType_m6_1661(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_34, L_36, sizeof(String_t*))), /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		String_t* L_38 = V_4;
		NullCheck(L_38);
		int32_t L_39 = String_IndexOf_m1_505(L_38, _stringLiteral4895, /*hidden argument*/NULL);
		V_5 = L_39;
		int32_t L_40 = V_5;
		if ((((int32_t)L_40) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_41 = V_4;
		int32_t L_42 = V_5;
		NullCheck(L_41);
		String_t* L_43 = String_Substring_m1_455(L_41, 0, L_42, /*hidden argument*/NULL);
		V_4 = L_43;
	}

IL_00fa:
	{
		String_t* L_44 = V_4;
		NullCheck(L_44);
		int32_t L_45 = String_IndexOf_m1_505(L_44, _stringLiteral4896, /*hidden argument*/NULL);
		if ((((int32_t)L_45) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		String_t* L_46 = V_4;
		NullCheck(L_46);
		int32_t L_47 = String_IndexOf_m1_505(L_46, _stringLiteral4897, /*hidden argument*/NULL);
		if ((((int32_t)L_47) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		String_t* L_48 = V_4;
		NullCheck(L_48);
		int32_t L_49 = String_IndexOf_m1_505(L_48, _stringLiteral4898, /*hidden argument*/NULL);
		if ((((int32_t)L_49) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		bool L_50 = ___stripEngineInternalInformation;
		if (!L_50)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_51 = V_4;
		NullCheck(L_51);
		bool L_52 = String_StartsWith_m1_531(L_51, _stringLiteral264, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_53 = V_4;
		NullCheck(L_53);
		bool L_54 = String_EndsWith_m1_482(L_53, _stringLiteral266, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		String_t* L_55 = V_4;
		NullCheck(L_55);
		bool L_56 = String_StartsWith_m1_531(L_55, _stringLiteral4899, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0188;
		}
	}
	{
		String_t* L_57 = V_4;
		NullCheck(L_57);
		String_t* L_58 = String_Remove_m1_539(L_57, 0, 3, /*hidden argument*/NULL);
		V_4 = L_58;
	}

IL_0188:
	{
		String_t* L_59 = V_4;
		NullCheck(L_59);
		int32_t L_60 = String_IndexOf_m1_505(L_59, _stringLiteral4900, /*hidden argument*/NULL);
		V_6 = L_60;
		V_7 = (-1);
		int32_t L_61 = V_6;
		if ((((int32_t)L_61) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		String_t* L_62 = V_4;
		int32_t L_63 = V_6;
		NullCheck(L_62);
		int32_t L_64 = String_IndexOf_m1_506(L_62, _stringLiteral266, L_63, /*hidden argument*/NULL);
		V_7 = L_64;
	}

IL_01b1:
	{
		int32_t L_65 = V_6;
		if ((((int32_t)L_65) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_66 = V_7;
		int32_t L_67 = V_6;
		if ((((int32_t)L_66) <= ((int32_t)L_67)))
		{
			goto IL_01d4;
		}
	}
	{
		String_t* L_68 = V_4;
		int32_t L_69 = V_6;
		int32_t L_70 = V_7;
		int32_t L_71 = V_6;
		NullCheck(L_68);
		String_t* L_72 = String_Remove_m1_539(L_68, L_69, ((int32_t)((int32_t)((int32_t)((int32_t)L_70-(int32_t)L_71))+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_72;
	}

IL_01d4:
	{
		String_t* L_73 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_73);
		String_t* L_75 = String_Replace_m1_536(L_73, _stringLiteral4901, L_74, /*hidden argument*/NULL);
		V_4 = L_75;
		String_t* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		String_t* L_77 = ((StackTraceUtility_t6_246_StaticFields*)StackTraceUtility_t6_246_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		String_t* L_78 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_76);
		String_t* L_79 = String_Replace_m1_536(L_76, L_77, L_78, /*hidden argument*/NULL);
		V_4 = L_79;
		String_t* L_80 = V_4;
		NullCheck(L_80);
		String_t* L_81 = String_Replace_m1_535(L_80, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_4 = L_81;
		String_t* L_82 = V_4;
		NullCheck(L_82);
		int32_t L_83 = String_LastIndexOf_m1_517(L_82, _stringLiteral4902, /*hidden argument*/NULL);
		V_8 = L_83;
		int32_t L_84 = V_8;
		if ((((int32_t)L_84) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		String_t* L_85 = V_4;
		int32_t L_86 = V_8;
		NullCheck(L_85);
		String_t* L_87 = String_Remove_m1_539(L_85, L_86, 5, /*hidden argument*/NULL);
		V_4 = L_87;
		String_t* L_88 = V_4;
		int32_t L_89 = V_8;
		NullCheck(L_88);
		String_t* L_90 = String_Insert_m1_565(L_88, L_89, _stringLiteral4903, /*hidden argument*/NULL);
		V_4 = L_90;
		String_t* L_91 = V_4;
		String_t* L_92 = V_4;
		NullCheck(L_92);
		int32_t L_93 = String_get_Length_m1_571(L_92, /*hidden argument*/NULL);
		NullCheck(L_91);
		String_t* L_94 = String_Insert_m1_565(L_91, L_93, _stringLiteral106, /*hidden argument*/NULL);
		V_4 = L_94;
	}

IL_024e:
	{
		StringBuilder_t1_247 * L_95 = V_1;
		String_t* L_96 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = String_Concat_m1_559(NULL /*static, unused*/, L_96, _stringLiteral4828, /*hidden argument*/NULL);
		NullCheck(L_95);
		StringBuilder_Append_m1_12438(L_95, L_97, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_98 = V_3;
		V_3 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0265:
	{
		int32_t L_99 = V_3;
		StringU5BU5D_t1_238* L_100 = V_0;
		NullCheck(L_100);
		if ((((int32_t)L_99) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_100)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		StringBuilder_t1_247 * L_101 = V_1;
		NullCheck(L_101);
		String_t* L_102 = StringBuilder_ToString_m1_12428(L_101, /*hidden argument*/NULL);
		return L_102;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* StackTraceUtility_t6_246_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral737;
extern Il2CppCodeGenString* _stringLiteral510;
extern Il2CppCodeGenString* _stringLiteral167;
extern Il2CppCodeGenString* _stringLiteral106;
extern Il2CppCodeGenString* _stringLiteral4904;
extern Il2CppCodeGenString* _stringLiteral4905;
extern Il2CppCodeGenString* _stringLiteral4903;
extern Il2CppCodeGenString* _stringLiteral4828;
extern "C" String_t* StackTraceUtility_ExtractFormattedStackTrace_m6_1665 (Object_t * __this /* static, unused */, StackTrace_t1_336 * ___stackTrace, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		StackTraceUtility_t6_246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1693);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral737 = il2cpp_codegen_string_literal_from_index(737);
		_stringLiteral510 = il2cpp_codegen_string_literal_from_index(510);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		_stringLiteral4904 = il2cpp_codegen_string_literal_from_index(4904);
		_stringLiteral4905 = il2cpp_codegen_string_literal_from_index(4905);
		_stringLiteral4903 = il2cpp_codegen_string_literal_from_index(4903);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1_247 * V_0 = {0};
	int32_t V_1 = 0;
	StackFrame_t1_334 * V_2 = {0};
	MethodBase_t1_335 * V_3 = {0};
	Type_t * V_4 = {0};
	String_t* V_5 = {0};
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t1_1685* V_7 = {0};
	bool V_8 = false;
	String_t* V_9 = {0};
	int32_t V_10 = 0;
	{
		StringBuilder_t1_247 * L_0 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12415(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_01c9;
	}

IL_0012:
	{
		StackTrace_t1_336 * L_1 = ___stackTrace;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t1_334 * L_3 = (StackFrame_t1_334 *)VirtFuncInvoker1< StackFrame_t1_334 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t1_334 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t1_335 * L_5 = (MethodBase_t1_335 *)VirtFuncInvoker0< MethodBase_t1_335 * >::Invoke(8 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t1_335 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_01c5;
	}

IL_002c:
	{
		MethodBase_t1_335 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(22 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_01c5;
	}

IL_0040:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(161 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1_571(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		StringBuilder_t1_247 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m1_12438(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m1_12438(L_17, _stringLiteral56, /*hidden argument*/NULL);
	}

IL_0071:
	{
		StringBuilder_t1_247 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m1_12438(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m1_12438(L_21, _stringLiteral737, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_22 = V_0;
		MethodBase_t1_335 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m1_12438(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m1_12438(L_25, _stringLiteral510, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t1_335 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t1_1685* L_27 = (ParameterInfoU5BU5D_t1_1685*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1_1685* >::Invoke(68 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = 1;
		goto IL_00ee;
	}

IL_00b7:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t1_247 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m1_12438(L_29, _stringLiteral167, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = 0;
	}

IL_00d2:
	{
		StringBuilder_t1_247 * L_30 = V_0;
		ParameterInfoU5BU5D_t1_1685* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		NullCheck((*(ParameterInfo_t1_627 **)(ParameterInfo_t1_627 **)SZArrayLdElema(L_31, L_33, sizeof(ParameterInfo_t1_627 *))));
		Type_t * L_34 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(11 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, (*(ParameterInfo_t1_627 **)(ParameterInfo_t1_627 **)SZArrayLdElema(L_31, L_33, sizeof(ParameterInfo_t1_627 *))));
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, L_34);
		NullCheck(L_30);
		StringBuilder_Append_m1_12438(L_30, L_35, /*hidden argument*/NULL);
		int32_t L_36 = V_6;
		V_6 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_37 = V_6;
		ParameterInfoU5BU5D_t1_1685* L_38 = V_7;
		NullCheck(L_38);
		if ((((int32_t)L_37) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_38)->max_length)))))))
		{
			goto IL_00b7;
		}
	}
	{
		StringBuilder_t1_247 * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m1_12438(L_39, _stringLiteral106, /*hidden argument*/NULL);
		StackFrame_t1_334 * L_40 = V_2;
		NullCheck(L_40);
		String_t* L_41 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_40);
		V_9 = L_41;
		String_t* L_42 = V_9;
		if (!L_42)
		{
			goto IL_01b9;
		}
	}
	{
		Type_t * L_43 = V_4;
		NullCheck(L_43);
		String_t* L_44 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, L_43);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_45 = String_op_Equality_m1_601(NULL /*static, unused*/, L_44, _stringLiteral4904, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0140;
		}
	}
	{
		Type_t * L_46 = V_4;
		NullCheck(L_46);
		String_t* L_47 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(161 /* System.String System.Type::get_Namespace() */, L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_48 = String_op_Equality_m1_601(NULL /*static, unused*/, L_47, _stringLiteral4905, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01b9;
		}
	}

IL_0140:
	{
		StringBuilder_t1_247 * L_49 = V_0;
		NullCheck(L_49);
		StringBuilder_Append_m1_12438(L_49, _stringLiteral4903, /*hidden argument*/NULL);
		String_t* L_50 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		String_t* L_51 = ((StackTraceUtility_t6_246_StaticFields*)StackTraceUtility_t6_246_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_50);
		bool L_52 = String_StartsWith_m1_531(L_50, L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0182;
		}
	}
	{
		String_t* L_53 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t6_246_il2cpp_TypeInfo_var);
		String_t* L_54 = ((StackTraceUtility_t6_246_StaticFields*)StackTraceUtility_t6_246_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_54);
		int32_t L_55 = String_get_Length_m1_571(L_54, /*hidden argument*/NULL);
		String_t* L_56 = V_9;
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m1_571(L_56, /*hidden argument*/NULL);
		String_t* L_58 = ((StackTraceUtility_t6_246_StaticFields*)StackTraceUtility_t6_246_il2cpp_TypeInfo_var->static_fields)->___projectFolder_0;
		NullCheck(L_58);
		int32_t L_59 = String_get_Length_m1_571(L_58, /*hidden argument*/NULL);
		NullCheck(L_53);
		String_t* L_60 = String_Substring_m1_455(L_53, L_55, ((int32_t)((int32_t)L_57-(int32_t)L_59)), /*hidden argument*/NULL);
		V_9 = L_60;
	}

IL_0182:
	{
		StringBuilder_t1_247 * L_61 = V_0;
		String_t* L_62 = V_9;
		NullCheck(L_61);
		StringBuilder_Append_m1_12438(L_61, L_62, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_63 = V_0;
		NullCheck(L_63);
		StringBuilder_Append_m1_12438(L_63, _stringLiteral737, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_64 = V_0;
		StackFrame_t1_334 * L_65 = V_2;
		NullCheck(L_65);
		int32_t L_66 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_65);
		V_10 = L_66;
		String_t* L_67 = Int32_ToString_m1_101((&V_10), /*hidden argument*/NULL);
		NullCheck(L_64);
		StringBuilder_Append_m1_12438(L_64, L_67, /*hidden argument*/NULL);
		StringBuilder_t1_247 * L_68 = V_0;
		NullCheck(L_68);
		StringBuilder_Append_m1_12438(L_68, _stringLiteral106, /*hidden argument*/NULL);
	}

IL_01b9:
	{
		StringBuilder_t1_247 * L_69 = V_0;
		NullCheck(L_69);
		StringBuilder_Append_m1_12438(L_69, _stringLiteral4828, /*hidden argument*/NULL);
	}

IL_01c5:
	{
		int32_t L_70 = V_1;
		V_1 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01c9:
	{
		int32_t L_71 = V_1;
		StackTrace_t1_336 * L_72 = ___stackTrace;
		NullCheck(L_72);
		int32_t L_73 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_72);
		if ((((int32_t)L_71) < ((int32_t)L_73)))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t1_247 * L_74 = V_0;
		NullCheck(L_74);
		String_t* L_75 = StringBuilder_ToString_m1_12428(L_74, /*hidden argument*/NULL);
		return L_75;
	}
}
// System.Void UnityEngine.UnityException::.ctor()
extern Il2CppCodeGenString* _stringLiteral4906;
extern "C" void UnityException__ctor_m6_1666 (UnityException_t6_247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4906 = il2cpp_codegen_string_literal_from_index(4906);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m1_1238(__this, _stringLiteral4906, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C" void UnityException__ctor_m6_1667 (UnityException_t6_247 * __this, String_t* ___message, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception__ctor_m1_1238(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C" void UnityException__ctor_m6_1668 (UnityException_t6_247 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message;
		Exception_t1_33 * L_1 = ___innerException;
		Exception__ctor_m1_1240(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m1_1245(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnityException__ctor_m6_1669 (UnityException_t6_247 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		StreamingContext_t1_1050  L_1 = ___context;
		Exception__ctor_m1_1239(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m6_1670 (SharedBetweenAnimatorsAttribute_t6_248 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern "C" void StateMachineBehaviour__ctor_m6_1671 (StateMachineBehaviour_t6_249 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SystemClock::.cctor()
extern TypeInfo* SystemClock_t6_250_il2cpp_TypeInfo_var;
extern "C" void SystemClock__cctor_m6_1672 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SystemClock_t6_250_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1691);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_150  L_0 = {0};
		DateTime__ctor_m1_13793(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		((SystemClock_t6_250_StaticFields*)SystemClock_t6_250_il2cpp_TypeInfo_var->static_fields)->___s_Epoch_0 = L_0;
		return;
	}
}
// System.DateTime UnityEngine.SystemClock::get_now()
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  SystemClock_get_now_m6_1673 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_0 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void TextEditor__ctor_m6_1674 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1239(L_0, /*hidden argument*/NULL);
		__this->___content_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_1 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___style_3 = L_1;
		Vector2_t6_47  L_2 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollOffset_8 = L_2;
		__this->___m_iAltCursorPos_19 = (-1);
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.TextEditor::get_position()
extern "C" Rect_t6_51  TextEditor_get_position_m6_1675 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = (__this->___m_Position_9);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_position(UnityEngine.Rect)
extern "C" void TextEditor_set_position_m6_1676 (TextEditor_t6_254 * __this, Rect_t6_51  ___value, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = (__this->___m_Position_9);
		Rect_t6_51  L_1 = ___value;
		bool L_2 = Rect_op_Equality_m6_326(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Rect_t6_51  L_3 = ___value;
		__this->___m_Position_9 = L_3;
		TextEditor_UpdateScrollOffset_m6_1746(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_cursorIndex()
extern "C" int32_t TextEditor_get_cursorIndex_m6_1677 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_CursorIndex_10);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_cursorIndex(System.Int32)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void TextEditor_set_cursorIndex_m6_1678 (TextEditor_t6_254 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___m_CursorIndex_10);
		V_0 = L_0;
		int32_t L_1 = ___value;
		GUIContent_t6_171 * L_2 = (__this->___content_2);
		NullCheck(L_2);
		String_t* L_3 = GUIContent_get_text_m6_1243(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_571(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Clamp_m6_440(NULL /*static, unused*/, L_1, 0, L_4, /*hidden argument*/NULL);
		__this->___m_CursorIndex_10 = L_5;
		int32_t L_6 = (__this->___m_CursorIndex_10);
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0037;
		}
	}
	{
		__this->___m_RevealCursor_12 = 1;
	}

IL_0037:
	{
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_selectIndex()
extern "C" int32_t TextEditor_get_selectIndex_m6_1679 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_SelectIndex_11);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_selectIndex(System.Int32)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void TextEditor_set_selectIndex_m6_1680 (TextEditor_t6_254 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		GUIContent_t6_171 * L_1 = (__this->___content_2);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m6_1243(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Clamp_m6_440(NULL /*static, unused*/, L_0, 0, L_3, /*hidden argument*/NULL);
		__this->___m_SelectIndex_11 = L_4;
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C" void TextEditor_ClearCursorPos_m6_1681 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		__this->___hasHorizontalCursorPos_5 = 0;
		__this->___m_iAltCursorPos_19 = (-1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C" void TextEditor_OnFocus_m6_1682 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___multiline_4);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 0;
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_2, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m6_1691(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		__this->___m_HasFocus_7 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C" void TextEditor_OnLostFocus_m6_1683 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasFocus_7 = 0;
		Vector2_t6_47  L_0 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___scrollOffset_8 = L_0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::GrabGraphicalCursorPos()
extern "C" void TextEditor_GrabGraphicalCursorPos_m6_1684 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___hasHorizontalCursorPos_5);
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (__this->___style_3);
		Rect_t6_51  L_2 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t6_47  L_5 = GUIStyle_GetCursorPixelPosition_m6_1445(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_5;
		GUIStyle_t6_176 * L_6 = (__this->___style_3);
		Rect_t6_51  L_7 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_8 = (__this->___content_2);
		int32_t L_9 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t6_47  L_10 = GUIStyle_GetCursorPixelPosition_m6_1445(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->___graphicalSelectCursorPos_14 = L_10;
		__this->___hasHorizontalCursorPos_5 = 0;
	}

IL_0058:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::HandleKeyEvent(UnityEngine.Event)
extern TypeInfo* TextEditor_t6_254_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_HandleKeyEvent_m6_1685 (TextEditor_t6_254 * __this, Event_t6_162 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1647);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		TextEditor_InitKeyActions_m6_1755(__this, /*hidden argument*/NULL);
		Event_t6_162 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = Event_get_modifiers_m6_1175(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_162 * L_2 = ___e;
		Event_t6_162 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = Event_get_modifiers_m6_1175(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Event_set_modifiers_m6_1176(L_3, ((int32_t)((int32_t)L_4&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		Dictionary_2_t1_1831 * L_5 = ((TextEditor_t6_254_StaticFields*)TextEditor_t6_254_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		Event_t6_162 * L_6 = ___e;
		NullCheck(L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, Event_t6_162 * >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(!0) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t1_1831 * L_8 = ((TextEditor_t6_254_StaticFields*)TextEditor_t6_254_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		Event_t6_162 * L_9 = ___e;
		NullCheck(L_8);
		int32_t L_10 = (int32_t)VirtFuncInvoker1< int32_t, Event_t6_162 * >::Invoke(22 /* !1 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(!0) */, L_8, L_9);
		V_1 = L_10;
		int32_t L_11 = V_1;
		TextEditor_PerformOperation_m6_1748(__this, L_11, /*hidden argument*/NULL);
		Event_t6_162 * L_12 = ___e;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Event_set_modifiers_m6_1176(L_12, L_13, /*hidden argument*/NULL);
		return 1;
	}

IL_0049:
	{
		Event_t6_162 * L_14 = ___e;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Event_set_modifiers_m6_1176(L_14, L_15, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteLineBack()
extern "C" bool TextEditor_DeleteLineBack_m6_1686 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1693(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		goto IL_0043;
	}

IL_0022:
	{
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1243(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		uint16_t L_6 = String_get_Chars_m1_442(L_4, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_7 = V_1;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		goto IL_004d;
	}

IL_0043:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = L_8;
		V_1 = ((int32_t)((int32_t)L_9-(int32_t)1));
		if (L_9)
		{
			goto IL_0022;
		}
	}

IL_004d:
	{
		int32_t L_10 = V_1;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_0056;
		}
	}
	{
		V_0 = 0;
	}

IL_0056:
	{
		int32_t L_11 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) == ((int32_t)L_12)))
		{
			goto IL_0098;
		}
	}
	{
		GUIContent_t6_171 * L_13 = (__this->___content_2);
		GUIContent_t6_171 * L_14 = (__this->___content_2);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m6_1243(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		NullCheck(L_15);
		String_t* L_19 = String_Remove_m1_539(L_15, L_16, ((int32_t)((int32_t)L_17-(int32_t)L_18)), /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIContent_set_text_m6_1244(L_13, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		V_2 = L_20;
		int32_t L_21 = V_2;
		TextEditor_set_cursorIndex_m6_1678(__this, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_2;
		TextEditor_set_selectIndex_m6_1680(__this, L_22, /*hidden argument*/NULL);
		return 1;
	}

IL_0098:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordBack()
extern "C" bool TextEditor_DeleteWordBack_m6_1687 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1693(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindEndOfPreviousWord_m6_1732(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0063;
		}
	}
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		GUIContent_t6_171 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1243(L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		NullCheck(L_7);
		String_t* L_11 = String_Remove_m1_539(L_7, L_8, ((int32_t)((int32_t)L_9-(int32_t)L_10)), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1244(L_5, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_1 = L_12;
		int32_t L_13 = V_1;
		TextEditor_set_cursorIndex_m6_1678(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		TextEditor_set_selectIndex_m6_1680(__this, L_14, /*hidden argument*/NULL);
		return 1;
	}

IL_0063:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordForward()
extern "C" bool TextEditor_DeleteWordForward_m6_1688 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1693(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindStartOfNextWord_m6_1731(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_4 = (__this->___content_2);
		NullCheck(L_4);
		String_t* L_5 = GUIContent_get_text_m6_1243(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_6)))
		{
			goto IL_0067;
		}
	}
	{
		GUIContent_t6_171 * L_7 = (__this->___content_2);
		GUIContent_t6_171 * L_8 = (__this->___content_2);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1243(L_8, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		int32_t L_12 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_13 = String_Remove_m1_539(L_9, L_10, ((int32_t)((int32_t)L_11-(int32_t)L_12)), /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIContent_set_text_m6_1244(L_7, L_13, /*hidden argument*/NULL);
		return 1;
	}

IL_0067:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::Delete()
extern "C" bool TextEditor_Delete_m6_1689 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1693(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_2 = (__this->___content_2);
		NullCheck(L_2);
		String_t* L_3 = GUIContent_get_text_m6_1243(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_571(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_4)))
		{
			goto IL_0053;
		}
	}
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		GUIContent_t6_171 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1243(L_6, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = String_Remove_m1_539(L_7, L_8, 1, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1244(L_5, L_9, /*hidden argument*/NULL);
		return 1;
	}

IL_0053:
	{
		return 0;
	}
}
// System.Boolean UnityEngine.TextEditor::Backspace()
extern "C" bool TextEditor_Backspace_m6_1690 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m6_1693(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0063;
		}
	}
	{
		GUIContent_t6_171 * L_2 = (__this->___content_2);
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1243(L_3, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_6 = String_Remove_m1_539(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), 1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIContent_set_text_m6_1244(L_2, L_6, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7-(int32_t)1));
		int32_t L_8 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_9, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0063:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C" void TextEditor_SelectAll_m6_1691 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m6_1678(__this, 0, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_2, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectNone()
extern "C" void TextEditor_SelectNone_m6_1692 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_0, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::get_hasSelection()
extern "C" bool TextEditor_get_hasSelection_m6_1693 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_DeleteSelection_m6_1694 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_5, /*hidden argument*/NULL);
	}

IL_0024:
	{
		int32_t L_6 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) <= ((int32_t)L_7)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_8 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_8, /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		int32_t L_11 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_00c0;
		}
	}
	{
		GUIContent_t6_171 * L_13 = (__this->___content_2);
		GUIContent_t6_171 * L_14 = (__this->___content_2);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m6_1243(L_14, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_17 = String_Substring_m1_455(L_15, 0, L_16, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_18 = (__this->___content_2);
		NullCheck(L_18);
		String_t* L_19 = GUIContent_get_text_m6_1243(L_18, /*hidden argument*/NULL);
		int32_t L_20 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_21 = (__this->___content_2);
		NullCheck(L_21);
		String_t* L_22 = GUIContent_get_text_m6_1243(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1_571(L_22, /*hidden argument*/NULL);
		int32_t L_24 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_25 = String_Substring_m1_455(L_19, L_20, ((int32_t)((int32_t)L_23-(int32_t)L_24)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_559(NULL /*static, unused*/, L_17, L_25, /*hidden argument*/NULL);
		NullCheck(L_13);
		GUIContent_set_text_m6_1244(L_13, L_26, /*hidden argument*/NULL);
		int32_t L_27 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_27, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_00c0:
	{
		GUIContent_t6_171 * L_28 = (__this->___content_2);
		GUIContent_t6_171 * L_29 = (__this->___content_2);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m6_1243(L_29, /*hidden argument*/NULL);
		int32_t L_31 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_32 = String_Substring_m1_455(L_30, 0, L_31, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_33 = (__this->___content_2);
		NullCheck(L_33);
		String_t* L_34 = GUIContent_get_text_m6_1243(L_33, /*hidden argument*/NULL);
		int32_t L_35 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_36 = (__this->___content_2);
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m6_1243(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m1_571(L_37, /*hidden argument*/NULL);
		int32_t L_39 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_40 = String_Substring_m1_455(L_34, L_35, ((int32_t)((int32_t)L_38-(int32_t)L_39)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m1_559(NULL /*static, unused*/, L_32, L_40, /*hidden argument*/NULL);
		NullCheck(L_28);
		GUIContent_set_text_m6_1244(L_28, L_41, /*hidden argument*/NULL);
		int32_t L_42 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_42, /*hidden argument*/NULL);
	}

IL_0120:
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C" void TextEditor_ReplaceSelection_m6_1695 (TextEditor_t6_254 * __this, String_t* ___replace, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		GUIContent_t6_171 * L_1 = (__this->___content_2);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m6_1243(L_1, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___replace;
		NullCheck(L_2);
		String_t* L_5 = String_Insert_m1_565(L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIContent_set_text_m6_1244(L_0, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		String_t* L_7 = ___replace;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1_571(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ((int32_t)((int32_t)L_6+(int32_t)L_8));
		V_0 = L_9;
		TextEditor_set_cursorIndex_m6_1678(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_10, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::Insert(System.Char)
extern "C" void TextEditor_Insert_m6_1696 (TextEditor_t6_254 * __this, uint16_t ___c, const MethodInfo* method)
{
	{
		String_t* L_0 = Char_ToString_m1_409((&___c), /*hidden argument*/NULL);
		TextEditor_ReplaceSelection_m6_1695(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveRight()
extern "C" void TextEditor_MoveRight_m6_1697 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_DetectFocusChange_m6_1756(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_3, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003c:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_6 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_6, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_005e:
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_7, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLeft()
extern "C" void TextEditor_MoveLeft_m6_1698 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_3, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0030:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_6, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0052:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_7, /*hidden argument*/NULL);
	}

IL_005e:
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveUp()
extern "C" void TextEditor_MoveUp_m6_1699 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m6_1684(__this, /*hidden argument*/NULL);
		Vector2_t6_47 * L_4 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_47 * L_5 = L_4;
		float L_6 = (L_5->___y_2);
		L_5->___y_2 = ((float)((float)L_6-(float)(1.0f)));
		GUIStyle_t6_176 * L_7 = (__this->___style_3);
		Rect_t6_51  L_8 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_9 = (__this->___content_2);
		Vector2_t6_47  L_10 = (__this->___graphicalCursorPos_13);
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m6_1446(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveDown()
extern "C" void TextEditor_MoveDown_m6_1700 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m6_1684(__this, /*hidden argument*/NULL);
		Vector2_t6_47 * L_4 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_47 * L_5 = L_4;
		float L_6 = (L_5->___y_2);
		GUIStyle_t6_176 * L_7 = (__this->___style_3);
		NullCheck(L_7);
		float L_8 = GUIStyle_get_lineHeight_m6_1436(L_7, /*hidden argument*/NULL);
		L_5->___y_2 = ((float)((float)L_6+(float)((float)((float)L_8+(float)(5.0f)))));
		GUIStyle_t6_176 * L_9 = (__this->___style_3);
		Rect_t6_51  L_10 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_11 = (__this->___content_2);
		Vector2_t6_47  L_12 = (__this->___graphicalCursorPos_13);
		NullCheck(L_9);
		int32_t L_13 = GUIStyle_GetCursorStringIndex_m6_1446(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_15, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_17 = (__this->___content_2);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m6_1243(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m1_571(L_18, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_19))))
		{
			goto IL_00a4;
		}
	}
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineStart()
extern "C" void TextEditor_MoveLineStart_m6_1701 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		goto IL_0055;
	}

IL_002a:
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1243(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m1_442(L_6, L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_9 = V_1;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		int32_t L_10 = V_2;
		TextEditor_set_cursorIndex_m6_1678(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		TextEditor_set_selectIndex_m6_1680(__this, L_11, /*hidden argument*/NULL);
		return;
	}

IL_0055:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		V_1 = ((int32_t)((int32_t)L_13-(int32_t)1));
		if (L_13)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = 0;
		int32_t L_14 = V_2;
		TextEditor_set_cursorIndex_m6_1678(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		TextEditor_set_selectIndex_m6_1680(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineEnd()
extern "C" void TextEditor_MoveLineEnd_m6_1702 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1243(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_571(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_0068;
	}

IL_003b:
	{
		GUIContent_t6_171 * L_8 = (__this->___content_2);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1243(L_8, /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		uint16_t L_11 = String_get_Chars_m1_442(L_9, L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_12 = V_1;
		V_3 = L_12;
		int32_t L_13 = V_3;
		TextEditor_set_cursorIndex_m6_1678(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_3;
		TextEditor_set_selectIndex_m6_1680(__this, L_14, /*hidden argument*/NULL);
		return;
	}

IL_0064:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_2;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_18 = V_2;
		V_3 = L_18;
		int32_t L_19 = V_3;
		TextEditor_set_cursorIndex_m6_1678(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_3;
		TextEditor_set_selectIndex_m6_1680(__this, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineStart()
extern "C" void TextEditor_MoveGraphicalLineStart_m6_1703 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B2_0 = {0};
	TextEditor_t6_254 * G_B2_1 = {0};
	TextEditor_t6_254 * G_B1_0 = {0};
	TextEditor_t6_254 * G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_254 * G_B3_1 = {0};
	TextEditor_t6_254 * G_B3_2 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineStart_m6_1720(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m6_1678(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineEnd()
extern "C" void TextEditor_MoveGraphicalLineEnd_m6_1704 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B2_0 = {0};
	TextEditor_t6_254 * G_B2_1 = {0};
	TextEditor_t6_254 * G_B1_0 = {0};
	TextEditor_t6_254 * G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_254 * G_B3_1 = {0};
	TextEditor_t6_254 * G_B3_2 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineEnd_m6_1721(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m6_1678(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextStart()
extern "C" void TextEditor_MoveTextStart_m6_1705 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextEnd()
extern "C" void TextEditor_MoveTextEnd_m6_1706 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::IndexOfEndOfLine(System.Int32)
extern "C" int32_t TextEditor_IndexOfEndOfLine_m6_1707 (TextEditor_t6_254 * __this, int32_t ___startIndex, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___startIndex;
		NullCheck(L_1);
		int32_t L_3 = String_IndexOf_m1_501(L_1, ((int32_t)10), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_5 = V_0;
		G_B3_0 = L_5;
		goto IL_0031;
	}

IL_0021:
	{
		GUIContent_t6_171 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1243(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1_571(L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0031:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphForward()
extern "C" void TextEditor_MoveParagraphForward_m6_1708 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B2_0 = {0};
	TextEditor_t6_254 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_254 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1678(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1243(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_571(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)L_7)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_IndexOfEndOfLine_m6_1707(__this, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_11, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphBackward()
extern "C" void TextEditor_MoveParagraphBackward_m6_1709 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B2_0 = {0};
	TextEditor_t6_254 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_254 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1678(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0064;
		}
	}
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1243(L_5, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_8 = String_LastIndexOf_m1_513(L_6, ((int32_t)10), ((int32_t)((int32_t)L_7-(int32_t)2)), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_9 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_10, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_0064:
	{
		V_0 = 0;
		int32_t L_11 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_12, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveCursorToPosition(UnityEngine.Vector2)
extern "C" void TextEditor_MoveCursorToPosition_m6_1710 (TextEditor_t6_254 * __this, Vector2_t6_47  ___cursorPosition, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___style_3);
		Rect_t6_51  L_1 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_2 = (__this->___content_2);
		Vector2_t6_47  L_3 = ___cursorPosition;
		Vector2_t6_47  L_4 = (__this->___scrollOffset_8);
		Vector2_t6_47  L_5 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_6 = GUIStyle_GetCursorStringIndex_m6_1446(L_0, L_1, L_2, L_5, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_6, /*hidden argument*/NULL);
		Event_t6_162 * L_7 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Event_get_shift_m6_1137(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		TextEditor_DetectFocusChange_m6_1756(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToPosition(UnityEngine.Vector2)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void TextEditor_SelectToPosition_m6_1711 (TextEditor_t6_254 * __this, Vector2_t6_47  ___cursorPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___m_MouseDragSelectsWholeWords_15);
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (__this->___style_3);
		Rect_t6_51  L_2 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		Vector2_t6_47  L_4 = ___cursorPosition;
		Vector2_t6_47  L_5 = (__this->___scrollOffset_8);
		Vector2_t6_47  L_6 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m6_1446(L_1, L_2, L_3, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_7, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0039:
	{
		GUIStyle_t6_176 * L_8 = (__this->___style_3);
		Rect_t6_51  L_9 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_10 = (__this->___content_2);
		Vector2_t6_47  L_11 = ___cursorPosition;
		Vector2_t6_47  L_12 = (__this->___scrollOffset_8);
		Vector2_t6_47  L_13 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_14 = GUIStyle_GetCursorStringIndex_m6_1446(L_8, L_9, L_10, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		uint8_t L_15 = (__this->___m_DblClickSnap_17);
		if (L_15)
		{
			goto IL_00eb;
		}
	}
	{
		int32_t L_16 = V_0;
		int32_t L_17 = (__this->___m_DblClickInitPos_16);
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_18 = V_0;
		int32_t L_19 = TextEditor_FindEndOfClassification_m6_1743(__this, L_18, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = (__this->___m_DblClickInitPos_16);
		int32_t L_21 = TextEditor_FindEndOfClassification_m6_1743(__this, L_20, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_21, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_009a:
	{
		int32_t L_22 = V_0;
		GUIContent_t6_171 * L_23 = (__this->___content_2);
		NullCheck(L_23);
		String_t* L_24 = GUIContent_get_text_m6_1243(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1_571(L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_25)))
		{
			goto IL_00c3;
		}
	}
	{
		GUIContent_t6_171 * L_26 = (__this->___content_2);
		NullCheck(L_26);
		String_t* L_27 = GUIContent_get_text_m6_1243(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = String_get_Length_m1_571(L_27, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_28-(int32_t)1));
	}

IL_00c3:
	{
		int32_t L_29 = V_0;
		int32_t L_30 = TextEditor_FindEndOfClassification_m6_1743(__this, L_29, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_30, /*hidden argument*/NULL);
		int32_t L_31 = (__this->___m_DblClickInitPos_16);
		int32_t L_32 = TextEditor_FindEndOfClassification_m6_1743(__this, ((int32_t)((int32_t)L_31-(int32_t)1)), (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_32, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		int32_t L_33 = V_0;
		int32_t L_34 = (__this->___m_DblClickInitPos_16);
		if ((((int32_t)L_33) >= ((int32_t)L_34)))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_35 = V_0;
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_0126;
		}
	}
	{
		GUIContent_t6_171 * L_36 = (__this->___content_2);
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m6_1243(L_36, /*hidden argument*/NULL);
		int32_t L_38 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_39 = Mathf_Max_m6_430(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_38-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_40 = String_LastIndexOf_m1_513(L_37, ((int32_t)10), L_39, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_40+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_012d;
	}

IL_0126:
	{
		TextEditor_set_cursorIndex_m6_1678(__this, 0, /*hidden argument*/NULL);
	}

IL_012d:
	{
		GUIContent_t6_171 * L_41 = (__this->___content_2);
		NullCheck(L_41);
		String_t* L_42 = GUIContent_get_text_m6_1243(L_41, /*hidden argument*/NULL);
		int32_t L_43 = (__this->___m_DblClickInitPos_16);
		NullCheck(L_42);
		int32_t L_44 = String_LastIndexOf_m1_513(L_42, ((int32_t)10), L_43, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_44, /*hidden argument*/NULL);
		goto IL_01b6;
	}

IL_0150:
	{
		int32_t L_45 = V_0;
		GUIContent_t6_171 * L_46 = (__this->___content_2);
		NullCheck(L_46);
		String_t* L_47 = GUIContent_get_text_m6_1243(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m1_571(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_45) >= ((int32_t)L_48)))
		{
			goto IL_0178;
		}
	}
	{
		int32_t L_49 = V_0;
		int32_t L_50 = TextEditor_IndexOfEndOfLine_m6_1707(__this, L_49, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_50, /*hidden argument*/NULL);
		goto IL_018e;
	}

IL_0178:
	{
		GUIContent_t6_171 * L_51 = (__this->___content_2);
		NullCheck(L_51);
		String_t* L_52 = GUIContent_get_text_m6_1243(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = String_get_Length_m1_571(L_52, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_53, /*hidden argument*/NULL);
	}

IL_018e:
	{
		GUIContent_t6_171 * L_54 = (__this->___content_2);
		NullCheck(L_54);
		String_t* L_55 = GUIContent_get_text_m6_1243(L_54, /*hidden argument*/NULL);
		int32_t L_56 = (__this->___m_DblClickInitPos_16);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_57 = Mathf_Max_m6_430(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_56-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_55);
		int32_t L_58 = String_LastIndexOf_m1_513(L_55, ((int32_t)10), L_57, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, ((int32_t)((int32_t)L_58+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_01b6:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectLeft()
extern "C" void TextEditor_SelectLeft_m6_1712 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___m_bJustSelected_18);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->___m_bJustSelected_18 = 0;
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectRight()
extern "C" void TextEditor_SelectRight_m6_1713 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = (__this->___m_bJustSelected_18);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->___m_bJustSelected_18 = 0;
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		GUIContent_t6_171 * L_7 = (__this->___content_2);
		NullCheck(L_7);
		String_t* L_8 = GUIContent_get_text_m6_1243(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1_571(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) <= ((int32_t)L_11)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_12 = V_1;
		TextEditor_set_cursorIndex_m6_1678(__this, L_12, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectUp()
extern "C" void TextEditor_SelectUp_m6_1714 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m6_1684(__this, /*hidden argument*/NULL);
		Vector2_t6_47 * L_0 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_47 * L_1 = L_0;
		float L_2 = (L_1->___y_2);
		L_1->___y_2 = ((float)((float)L_2-(float)(1.0f)));
		GUIStyle_t6_176 * L_3 = (__this->___style_3);
		Rect_t6_51  L_4 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		Vector2_t6_47  L_6 = (__this->___graphicalCursorPos_13);
		NullCheck(L_3);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m6_1446(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectDown()
extern "C" void TextEditor_SelectDown_m6_1715 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m6_1684(__this, /*hidden argument*/NULL);
		Vector2_t6_47 * L_0 = &(__this->___graphicalCursorPos_13);
		Vector2_t6_47 * L_1 = L_0;
		float L_2 = (L_1->___y_2);
		GUIStyle_t6_176 * L_3 = (__this->___style_3);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_lineHeight_m6_1436(L_3, /*hidden argument*/NULL);
		L_1->___y_2 = ((float)((float)L_2+(float)((float)((float)L_4+(float)(5.0f)))));
		GUIStyle_t6_176 * L_5 = (__this->___style_3);
		Rect_t6_51  L_6 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_7 = (__this->___content_2);
		Vector2_t6_47  L_8 = (__this->___graphicalCursorPos_13);
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m6_1446(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextEnd()
extern "C" void TextEditor_SelectTextEnd_m6_1716 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextStart()
extern "C" void TextEditor_SelectTextStart_m6_1717 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m6_1678(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MouseDragSelectsWholeWords(System.Boolean)
extern "C" void TextEditor_MouseDragSelectsWholeWords_m6_1718 (TextEditor_t6_254 * __this, bool ___on, const MethodInfo* method)
{
	{
		bool L_0 = ___on;
		__this->___m_MouseDragSelectsWholeWords_15 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		__this->___m_DblClickInitPos_16 = L_1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::DblClickSnap(UnityEngine.TextEditor/DblClickSnapping)
extern "C" void TextEditor_DblClickSnap_m6_1719 (TextEditor_t6_254 * __this, uint8_t ___snapping, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___snapping;
		__this->___m_DblClickSnap_17 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineStart(System.Int32)
extern "C" int32_t TextEditor_GetGraphicalLineStart_m6_1720 (TextEditor_t6_254 * __this, int32_t ___p, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		GUIStyle_t6_176 * L_0 = (__this->___style_3);
		Rect_t6_51  L_1 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_2 = (__this->___content_2);
		int32_t L_3 = ___p;
		NullCheck(L_0);
		Vector2_t6_47  L_4 = GUIStyle_GetCursorPixelPosition_m6_1445(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		(&V_0)->___x_1 = (0.0f);
		GUIStyle_t6_176 * L_5 = (__this->___style_3);
		Rect_t6_51  L_6 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_7 = (__this->___content_2);
		Vector2_t6_47  L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m6_1446(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineEnd(System.Int32)
extern "C" int32_t TextEditor_GetGraphicalLineEnd_m6_1721 (TextEditor_t6_254 * __this, int32_t ___p, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		GUIStyle_t6_176 * L_0 = (__this->___style_3);
		Rect_t6_51  L_1 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_2 = (__this->___content_2);
		int32_t L_3 = ___p;
		NullCheck(L_0);
		Vector2_t6_47  L_4 = GUIStyle_GetCursorPixelPosition_m6_1445(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t6_47 * L_5 = (&V_0);
		float L_6 = (L_5->___x_1);
		L_5->___x_1 = ((float)((float)L_6+(float)(5000.0f)));
		GUIStyle_t6_176 * L_7 = (__this->___style_3);
		Rect_t6_51  L_8 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_9 = (__this->___content_2);
		Vector2_t6_47  L_10 = V_0;
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m6_1446(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Int32 UnityEngine.TextEditor::FindNextSeperator(System.Int32)
extern "C" int32_t TextEditor_FindNextSeperator_m6_1722 (TextEditor_t6_254 * __this, int32_t ___startPos, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001b;
	}

IL_0016:
	{
		int32_t L_3 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_4 = ___startPos;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_003d;
		}
	}
	{
		GUIContent_t6_171 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1243(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___startPos;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_442(L_7, L_8, /*hidden argument*/NULL);
		bool L_10 = TextEditor_isLetterLikeChar_m6_1723(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0016;
		}
	}

IL_003d:
	{
		goto IL_0047;
	}

IL_0042:
	{
		int32_t L_11 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_12 = ___startPos;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_0069;
		}
	}
	{
		GUIContent_t6_171 * L_14 = (__this->___content_2);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m6_1243(L_14, /*hidden argument*/NULL);
		int32_t L_16 = ___startPos;
		NullCheck(L_15);
		uint16_t L_17 = String_get_Chars_m1_442(L_15, L_16, /*hidden argument*/NULL);
		bool L_18 = TextEditor_isLetterLikeChar_m6_1723(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0042;
		}
	}

IL_0069:
	{
		int32_t L_19 = ___startPos;
		return L_19;
	}
}
// System.Boolean UnityEngine.TextEditor::isLetterLikeChar(System.Char)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_isLetterLikeChar_m6_1723 (Object_t * __this /* static, unused */, uint16_t ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		uint16_t L_0 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsLetterOrDigit_m1_380(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		uint16_t L_2 = ___c;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)((int32_t)39)))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 1;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.TextEditor::FindPrevSeperator(System.Int32)
extern "C" int32_t TextEditor_FindPrevSeperator_m6_1724 (TextEditor_t6_254 * __this, int32_t ___startPos, const MethodInfo* method)
{
	{
		int32_t L_0 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_0-(int32_t)1));
		goto IL_000f;
	}

IL_000a:
	{
		int32_t L_1 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_1-(int32_t)1));
	}

IL_000f:
	{
		int32_t L_2 = ___startPos;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1243(L_3, /*hidden argument*/NULL);
		int32_t L_5 = ___startPos;
		NullCheck(L_4);
		uint16_t L_6 = String_get_Chars_m1_442(L_4, L_5, /*hidden argument*/NULL);
		bool L_7 = TextEditor_isLetterLikeChar_m6_1723(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_000a;
		}
	}

IL_0031:
	{
		goto IL_003b;
	}

IL_0036:
	{
		int32_t L_8 = ___startPos;
		___startPos = ((int32_t)((int32_t)L_8-(int32_t)1));
	}

IL_003b:
	{
		int32_t L_9 = ___startPos;
		if ((((int32_t)L_9) < ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GUIContent_t6_171 * L_10 = (__this->___content_2);
		NullCheck(L_10);
		String_t* L_11 = GUIContent_get_text_m6_1243(L_10, /*hidden argument*/NULL);
		int32_t L_12 = ___startPos;
		NullCheck(L_11);
		uint16_t L_13 = String_get_Chars_m1_442(L_11, L_12, /*hidden argument*/NULL);
		bool L_14 = TextEditor_isLetterLikeChar_m6_1723(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0036;
		}
	}

IL_005d:
	{
		int32_t L_15 = ___startPos;
		return ((int32_t)((int32_t)L_15+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::MoveWordRight()
extern "C" void TextEditor_MoveWordRight_m6_1725 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B2_0 = {0};
	TextEditor_t6_254 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_254 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1678(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindNextSeperator_m6_1722(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_7, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToStartOfNextWord()
extern "C" void TextEditor_MoveToStartOfNextWord_m6_1726 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveRight_m6_1697(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindStartOfNextWord_m6_1731(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToEndOfPreviousWord()
extern "C" void TextEditor_MoveToEndOfPreviousWord_m6_1727 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveLeft_m6_1698(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindEndOfPreviousWord_m6_1732(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToStartOfNextWord()
extern "C" void TextEditor_SelectToStartOfNextWord_m6_1728 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindStartOfNextWord_m6_1731(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToEndOfPreviousWord()
extern "C" void TextEditor_SelectToEndOfPreviousWord_m6_1729 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindEndOfPreviousWord_m6_1732(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextEditor/CharacterType UnityEngine.TextEditor::ClassifyChar(System.Char)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int32_t TextEditor_ClassifyChar_m6_1730 (TextEditor_t6_254 * __this, uint16_t ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m1_398(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(3);
	}

IL_000d:
	{
		uint16_t L_2 = ___c;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_3 = Char_IsLetterOrDigit_m1_380(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		uint16_t L_4 = ___c;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (int32_t)(0);
	}

IL_0022:
	{
		return (int32_t)(1);
	}
}
// System.Int32 UnityEngine.TextEditor::FindStartOfNextWord(System.Int32)
extern TypeInfo* Char_t1_15_il2cpp_TypeInfo_var;
extern "C" int32_t TextEditor_FindStartOfNextWord_m6_1731 (TextEditor_t6_254 * __this, int32_t ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Char_t1_15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint16_t V_1 = 0x0;
	int32_t V_2 = {0};
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = ___p;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_5 = ___p;
		return L_5;
	}

IL_001a:
	{
		GUIContent_t6_171 * L_6 = (__this->___content_2);
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1243(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___p;
		NullCheck(L_7);
		uint16_t L_9 = String_get_Chars_m1_442(L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		uint16_t L_10 = V_1;
		int32_t L_11 = TextEditor_ClassifyChar_m6_1730(__this, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) == ((int32_t)3)))
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_13 = ___p;
		___p = ((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_004a;
	}

IL_0045:
	{
		int32_t L_14 = ___p;
		___p = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_15 = ___p;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_006e;
		}
	}
	{
		GUIContent_t6_171 * L_17 = (__this->___content_2);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m6_1243(L_17, /*hidden argument*/NULL);
		int32_t L_19 = ___p;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m1_442(L_18, L_19, /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_ClassifyChar_m6_1730(__this, L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) == ((int32_t)L_22)))
		{
			goto IL_0045;
		}
	}

IL_006e:
	{
		goto IL_0087;
	}

IL_0073:
	{
		uint16_t L_23 = V_1;
		if ((((int32_t)L_23) == ((int32_t)((int32_t)9))))
		{
			goto IL_0083;
		}
	}
	{
		uint16_t L_24 = V_1;
		if ((!(((uint32_t)L_24) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0087;
		}
	}

IL_0083:
	{
		int32_t L_25 = ___p;
		return ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_26 = ___p;
		int32_t L_27 = V_0;
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_28 = ___p;
		return L_28;
	}

IL_0090:
	{
		GUIContent_t6_171 * L_29 = (__this->___content_2);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m6_1243(L_29, /*hidden argument*/NULL);
		int32_t L_31 = ___p;
		NullCheck(L_30);
		uint16_t L_32 = String_get_Chars_m1_442(L_30, L_31, /*hidden argument*/NULL);
		V_1 = L_32;
		uint16_t L_33 = V_1;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_00b4;
	}

IL_00af:
	{
		int32_t L_34 = ___p;
		___p = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00b4:
	{
		int32_t L_35 = ___p;
		int32_t L_36 = V_0;
		if ((((int32_t)L_35) >= ((int32_t)L_36)))
		{
			goto IL_00d6;
		}
	}
	{
		GUIContent_t6_171 * L_37 = (__this->___content_2);
		NullCheck(L_37);
		String_t* L_38 = GUIContent_get_text_m6_1243(L_37, /*hidden argument*/NULL);
		int32_t L_39 = ___p;
		NullCheck(L_38);
		uint16_t L_40 = String_get_Chars_m1_442(L_38, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t1_15_il2cpp_TypeInfo_var);
		bool L_41 = Char_IsWhiteSpace_m1_398(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_00af;
		}
	}

IL_00d6:
	{
		goto IL_00ed;
	}

IL_00db:
	{
		uint16_t L_42 = V_1;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)9))))
		{
			goto IL_00eb;
		}
	}
	{
		uint16_t L_43 = V_1;
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00ed;
		}
	}

IL_00eb:
	{
		int32_t L_44 = ___p;
		return L_44;
	}

IL_00ed:
	{
		int32_t L_45 = ___p;
		return L_45;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfPreviousWord(System.Int32)
extern "C" int32_t TextEditor_FindEndOfPreviousWord_m6_1732 (TextEditor_t6_254 * __this, int32_t ___p, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___p;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___p;
		return L_1;
	}

IL_0008:
	{
		int32_t L_2 = ___p;
		___p = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_0017;
	}

IL_0012:
	{
		int32_t L_3 = ___p;
		___p = ((int32_t)((int32_t)L_3-(int32_t)1));
	}

IL_0017:
	{
		int32_t L_4 = ___p;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1243(L_5, /*hidden argument*/NULL);
		int32_t L_7 = ___p;
		NullCheck(L_6);
		uint16_t L_8 = String_get_Chars_m1_442(L_6, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)((int32_t)32))))
		{
			goto IL_0012;
		}
	}

IL_0036:
	{
		GUIContent_t6_171 * L_9 = (__this->___content_2);
		NullCheck(L_9);
		String_t* L_10 = GUIContent_get_text_m6_1243(L_9, /*hidden argument*/NULL);
		int32_t L_11 = ___p;
		NullCheck(L_10);
		uint16_t L_12 = String_get_Chars_m1_442(L_10, L_11, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_ClassifyChar_m6_1730(__this, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) == ((int32_t)3)))
		{
			goto IL_0085;
		}
	}
	{
		goto IL_005f;
	}

IL_005a:
	{
		int32_t L_15 = ___p;
		___p = ((int32_t)((int32_t)L_15-(int32_t)1));
	}

IL_005f:
	{
		int32_t L_16 = ___p;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		GUIContent_t6_171 * L_17 = (__this->___content_2);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m6_1243(L_17, /*hidden argument*/NULL);
		int32_t L_19 = ___p;
		NullCheck(L_18);
		uint16_t L_20 = String_get_Chars_m1_442(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_ClassifyChar_m6_1730(__this, L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) == ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}

IL_0085:
	{
		int32_t L_23 = ___p;
		return L_23;
	}
}
// System.Void UnityEngine.TextEditor::MoveWordLeft()
extern "C" void TextEditor_MoveWordLeft_m6_1733 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	TextEditor_t6_254 * G_B2_0 = {0};
	TextEditor_t6_254 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TextEditor_t6_254 * G_B3_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m6_1678(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindPrevSeperator_m6_1724(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordRight()
extern "C" void TextEditor_SelectWordRight_m6_1734 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B3_0 = {0};
	TextEditor_t6_254 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	TextEditor_t6_254 * G_B4_1 = {0};
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m6_1725(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m6_1678(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m6_1725(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordLeft()
extern "C" void TextEditor_SelectWordLeft_m6_1735 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t6_254 * G_B3_0 = {0};
	TextEditor_t6_254 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	TextEditor_t6_254 * G_B4_1 = {0};
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m6_1733(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m6_1678(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m6_1733(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineStart()
extern "C" void TextEditor_ExpandSelectGraphicalLineStart_m6_1736 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineStart_m6_1720(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineStart_m6_1720(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineEnd()
extern "C" void TextEditor_ExpandSelectGraphicalLineEnd_m6_1737 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineEnd_m6_1721(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineEnd_m6_1721(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m6_1680(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineStart()
extern "C" void TextEditor_SelectGraphicalLineStart_m6_1738 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineStart_m6_1720(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineEnd()
extern "C" void TextEditor_SelectGraphicalLineEnd_m6_1739 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineEnd_m6_1721(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphForward()
extern "C" void TextEditor_SelectParagraphForward_m6_1740 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		V_0 = ((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1243(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1_571(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_5)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_6 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_IndexOfEndOfLine_m6_1707(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)L_10)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_11 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_11, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphBackward()
extern "C" void TextEditor_SelectParagraphBackward_m6_1741 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		V_0 = ((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_006b;
		}
	}
	{
		GUIContent_t6_171 * L_3 = (__this->___content_2);
		NullCheck(L_3);
		String_t* L_4 = GUIContent_get_text_m6_1243(L_3, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_6 = String_LastIndexOf_m1_513(L_4, ((int32_t)10), ((int32_t)((int32_t)L_5-(int32_t)2)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) >= ((int32_t)L_9)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_10, /*hidden argument*/NULL);
	}

IL_0066:
	{
		goto IL_007b;
	}

IL_006b:
	{
		V_1 = 0;
		int32_t L_11 = V_1;
		TextEditor_set_cursorIndex_m6_1678(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		TextEditor_set_selectIndex_m6_1680(__this, L_12, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentWord()
extern "C" void TextEditor_SelectCurrentWord_m6_1742 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_7-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003f:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0059:
	{
		int32_t L_11 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_13 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_FindEndOfClassification_m6_1743(__this, L_13, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_FindEndOfClassification_m6_1743(__this, L_15, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_16, /*hidden argument*/NULL);
		goto IL_00bb;
	}

IL_0095:
	{
		int32_t L_17 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_FindEndOfClassification_m6_1743(__this, L_17, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, L_18, /*hidden argument*/NULL);
		int32_t L_19 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_20 = TextEditor_FindEndOfClassification_m6_1743(__this, L_19, (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, L_20, /*hidden argument*/NULL);
	}

IL_00bb:
	{
		__this->___m_bJustSelected_18 = 1;
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfClassification(System.Int32,System.Int32)
extern "C" int32_t TextEditor_FindEndOfClassification_m6_1743 (TextEditor_t6_254 * __this, int32_t ___p, int32_t ___dir, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = ___p;
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_5 = ___p;
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0021;
		}
	}

IL_001f:
	{
		int32_t L_6 = ___p;
		return L_6;
	}

IL_0021:
	{
		GUIContent_t6_171 * L_7 = (__this->___content_2);
		NullCheck(L_7);
		String_t* L_8 = GUIContent_get_text_m6_1243(L_7, /*hidden argument*/NULL);
		int32_t L_9 = ___p;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1_442(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_ClassifyChar_m6_1730(__this, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
	}

IL_0039:
	{
		int32_t L_12 = ___p;
		int32_t L_13 = ___dir;
		___p = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		int32_t L_14 = ___p;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_0047;
		}
	}
	{
		return 0;
	}

IL_0047:
	{
		int32_t L_15 = ___p;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_17 = V_0;
		return L_17;
	}

IL_0050:
	{
		GUIContent_t6_171 * L_18 = (__this->___content_2);
		NullCheck(L_18);
		String_t* L_19 = GUIContent_get_text_m6_1243(L_18, /*hidden argument*/NULL);
		int32_t L_20 = ___p;
		NullCheck(L_19);
		uint16_t L_21 = String_get_Chars_m1_442(L_19, L_20, /*hidden argument*/NULL);
		int32_t L_22 = TextEditor_ClassifyChar_m6_1730(__this, L_21, /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) == ((int32_t)L_23)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_24 = ___dir;
		if ((!(((uint32_t)L_24) == ((uint32_t)1))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_25 = ___p;
		return L_25;
	}

IL_0076:
	{
		int32_t L_26 = ___p;
		return ((int32_t)((int32_t)L_26+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentParagraph()
extern "C" void TextEditor_SelectCurrentParagraph_m6_1744 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m6_1681(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1_571(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_5 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_IndexOfEndOfLine_m6_1707(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m6_1678(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0037:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0064;
		}
	}
	{
		GUIContent_t6_171 * L_8 = (__this->___content_2);
		NullCheck(L_8);
		String_t* L_9 = GUIContent_get_text_m6_1243(L_8, /*hidden argument*/NULL);
		int32_t L_10 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_11 = String_LastIndexOf_m1_513(L_9, ((int32_t)10), ((int32_t)((int32_t)L_10-(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m6_1680(__this, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded()
extern "C" void TextEditor_UpdateScrollOffsetIfNeeded_m6_1745 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0026;
		}
	}
	{
		Event_t6_162 * L_2 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1164(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)8)))
		{
			goto IL_0026;
		}
	}
	{
		TextEditor_UpdateScrollOffset_m6_1746(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C" void TextEditor_UpdateScrollOffset_m6_1746 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Rect_t6_51  V_1 = {0};
	Vector2_t6_47  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	Rect_t6_51  V_4 = {0};
	Vector2_t6_47  V_5 = {0};
	Rect_t6_51  V_6 = {0};
	Rect_t6_51  V_7 = {0};
	Vector2_t6_47 * G_B19_0 = {0};
	Vector2_t6_47 * G_B18_0 = {0};
	float G_B20_0 = 0.0f;
	Vector2_t6_47 * G_B20_1 = {0};
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GUIStyle_t6_176 * L_1 = (__this->___style_3);
		Rect_t6_51  L_2 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3 = Rect_get_width_m6_306((&V_3), /*hidden argument*/NULL);
		Rect_t6_51  L_4 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_height_m6_308((&V_4), /*hidden argument*/NULL);
		Rect_t6_51  L_6 = {0};
		Rect__ctor_m6_295(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_7 = (__this->___content_2);
		int32_t L_8 = V_0;
		NullCheck(L_1);
		Vector2_t6_47  L_9 = GUIStyle_GetCursorPixelPosition_m6_1445(L_1, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_9;
		GUIStyle_t6_176 * L_10 = (__this->___style_3);
		NullCheck(L_10);
		RectOffset_t6_178 * L_11 = GUIStyle_get_padding_m6_1434(L_10, /*hidden argument*/NULL);
		Rect_t6_51  L_12 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t6_51  L_13 = RectOffset_Remove_m6_1426(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		GUIStyle_t6_176 * L_14 = (__this->___style_3);
		GUIContent_t6_171 * L_15 = (__this->___content_2);
		NullCheck(L_14);
		Vector2_t6_47  L_16 = GUIStyle_CalcSize_m6_1447(L_14, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = ((&V_5)->___x_1);
		GUIStyle_t6_176 * L_18 = (__this->___style_3);
		GUIContent_t6_171 * L_19 = (__this->___content_2);
		Rect_t6_51  L_20 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = Rect_get_width_m6_306((&V_6), /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_22 = GUIStyle_CalcHeight_m6_1448(L_18, L_19, L_21, /*hidden argument*/NULL);
		Vector2__ctor_m6_215((&V_2), L_17, L_22, /*hidden argument*/NULL);
		float L_23 = ((&V_2)->___x_1);
		Rect_t6_51  L_24 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_7 = L_24;
		float L_25 = Rect_get_width_m6_306((&V_7), /*hidden argument*/NULL);
		if ((!(((float)L_23) < ((float)L_25))))
		{
			goto IL_00d3;
		}
	}
	{
		Vector2_t6_47 * L_26 = &(__this->___scrollOffset_8);
		L_26->___x_1 = (0.0f);
		goto IL_017a;
	}

IL_00d3:
	{
		bool L_27 = (__this->___m_RevealCursor_12);
		if (!L_27)
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t6_47 * L_28 = &(__this->___graphicalCursorPos_13);
		float L_29 = (L_28->___x_1);
		Vector2_t6_47 * L_30 = &(__this->___scrollOffset_8);
		float L_31 = (L_30->___x_1);
		float L_32 = Rect_get_width_m6_306((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_29+(float)(1.0f)))) > ((float)((float)((float)L_31+(float)L_32))))))
		{
			goto IL_0125;
		}
	}
	{
		Vector2_t6_47 * L_33 = &(__this->___scrollOffset_8);
		Vector2_t6_47 * L_34 = &(__this->___graphicalCursorPos_13);
		float L_35 = (L_34->___x_1);
		float L_36 = Rect_get_width_m6_306((&V_1), /*hidden argument*/NULL);
		L_33->___x_1 = ((float)((float)L_35-(float)L_36));
	}

IL_0125:
	{
		Vector2_t6_47 * L_37 = &(__this->___graphicalCursorPos_13);
		float L_38 = (L_37->___x_1);
		Vector2_t6_47 * L_39 = &(__this->___scrollOffset_8);
		float L_40 = (L_39->___x_1);
		GUIStyle_t6_176 * L_41 = (__this->___style_3);
		NullCheck(L_41);
		RectOffset_t6_178 * L_42 = GUIStyle_get_padding_m6_1434(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_left_m6_1416(L_42, /*hidden argument*/NULL);
		if ((!(((float)L_38) < ((float)((float)((float)L_40+(float)(((float)((float)L_43)))))))))
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t6_47 * L_44 = &(__this->___scrollOffset_8);
		Vector2_t6_47 * L_45 = &(__this->___graphicalCursorPos_13);
		float L_46 = (L_45->___x_1);
		GUIStyle_t6_176 * L_47 = (__this->___style_3);
		NullCheck(L_47);
		RectOffset_t6_178 * L_48 = GUIStyle_get_padding_m6_1434(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_left_m6_1416(L_48, /*hidden argument*/NULL);
		L_44->___x_1 = ((float)((float)L_46-(float)(((float)((float)L_49)))));
	}

IL_017a:
	{
		float L_50 = ((&V_2)->___y_2);
		float L_51 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_50) < ((float)L_51))))
		{
			goto IL_01a2;
		}
	}
	{
		Vector2_t6_47 * L_52 = &(__this->___scrollOffset_8);
		L_52->___y_2 = (0.0f);
		goto IL_027f;
	}

IL_01a2:
	{
		bool L_53 = (__this->___m_RevealCursor_12);
		if (!L_53)
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t6_47 * L_54 = &(__this->___graphicalCursorPos_13);
		float L_55 = (L_54->___y_2);
		GUIStyle_t6_176 * L_56 = (__this->___style_3);
		NullCheck(L_56);
		float L_57 = GUIStyle_get_lineHeight_m6_1436(L_56, /*hidden argument*/NULL);
		Vector2_t6_47 * L_58 = &(__this->___scrollOffset_8);
		float L_59 = (L_58->___y_2);
		float L_60 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_61 = (__this->___style_3);
		NullCheck(L_61);
		RectOffset_t6_178 * L_62 = GUIStyle_get_padding_m6_1434(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_top_m6_1420(L_62, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_55+(float)L_57))) > ((float)((float)((float)((float)((float)L_59+(float)L_60))+(float)(((float)((float)L_63)))))))))
		{
			goto IL_022a;
		}
	}
	{
		Vector2_t6_47 * L_64 = &(__this->___scrollOffset_8);
		Vector2_t6_47 * L_65 = &(__this->___graphicalCursorPos_13);
		float L_66 = (L_65->___y_2);
		float L_67 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_68 = (__this->___style_3);
		NullCheck(L_68);
		RectOffset_t6_178 * L_69 = GUIStyle_get_padding_m6_1434(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		int32_t L_70 = RectOffset_get_top_m6_1420(L_69, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_71 = (__this->___style_3);
		NullCheck(L_71);
		float L_72 = GUIStyle_get_lineHeight_m6_1436(L_71, /*hidden argument*/NULL);
		L_64->___y_2 = ((float)((float)((float)((float)((float)((float)L_66-(float)L_67))-(float)(((float)((float)L_70)))))+(float)L_72));
	}

IL_022a:
	{
		Vector2_t6_47 * L_73 = &(__this->___graphicalCursorPos_13);
		float L_74 = (L_73->___y_2);
		Vector2_t6_47 * L_75 = &(__this->___scrollOffset_8);
		float L_76 = (L_75->___y_2);
		GUIStyle_t6_176 * L_77 = (__this->___style_3);
		NullCheck(L_77);
		RectOffset_t6_178 * L_78 = GUIStyle_get_padding_m6_1434(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = RectOffset_get_top_m6_1420(L_78, /*hidden argument*/NULL);
		if ((!(((float)L_74) < ((float)((float)((float)L_76+(float)(((float)((float)L_79)))))))))
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t6_47 * L_80 = &(__this->___scrollOffset_8);
		Vector2_t6_47 * L_81 = &(__this->___graphicalCursorPos_13);
		float L_82 = (L_81->___y_2);
		GUIStyle_t6_176 * L_83 = (__this->___style_3);
		NullCheck(L_83);
		RectOffset_t6_178 * L_84 = GUIStyle_get_padding_m6_1434(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_top_m6_1420(L_84, /*hidden argument*/NULL);
		L_80->___y_2 = ((float)((float)L_82-(float)(((float)((float)L_85)))));
	}

IL_027f:
	{
		Vector2_t6_47 * L_86 = &(__this->___scrollOffset_8);
		float L_87 = (L_86->___y_2);
		if ((!(((float)L_87) > ((float)(0.0f)))))
		{
			goto IL_02f1;
		}
	}
	{
		float L_88 = ((&V_2)->___y_2);
		Vector2_t6_47 * L_89 = &(__this->___scrollOffset_8);
		float L_90 = (L_89->___y_2);
		float L_91 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_88-(float)L_90))) < ((float)L_91))))
		{
			goto IL_02f1;
		}
	}
	{
		Vector2_t6_47 * L_92 = &(__this->___scrollOffset_8);
		float L_93 = ((&V_2)->___y_2);
		float L_94 = Rect_get_height_m6_308((&V_1), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_95 = (__this->___style_3);
		NullCheck(L_95);
		RectOffset_t6_178 * L_96 = GUIStyle_get_padding_m6_1434(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_top_m6_1420(L_96, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_98 = (__this->___style_3);
		NullCheck(L_98);
		RectOffset_t6_178 * L_99 = GUIStyle_get_padding_m6_1434(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		int32_t L_100 = RectOffset_get_bottom_m6_1422(L_99, /*hidden argument*/NULL);
		L_92->___y_2 = ((float)((float)((float)((float)((float)((float)L_93-(float)L_94))-(float)(((float)((float)L_97)))))-(float)(((float)((float)L_100)))));
	}

IL_02f1:
	{
		Vector2_t6_47 * L_101 = &(__this->___scrollOffset_8);
		Vector2_t6_47 * L_102 = &(__this->___scrollOffset_8);
		float L_103 = (L_102->___y_2);
		G_B18_0 = L_101;
		if ((!(((float)L_103) < ((float)(0.0f)))))
		{
			G_B19_0 = L_101;
			goto IL_0316;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		goto IL_0321;
	}

IL_0316:
	{
		Vector2_t6_47 * L_104 = &(__this->___scrollOffset_8);
		float L_105 = (L_104->___y_2);
		G_B20_0 = L_105;
		G_B20_1 = G_B19_0;
	}

IL_0321:
	{
		G_B20_1->___y_2 = G_B20_0;
		__this->___m_RevealCursor_12 = 0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::DrawCursor(System.String)
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TextEditor_DrawCursor_m6_1747 (TextEditor_t6_254 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	Vector2_t6_47  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	Rect_t6_51  V_4 = {0};
	Rect_t6_51  V_5 = {0};
	Rect_t6_51  V_6 = {0};
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		String_t* L_3 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1_571(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0063;
		}
	}
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		String_t* L_6 = ___text;
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_8 = String_Substring_m1_455(L_6, 0, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		String_t* L_9 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = ___text;
		int32_t L_11 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_12 = String_Substring_m1_454(L_10, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_560(NULL /*static, unused*/, L_8, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1244(L_5, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		String_t* L_15 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m1_571(L_15, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)L_16));
		goto IL_006f;
	}

IL_0063:
	{
		GUIContent_t6_171 * L_17 = (__this->___content_2);
		String_t* L_18 = ___text;
		NullCheck(L_17);
		GUIContent_set_text_m6_1244(L_17, L_18, /*hidden argument*/NULL);
	}

IL_006f:
	{
		GUIStyle_t6_176 * L_19 = (__this->___style_3);
		Rect_t6_51  L_20 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = Rect_get_width_m6_306((&V_3), /*hidden argument*/NULL);
		Rect_t6_51  L_22 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_4 = L_22;
		float L_23 = Rect_get_height_m6_308((&V_4), /*hidden argument*/NULL);
		Rect_t6_51  L_24 = {0};
		Rect__ctor_m6_295(&L_24, (0.0f), (0.0f), L_21, L_23, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_25 = (__this->___content_2);
		int32_t L_26 = V_1;
		NullCheck(L_19);
		Vector2_t6_47  L_27 = GUIStyle_GetCursorPixelPosition_m6_1445(L_19, L_24, L_25, L_26, /*hidden argument*/NULL);
		__this->___graphicalCursorPos_13 = L_27;
		GUIStyle_t6_176 * L_28 = (__this->___style_3);
		NullCheck(L_28);
		Vector2_t6_47  L_29 = GUIStyle_get_contentOffset_m6_1462(L_28, /*hidden argument*/NULL);
		V_2 = L_29;
		GUIStyle_t6_176 * L_30 = (__this->___style_3);
		GUIStyle_t6_176 * L_31 = L_30;
		NullCheck(L_31);
		Vector2_t6_47  L_32 = GUIStyle_get_contentOffset_m6_1462(L_31, /*hidden argument*/NULL);
		Vector2_t6_47  L_33 = (__this->___scrollOffset_8);
		Vector2_t6_47  L_34 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		GUIStyle_set_contentOffset_m6_1463(L_31, L_34, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_35 = (__this->___style_3);
		Vector2_t6_47  L_36 = (__this->___scrollOffset_8);
		NullCheck(L_35);
		GUIStyle_set_Internal_clipOffset_m6_1466(L_35, L_36, /*hidden argument*/NULL);
		Vector2_t6_47  L_37 = (__this->___graphicalCursorPos_13);
		Rect_t6_51  L_38 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_5 = L_38;
		float L_39 = Rect_get_x_m6_298((&V_5), /*hidden argument*/NULL);
		Rect_t6_51  L_40 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		V_6 = L_40;
		float L_41 = Rect_get_y_m6_300((&V_6), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_42 = (__this->___style_3);
		NullCheck(L_42);
		float L_43 = GUIStyle_get_lineHeight_m6_1436(L_42, /*hidden argument*/NULL);
		Vector2_t6_47  L_44 = {0};
		Vector2__ctor_m6_215(&L_44, L_39, ((float)((float)L_41+(float)L_43)), /*hidden argument*/NULL);
		Vector2_t6_47  L_45 = Vector2_op_Addition_m6_229(NULL /*static, unused*/, L_37, L_44, /*hidden argument*/NULL);
		Vector2_t6_47  L_46 = (__this->___scrollOffset_8);
		Vector2_t6_47  L_47 = Vector2_op_Subtraction_m6_230(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		Input_set_compositionCursorPos_m6_699(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		String_t* L_48 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = String_get_Length_m1_571(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_0180;
		}
	}
	{
		GUIStyle_t6_176 * L_50 = (__this->___style_3);
		Rect_t6_51  L_51 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_52 = (__this->___content_2);
		int32_t L_53 = (__this->___controlID_1);
		int32_t L_54 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_55 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		String_t* L_56 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = String_get_Length_m1_571(L_56, /*hidden argument*/NULL);
		NullCheck(L_50);
		GUIStyle_DrawWithTextSelection_m6_1442(L_50, L_51, L_52, L_53, L_54, ((int32_t)((int32_t)L_55+(int32_t)L_57)), 1, /*hidden argument*/NULL);
		goto IL_01a9;
	}

IL_0180:
	{
		GUIStyle_t6_176 * L_58 = (__this->___style_3);
		Rect_t6_51  L_59 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_60 = (__this->___content_2);
		int32_t L_61 = (__this->___controlID_1);
		int32_t L_62 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_63 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		GUIStyle_DrawWithTextSelection_m6_1443(L_58, L_59, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
	}

IL_01a9:
	{
		int32_t L_64 = (__this->___m_iAltCursorPos_19);
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01d8;
		}
	}
	{
		GUIStyle_t6_176 * L_65 = (__this->___style_3);
		Rect_t6_51  L_66 = TextEditor_get_position_m6_1675(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_67 = (__this->___content_2);
		int32_t L_68 = (__this->___controlID_1);
		int32_t L_69 = (__this->___m_iAltCursorPos_19);
		NullCheck(L_65);
		GUIStyle_DrawCursor_m6_1441(L_65, L_66, L_67, L_68, L_69, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		GUIStyle_t6_176 * L_70 = (__this->___style_3);
		Vector2_t6_47  L_71 = V_2;
		NullCheck(L_70);
		GUIStyle_set_contentOffset_m6_1463(L_70, L_71, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_72 = (__this->___style_3);
		Vector2_t6_47  L_73 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_72);
		GUIStyle_set_Internal_clipOffset_m6_1466(L_72, L_73, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_74 = (__this->___content_2);
		String_t* L_75 = V_0;
		NullCheck(L_74);
		GUIContent_set_text_m6_1244(L_74, L_75, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::PerformOperation(UnityEngine.TextEditor/TextEditOp)
extern TypeInfo* TextEditOp_t6_253_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4907;
extern "C" bool TextEditor_PerformOperation_m6_1748 (TextEditor_t6_254 * __this, int32_t ___operation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditOp_t6_253_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1694);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4907 = il2cpp_codegen_string_literal_from_index(4907);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		__this->___m_RevealCursor_12 = 1;
		int32_t L_0 = ___operation;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_00cc;
		}
		if (L_1 == 1)
		{
			goto IL_00d7;
		}
		if (L_1 == 2)
		{
			goto IL_00e2;
		}
		if (L_1 == 3)
		{
			goto IL_00ed;
		}
		if (L_1 == 4)
		{
			goto IL_00f8;
		}
		if (L_1 == 5)
		{
			goto IL_0103;
		}
		if (L_1 == 6)
		{
			goto IL_013a;
		}
		if (L_1 == 7)
		{
			goto IL_0145;
		}
		if (L_1 == 8)
		{
			goto IL_027e;
		}
		if (L_1 == 9)
		{
			goto IL_027e;
		}
		if (L_1 == 10)
		{
			goto IL_0166;
		}
		if (L_1 == 11)
		{
			goto IL_0171;
		}
		if (L_1 == 12)
		{
			goto IL_012f;
		}
		if (L_1 == 13)
		{
			goto IL_010e;
		}
		if (L_1 == 14)
		{
			goto IL_0150;
		}
		if (L_1 == 15)
		{
			goto IL_015b;
		}
		if (L_1 == 16)
		{
			goto IL_0119;
		}
		if (L_1 == 17)
		{
			goto IL_0124;
		}
		if (L_1 == 18)
		{
			goto IL_017c;
		}
		if (L_1 == 19)
		{
			goto IL_0187;
		}
		if (L_1 == 20)
		{
			goto IL_0192;
		}
		if (L_1 == 21)
		{
			goto IL_019d;
		}
		if (L_1 == 22)
		{
			goto IL_01d4;
		}
		if (L_1 == 23)
		{
			goto IL_01df;
		}
		if (L_1 == 24)
		{
			goto IL_027e;
		}
		if (L_1 == 25)
		{
			goto IL_027e;
		}
		if (L_1 == 26)
		{
			goto IL_01ea;
		}
		if (L_1 == 27)
		{
			goto IL_01f5;
		}
		if (L_1 == 28)
		{
			goto IL_0216;
		}
		if (L_1 == 29)
		{
			goto IL_0221;
		}
		if (L_1 == 30)
		{
			goto IL_01b3;
		}
		if (L_1 == 31)
		{
			goto IL_01a8;
		}
		if (L_1 == 32)
		{
			goto IL_01be;
		}
		if (L_1 == 33)
		{
			goto IL_01c9;
		}
		if (L_1 == 34)
		{
			goto IL_020b;
		}
		if (L_1 == 35)
		{
			goto IL_0200;
		}
		if (L_1 == 36)
		{
			goto IL_022c;
		}
		if (L_1 == 37)
		{
			goto IL_0233;
		}
		if (L_1 == 38)
		{
			goto IL_0269;
		}
		if (L_1 == 39)
		{
			goto IL_0277;
		}
		if (L_1 == 40)
		{
			goto IL_0270;
		}
		if (L_1 == 41)
		{
			goto IL_023a;
		}
		if (L_1 == 42)
		{
			goto IL_0241;
		}
		if (L_1 == 43)
		{
			goto IL_024c;
		}
		if (L_1 == 44)
		{
			goto IL_0253;
		}
		if (L_1 == 45)
		{
			goto IL_025e;
		}
	}
	{
		goto IL_027e;
	}

IL_00cc:
	{
		TextEditor_MoveLeft_m6_1698(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00d7:
	{
		TextEditor_MoveRight_m6_1697(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00e2:
	{
		TextEditor_MoveUp_m6_1699(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00ed:
	{
		TextEditor_MoveDown_m6_1700(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00f8:
	{
		TextEditor_MoveLineStart_m6_1701(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0103:
	{
		TextEditor_MoveLineEnd_m6_1702(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_010e:
	{
		TextEditor_MoveWordRight_m6_1725(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0119:
	{
		TextEditor_MoveToStartOfNextWord_m6_1726(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0124:
	{
		TextEditor_MoveToEndOfPreviousWord_m6_1727(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_012f:
	{
		TextEditor_MoveWordLeft_m6_1733(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_013a:
	{
		TextEditor_MoveTextStart_m6_1705(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0145:
	{
		TextEditor_MoveTextEnd_m6_1706(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0150:
	{
		TextEditor_MoveParagraphForward_m6_1708(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_015b:
	{
		TextEditor_MoveParagraphBackward_m6_1709(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0166:
	{
		TextEditor_MoveGraphicalLineStart_m6_1703(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0171:
	{
		TextEditor_MoveGraphicalLineEnd_m6_1704(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_017c:
	{
		TextEditor_SelectLeft_m6_1712(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0187:
	{
		TextEditor_SelectRight_m6_1713(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0192:
	{
		TextEditor_SelectUp_m6_1714(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_019d:
	{
		TextEditor_SelectDown_m6_1715(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01a8:
	{
		TextEditor_SelectWordRight_m6_1734(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01b3:
	{
		TextEditor_SelectWordLeft_m6_1735(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01be:
	{
		TextEditor_SelectToEndOfPreviousWord_m6_1729(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01c9:
	{
		TextEditor_SelectToStartOfNextWord_m6_1728(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01d4:
	{
		TextEditor_SelectTextStart_m6_1717(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01df:
	{
		TextEditor_SelectTextEnd_m6_1716(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01ea:
	{
		TextEditor_ExpandSelectGraphicalLineStart_m6_1736(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01f5:
	{
		TextEditor_ExpandSelectGraphicalLineEnd_m6_1737(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0200:
	{
		TextEditor_SelectParagraphForward_m6_1740(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_020b:
	{
		TextEditor_SelectParagraphBackward_m6_1741(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0216:
	{
		TextEditor_SelectGraphicalLineStart_m6_1738(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0221:
	{
		TextEditor_SelectGraphicalLineEnd_m6_1739(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_022c:
	{
		bool L_2 = TextEditor_Delete_m6_1689(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0233:
	{
		bool L_3 = TextEditor_Backspace_m6_1690(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_023a:
	{
		bool L_4 = TextEditor_Cut_m6_1750(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0241:
	{
		TextEditor_Copy_m6_1751(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_024c:
	{
		bool L_5 = TextEditor_Paste_m6_1753(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0253:
	{
		TextEditor_SelectAll_m6_1691(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_025e:
	{
		TextEditor_SelectNone_m6_1692(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0269:
	{
		bool L_6 = TextEditor_DeleteWordBack_m6_1687(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0270:
	{
		bool L_7 = TextEditor_DeleteLineBack_m6_1686(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0277:
	{
		bool L_8 = TextEditor_DeleteWordForward_m6_1688(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_027e:
	{
		int32_t L_9 = ___operation;
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(TextEditOp_t6_253_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral4907, L_11, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0298:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::SaveBackup()
extern "C" void TextEditor_SaveBackup_m6_1749 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		GUIContent_t6_171 * L_0 = (__this->___content_2);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m6_1243(L_0, /*hidden argument*/NULL);
		__this->___oldText_20 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		__this->___oldPos_21 = L_2;
		int32_t L_3 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		__this->___oldSelectPos_22 = L_3;
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::Cut()
extern "C" bool TextEditor_Cut_m6_1750 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___isPasswordField_6);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TextEditor_Copy_m6_1751(__this, /*hidden argument*/NULL);
		bool L_1 = TextEditor_DeleteSelection_m6_1694(__this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void TextEditor_Copy_m6_1751 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		int32_t L_0 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = (__this->___isPasswordField_6);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0058;
		}
	}
	{
		GUIContent_t6_171 * L_5 = (__this->___content_2);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m6_1243(L_5, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_10 = String_Substring_m1_455(L_6, L_7, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_007c;
	}

IL_0058:
	{
		GUIContent_t6_171 * L_11 = (__this->___content_2);
		NullCheck(L_11);
		String_t* L_12 = GUIContent_get_text_m6_1243(L_11, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_m6_1677(__this, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_selectIndex_m6_1679(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_16 = String_Substring_m1_455(L_12, L_13, ((int32_t)((int32_t)L_14-(int32_t)L_15)), /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_007c:
	{
		String_t* L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_systemCopyBuffer_m6_1515(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern Il2CppCodeGenString* _stringLiteral4335;
extern Il2CppCodeGenString* _stringLiteral268;
extern "C" String_t* TextEditor_ReplaceNewlinesWithSpaces_m6_1752 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4335 = il2cpp_codegen_string_literal_from_index(4335);
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m1_536(L_0, _stringLiteral4335, _stringLiteral268, /*hidden argument*/NULL);
		___value = L_1;
		String_t* L_2 = ___value;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m1_535(L_2, ((int32_t)10), ((int32_t)32), /*hidden argument*/NULL);
		___value = L_3;
		String_t* L_4 = ___value;
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m1_535(L_4, ((int32_t)13), ((int32_t)32), /*hidden argument*/NULL);
		___value = L_5;
		String_t* L_6 = ___value;
		return L_6;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextEditor_Paste_m6_1753 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m6_1514(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_3 = String_op_Inequality_m1_602(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		bool L_4 = (__this->___multiline_4);
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_5 = V_0;
		String_t* L_6 = TextEditor_ReplaceNewlinesWithSpaces_m6_1752(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0028:
	{
		String_t* L_7 = V_0;
		TextEditor_ReplaceSelection_m6_1695(__this, L_7, /*hidden argument*/NULL);
		return 1;
	}

IL_0031:
	{
		return 0;
	}
}
// System.Void UnityEngine.TextEditor::MapKey(System.String,UnityEngine.TextEditor/TextEditOp)
extern TypeInfo* TextEditor_t6_254_il2cpp_TypeInfo_var;
extern "C" void TextEditor_MapKey_m6_1754 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1647);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1831 * L_0 = ((TextEditor_t6_254_StaticFields*)TextEditor_t6_254_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		String_t* L_1 = ___key;
		Event_t6_162 * L_2 = Event_KeyboardEvent_m6_1155(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___action;
		NullCheck(L_0);
		VirtActionInvoker2< Event_t6_162 *, int32_t >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(!0,!1) */, L_0, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.TextEditor::InitKeyActions()
extern TypeInfo* TextEditor_t6_254_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1831_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14967_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4762;
extern Il2CppCodeGenString* _stringLiteral4763;
extern Il2CppCodeGenString* _stringLiteral4760;
extern Il2CppCodeGenString* _stringLiteral4761;
extern Il2CppCodeGenString* _stringLiteral4908;
extern Il2CppCodeGenString* _stringLiteral4909;
extern Il2CppCodeGenString* _stringLiteral4910;
extern Il2CppCodeGenString* _stringLiteral4911;
extern Il2CppCodeGenString* _stringLiteral4772;
extern Il2CppCodeGenString* _stringLiteral4771;
extern Il2CppCodeGenString* _stringLiteral4912;
extern Il2CppCodeGenString* _stringLiteral4913;
extern Il2CppCodeGenString* _stringLiteral4914;
extern Il2CppCodeGenString* _stringLiteral4915;
extern Il2CppCodeGenString* _stringLiteral4916;
extern Il2CppCodeGenString* _stringLiteral4917;
extern Il2CppCodeGenString* _stringLiteral4918;
extern Il2CppCodeGenString* _stringLiteral4919;
extern Il2CppCodeGenString* _stringLiteral4920;
extern Il2CppCodeGenString* _stringLiteral4921;
extern Il2CppCodeGenString* _stringLiteral4922;
extern Il2CppCodeGenString* _stringLiteral4923;
extern Il2CppCodeGenString* _stringLiteral4924;
extern Il2CppCodeGenString* _stringLiteral4925;
extern Il2CppCodeGenString* _stringLiteral4926;
extern Il2CppCodeGenString* _stringLiteral4927;
extern Il2CppCodeGenString* _stringLiteral4928;
extern Il2CppCodeGenString* _stringLiteral4929;
extern Il2CppCodeGenString* _stringLiteral4930;
extern Il2CppCodeGenString* _stringLiteral4931;
extern Il2CppCodeGenString* _stringLiteral4932;
extern Il2CppCodeGenString* _stringLiteral4933;
extern Il2CppCodeGenString* _stringLiteral4934;
extern Il2CppCodeGenString* _stringLiteral4935;
extern Il2CppCodeGenString* _stringLiteral4936;
extern Il2CppCodeGenString* _stringLiteral4937;
extern Il2CppCodeGenString* _stringLiteral4938;
extern Il2CppCodeGenString* _stringLiteral4939;
extern Il2CppCodeGenString* _stringLiteral4940;
extern Il2CppCodeGenString* _stringLiteral4941;
extern Il2CppCodeGenString* _stringLiteral4942;
extern Il2CppCodeGenString* _stringLiteral4943;
extern Il2CppCodeGenString* _stringLiteral4944;
extern Il2CppCodeGenString* _stringLiteral4945;
extern Il2CppCodeGenString* _stringLiteral4946;
extern Il2CppCodeGenString* _stringLiteral4947;
extern Il2CppCodeGenString* _stringLiteral4948;
extern Il2CppCodeGenString* _stringLiteral4949;
extern Il2CppCodeGenString* _stringLiteral4950;
extern Il2CppCodeGenString* _stringLiteral4765;
extern Il2CppCodeGenString* _stringLiteral4766;
extern Il2CppCodeGenString* _stringLiteral4951;
extern Il2CppCodeGenString* _stringLiteral4952;
extern Il2CppCodeGenString* _stringLiteral4953;
extern Il2CppCodeGenString* _stringLiteral4954;
extern Il2CppCodeGenString* _stringLiteral4955;
extern Il2CppCodeGenString* _stringLiteral4956;
extern Il2CppCodeGenString* _stringLiteral4957;
extern Il2CppCodeGenString* _stringLiteral4958;
extern Il2CppCodeGenString* _stringLiteral4959;
extern Il2CppCodeGenString* _stringLiteral4960;
extern "C" void TextEditor_InitKeyActions_m6_1755 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1647);
		Dictionary_2_t1_1831_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1695);
		Dictionary_2__ctor_m1_14967_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483812);
		_stringLiteral4762 = il2cpp_codegen_string_literal_from_index(4762);
		_stringLiteral4763 = il2cpp_codegen_string_literal_from_index(4763);
		_stringLiteral4760 = il2cpp_codegen_string_literal_from_index(4760);
		_stringLiteral4761 = il2cpp_codegen_string_literal_from_index(4761);
		_stringLiteral4908 = il2cpp_codegen_string_literal_from_index(4908);
		_stringLiteral4909 = il2cpp_codegen_string_literal_from_index(4909);
		_stringLiteral4910 = il2cpp_codegen_string_literal_from_index(4910);
		_stringLiteral4911 = il2cpp_codegen_string_literal_from_index(4911);
		_stringLiteral4772 = il2cpp_codegen_string_literal_from_index(4772);
		_stringLiteral4771 = il2cpp_codegen_string_literal_from_index(4771);
		_stringLiteral4912 = il2cpp_codegen_string_literal_from_index(4912);
		_stringLiteral4913 = il2cpp_codegen_string_literal_from_index(4913);
		_stringLiteral4914 = il2cpp_codegen_string_literal_from_index(4914);
		_stringLiteral4915 = il2cpp_codegen_string_literal_from_index(4915);
		_stringLiteral4916 = il2cpp_codegen_string_literal_from_index(4916);
		_stringLiteral4917 = il2cpp_codegen_string_literal_from_index(4917);
		_stringLiteral4918 = il2cpp_codegen_string_literal_from_index(4918);
		_stringLiteral4919 = il2cpp_codegen_string_literal_from_index(4919);
		_stringLiteral4920 = il2cpp_codegen_string_literal_from_index(4920);
		_stringLiteral4921 = il2cpp_codegen_string_literal_from_index(4921);
		_stringLiteral4922 = il2cpp_codegen_string_literal_from_index(4922);
		_stringLiteral4923 = il2cpp_codegen_string_literal_from_index(4923);
		_stringLiteral4924 = il2cpp_codegen_string_literal_from_index(4924);
		_stringLiteral4925 = il2cpp_codegen_string_literal_from_index(4925);
		_stringLiteral4926 = il2cpp_codegen_string_literal_from_index(4926);
		_stringLiteral4927 = il2cpp_codegen_string_literal_from_index(4927);
		_stringLiteral4928 = il2cpp_codegen_string_literal_from_index(4928);
		_stringLiteral4929 = il2cpp_codegen_string_literal_from_index(4929);
		_stringLiteral4930 = il2cpp_codegen_string_literal_from_index(4930);
		_stringLiteral4931 = il2cpp_codegen_string_literal_from_index(4931);
		_stringLiteral4932 = il2cpp_codegen_string_literal_from_index(4932);
		_stringLiteral4933 = il2cpp_codegen_string_literal_from_index(4933);
		_stringLiteral4934 = il2cpp_codegen_string_literal_from_index(4934);
		_stringLiteral4935 = il2cpp_codegen_string_literal_from_index(4935);
		_stringLiteral4936 = il2cpp_codegen_string_literal_from_index(4936);
		_stringLiteral4937 = il2cpp_codegen_string_literal_from_index(4937);
		_stringLiteral4938 = il2cpp_codegen_string_literal_from_index(4938);
		_stringLiteral4939 = il2cpp_codegen_string_literal_from_index(4939);
		_stringLiteral4940 = il2cpp_codegen_string_literal_from_index(4940);
		_stringLiteral4941 = il2cpp_codegen_string_literal_from_index(4941);
		_stringLiteral4942 = il2cpp_codegen_string_literal_from_index(4942);
		_stringLiteral4943 = il2cpp_codegen_string_literal_from_index(4943);
		_stringLiteral4944 = il2cpp_codegen_string_literal_from_index(4944);
		_stringLiteral4945 = il2cpp_codegen_string_literal_from_index(4945);
		_stringLiteral4946 = il2cpp_codegen_string_literal_from_index(4946);
		_stringLiteral4947 = il2cpp_codegen_string_literal_from_index(4947);
		_stringLiteral4948 = il2cpp_codegen_string_literal_from_index(4948);
		_stringLiteral4949 = il2cpp_codegen_string_literal_from_index(4949);
		_stringLiteral4950 = il2cpp_codegen_string_literal_from_index(4950);
		_stringLiteral4765 = il2cpp_codegen_string_literal_from_index(4765);
		_stringLiteral4766 = il2cpp_codegen_string_literal_from_index(4766);
		_stringLiteral4951 = il2cpp_codegen_string_literal_from_index(4951);
		_stringLiteral4952 = il2cpp_codegen_string_literal_from_index(4952);
		_stringLiteral4953 = il2cpp_codegen_string_literal_from_index(4953);
		_stringLiteral4954 = il2cpp_codegen_string_literal_from_index(4954);
		_stringLiteral4955 = il2cpp_codegen_string_literal_from_index(4955);
		_stringLiteral4956 = il2cpp_codegen_string_literal_from_index(4956);
		_stringLiteral4957 = il2cpp_codegen_string_literal_from_index(4957);
		_stringLiteral4958 = il2cpp_codegen_string_literal_from_index(4958);
		_stringLiteral4959 = il2cpp_codegen_string_literal_from_index(4959);
		_stringLiteral4960 = il2cpp_codegen_string_literal_from_index(4960);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1831 * L_0 = ((TextEditor_t6_254_StaticFields*)TextEditor_t6_254_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Dictionary_2_t1_1831 * L_1 = (Dictionary_2_t1_1831 *)il2cpp_codegen_object_new (Dictionary_2_t1_1831_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14967(L_1, /*hidden argument*/Dictionary_2__ctor_m1_14967_MethodInfo_var);
		((TextEditor_t6_254_StaticFields*)TextEditor_t6_254_il2cpp_TypeInfo_var->static_fields)->___s_Keyactions_23 = L_1;
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4762, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4763, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4760, 2, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4761, 3, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4908, ((int32_t)18), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4909, ((int32_t)19), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4910, ((int32_t)20), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4911, ((int32_t)21), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4772, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4771, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4912, ((int32_t)37), /*hidden argument*/NULL);
		int32_t L_2 = Application_get_platform_m6_587(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m6_587(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)3)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m6_587(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)4)))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m6_587(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_6 = Application_get_platform_m6_587(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_029b;
		}
	}
	{
		String_t* L_7 = SystemInfo_get_operatingSystem_m6_9(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1_531(L_7, _stringLiteral4913, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_029b;
		}
	}

IL_00e0:
	{
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4914, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4915, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4916, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4917, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4918, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4919, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4920, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4921, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4922, 6, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4923, 7, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4924, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4925, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4926, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4927, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4928, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4929, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4930, ((int32_t)30), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4931, ((int32_t)31), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4932, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4933, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4934, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4935, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4936, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4937, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4938, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4939, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4940, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4941, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4942, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4943, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4944, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4945, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4946, 4, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4947, 5, /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4948, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4949, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4950, ((int32_t)40), /*hidden argument*/NULL);
		goto IL_03d3;
	}

IL_029b:
	{
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4765, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4766, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4920, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4921, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4922, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4923, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4914, ((int32_t)17), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4915, ((int32_t)16), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4951, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4952, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4926, ((int32_t)32), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4927, ((int32_t)33), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4928, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4929, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4924, ((int32_t)28), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4925, ((int32_t)29), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4953, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4954, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4950, ((int32_t)40), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4946, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4955, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4956, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4957, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4958, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4959, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m6_1754(NULL /*static, unused*/, _stringLiteral4960, ((int32_t)43), /*hidden argument*/NULL);
	}

IL_03d3:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::DetectFocusChange()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void TextEditor_DetectFocusChange_m6_1756 (TextEditor_t6_254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasFocus_7);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = (__this->___controlID_1);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		TextEditor_OnLostFocus_m6_1683(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = (__this->___m_HasFocus_7);
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_4 = (__this->___controlID_1);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_5 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0042;
		}
	}
	{
		TextEditor_OnFocus_m6_1682(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_CompareColors_m6_1757 (TextGenerationSettings_t6_153 * __this, Color_t6_40  ___left, Color_t6_40  ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___left)->___r_0);
		float L_1 = ((&___right)->___r_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005d;
		}
	}
	{
		float L_3 = ((&___left)->___g_1);
		float L_4 = ((&___right)->___g_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		float L_6 = ((&___left)->___b_2);
		float L_7 = ((&___right)->___b_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005d;
		}
	}
	{
		float L_9 = ((&___left)->___a_3);
		float L_10 = ((&___right)->___a_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_11 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_005e;
	}

IL_005d:
	{
		G_B5_0 = 0;
	}

IL_005e:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_CompareVector2_m6_1758 (TextGenerationSettings_t6_153 * __this, Vector2_t6_47  ___left, Vector2_t6_47  ___right, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = ((&___left)->___x_1);
		float L_1 = ((&___right)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ((&___left)->___y_2);
		float L_4 = ((&___right)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" bool TextGenerationSettings_Equals_m6_1759 (TextGenerationSettings_t6_153 * __this, TextGenerationSettings_t6_153  ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B20_0 = 0;
	{
		Color_t6_40  L_0 = (__this->___color_1);
		Color_t6_40  L_1 = ((&___other)->___color_1);
		bool L_2 = TextGenerationSettings_CompareColors_m6_1757(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_3 = (__this->___fontSize_2);
		int32_t L_4 = ((&___other)->___fontSize_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0174;
		}
	}
	{
		float L_5 = (__this->___scaleFactor_5);
		float L_6 = ((&___other)->___scaleFactor_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_8 = (__this->___resizeTextMinSize_9);
		int32_t L_9 = ((&___other)->___resizeTextMinSize_9);
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_10 = (__this->___resizeTextMaxSize_10);
		int32_t L_11 = ((&___other)->___resizeTextMaxSize_10);
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0174;
		}
	}
	{
		float L_12 = (__this->___lineSpacing_3);
		float L_13 = ((&___other)->___lineSpacing_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		bool L_14 = Mathf_Approximately_m6_443(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_15 = (__this->___fontStyle_6);
		int32_t L_16 = ((&___other)->___fontStyle_6);
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_17 = (__this->___richText_4);
		bool L_18 = ((&___other)->___richText_4);
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_19 = (__this->___textAnchor_7);
		int32_t L_20 = ((&___other)->___textAnchor_7);
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_21 = (__this->___resizeTextForBestFit_8);
		bool L_22 = ((&___other)->___resizeTextForBestFit_8);
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_23 = (__this->___resizeTextMinSize_9);
		int32_t L_24 = ((&___other)->___resizeTextMinSize_9);
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_25 = (__this->___resizeTextMaxSize_10);
		int32_t L_26 = ((&___other)->___resizeTextMaxSize_10);
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_27 = (__this->___resizeTextForBestFit_8);
		bool L_28 = ((&___other)->___resizeTextForBestFit_8);
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_0174;
		}
	}
	{
		bool L_29 = (__this->___updateBounds_11);
		bool L_30 = ((&___other)->___updateBounds_11);
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_31 = (__this->___horizontalOverflow_13);
		int32_t L_32 = ((&___other)->___horizontalOverflow_13);
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0174;
		}
	}
	{
		int32_t L_33 = (__this->___verticalOverflow_12);
		int32_t L_34 = ((&___other)->___verticalOverflow_12);
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0174;
		}
	}
	{
		Vector2_t6_47  L_35 = (__this->___generationExtents_14);
		Vector2_t6_47  L_36 = ((&___other)->___generationExtents_14);
		bool L_37 = TextGenerationSettings_CompareVector2_m6_1758(__this, L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0174;
		}
	}
	{
		Vector2_t6_47  L_38 = (__this->___pivot_15);
		Vector2_t6_47  L_39 = ((&___other)->___pivot_15);
		bool L_40 = TextGenerationSettings_CompareVector2_m6_1758(__this, L_38, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0174;
		}
	}
	{
		Font_t6_149 * L_41 = (__this->___font_0);
		Font_t6_149 * L_42 = ((&___other)->___font_0);
		bool L_43 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		G_B20_0 = ((int32_t)(L_43));
		goto IL_0175;
	}

IL_0174:
	{
		G_B20_0 = 0;
	}

IL_0175:
	{
		return G_B20_0;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern TypeInfo* TrackedReference_t6_137_il2cpp_TypeInfo_var;
extern "C" bool TrackedReference_Equals_m6_1760 (TrackedReference_t6_137 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackedReference_t6_137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1696);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = TrackedReference_op_Equality_m6_1762(NULL /*static, unused*/, ((TrackedReference_t6_137 *)IsInstClass(L_0, TrackedReference_t6_137_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C" int32_t TrackedReference_GetHashCode_m6_1761 (TrackedReference_t6_137 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		int32_t L_1 = IntPtr_op_Explicit_m1_845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool TrackedReference_op_Equality_m6_1762 (Object_t * __this /* static, unused */, TrackedReference_t6_137 * ___x, TrackedReference_t6_137 * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		TrackedReference_t6_137 * L_0 = ___x;
		V_0 = L_0;
		TrackedReference_t6_137 * L_1 = ___y;
		V_1 = L_1;
		Object_t * L_2 = V_1;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		Object_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		TrackedReference_t6_137 * L_5 = ___x;
		NullCheck(L_5);
		IntPtr_t L_6 = (L_5->___m_Ptr_0);
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_8 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0029:
	{
		Object_t * L_9 = V_0;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		TrackedReference_t6_137 * L_10 = ___y;
		NullCheck(L_10);
		IntPtr_t L_11 = (L_10->___m_Ptr_0);
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_13 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0040:
	{
		TrackedReference_t6_137 * L_14 = ___x;
		NullCheck(L_14);
		IntPtr_t L_15 = (L_14->___m_Ptr_0);
		TrackedReference_t6_137 * L_16 = ___y;
		NullCheck(L_16);
		IntPtr_t L_17 = (L_16->___m_Ptr_0);
		bool L_18 = IntPtr_op_Equality_m1_840(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t6_137_marshal(const TrackedReference_t6_137& unmarshaled, TrackedReference_t6_137_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void TrackedReference_t6_137_marshal_back(const TrackedReference_t6_137_marshaled& marshaled, TrackedReference_t6_137& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t6_137_marshal_cleanup(TrackedReference_t6_137_marshaled& marshaled)
{
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C" void ArgumentCache__ctor_m6_1763 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C" Object_t6_5 * ArgumentCache_get_unityObjectArgument_m6_1764 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = (__this->___m_ObjectArgument_3);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C" String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6_1765 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_ObjectArgumentAssemblyTypeName_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C" int32_t ArgumentCache_get_intArgument_m6_1766 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntArgument_5);
		return L_0;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C" float ArgumentCache_get_floatArgument_m6_1767 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatArgument_6);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C" String_t* ArgumentCache_get_stringArgument_m6_1768 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringArgument_7);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C" bool ArgumentCache_get_boolArgument_m6_1769 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_BoolArgument_8);
		return L_0;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Regex_t3_11_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4961;
extern Il2CppCodeGenString* _stringLiteral4962;
extern Il2CppCodeGenString* _stringLiteral4963;
extern "C" void ArgumentCache_TidyAssemblyTypeName_m6_1770 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Regex_t3_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1211);
		_stringLiteral4961 = il2cpp_codegen_string_literal_from_index(4961);
		_stringLiteral4962 = il2cpp_codegen_string_literal_from_index(4962);
		_stringLiteral4963 = il2cpp_codegen_string_literal_from_index(4963);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___m_ObjectArgumentAssemblyTypeName_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		String_t* L_2 = (__this->___m_ObjectArgumentAssemblyTypeName_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3_11_il2cpp_TypeInfo_var);
		String_t* L_4 = Regex_Replace_m3_1389(NULL /*static, unused*/, L_2, _stringLiteral4961, L_3, /*hidden argument*/NULL);
		__this->___m_ObjectArgumentAssemblyTypeName_4 = L_4;
		String_t* L_5 = (__this->___m_ObjectArgumentAssemblyTypeName_4);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_7 = Regex_Replace_m3_1389(NULL /*static, unused*/, L_5, _stringLiteral4962, L_6, /*hidden argument*/NULL);
		__this->___m_ObjectArgumentAssemblyTypeName_4 = L_7;
		String_t* L_8 = (__this->___m_ObjectArgumentAssemblyTypeName_4);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_10 = Regex_Replace_m3_1389(NULL /*static, unused*/, L_8, _stringLiteral4963, L_9, /*hidden argument*/NULL);
		__this->___m_ObjectArgumentAssemblyTypeName_4 = L_10;
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C" void ArgumentCache_OnBeforeSerialize_m6_1771 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m6_1770(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C" void ArgumentCache_OnAfterDeserialize_m6_1772 (ArgumentCache_t6_256 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m6_1770(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C" void BaseInvokableCall__ctor_m6_1773 (BaseInvokableCall_t6_257 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral139;
extern Il2CppCodeGenString* _stringLiteral4964;
extern "C" void BaseInvokableCall__ctor_m6_1774 (BaseInvokableCall_t6_257 * __this, Object_t * ___target, MethodInfo_t * ___function, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral139 = il2cpp_codegen_string_literal_from_index(139);
		_stringLiteral4964 = il2cpp_codegen_string_literal_from_index(4964);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Object_t * L_0 = ___target;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, _stringLiteral139, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		MethodInfo_t * L_2 = ___function;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, _stringLiteral4964, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern TypeInfo* Object_t6_5_il2cpp_TypeInfo_var;
extern "C" bool BaseInvokableCall_AllowInvoke_m6_1775 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___delegate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t6_5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1599);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t6_5 * V_1 = {0};
	{
		Delegate_t1_22 * L_0 = ___delegate;
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m1_884(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t * L_2 = V_0;
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return 1;
	}

IL_000f:
	{
		Object_t * L_3 = V_0;
		V_1 = ((Object_t6_5 *)IsInstClass(L_3, Object_t6_5_il2cpp_TypeInfo_var));
		Object_t6_5 * L_4 = V_1;
		bool L_5 = Object_ReferenceEquals_m1_8(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		Object_t6_5 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_6, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		return L_7;
	}

IL_002a:
	{
		return 1;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const Il2CppType* UnityAction_t6_259_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAction_t6_259_il2cpp_TypeInfo_var;
extern "C" void InvokableCall__ctor_m6_1776 (InvokableCall_t6_258 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAction_t6_259_0_0_0_var = il2cpp_codegen_type_from_index(1697);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		UnityAction_t6_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1697);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___target;
		MethodInfo_t * L_1 = ___theFunction;
		BaseInvokableCall__ctor_m6_1774(__this, L_0, L_1, /*hidden argument*/NULL);
		UnityAction_t6_259 * L_2 = (__this->___Delegate_0);
		MethodInfo_t * L_3 = ___theFunction;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(UnityAction_t6_259_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_5 = ___target;
		Delegate_t1_22 * L_6 = NetFxCoreExtensions_CreateDelegate_m6_1824(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		Delegate_t1_22 * L_7 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_2, ((UnityAction_t6_259 *)CastclassSealed(L_6, UnityAction_t6_259_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->___Delegate_0 = ((UnityAction_t6_259 *)CastclassSealed(L_7, UnityAction_t6_259_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern TypeInfo* UnityAction_t6_259_il2cpp_TypeInfo_var;
extern "C" void InvokableCall__ctor_m6_1777 (InvokableCall_t6_258 * __this, UnityAction_t6_259 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAction_t6_259_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1697);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseInvokableCall__ctor_m6_1773(__this, /*hidden argument*/NULL);
		UnityAction_t6_259 * L_0 = (__this->___Delegate_0);
		UnityAction_t6_259 * L_1 = ___action;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___Delegate_0 = ((UnityAction_t6_259 *)CastclassSealed(L_2, UnityAction_t6_259_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C" void InvokableCall_Invoke_m6_1778 (InvokableCall_t6_258 * __this, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method)
{
	{
		UnityAction_t6_259 * L_0 = (__this->___Delegate_0);
		bool L_1 = BaseInvokableCall_AllowInvoke_m6_1775(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UnityAction_t6_259 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		UnityAction_Invoke_m6_1831(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_Find_m6_1779 (InvokableCall_t6_258 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_t6_259 * L_0 = (__this->___Delegate_0);
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m1_884(L_0, /*hidden argument*/NULL);
		Object_t * L_2 = ___targetObj;
		if ((!(((Object_t*)(Object_t *)L_1) == ((Object_t*)(Object_t *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_t6_259 * L_3 = (__this->___Delegate_0);
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m6_1825(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method;
		G_B3_0 = ((((Object_t*)(MethodInfo_t *)L_4) == ((Object_t*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern TypeInfo* ArgumentCache_t6_256_il2cpp_TypeInfo_var;
extern "C" void PersistentCall__ctor_m6_1780 (PersistentCall_t6_261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentCache_t6_256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1698);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArgumentCache_t6_256 * L_0 = (ArgumentCache_t6_256 *)il2cpp_codegen_object_new (ArgumentCache_t6_256_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m6_1763(L_0, /*hidden argument*/NULL);
		__this->___m_Arguments_3 = L_0;
		__this->___m_CallState_4 = 2;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C" Object_t6_5 * PersistentCall_get_target_m6_1781 (PersistentCall_t6_261 * __this, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = (__this->___m_Target_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C" String_t* PersistentCall_get_methodName_m6_1782 (PersistentCall_t6_261 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_MethodName_1);
		return L_0;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C" int32_t PersistentCall_get_mode_m6_1783 (PersistentCall_t6_261 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Mode_2);
		return L_0;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C" ArgumentCache_t6_256 * PersistentCall_get_arguments_m6_1784 (PersistentCall_t6_261 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_t6_256 * L_0 = (__this->___m_Arguments_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool PersistentCall_IsValid_m6_1785 (PersistentCall_t6_261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Object_t6_5 * L_0 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m6_1782(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern TypeInfo* CachedInvokableCall_1_t6_295_il2cpp_TypeInfo_var;
extern TypeInfo* CachedInvokableCall_1_t6_296_il2cpp_TypeInfo_var;
extern TypeInfo* CachedInvokableCall_1_t6_297_il2cpp_TypeInfo_var;
extern TypeInfo* CachedInvokableCall_1_t6_298_il2cpp_TypeInfo_var;
extern TypeInfo* InvokableCall_t6_258_il2cpp_TypeInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m6_1836_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m6_1837_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m6_1838_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m6_1839_MethodInfo_var;
extern "C" BaseInvokableCall_t6_257 * PersistentCall_GetRuntimeCall_m6_1786 (PersistentCall_t6_261 * __this, UnityEventBase_t6_264 * ___theEvent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CachedInvokableCall_1_t6_295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1699);
		CachedInvokableCall_1_t6_296_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1700);
		CachedInvokableCall_1_t6_297_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1701);
		CachedInvokableCall_1_t6_298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1702);
		InvokableCall_t6_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1703);
		CachedInvokableCall_1__ctor_m6_1836_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483813);
		CachedInvokableCall_1__ctor_m6_1837_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483814);
		CachedInvokableCall_1__ctor_m6_1838_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483815);
		CachedInvokableCall_1__ctor_m6_1839_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483816);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = (__this->___m_CallState_4);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		UnityEventBase_t6_264 * L_1 = ___theEvent;
		if (L_1)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (BaseInvokableCall_t6_257 *)NULL;
	}

IL_0013:
	{
		UnityEventBase_t6_264 * L_2 = ___theEvent;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m6_1799(L_2, __this, /*hidden argument*/NULL);
		V_0 = L_3;
		MethodInfo_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (BaseInvokableCall_t6_257 *)NULL;
	}

IL_0023:
	{
		int32_t L_5 = (__this->___m_Mode_2);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6 == 0)
		{
			goto IL_0051;
		}
		if (L_6 == 1)
		{
			goto IL_00d2;
		}
		if (L_6 == 2)
		{
			goto IL_005f;
		}
		if (L_6 == 3)
		{
			goto IL_008a;
		}
		if (L_6 == 4)
		{
			goto IL_0072;
		}
		if (L_6 == 5)
		{
			goto IL_00a2;
		}
		if (L_6 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		UnityEventBase_t6_264 * L_7 = ___theEvent;
		Object_t6_5 * L_8 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_0;
		NullCheck(L_7);
		BaseInvokableCall_t6_257 * L_10 = (BaseInvokableCall_t6_257 *)VirtFuncInvoker2< BaseInvokableCall_t6_257 *, Object_t *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		return L_10;
	}

IL_005f:
	{
		Object_t6_5 * L_11 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_0;
		ArgumentCache_t6_256 * L_13 = (__this->___m_Arguments_3);
		BaseInvokableCall_t6_257 * L_14 = PersistentCall_GetObjectCall_m6_1787(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0072:
	{
		Object_t6_5 * L_15 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_0;
		ArgumentCache_t6_256 * L_17 = (__this->___m_Arguments_3);
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m6_1767(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t6_295 * L_19 = (CachedInvokableCall_1_t6_295 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t6_295_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m6_1836(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m6_1836_MethodInfo_var);
		return L_19;
	}

IL_008a:
	{
		Object_t6_5 * L_20 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_0;
		ArgumentCache_t6_256 * L_22 = (__this->___m_Arguments_3);
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m6_1766(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t6_296 * L_24 = (CachedInvokableCall_1_t6_296 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t6_296_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m6_1837(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m6_1837_MethodInfo_var);
		return L_24;
	}

IL_00a2:
	{
		Object_t6_5 * L_25 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_0;
		ArgumentCache_t6_256 * L_27 = (__this->___m_Arguments_3);
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m6_1768(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t6_297 * L_29 = (CachedInvokableCall_1_t6_297 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t6_297_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m6_1838(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m6_1838_MethodInfo_var);
		return L_29;
	}

IL_00ba:
	{
		Object_t6_5 * L_30 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_0;
		ArgumentCache_t6_256 * L_32 = (__this->___m_Arguments_3);
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m6_1769(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t6_298 * L_34 = (CachedInvokableCall_1_t6_298 *)il2cpp_codegen_object_new (CachedInvokableCall_1_t6_298_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m6_1839(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m6_1839_MethodInfo_var);
		return L_34;
	}

IL_00d2:
	{
		Object_t6_5 * L_35 = PersistentCall_get_target_m6_1781(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_0;
		InvokableCall_t6_258 * L_37 = (InvokableCall_t6_258 *)il2cpp_codegen_object_new (InvokableCall_t6_258_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m6_1776(L_37, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}

IL_00df:
	{
		return (BaseInvokableCall_t6_257 *)NULL;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const Il2CppType* Object_t6_5_0_0_0_var;
extern const Il2CppType* CachedInvokableCall_1_t6_299_0_0_0_var;
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* BaseInvokableCall_t6_257_il2cpp_TypeInfo_var;
extern "C" BaseInvokableCall_t6_257 * PersistentCall_GetObjectCall_m6_1787 (Object_t * __this /* static, unused */, Object_t6_5 * ___target, MethodInfo_t * ___method, ArgumentCache_t6_256 * ___arguments, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t6_5_0_0_0_var = il2cpp_codegen_type_from_index(1599);
		CachedInvokableCall_1_t6_299_0_0_0_var = il2cpp_codegen_type_from_index(1704);
		MethodInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(62);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		BaseInvokableCall_t6_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1705);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	ConstructorInfo_t1_478 * V_3 = {0};
	Object_t6_5 * V_4 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t6_256 * L_1 = ___arguments;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6_1765(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentCache_t6_256 * L_4 = ___arguments;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6_1765(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m1_1129, L_5, 0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t6_299_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t1_31* L_11 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0, sizeof(Type_t *))) = (Type_t *)L_12;
		NullCheck(L_10);
		Type_t * L_13 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t1_31* L_15 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 3));
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_15, 0, sizeof(Type_t *))) = (Type_t *)L_16;
		TypeU5BU5D_t1_31* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_18);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_17, 1, sizeof(Type_t *))) = (Type_t *)L_18;
		TypeU5BU5D_t1_31* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_20);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_19, 2, sizeof(Type_t *))) = (Type_t *)L_20;
		NullCheck(L_14);
		ConstructorInfo_t1_478 * L_21 = (ConstructorInfo_t1_478 *)VirtFuncInvoker1< ConstructorInfo_t1_478 *, TypeU5BU5D_t1_31* >::Invoke(49 /* System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[]) */, L_14, L_19);
		V_3 = L_21;
		ArgumentCache_t6_256 * L_22 = ___arguments;
		NullCheck(L_22);
		Object_t6_5 * L_23 = ArgumentCache_get_unityObjectArgument_m6_1764(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t6_5 * L_24 = V_4;
		bool L_25 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_24, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00aa;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t6_5 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m1_5(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (Object_t6_5 *)NULL;
	}

IL_00aa:
	{
		ConstructorInfo_t1_478 * L_30 = V_3;
		ObjectU5BU5D_t1_272* L_31 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		Object_t6_5 * L_32 = ___target;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 0);
		ArrayElementTypeCheck (L_31, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 0, sizeof(Object_t *))) = (Object_t *)L_32;
		ObjectU5BU5D_t1_272* L_33 = L_31;
		MethodInfo_t * L_34 = ___method;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 1);
		ArrayElementTypeCheck (L_33, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, 1, sizeof(Object_t *))) = (Object_t *)L_34;
		ObjectU5BU5D_t1_272* L_35 = L_33;
		Object_t6_5 * L_36 = V_4;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 2);
		ArrayElementTypeCheck (L_35, L_36);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_35, 2, sizeof(Object_t *))) = (Object_t *)L_36;
		NullCheck(L_30);
		Object_t * L_37 = ConstructorInfo_Invoke_m1_6777(L_30, L_35, /*hidden argument*/NULL);
		return ((BaseInvokableCall_t6_257 *)IsInstClass(L_37, BaseInvokableCall_t6_257_il2cpp_TypeInfo_var));
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern TypeInfo* List_1_t1_1832_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14968_MethodInfo_var;
extern "C" void PersistentCallGroup__ctor_m6_1788 (PersistentCallGroup_t6_262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1832_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1707);
		List_1__ctor_m1_14968_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483817);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		List_1_t1_1832 * L_0 = (List_1_t1_1832 *)il2cpp_codegen_object_new (List_1_t1_1832_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14968(L_0, /*hidden argument*/List_1__ctor_m1_14968_MethodInfo_var);
		__this->___m_Calls_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern TypeInfo* Enumerator_t1_1851_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14969_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14970_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14971_MethodInfo_var;
extern "C" void PersistentCallGroup_Initialize_m6_1789 (PersistentCallGroup_t6_262 * __this, InvokableCallList_t6_263 * ___invokableList, UnityEventBase_t6_264 * ___unityEventBase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1851_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1708);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14969_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483818);
		Enumerator_get_Current_m1_14970_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483819);
		Enumerator_MoveNext_m1_14971_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483820);
		s_Il2CppMethodIntialized = true;
	}
	PersistentCall_t6_261 * V_0 = {0};
	Enumerator_t1_1851  V_1 = {0};
	BaseInvokableCall_t6_257 * V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1_1832 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		Enumerator_t1_1851  L_1 = List_1_GetEnumerator_m1_14969(L_0, /*hidden argument*/List_1_GetEnumerator_m1_14969_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			PersistentCall_t6_261 * L_2 = Enumerator_get_Current_m1_14970((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14970_MethodInfo_var);
			V_0 = L_2;
			PersistentCall_t6_261 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m6_1785(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			PersistentCall_t6_261 * L_5 = V_0;
			UnityEventBase_t6_264 * L_6 = ___unityEventBase;
			NullCheck(L_5);
			BaseInvokableCall_t6_257 * L_7 = PersistentCall_GetRuntimeCall_m6_1786(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t6_257 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			InvokableCallList_t6_263 * L_9 = ___invokableList;
			BaseInvokableCall_t6_257 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m6_1791(L_9, L_10, /*hidden argument*/NULL);
		}

IL_003e:
		{
			bool L_11 = Enumerator_MoveNext_m1_14971((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14971_MethodInfo_var);
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_t1_1851  L_12 = V_1;
		Enumerator_t1_1851  L_13 = L_12;
		Object_t * L_14 = Box(Enumerator_t1_1851_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_14);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern TypeInfo* List_1_t1_1833_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14972_MethodInfo_var;
extern "C" void InvokableCallList__ctor_m6_1790 (InvokableCallList_t6_263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1833_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1709);
		List_1__ctor_m1_14972_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483821);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1833 * L_0 = (List_1_t1_1833 *)il2cpp_codegen_object_new (List_1_t1_1833_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14972(L_0, /*hidden argument*/List_1__ctor_m1_14972_MethodInfo_var);
		__this->___m_PersistentCalls_0 = L_0;
		List_1_t1_1833 * L_1 = (List_1_t1_1833 *)il2cpp_codegen_object_new (List_1_t1_1833_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14972(L_1, /*hidden argument*/List_1__ctor_m1_14972_MethodInfo_var);
		__this->___m_RuntimeCalls_1 = L_1;
		List_1_t1_1833 * L_2 = (List_1_t1_1833 *)il2cpp_codegen_object_new (List_1_t1_1833_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14972(L_2, /*hidden argument*/List_1__ctor_m1_14972_MethodInfo_var);
		__this->___m_ExecutingCalls_2 = L_2;
		__this->___m_NeedsUpdate_3 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C" void InvokableCallList_AddPersistentInvokableCall_m6_1791 (InvokableCallList_t6_263 * __this, BaseInvokableCall_t6_257 * ___call, const MethodInfo* method)
{
	{
		List_1_t1_1833 * L_0 = (__this->___m_PersistentCalls_0);
		BaseInvokableCall_t6_257 * L_1 = ___call;
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t6_257 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0) */, L_0, L_1);
		__this->___m_NeedsUpdate_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C" void InvokableCallList_AddListener_m6_1792 (InvokableCallList_t6_263 * __this, BaseInvokableCall_t6_257 * ___call, const MethodInfo* method)
{
	{
		List_1_t1_1833 * L_0 = (__this->___m_RuntimeCalls_1);
		BaseInvokableCall_t6_257 * L_1 = ___call;
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t6_257 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0) */, L_0, L_1);
		__this->___m_NeedsUpdate_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* List_1_t1_1833_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t1_1852_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14972_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m1_14973_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m1_14974_MethodInfo_var;
extern "C" void InvokableCallList_RemoveListener_m6_1793 (InvokableCallList_t6_263 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1833_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1709);
		Predicate_1_t1_1852_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1710);
		List_1__ctor_m1_14972_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483821);
		Predicate_1__ctor_m1_14973_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483822);
		List_1_RemoveAll_m1_14974_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483823);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1_1833 * V_0 = {0};
	int32_t V_1 = 0;
	{
		List_1_t1_1833 * L_0 = (List_1_t1_1833 *)il2cpp_codegen_object_new (List_1_t1_1833_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14972(L_0, /*hidden argument*/List_1__ctor_m1_14972_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		List_1_t1_1833 * L_1 = (__this->___m_RuntimeCalls_1);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t6_257 * L_3 = (BaseInvokableCall_t6_257 *)VirtFuncInvoker1< BaseInvokableCall_t6_257 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32) */, L_1, L_2);
		Object_t * L_4 = ___targetObj;
		MethodInfo_t * L_5 = ___method;
		NullCheck(L_3);
		bool L_6 = (bool)VirtFuncInvoker2< bool, Object_t *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t1_1833 * L_7 = V_0;
		List_1_t1_1833 * L_8 = (__this->___m_RuntimeCalls_1);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t6_257 * L_10 = (BaseInvokableCall_t6_257 *)VirtFuncInvoker1< BaseInvokableCall_t6_257 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_7);
		VirtActionInvoker1< BaseInvokableCall_t6_257 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0) */, L_7, L_10);
	}

IL_0037:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_12 = V_1;
		List_1_t1_1833 * L_13 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count() */, L_13);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1_1833 * L_15 = (__this->___m_RuntimeCalls_1);
		List_1_t1_1833 * L_16 = V_0;
		List_1_t1_1833 * L_17 = L_16;
		IntPtr_t L_18 = { (void*)GetVirtualMethodInfo(L_17, 24) };
		Predicate_1_t1_1852 * L_19 = (Predicate_1_t1_1852 *)il2cpp_codegen_object_new (Predicate_1_t1_1852_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1_14973(L_19, L_17, L_18, /*hidden argument*/Predicate_1__ctor_m1_14973_MethodInfo_var);
		NullCheck(L_15);
		List_1_RemoveAll_m1_14974(L_15, L_19, /*hidden argument*/List_1_RemoveAll_m1_14974_MethodInfo_var);
		__this->___m_NeedsUpdate_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C" void InvokableCallList_ClearPersistent_m6_1794 (InvokableCallList_t6_263 * __this, const MethodInfo* method)
{
	{
		List_1_t1_1833 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear() */, L_0);
		__this->___m_NeedsUpdate_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo* List_1_AddRange_m1_14975_MethodInfo_var;
extern "C" void InvokableCallList_Invoke_m6_1795 (InvokableCallList_t6_263 * __this, ObjectU5BU5D_t1_272* ___parameters, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_AddRange_m1_14975_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483824);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___m_NeedsUpdate_3);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		List_1_t1_1833 * L_1 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear() */, L_1);
		List_1_t1_1833 * L_2 = (__this->___m_ExecutingCalls_2);
		List_1_t1_1833 * L_3 = (__this->___m_PersistentCalls_0);
		NullCheck(L_2);
		List_1_AddRange_m1_14975(L_2, L_3, /*hidden argument*/List_1_AddRange_m1_14975_MethodInfo_var);
		List_1_t1_1833 * L_4 = (__this->___m_ExecutingCalls_2);
		List_1_t1_1833 * L_5 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_4);
		List_1_AddRange_m1_14975(L_4, L_5, /*hidden argument*/List_1_AddRange_m1_14975_MethodInfo_var);
		__this->___m_NeedsUpdate_3 = 0;
	}

IL_003f:
	{
		V_0 = 0;
		goto IL_005c;
	}

IL_0046:
	{
		List_1_t1_1833 * L_6 = (__this->___m_ExecutingCalls_2);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		BaseInvokableCall_t6_257 * L_8 = (BaseInvokableCall_t6_257 *)VirtFuncInvoker1< BaseInvokableCall_t6_257 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32) */, L_6, L_7);
		ObjectU5BU5D_t1_272* L_9 = ___parameters;
		NullCheck(L_8);
		VirtActionInvoker1< ObjectU5BU5D_t1_272* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_11 = V_0;
		List_1_t1_1833 * L_12 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern TypeInfo* InvokableCallList_t6_263_il2cpp_TypeInfo_var;
extern TypeInfo* PersistentCallGroup_t6_262_il2cpp_TypeInfo_var;
extern "C" void UnityEventBase__ctor_m6_1796 (UnityEventBase_t6_264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvokableCallList_t6_263_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1711);
		PersistentCallGroup_t6_262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1712);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_CallsDirty_3 = 1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		InvokableCallList_t6_263 * L_0 = (InvokableCallList_t6_263 *)il2cpp_codegen_object_new (InvokableCallList_t6_263_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m6_1790(L_0, /*hidden argument*/NULL);
		__this->___m_Calls_0 = L_0;
		PersistentCallGroup_t6_262 * L_1 = (PersistentCallGroup_t6_262 *)il2cpp_codegen_object_new (PersistentCallGroup_t6_262_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m6_1788(L_1, /*hidden argument*/NULL);
		__this->___m_PersistentCalls_1 = L_1;
		Type_t * L_2 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(157 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->___m_TypeName_2 = L_3;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m6_1797 (UnityEventBase_t6_264 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m6_1798 (UnityEventBase_t6_264 * __this, const MethodInfo* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m6_1801(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(157 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->___m_TypeName_2 = L_1;
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern const Il2CppType* Object_t6_5_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEventBase_FindMethod_m6_1799 (UnityEventBase_t6_264 * __this, PersistentCall_t6_261 * ___call, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t6_5_0_0_0_var = il2cpp_codegen_type_from_index(1599);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t6_261 * L_1 = ___call;
		NullCheck(L_1);
		ArgumentCache_t6_256 * L_2 = PersistentCall_get_arguments_m6_1784(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6_1765(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		PersistentCall_t6_261 * L_5 = ___call;
		NullCheck(L_5);
		ArgumentCache_t6_256 * L_6 = PersistentCall_get_arguments_m6_1784(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6_1765(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((methodPointerType)&Type_GetType_m1_1129, L_7, 0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0042:
	{
		V_0 = G_B3_0;
	}

IL_0043:
	{
		PersistentCall_t6_261 * L_11 = ___call;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m6_1782(L_11, /*hidden argument*/NULL);
		PersistentCall_t6_261 * L_13 = ___call;
		NullCheck(L_13);
		Object_t6_5 * L_14 = PersistentCall_get_target_m6_1781(L_13, /*hidden argument*/NULL);
		PersistentCall_t6_261 * L_15 = ___call;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m6_1783(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m6_1800(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern const Il2CppType* Single_t1_17_0_0_0_var;
extern const Il2CppType* Int32_t1_3_0_0_0_var;
extern const Il2CppType* Boolean_t1_20_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Object_t6_5_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEventBase_FindMethod_m6_1800 (UnityEventBase_t6_264 * __this, String_t* ___name, Object_t * ___listener, int32_t ___mode, Type_t * ___argumentType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_0_0_0_var = il2cpp_codegen_type_from_index(47);
		Int32_t1_3_0_0_0_var = il2cpp_codegen_type_from_index(11);
		Boolean_t1_20_0_0_0_var = il2cpp_codegen_type_from_index(54);
		String_t_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Object_t6_5_0_0_0_var = il2cpp_codegen_type_from_index(1599);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Type_t * G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t1_31* G_B10_2 = {0};
	TypeU5BU5D_t1_31* G_B10_3 = {0};
	String_t* G_B10_4 = {0};
	Object_t * G_B10_5 = {0};
	Type_t * G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t1_31* G_B9_2 = {0};
	TypeU5BU5D_t1_31* G_B9_3 = {0};
	String_t* G_B9_4 = {0};
	Object_t * G_B9_5 = {0};
	{
		int32_t L_0 = ___mode;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0032;
		}
		if (L_1 == 2)
		{
			goto IL_00ac;
		}
		if (L_1 == 3)
		{
			goto IL_005b;
		}
		if (L_1 == 4)
		{
			goto IL_0040;
		}
		if (L_1 == 5)
		{
			goto IL_0091;
		}
		if (L_1 == 6)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_00d0;
	}

IL_0029:
	{
		String_t* L_2 = ___name;
		Object_t * L_3 = ___listener;
		MethodInfo_t * L_4 = (MethodInfo_t *)VirtFuncInvoker2< MethodInfo_t *, String_t*, Object_t * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_2, L_3);
		return L_4;
	}

IL_0032:
	{
		Object_t * L_5 = ___listener;
		String_t* L_6 = ___name;
		MethodInfo_t * L_7 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, L_5, L_6, ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return L_7;
	}

IL_0040:
	{
		Object_t * L_8 = ___listener;
		String_t* L_9 = ___name;
		TypeU5BU5D_t1_31* L_10 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Single_t1_17_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_10, 0, sizeof(Type_t *))) = (Type_t *)L_11;
		MethodInfo_t * L_12 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_12;
	}

IL_005b:
	{
		Object_t * L_13 = ___listener;
		String_t* L_14 = ___name;
		TypeU5BU5D_t1_31* L_15 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Int32_t1_3_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_15, 0, sizeof(Type_t *))) = (Type_t *)L_16;
		MethodInfo_t * L_17 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_17;
	}

IL_0076:
	{
		Object_t * L_18 = ___listener;
		String_t* L_19 = ___name;
		TypeU5BU5D_t1_31* L_20 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Boolean_t1_20_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, L_21);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_20, 0, sizeof(Type_t *))) = (Type_t *)L_21;
		MethodInfo_t * L_22 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_22;
	}

IL_0091:
	{
		Object_t * L_23 = ___listener;
		String_t* L_24 = ___name;
		TypeU5BU5D_t1_31* L_25 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		ArrayElementTypeCheck (L_25, L_26);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_25, 0, sizeof(Type_t *))) = (Type_t *)L_26;
		MethodInfo_t * L_27 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, L_23, L_24, L_25, /*hidden argument*/NULL);
		return L_27;
	}

IL_00ac:
	{
		Object_t * L_28 = ___listener;
		String_t* L_29 = ___name;
		TypeU5BU5D_t1_31* L_30 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_31 = ___argumentType;
		Type_t * L_32 = L_31;
		G_B9_0 = L_32;
		G_B9_1 = 0;
		G_B9_2 = L_30;
		G_B9_3 = L_30;
		G_B9_4 = L_29;
		G_B9_5 = L_28;
		if (L_32)
		{
			G_B10_0 = L_32;
			G_B10_1 = 0;
			G_B10_2 = L_30;
			G_B10_3 = L_30;
			G_B10_4 = L_29;
			G_B10_5 = L_28;
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t6_5_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_33;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00c9:
	{
		NullCheck(G_B10_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B10_2, G_B10_1);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(G_B10_2, G_B10_1, sizeof(Type_t *))) = (Type_t *)G_B10_0;
		MethodInfo_t * L_34 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		return L_34;
	}

IL_00d0:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C" void UnityEventBase_DirtyPersistentCalls_m6_1801 (UnityEventBase_t6_264 * __this, const MethodInfo* method)
{
	{
		InvokableCallList_t6_263 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m6_1794(L_0, /*hidden argument*/NULL);
		__this->___m_CallsDirty_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C" void UnityEventBase_RebuildPersistentCallsIfNeeded_m6_1802 (UnityEventBase_t6_264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CallsDirty_3);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		PersistentCallGroup_t6_262 * L_1 = (__this->___m_PersistentCalls_1);
		InvokableCallList_t6_263 * L_2 = (__this->___m_Calls_0);
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m6_1789(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->___m_CallsDirty_3 = 0;
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C" void UnityEventBase_AddCall_m6_1803 (UnityEventBase_t6_264 * __this, BaseInvokableCall_t6_257 * ___call, const MethodInfo* method)
{
	{
		InvokableCallList_t6_263 * L_0 = (__this->___m_Calls_0);
		BaseInvokableCall_t6_257 * L_1 = ___call;
		NullCheck(L_0);
		InvokableCallList_AddListener_m6_1792(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C" void UnityEventBase_RemoveListener_m6_1804 (UnityEventBase_t6_264 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	{
		InvokableCallList_t6_263 * L_0 = (__this->___m_Calls_0);
		Object_t * L_1 = ___targetObj;
		MethodInfo_t * L_2 = ___method;
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m6_1793(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C" void UnityEventBase_Invoke_m6_1805 (UnityEventBase_t6_264 * __this, ObjectU5BU5D_t1_272* ___parameters, const MethodInfo* method)
{
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m6_1802(__this, /*hidden argument*/NULL);
		InvokableCallList_t6_263 * L_0 = (__this->___m_Calls_0);
		ObjectU5BU5D_t1_272* L_1 = ___parameters;
		NullCheck(L_0);
		InvokableCallList_Invoke_m6_1795(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral268;
extern "C" String_t* UnityEventBase_ToString_m6_1806 (UnityEventBase_t6_264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Object_ToString_m1_7(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(159 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_560(NULL /*static, unused*/, L_0, _stringLiteral268, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern const Il2CppType* Object_t_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEventBase_GetValidMethodInfo_m6_1807 (Object_t * __this /* static, unused */, Object_t * ___obj, String_t* ___functionName, TypeU5BU5D_t1_31* ___argumentTypes, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_0_0_0_var = il2cpp_codegen_type_from_index(0);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	MethodInfo_t * V_1 = {0};
	ParameterInfoU5BU5D_t1_1685* V_2 = {0};
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t1_627 * V_5 = {0};
	ParameterInfoU5BU5D_t1_1685* V_6 = {0};
	int32_t V_7 = 0;
	Type_t * V_8 = {0};
	Type_t * V_9 = {0};
	{
		Object_t * L_0 = ___obj;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m1_5(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_008e;
	}

IL_000c:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName;
		TypeU5BU5D_t1_31* L_4 = ___argumentTypes;
		NullCheck(L_2);
		MethodInfo_t * L_5 = (MethodInfo_t *)VirtFuncInvoker5< MethodInfo_t *, String_t*, int32_t, Binder_t1_585 *, TypeU5BU5D_t1_31*, ParameterModifierU5BU5D_t1_1669* >::Invoke(84 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[]) */, L_2, L_3, ((int32_t)52), (Binder_t1_585 *)NULL, L_4, (ParameterModifierU5BU5D_t1_1669*)(ParameterModifierU5BU5D_t1_1669*)NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0087;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t1_1685* L_8 = (ParameterInfoU5BU5D_t1_1685*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1_1685* >::Invoke(68 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = 1;
		V_4 = 0;
		ParameterInfoU5BU5D_t1_1685* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_0074;
	}

IL_0036:
	{
		ParameterInfoU5BU5D_t1_1685* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_5 = (*(ParameterInfo_t1_627 **)(ParameterInfo_t1_627 **)SZArrayLdElema(L_10, L_12, sizeof(ParameterInfo_t1_627 *)));
		TypeU5BU5D_t1_31* L_13 = ___argumentTypes;
		int32_t L_14 = V_4;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		V_8 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_13, L_15, sizeof(Type_t *)));
		ParameterInfo_t1_627 * L_16 = V_5;
		NullCheck(L_16);
		Type_t * L_17 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(11 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_16);
		V_9 = L_17;
		Type_t * L_18 = V_8;
		NullCheck(L_18);
		bool L_19 = (bool)VirtFuncInvoker0< bool >::Invoke(141 /* System.Boolean System.Type::get_IsPrimitive() */, L_18);
		Type_t * L_20 = V_9;
		NullCheck(L_20);
		bool L_21 = (bool)VirtFuncInvoker0< bool >::Invoke(141 /* System.Boolean System.Type::get_IsPrimitive() */, L_20);
		V_3 = ((((int32_t)L_19) == ((int32_t)L_21))? 1 : 0);
		bool L_22 = V_3;
		if (L_22)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_007f;
	}

IL_0068:
	{
		int32_t L_23 = V_4;
		V_4 = ((int32_t)((int32_t)L_23+(int32_t)1));
		int32_t L_24 = V_7;
		V_7 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_25 = V_7;
		ParameterInfoU5BU5D_t1_1685* L_26 = V_6;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_26)->max_length)))))))
		{
			goto IL_0036;
		}
	}

IL_007f:
	{
		bool L_27 = V_3;
		if (!L_27)
		{
			goto IL_0087;
		}
	}
	{
		MethodInfo_t * L_28 = V_1;
		return L_28;
	}

IL_0087:
	{
		Type_t * L_29 = V_0;
		NullCheck(L_29);
		Type_t * L_30 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_29);
		V_0 = L_30;
	}

IL_008e:
	{
		Type_t * L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(Object_t_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_31) == ((Object_t*)(Type_t *)L_32)))
		{
			goto IL_00a4;
		}
	}
	{
		Type_t * L_33 = V_0;
		if (L_33)
		{
			goto IL_000c;
		}
	}

IL_00a4:
	{
		return (MethodInfo_t *)NULL;
	}
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern "C" void UnityEvent__ctor_m6_1808 (UnityEvent_t6_265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 0));
		UnityEventBase__ctor_m6_1796(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C" void UnityEvent_AddListener_m6_1809 (UnityEvent_t6_265 * __this, UnityAction_t6_259 * ___call, const MethodInfo* method)
{
	{
		UnityAction_t6_259 * L_0 = ___call;
		BaseInvokableCall_t6_257 * L_1 = UnityEvent_GetDelegate_m6_1812(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m6_1803(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEvent_FindMethod_Impl_m6_1810 (UnityEvent_t6_265 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___targetObj;
		String_t* L_1 = ___name;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* InvokableCall_t6_258_il2cpp_TypeInfo_var;
extern "C" BaseInvokableCall_t6_257 * UnityEvent_GetDelegate_m6_1811 (UnityEvent_t6_265 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvokableCall_t6_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1703);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___target;
		MethodInfo_t * L_1 = ___theFunction;
		InvokableCall_t6_258 * L_2 = (InvokableCall_t6_258 *)il2cpp_codegen_object_new (InvokableCall_t6_258_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m6_1776(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern TypeInfo* InvokableCall_t6_258_il2cpp_TypeInfo_var;
extern "C" BaseInvokableCall_t6_257 * UnityEvent_GetDelegate_m6_1812 (Object_t * __this /* static, unused */, UnityAction_t6_259 * ___action, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvokableCall_t6_258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1703);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_t6_259 * L_0 = ___action;
		InvokableCall_t6_258 * L_1 = (InvokableCall_t6_258 *)il2cpp_codegen_object_new (InvokableCall_t6_258_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m6_1777(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C" void UnityEvent_Invoke_m6_1813 (UnityEvent_t6_265 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1_272* L_0 = (__this->___m_InvokeArray_4);
		UnityEventBase_Invoke_m6_1805(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern "C" void DefaultValueAttribute__ctor_m6_1814 (DefaultValueAttribute_t6_266 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___value;
		__this->___DefaultValue_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern "C" Object_t * DefaultValueAttribute_get_Value_m6_1815 (DefaultValueAttribute_t6_266 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern TypeInfo* DefaultValueAttribute_t6_266_il2cpp_TypeInfo_var;
extern "C" bool DefaultValueAttribute_Equals_m6_1816 (DefaultValueAttribute_t6_266 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t6_266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1713);
		s_Il2CppMethodIntialized = true;
	}
	DefaultValueAttribute_t6_266 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((DefaultValueAttribute_t6_266 *)IsInstClass(L_0, DefaultValueAttribute_t6_266_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t6_266 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		DefaultValueAttribute_t6_266 * L_3 = V_0;
		NullCheck(L_3);
		Object_t * L_4 = DefaultValueAttribute_get_Value_m6_1815(L_3, /*hidden argument*/NULL);
		return ((((Object_t*)(Object_t *)L_4) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0024:
	{
		Object_t * L_5 = (__this->___DefaultValue_0);
		DefaultValueAttribute_t6_266 * L_6 = V_0;
		NullCheck(L_6);
		Object_t * L_7 = DefaultValueAttribute_get_Value_m6_1815(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_5, L_7);
		return L_8;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern "C" int32_t DefaultValueAttribute_GetHashCode_m6_1817 (DefaultValueAttribute_t6_266 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m1_52(__this, /*hidden argument*/NULL);
		return L_1;
	}

IL_0012:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		return L_3;
	}
}
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern "C" void ExcludeFromDocsAttribute__ctor_m6_1818 (ExcludeFromDocsAttribute_t6_267 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C" void FormerlySerializedAsAttribute__ctor_m6_1819 (FormerlySerializedAsAttribute_t6_268 * __this, String_t* ___oldName, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName;
		__this->___m_oldName_0 = L_0;
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern TypeInfo* TypeInferenceRules_t6_269_il2cpp_TypeInfo_var;
extern "C" void TypeInferenceRuleAttribute__ctor_m6_1820 (TypeInferenceRuleAttribute_t6_270 * __this, int32_t ___rule, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRules_t6_269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1714);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___rule;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TypeInferenceRules_t6_269_il2cpp_TypeInfo_var, &L_1);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_2);
		TypeInferenceRuleAttribute__ctor_m6_1821(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C" void TypeInferenceRuleAttribute__ctor_m6_1821 (TypeInferenceRuleAttribute_t6_270 * __this, String_t* ___rule, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule;
		__this->____rule_0 = L_0;
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C" String_t* TypeInferenceRuleAttribute_ToString_m6_1822 (TypeInferenceRuleAttribute_t6_270 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->____rule_0);
		return L_0;
	}
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m6_1823 (GenericStack_t6_170 * __this, const MethodInfo* method)
{
	{
		Stack__ctor_m1_3518(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C" Delegate_t1_22 * NetFxCoreExtensions_CreateDelegate_m6_1824 (Object_t * __this /* static, unused */, MethodInfo_t * ___self, Type_t * ___delegateType, Object_t * ___target, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___delegateType;
		Object_t * L_1 = ___target;
		MethodInfo_t * L_2 = ___self;
		Delegate_t1_22 * L_3 = Delegate_CreateDelegate_m1_890(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C" MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m6_1825 (Object_t * __this /* static, unused */, Delegate_t1_22 * ___self, const MethodInfo* method)
{
	{
		Delegate_t1_22 * L_0 = ___self;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m1_883(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAdsDelegate__ctor_m6_1826 (UnityAdsDelegate_t6_106 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::Invoke()
extern "C" void UnityAdsDelegate_Invoke_m6_1827 (UnityAdsDelegate_t6_106 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAdsDelegate_Invoke_m6_1827((UnityAdsDelegate_t6_106 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t6_106(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAdsDelegate_BeginInvoke_m6_1828 (UnityAdsDelegate_t6_106 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::EndInvoke(System.IAsyncResult)
extern "C" void UnityAdsDelegate_EndInvoke_m6_1829 (UnityAdsDelegate_t6_106 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAction__ctor_m6_1830 (UnityAction_t6_259 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C" void UnityAction_Invoke_m6_1831 (UnityAction_t6_259 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAction_Invoke_m6_1831((UnityAction_t6_259 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UnityAction_t6_259(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAction_BeginInvoke_m6_1832 (UnityAction_t6_259 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C" void UnityAction_EndInvoke_m6_1833 (UnityAction_t6_259 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
