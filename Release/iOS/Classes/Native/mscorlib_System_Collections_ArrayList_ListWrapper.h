﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IList
struct IList_t1_262;

#include "mscorlib_System_Object.h"

// System.Collections.ArrayList/ListWrapper
struct  ListWrapper_t1_269  : public Object_t
{
	// System.Collections.IList System.Collections.ArrayList/ListWrapper::m_InnerList
	Object_t * ___m_InnerList_0;
};
