﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDParameterEntityDeclaration
struct DTDParameterEntityDeclaration_t4_100;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDParameterEntityDeclaration::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDParameterEntityDeclaration__ctor_m4_253 (DTDParameterEntityDeclaration_t4_100 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
