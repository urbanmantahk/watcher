﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.InternalRemotingServices
struct InternalRemotingServices_t1_1015;
// System.String
struct String_t;
// System.Runtime.Remoting.Metadata.SoapAttribute
struct SoapAttribute_t1_997;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.MethodCall
struct MethodCall_t1_931;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.InternalRemotingServices::.ctor()
extern "C" void InternalRemotingServices__ctor_m1_9073 (InternalRemotingServices_t1_1015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.InternalRemotingServices::.cctor()
extern "C" void InternalRemotingServices__cctor_m1_9074 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.InternalRemotingServices::DebugOutChnl(System.String)
extern "C" void InternalRemotingServices_DebugOutChnl_m1_9075 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.SoapAttribute System.Runtime.Remoting.InternalRemotingServices::GetCachedSoapAttribute(System.Object)
extern "C" SoapAttribute_t1_997 * InternalRemotingServices_GetCachedSoapAttribute_m1_9076 (Object_t * __this /* static, unused */, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.InternalRemotingServices::RemotingAssert(System.Boolean,System.String)
extern "C" void InternalRemotingServices_RemotingAssert_m1_9077 (Object_t * __this /* static, unused */, bool ___condition, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.InternalRemotingServices::RemotingTrace(System.Object[])
extern "C" void InternalRemotingServices_RemotingTrace_m1_9078 (Object_t * __this /* static, unused */, ObjectU5BU5D_t1_272* ___messages, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.InternalRemotingServices::SetServerIdentity(System.Runtime.Remoting.Messaging.MethodCall,System.Object)
extern "C" void InternalRemotingServices_SetServerIdentity_m1_9079 (Object_t * __this /* static, unused */, MethodCall_t1_931 * ___m, Object_t * ___srvID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
