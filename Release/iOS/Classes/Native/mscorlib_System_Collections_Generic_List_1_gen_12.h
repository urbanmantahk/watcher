﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1_1802;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<System.Byte[]>
struct  List_1_t1_1821  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	ByteU5BU5DU5BU5D_t1_1802* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1821_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	ByteU5BU5DU5BU5D_t1_1802* ___EmptyArray_4;
};
