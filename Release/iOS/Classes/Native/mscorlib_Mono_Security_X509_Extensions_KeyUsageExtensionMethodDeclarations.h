﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.Extensions.KeyUsageExtension
struct KeyUsageExtension_t1_180;
// Mono.Security.ASN1
struct ASN1_t1_149;
// Mono.Security.X509.X509Extension
struct X509Extension_t1_178;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_Mono_Security_X509_Extensions_KeyUsages.h"

// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::.ctor(Mono.Security.ASN1)
extern "C" void KeyUsageExtension__ctor_m1_2104 (KeyUsageExtension_t1_180 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::.ctor(Mono.Security.X509.X509Extension)
extern "C" void KeyUsageExtension__ctor_m1_2105 (KeyUsageExtension_t1_180 * __this, X509Extension_t1_178 * ___extension, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::.ctor()
extern "C" void KeyUsageExtension__ctor_m1_2106 (KeyUsageExtension_t1_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::Decode()
extern "C" void KeyUsageExtension_Decode_m1_2107 (KeyUsageExtension_t1_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::Encode()
extern "C" void KeyUsageExtension_Encode_m1_2108 (KeyUsageExtension_t1_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.Extensions.KeyUsages Mono.Security.X509.Extensions.KeyUsageExtension::get_KeyUsage()
extern "C" int32_t KeyUsageExtension_get_KeyUsage_m1_2109 (KeyUsageExtension_t1_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.KeyUsageExtension::set_KeyUsage(Mono.Security.X509.Extensions.KeyUsages)
extern "C" void KeyUsageExtension_set_KeyUsage_m1_2110 (KeyUsageExtension_t1_180 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.KeyUsageExtension::get_Name()
extern "C" String_t* KeyUsageExtension_get_Name_m1_2111 (KeyUsageExtension_t1_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.Extensions.KeyUsageExtension::Support(Mono.Security.X509.Extensions.KeyUsages)
extern "C" bool KeyUsageExtension_Support_m1_2112 (KeyUsageExtension_t1_180 * __this, int32_t ___usage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.KeyUsageExtension::ToString()
extern "C" String_t* KeyUsageExtension_ToString_m1_2113 (KeyUsageExtension_t1_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
