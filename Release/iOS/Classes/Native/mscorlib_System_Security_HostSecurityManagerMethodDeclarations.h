﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.HostSecurityManager
struct HostSecurityManager_t1_1389;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t1_1333;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.TrustManagerContext
struct TrustManagerContext_t1_1365;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Security.PermissionSet
struct PermissionSet_t1_563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_HostSecurityManagerOptions.h"

// System.Void System.Security.HostSecurityManager::.ctor()
extern "C" void HostSecurityManager__ctor_m1_11934 (HostSecurityManager_t1_1389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyLevel System.Security.HostSecurityManager::get_DomainPolicy()
extern "C" PolicyLevel_t1_1357 * HostSecurityManager_get_DomainPolicy_m1_11935 (HostSecurityManager_t1_1389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.HostSecurityManagerOptions System.Security.HostSecurityManager::get_Flags()
extern "C" int32_t HostSecurityManager_get_Flags_m1_11936 (HostSecurityManager_t1_1389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.HostSecurityManager::DetermineApplicationTrust(System.Security.Policy.Evidence,System.Security.Policy.Evidence,System.Security.Policy.TrustManagerContext)
extern "C" ApplicationTrust_t1_1333 * HostSecurityManager_DetermineApplicationTrust_m1_11937 (HostSecurityManager_t1_1389 * __this, Evidence_t1_398 * ___applicationEvidence, Evidence_t1_398 * ___activatorEvidence, TrustManagerContext_t1_1365 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Security.HostSecurityManager::ProvideAppDomainEvidence(System.Security.Policy.Evidence)
extern "C" Evidence_t1_398 * HostSecurityManager_ProvideAppDomainEvidence_m1_11938 (HostSecurityManager_t1_1389 * __this, Evidence_t1_398 * ___inputEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Security.HostSecurityManager::ProvideAssemblyEvidence(System.Reflection.Assembly,System.Security.Policy.Evidence)
extern "C" Evidence_t1_398 * HostSecurityManager_ProvideAssemblyEvidence_m1_11939 (HostSecurityManager_t1_1389 * __this, Assembly_t1_467 * ___loadedAssembly, Evidence_t1_398 * ___inputEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.HostSecurityManager::ResolvePolicy(System.Security.Policy.Evidence)
extern "C" PermissionSet_t1_563 * HostSecurityManager_ResolvePolicy_m1_11940 (HostSecurityManager_t1_1389 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
