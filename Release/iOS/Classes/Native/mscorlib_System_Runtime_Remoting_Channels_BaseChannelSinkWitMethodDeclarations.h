﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.BaseChannelSinkWithProperties
struct BaseChannelSinkWithProperties_t1_865;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.BaseChannelSinkWithProperties::.ctor()
extern "C" void BaseChannelSinkWithProperties__ctor_m1_8025 (BaseChannelSinkWithProperties_t1_865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
