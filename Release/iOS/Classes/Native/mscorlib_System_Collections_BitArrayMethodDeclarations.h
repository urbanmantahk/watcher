﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.BitArray
struct BitArray_t1_274;
// System.Boolean[]
struct BooleanU5BU5D_t1_458;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.BitArray::.ctor(System.Collections.BitArray)
extern "C" void BitArray__ctor_m1_3117 (BitArray_t1_274 * __this, BitArray_t1_274 * ___bits, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Boolean[])
extern "C" void BitArray__ctor_m1_3118 (BitArray_t1_274 * __this, BooleanU5BU5D_t1_458* ___values, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Byte[])
extern "C" void BitArray__ctor_m1_3119 (BitArray_t1_274 * __this, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Int32[])
extern "C" void BitArray__ctor_m1_3120 (BitArray_t1_274 * __this, Int32U5BU5D_t1_275* ___values, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Int32)
extern "C" void BitArray__ctor_m1_3121 (BitArray_t1_274 * __this, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Int32,System.Boolean)
extern "C" void BitArray__ctor_m1_3122 (BitArray_t1_274 * __this, int32_t ___length, bool ___defaultValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Int32[],System.Int32)
extern "C" void BitArray__ctor_m1_3123 (BitArray_t1_274 * __this, Int32U5BU5D_t1_275* ___array, int32_t ___length, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Collections.BitArray::getByte(System.Int32)
extern "C" uint8_t BitArray_getByte_m1_3124 (BitArray_t1_274 * __this, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::setByte(System.Int32,System.Byte)
extern "C" void BitArray_setByte_m1_3125 (BitArray_t1_274 * __this, int32_t ___byteIndex, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::checkOperand(System.Collections.BitArray)
extern "C" void BitArray_checkOperand_m1_3126 (BitArray_t1_274 * __this, BitArray_t1_274 * ___operand, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Count()
extern "C" int32_t BitArray_get_Count_m1_3127 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_IsReadOnly()
extern "C" bool BitArray_get_IsReadOnly_m1_3128 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_IsSynchronized()
extern "C" bool BitArray_get_IsSynchronized_m1_3129 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_Item(System.Int32)
extern "C" bool BitArray_get_Item_m1_3130 (BitArray_t1_274 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::set_Item(System.Int32,System.Boolean)
extern "C" void BitArray_set_Item_m1_3131 (BitArray_t1_274 * __this, int32_t ___index, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Length()
extern "C" int32_t BitArray_get_Length_m1_3132 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::set_Length(System.Int32)
extern "C" void BitArray_set_Length_m1_3133 (BitArray_t1_274 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray::get_SyncRoot()
extern "C" Object_t * BitArray_get_SyncRoot_m1_3134 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray::Clone()
extern "C" Object_t * BitArray_Clone_m1_3135 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::CopyTo(System.Array,System.Int32)
extern "C" void BitArray_CopyTo_m1_3136 (BitArray_t1_274 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.BitArray System.Collections.BitArray::Not()
extern "C" BitArray_t1_274 * BitArray_Not_m1_3137 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.BitArray System.Collections.BitArray::And(System.Collections.BitArray)
extern "C" BitArray_t1_274 * BitArray_And_m1_3138 (BitArray_t1_274 * __this, BitArray_t1_274 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.BitArray System.Collections.BitArray::Or(System.Collections.BitArray)
extern "C" BitArray_t1_274 * BitArray_Or_m1_3139 (BitArray_t1_274 * __this, BitArray_t1_274 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.BitArray System.Collections.BitArray::Xor(System.Collections.BitArray)
extern "C" BitArray_t1_274 * BitArray_Xor_m1_3140 (BitArray_t1_274 * __this, BitArray_t1_274 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::Get(System.Int32)
extern "C" bool BitArray_Get_m1_3141 (BitArray_t1_274 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::Set(System.Int32,System.Boolean)
extern "C" void BitArray_Set_m1_3142 (BitArray_t1_274 * __this, int32_t ___index, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::SetAll(System.Boolean)
extern "C" void BitArray_SetAll_m1_3143 (BitArray_t1_274 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.BitArray::GetEnumerator()
extern "C" Object_t * BitArray_GetEnumerator_m1_3144 (BitArray_t1_274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
