﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Resources_UltimateResourceFallbackLocation.h"

// System.Resources.UltimateResourceFallbackLocation
struct  UltimateResourceFallbackLocation_t1_661 
{
	// System.Int32 System.Resources.UltimateResourceFallbackLocation::value__
	int32_t ___value___1;
};
