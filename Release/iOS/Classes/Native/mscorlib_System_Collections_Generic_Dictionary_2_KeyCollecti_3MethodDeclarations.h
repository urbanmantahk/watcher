﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1_2018;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_3.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_15984_gshared (Enumerator_t1_2021 * __this, Dictionary_2_t1_2018 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_15984(__this, ___host, method) (( void (*) (Enumerator_t1_2021 *, Dictionary_2_t1_2018 *, const MethodInfo*))Enumerator__ctor_m1_15984_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_15985_gshared (Enumerator_t1_2021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15985(__this, method) (( Object_t * (*) (Enumerator_t1_2021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15985_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_15986_gshared (Enumerator_t1_2021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15986(__this, method) (( void (*) (Enumerator_t1_2021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15986_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_15987_gshared (Enumerator_t1_2021 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_15987(__this, method) (( void (*) (Enumerator_t1_2021 *, const MethodInfo*))Enumerator_Dispose_m1_15987_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_15988_gshared (Enumerator_t1_2021 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_15988(__this, method) (( bool (*) (Enumerator_t1_2021 *, const MethodInfo*))Enumerator_MoveNext_m1_15988_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_15989_gshared (Enumerator_t1_2021 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_15989(__this, method) (( Object_t * (*) (Enumerator_t1_2021 *, const MethodInfo*))Enumerator_get_Current_m1_15989_gshared)(__this, method)
