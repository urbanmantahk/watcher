﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;
// System.Security.AccessControl.SystemAcl
struct SystemAcl_t1_1133;
// System.Security.AccessControl.DiscretionaryAcl
struct DiscretionaryAcl_t1_1134;

#include "mscorlib_System_Security_AccessControl_GenericSecurityDescri.h"
#include "mscorlib_System_Security_AccessControl_ControlFlags.h"

// System.Security.AccessControl.CommonSecurityDescriptor
struct  CommonSecurityDescriptor_t1_1130  : public GenericSecurityDescriptor_t1_1131
{
	// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::isContainer
	bool ___isContainer_0;
	// System.Boolean System.Security.AccessControl.CommonSecurityDescriptor::isDS
	bool ___isDS_1;
	// System.Security.AccessControl.ControlFlags System.Security.AccessControl.CommonSecurityDescriptor::flags
	int32_t ___flags_2;
	// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.CommonSecurityDescriptor::owner
	SecurityIdentifier_t1_1132 * ___owner_3;
	// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.CommonSecurityDescriptor::group
	SecurityIdentifier_t1_1132 * ___group_4;
	// System.Security.AccessControl.SystemAcl System.Security.AccessControl.CommonSecurityDescriptor::systemAcl
	SystemAcl_t1_1133 * ___systemAcl_5;
	// System.Security.AccessControl.DiscretionaryAcl System.Security.AccessControl.CommonSecurityDescriptor::discretionaryAcl
	DiscretionaryAcl_t1_1134 * ___discretionaryAcl_6;
};
