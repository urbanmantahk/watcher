﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IDLFLAG.h"

// System.Runtime.InteropServices.ComTypes.IDLDESC
struct  IDLDESC_t1_727 
{
	// System.IntPtr System.Runtime.InteropServices.ComTypes.IDLDESC::dwReserved
	IntPtr_t ___dwReserved_0;
	// System.Runtime.InteropServices.ComTypes.IDLFLAG System.Runtime.InteropServices.ComTypes.IDLDESC::wIDLFlags
	int32_t ___wIDLFlags_1;
};
// Native definition for marshalling of: System.Runtime.InteropServices.ComTypes.IDLDESC
struct IDLDESC_t1_727_marshaled
{
	intptr_t ___dwReserved_0;
	int32_t ___wIDLFlags_1;
};
