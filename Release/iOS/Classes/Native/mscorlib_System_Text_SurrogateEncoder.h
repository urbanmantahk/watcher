﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t1_406;
// System.Text.Encoder
struct Encoder_t1_1428;

#include "mscorlib_System_Object.h"

// System.Text.SurrogateEncoder
struct  SurrogateEncoder_t1_1444  : public Object_t
{
	// System.Text.Encoding System.Text.SurrogateEncoder::encoding
	Encoding_t1_406 * ___encoding_0;
	// System.Text.Encoder System.Text.SurrogateEncoder::realObject
	Encoder_t1_1428 * ___realObject_1;
};
