﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdShort
struct XsdShort_t4_25;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.Schema.XsdShort::.ctor()
extern "C" void XsdShort__ctor_m4_40 (XsdShort_t4_25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
