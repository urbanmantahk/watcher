﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NotificationService
struct NotificationService_t8_261;
// VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion
struct RegisterForRemoteNotificationCompletion_t8_257;
// VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse
struct ReceivedNotificationResponse_t8_258;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// VoxelBusters.NativePlugins.NotificationServiceSettings
struct NotificationServiceSettings_t8_270;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"

// System.Void VoxelBusters.NativePlugins.NotificationService::.ctor()
extern "C" void NotificationService__ctor_m8_1437 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidFinishRegisterForRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion)
extern "C" void NotificationService_add_DidFinishRegisterForRemoteNotificationEvent_m8_1438 (Object_t * __this /* static, unused */, RegisterForRemoteNotificationCompletion_t8_257 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidFinishRegisterForRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/RegisterForRemoteNotificationCompletion)
extern "C" void NotificationService_remove_DidFinishRegisterForRemoteNotificationEvent_m8_1439 (Object_t * __this /* static, unused */, RegisterForRemoteNotificationCompletion_t8_257 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidLaunchWithLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_add_DidLaunchWithLocalNotificationEvent_m8_1440 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidLaunchWithLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_remove_DidLaunchWithLocalNotificationEvent_m8_1441 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidLaunchWithRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_add_DidLaunchWithRemoteNotificationEvent_m8_1442 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidLaunchWithRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_remove_DidLaunchWithRemoteNotificationEvent_m8_1443 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_add_DidReceiveLocalNotificationEvent_m8_1444 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_remove_DidReceiveLocalNotificationEvent_m8_1445 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::add_DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_add_DidReceiveRemoteNotificationEvent_m8_1446 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::remove_DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.NotificationService/ReceivedNotificationResponse)
extern "C" void NotificationService_remove_DidReceiveRemoteNotificationEvent_m8_1447 (Object_t * __this /* static, unused */, ReceivedNotificationResponse_t8_258 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveAppLaunchInfo(System.String)
extern "C" void NotificationService_DidReceiveAppLaunchInfo_m8_1448 (NotificationService_t8_261 * __this, String_t* ____launchData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveAppLaunchInfo(VoxelBusters.NativePlugins.CrossPlatformNotification,VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationService_DidReceiveAppLaunchInfo_m8_1449 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____launchLocalNotification, CrossPlatformNotification_t8_259 * ____launchRemoteNotification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveLocalNotification(System.String)
extern "C" void NotificationService_DidReceiveLocalNotification_m8_1450 (NotificationService_t8_261 * __this, String_t* ____notificationPayload, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationService_DidReceiveLocalNotification_m8_1451 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidRegisterRemoteNotification(System.String)
extern "C" void NotificationService_DidRegisterRemoteNotification_m8_1452 (NotificationService_t8_261 * __this, String_t* ____deviceToken, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidFailToRegisterRemoteNotifications(System.String)
extern "C" void NotificationService_DidFailToRegisterRemoteNotifications_m8_1453 (NotificationService_t8_261 * __this, String_t* ____errorDescription, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveRemoteNotification(System.String)
extern "C" void NotificationService_DidReceiveRemoteNotification_m8_1454 (NotificationService_t8_261 * __this, String_t* ____notificationPayload, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::DidReceiveRemoteNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationService_DidReceiveRemoteNotification_m8_1455 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::ParseAppLaunchInfo(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&,VoxelBusters.NativePlugins.CrossPlatformNotification&)
extern "C" void NotificationService_ParseAppLaunchInfo_m8_1456 (NotificationService_t8_261 * __this, String_t* ____launchData, CrossPlatformNotification_t8_259 ** ____launchLocalNotification, CrossPlatformNotification_t8_259 ** ____launchRemoteNotification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::ParseNotificationPayloadData(System.String,VoxelBusters.NativePlugins.CrossPlatformNotification&)
extern "C" void NotificationService_ParseNotificationPayloadData_m8_1457 (NotificationService_t8_261 * __this, String_t* ____payload, CrossPlatformNotification_t8_259 ** ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::Awake()
extern "C" void NotificationService_Awake_m8_1458 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::Start()
extern "C" void NotificationService_Start_m8_1459 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::Initialise(VoxelBusters.NativePlugins.NotificationServiceSettings)
extern "C" void NotificationService_Initialise_m8_1460 (NotificationService_t8_261 * __this, NotificationServiceSettings_t8_270 * ____settings, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator VoxelBusters.NativePlugins.NotificationService::ProcessAppLaunchInfo()
extern "C" Object_t * NotificationService_ProcessAppLaunchInfo_m8_1461 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType)
extern "C" void NotificationService_RegisterNotificationTypes_m8_1462 (NotificationService_t8_261 * __this, int32_t ____notificationTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NotificationService::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* NotificationService_ScheduleLocalNotification_m8_1463 (NotificationService_t8_261 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::CancelLocalNotification(System.String)
extern "C" void NotificationService_CancelLocalNotification_m8_1464 (NotificationService_t8_261 * __this, String_t* ____notificationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::CancelAllLocalNotification()
extern "C" void NotificationService_CancelAllLocalNotification_m8_1465 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::ClearNotifications()
extern "C" void NotificationService_ClearNotifications_m8_1466 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::RegisterForRemoteNotifications()
extern "C" void NotificationService_RegisterForRemoteNotifications_m8_1467 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NotificationService::UnregisterForRemoteNotifications()
extern "C" void NotificationService_UnregisterForRemoteNotifications_m8_1468 (NotificationService_t8_261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
