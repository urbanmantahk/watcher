﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Microsoft.Win32.SafeHandles.SafeHandleMinusOneIsInvalid
struct SafeHandleMinusOneIsInvalid_t1_87;

#include "codegen/il2cpp-codegen.h"

// System.Void Microsoft.Win32.SafeHandles.SafeHandleMinusOneIsInvalid::.ctor(System.Boolean)
extern "C" void SafeHandleMinusOneIsInvalid__ctor_m1_1421 (SafeHandleMinusOneIsInvalid_t1_87 * __this, bool ___ownsHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.SafeHandleMinusOneIsInvalid::get_IsInvalid()
extern "C" bool SafeHandleMinusOneIsInvalid_get_IsInvalid_m1_1422 (SafeHandleMinusOneIsInvalid_t1_87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
