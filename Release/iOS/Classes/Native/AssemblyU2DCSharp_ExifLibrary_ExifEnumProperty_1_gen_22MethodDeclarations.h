﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>
struct ExifEnumProperty_1_t8_361;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRef.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2279_gshared (ExifEnumProperty_1_t8_361 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2279(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_361 *, int32_t, uint8_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2279_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1998_gshared (ExifEnumProperty_1_t8_361 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1998(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_361 *, int32_t, uint8_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1998_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2280_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2280(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_361 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2280_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2281_gshared (ExifEnumProperty_1_t8_361 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2281(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_361 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2281_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2282_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2282(__this, method) (( uint8_t (*) (ExifEnumProperty_1_t8_361 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2282_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2283_gshared (ExifEnumProperty_1_t8_361 * __this, uint8_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2283(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_361 *, uint8_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2283_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2284_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2284(__this, method) (( bool (*) (ExifEnumProperty_1_t8_361 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2284_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2285_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2285(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_361 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2285_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2286_gshared (ExifEnumProperty_1_t8_361 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2286(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_361 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2286_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2287_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_361 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2287(__this /* static, unused */, ___obj, method) (( uint8_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_361 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2287_gshared)(__this /* static, unused */, ___obj, method)
