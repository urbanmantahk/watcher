﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.UrlMembershipCondition
struct UrlMembershipCondition_t1_1369;
// System.String
struct String_t;
// System.Security.Policy.Url
struct Url_t1_1368;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.UrlMembershipCondition::.ctor(System.String)
extern "C" void UrlMembershipCondition__ctor_m1_11746 (UrlMembershipCondition_t1_1369 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.UrlMembershipCondition::.ctor(System.Security.Policy.Url,System.String)
extern "C" void UrlMembershipCondition__ctor_m1_11747 (UrlMembershipCondition_t1_1369 * __this, Url_t1_1368 * ___url, String_t* ___userUrl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.UrlMembershipCondition::get_Url()
extern "C" String_t* UrlMembershipCondition_get_Url_m1_11748 (UrlMembershipCondition_t1_1369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.UrlMembershipCondition::set_Url(System.String)
extern "C" void UrlMembershipCondition_set_Url_m1_11749 (UrlMembershipCondition_t1_1369 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.UrlMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool UrlMembershipCondition_Check_m1_11750 (UrlMembershipCondition_t1_1369 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.UrlMembershipCondition::Copy()
extern "C" Object_t * UrlMembershipCondition_Copy_m1_11751 (UrlMembershipCondition_t1_1369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.UrlMembershipCondition::Equals(System.Object)
extern "C" bool UrlMembershipCondition_Equals_m1_11752 (UrlMembershipCondition_t1_1369 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.UrlMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void UrlMembershipCondition_FromXml_m1_11753 (UrlMembershipCondition_t1_1369 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.UrlMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void UrlMembershipCondition_FromXml_m1_11754 (UrlMembershipCondition_t1_1369 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.UrlMembershipCondition::GetHashCode()
extern "C" int32_t UrlMembershipCondition_GetHashCode_m1_11755 (UrlMembershipCondition_t1_1369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.UrlMembershipCondition::ToString()
extern "C" String_t* UrlMembershipCondition_ToString_m1_11756 (UrlMembershipCondition_t1_1369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.UrlMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * UrlMembershipCondition_ToXml_m1_11757 (UrlMembershipCondition_t1_1369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.UrlMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * UrlMembershipCondition_ToXml_m1_11758 (UrlMembershipCondition_t1_1369 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.UrlMembershipCondition::CheckUrl(System.String)
extern "C" void UrlMembershipCondition_CheckUrl_m1_11759 (UrlMembershipCondition_t1_1369 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
