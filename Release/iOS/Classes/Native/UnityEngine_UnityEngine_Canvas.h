﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t6_155;

#include "UnityEngine_UnityEngine_Behaviour.h"

// UnityEngine.Canvas
struct  Canvas_t6_156  : public Behaviour_t6_30
{
};
struct Canvas_t6_156_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t6_155 * ___willRenderCanvases_2;
};
