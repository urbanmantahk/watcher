﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1_2221;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Collections.Generic.IDictionary`2<System.Object,System.Boolean>
struct IDictionary_2_t1_2788;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.Generic.ICollection`1<System.Boolean>
struct ICollection_1_t1_2789;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1_2787;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t1_2790;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t1_2225;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t1_2229;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m1_18234_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18234(__this, method) (( void (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2__ctor_m1_18234_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_18235_gshared (Dictionary_2_t1_2221 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18235(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_18235_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_18237_gshared (Dictionary_2_t1_2221 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18237(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_18237_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_18239_gshared (Dictionary_2_t1_2221 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18239(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_2221 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_18239_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_18241_gshared (Dictionary_2_t1_2221 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18241(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_18241_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_18243_gshared (Dictionary_2_t1_2221 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18243(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_2221 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_18243_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_18245_gshared (Dictionary_2_t1_2221 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_18245(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2221 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_18245_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_18247_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_18247(__this, method) (( Object_t* (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_18247_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_18249_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_18249(__this, method) (( Object_t* (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_18249_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_18251_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_18251(__this, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_18251_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_18253_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_18253(__this, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_18253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_18255_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_18255(__this, method) (( bool (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_18255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_18257_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_18257(__this, method) (( bool (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_18257_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_18259_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_18259(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_18259_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_18261_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_18261(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_18261_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_18263_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_18263(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_18263_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_18265_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_18265(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_18265_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_18267_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_18267(__this, ___key, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_18267_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_18269_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_18269(__this, method) (( bool (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_18269_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_18271_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_18271(__this, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_18271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_18273_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_18273(__this, method) (( bool (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_18273_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_18275_gshared (Dictionary_2_t1_2221 * __this, KeyValuePair_2_t1_2223  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_18275(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_2221 *, KeyValuePair_2_t1_2223 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_18275_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_18277_gshared (Dictionary_2_t1_2221 * __this, KeyValuePair_2_t1_2223  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_18277(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2221 *, KeyValuePair_2_t1_2223 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_18277_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_18279_gshared (Dictionary_2_t1_2221 * __this, KeyValuePair_2U5BU5D_t1_2787* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_18279(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2221 *, KeyValuePair_2U5BU5D_t1_2787*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_18279_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_18281_gshared (Dictionary_2_t1_2221 * __this, KeyValuePair_2_t1_2223  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_18281(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2221 *, KeyValuePair_2_t1_2223 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_18281_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_18283_gshared (Dictionary_2_t1_2221 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_18283(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2221 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_18283_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_18285_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_18285(__this, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_18285_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_18287_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_18287(__this, method) (( Object_t* (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_18287_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_18289_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_18289(__this, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_18289_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_18291_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_18291(__this, method) (( int32_t (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_get_Count_m1_18291_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m1_18293_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_18293(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m1_18293_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_18295_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_18295(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m1_18295_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_18297_gshared (Dictionary_2_t1_2221 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_18297(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_2221 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_18297_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_18299_gshared (Dictionary_2_t1_2221 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_18299(__this, ___size, method) (( void (*) (Dictionary_2_t1_2221 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_18299_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_18301_gshared (Dictionary_2_t1_2221 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_18301(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2221 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_18301_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2223  Dictionary_2_make_pair_m1_18303_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_18303(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_2223  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m1_18303_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m1_18305_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_18305(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_key_m1_18305_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m1_18307_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_18307(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m1_18307_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_18309_gshared (Dictionary_2_t1_2221 * __this, KeyValuePair_2U5BU5D_t1_2787* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_18309(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2221 *, KeyValuePair_2U5BU5D_t1_2787*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_18309_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m1_18311_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_18311(__this, method) (( void (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_Resize_m1_18311_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_18313_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_18313(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m1_18313_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_18315_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_18315(__this, method) (( Object_t* (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_18315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m1_18317_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_18317(__this, method) (( void (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_Clear_m1_18317_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_18319_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_18319(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m1_18319_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_18321_gshared (Dictionary_2_t1_2221 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_18321(__this, ___value, method) (( bool (*) (Dictionary_2_t1_2221 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m1_18321_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_18323_gshared (Dictionary_2_t1_2221 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_18323(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2221 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_18323_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_18325_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_18325(__this, ___sender, method) (( void (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_18325_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_18327_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_18327(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m1_18327_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_18329_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_18329(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_2221 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m1_18329_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Keys()
extern "C" KeyCollection_t1_2225 * Dictionary_2_get_Keys_m1_18331_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_18331(__this, method) (( KeyCollection_t1_2225 * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_get_Keys_m1_18331_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t1_2229 * Dictionary_2_get_Values_m1_18333_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_18333(__this, method) (( ValueCollection_t1_2229 * (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_get_Values_m1_18333_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m1_18335_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_18335(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_18335_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m1_18337_gshared (Dictionary_2_t1_2221 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_18337(__this, ___value, method) (( bool (*) (Dictionary_2_t1_2221 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_18337_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_18339_gshared (Dictionary_2_t1_2221 * __this, KeyValuePair_2_t1_2223  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_18339(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_2221 *, KeyValuePair_2_t1_2223 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_18339_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t1_2227  Dictionary_2_GetEnumerator_m1_18341_gshared (Dictionary_2_t1_2221 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_18341(__this, method) (( Enumerator_t1_2227  (*) (Dictionary_2_t1_2221 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_18341_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_18343_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_18343(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_18343_gshared)(__this /* static, unused */, ___key, ___value, method)
