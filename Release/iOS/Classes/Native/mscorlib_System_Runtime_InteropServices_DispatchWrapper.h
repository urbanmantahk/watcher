﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Runtime.InteropServices.DispatchWrapper
struct  DispatchWrapper_t1_781  : public Object_t
{
	// System.Object System.Runtime.InteropServices.DispatchWrapper::wrappedObject
	Object_t * ___wrappedObject_0;
};
