﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Advertisements.UnityAdsInternal
struct UnityAdsInternal_t6_105;
// UnityEngine.Advertisements.UnityAdsDelegate
struct UnityAdsDelegate_t6_106;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
struct UnityAdsDelegate_2_t6_107;
// System.String
struct String_t;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t6_283;
// UnityEngine.Collider
struct Collider_t6_111;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t6_284;
// UnityEngine.Collider2D
struct Collider2D_t6_119;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t6_120;
// UnityEngine.Transform
struct Transform_t6_65;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t6_124;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t6_126;
// System.Single[]
struct SingleU5BU5D_t1_1695;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t6_127;
// UnityEngine.AudioClip
struct AudioClip_t6_128;
// UnityEngine.AnimationEvent
struct AnimationEvent_t6_131;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// UnityEngine.AnimationState
struct AnimationState_t6_132;
// UnityEngine.AnimationCurve
struct AnimationCurve_t6_136;
struct AnimationCurve_t6_136_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t6_285;
// UnityEngine.Animator
struct Animator_t6_139;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t6_143;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t6_148;
// UnityEngine.Font
struct Font_t6_149;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Action`1<UnityEngine.Font>
struct Action_1_t1_1823;
// UnityEngine.Material
struct Material_t6_72;
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t6_286;
// UnityEngine.TextGenerator
struct TextGenerator_t6_152;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t6_287;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t6_288;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_289;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1_1825;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1_1826;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1_1824;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t1_1841;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1_1842;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t1_1843;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t6_155;
// UnityEngine.Canvas
struct Canvas_t6_156;
// UnityEngine.Camera
struct Camera_t6_86;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6_157;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t6_159;
// UnityEngine.Texture
struct Texture_t6_32;
// UnityEngine.Mesh
struct Mesh_t6_23;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1_1834;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1_1837;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1_1836;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1_1835;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1_1838;
// UnityEngine.RectTransform
struct RectTransform_t6_64;
// UnityEngine.Event
struct Event_t6_162;
struct Event_t6_162_marshaled;
// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t6_166;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t6_167;
// UnityEngine.GUISkin
struct GUISkin_t6_169;
// UnityEngine.GUIStyle
struct GUIStyle_t6_176;
// UnityEngine.GUIContent
struct GUIContent_t6_171;
// UnityEngine.TextEditor
struct TextEditor_t6_254;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_290;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t6_182;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t6_173;
// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_t6_174;
// System.Type
struct Type_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t6_177;
// UnityEngine.RectOffset
struct RectOffset_t6_178;
// UnityEngine.GUIScrollGroup
struct GUIScrollGroup_t6_179;
// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t6_180;
// UnityEngine.GUISettings
struct GUISettings_t6_183;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t6_184;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t6_185;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// UnityEngine.GUIStyleState
struct GUIStyleState_t6_186;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.Exception
struct Exception_t1_33;
// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t6_195;
// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t6_196;
// System.Type[]
struct TypeU5BU5D_t1_31;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t6_201;
// UnityEngine.RequireComponent
struct RequireComponent_t6_202;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t6_203;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t6_204;
// UnityEngine.SetupCoroutine
struct SetupCoroutine_t6_205;
// UnityEngine.WritableAttribute
struct WritableAttribute_t6_206;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t6_207;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t6_221;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t6_19;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t6_224;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t6_223;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t6_225;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternal.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInternalMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_ge.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_geMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Particle.h"
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Collision.h"
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteractionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D.h"
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_14.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D.h"
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
#include "UnityEngine_UnityEngine_ContactPoint2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D.h"
#include "UnityEngine_UnityEngine_Collision2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_AudioSettings.h"
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip.h"
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEvent.h"
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
#include "UnityEngine_UnityEngine_SkeletonBone.h"
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_HumanLimit.h"
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBone.h"
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen_4.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Action_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerator.h"
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_15MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_16MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_17MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_15.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_16.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_17.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas.h"
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_CanvasGroup.h"
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "mscorlib_System_MathMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_7.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_10.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_8.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11.h"
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event.h"
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_GUISkin.h"
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle.h"
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent.h"
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FocusType.h"
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISettings.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#include "UnityEngine_UnityEngine_SliderHandlerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderHandler.h"
#include "mscorlib_System_Double.h"
#include "UnityEngine_UnityEngine_GUIClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
#include "mscorlib_System_Collections_Stack.h"
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset.h"
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
#include "UnityEngine_UnityEngine_GUILayout.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_2.h"
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "mscorlib_System_StringComparerMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
#include "mscorlib_System_StringComparer.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_3.h"
#include "UnityEngine_UnityEngine_GUIStyleState.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollecMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_0.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ImagePosition.h"
#include "UnityEngine_UnityEngine_ImagePositionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelectionArgume.h"
#include "UnityEngine_UnityEngine_ExitGUIException.h"
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_FocusTypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility.h"
#include "UnityEngine_UnityEngine_GUIStateObjectsMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIClip.h"
#include "UnityEngine_UnityEngine_Internal_DrawArgumentsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelectionArgumeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "mscorlib_System_Attribute.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
#include "UnityEngine_UnityEngine_RequireComponent.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen_0.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_19MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_19.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_Binder.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "UnityEngine_UnityEngine_WritableAttribute.h"
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_Resolution.h"
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat.h"
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunctionMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Advertisements.UnityAdsInternal::.ctor()
extern "C" void UnityAdsInternal__ctor_m6_810 (UnityAdsInternal_t6_105 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m6_811 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m6_812 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m6_813 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m6_814 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onShow_m6_815 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onShow_m6_816 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onHide_m6_817 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onHide_m6_818 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_2_t6_107_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onVideoCompleted_m6_819 (Object_t * __this /* static, unused */, UnityAdsDelegate_2_t6_107 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_2_t6_107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1621);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t6_107 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		UnityAdsDelegate_2_t6_107 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = ((UnityAdsDelegate_2_t6_107 *)CastclassSealed(L_2, UnityAdsDelegate_2_t6_107_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_2_t6_107_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m6_820 (Object_t * __this /* static, unused */, UnityAdsDelegate_2_t6_107 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_2_t6_107_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1621);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t6_107 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		UnityAdsDelegate_2_t6_107 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = ((UnityAdsDelegate_2_t6_107 *)CastclassSealed(L_2, UnityAdsDelegate_2_t6_107_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_add_onVideoStarted_m6_821 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern TypeInfo* UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_remove_onVideoStarted_m6_822 (Object_t * __this /* static, unused */, UnityAdsDelegate_t6_106 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1620);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		UnityAdsDelegate_t6_106 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = ((UnityAdsDelegate_t6_106 *)CastclassSealed(L_2, UnityAdsDelegate_t6_106_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
extern "C" void UnityAdsInternal_RegisterNative_m6_823 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_RegisterNative_m6_823_ftn) ();
	static UnityAdsInternal_RegisterNative_m6_823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_RegisterNative_m6_823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
extern "C" void UnityAdsInternal_Init_m6_824 (Object_t * __this /* static, unused */, String_t* ___gameId, bool ___testModeEnabled, bool ___debugModeEnabled, String_t* ___unityVersion, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_Init_m6_824_ftn) (String_t*, bool, bool, String_t*);
	static UnityAdsInternal_Init_m6_824_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Init_m6_824_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)");
	_il2cpp_icall_func(___gameId, ___testModeEnabled, ___debugModeEnabled, ___unityVersion);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
extern "C" bool UnityAdsInternal_Show_m6_825 (Object_t * __this /* static, unused */, String_t* ___zoneId, String_t* ___rewardItemKey, String_t* ___options, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_Show_m6_825_ftn) (String_t*, String_t*, String_t*);
	static UnityAdsInternal_Show_m6_825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Show_m6_825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)");
	return _il2cpp_icall_func(___zoneId, ___rewardItemKey, ___options);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
extern "C" bool UnityAdsInternal_CanShowAds_m6_826 (Object_t * __this /* static, unused */, String_t* ___zoneId, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_CanShowAds_m6_826_ftn) (String_t*);
	static UnityAdsInternal_CanShowAds_m6_826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_CanShowAds_m6_826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)");
	return _il2cpp_icall_func(___zoneId);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
extern "C" void UnityAdsInternal_SetLogLevel_m6_827 (Object_t * __this /* static, unused */, int32_t ___logLevel, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetLogLevel_m6_827_ftn) (int32_t);
	static UnityAdsInternal_SetLogLevel_m6_827_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetLogLevel_m6_827_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)");
	_il2cpp_icall_func(___logLevel);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
extern "C" void UnityAdsInternal_SetCampaignDataURL_m6_828 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetCampaignDataURL_m6_828_ftn) (String_t*);
	static UnityAdsInternal_SetCampaignDataURL_m6_828_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetCampaignDataURL_m6_828_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RemoveAllEventHandlers()
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m6_829 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0 = (UnityAdsDelegate_t6_106 *)NULL;
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1 = (UnityAdsDelegate_t6_106 *)NULL;
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onShow_2 = (UnityAdsDelegate_t6_106 *)NULL;
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onHide_3 = (UnityAdsDelegate_t6_106 *)NULL;
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4 = (UnityAdsDelegate_2_t6_107 *)NULL;
		((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5 = (UnityAdsDelegate_t6_106 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsAvailable()
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m6_830 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_106 * V_0 = {0};
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsAvailable_0;
		V_0 = L_0;
		UnityAdsDelegate_t6_106 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_106 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1827(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsFetchFailed()
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m6_831 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_106 * V_0 = {0};
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onCampaignsFetchFailed_1;
		V_0 = L_0;
		UnityAdsDelegate_t6_106 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_106 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1827(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsShow()
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsShow_m6_832 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_106 * V_0 = {0};
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onShow_2;
		V_0 = L_0;
		UnityAdsDelegate_t6_106 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_106 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1827(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsHide()
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsHide_m6_833 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_106 * V_0 = {0};
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onHide_3;
		V_0 = L_0;
		UnityAdsDelegate_t6_106 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_106 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1827(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoCompleted(System.String,System.Boolean)
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsDelegate_2_Invoke_m6_1834_MethodInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m6_834 (Object_t * __this /* static, unused */, String_t* ___rewardItemKey, bool ___skipped, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		UnityAdsDelegate_2_Invoke_m6_1834_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483791);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_2_t6_107 * V_0 = {0};
	{
		UnityAdsDelegate_2_t6_107 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoCompleted_4;
		V_0 = L_0;
		UnityAdsDelegate_2_t6_107 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		UnityAdsDelegate_2_t6_107 * L_2 = V_0;
		String_t* L_3 = ___rewardItemKey;
		bool L_4 = ___skipped;
		NullCheck(L_2);
		UnityAdsDelegate_2_Invoke_m6_1834(L_2, L_3, L_4, /*hidden argument*/UnityAdsDelegate_2_Invoke_m6_1834_MethodInfo_var);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoStarted()
extern TypeInfo* UnityAdsInternal_t6_105_il2cpp_TypeInfo_var;
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m6_835 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityAdsInternal_t6_105_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1619);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t6_106 * V_0 = {0};
	{
		UnityAdsDelegate_t6_106 * L_0 = ((UnityAdsInternal_t6_105_StaticFields*)UnityAdsInternal_t6_105_il2cpp_TypeInfo_var->static_fields)->___onVideoStarted_5;
		V_0 = L_0;
		UnityAdsDelegate_t6_106 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t6_106 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m6_1827(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t6_48  Particle_get_position_m6_836 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m6_837 (Particle_t6_108 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t6_48  Particle_get_velocity_m6_838 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m6_839 (Particle_t6_108 * __this, Vector3_t6_48  ___value, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m6_840 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m6_841 (Particle_t6_108 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m6_842 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m6_843 (Particle_t6_108 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m6_844 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m6_845 (Particle_t6_108 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m6_846 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m6_847 (Particle_t6_108 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m6_848 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m6_849 (Particle_t6_108 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t6_40  Particle_get_color_m6_850 (Particle_t6_108 * __this, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m6_851 (Particle_t6_108 * __this, Color_t6_40  ___value, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Raycast_m6_852 (Object_t * __this /* static, unused */, Vector3_t6_48  ___origin, Vector3_t6_48  ___direction, RaycastHit_t6_116 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = ___origin;
		Vector3_t6_48  L_1 = ___direction;
		RaycastHit_t6_116 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		int32_t L_5 = ___queryTriggerInteraction;
		bool L_6 = Physics_Internal_Raycast_m6_859(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m6_853 (Object_t * __this /* static, unused */, Ray_t6_55  ___ray, RaycastHit_t6_116 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		Ray_t6_55  L_0 = ___ray;
		RaycastHit_t6_116 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m6_854(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Raycast_m6_854 (Object_t * __this /* static, unused */, Ray_t6_55  ___ray, RaycastHit_t6_116 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Ray_get_origin_m6_413((&___ray), /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Ray_get_direction_m6_414((&___ray), /*hidden argument*/NULL);
		RaycastHit_t6_116 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		int32_t L_5 = ___queryTriggerInteraction;
		bool L_6 = Physics_Raycast_m6_852(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t6_283* Physics_RaycastAll_m6_855 (Object_t * __this /* static, unused */, Ray_t6_55  ___ray, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		Ray_t6_55  L_0 = ___ray;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layerMask;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t6_283* L_4 = Physics_RaycastAll_m6_856(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" RaycastHitU5BU5D_t6_283* Physics_RaycastAll_m6_856 (Object_t * __this /* static, unused */, Ray_t6_55  ___ray, float ___maxDistance, int32_t ___layerMask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Ray_get_origin_m6_413((&___ray), /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Ray_get_direction_m6_414((&___ray), /*hidden argument*/NULL);
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		int32_t L_4 = ___queryTriggerInteraction;
		RaycastHitU5BU5D_t6_283* L_5 = Physics_RaycastAll_m6_857(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" RaycastHitU5BU5D_t6_283* Physics_RaycastAll_m6_857 (Object_t * __this /* static, unused */, Vector3_t6_48  ___origin, Vector3_t6_48  ___direction, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance;
		int32_t L_1 = ___layermask;
		int32_t L_2 = ___queryTriggerInteraction;
		RaycastHitU5BU5D_t6_283* L_3 = Physics_INTERNAL_CALL_RaycastAll_m6_858(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" RaycastHitU5BU5D_t6_283* Physics_INTERNAL_CALL_RaycastAll_m6_858 (Object_t * __this /* static, unused */, Vector3_t6_48 * ___origin, Vector3_t6_48 * ___direction, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t6_283* (*Physics_INTERNAL_CALL_RaycastAll_m6_858_ftn) (Vector3_t6_48 *, Vector3_t6_48 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m6_858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m6_858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin, ___direction, ___maxDistance, ___layermask, ___queryTriggerInteraction);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_Internal_Raycast_m6_859 (Object_t * __this /* static, unused */, Vector3_t6_48  ___origin, Vector3_t6_48  ___direction, RaycastHit_t6_116 * ___hitInfo, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	{
		RaycastHit_t6_116 * L_0 = ___hitInfo;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layermask;
		int32_t L_3 = ___queryTriggerInteraction;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m6_860(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m6_860 (Object_t * __this /* static, unused */, Vector3_t6_48 * ___origin, Vector3_t6_48 * ___direction, RaycastHit_t6_116 * ___hitInfo, float ___maxDistance, int32_t ___layermask, int32_t ___queryTriggerInteraction, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m6_860_ftn) (Vector3_t6_48 *, Vector3_t6_48 *, RaycastHit_t6_116 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m6_860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m6_860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___maxDistance, ___layermask, ___queryTriggerInteraction);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t6_48  RaycastHit_get_point_m6_861 (RaycastHit_t6_116 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t6_48  RaycastHit_get_normal_m6_862 (RaycastHit_t6_116 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = (__this->___m_Normal_1);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m6_863 (RaycastHit_t6_116 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_3);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t6_111 * RaycastHit_get_collider_m6_864 (RaycastHit_t6_116 * __this, const MethodInfo* method)
{
	{
		Collider_t6_111 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// System.Void UnityEngine.Physics2D::.cctor()
extern TypeInfo* List_1_t1_1822_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t6_117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14951_MethodInfo_var;
extern "C" void Physics2D__cctor_m6_865 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1623);
		Physics2D_t6_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1624);
		List_1__ctor_m1_14951_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483792);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1822 * L_0 = (List_1_t1_1822 *)il2cpp_codegen_object_new (List_1_t1_1822_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14951(L_0, /*hidden argument*/List_1__ctor_m1_14951_MethodInfo_var);
		((Physics2D_t6_117_StaticFields*)Physics2D_t6_117_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t6_117_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m6_866 (Object_t * __this /* static, unused */, Vector2_t6_47  ___origin, Vector2_t6_47  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t6_118 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1624);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t6_118 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_117_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m6_867(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m6_867 (Object_t * __this /* static, unused */, Vector2_t6_47 * ___origin, Vector2_t6_47 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t6_118 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m6_867_ftn) (Vector2_t6_47 *, Vector2_t6_47 *, float, int32_t, float, float, RaycastHit2D_t6_118 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m6_867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m6_867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t6_117_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t6_118  Physics2D_Raycast_m6_868 (Object_t * __this /* static, unused */, Vector2_t6_47  ___origin, Vector2_t6_47  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1624);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t6_47  L_0 = ___origin;
		Vector2_t6_47  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_117_il2cpp_TypeInfo_var);
		RaycastHit2D_t6_118  L_6 = Physics2D_Raycast_m6_869(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t6_117_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t6_118  Physics2D_Raycast_m6_869 (Object_t * __this /* static, unused */, Vector2_t6_47  ___origin, Vector2_t6_47  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1624);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t6_118  V_0 = {0};
	{
		Vector2_t6_47  L_0 = ___origin;
		Vector2_t6_47  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_117_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m6_866(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t6_118  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t6_117_il2cpp_TypeInfo_var;
extern "C" RaycastHit2DU5BU5D_t6_284* Physics2D_RaycastAll_m6_870 (Object_t * __this /* static, unused */, Vector2_t6_47  ___origin, Vector2_t6_47  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t6_117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1624);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t6_117_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t6_284* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m6_871(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C" RaycastHit2DU5BU5D_t6_284* Physics2D_INTERNAL_CALL_RaycastAll_m6_871 (Object_t * __this /* static, unused */, Vector2_t6_47 * ___origin, Vector2_t6_47 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t6_284* (*Physics2D_INTERNAL_CALL_RaycastAll_m6_871_ftn) (Vector2_t6_47 *, Vector2_t6_47 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m6_871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m6_871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t6_47  RaycastHit2D_get_point_m6_872 (RaycastHit2D_t6_118 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = (__this->___m_Point_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t6_47  RaycastHit2D_get_normal_m6_873 (RaycastHit2D_t6_118 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = (__this->___m_Normal_2);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m6_874 (RaycastHit2D_t6_118 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Fraction_4);
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t6_119 * RaycastHit2D_get_collider_m6_875 (RaycastHit2D_t6_118 * __this, const MethodInfo* method)
{
	{
		Collider2D_t6_119 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t6_120 * RaycastHit2D_get_rigidbody_m6_876 (RaycastHit2D_t6_118 * __this, const MethodInfo* method)
{
	Rigidbody2D_t6_120 * G_B3_0 = {0};
	{
		Collider2D_t6_119 * L_0 = RaycastHit2D_get_collider_m6_875(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t6_119 * L_2 = RaycastHit2D_get_collider_m6_875(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t6_120 * L_3 = Collider2D_get_attachedRigidbody_m6_878(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t6_120 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t6_65 * RaycastHit2D_get_transform_m6_877 (RaycastHit2D_t6_118 * __this, const MethodInfo* method)
{
	Rigidbody2D_t6_120 * V_0 = {0};
	{
		Rigidbody2D_t6_120 * L_0 = RaycastHit2D_get_rigidbody_m6_876(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t6_120 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_1, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t6_120 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t6_65 * L_4 = Component_get_transform_m6_724(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t6_119 * L_5 = RaycastHit2D_get_collider_m6_875(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_5, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t6_119 * L_7 = RaycastHit2D_get_collider_m6_875(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t6_65 * L_8 = Component_get_transform_m6_724(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t6_65 *)NULL;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t6_120 * Collider2D_get_attachedRigidbody_m6_878 (Collider2D_t6_119 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t6_120 * (*Collider2D_get_attachedRigidbody_m6_878_ftn) (Collider2D_t6_119 *);
	static Collider2D_get_attachedRigidbody_m6_878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m6_878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void AudioConfigurationChangeHandler__ctor_m6_879 (AudioConfigurationChangeHandler_t6_124 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C" void AudioConfigurationChangeHandler_Invoke_m6_880 (AudioConfigurationChangeHandler_t6_124 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m6_880((AudioConfigurationChangeHandler_t6_124 *)__this->___prev_9,___deviceWasChanged, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t6_124(Il2CppObject* delegate, bool ___deviceWasChanged)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___deviceWasChanged' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___deviceWasChanged);

	// Marshaling cleanup of parameter '___deviceWasChanged' native representation

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t1_20_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioConfigurationChangeHandler_BeginInvoke_m6_881 (AudioConfigurationChangeHandler_t6_124 * __this, bool ___deviceWasChanged, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t1_20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t1_20_il2cpp_TypeInfo_var, &___deviceWasChanged);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m6_882 (AudioConfigurationChangeHandler_t6_124 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern TypeInfo* AudioSettings_t6_125_il2cpp_TypeInfo_var;
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m6_883 (Object_t * __this /* static, unused */, bool ___deviceWasChanged, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSettings_t6_125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1625);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t6_124 * L_0 = ((AudioSettings_t6_125_StaticFields*)AudioSettings_t6_125_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t6_124 * L_1 = ((AudioSettings_t6_125_StaticFields*)AudioSettings_t6_125_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		bool L_2 = ___deviceWasChanged;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m6_880(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMReaderCallback__ctor_m6_884 (PCMReaderCallback_t6_126 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C" void PCMReaderCallback_Invoke_m6_885 (PCMReaderCallback_t6_126 * __this, SingleU5BU5D_t1_1695* ___data, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m6_885((PCMReaderCallback_t6_126 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t1_1695* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t1_1695* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t6_126(Il2CppObject* delegate, SingleU5BU5D_t1_1695* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m6_886 (PCMReaderCallback_t6_126 * __this, SingleU5BU5D_t1_1695* ___data, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m6_887 (PCMReaderCallback_t6_126 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMSetPositionCallback__ctor_m6_888 (PCMSetPositionCallback_t6_127 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C" void PCMSetPositionCallback_Invoke_m6_889 (PCMSetPositionCallback_t6_127 * __this, int32_t ___position, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m6_889((PCMSetPositionCallback_t6_127 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t6_127(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m6_890 (PCMSetPositionCallback_t6_127 * __this, int32_t ___position, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m6_891 (PCMSetPositionCallback_t6_127 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m6_892 (AudioClip_t6_128 * __this, SingleU5BU5D_t1_1695* ___data, const MethodInfo* method)
{
	{
		PCMReaderCallback_t6_126 * L_0 = (__this->___m_PCMReaderCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t6_126 * L_1 = (__this->___m_PCMReaderCallback_2);
		SingleU5BU5D_t1_1695* L_2 = ___data;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m6_885(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m6_893 (AudioClip_t6_128 * __this, int32_t ___position, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t6_127 * L_0 = (__this->___m_PCMSetPositionCallback_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t6_127 * L_1 = (__this->___m_PCMSetPositionCallback_3);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m6_889(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m6_894 (WebCamDevice_t6_129 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m6_895 (WebCamDevice_t6_129 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t6_129_marshal(const WebCamDevice_t6_129& unmarshaled, WebCamDevice_t6_129_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
extern "C" void WebCamDevice_t6_129_marshal_back(const WebCamDevice_t6_129_marshaled& marshaled, WebCamDevice_t6_129& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t6_129_marshal_cleanup(WebCamDevice_t6_129_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AnimationEvent__ctor_m6_896 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		__this->___m_Time_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_FunctionName_1 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_StringParameter_2 = L_1;
		__this->___m_ObjectReferenceParameter_3 = (Object_t6_5 *)NULL;
		__this->___m_FloatParameter_4 = (0.0f);
		__this->___m_IntParameter_5 = 0;
		__this->___m_MessageOptions_6 = 0;
		__this->___m_Source_7 = 0;
		__this->___m_StateSender_8 = (AnimationState_t6_132 *)NULL;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m6_897 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C" void AnimationEvent_set_data_m6_898 (AnimationEvent_t6_131 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m6_899 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m6_900 (AnimationEvent_t6_131 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m6_901 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatParameter_4);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C" void AnimationEvent_set_floatParameter_m6_902 (AnimationEvent_t6_131 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FloatParameter_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m6_903 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntParameter_5);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C" void AnimationEvent_set_intParameter_m6_904 (AnimationEvent_t6_131 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_IntParameter_5 = L_0;
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t6_5 * AnimationEvent_get_objectReferenceParameter_m6_905 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = (__this->___m_ObjectReferenceParameter_3);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C" void AnimationEvent_set_objectReferenceParameter_m6_906 (AnimationEvent_t6_131 * __this, Object_t6_5 * ___value, const MethodInfo* method)
{
	{
		Object_t6_5 * L_0 = ___value;
		__this->___m_ObjectReferenceParameter_3 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m6_907 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_FunctionName_1);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m6_908 (AnimationEvent_t6_131 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_FunctionName_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m6_909 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Time_0);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m6_910 (AnimationEvent_t6_131 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Time_0 = L_0;
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m6_911 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_MessageOptions_6);
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C" void AnimationEvent_set_messageOptions_m6_912 (AnimationEvent_t6_131 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_MessageOptions_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C" bool AnimationEvent_get_isFiredByLegacy_m6_913 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C" bool AnimationEvent_get_isFiredByAnimator_m6_914 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppCodeGenString* _stringLiteral4737;
extern "C" AnimationState_t6_132 * AnimationEvent_get_animationState_m6_915 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4737 = il2cpp_codegen_string_literal_from_index(4737);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m6_913(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4737, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t6_132 * L_1 = (__this->___m_StateSender_8);
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppCodeGenString* _stringLiteral4738;
extern "C" AnimatorStateInfo_t6_133  AnimationEvent_get_animatorStateInfo_m6_916 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4738 = il2cpp_codegen_string_literal_from_index(4738);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m6_914(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4738, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t6_133  L_1 = (__this->___m_AnimatorStateInfo_9);
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppCodeGenString* _stringLiteral4739;
extern "C" AnimatorClipInfo_t6_134  AnimationEvent_get_animatorClipInfo_m6_917 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4739 = il2cpp_codegen_string_literal_from_index(4739);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m6_914(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4739, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t6_134  L_1 = (__this->___m_AnimatorClipInfo_10);
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C" int32_t AnimationEvent_GetHash_m6_918 (AnimationEvent_t6_131 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m6_907(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m1_577(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m6_909(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m1_622((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m6_919 (AnimationCurve_t6_136 * __this, KeyframeU5BU5D_t6_285* ___keys, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t6_285* L_0 = ___keys;
		AnimationCurve_Init_m6_923(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m6_920 (AnimationCurve_t6_136 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m6_923(__this, (KeyframeU5BU5D_t6_285*)(KeyframeU5BU5D_t6_285*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m6_921 (AnimationCurve_t6_136 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m6_921_ftn) (AnimationCurve_t6_136 *);
	static AnimationCurve_Cleanup_m6_921_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m6_921_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m6_922 (AnimationCurve_t6_136 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m6_921(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m6_923 (AnimationCurve_t6_136 * __this, KeyframeU5BU5D_t6_285* ___keys, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m6_923_ftn) (AnimationCurve_t6_136 *, KeyframeU5BU5D_t6_285*);
	static AnimationCurve_Init_m6_923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m6_923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t6_136_marshal(const AnimationCurve_t6_136& unmarshaled, AnimationCurve_t6_136_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AnimationCurve_t6_136_marshal_back(const AnimationCurve_t6_136_marshaled& marshaled, AnimationCurve_t6_136& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t6_136_marshal_cleanup(AnimationCurve_t6_136_marshaled& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" bool AnimatorStateInfo_IsName_m6_924 (AnimatorStateInfo_t6_133 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m6_947(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_FullPath_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Name_0);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (__this->___m_Path_1);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" int32_t AnimatorStateInfo_get_fullPathHash_m6_925 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m6_926 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" int32_t AnimatorStateInfo_get_shortNameHash_m6_927 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m6_928 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m6_929 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_4);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C" float AnimatorStateInfo_get_speed_m6_930 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Speed_5);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C" float AnimatorStateInfo_get_speedMultiplier_m6_931 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_SpeedMultiplier_6);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m6_932 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_7);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m6_933 (AnimatorStateInfo_t6_133 * __this, String_t* ___tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m6_947(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_7);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m6_934 (AnimatorStateInfo_t6_133 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_8);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" bool AnimatorTransitionInfo_IsName_m6_935 (AnimatorTransitionInfo_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m6_947(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name;
		int32_t L_4 = Animator_StringToHash_m6_947(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = (__this->___m_FullPath_0);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m6_936 (AnimatorTransitionInfo_t6_138 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m6_947(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C" int32_t AnimatorTransitionInfo_get_fullPathHash_m6_937 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m6_938 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m6_939 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m6_940 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C" bool AnimatorTransitionInfo_get_anyState_m6_941 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AnyState_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C" bool AnimatorTransitionInfo_get_entry_m6_942 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C" bool AnimatorTransitionInfo_get_exit_m6_943 (AnimatorTransitionInfo_t6_138 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t6_138_marshal(const AnimatorTransitionInfo_t6_138& unmarshaled, AnimatorTransitionInfo_t6_138_marshaled& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.___m_FullPath_0;
	marshaled.___m_UserName_1 = unmarshaled.___m_UserName_1;
	marshaled.___m_Name_2 = unmarshaled.___m_Name_2;
	marshaled.___m_NormalizedTime_3 = unmarshaled.___m_NormalizedTime_3;
	marshaled.___m_AnyState_4 = unmarshaled.___m_AnyState_4;
	marshaled.___m_TransitionType_5 = unmarshaled.___m_TransitionType_5;
}
extern "C" void AnimatorTransitionInfo_t6_138_marshal_back(const AnimatorTransitionInfo_t6_138_marshaled& marshaled, AnimatorTransitionInfo_t6_138& unmarshaled)
{
	unmarshaled.___m_FullPath_0 = marshaled.___m_FullPath_0;
	unmarshaled.___m_UserName_1 = marshaled.___m_UserName_1;
	unmarshaled.___m_Name_2 = marshaled.___m_Name_2;
	unmarshaled.___m_NormalizedTime_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.___m_AnyState_4 = marshaled.___m_AnyState_4;
	unmarshaled.___m_TransitionType_5 = marshaled.___m_TransitionType_5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t6_138_marshal_cleanup(AnimatorTransitionInfo_t6_138_marshaled& marshaled)
{
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m6_944 (Animator_t6_139 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m6_948(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m6_945 (Animator_t6_139 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_ResetTriggerString_m6_949(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t6_143 * Animator_get_runtimeAnimatorController_m6_946 (Animator_t6_139 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t6_143 * (*Animator_get_runtimeAnimatorController_m6_946_ftn) (Animator_t6_139 *);
	static Animator_get_runtimeAnimatorController_m6_946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m6_946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m6_947 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m6_947_ftn) (String_t*);
	static Animator_StringToHash_m6_947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m6_947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m6_948 (Animator_t6_139 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m6_948_ftn) (Animator_t6_139 *, String_t*);
	static Animator_SetTriggerString_m6_948_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m6_948_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m6_949 (Animator_t6_139 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m6_949_ftn) (Animator_t6_139 *, String_t*);
	static Animator_ResetTriggerString_m6_949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m6_949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t6_140_marshal(const SkeletonBone_t6_140& unmarshaled, SkeletonBone_t6_140_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
extern "C" void SkeletonBone_t6_140_marshal_back(const SkeletonBone_t6_140_marshaled& marshaled, SkeletonBone_t6_140& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t6_140_marshal_cleanup(SkeletonBone_t6_140_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m6_950 (HumanBone_t6_142 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C" void HumanBone_set_boneName_m6_951 (HumanBone_t6_142 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m6_952 (HumanBone_t6_142 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m6_953 (HumanBone_t6_142 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t6_142_marshal(const HumanBone_t6_142& unmarshaled, HumanBone_t6_142_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
extern "C" void HumanBone_t6_142_marshal_back(const HumanBone_t6_142_marshaled& marshaled, HumanBone_t6_142& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t6_142_marshal_cleanup(HumanBone_t6_142_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m6_954 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___width_3);
		return (((int32_t)((int32_t)L_0)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_advance(System.Int32)
extern "C" void CharacterInfo_set_advance_m6_955 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___width_3 = (((float)((float)L_0)));
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m6_956 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_width_m6_306(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_glyphWidth(System.Int32)
extern "C" void CharacterInfo_set_glyphWidth_m6_957 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_set_width_m6_307(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m6_958 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m6_308(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)((-L_1)))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_glyphHeight(System.Int32)
extern "C" void CharacterInfo_set_glyphHeight_m6_959 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m6_308(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_51 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		Rect_set_height_m6_309(L_2, (((float)((float)((-L_3))))), /*hidden argument*/NULL);
		Rect_t6_51 * L_4 = &(__this->___vert_2);
		Rect_t6_51 * L_5 = L_4;
		float L_6 = Rect_get_y_m6_300(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		Rect_t6_51 * L_8 = &(__this->___vert_2);
		float L_9 = Rect_get_height_m6_308(L_8, /*hidden argument*/NULL);
		Rect_set_y_m6_301(L_5, ((float)((float)L_6+(float)((float)((float)L_7-(float)L_9)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m6_960 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_bearing(System.Int32)
extern "C" void CharacterInfo_set_bearing_m6_961 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_set_x_m6_299(L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m6_962 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t6_51 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m6_300(L_1, /*hidden argument*/NULL);
		Rect_t6_51 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_height_m6_308(L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((int32_t)((float)((float)L_2+(float)L_4)))))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_minY(System.Int32)
extern "C" void CharacterInfo_set_minY_m6_963 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		int32_t L_2 = (__this->___ascent_7);
		Rect_t6_51 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_y_m6_300(L_3, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_0, ((float)((float)(((float)((float)((int32_t)((int32_t)L_1-(int32_t)L_2)))))-(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m6_964 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t6_51 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m6_300(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((int32_t)L_2)))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_maxY(System.Int32)
extern "C" void CharacterInfo_set_maxY_m6_965 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_y_m6_300(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_51 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		int32_t L_4 = (__this->___ascent_7);
		Rect_set_y_m6_301(L_2, (((float)((float)((int32_t)((int32_t)L_3-(int32_t)L_4))))), /*hidden argument*/NULL);
		Rect_t6_51 * L_5 = &(__this->___vert_2);
		Rect_t6_51 * L_6 = L_5;
		float L_7 = Rect_get_height_m6_308(L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		Rect_t6_51 * L_9 = &(__this->___vert_2);
		float L_10 = Rect_get_y_m6_300(L_9, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_6, ((float)((float)L_7+(float)((float)((float)L_8-(float)L_10)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m6_966 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Void UnityEngine.CharacterInfo::set_minX(System.Int32)
extern "C" void CharacterInfo_set_minX_m6_967 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Rect_t6_51 * L_2 = &(__this->___vert_2);
		int32_t L_3 = ___value;
		Rect_set_x_m6_299(L_2, (((float)((float)L_3))), /*hidden argument*/NULL);
		Rect_t6_51 * L_4 = &(__this->___vert_2);
		Rect_t6_51 * L_5 = L_4;
		float L_6 = Rect_get_width_m6_306(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		Rect_t6_51 * L_8 = &(__this->___vert_2);
		float L_9 = Rect_get_x_m6_298(L_8, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_5, ((float)((float)L_6+(float)((float)((float)L_7-(float)L_9)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m6_968 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_width_m6_306(L_2, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)((float)((float)L_1+(float)L_3)))));
	}
}
// System.Void UnityEngine.CharacterInfo::set_maxX(System.Int32)
extern "C" void CharacterInfo_set_maxX_m6_969 (CharacterInfo_t6_147 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___vert_2);
		int32_t L_1 = ___value;
		Rect_t6_51 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_x_m6_298(L_2, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_0, ((float)((float)(((float)((float)L_1)))-(float)L_3)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t6_47  CharacterInfo_get_uvBottomLeftUnFlipped_m6_970 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m6_300(L_2, /*hidden argument*/NULL);
		Vector2_t6_47  L_4 = {0};
		Vector2__ctor_m6_215(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeftUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomLeftUnFlipped_m6_971 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Vector2_t6_47  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_974(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t6_51 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_set_x_m6_299(L_1, L_2, /*hidden argument*/NULL);
		Rect_t6_51 * L_3 = &(__this->___uv_1);
		float L_4 = ((&___value)->___y_2);
		Rect_set_y_m6_301(L_3, L_4, /*hidden argument*/NULL);
		Rect_t6_51 * L_5 = &(__this->___uv_1);
		float L_6 = ((&V_0)->___x_1);
		Rect_t6_51 * L_7 = &(__this->___uv_1);
		float L_8 = Rect_get_x_m6_298(L_7, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_5, ((float)((float)L_6-(float)L_8)), /*hidden argument*/NULL);
		Rect_t6_51 * L_9 = &(__this->___uv_1);
		float L_10 = ((&V_0)->___y_2);
		Rect_t6_51 * L_11 = &(__this->___uv_1);
		float L_12 = Rect_get_y_m6_300(L_11, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_9, ((float)((float)L_10-(float)L_12)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t6_47  CharacterInfo_get_uvBottomRightUnFlipped_m6_972 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m6_306(L_2, /*hidden argument*/NULL);
		Rect_t6_51 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m6_300(L_4, /*hidden argument*/NULL);
		Vector2_t6_47  L_6 = {0};
		Vector2__ctor_m6_215(&L_6, ((float)((float)L_1+(float)L_3)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomRightUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomRightUnFlipped_m6_973 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Vector2_t6_47  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_974(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t6_51 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_t6_51 * L_3 = &(__this->___uv_1);
		float L_4 = Rect_get_x_m6_298(L_3, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_1, ((float)((float)L_2-(float)L_4)), /*hidden argument*/NULL);
		Rect_t6_51 * L_5 = &(__this->___uv_1);
		float L_6 = ((&___value)->___y_2);
		Rect_set_y_m6_301(L_5, L_6, /*hidden argument*/NULL);
		Rect_t6_51 * L_7 = &(__this->___uv_1);
		float L_8 = ((&V_0)->___y_2);
		Rect_t6_51 * L_9 = &(__this->___uv_1);
		float L_10 = Rect_get_y_m6_300(L_9, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_7, ((float)((float)L_8-(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t6_47  CharacterInfo_get_uvTopRightUnFlipped_m6_974 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m6_306(L_2, /*hidden argument*/NULL);
		Rect_t6_51 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m6_300(L_4, /*hidden argument*/NULL);
		Rect_t6_51 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_height_m6_308(L_6, /*hidden argument*/NULL);
		Vector2_t6_47  L_8 = {0};
		Vector2__ctor_m6_215(&L_8, ((float)((float)L_1+(float)L_3)), ((float)((float)L_5+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopRightUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopRightUnFlipped_m6_975 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___uv_1);
		float L_1 = ((&___value)->___x_1);
		Rect_t6_51 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_x_m6_298(L_2, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_0, ((float)((float)L_1-(float)L_3)), /*hidden argument*/NULL);
		Rect_t6_51 * L_4 = &(__this->___uv_1);
		float L_5 = ((&___value)->___y_2);
		Rect_t6_51 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_y_m6_300(L_6, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_4, ((float)((float)L_5-(float)L_7)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t6_47  CharacterInfo_get_uvTopLeftUnFlipped_m6_976 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m6_298(L_0, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m6_300(L_2, /*hidden argument*/NULL);
		Rect_t6_51 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_height_m6_308(L_4, /*hidden argument*/NULL);
		Vector2_t6_47  L_6 = {0};
		Vector2__ctor_m6_215(&L_6, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopLeftUnFlipped(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopLeftUnFlipped_m6_977 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Vector2_t6_47  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_974(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rect_t6_51 * L_1 = &(__this->___uv_1);
		float L_2 = ((&___value)->___x_1);
		Rect_set_x_m6_299(L_1, L_2, /*hidden argument*/NULL);
		Rect_t6_51 * L_3 = &(__this->___uv_1);
		float L_4 = ((&___value)->___y_2);
		Rect_t6_51 * L_5 = &(__this->___uv_1);
		float L_6 = Rect_get_y_m6_300(L_5, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_3, ((float)((float)L_4-(float)L_6)), /*hidden argument*/NULL);
		Rect_t6_51 * L_7 = &(__this->___uv_1);
		float L_8 = ((&V_0)->___x_1);
		Rect_t6_51 * L_9 = &(__this->___uv_1);
		float L_10 = Rect_get_x_m6_298(L_9, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_7, ((float)((float)L_8-(float)L_10)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t6_47  CharacterInfo_get_uvBottomLeft_m6_978 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = CharacterInfo_get_uvBottomLeftUnFlipped_m6_970(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomLeft(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomLeft_m6_979 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = ___value;
		CharacterInfo_set_uvBottomLeftUnFlipped_m6_971(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t6_47  CharacterInfo_get_uvBottomRight_m6_980 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	Vector2_t6_47  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t6_47  L_1 = CharacterInfo_get_uvTopLeftUnFlipped_m6_976(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t6_47  L_2 = CharacterInfo_get_uvBottomRightUnFlipped_m6_972(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvBottomRight(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvBottomRight_m6_981 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Vector2_t6_47  L_1 = ___value;
		CharacterInfo_set_uvTopLeftUnFlipped_m6_977(__this, L_1, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		Vector2_t6_47  L_2 = ___value;
		CharacterInfo_set_uvBottomRightUnFlipped_m6_973(__this, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t6_47  CharacterInfo_get_uvTopRight_m6_982 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = CharacterInfo_get_uvTopRightUnFlipped_m6_974(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopRight(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopRight_m6_983 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = ___value;
		CharacterInfo_set_uvTopRightUnFlipped_m6_975(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t6_47  CharacterInfo_get_uvTopLeft_m6_984 (CharacterInfo_t6_147 * __this, const MethodInfo* method)
{
	Vector2_t6_47  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t6_47  L_1 = CharacterInfo_get_uvBottomRightUnFlipped_m6_972(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t6_47  L_2 = CharacterInfo_get_uvTopLeftUnFlipped_m6_976(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.CharacterInfo::set_uvTopLeft(UnityEngine.Vector2)
extern "C" void CharacterInfo_set_uvTopLeft_m6_985 (CharacterInfo_t6_147 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Vector2_t6_47  L_1 = ___value;
		CharacterInfo_set_uvBottomRightUnFlipped_m6_973(__this, L_1, /*hidden argument*/NULL);
		goto IL_001e;
	}

IL_0017:
	{
		Vector2_t6_47  L_2 = ___value;
		CharacterInfo_set_uvTopLeftUnFlipped_m6_977(__this, L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t6_147_marshal(const CharacterInfo_t6_147& unmarshaled, CharacterInfo_t6_147_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
	marshaled.___ascent_7 = unmarshaled.___ascent_7;
}
extern "C" void CharacterInfo_t6_147_marshal_back(const CharacterInfo_t6_147_marshaled& marshaled, CharacterInfo_t6_147& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
	unmarshaled.___ascent_7 = marshaled.___ascent_7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t6_147_marshal_cleanup(CharacterInfo_t6_147_marshaled& marshaled)
{
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FontTextureRebuildCallback__ctor_m6_986 (FontTextureRebuildCallback_t6_148 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m6_987 (FontTextureRebuildCallback_t6_148 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m6_987((FontTextureRebuildCallback_t6_148 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t6_148(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m6_988 (FontTextureRebuildCallback_t6_148 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m6_989 (FontTextureRebuildCallback_t6_148 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Font::.ctor()
extern "C" void Font__ctor_m6_990 (Font_t6_149 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		Font_Internal_CreateFont_m6_998(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::.ctor(System.String)
extern "C" void Font__ctor_m6_991 (Font_t6_149 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		Font_Internal_CreateFont_m6_998(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::.ctor(System.String[],System.Int32)
extern "C" void Font__ctor_m6_992 (Font_t6_149 * __this, StringU5BU5D_t1_238* ___names, int32_t ___size, const MethodInfo* method)
{
	{
		Object__ctor_m6_701(__this, /*hidden argument*/NULL);
		StringU5BU5D_t1_238* L_0 = ___names;
		int32_t L_1 = ___size;
		Font_Internal_CreateDynamicFont_m6_999(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t6_149_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_1823_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m6_993 (Object_t * __this /* static, unused */, Action_1_t1_1823 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1626);
		Action_1_t1_1823_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1627);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_1823 * L_0 = ((Font_t6_149_StaticFields*)Font_t6_149_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t1_1823 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t6_149_StaticFields*)Font_t6_149_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t1_1823 *)CastclassSealed(L_2, Action_1_t1_1823_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t6_149_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t1_1823_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m6_994 (Object_t * __this /* static, unused */, Action_1_t1_1823 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1626);
		Action_1_t1_1823_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1627);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t1_1823 * L_0 = ((Font_t6_149_StaticFields*)Font_t6_149_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t1_1823 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t6_149_StaticFields*)Font_t6_149_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t1_1823 *)CastclassSealed(L_2, Action_1_t1_1823_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::add_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern TypeInfo* FontTextureRebuildCallback_t6_148_il2cpp_TypeInfo_var;
extern "C" void Font_add_m_FontTextureRebuildCallback_m6_995 (Font_t6_149 * __this, FontTextureRebuildCallback_t6_148 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontTextureRebuildCallback_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1628);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontTextureRebuildCallback_t6_148 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		FontTextureRebuildCallback_t6_148 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_FontTextureRebuildCallback_3 = ((FontTextureRebuildCallback_t6_148 *)CastclassSealed(L_2, FontTextureRebuildCallback_t6_148_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_m_FontTextureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern TypeInfo* FontTextureRebuildCallback_t6_148_il2cpp_TypeInfo_var;
extern "C" void Font_remove_m_FontTextureRebuildCallback_m6_996 (Font_t6_149 * __this, FontTextureRebuildCallback_t6_148 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FontTextureRebuildCallback_t6_148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1628);
		s_Il2CppMethodIntialized = true;
	}
	{
		FontTextureRebuildCallback_t6_148 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		FontTextureRebuildCallback_t6_148 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___m_FontTextureRebuildCallback_3 = ((FontTextureRebuildCallback_t6_148 *)CastclassSealed(L_2, FontTextureRebuildCallback_t6_148_il2cpp_TypeInfo_var));
		return;
	}
}
// System.String[] UnityEngine.Font::GetOSInstalledFontNames()
extern "C" StringU5BU5D_t1_238* Font_GetOSInstalledFontNames_m6_997 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef StringU5BU5D_t1_238* (*Font_GetOSInstalledFontNames_m6_997_ftn) ();
	static Font_GetOSInstalledFontNames_m6_997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_GetOSInstalledFontNames_m6_997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::GetOSInstalledFontNames()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)
extern "C" void Font_Internal_CreateFont_m6_998 (Object_t * __this /* static, unused */, Font_t6_149 * ____font, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Font_Internal_CreateFont_m6_998_ftn) (Font_t6_149 *, String_t*);
	static Font_Internal_CreateFont_m6_998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_Internal_CreateFont_m6_998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::Internal_CreateFont(UnityEngine.Font,System.String)");
	_il2cpp_icall_func(____font, ___name);
}
// System.Void UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)
extern "C" void Font_Internal_CreateDynamicFont_m6_999 (Object_t * __this /* static, unused */, Font_t6_149 * ____font, StringU5BU5D_t1_238* ____names, int32_t ___size, const MethodInfo* method)
{
	typedef void (*Font_Internal_CreateDynamicFont_m6_999_ftn) (Font_t6_149 *, StringU5BU5D_t1_238*, int32_t);
	static Font_Internal_CreateDynamicFont_m6_999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_Internal_CreateDynamicFont_m6_999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::Internal_CreateDynamicFont(UnityEngine.Font,System.String[],System.Int32)");
	_il2cpp_icall_func(____font, ____names, ___size);
}
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String,System.Int32)
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* Font_t6_149_il2cpp_TypeInfo_var;
extern "C" Font_t6_149 * Font_CreateDynamicFontFromOSFont_m6_1000 (Object_t * __this /* static, unused */, String_t* ___fontname, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		Font_t6_149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1626);
		s_Il2CppMethodIntialized = true;
	}
	Font_t6_149 * V_0 = {0};
	{
		StringU5BU5D_t1_238* L_0 = ((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = ___fontname;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)L_1;
		int32_t L_2 = ___size;
		Font_t6_149 * L_3 = (Font_t6_149 *)il2cpp_codegen_object_new (Font_t6_149_il2cpp_TypeInfo_var);
		Font__ctor_m6_992(L_3, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Font_t6_149 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Font UnityEngine.Font::CreateDynamicFontFromOSFont(System.String[],System.Int32)
extern TypeInfo* Font_t6_149_il2cpp_TypeInfo_var;
extern "C" Font_t6_149 * Font_CreateDynamicFontFromOSFont_m6_1001 (Object_t * __this /* static, unused */, StringU5BU5D_t1_238* ___fontnames, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1626);
		s_Il2CppMethodIntialized = true;
	}
	Font_t6_149 * V_0 = {0};
	{
		StringU5BU5D_t1_238* L_0 = ___fontnames;
		int32_t L_1 = ___size;
		Font_t6_149 * L_2 = (Font_t6_149 *)il2cpp_codegen_object_new (Font_t6_149_il2cpp_TypeInfo_var);
		Font__ctor_m6_992(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Font_t6_149 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t6_72 * Font_get_material_m6_1002 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef Material_t6_72 * (*Font_get_material_m6_1002_ftn) (Font_t6_149 *);
	static Font_get_material_m6_1002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m6_1002_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_material(UnityEngine.Material)
extern "C" void Font_set_material_m6_1003 (Font_t6_149 * __this, Material_t6_72 * ___value, const MethodInfo* method)
{
	typedef void (*Font_set_material_m6_1003_ftn) (Font_t6_149 *, Material_t6_72 *);
	static Font_set_material_m6_1003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_material_m6_1003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C" bool Font_HasCharacter_m6_1004 (Font_t6_149 * __this, uint16_t ___c, const MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m6_1004_ftn) (Font_t6_149 *, uint16_t);
	static Font_HasCharacter_m6_1004_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m6_1004_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.String[] UnityEngine.Font::get_fontNames()
extern "C" StringU5BU5D_t1_238* Font_get_fontNames_m6_1005 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef StringU5BU5D_t1_238* (*Font_get_fontNames_m6_1005_ftn) (Font_t6_149 *);
	static Font_get_fontNames_m6_1005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontNames_m6_1005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontNames()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_fontNames(System.String[])
extern "C" void Font_set_fontNames_m6_1006 (Font_t6_149 * __this, StringU5BU5D_t1_238* ___value, const MethodInfo* method)
{
	typedef void (*Font_set_fontNames_m6_1006_ftn) (Font_t6_149 *, StringU5BU5D_t1_238*);
	static Font_set_fontNames_m6_1006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_fontNames_m6_1006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_fontNames(System.String[])");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CharacterInfo[] UnityEngine.Font::get_characterInfo()
extern "C" CharacterInfoU5BU5D_t6_286* Font_get_characterInfo_m6_1007 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef CharacterInfoU5BU5D_t6_286* (*Font_get_characterInfo_m6_1007_ftn) (Font_t6_149 *);
	static Font_get_characterInfo_m6_1007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_characterInfo_m6_1007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_characterInfo()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])
extern "C" void Font_set_characterInfo_m6_1008 (Font_t6_149 * __this, CharacterInfoU5BU5D_t6_286* ___value, const MethodInfo* method)
{
	typedef void (*Font_set_characterInfo_m6_1008_ftn) (Font_t6_149 *, CharacterInfoU5BU5D_t6_286*);
	static Font_set_characterInfo_m6_1008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_set_characterInfo_m6_1008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::set_characterInfo(UnityEngine.CharacterInfo[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)
extern "C" void Font_RequestCharactersInTexture_m6_1009 (Font_t6_149 * __this, String_t* ___characters, int32_t ___size, int32_t ___style, const MethodInfo* method)
{
	typedef void (*Font_RequestCharactersInTexture_m6_1009_ftn) (Font_t6_149 *, String_t*, int32_t, int32_t);
	static Font_RequestCharactersInTexture_m6_1009_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_RequestCharactersInTexture_m6_1009_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32,UnityEngine.FontStyle)");
	_il2cpp_icall_func(__this, ___characters, ___size, ___style);
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String,System.Int32)
extern "C" void Font_RequestCharactersInTexture_m6_1010 (Font_t6_149 * __this, String_t* ___characters, int32_t ___size, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		String_t* L_0 = ___characters;
		int32_t L_1 = ___size;
		int32_t L_2 = V_0;
		Font_RequestCharactersInTexture_m6_1009(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::RequestCharactersInTexture(System.String)
extern "C" void Font_RequestCharactersInTexture_m6_1011 (Font_t6_149 * __this, String_t* ___characters, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		String_t* L_0 = ___characters;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		Font_RequestCharactersInTexture_m6_1009(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern TypeInfo* Font_t6_149_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m1_14952_MethodInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m6_1012 (Object_t * __this /* static, unused */, Font_t6_149 * ___font, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t6_149_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1626);
		Action_1_Invoke_m1_14952_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483793);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1_1823 * V_0 = {0};
	{
		Action_1_t1_1823 * L_0 = ((Font_t6_149_StaticFields*)Font_t6_149_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		V_0 = L_0;
		Action_1_t1_1823 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t1_1823 * L_2 = V_0;
		Font_t6_149 * L_3 = ___font;
		NullCheck(L_2);
		Action_1_Invoke_m1_14952(L_2, L_3, /*hidden argument*/Action_1_Invoke_m1_14952_MethodInfo_var);
	}

IL_0013:
	{
		Font_t6_149 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t6_148 * L_5 = (L_4->___m_FontTextureRebuildCallback_3);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t6_149 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t6_148 * L_7 = (L_6->___m_FontTextureRebuildCallback_3);
		NullCheck(L_7);
		FontTextureRebuildCallback_Invoke_m6_987(L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::get_textureRebuildCallback()
extern "C" FontTextureRebuildCallback_t6_148 * Font_get_textureRebuildCallback_m6_1013 (Font_t6_149 * __this, const MethodInfo* method)
{
	{
		FontTextureRebuildCallback_t6_148 * L_0 = (__this->___m_FontTextureRebuildCallback_3);
		return L_0;
	}
}
// System.Void UnityEngine.Font::set_textureRebuildCallback(UnityEngine.Font/FontTextureRebuildCallback)
extern "C" void Font_set_textureRebuildCallback_m6_1014 (Font_t6_149 * __this, FontTextureRebuildCallback_t6_148 * ___value, const MethodInfo* method)
{
	{
		FontTextureRebuildCallback_t6_148 * L_0 = ___value;
		__this->___m_FontTextureRebuildCallback_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Font::GetMaxVertsForString(System.String)
extern "C" int32_t Font_GetMaxVertsForString_m6_1015 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1_571(L_0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1*(int32_t)4))+(int32_t)4));
	}
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)
extern "C" bool Font_GetCharacterInfo_m6_1016 (Font_t6_149 * __this, uint16_t ___ch, CharacterInfo_t6_147 * ___info, int32_t ___size, int32_t ___style, const MethodInfo* method)
{
	typedef bool (*Font_GetCharacterInfo_m6_1016_ftn) (Font_t6_149 *, uint16_t, CharacterInfo_t6_147 *, int32_t, int32_t);
	static Font_GetCharacterInfo_m6_1016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_GetCharacterInfo_m6_1016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32,UnityEngine.FontStyle)");
	return _il2cpp_icall_func(__this, ___ch, ___info, ___size, ___style);
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&,System.Int32)
extern "C" bool Font_GetCharacterInfo_m6_1017 (Font_t6_149 * __this, uint16_t ___ch, CharacterInfo_t6_147 * ___info, int32_t ___size, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		uint16_t L_0 = ___ch;
		CharacterInfo_t6_147 * L_1 = ___info;
		int32_t L_2 = ___size;
		int32_t L_3 = V_0;
		bool L_4 = Font_GetCharacterInfo_m6_1016(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Font::GetCharacterInfo(System.Char,UnityEngine.CharacterInfo&)
extern "C" bool Font_GetCharacterInfo_m6_1018 (Font_t6_149 * __this, uint16_t ___ch, CharacterInfo_t6_147 * ___info, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		uint16_t L_0 = ___ch;
		CharacterInfo_t6_147 * L_1 = ___info;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Font_GetCharacterInfo_m6_1016(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m6_1019 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m6_1019_ftn) (Font_t6_149 *);
	static Font_get_dynamic_m6_1019_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m6_1019_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_ascent()
extern "C" int32_t Font_get_ascent_m6_1020 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_ascent_m6_1020_ftn) (Font_t6_149 *);
	static Font_get_ascent_m6_1020_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_ascent_m6_1020_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_ascent()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_lineHeight()
extern "C" int32_t Font_get_lineHeight_m6_1021 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_lineHeight_m6_1021_ftn) (Font_t6_149 *);
	static Font_get_lineHeight_m6_1021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_lineHeight_m6_1021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_lineHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m6_1022 (Font_t6_149 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m6_1022_ftn) (Font_t6_149 *);
	static Font_get_fontSize_m6_1022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m6_1022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C" void TextGenerator__ctor_m6_1023 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m6_1024(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern TypeInfo* List_1_t1_1824_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1825_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1826_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14953_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_14954_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1_14955_MethodInfo_var;
extern "C" void TextGenerator__ctor_m6_1024 (TextGenerator_t6_152 * __this, int32_t ___initialCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1824_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1632);
		List_1_t1_1825_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1633);
		List_1_t1_1826_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1634);
		List_1__ctor_m1_14953_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483794);
		List_1__ctor_m1_14954_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483795);
		List_1__ctor_m1_14955_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483796);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		List_1_t1_1824 * L_1 = (List_1_t1_1824 *)il2cpp_codegen_object_new (List_1_t1_1824_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14953(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m1_14953_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		List_1_t1_1825 * L_3 = (List_1_t1_1825 *)il2cpp_codegen_object_new (List_1_t1_1825_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14954(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m1_14954_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		List_1_t1_1826 * L_4 = (List_1_t1_1826 *)il2cpp_codegen_object_new (List_1_t1_1826_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14955(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m1_14955_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m6_1026(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m6_1025 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m6_1027(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m6_1026 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m6_1026_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_Init_m6_1026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m6_1026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m6_1027 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m6_1027_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_Dispose_cpp_m6_1027_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m6_1027_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_m6_1028 (TextGenerator_t6_152 * __this, String_t* ___str, Font_t6_149 * ___font, Color_t6_40  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t6_47  ___extents, Vector2_t6_47  ___pivot, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t6_149 * L_1 = ___font;
		Color_t6_40  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___scaleFactor;
		float L_5 = ___lineSpacing;
		int32_t L_6 = ___style;
		bool L_7 = ___richText;
		bool L_8 = ___resizeTextForBestFit;
		int32_t L_9 = ___resizeTextMinSize;
		int32_t L_10 = ___resizeTextMaxSize;
		int32_t L_11 = ___verticalOverFlow;
		int32_t L_12 = ___horizontalOverflow;
		bool L_13 = ___updateBounds;
		int32_t L_14 = ___anchor;
		float L_15 = ((&___extents)->___x_1);
		float L_16 = ((&___extents)->___y_2);
		float L_17 = ((&___pivot)->___x_1);
		float L_18 = ((&___pivot)->___y_2);
		bool L_19 = ___generateOutOfBounds;
		bool L_20 = TextGenerator_Populate_Internal_cpp_m6_1029(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m6_1029 (TextGenerator_t6_152 * __this, String_t* ___str, Font_t6_149 * ___font, Color_t6_40  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t6_149 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___scaleFactor;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ___extentsX;
		float L_15 = ___extentsY;
		float L_16 = ___pivotX;
		float L_17 = ___pivotY;
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_1030(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_1030 (Object_t * __this /* static, unused */, TextGenerator_t6_152 * ___self, String_t* ___str, Font_t6_149 * ___font, Color_t6_40 * ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_1030_ftn) (TextGenerator_t6_152 *, String_t*, Font_t6_149 *, Color_t6_40 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_1030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6_1030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___scaleFactor, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t6_51  TextGenerator_get_rectExtents_m6_1031 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		TextGenerator_INTERNAL_get_rectExtents_m6_1032(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t6_51  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m6_1032 (TextGenerator_t6_152 * __this, Rect_t6_51 * ___value, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m6_1032_ftn) (TextGenerator_t6_152 *, Rect_t6_51 *);
	static TextGenerator_INTERNAL_get_rectExtents_m6_1032_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m6_1032_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m6_1033 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m6_1033_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_get_vertexCount_m6_1033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m6_1033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C" void TextGenerator_GetVerticesInternal_m6_1034 (TextGenerator_t6_152 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m6_1034_ftn) (TextGenerator_t6_152 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m6_1034_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m6_1034_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t6_287* TextGenerator_GetVerticesArray_m6_1035 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef UIVertexU5BU5D_t6_287* (*TextGenerator_GetVerticesArray_m6_1035_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_GetVerticesArray_m6_1035_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m6_1035_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m6_1036 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m6_1036_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_get_characterCount_m6_1036_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m6_1036_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m6_1037 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m6_1033(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m6_430(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m6_428(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m6_1038 (TextGenerator_t6_152 * __this, Object_t * ___characters, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m6_1038_ftn) (TextGenerator_t6_152 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m6_1038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m6_1038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t6_288* TextGenerator_GetCharactersArray_m6_1039 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t6_288* (*TextGenerator_GetCharactersArray_m6_1039_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_GetCharactersArray_m6_1039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m6_1039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m6_1040 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m6_1040_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_get_lineCount_m6_1040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m6_1040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m6_1041 (TextGenerator_t6_152 * __this, Object_t * ___lines, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m6_1041_ftn) (TextGenerator_t6_152 *, Object_t *);
	static TextGenerator_GetLinesInternal_m6_1041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m6_1041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t6_289* TextGenerator_GetLinesArray_m6_1042 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t6_289* (*TextGenerator_GetLinesArray_m6_1042_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_GetLinesArray_m6_1042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m6_1042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m6_1043 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m6_1043_ftn) (TextGenerator_t6_152 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m6_1043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m6_1043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m6_1044 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppCodeGenString* _stringLiteral4740;
extern Il2CppCodeGenString* _stringLiteral4741;
extern "C" TextGenerationSettings_t6_153  TextGenerator_ValidatedSettings_m6_1045 (TextGenerator_t6_152 * __this, TextGenerationSettings_t6_153  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4740 = il2cpp_codegen_string_literal_from_index(4740);
		_stringLiteral4741 = il2cpp_codegen_string_literal_from_index(4741);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t6_149 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t6_149 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m6_1019(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t6_153  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_6);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m6_636(NULL /*static, unused*/, _stringLiteral4740, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_6 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_8);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m6_636(NULL /*static, unused*/, _stringLiteral4741, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_8 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t6_153  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m6_1046 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C" void TextGenerator_GetCharacters_m6_1047 (TextGenerator_t6_152 * __this, List_1_t1_1825 * ___characters, const MethodInfo* method)
{
	{
		List_1_t1_1825 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m6_1038(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C" void TextGenerator_GetLines_m6_1048 (TextGenerator_t6_152 * __this, List_1_t1_1826 * ___lines, const MethodInfo* method)
{
	{
		List_1_t1_1826 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m6_1041(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void TextGenerator_GetVertices_m6_1049 (TextGenerator_t6_152 * __this, List_1_t1_1824 * ___vertices, const MethodInfo* method)
{
	{
		List_1_t1_1824 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m6_1034(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredWidth_m6_1050 (TextGenerator_t6_152 * __this, String_t* ___str, TextGenerationSettings_t6_153  ___settings, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_13 = 1;
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t6_153  L_1 = ___settings;
		TextGenerator_Populate_m6_1052(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_51  L_2 = TextGenerator_get_rectExtents_m6_1031(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m6_306((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m6_1051 (TextGenerator_t6_152 * __this, String_t* ___str, TextGenerationSettings_t6_153  ___settings, const MethodInfo* method)
{
	Rect_t6_51  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t6_153  L_1 = ___settings;
		TextGenerator_Populate_m6_1052(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_51  L_2 = TextGenerator_get_rectExtents_m6_1031(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m6_308((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m6_1052 (TextGenerator_t6_152 * __this, String_t* ___str, TextGenerationSettings_t6_153  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1_601(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t6_153  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m6_1759((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t6_153  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m6_1053(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m6_1053 (TextGenerator_t6_152 * __this, String_t* ___str, TextGenerationSettings_t6_153  ___settings, const MethodInfo* method)
{
	TextGenerationSettings_t6_153  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t6_153  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t6_153  L_2 = ___settings;
		TextGenerationSettings_t6_153  L_3 = TextGenerator_ValidatedSettings_m6_1045(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t6_149 * L_5 = ((&V_0)->___font_0);
		Color_t6_40  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___scaleFactor_5);
		float L_9 = ((&V_0)->___lineSpacing_3);
		int32_t L_10 = ((&V_0)->___fontStyle_6);
		bool L_11 = ((&V_0)->___richText_4);
		bool L_12 = ((&V_0)->___resizeTextForBestFit_8);
		int32_t L_13 = ((&V_0)->___resizeTextMinSize_9);
		int32_t L_14 = ((&V_0)->___resizeTextMaxSize_10);
		int32_t L_15 = ((&V_0)->___verticalOverflow_12);
		int32_t L_16 = ((&V_0)->___horizontalOverflow_13);
		bool L_17 = ((&V_0)->___updateBounds_11);
		int32_t L_18 = ((&V_0)->___textAnchor_7);
		Vector2_t6_47  L_19 = ((&V_0)->___generationExtents_14);
		Vector2_t6_47  L_20 = ((&V_0)->___pivot_15);
		bool L_21 = ((&V_0)->___generateOutOfBounds_16);
		bool L_22 = TextGenerator_Populate_Internal_m6_1028(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_22;
		bool L_23 = (__this->___m_LastValid_4);
		return L_23;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m6_1054 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1_1824 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m6_1049(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t1_1824 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m6_1055 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1_1825 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m6_1047(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t1_1825 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m6_1056 (TextGenerator_t6_152 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1_1826 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m6_1048(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t1_1826 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C" void WillRenderCanvases__ctor_m6_1057 (WillRenderCanvases_t6_155 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C" void WillRenderCanvases_Invoke_m6_1058 (WillRenderCanvases_t6_155 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WillRenderCanvases_Invoke_m6_1058((WillRenderCanvases_t6_155 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t6_155(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * WillRenderCanvases_BeginInvoke_m6_1059 (WillRenderCanvases_t6_155 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C" void WillRenderCanvases_EndInvoke_m6_1060 (WillRenderCanvases_t6_155 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t6_155_il2cpp_TypeInfo_var;
extern "C" void Canvas_add_willRenderCanvases_m6_1061 (Object_t * __this /* static, unused */, WillRenderCanvases_t6_155 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1635);
		WillRenderCanvases_t6_155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1636);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t6_155 * L_0 = ((Canvas_t6_156_StaticFields*)Canvas_t6_156_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t6_155 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t6_156_StaticFields*)Canvas_t6_156_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t6_155 *)CastclassSealed(L_2, WillRenderCanvases_t6_155_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t6_156_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t6_155_il2cpp_TypeInfo_var;
extern "C" void Canvas_remove_willRenderCanvases_m6_1062 (Object_t * __this /* static, unused */, WillRenderCanvases_t6_155 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1635);
		WillRenderCanvases_t6_155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1636);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t6_155 * L_0 = ((Canvas_t6_156_StaticFields*)Canvas_t6_156_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t6_155 * L_1 = ___value;
		Delegate_t1_22 * L_2 = Delegate_Remove_m1_911(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t6_156_StaticFields*)Canvas_t6_156_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t6_155 *)CastclassSealed(L_2, WillRenderCanvases_t6_155_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" int32_t Canvas_get_renderMode_m6_1063 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m6_1063_ftn) (Canvas_t6_156 *);
	static Canvas_get_renderMode_m6_1063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m6_1063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C" bool Canvas_get_isRootCanvas_m6_1064 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m6_1064_ftn) (Canvas_t6_156 *);
	static Canvas_get_isRootCanvas_m6_1064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m6_1064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" Camera_t6_86 * Canvas_get_worldCamera_m6_1065 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef Camera_t6_86 * (*Canvas_get_worldCamera_m6_1065_ftn) (Canvas_t6_156 *);
	static Canvas_get_worldCamera_m6_1065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m6_1065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C" float Canvas_get_scaleFactor_m6_1066 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m6_1066_ftn) (Canvas_t6_156 *);
	static Canvas_get_scaleFactor_m6_1066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m6_1066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C" void Canvas_set_scaleFactor_m6_1067 (Canvas_t6_156 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m6_1067_ftn) (Canvas_t6_156 *, float);
	static Canvas_set_scaleFactor_m6_1067_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m6_1067_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C" float Canvas_get_referencePixelsPerUnit_m6_1068 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m6_1068_ftn) (Canvas_t6_156 *);
	static Canvas_get_referencePixelsPerUnit_m6_1068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m6_1068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C" void Canvas_set_referencePixelsPerUnit_m6_1069 (Canvas_t6_156 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m6_1069_ftn) (Canvas_t6_156 *, float);
	static Canvas_set_referencePixelsPerUnit_m6_1069_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m6_1069_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C" bool Canvas_get_pixelPerfect_m6_1070 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m6_1070_ftn) (Canvas_t6_156 *);
	static Canvas_get_pixelPerfect_m6_1070_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m6_1070_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C" int32_t Canvas_get_renderOrder_m6_1071 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m6_1071_ftn) (Canvas_t6_156 *);
	static Canvas_get_renderOrder_m6_1071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m6_1071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_overrideSorting()
extern "C" bool Canvas_get_overrideSorting_m6_1072 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_overrideSorting_m6_1072_ftn) (Canvas_t6_156 *);
	static Canvas_get_overrideSorting_m6_1072_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_overrideSorting_m6_1072_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_overrideSorting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
extern "C" void Canvas_set_overrideSorting_m6_1073 (Canvas_t6_156 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_overrideSorting_m6_1073_ftn) (Canvas_t6_156 *, bool);
	static Canvas_set_overrideSorting_m6_1073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_overrideSorting_m6_1073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_overrideSorting(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C" int32_t Canvas_get_sortingOrder_m6_1074 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m6_1074_ftn) (Canvas_t6_156 *);
	static Canvas_get_sortingOrder_m6_1074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m6_1074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
extern "C" void Canvas_set_sortingOrder_m6_1075 (Canvas_t6_156 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingOrder_m6_1075_ftn) (Canvas_t6_156 *, int32_t);
	static Canvas_set_sortingOrder_m6_1075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingOrder_m6_1075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C" int32_t Canvas_get_sortingLayerID_m6_1076 (Canvas_t6_156 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m6_1076_ftn) (Canvas_t6_156 *);
	static Canvas_get_sortingLayerID_m6_1076_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m6_1076_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
extern "C" void Canvas_set_sortingLayerID_m6_1077 (Canvas_t6_156 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingLayerID_m6_1077_ftn) (Canvas_t6_156 *, int32_t);
	static Canvas_set_sortingLayerID_m6_1077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingLayerID_m6_1077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C" Material_t6_72 * Canvas_GetDefaultCanvasMaterial_m6_1078 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t6_72 * (*Canvas_GetDefaultCanvasMaterial_m6_1078_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m6_1078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m6_1078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern TypeInfo* Canvas_t6_156_il2cpp_TypeInfo_var;
extern "C" void Canvas_SendWillRenderCanvases_m6_1079 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t6_156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1635);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t6_155 * L_0 = ((Canvas_t6_156_StaticFields*)Canvas_t6_156_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t6_155 * L_1 = ((Canvas_t6_156_StaticFields*)Canvas_t6_156_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m6_1058(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C" void Canvas_ForceUpdateCanvases_m6_1080 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m6_1079(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C" float CanvasGroup_get_alpha_m6_1081 (CanvasGroup_t6_157 * __this, const MethodInfo* method)
{
	typedef float (*CanvasGroup_get_alpha_m6_1081_ftn) (CanvasGroup_t6_157 *);
	static CanvasGroup_get_alpha_m6_1081_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_alpha_m6_1081_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_alpha()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C" void CanvasGroup_set_alpha_m6_1082 (CanvasGroup_t6_157 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_alpha_m6_1082_ftn) (CanvasGroup_t6_157 *, float);
	static CanvasGroup_set_alpha_m6_1082_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m6_1082_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C" bool CanvasGroup_get_interactable_m6_1083 (CanvasGroup_t6_157 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m6_1083_ftn) (CanvasGroup_t6_157 *);
	static CanvasGroup_get_interactable_m6_1083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m6_1083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C" bool CanvasGroup_get_blocksRaycasts_m6_1084 (CanvasGroup_t6_157 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m6_1084_ftn) (CanvasGroup_t6_157 *);
	static CanvasGroup_get_blocksRaycasts_m6_1084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m6_1084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C" bool CanvasGroup_get_ignoreParentGroups_m6_1085 (CanvasGroup_t6_157 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m6_1085_ftn) (CanvasGroup_t6_157 *);
	static CanvasGroup_get_ignoreParentGroups_m6_1085_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m6_1085_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" bool CanvasGroup_IsRaycastLocationValid_m6_1086 (CanvasGroup_t6_157 * __this, Vector2_t6_47  ___sp, Camera_t6_86 * ___eventCamera, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m6_1084(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UIVertex::.cctor()
extern TypeInfo* UIVertex_t6_158_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m6_1087 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t6_158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1629);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t6_158  V_0 = {0};
	{
		Color32_t6_49  L_0 = {0};
		Color32__ctor_m6_281(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t6_158_StaticFields*)UIVertex_t6_158_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t6_54  L_1 = {0};
		Vector4__ctor_m6_399(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t6_158_StaticFields*)UIVertex_t6_158_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t6_158_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t6_48  L_2 = Vector3_get_zero_m6_255(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t6_48  L_3 = Vector3_get_back_m6_258(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t6_54  L_4 = ((UIVertex_t6_158_StaticFields*)UIVertex_t6_158_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t6_49  L_5 = ((UIVertex_t6_158_StaticFields*)UIVertex_t6_158_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t6_47  L_6 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t6_47  L_7 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t6_158  L_8 = V_0;
		((UIVertex_t6_158_StaticFields*)UIVertex_t6_158_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C" void CanvasRenderer_SetColor_m6_1088 (CanvasRenderer_t6_159 * __this, Color_t6_40  ___color, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m6_1089(NULL /*static, unused*/, __this, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m6_1089 (Object_t * __this /* static, unused */, CanvasRenderer_t6_159 * ___self, Color_t6_40 * ___color, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m6_1089_ftn) (CanvasRenderer_t6_159 *, Color_t6_40 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m6_1089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m6_1089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___color);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C" Color_t6_40  CanvasRenderer_GetColor_m6_1090 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef Color_t6_40  (*CanvasRenderer_GetColor_m6_1090_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_GetColor_m6_1090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetColor_m6_1090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::EnableRectClipping(UnityEngine.Rect)
extern "C" void CanvasRenderer_EnableRectClipping_m6_1091 (CanvasRenderer_t6_159 * __this, Rect_t6_51  ___rect, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m6_1092(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C" void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m6_1092 (Object_t * __this /* static, unused */, CanvasRenderer_t6_159 * ___self, Rect_t6_51 * ___rect, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m6_1092_ftn) (CanvasRenderer_t6_159 *, Rect_t6_51 *);
	static CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m6_1092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m6_1092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self, ___rect);
}
// System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
extern "C" void CanvasRenderer_DisableRectClipping_m6_1093 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_DisableRectClipping_m6_1093_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_DisableRectClipping_m6_1093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_DisableRectClipping_m6_1093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::DisableRectClipping()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
extern "C" void CanvasRenderer_set_hasPopInstruction_m6_1094 (CanvasRenderer_t6_159 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_hasPopInstruction_m6_1094_ftn) (CanvasRenderer_t6_159 *, bool);
	static CanvasRenderer_set_hasPopInstruction_m6_1094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_hasPopInstruction_m6_1094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C" int32_t CanvasRenderer_get_materialCount_m6_1095 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_materialCount_m6_1095_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_get_materialCount_m6_1095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_materialCount_m6_1095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_materialCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C" void CanvasRenderer_set_materialCount_m6_1096 (CanvasRenderer_t6_159 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_materialCount_m6_1096_ftn) (CanvasRenderer_t6_159 *, int32_t);
	static CanvasRenderer_set_materialCount_m6_1096_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_materialCount_m6_1096_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_materialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C" void CanvasRenderer_SetMaterial_m6_1097 (CanvasRenderer_t6_159 * __this, Material_t6_72 * ___material, int32_t ___index, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m6_1097_ftn) (CanvasRenderer_t6_159 *, Material_t6_72 *, int32_t);
	static CanvasRenderer_SetMaterial_m6_1097_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m6_1097_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material, ___index);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C" void CanvasRenderer_SetMaterial_m6_1098 (CanvasRenderer_t6_159 * __this, Material_t6_72 * ___material, Texture_t6_32 * ___texture, const MethodInfo* method)
{
	{
		int32_t L_0 = CanvasRenderer_get_materialCount_m6_1095(__this, /*hidden argument*/NULL);
		int32_t L_1 = Math_Max_m1_14207(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		CanvasRenderer_set_materialCount_m6_1096(__this, L_1, /*hidden argument*/NULL);
		Material_t6_72 * L_2 = ___material;
		CanvasRenderer_SetMaterial_m6_1097(__this, L_2, 0, /*hidden argument*/NULL);
		Texture_t6_32 * L_3 = ___texture;
		CanvasRenderer_SetTexture_m6_1101(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
extern "C" void CanvasRenderer_set_popMaterialCount_m6_1099 (CanvasRenderer_t6_159 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_popMaterialCount_m6_1099_ftn) (CanvasRenderer_t6_159 *, int32_t);
	static CanvasRenderer_set_popMaterialCount_m6_1099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_popMaterialCount_m6_1099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
extern "C" void CanvasRenderer_SetPopMaterial_m6_1100 (CanvasRenderer_t6_159 * __this, Material_t6_72 * ___material, int32_t ___index, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetPopMaterial_m6_1100_ftn) (CanvasRenderer_t6_159 *, Material_t6_72 *, int32_t);
	static CanvasRenderer_SetPopMaterial_m6_1100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetPopMaterial_m6_1100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material, ___index);
}
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C" void CanvasRenderer_SetTexture_m6_1101 (CanvasRenderer_t6_159 * __this, Texture_t6_32 * ___texture, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetTexture_m6_1101_ftn) (CanvasRenderer_t6_159 *, Texture_t6_32 *);
	static CanvasRenderer_SetTexture_m6_1101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetTexture_m6_1101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture);
}
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C" void CanvasRenderer_SetMesh_m6_1102 (CanvasRenderer_t6_159 * __this, Mesh_t6_23 * ___mesh, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMesh_m6_1102_ftn) (CanvasRenderer_t6_159 *, Mesh_t6_23 *);
	static CanvasRenderer_SetMesh_m6_1102_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMesh_m6_1102_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C" void CanvasRenderer_Clear_m6_1103 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m6_1103_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_Clear_m6_1103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m6_1103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreams(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C" void CanvasRenderer_SplitUIVertexStreams_m6_1104 (Object_t * __this /* static, unused */, List_1_t1_1824 * ___verts, List_1_t1_1834 * ___positions, List_1_t1_1837 * ___colors, List_1_t1_1836 * ___uv0S, List_1_t1_1836 * ___uv1S, List_1_t1_1834 * ___normals, List_1_t1_1835 * ___tangents, List_1_t1_1838 * ___indicies, const MethodInfo* method)
{
	{
		List_1_t1_1824 * L_0 = ___verts;
		List_1_t1_1834 * L_1 = ___positions;
		List_1_t1_1837 * L_2 = ___colors;
		List_1_t1_1836 * L_3 = ___uv0S;
		List_1_t1_1836 * L_4 = ___uv1S;
		List_1_t1_1834 * L_5 = ___normals;
		List_1_t1_1835 * L_6 = ___tangents;
		CanvasRenderer_SplitUIVertexStreamsInternal_m6_1105(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		List_1_t1_1824 * L_7 = ___verts;
		List_1_t1_1838 * L_8 = ___indicies;
		CanvasRenderer_SplitIndiciesStreamsInternal_m6_1106(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C" void CanvasRenderer_SplitUIVertexStreamsInternal_m6_1105 (Object_t * __this /* static, unused */, Object_t * ___verts, Object_t * ___positions, Object_t * ___colors, Object_t * ___uv0S, Object_t * ___uv1S, Object_t * ___normals, Object_t * ___tangents, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitUIVertexStreamsInternal_m6_1105_ftn) (Object_t *, Object_t *, Object_t *, Object_t *, Object_t *, Object_t *, Object_t *);
	static CanvasRenderer_SplitUIVertexStreamsInternal_m6_1105_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitUIVertexStreamsInternal_m6_1105_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts, ___positions, ___colors, ___uv0S, ___uv1S, ___normals, ___tangents);
}
// System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
extern "C" void CanvasRenderer_SplitIndiciesStreamsInternal_m6_1106 (Object_t * __this /* static, unused */, Object_t * ___verts, Object_t * ___indicies, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitIndiciesStreamsInternal_m6_1106_ftn) (Object_t *, Object_t *);
	static CanvasRenderer_SplitIndiciesStreamsInternal_m6_1106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitIndiciesStreamsInternal_m6_1106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)");
	_il2cpp_icall_func(___verts, ___indicies);
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C" void CanvasRenderer_CreateUIVertexStream_m6_1107 (Object_t * __this /* static, unused */, List_1_t1_1824 * ___verts, List_1_t1_1834 * ___positions, List_1_t1_1837 * ___colors, List_1_t1_1836 * ___uv0S, List_1_t1_1836 * ___uv1S, List_1_t1_1834 * ___normals, List_1_t1_1835 * ___tangents, List_1_t1_1838 * ___indicies, const MethodInfo* method)
{
	{
		List_1_t1_1824 * L_0 = ___verts;
		List_1_t1_1834 * L_1 = ___positions;
		List_1_t1_1837 * L_2 = ___colors;
		List_1_t1_1836 * L_3 = ___uv0S;
		List_1_t1_1836 * L_4 = ___uv1S;
		List_1_t1_1834 * L_5 = ___normals;
		List_1_t1_1835 * L_6 = ___tangents;
		List_1_t1_1838 * L_7 = ___indicies;
		CanvasRenderer_CreateUIVertexStreamInternal_m6_1108(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C" void CanvasRenderer_CreateUIVertexStreamInternal_m6_1108 (Object_t * __this /* static, unused */, Object_t * ___verts, Object_t * ___positions, Object_t * ___colors, Object_t * ___uv0S, Object_t * ___uv1S, Object_t * ___normals, Object_t * ___tangents, Object_t * ___indicies, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_CreateUIVertexStreamInternal_m6_1108_ftn) (Object_t *, Object_t *, Object_t *, Object_t *, Object_t *, Object_t *, Object_t *, Object_t *);
	static CanvasRenderer_CreateUIVertexStreamInternal_m6_1108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_CreateUIVertexStreamInternal_m6_1108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts, ___positions, ___colors, ___uv0S, ___uv1S, ___normals, ___tangents, ___indicies);
}
// System.Void UnityEngine.CanvasRenderer::AddUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C" void CanvasRenderer_AddUIVertexStream_m6_1109 (Object_t * __this /* static, unused */, List_1_t1_1824 * ___verts, List_1_t1_1834 * ___positions, List_1_t1_1837 * ___colors, List_1_t1_1836 * ___uv0S, List_1_t1_1836 * ___uv1S, List_1_t1_1834 * ___normals, List_1_t1_1835 * ___tangents, const MethodInfo* method)
{
	{
		List_1_t1_1824 * L_0 = ___verts;
		List_1_t1_1834 * L_1 = ___positions;
		List_1_t1_1837 * L_2 = ___colors;
		List_1_t1_1836 * L_3 = ___uv0S;
		List_1_t1_1836 * L_4 = ___uv1S;
		List_1_t1_1834 * L_5 = ___normals;
		List_1_t1_1835 * L_6 = ___tangents;
		CanvasRenderer_SplitUIVertexStreamsInternal_m6_1105(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.CanvasRenderer::get_cull()
extern "C" bool CanvasRenderer_get_cull_m6_1110 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_cull_m6_1110_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_get_cull_m6_1110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_cull_m6_1110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_cull()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
extern "C" void CanvasRenderer_set_cull_m6_1111 (CanvasRenderer_t6_159 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_cull_m6_1111_ftn) (CanvasRenderer_t6_159 *, bool);
	static CanvasRenderer_set_cull_m6_1111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_cull_m6_1111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_cull(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C" int32_t CanvasRenderer_get_absoluteDepth_m6_1112 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m6_1112_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_get_absoluteDepth_m6_1112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m6_1112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
extern "C" bool CanvasRenderer_get_hasMoved_m6_1113 (CanvasRenderer_t6_159 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_hasMoved_m6_1113_ftn) (CanvasRenderer_t6_159 *);
	static CanvasRenderer_get_hasMoved_m6_1113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_hasMoved_m6_1113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_hasMoved()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern TypeInfo* Vector3U5BU5D_t6_161_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility__cctor_m6_1114 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t6_161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1637);
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t6_160_StaticFields*)RectTransformUtility_t6_160_il2cpp_TypeInfo_var->static_fields)->___s_Corners_0 = ((Vector3U5BU5D_t6_161*)SZArrayNew(Vector3U5BU5D_t6_161_il2cpp_TypeInfo_var, 4));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_RectangleContainsScreenPoint_m6_1115 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rect, Vector2_t6_47  ___screenPoint, Camera_t6_86 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t6_64 * L_0 = ___rect;
		Camera_t6_86 * L_1 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6_1116(NULL /*static, unused*/, L_0, (&___screenPoint), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C" bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6_1116 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rect, Vector2_t6_47 * ___screenPoint, Camera_t6_86 * ___cam, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6_1116_ftn) (RectTransform_t6_64 *, Vector2_t6_47 *, Camera_t6_86 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6_1116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6_1116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect, ___screenPoint, ___cam);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  RectTransformUtility_PixelAdjustPoint_m6_1117 (Object_t * __this /* static, unused */, Vector2_t6_47  ___point, Transform_t6_65 * ___elementTransform, Canvas_t6_156 * ___canvas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	{
		Vector2_t6_47  L_0 = ___point;
		Transform_t6_65 * L_1 = ___elementTransform;
		Canvas_t6_156 * L_2 = ___canvas;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m6_1118(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_PixelAdjustPoint_m6_1118 (Object_t * __this /* static, unused */, Vector2_t6_47  ___point, Transform_t6_65 * ___elementTransform, Canvas_t6_156 * ___canvas, Vector2_t6_47 * ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t6_65 * L_0 = ___elementTransform;
		Canvas_t6_156 * L_1 = ___canvas;
		Vector2_t6_47 * L_2 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6_1119(NULL /*static, unused*/, (&___point), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6_1119 (Object_t * __this /* static, unused */, Vector2_t6_47 * ___point, Transform_t6_65 * ___elementTransform, Canvas_t6_156 * ___canvas, Vector2_t6_47 * ___output, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6_1119_ftn) (Vector2_t6_47 *, Transform_t6_65 *, Canvas_t6_156 *, Vector2_t6_47 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6_1119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6_1119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point, ___elementTransform, ___canvas, ___output);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t6_51  RectTransformUtility_PixelAdjustRect_m6_1120 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rectTransform, Canvas_t6_156 * ___canvas, const MethodInfo* method)
{
	typedef Rect_t6_51  (*RectTransformUtility_PixelAdjustRect_m6_1120_ftn) (RectTransform_t6_64 *, Canvas_t6_156 *);
	static RectTransformUtility_PixelAdjustRect_m6_1120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_PixelAdjustRect_m6_1120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(___rectTransform, ___canvas);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m6_1121 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rect, Vector2_t6_47  ___screenPoint, Camera_t6_86 * ___cam, Vector3_t6_48 * ___worldPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t6_55  V_0 = {0};
	Plane_t6_56  V_1 = {0};
	float V_2 = 0.0f;
	{
		Vector3_t6_48 * L_0 = ___worldPoint;
		Vector2_t6_47  L_1 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_48  L_2 = Vector2_op_Implicit_m6_236(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)L_0) = L_2;
		Camera_t6_86 * L_3 = ___cam;
		Vector2_t6_47  L_4 = ___screenPoint;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		Ray_t6_55  L_5 = RectTransformUtility_ScreenPointToRay_m6_1123(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t6_64 * L_6 = ___rect;
		NullCheck(L_6);
		Quaternion_t6_50  L_7 = Transform_get_rotation_m6_761(L_6, /*hidden argument*/NULL);
		Vector3_t6_48  L_8 = Vector3_get_back_m6_258(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_48  L_9 = Quaternion_op_Multiply_m6_293(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t6_64 * L_10 = ___rect;
		NullCheck(L_10);
		Vector3_t6_48  L_11 = Transform_get_position_m6_754(L_10, /*hidden argument*/NULL);
		Plane__ctor_m6_417((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t6_55  L_12 = V_0;
		bool L_13 = Plane_Raycast_m6_420((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		Vector3_t6_48 * L_14 = ___worldPoint;
		float L_15 = V_2;
		Vector3_t6_48  L_16 = Ray_GetPoint_m6_415((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t6_48 *)L_14) = L_16;
		return 1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m6_1122 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rect, Vector2_t6_47  ___screenPoint, Camera_t6_86 * ___cam, Vector2_t6_47 * ___localPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t6_48  V_0 = {0};
	{
		Vector2_t6_47 * L_0 = ___localPoint;
		Vector2_t6_47  L_1 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t6_47 *)L_0) = L_1;
		RectTransform_t6_64 * L_2 = ___rect;
		Vector2_t6_47  L_3 = ___screenPoint;
		Camera_t6_86 * L_4 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m6_1121(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t6_47 * L_6 = ___localPoint;
		RectTransform_t6_64 * L_7 = ___rect;
		Vector3_t6_48  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t6_48  L_9 = Transform_InverseTransformPoint_m6_781(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t6_47  L_10 = Vector2_op_Implicit_m6_235(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t6_47 *)L_6) = L_10;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C" Ray_t6_55  RectTransformUtility_ScreenPointToRay_m6_1123 (Object_t * __this /* static, unused */, Camera_t6_86 * ___cam, Vector2_t6_47  ___screenPos, const MethodInfo* method)
{
	Vector3_t6_48  V_0 = {0};
	{
		Camera_t6_86 * L_0 = ___cam;
		bool L_1 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t6_86 * L_2 = ___cam;
		Vector2_t6_47  L_3 = ___screenPos;
		Vector3_t6_48  L_4 = Vector2_op_Implicit_m6_236(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t6_55  L_5 = Camera_ScreenPointToRay_m6_614(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t6_47  L_6 = ___screenPos;
		Vector3_t6_48  L_7 = Vector2_op_Implicit_m6_236(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t6_48 * L_8 = (&V_0);
		float L_9 = (L_8->___z_3);
		L_8->___z_3 = ((float)((float)L_9-(float)(100.0f)));
		Vector3_t6_48  L_10 = V_0;
		Vector3_t6_48  L_11 = Vector3_get_forward_m6_257(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t6_55  L_12 = {0};
		Ray__ctor_m6_412(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m6_1124 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rect, int32_t ___axis, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t6_64 * V_1 = {0};
	Vector2_t6_47  V_2 = {0};
	Vector2_t6_47  V_3 = {0};
	Vector2_t6_47  V_4 = {0};
	Vector2_t6_47  V_5 = {0};
	float V_6 = 0.0f;
	{
		RectTransform_t6_64 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t6_64 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t6_65 * L_5 = Transform_GetChild_m6_789(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t6_64 *)IsInstSealed(L_5, RectTransform_t6_64_il2cpp_TypeInfo_var));
		RectTransform_t6_64 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_6, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t6_64 * L_8 = V_1;
		int32_t L_9 = ___axis;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m6_1124(NULL /*static, unused*/, L_8, L_9, 0, 1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t6_64 * L_12 = ___rect;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m6_783(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t6_64 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t6_47  L_15 = RectTransform_get_pivot_m6_473(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis;
		int32_t L_17 = ___axis;
		float L_18 = Vector2_get_Item_m6_216((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m6_217((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t6_64 * L_19 = ___rect;
		Vector2_t6_47  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m6_474(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t6_64 * L_22 = ___rect;
		NullCheck(L_22);
		Vector2_t6_47  L_23 = RectTransform_get_anchoredPosition_m6_465(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis;
		int32_t L_25 = ___axis;
		float L_26 = Vector2_get_Item_m6_216((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m6_217((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t6_64 * L_27 = ___rect;
		Vector2_t6_47  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m6_466(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t6_64 * L_29 = ___rect;
		NullCheck(L_29);
		Vector2_t6_47  L_30 = RectTransform_get_anchorMin_m6_457(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t6_64 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t6_47  L_32 = RectTransform_get_anchorMax_m6_461(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis;
		float L_34 = Vector2_get_Item_m6_216((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis;
		int32_t L_36 = ___axis;
		float L_37 = Vector2_get_Item_m6_216((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m6_217((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis;
		float L_39 = V_6;
		Vector2_set_Item_m6_217((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t6_64 * L_40 = ___rect;
		Vector2_t6_47  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m6_458(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t6_64 * L_42 = ___rect;
		Vector2_t6_47  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m6_462(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t6_64_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t6_160_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutAxes_m6_1125 (Object_t * __this /* static, unused */, RectTransform_t6_64 * ___rect, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t6_64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1597);
		RectTransformUtility_t6_160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1638);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t6_64 * V_1 = {0};
	{
		RectTransform_t6_64 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t6_64 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t6_65 * L_5 = Transform_GetChild_m6_789(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t6_64 *)IsInstSealed(L_5, RectTransform_t6_64_il2cpp_TypeInfo_var));
		RectTransform_t6_64 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m6_722(NULL /*static, unused*/, L_6, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t6_64 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m6_1125(NULL /*static, unused*/, L_8, 0, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t6_64 * L_11 = ___rect;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m6_783(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t6_64 * L_13 = ___rect;
		RectTransform_t6_64 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t6_47  L_15 = RectTransform_get_pivot_m6_473(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		Vector2_t6_47  L_16 = RectTransformUtility_GetTransposed_m6_1126(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m6_474(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t6_64 * L_17 = ___rect;
		RectTransform_t6_64 * L_18 = ___rect;
		NullCheck(L_18);
		Vector2_t6_47  L_19 = RectTransform_get_sizeDelta_m6_469(L_18, /*hidden argument*/NULL);
		Vector2_t6_47  L_20 = RectTransformUtility_GetTransposed_m6_1126(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m6_470(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t6_64 * L_22 = ___rect;
		RectTransform_t6_64 * L_23 = ___rect;
		NullCheck(L_23);
		Vector2_t6_47  L_24 = RectTransform_get_anchoredPosition_m6_465(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t6_160_il2cpp_TypeInfo_var);
		Vector2_t6_47  L_25 = RectTransformUtility_GetTransposed_m6_1126(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m6_466(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t6_64 * L_26 = ___rect;
		RectTransform_t6_64 * L_27 = ___rect;
		NullCheck(L_27);
		Vector2_t6_47  L_28 = RectTransform_get_anchorMin_m6_457(L_27, /*hidden argument*/NULL);
		Vector2_t6_47  L_29 = RectTransformUtility_GetTransposed_m6_1126(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m6_458(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t6_64 * L_30 = ___rect;
		RectTransform_t6_64 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t6_47  L_32 = RectTransform_get_anchorMax_m6_461(L_31, /*hidden argument*/NULL);
		Vector2_t6_47  L_33 = RectTransformUtility_GetTransposed_m6_1126(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m6_462(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C" Vector2_t6_47  RectTransformUtility_GetTransposed_m6_1126 (Object_t * __this /* static, unused */, Vector2_t6_47  ___input, const MethodInfo* method)
{
	{
		float L_0 = ((&___input)->___y_2);
		float L_1 = ((&___input)->___x_1);
		Vector2_t6_47  L_2 = {0};
		Vector2__ctor_m6_215(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Event::.ctor()
extern "C" void Event__ctor_m6_1127 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Event_Init_m6_1159(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(UnityEngine.Event)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4742;
extern "C" void Event__ctor_m6_1128 (Event_t6_162 * __this, Event_t6_162 * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4742 = il2cpp_codegen_string_literal_from_index(4742);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		Event_t6_162 * L_0 = ___other;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_1, _stringLiteral4742, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Event_t6_162 * L_2 = ___other;
		Event_InitCopy_m6_1161(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.IntPtr)
extern "C" void Event__ctor_m6_1129 (Event_t6_162 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___ptr;
		Event_InitPtr_m6_1162(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Finalize()
extern "C" void Event_Finalize_m6_1130 (Event_t6_162 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m6_1160(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C" Vector2_t6_47  Event_get_mousePosition_m6_1131 (Event_t6_162 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Event_Internal_GetMousePosition_m6_1169(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_mousePosition(UnityEngine.Vector2)
extern "C" void Event_set_mousePosition_m6_1132 (Event_t6_162 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = ___value;
		Event_Internal_SetMousePosition_m6_1167(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_delta()
extern "C" Vector2_t6_47  Event_get_delta_m6_1133 (Event_t6_162 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		Event_Internal_GetMouseDelta_m6_1172(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_delta(UnityEngine.Vector2)
extern "C" void Event_set_delta_m6_1134 (Event_t6_162 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Vector2_t6_47  L_0 = ___value;
		Event_Internal_SetMouseDelta_m6_1170(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Ray UnityEngine.Event::get_mouseRay()
extern "C" Ray_t6_55  Event_get_mouseRay_m6_1135 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		Vector3_t6_48  L_0 = Vector3_get_up_m6_259(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t6_48  L_1 = Vector3_get_up_m6_259(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t6_55  L_2 = {0};
		Ray__ctor_m6_412(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Event::set_mouseRay(UnityEngine.Ray)
extern "C" void Event_set_mouseRay_m6_1136 (Event_t6_162 * __this, Ray_t6_55  ___value, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_shift()
extern "C" bool Event_get_shift_m6_1137 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_shift(System.Boolean)
extern "C" void Event_set_shift_m6_1138 (Event_t6_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_control()
extern "C" bool Event_get_control_m6_1139 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_control(System.Boolean)
extern "C" void Event_set_control_m6_1140 (Event_t6_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-3))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_2|(int32_t)2)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_alt()
extern "C" bool Event_get_alt_m6_1141 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_alt(System.Boolean)
extern "C" void Event_set_alt_m6_1142 (Event_t6_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-5))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_2|(int32_t)4)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_command()
extern "C" bool Event_get_command_m6_1143 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_command(System.Boolean)
extern "C" void Event_set_command_m6_1144 (Event_t6_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-9))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_2|(int32_t)8)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_capsLock()
extern "C" bool Event_get_capsLock_m6_1145 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)32)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_capsLock(System.Boolean)
extern "C" void Event_set_capsLock_m6_1146 (Event_t6_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		goto IL_0029;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_2|(int32_t)((int32_t)32))), /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_numeric()
extern "C" bool Event_get_numeric_m6_1147 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.Event::set_numeric(System.Boolean)
extern "C" void Event_set_numeric_m6_1148 (Event_t6_162 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)-2))), /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001a:
	{
		int32_t L_2 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_set_modifiers_m6_1176(__this, ((int32_t)((int32_t)L_2|(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Event::get_functionKey()
extern "C" bool Event_get_functionKey_m6_1149 (Event_t6_162 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern TypeInfo* Event_t6_162_il2cpp_TypeInfo_var;
extern "C" Event_t6_162 * Event_get_current_m6_1150 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1639);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t6_162 * L_0 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		return L_0;
	}
}
// System.Void UnityEngine.Event::set_current(UnityEngine.Event)
extern TypeInfo* Event_t6_162_il2cpp_TypeInfo_var;
extern "C" void Event_set_current_m6_1151 (Object_t * __this /* static, unused */, Event_t6_162 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1639);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t6_162 * L_0 = ___value;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Event_t6_162 * L_1 = ___value;
		((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_1;
		goto IL_001b;
	}

IL_0011:
	{
		Event_t6_162 * L_2 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
	}

IL_001b:
	{
		Event_t6_162 * L_3 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m6_1187(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent()
extern TypeInfo* Event_t6_162_il2cpp_TypeInfo_var;
extern "C" void Event_Internal_MakeMasterEventCurrent_m6_1152 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1639);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t6_162 * L_0 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Event_t6_162 * L_1 = (Event_t6_162 *)il2cpp_codegen_object_new (Event_t6_162_il2cpp_TypeInfo_var);
		Event__ctor_m6_1127(L_1, /*hidden argument*/NULL);
		((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2 = L_1;
	}

IL_0014:
	{
		Event_t6_162 * L_2 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
		Event_t6_162 * L_3 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m6_1187(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C" bool Event_get_isKey_m6_1153 (Event_t6_162 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C" bool Event_get_isMouse_m6_1154 (Event_t6_162 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return G_B5_0;
	}
}
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
extern const Il2CppType* KeyCode_t6_163_0_0_0_var;
extern TypeInfo* Event_t6_162_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_92_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14878_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4743;
extern Il2CppCodeGenString* _stringLiteral4744;
extern Il2CppCodeGenString* _stringLiteral4745;
extern Il2CppCodeGenString* _stringLiteral4746;
extern Il2CppCodeGenString* _stringLiteral4747;
extern Il2CppCodeGenString* _stringLiteral4748;
extern Il2CppCodeGenString* _stringLiteral4749;
extern Il2CppCodeGenString* _stringLiteral4750;
extern Il2CppCodeGenString* _stringLiteral4751;
extern Il2CppCodeGenString* _stringLiteral4752;
extern Il2CppCodeGenString* _stringLiteral4753;
extern Il2CppCodeGenString* _stringLiteral4754;
extern Il2CppCodeGenString* _stringLiteral4755;
extern Il2CppCodeGenString* _stringLiteral4756;
extern Il2CppCodeGenString* _stringLiteral4757;
extern Il2CppCodeGenString* _stringLiteral4758;
extern Il2CppCodeGenString* _stringLiteral4759;
extern Il2CppCodeGenString* _stringLiteral4760;
extern Il2CppCodeGenString* _stringLiteral4761;
extern Il2CppCodeGenString* _stringLiteral4762;
extern Il2CppCodeGenString* _stringLiteral4763;
extern Il2CppCodeGenString* _stringLiteral4764;
extern Il2CppCodeGenString* _stringLiteral4765;
extern Il2CppCodeGenString* _stringLiteral4766;
extern Il2CppCodeGenString* _stringLiteral4767;
extern Il2CppCodeGenString* _stringLiteral4768;
extern Il2CppCodeGenString* _stringLiteral4769;
extern Il2CppCodeGenString* _stringLiteral4770;
extern Il2CppCodeGenString* _stringLiteral4771;
extern Il2CppCodeGenString* _stringLiteral4772;
extern Il2CppCodeGenString* _stringLiteral4773;
extern Il2CppCodeGenString* _stringLiteral4774;
extern Il2CppCodeGenString* _stringLiteral4775;
extern Il2CppCodeGenString* _stringLiteral4776;
extern Il2CppCodeGenString* _stringLiteral4777;
extern Il2CppCodeGenString* _stringLiteral4778;
extern Il2CppCodeGenString* _stringLiteral4779;
extern Il2CppCodeGenString* _stringLiteral4780;
extern Il2CppCodeGenString* _stringLiteral4781;
extern Il2CppCodeGenString* _stringLiteral4782;
extern Il2CppCodeGenString* _stringLiteral4783;
extern Il2CppCodeGenString* _stringLiteral4784;
extern Il2CppCodeGenString* _stringLiteral4785;
extern Il2CppCodeGenString* _stringLiteral4786;
extern Il2CppCodeGenString* _stringLiteral4787;
extern Il2CppCodeGenString* _stringLiteral4788;
extern Il2CppCodeGenString* _stringLiteral4789;
extern Il2CppCodeGenString* _stringLiteral2318;
extern Il2CppCodeGenString* _stringLiteral3953;
extern Il2CppCodeGenString* _stringLiteral4790;
extern "C" Event_t6_162 * Event_KeyboardEvent_m6_1155 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyCode_t6_163_0_0_0_var = il2cpp_codegen_type_from_index(1640);
		Event_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1639);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Dictionary_2_t1_92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Dictionary_2__ctor_m1_14878_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		_stringLiteral4743 = il2cpp_codegen_string_literal_from_index(4743);
		_stringLiteral4744 = il2cpp_codegen_string_literal_from_index(4744);
		_stringLiteral4745 = il2cpp_codegen_string_literal_from_index(4745);
		_stringLiteral4746 = il2cpp_codegen_string_literal_from_index(4746);
		_stringLiteral4747 = il2cpp_codegen_string_literal_from_index(4747);
		_stringLiteral4748 = il2cpp_codegen_string_literal_from_index(4748);
		_stringLiteral4749 = il2cpp_codegen_string_literal_from_index(4749);
		_stringLiteral4750 = il2cpp_codegen_string_literal_from_index(4750);
		_stringLiteral4751 = il2cpp_codegen_string_literal_from_index(4751);
		_stringLiteral4752 = il2cpp_codegen_string_literal_from_index(4752);
		_stringLiteral4753 = il2cpp_codegen_string_literal_from_index(4753);
		_stringLiteral4754 = il2cpp_codegen_string_literal_from_index(4754);
		_stringLiteral4755 = il2cpp_codegen_string_literal_from_index(4755);
		_stringLiteral4756 = il2cpp_codegen_string_literal_from_index(4756);
		_stringLiteral4757 = il2cpp_codegen_string_literal_from_index(4757);
		_stringLiteral4758 = il2cpp_codegen_string_literal_from_index(4758);
		_stringLiteral4759 = il2cpp_codegen_string_literal_from_index(4759);
		_stringLiteral4760 = il2cpp_codegen_string_literal_from_index(4760);
		_stringLiteral4761 = il2cpp_codegen_string_literal_from_index(4761);
		_stringLiteral4762 = il2cpp_codegen_string_literal_from_index(4762);
		_stringLiteral4763 = il2cpp_codegen_string_literal_from_index(4763);
		_stringLiteral4764 = il2cpp_codegen_string_literal_from_index(4764);
		_stringLiteral4765 = il2cpp_codegen_string_literal_from_index(4765);
		_stringLiteral4766 = il2cpp_codegen_string_literal_from_index(4766);
		_stringLiteral4767 = il2cpp_codegen_string_literal_from_index(4767);
		_stringLiteral4768 = il2cpp_codegen_string_literal_from_index(4768);
		_stringLiteral4769 = il2cpp_codegen_string_literal_from_index(4769);
		_stringLiteral4770 = il2cpp_codegen_string_literal_from_index(4770);
		_stringLiteral4771 = il2cpp_codegen_string_literal_from_index(4771);
		_stringLiteral4772 = il2cpp_codegen_string_literal_from_index(4772);
		_stringLiteral4773 = il2cpp_codegen_string_literal_from_index(4773);
		_stringLiteral4774 = il2cpp_codegen_string_literal_from_index(4774);
		_stringLiteral4775 = il2cpp_codegen_string_literal_from_index(4775);
		_stringLiteral4776 = il2cpp_codegen_string_literal_from_index(4776);
		_stringLiteral4777 = il2cpp_codegen_string_literal_from_index(4777);
		_stringLiteral4778 = il2cpp_codegen_string_literal_from_index(4778);
		_stringLiteral4779 = il2cpp_codegen_string_literal_from_index(4779);
		_stringLiteral4780 = il2cpp_codegen_string_literal_from_index(4780);
		_stringLiteral4781 = il2cpp_codegen_string_literal_from_index(4781);
		_stringLiteral4782 = il2cpp_codegen_string_literal_from_index(4782);
		_stringLiteral4783 = il2cpp_codegen_string_literal_from_index(4783);
		_stringLiteral4784 = il2cpp_codegen_string_literal_from_index(4784);
		_stringLiteral4785 = il2cpp_codegen_string_literal_from_index(4785);
		_stringLiteral4786 = il2cpp_codegen_string_literal_from_index(4786);
		_stringLiteral4787 = il2cpp_codegen_string_literal_from_index(4787);
		_stringLiteral4788 = il2cpp_codegen_string_literal_from_index(4788);
		_stringLiteral4789 = il2cpp_codegen_string_literal_from_index(4789);
		_stringLiteral2318 = il2cpp_codegen_string_literal_from_index(2318);
		_stringLiteral3953 = il2cpp_codegen_string_literal_from_index(3953);
		_stringLiteral4790 = il2cpp_codegen_string_literal_from_index(4790);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_162 * V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = {0};
	uint16_t V_4 = 0x0;
	String_t* V_5 = {0};
	Dictionary_2_t1_92 * V_6 = {0};
	int32_t V_7 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Event_t6_162 * L_0 = (Event_t6_162 *)il2cpp_codegen_object_new (Event_t6_162_il2cpp_TypeInfo_var);
		Event__ctor_m6_1127(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t6_162 * L_1 = V_0;
		NullCheck(L_1);
		Event_set_type_m6_1165(L_1, 4, /*hidden argument*/NULL);
		String_t* L_2 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1_521(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		Event_t6_162 * L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		V_1 = 0;
		V_2 = 0;
	}

IL_001e:
	{
		V_2 = 1;
		int32_t L_5 = V_1;
		String_t* L_6 = ___key;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1_571(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0033;
		}
	}
	{
		V_2 = 0;
		goto IL_00cd;
	}

IL_0033:
	{
		String_t* L_8 = ___key;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		uint16_t L_10 = String_get_Chars_m1_442(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		uint16_t L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 0)
		{
			goto IL_00a9;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 1)
		{
			goto IL_0056;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 2)
		{
			goto IL_0092;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 3)
		{
			goto IL_0064;
		}
	}

IL_0056:
	{
		uint16_t L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)94))))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00c0;
	}

IL_0064:
	{
		Event_t6_162 * L_13 = V_0;
		Event_t6_162 * L_14 = L_13;
		NullCheck(L_14);
		int32_t L_15 = Event_get_modifiers_m6_1175(L_14, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_set_modifiers_m6_1176(L_14, ((int32_t)((int32_t)L_15|(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_00c7;
	}

IL_007b:
	{
		Event_t6_162 * L_17 = V_0;
		Event_t6_162 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = Event_get_modifiers_m6_1175(L_18, /*hidden argument*/NULL);
		NullCheck(L_18);
		Event_set_modifiers_m6_1176(L_18, ((int32_t)((int32_t)L_19|(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		goto IL_00c7;
	}

IL_0092:
	{
		Event_t6_162 * L_21 = V_0;
		Event_t6_162 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = Event_get_modifiers_m6_1175(L_22, /*hidden argument*/NULL);
		NullCheck(L_22);
		Event_set_modifiers_m6_1176(L_22, ((int32_t)((int32_t)L_23|(int32_t)8)), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
		goto IL_00c7;
	}

IL_00a9:
	{
		Event_t6_162 * L_25 = V_0;
		Event_t6_162 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = Event_get_modifiers_m6_1175(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		Event_set_modifiers_m6_1176(L_26, ((int32_t)((int32_t)L_27|(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_00c7;
	}

IL_00c0:
	{
		V_2 = 0;
		goto IL_00c7;
	}

IL_00c7:
	{
		bool L_29 = V_2;
		if (L_29)
		{
			goto IL_001e;
		}
	}

IL_00cd:
	{
		String_t* L_30 = ___key;
		int32_t L_31 = V_1;
		String_t* L_32 = ___key;
		NullCheck(L_32);
		int32_t L_33 = String_get_Length_m1_571(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_1;
		NullCheck(L_30);
		String_t* L_35 = String_Substring_m1_455(L_30, L_31, ((int32_t)((int32_t)L_33-(int32_t)L_34)), /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = String_ToLower_m1_540(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		String_t* L_37 = V_3;
		V_5 = L_37;
		String_t* L_38 = V_5;
		if (!L_38)
		{
			goto IL_09e5;
		}
	}
	{
		Dictionary_2_t1_92 * L_39 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		if (L_39)
		{
			goto IL_03ab;
		}
	}
	{
		Dictionary_2_t1_92 * L_40 = (Dictionary_2_t1_92 *)il2cpp_codegen_object_new (Dictionary_2_t1_92_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14878(L_40, ((int32_t)49), /*hidden argument*/Dictionary_2__ctor_m1_14878_MethodInfo_var);
		V_6 = L_40;
		Dictionary_2_t1_92 * L_41 = V_6;
		NullCheck(L_41);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_41, _stringLiteral4743, 0);
		Dictionary_2_t1_92 * L_42 = V_6;
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_42, _stringLiteral4744, 1);
		Dictionary_2_t1_92 * L_43 = V_6;
		NullCheck(L_43);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_43, _stringLiteral4745, 2);
		Dictionary_2_t1_92 * L_44 = V_6;
		NullCheck(L_44);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_44, _stringLiteral4746, 3);
		Dictionary_2_t1_92 * L_45 = V_6;
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_45, _stringLiteral4747, 4);
		Dictionary_2_t1_92 * L_46 = V_6;
		NullCheck(L_46);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_46, _stringLiteral4748, 5);
		Dictionary_2_t1_92 * L_47 = V_6;
		NullCheck(L_47);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_47, _stringLiteral4749, 6);
		Dictionary_2_t1_92 * L_48 = V_6;
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_48, _stringLiteral4750, 7);
		Dictionary_2_t1_92 * L_49 = V_6;
		NullCheck(L_49);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_49, _stringLiteral4751, 8);
		Dictionary_2_t1_92 * L_50 = V_6;
		NullCheck(L_50);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_50, _stringLiteral4752, ((int32_t)9));
		Dictionary_2_t1_92 * L_51 = V_6;
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_51, _stringLiteral4753, ((int32_t)10));
		Dictionary_2_t1_92 * L_52 = V_6;
		NullCheck(L_52);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_52, _stringLiteral4754, ((int32_t)11));
		Dictionary_2_t1_92 * L_53 = V_6;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_53, _stringLiteral4755, ((int32_t)12));
		Dictionary_2_t1_92 * L_54 = V_6;
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_54, _stringLiteral4756, ((int32_t)13));
		Dictionary_2_t1_92 * L_55 = V_6;
		NullCheck(L_55);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_55, _stringLiteral4757, ((int32_t)14));
		Dictionary_2_t1_92 * L_56 = V_6;
		NullCheck(L_56);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_56, _stringLiteral4758, ((int32_t)15));
		Dictionary_2_t1_92 * L_57 = V_6;
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_57, _stringLiteral4759, ((int32_t)16));
		Dictionary_2_t1_92 * L_58 = V_6;
		NullCheck(L_58);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_58, _stringLiteral4760, ((int32_t)17));
		Dictionary_2_t1_92 * L_59 = V_6;
		NullCheck(L_59);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_59, _stringLiteral4761, ((int32_t)18));
		Dictionary_2_t1_92 * L_60 = V_6;
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_60, _stringLiteral4762, ((int32_t)19));
		Dictionary_2_t1_92 * L_61 = V_6;
		NullCheck(L_61);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_61, _stringLiteral4763, ((int32_t)20));
		Dictionary_2_t1_92 * L_62 = V_6;
		NullCheck(L_62);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_62, _stringLiteral4764, ((int32_t)21));
		Dictionary_2_t1_92 * L_63 = V_6;
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_63, _stringLiteral4765, ((int32_t)22));
		Dictionary_2_t1_92 * L_64 = V_6;
		NullCheck(L_64);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_64, _stringLiteral4766, ((int32_t)23));
		Dictionary_2_t1_92 * L_65 = V_6;
		NullCheck(L_65);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_65, _stringLiteral4767, ((int32_t)24));
		Dictionary_2_t1_92 * L_66 = V_6;
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_66, _stringLiteral4768, ((int32_t)25));
		Dictionary_2_t1_92 * L_67 = V_6;
		NullCheck(L_67);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_67, _stringLiteral4769, ((int32_t)26));
		Dictionary_2_t1_92 * L_68 = V_6;
		NullCheck(L_68);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_68, _stringLiteral4770, ((int32_t)27));
		Dictionary_2_t1_92 * L_69 = V_6;
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_69, _stringLiteral4771, ((int32_t)28));
		Dictionary_2_t1_92 * L_70 = V_6;
		NullCheck(L_70);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_70, _stringLiteral4772, ((int32_t)29));
		Dictionary_2_t1_92 * L_71 = V_6;
		NullCheck(L_71);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_71, _stringLiteral4773, ((int32_t)30));
		Dictionary_2_t1_92 * L_72 = V_6;
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_72, _stringLiteral4774, ((int32_t)31));
		Dictionary_2_t1_92 * L_73 = V_6;
		NullCheck(L_73);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_73, _stringLiteral4775, ((int32_t)32));
		Dictionary_2_t1_92 * L_74 = V_6;
		NullCheck(L_74);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_74, _stringLiteral4776, ((int32_t)33));
		Dictionary_2_t1_92 * L_75 = V_6;
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_75, _stringLiteral4777, ((int32_t)34));
		Dictionary_2_t1_92 * L_76 = V_6;
		NullCheck(L_76);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_76, _stringLiteral4778, ((int32_t)35));
		Dictionary_2_t1_92 * L_77 = V_6;
		NullCheck(L_77);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_77, _stringLiteral4779, ((int32_t)36));
		Dictionary_2_t1_92 * L_78 = V_6;
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_78, _stringLiteral4780, ((int32_t)37));
		Dictionary_2_t1_92 * L_79 = V_6;
		NullCheck(L_79);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_79, _stringLiteral4781, ((int32_t)38));
		Dictionary_2_t1_92 * L_80 = V_6;
		NullCheck(L_80);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_80, _stringLiteral4782, ((int32_t)39));
		Dictionary_2_t1_92 * L_81 = V_6;
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_81, _stringLiteral4783, ((int32_t)40));
		Dictionary_2_t1_92 * L_82 = V_6;
		NullCheck(L_82);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_82, _stringLiteral4784, ((int32_t)41));
		Dictionary_2_t1_92 * L_83 = V_6;
		NullCheck(L_83);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_83, _stringLiteral4785, ((int32_t)42));
		Dictionary_2_t1_92 * L_84 = V_6;
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_84, _stringLiteral4786, ((int32_t)43));
		Dictionary_2_t1_92 * L_85 = V_6;
		NullCheck(L_85);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_85, _stringLiteral4787, ((int32_t)44));
		Dictionary_2_t1_92 * L_86 = V_6;
		NullCheck(L_86);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_86, _stringLiteral4788, ((int32_t)45));
		Dictionary_2_t1_92 * L_87 = V_6;
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_87, _stringLiteral4789, ((int32_t)46));
		Dictionary_2_t1_92 * L_88 = V_6;
		NullCheck(L_88);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_88, _stringLiteral2318, ((int32_t)47));
		Dictionary_2_t1_92 * L_89 = V_6;
		NullCheck(L_89);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_89, _stringLiteral3953, ((int32_t)48));
		Dictionary_2_t1_92 * L_90 = V_6;
		((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3 = L_90;
	}

IL_03ab:
	{
		Dictionary_2_t1_92 * L_91 = ((Event_t6_162_StaticFields*)Event_t6_162_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_3;
		String_t* L_92 = V_5;
		NullCheck(L_91);
		bool L_93 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_91, L_92, (&V_7));
		if (!L_93)
		{
			goto IL_09e5;
		}
	}
	{
		int32_t L_94 = V_7;
		if (L_94 == 0)
		{
			goto IL_048e;
		}
		if (L_94 == 1)
		{
			goto IL_04a6;
		}
		if (L_94 == 2)
		{
			goto IL_04be;
		}
		if (L_94 == 3)
		{
			goto IL_04d6;
		}
		if (L_94 == 4)
		{
			goto IL_04ee;
		}
		if (L_94 == 5)
		{
			goto IL_0506;
		}
		if (L_94 == 6)
		{
			goto IL_051e;
		}
		if (L_94 == 7)
		{
			goto IL_0536;
		}
		if (L_94 == 8)
		{
			goto IL_054e;
		}
		if (L_94 == 9)
		{
			goto IL_0566;
		}
		if (L_94 == 10)
		{
			goto IL_057e;
		}
		if (L_94 == 11)
		{
			goto IL_0596;
		}
		if (L_94 == 12)
		{
			goto IL_05ae;
		}
		if (L_94 == 13)
		{
			goto IL_05c6;
		}
		if (L_94 == 14)
		{
			goto IL_05de;
		}
		if (L_94 == 15)
		{
			goto IL_05f6;
		}
		if (L_94 == 16)
		{
			goto IL_060e;
		}
		if (L_94 == 17)
		{
			goto IL_0626;
		}
		if (L_94 == 18)
		{
			goto IL_0645;
		}
		if (L_94 == 19)
		{
			goto IL_0664;
		}
		if (L_94 == 20)
		{
			goto IL_0683;
		}
		if (L_94 == 21)
		{
			goto IL_06a2;
		}
		if (L_94 == 22)
		{
			goto IL_06c1;
		}
		if (L_94 == 23)
		{
			goto IL_06e0;
		}
		if (L_94 == 24)
		{
			goto IL_06ff;
		}
		if (L_94 == 25)
		{
			goto IL_071e;
		}
		if (L_94 == 26)
		{
			goto IL_073d;
		}
		if (L_94 == 27)
		{
			goto IL_075c;
		}
		if (L_94 == 28)
		{
			goto IL_077b;
		}
		if (L_94 == 29)
		{
			goto IL_0796;
		}
		if (L_94 == 30)
		{
			goto IL_07b2;
		}
		if (L_94 == 31)
		{
			goto IL_07bf;
		}
		if (L_94 == 32)
		{
			goto IL_07de;
		}
		if (L_94 == 33)
		{
			goto IL_07fd;
		}
		if (L_94 == 34)
		{
			goto IL_081c;
		}
		if (L_94 == 35)
		{
			goto IL_083b;
		}
		if (L_94 == 36)
		{
			goto IL_085a;
		}
		if (L_94 == 37)
		{
			goto IL_0879;
		}
		if (L_94 == 38)
		{
			goto IL_0898;
		}
		if (L_94 == 39)
		{
			goto IL_08b7;
		}
		if (L_94 == 40)
		{
			goto IL_08d6;
		}
		if (L_94 == 41)
		{
			goto IL_08f5;
		}
		if (L_94 == 42)
		{
			goto IL_0914;
		}
		if (L_94 == 43)
		{
			goto IL_0933;
		}
		if (L_94 == 44)
		{
			goto IL_0952;
		}
		if (L_94 == 45)
		{
			goto IL_0971;
		}
		if (L_94 == 46)
		{
			goto IL_0990;
		}
		if (L_94 == 47)
		{
			goto IL_099d;
		}
		if (L_94 == 48)
		{
			goto IL_09c1;
		}
	}
	{
		goto IL_09e5;
	}

IL_048e:
	{
		Event_t6_162 * L_95 = V_0;
		NullCheck(L_95);
		Event_set_character_m6_1182(L_95, ((int32_t)48), /*hidden argument*/NULL);
		Event_t6_162 * L_96 = V_0;
		NullCheck(L_96);
		Event_set_keyCode_m6_1186(L_96, ((int32_t)256), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04a6:
	{
		Event_t6_162 * L_97 = V_0;
		NullCheck(L_97);
		Event_set_character_m6_1182(L_97, ((int32_t)49), /*hidden argument*/NULL);
		Event_t6_162 * L_98 = V_0;
		NullCheck(L_98);
		Event_set_keyCode_m6_1186(L_98, ((int32_t)257), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04be:
	{
		Event_t6_162 * L_99 = V_0;
		NullCheck(L_99);
		Event_set_character_m6_1182(L_99, ((int32_t)50), /*hidden argument*/NULL);
		Event_t6_162 * L_100 = V_0;
		NullCheck(L_100);
		Event_set_keyCode_m6_1186(L_100, ((int32_t)258), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04d6:
	{
		Event_t6_162 * L_101 = V_0;
		NullCheck(L_101);
		Event_set_character_m6_1182(L_101, ((int32_t)51), /*hidden argument*/NULL);
		Event_t6_162 * L_102 = V_0;
		NullCheck(L_102);
		Event_set_keyCode_m6_1186(L_102, ((int32_t)259), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_04ee:
	{
		Event_t6_162 * L_103 = V_0;
		NullCheck(L_103);
		Event_set_character_m6_1182(L_103, ((int32_t)52), /*hidden argument*/NULL);
		Event_t6_162 * L_104 = V_0;
		NullCheck(L_104);
		Event_set_keyCode_m6_1186(L_104, ((int32_t)260), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0506:
	{
		Event_t6_162 * L_105 = V_0;
		NullCheck(L_105);
		Event_set_character_m6_1182(L_105, ((int32_t)53), /*hidden argument*/NULL);
		Event_t6_162 * L_106 = V_0;
		NullCheck(L_106);
		Event_set_keyCode_m6_1186(L_106, ((int32_t)261), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_051e:
	{
		Event_t6_162 * L_107 = V_0;
		NullCheck(L_107);
		Event_set_character_m6_1182(L_107, ((int32_t)54), /*hidden argument*/NULL);
		Event_t6_162 * L_108 = V_0;
		NullCheck(L_108);
		Event_set_keyCode_m6_1186(L_108, ((int32_t)262), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0536:
	{
		Event_t6_162 * L_109 = V_0;
		NullCheck(L_109);
		Event_set_character_m6_1182(L_109, ((int32_t)55), /*hidden argument*/NULL);
		Event_t6_162 * L_110 = V_0;
		NullCheck(L_110);
		Event_set_keyCode_m6_1186(L_110, ((int32_t)263), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_054e:
	{
		Event_t6_162 * L_111 = V_0;
		NullCheck(L_111);
		Event_set_character_m6_1182(L_111, ((int32_t)56), /*hidden argument*/NULL);
		Event_t6_162 * L_112 = V_0;
		NullCheck(L_112);
		Event_set_keyCode_m6_1186(L_112, ((int32_t)264), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0566:
	{
		Event_t6_162 * L_113 = V_0;
		NullCheck(L_113);
		Event_set_character_m6_1182(L_113, ((int32_t)57), /*hidden argument*/NULL);
		Event_t6_162 * L_114 = V_0;
		NullCheck(L_114);
		Event_set_keyCode_m6_1186(L_114, ((int32_t)265), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_057e:
	{
		Event_t6_162 * L_115 = V_0;
		NullCheck(L_115);
		Event_set_character_m6_1182(L_115, ((int32_t)46), /*hidden argument*/NULL);
		Event_t6_162 * L_116 = V_0;
		NullCheck(L_116);
		Event_set_keyCode_m6_1186(L_116, ((int32_t)266), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0596:
	{
		Event_t6_162 * L_117 = V_0;
		NullCheck(L_117);
		Event_set_character_m6_1182(L_117, ((int32_t)47), /*hidden argument*/NULL);
		Event_t6_162 * L_118 = V_0;
		NullCheck(L_118);
		Event_set_keyCode_m6_1186(L_118, ((int32_t)267), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05ae:
	{
		Event_t6_162 * L_119 = V_0;
		NullCheck(L_119);
		Event_set_character_m6_1182(L_119, ((int32_t)45), /*hidden argument*/NULL);
		Event_t6_162 * L_120 = V_0;
		NullCheck(L_120);
		Event_set_keyCode_m6_1186(L_120, ((int32_t)269), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05c6:
	{
		Event_t6_162 * L_121 = V_0;
		NullCheck(L_121);
		Event_set_character_m6_1182(L_121, ((int32_t)43), /*hidden argument*/NULL);
		Event_t6_162 * L_122 = V_0;
		NullCheck(L_122);
		Event_set_keyCode_m6_1186(L_122, ((int32_t)270), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05de:
	{
		Event_t6_162 * L_123 = V_0;
		NullCheck(L_123);
		Event_set_character_m6_1182(L_123, ((int32_t)61), /*hidden argument*/NULL);
		Event_t6_162 * L_124 = V_0;
		NullCheck(L_124);
		Event_set_keyCode_m6_1186(L_124, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_05f6:
	{
		Event_t6_162 * L_125 = V_0;
		NullCheck(L_125);
		Event_set_character_m6_1182(L_125, ((int32_t)61), /*hidden argument*/NULL);
		Event_t6_162 * L_126 = V_0;
		NullCheck(L_126);
		Event_set_keyCode_m6_1186(L_126, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_060e:
	{
		Event_t6_162 * L_127 = V_0;
		NullCheck(L_127);
		Event_set_character_m6_1182(L_127, ((int32_t)10), /*hidden argument*/NULL);
		Event_t6_162 * L_128 = V_0;
		NullCheck(L_128);
		Event_set_keyCode_m6_1186(L_128, ((int32_t)271), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0626:
	{
		Event_t6_162 * L_129 = V_0;
		NullCheck(L_129);
		Event_set_keyCode_m6_1186(L_129, ((int32_t)273), /*hidden argument*/NULL);
		Event_t6_162 * L_130 = V_0;
		Event_t6_162 * L_131 = L_130;
		NullCheck(L_131);
		int32_t L_132 = Event_get_modifiers_m6_1175(L_131, /*hidden argument*/NULL);
		NullCheck(L_131);
		Event_set_modifiers_m6_1176(L_131, ((int32_t)((int32_t)L_132|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0645:
	{
		Event_t6_162 * L_133 = V_0;
		NullCheck(L_133);
		Event_set_keyCode_m6_1186(L_133, ((int32_t)274), /*hidden argument*/NULL);
		Event_t6_162 * L_134 = V_0;
		Event_t6_162 * L_135 = L_134;
		NullCheck(L_135);
		int32_t L_136 = Event_get_modifiers_m6_1175(L_135, /*hidden argument*/NULL);
		NullCheck(L_135);
		Event_set_modifiers_m6_1176(L_135, ((int32_t)((int32_t)L_136|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0664:
	{
		Event_t6_162 * L_137 = V_0;
		NullCheck(L_137);
		Event_set_keyCode_m6_1186(L_137, ((int32_t)276), /*hidden argument*/NULL);
		Event_t6_162 * L_138 = V_0;
		Event_t6_162 * L_139 = L_138;
		NullCheck(L_139);
		int32_t L_140 = Event_get_modifiers_m6_1175(L_139, /*hidden argument*/NULL);
		NullCheck(L_139);
		Event_set_modifiers_m6_1176(L_139, ((int32_t)((int32_t)L_140|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0683:
	{
		Event_t6_162 * L_141 = V_0;
		NullCheck(L_141);
		Event_set_keyCode_m6_1186(L_141, ((int32_t)275), /*hidden argument*/NULL);
		Event_t6_162 * L_142 = V_0;
		Event_t6_162 * L_143 = L_142;
		NullCheck(L_143);
		int32_t L_144 = Event_get_modifiers_m6_1175(L_143, /*hidden argument*/NULL);
		NullCheck(L_143);
		Event_set_modifiers_m6_1176(L_143, ((int32_t)((int32_t)L_144|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06a2:
	{
		Event_t6_162 * L_145 = V_0;
		NullCheck(L_145);
		Event_set_keyCode_m6_1186(L_145, ((int32_t)277), /*hidden argument*/NULL);
		Event_t6_162 * L_146 = V_0;
		Event_t6_162 * L_147 = L_146;
		NullCheck(L_147);
		int32_t L_148 = Event_get_modifiers_m6_1175(L_147, /*hidden argument*/NULL);
		NullCheck(L_147);
		Event_set_modifiers_m6_1176(L_147, ((int32_t)((int32_t)L_148|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06c1:
	{
		Event_t6_162 * L_149 = V_0;
		NullCheck(L_149);
		Event_set_keyCode_m6_1186(L_149, ((int32_t)278), /*hidden argument*/NULL);
		Event_t6_162 * L_150 = V_0;
		Event_t6_162 * L_151 = L_150;
		NullCheck(L_151);
		int32_t L_152 = Event_get_modifiers_m6_1175(L_151, /*hidden argument*/NULL);
		NullCheck(L_151);
		Event_set_modifiers_m6_1176(L_151, ((int32_t)((int32_t)L_152|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06e0:
	{
		Event_t6_162 * L_153 = V_0;
		NullCheck(L_153);
		Event_set_keyCode_m6_1186(L_153, ((int32_t)279), /*hidden argument*/NULL);
		Event_t6_162 * L_154 = V_0;
		Event_t6_162 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = Event_get_modifiers_m6_1175(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		Event_set_modifiers_m6_1176(L_155, ((int32_t)((int32_t)L_156|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_06ff:
	{
		Event_t6_162 * L_157 = V_0;
		NullCheck(L_157);
		Event_set_keyCode_m6_1186(L_157, ((int32_t)281), /*hidden argument*/NULL);
		Event_t6_162 * L_158 = V_0;
		Event_t6_162 * L_159 = L_158;
		NullCheck(L_159);
		int32_t L_160 = Event_get_modifiers_m6_1175(L_159, /*hidden argument*/NULL);
		NullCheck(L_159);
		Event_set_modifiers_m6_1176(L_159, ((int32_t)((int32_t)L_160|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_071e:
	{
		Event_t6_162 * L_161 = V_0;
		NullCheck(L_161);
		Event_set_keyCode_m6_1186(L_161, ((int32_t)280), /*hidden argument*/NULL);
		Event_t6_162 * L_162 = V_0;
		Event_t6_162 * L_163 = L_162;
		NullCheck(L_163);
		int32_t L_164 = Event_get_modifiers_m6_1175(L_163, /*hidden argument*/NULL);
		NullCheck(L_163);
		Event_set_modifiers_m6_1176(L_163, ((int32_t)((int32_t)L_164|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_073d:
	{
		Event_t6_162 * L_165 = V_0;
		NullCheck(L_165);
		Event_set_keyCode_m6_1186(L_165, ((int32_t)280), /*hidden argument*/NULL);
		Event_t6_162 * L_166 = V_0;
		Event_t6_162 * L_167 = L_166;
		NullCheck(L_167);
		int32_t L_168 = Event_get_modifiers_m6_1175(L_167, /*hidden argument*/NULL);
		NullCheck(L_167);
		Event_set_modifiers_m6_1176(L_167, ((int32_t)((int32_t)L_168|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_075c:
	{
		Event_t6_162 * L_169 = V_0;
		NullCheck(L_169);
		Event_set_keyCode_m6_1186(L_169, ((int32_t)281), /*hidden argument*/NULL);
		Event_t6_162 * L_170 = V_0;
		Event_t6_162 * L_171 = L_170;
		NullCheck(L_171);
		int32_t L_172 = Event_get_modifiers_m6_1175(L_171, /*hidden argument*/NULL);
		NullCheck(L_171);
		Event_set_modifiers_m6_1176(L_171, ((int32_t)((int32_t)L_172|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_077b:
	{
		Event_t6_162 * L_173 = V_0;
		NullCheck(L_173);
		Event_set_keyCode_m6_1186(L_173, 8, /*hidden argument*/NULL);
		Event_t6_162 * L_174 = V_0;
		Event_t6_162 * L_175 = L_174;
		NullCheck(L_175);
		int32_t L_176 = Event_get_modifiers_m6_1175(L_175, /*hidden argument*/NULL);
		NullCheck(L_175);
		Event_set_modifiers_m6_1176(L_175, ((int32_t)((int32_t)L_176|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0796:
	{
		Event_t6_162 * L_177 = V_0;
		NullCheck(L_177);
		Event_set_keyCode_m6_1186(L_177, ((int32_t)127), /*hidden argument*/NULL);
		Event_t6_162 * L_178 = V_0;
		Event_t6_162 * L_179 = L_178;
		NullCheck(L_179);
		int32_t L_180 = Event_get_modifiers_m6_1175(L_179, /*hidden argument*/NULL);
		NullCheck(L_179);
		Event_set_modifiers_m6_1176(L_179, ((int32_t)((int32_t)L_180|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07b2:
	{
		Event_t6_162 * L_181 = V_0;
		NullCheck(L_181);
		Event_set_keyCode_m6_1186(L_181, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07bf:
	{
		Event_t6_162 * L_182 = V_0;
		NullCheck(L_182);
		Event_set_keyCode_m6_1186(L_182, ((int32_t)282), /*hidden argument*/NULL);
		Event_t6_162 * L_183 = V_0;
		Event_t6_162 * L_184 = L_183;
		NullCheck(L_184);
		int32_t L_185 = Event_get_modifiers_m6_1175(L_184, /*hidden argument*/NULL);
		NullCheck(L_184);
		Event_set_modifiers_m6_1176(L_184, ((int32_t)((int32_t)L_185|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07de:
	{
		Event_t6_162 * L_186 = V_0;
		NullCheck(L_186);
		Event_set_keyCode_m6_1186(L_186, ((int32_t)283), /*hidden argument*/NULL);
		Event_t6_162 * L_187 = V_0;
		Event_t6_162 * L_188 = L_187;
		NullCheck(L_188);
		int32_t L_189 = Event_get_modifiers_m6_1175(L_188, /*hidden argument*/NULL);
		NullCheck(L_188);
		Event_set_modifiers_m6_1176(L_188, ((int32_t)((int32_t)L_189|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_07fd:
	{
		Event_t6_162 * L_190 = V_0;
		NullCheck(L_190);
		Event_set_keyCode_m6_1186(L_190, ((int32_t)284), /*hidden argument*/NULL);
		Event_t6_162 * L_191 = V_0;
		Event_t6_162 * L_192 = L_191;
		NullCheck(L_192);
		int32_t L_193 = Event_get_modifiers_m6_1175(L_192, /*hidden argument*/NULL);
		NullCheck(L_192);
		Event_set_modifiers_m6_1176(L_192, ((int32_t)((int32_t)L_193|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_081c:
	{
		Event_t6_162 * L_194 = V_0;
		NullCheck(L_194);
		Event_set_keyCode_m6_1186(L_194, ((int32_t)285), /*hidden argument*/NULL);
		Event_t6_162 * L_195 = V_0;
		Event_t6_162 * L_196 = L_195;
		NullCheck(L_196);
		int32_t L_197 = Event_get_modifiers_m6_1175(L_196, /*hidden argument*/NULL);
		NullCheck(L_196);
		Event_set_modifiers_m6_1176(L_196, ((int32_t)((int32_t)L_197|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_083b:
	{
		Event_t6_162 * L_198 = V_0;
		NullCheck(L_198);
		Event_set_keyCode_m6_1186(L_198, ((int32_t)286), /*hidden argument*/NULL);
		Event_t6_162 * L_199 = V_0;
		Event_t6_162 * L_200 = L_199;
		NullCheck(L_200);
		int32_t L_201 = Event_get_modifiers_m6_1175(L_200, /*hidden argument*/NULL);
		NullCheck(L_200);
		Event_set_modifiers_m6_1176(L_200, ((int32_t)((int32_t)L_201|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_085a:
	{
		Event_t6_162 * L_202 = V_0;
		NullCheck(L_202);
		Event_set_keyCode_m6_1186(L_202, ((int32_t)287), /*hidden argument*/NULL);
		Event_t6_162 * L_203 = V_0;
		Event_t6_162 * L_204 = L_203;
		NullCheck(L_204);
		int32_t L_205 = Event_get_modifiers_m6_1175(L_204, /*hidden argument*/NULL);
		NullCheck(L_204);
		Event_set_modifiers_m6_1176(L_204, ((int32_t)((int32_t)L_205|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0879:
	{
		Event_t6_162 * L_206 = V_0;
		NullCheck(L_206);
		Event_set_keyCode_m6_1186(L_206, ((int32_t)288), /*hidden argument*/NULL);
		Event_t6_162 * L_207 = V_0;
		Event_t6_162 * L_208 = L_207;
		NullCheck(L_208);
		int32_t L_209 = Event_get_modifiers_m6_1175(L_208, /*hidden argument*/NULL);
		NullCheck(L_208);
		Event_set_modifiers_m6_1176(L_208, ((int32_t)((int32_t)L_209|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0898:
	{
		Event_t6_162 * L_210 = V_0;
		NullCheck(L_210);
		Event_set_keyCode_m6_1186(L_210, ((int32_t)289), /*hidden argument*/NULL);
		Event_t6_162 * L_211 = V_0;
		Event_t6_162 * L_212 = L_211;
		NullCheck(L_212);
		int32_t L_213 = Event_get_modifiers_m6_1175(L_212, /*hidden argument*/NULL);
		NullCheck(L_212);
		Event_set_modifiers_m6_1176(L_212, ((int32_t)((int32_t)L_213|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08b7:
	{
		Event_t6_162 * L_214 = V_0;
		NullCheck(L_214);
		Event_set_keyCode_m6_1186(L_214, ((int32_t)290), /*hidden argument*/NULL);
		Event_t6_162 * L_215 = V_0;
		Event_t6_162 * L_216 = L_215;
		NullCheck(L_216);
		int32_t L_217 = Event_get_modifiers_m6_1175(L_216, /*hidden argument*/NULL);
		NullCheck(L_216);
		Event_set_modifiers_m6_1176(L_216, ((int32_t)((int32_t)L_217|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08d6:
	{
		Event_t6_162 * L_218 = V_0;
		NullCheck(L_218);
		Event_set_keyCode_m6_1186(L_218, ((int32_t)291), /*hidden argument*/NULL);
		Event_t6_162 * L_219 = V_0;
		Event_t6_162 * L_220 = L_219;
		NullCheck(L_220);
		int32_t L_221 = Event_get_modifiers_m6_1175(L_220, /*hidden argument*/NULL);
		NullCheck(L_220);
		Event_set_modifiers_m6_1176(L_220, ((int32_t)((int32_t)L_221|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_08f5:
	{
		Event_t6_162 * L_222 = V_0;
		NullCheck(L_222);
		Event_set_keyCode_m6_1186(L_222, ((int32_t)292), /*hidden argument*/NULL);
		Event_t6_162 * L_223 = V_0;
		Event_t6_162 * L_224 = L_223;
		NullCheck(L_224);
		int32_t L_225 = Event_get_modifiers_m6_1175(L_224, /*hidden argument*/NULL);
		NullCheck(L_224);
		Event_set_modifiers_m6_1176(L_224, ((int32_t)((int32_t)L_225|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0914:
	{
		Event_t6_162 * L_226 = V_0;
		NullCheck(L_226);
		Event_set_keyCode_m6_1186(L_226, ((int32_t)293), /*hidden argument*/NULL);
		Event_t6_162 * L_227 = V_0;
		Event_t6_162 * L_228 = L_227;
		NullCheck(L_228);
		int32_t L_229 = Event_get_modifiers_m6_1175(L_228, /*hidden argument*/NULL);
		NullCheck(L_228);
		Event_set_modifiers_m6_1176(L_228, ((int32_t)((int32_t)L_229|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0933:
	{
		Event_t6_162 * L_230 = V_0;
		NullCheck(L_230);
		Event_set_keyCode_m6_1186(L_230, ((int32_t)294), /*hidden argument*/NULL);
		Event_t6_162 * L_231 = V_0;
		Event_t6_162 * L_232 = L_231;
		NullCheck(L_232);
		int32_t L_233 = Event_get_modifiers_m6_1175(L_232, /*hidden argument*/NULL);
		NullCheck(L_232);
		Event_set_modifiers_m6_1176(L_232, ((int32_t)((int32_t)L_233|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0952:
	{
		Event_t6_162 * L_234 = V_0;
		NullCheck(L_234);
		Event_set_keyCode_m6_1186(L_234, ((int32_t)295), /*hidden argument*/NULL);
		Event_t6_162 * L_235 = V_0;
		Event_t6_162 * L_236 = L_235;
		NullCheck(L_236);
		int32_t L_237 = Event_get_modifiers_m6_1175(L_236, /*hidden argument*/NULL);
		NullCheck(L_236);
		Event_set_modifiers_m6_1176(L_236, ((int32_t)((int32_t)L_237|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0971:
	{
		Event_t6_162 * L_238 = V_0;
		NullCheck(L_238);
		Event_set_keyCode_m6_1186(L_238, ((int32_t)296), /*hidden argument*/NULL);
		Event_t6_162 * L_239 = V_0;
		Event_t6_162 * L_240 = L_239;
		NullCheck(L_240);
		int32_t L_241 = Event_get_modifiers_m6_1175(L_240, /*hidden argument*/NULL);
		NullCheck(L_240);
		Event_set_modifiers_m6_1176(L_240, ((int32_t)((int32_t)L_241|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_0990:
	{
		Event_t6_162 * L_242 = V_0;
		NullCheck(L_242);
		Event_set_keyCode_m6_1186(L_242, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_099d:
	{
		Event_t6_162 * L_243 = V_0;
		NullCheck(L_243);
		Event_set_character_m6_1182(L_243, ((int32_t)10), /*hidden argument*/NULL);
		Event_t6_162 * L_244 = V_0;
		NullCheck(L_244);
		Event_set_keyCode_m6_1186(L_244, ((int32_t)13), /*hidden argument*/NULL);
		Event_t6_162 * L_245 = V_0;
		Event_t6_162 * L_246 = L_245;
		NullCheck(L_246);
		int32_t L_247 = Event_get_modifiers_m6_1175(L_246, /*hidden argument*/NULL);
		NullCheck(L_246);
		Event_set_modifiers_m6_1176(L_246, ((int32_t)((int32_t)L_247&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_09c1:
	{
		Event_t6_162 * L_248 = V_0;
		NullCheck(L_248);
		Event_set_keyCode_m6_1186(L_248, ((int32_t)32), /*hidden argument*/NULL);
		Event_t6_162 * L_249 = V_0;
		NullCheck(L_249);
		Event_set_character_m6_1182(L_249, ((int32_t)32), /*hidden argument*/NULL);
		Event_t6_162 * L_250 = V_0;
		Event_t6_162 * L_251 = L_250;
		NullCheck(L_251);
		int32_t L_252 = Event_get_modifiers_m6_1175(L_251, /*hidden argument*/NULL);
		NullCheck(L_251);
		Event_set_modifiers_m6_1176(L_251, ((int32_t)((int32_t)L_252&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6b;
	}

IL_09e5:
	{
		String_t* L_253 = V_3;
		NullCheck(L_253);
		int32_t L_254 = String_get_Length_m1_571(L_253, /*hidden argument*/NULL);
		if ((((int32_t)L_254) == ((int32_t)1)))
		{
			goto IL_0a36;
		}
	}

IL_09f1:
	try
	{ // begin try (depth: 1)
		Event_t6_162 * L_255 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_256 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(KeyCode_t6_163_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_257 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Object_t * L_258 = Enum_Parse_m1_946(NULL /*static, unused*/, L_256, L_257, 1, /*hidden argument*/NULL);
		NullCheck(L_255);
		Event_set_keyCode_m6_1186(L_255, ((*(int32_t*)((int32_t*)UnBox (L_258, Int32_t1_3_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0a31;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t1_1425_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0a12;
		throw e;
	}

CATCH_0a12:
	{ // begin catch(System.ArgumentException)
		ObjectU5BU5D_t1_272* L_259 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		String_t* L_260 = V_3;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, 0);
		ArrayElementTypeCheck (L_259, L_260);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_259, 0, sizeof(Object_t *))) = (Object_t *)L_260;
		String_t* L_261 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4790, L_259, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_261, /*hidden argument*/NULL);
		goto IL_0a31;
	} // end catch (depth: 1)

IL_0a31:
	{
		goto IL_0a66;
	}

IL_0a36:
	{
		Event_t6_162 * L_262 = V_0;
		String_t* L_263 = V_3;
		NullCheck(L_263);
		String_t* L_264 = String_ToLower_m1_540(L_263, /*hidden argument*/NULL);
		NullCheck(L_264);
		uint16_t L_265 = String_get_Chars_m1_442(L_264, 0, /*hidden argument*/NULL);
		NullCheck(L_262);
		Event_set_character_m6_1182(L_262, L_265, /*hidden argument*/NULL);
		Event_t6_162 * L_266 = V_0;
		Event_t6_162 * L_267 = V_0;
		NullCheck(L_267);
		uint16_t L_268 = Event_get_character_m6_1181(L_267, /*hidden argument*/NULL);
		NullCheck(L_266);
		Event_set_keyCode_m6_1186(L_266, L_268, /*hidden argument*/NULL);
		Event_t6_162 * L_269 = V_0;
		NullCheck(L_269);
		int32_t L_270 = Event_get_modifiers_m6_1175(L_269, /*hidden argument*/NULL);
		if (!L_270)
		{
			goto IL_0a66;
		}
	}
	{
		Event_t6_162 * L_271 = V_0;
		NullCheck(L_271);
		Event_set_character_m6_1182(L_271, 0, /*hidden argument*/NULL);
	}

IL_0a66:
	{
		goto IL_0a6b;
	}

IL_0a6b:
	{
		Event_t6_162 * L_272 = V_0;
		return L_272;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C" int32_t Event_GetHashCode_m6_1156 (Event_t6_162 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t6_47  V_1 = {0};
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m6_1153(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m6_1185(__this, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint16_t)L_1)));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m6_1154(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t6_47  L_3 = Event_get_mousePosition_m6_1131(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m6_220((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern TypeInfo* Event_t6_162_il2cpp_TypeInfo_var;
extern "C" bool Event_Equals_m6_1157 (Event_t6_162 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t6_162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1639);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_162 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m1_8(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return 0;
	}

IL_0029:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Event_t6_162 *)CastclassSealed(L_6, Event_t6_162_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		Event_t6_162 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m6_1164(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		Event_t6_162 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m6_1175(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_005a;
		}
	}

IL_0058:
	{
		return 0;
	}

IL_005a:
	{
		bool L_13 = Event_get_isKey_m6_1153(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m6_1185(__this, /*hidden argument*/NULL);
		Event_t6_162 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m6_1185(L_15, /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
	}

IL_0074:
	{
		bool L_17 = Event_get_isMouse_m6_1154(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t6_47  L_18 = Event_get_mousePosition_m6_1131(__this, /*hidden argument*/NULL);
		Event_t6_162 * L_19 = V_0;
		NullCheck(L_19);
		Vector2_t6_47  L_20 = Event_get_mousePosition_m6_1131(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m6_233(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0091:
	{
		return 0;
	}
}
// System.String UnityEngine.Event::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* EventModifiers_t6_165_il2cpp_TypeInfo_var;
extern TypeInfo* KeyCode_t6_163_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t6_47_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4791;
extern Il2CppCodeGenString* _stringLiteral4792;
extern Il2CppCodeGenString* _stringLiteral4793;
extern Il2CppCodeGenString* _stringLiteral4794;
extern Il2CppCodeGenString* _stringLiteral4795;
extern Il2CppCodeGenString* _stringLiteral4796;
extern Il2CppCodeGenString* _stringLiteral4797;
extern "C" String_t* Event_ToString_m6_1158 (Event_t6_162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		EventType_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1641);
		EventModifiers_t6_165_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1642);
		KeyCode_t6_163_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1640);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Vector2_t6_47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1588);
		_stringLiteral4791 = il2cpp_codegen_string_literal_from_index(4791);
		_stringLiteral4792 = il2cpp_codegen_string_literal_from_index(4792);
		_stringLiteral4793 = il2cpp_codegen_string_literal_from_index(4793);
		_stringLiteral4794 = il2cpp_codegen_string_literal_from_index(4794);
		_stringLiteral4795 = il2cpp_codegen_string_literal_from_index(4795);
		_stringLiteral4796 = il2cpp_codegen_string_literal_from_index(4796);
		_stringLiteral4797 = il2cpp_codegen_string_literal_from_index(4797);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m6_1153(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00b5;
		}
	}
	{
		uint16_t L_1 = Event_get_character_m6_1181(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_2 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		int32_t L_3 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(EventModifiers_t6_165_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m6_1185(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(KeyCode_t6_163_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 2, sizeof(Object_t *))) = (Object_t *)L_13;
		String_t* L_14 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4791, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t1_272* L_15 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral4792);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4792;
		ObjectU5BU5D_t1_272* L_16 = L_15;
		int32_t L_17 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 1, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_272* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral4793);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4793;
		ObjectU5BU5D_t1_272* L_21 = L_20;
		uint16_t L_22 = Event_get_character_m6_1181(__this, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_272* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral4794);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4794;
		ObjectU5BU5D_t1_272* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Object_t * L_29 = Box(EventModifiers_t6_165_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 5, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t1_272* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral4795);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral4795;
		ObjectU5BU5D_t1_272* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m6_1185(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(KeyCode_t6_163_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 7, sizeof(Object_t *))) = (Object_t *)L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1_562(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		return L_35;
	}

IL_00b5:
	{
		bool L_36 = Event_get_isMouse_m6_1154(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00fb;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_37 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		int32_t L_38 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		int32_t L_39 = L_38;
		Object_t * L_40 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 0, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t1_272* L_41 = L_37;
		Vector2_t6_47  L_42 = Event_get_mousePosition_m6_1131(__this, /*hidden argument*/NULL);
		Vector2_t6_47  L_43 = L_42;
		Object_t * L_44 = Box(Vector2_t6_47_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_44);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 1, sizeof(Object_t *))) = (Object_t *)L_44;
		ObjectU5BU5D_t1_272* L_45 = L_41;
		int32_t L_46 = Event_get_modifiers_m6_1175(__this, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Object_t * L_48 = Box(EventModifiers_t6_165_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 2);
		ArrayElementTypeCheck (L_45, L_48);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_45, 2, sizeof(Object_t *))) = (Object_t *)L_48;
		String_t* L_49 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4796, L_45, /*hidden argument*/NULL);
		return L_49;
	}

IL_00fb:
	{
		int32_t L_50 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)((int32_t)14))))
		{
			goto IL_0115;
		}
	}
	{
		int32_t L_51 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_013d;
		}
	}

IL_0115:
	{
		ObjectU5BU5D_t1_272* L_52 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 2));
		int32_t L_53 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		int32_t L_54 = L_53;
		Object_t * L_55 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, 0, sizeof(Object_t *))) = (Object_t *)L_55;
		ObjectU5BU5D_t1_272* L_56 = L_52;
		String_t* L_57 = Event_get_commandName_m6_1183(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_57);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, 1, sizeof(Object_t *))) = (Object_t *)L_57;
		String_t* L_58 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4797, L_56, /*hidden argument*/NULL);
		return L_58;
	}

IL_013d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		int32_t L_60 = Event_get_type_m6_1164(__this, /*hidden argument*/NULL);
		int32_t L_61 = L_60;
		Object_t * L_62 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = String_Concat_m1_556(NULL /*static, unused*/, L_59, L_62, /*hidden argument*/NULL);
		return L_63;
	}
}
// System.Void UnityEngine.Event::Init()
extern "C" void Event_Init_m6_1159 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef void (*Event_Init_m6_1159_ftn) (Event_t6_162 *);
	static Event_Init_m6_1159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m6_1159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Cleanup()
extern "C" void Event_Cleanup_m6_1160 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m6_1160_ftn) (Event_t6_162 *);
	static Event_Cleanup_m6_1160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m6_1160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::InitCopy(UnityEngine.Event)
extern "C" void Event_InitCopy_m6_1161 (Event_t6_162 * __this, Event_t6_162 * ___other, const MethodInfo* method)
{
	typedef void (*Event_InitCopy_m6_1161_ftn) (Event_t6_162 *, Event_t6_162 *);
	static Event_InitCopy_m6_1161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitCopy_m6_1161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitCopy(UnityEngine.Event)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.Event::InitPtr(System.IntPtr)
extern "C" void Event_InitPtr_m6_1162 (Event_t6_162 * __this, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_InitPtr_m6_1162_ftn) (Event_t6_162 *, IntPtr_t);
	static Event_InitPtr_m6_1162_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_InitPtr_m6_1162_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::InitPtr(System.IntPtr)");
	_il2cpp_icall_func(__this, ___ptr);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C" int32_t Event_get_rawType_m6_1163 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m6_1163_ftn) (Event_t6_162 *);
	static Event_get_rawType_m6_1163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m6_1163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" int32_t Event_get_type_m6_1164 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m6_1164_ftn) (Event_t6_162 *);
	static Event_get_type_m6_1164_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m6_1164_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
extern "C" void Event_set_type_m6_1165 (Event_t6_162 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_type_m6_1165_ftn) (Event_t6_162 *, int32_t);
	static Event_set_type_m6_1165_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_type_m6_1165_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_type(UnityEngine.EventType)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
extern "C" int32_t Event_GetTypeForControl_m6_1166 (Event_t6_162 * __this, int32_t ___controlID, const MethodInfo* method)
{
	typedef int32_t (*Event_GetTypeForControl_m6_1166_ftn) (Event_t6_162 *, int32_t);
	static Event_GetTypeForControl_m6_1166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetTypeForControl_m6_1166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetTypeForControl(System.Int32)");
	return _il2cpp_icall_func(__this, ___controlID);
}
// System.Void UnityEngine.Event::Internal_SetMousePosition(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMousePosition_m6_1167 (Event_t6_162 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1168(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1168 (Object_t * __this /* static, unused */, Event_t6_162 * ___self, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1168_ftn) (Event_t6_162 *, Vector2_t6_47 *);
	static Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1168_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMousePosition_m6_1168_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMousePosition(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMousePosition_m6_1169 (Event_t6_162 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m6_1169_ftn) (Event_t6_162 *, Vector2_t6_47 *);
	static Event_Internal_GetMousePosition_m6_1169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m6_1169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Event::Internal_SetMouseDelta(UnityEngine.Vector2)
extern "C" void Event_Internal_SetMouseDelta_m6_1170 (Event_t6_162 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1171(NULL /*static, unused*/, __this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1171 (Object_t * __this /* static, unused */, Event_t6_162 * ___self, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1171_ftn) (Event_t6_162 *, Vector2_t6_47 *);
	static Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_INTERNAL_CALL_Internal_SetMouseDelta_m6_1171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::INTERNAL_CALL_Internal_SetMouseDelta(UnityEngine.Event,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___value);
}
// System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMouseDelta_m6_1172 (Event_t6_162 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMouseDelta_m6_1172_ftn) (Event_t6_162 *, Vector2_t6_47 *);
	static Event_Internal_GetMouseDelta_m6_1172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMouseDelta_m6_1172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_button()
extern "C" int32_t Event_get_button_m6_1173 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_button_m6_1173_ftn) (Event_t6_162 *);
	static Event_get_button_m6_1173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_button_m6_1173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_button()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_button(System.Int32)
extern "C" void Event_set_button_m6_1174 (Event_t6_162 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_button_m6_1174_ftn) (Event_t6_162 *, int32_t);
	static Event_set_button_m6_1174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_button_m6_1174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_button(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C" int32_t Event_get_modifiers_m6_1175 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m6_1175_ftn) (Event_t6_162 *);
	static Event_get_modifiers_m6_1175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m6_1175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
extern "C" void Event_set_modifiers_m6_1176 (Event_t6_162 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_modifiers_m6_1176_ftn) (Event_t6_162 *, int32_t);
	static Event_set_modifiers_m6_1176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_modifiers_m6_1176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Event::get_pressure()
extern "C" float Event_get_pressure_m6_1177 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef float (*Event_get_pressure_m6_1177_ftn) (Event_t6_162 *);
	static Event_get_pressure_m6_1177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_pressure_m6_1177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_pressure()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_pressure(System.Single)
extern "C" void Event_set_pressure_m6_1178 (Event_t6_162 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Event_set_pressure_m6_1178_ftn) (Event_t6_162 *, float);
	static Event_set_pressure_m6_1178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_pressure_m6_1178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_pressure(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Event::get_clickCount()
extern "C" int32_t Event_get_clickCount_m6_1179 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_clickCount_m6_1179_ftn) (Event_t6_162 *);
	static Event_get_clickCount_m6_1179_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_clickCount_m6_1179_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_clickCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_clickCount(System.Int32)
extern "C" void Event_set_clickCount_m6_1180 (Event_t6_162 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_clickCount_m6_1180_ftn) (Event_t6_162 *, int32_t);
	static Event_set_clickCount_m6_1180_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_clickCount_m6_1180_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_clickCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Char UnityEngine.Event::get_character()
extern "C" uint16_t Event_get_character_m6_1181 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef uint16_t (*Event_get_character_m6_1181_ftn) (Event_t6_162 *);
	static Event_get_character_m6_1181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m6_1181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_character(System.Char)
extern "C" void Event_set_character_m6_1182 (Event_t6_162 * __this, uint16_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_character_m6_1182_ftn) (Event_t6_162 *, uint16_t);
	static Event_set_character_m6_1182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_character_m6_1182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_character(System.Char)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Event::get_commandName()
extern "C" String_t* Event_get_commandName_m6_1183 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m6_1183_ftn) (Event_t6_162 *);
	static Event_get_commandName_m6_1183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m6_1183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_commandName(System.String)
extern "C" void Event_set_commandName_m6_1184 (Event_t6_162 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Event_set_commandName_m6_1184_ftn) (Event_t6_162 *, String_t*);
	static Event_set_commandName_m6_1184_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_commandName_m6_1184_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_commandName(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C" int32_t Event_get_keyCode_m6_1185 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m6_1185_ftn) (Event_t6_162 *);
	static Event_get_keyCode_m6_1185_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m6_1185_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
extern "C" void Event_set_keyCode_m6_1186 (Event_t6_162 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Event_set_keyCode_m6_1186_ftn) (Event_t6_162 *, int32_t);
	static Event_set_keyCode_m6_1186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_keyCode_m6_1186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C" void Event_Internal_SetNativeEvent_m6_1187 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m6_1187_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m6_1187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m6_1187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr);
}
// System.Void UnityEngine.Event::Use()
extern "C" void Event_Use_m6_1188 (Event_t6_162 * __this, const MethodInfo* method)
{
	typedef void (*Event_Use_m6_1188_ftn) (Event_t6_162 *);
	static Event_Use_m6_1188_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Use_m6_1188_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Use()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C" bool Event_PopEvent_m6_1189 (Object_t * __this /* static, unused */, Event_t6_162 * ___outEvent, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m6_1189_ftn) (Event_t6_162 *);
	static Event_PopEvent_m6_1189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m6_1189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent);
}
// System.Int32 UnityEngine.Event::GetEventCount()
extern "C" int32_t Event_GetEventCount_m6_1190 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Event_GetEventCount_m6_1190_ftn) ();
	static Event_GetEventCount_m6_1190_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetEventCount_m6_1190_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetEventCount()");
	return _il2cpp_icall_func();
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t6_162_marshal(const Event_t6_162& unmarshaled, Event_t6_162_marshaled& marshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
extern "C" void Event_t6_162_marshal_back(const Event_t6_162_marshaled& marshaled, Event_t6_162& unmarshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)___s_Current_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t6_162_marshal_cleanup(Event_t6_162_marshaled& marshaled)
{
}
// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C" void ScrollViewState__ctor_m6_1191 (ScrollViewState_t6_166 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C" void WindowFunction__ctor_m6_1192 (WindowFunction_t6_167 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUI/WindowFunction::Invoke(System.Int32)
extern "C" void WindowFunction_Invoke_m6_1193 (WindowFunction_t6_167 * __this, int32_t ___id, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WindowFunction_Invoke_m6_1193((WindowFunction_t6_167 *)__this->___prev_9,___id, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t6_167(Il2CppObject* delegate, int32_t ___id)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id);

	// Marshaling cleanup of parameter '___id' native representation

}
// System.IAsyncResult UnityEngine.GUI/WindowFunction::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" Object_t * WindowFunction_BeginInvoke_m6_1194 (WindowFunction_t6_167 * __this, int32_t ___id, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t1_3_il2cpp_TypeInfo_var, &___id);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUI/WindowFunction::EndInvoke(System.IAsyncResult)
extern "C" void WindowFunction_EndInvoke_m6_1195 (WindowFunction_t6_167 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.GUI::.cctor()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t6_170_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4798;
extern Il2CppCodeGenString* _stringLiteral4799;
extern Il2CppCodeGenString* _stringLiteral4800;
extern Il2CppCodeGenString* _stringLiteral4801;
extern Il2CppCodeGenString* _stringLiteral4802;
extern Il2CppCodeGenString* _stringLiteral4803;
extern Il2CppCodeGenString* _stringLiteral4804;
extern "C" void GUI__cctor_m6_1196 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GenericStack_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1644);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		_stringLiteral4798 = il2cpp_codegen_string_literal_from_index(4798);
		_stringLiteral4799 = il2cpp_codegen_string_literal_from_index(4799);
		_stringLiteral4800 = il2cpp_codegen_string_literal_from_index(4800);
		_stringLiteral4801 = il2cpp_codegen_string_literal_from_index(4801);
		_stringLiteral4802 = il2cpp_codegen_string_literal_from_index(4802);
		_stringLiteral4803 = il2cpp_codegen_string_literal_from_index(4803);
		_stringLiteral4804 = il2cpp_codegen_string_literal_from_index(4804);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0 = (10.0f);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2 = (-1);
		NullCheck(_stringLiteral4798);
		int32_t L_0 = String_GetHashCode_m1_577(_stringLiteral4798, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_BoxHash_3 = L_0;
		NullCheck(_stringLiteral4799);
		int32_t L_1 = String_GetHashCode_m1_577(_stringLiteral4799, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4 = L_1;
		NullCheck(_stringLiteral4800);
		int32_t L_2 = String_GetHashCode_m1_577(_stringLiteral4800, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ToggleHash_5 = L_2;
		NullCheck(_stringLiteral4801);
		int32_t L_3 = String_GetHashCode_m1_577(_stringLiteral4801, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ButtonGridHash_6 = L_3;
		NullCheck(_stringLiteral4802);
		int32_t L_4 = String_GetHashCode_m1_577(_stringLiteral4802, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7 = L_4;
		NullCheck(_stringLiteral4803);
		int32_t L_5 = String_GetHashCode_m1_577(_stringLiteral4803, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_BeginGroupHash_8 = L_5;
		NullCheck(_stringLiteral4804);
		int32_t L_6 = String_GetHashCode_m1_577(_stringLiteral4804, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollviewHash_9 = L_6;
		GenericStack_t6_170 * L_7 = (GenericStack_t6_170 *)il2cpp_codegen_object_new (GenericStack_t6_170_il2cpp_TypeInfo_var);
		GenericStack__ctor_m6_1823(L_7, /*hidden argument*/NULL);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_8 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_nextScrollStepTime_m6_1198(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime UnityEngine.GUI::get_nextScrollStepTime()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" DateTime_t1_150  GUI_get_nextScrollStepTime_m6_1197 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_set_nextScrollStepTime_m6_1198 (Object_t * __this /* static, unused */, DateTime_t1_150  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t1_150  L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.GUI::get_scrollTroughSide()
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" int32_t GUI_get_scrollTroughSide_m6_1199 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___U3CscrollTroughSideU3Ek__BackingField_14;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::set_scrollTroughSide(System.Int32)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_set_scrollTroughSide_m6_1200 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___U3CscrollTroughSideU3Ek__BackingField_14 = L_0;
		return;
	}
}
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_set_skin_m6_1201 (Object_t * __this /* static, unused */, GUISkin_t6_169 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_DoSetSkin_m6_1203(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" GUISkin_t6_169 * GUI_get_skin_m6_1202 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::DoSetSkin(UnityEngine.GUISkin)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_DoSetSkin_m6_1203 (Object_t * __this /* static, unused */, GUISkin_t6_169 * ___newSkin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t6_169 * L_0 = ___newSkin;
		bool L_1 = Object_op_Implicit_m6_720(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_2 = GUIUtility_GetDefaultSkin_m6_1501(NULL /*static, unused*/, /*hidden argument*/NULL);
		___newSkin = L_2;
	}

IL_0012:
	{
		GUISkin_t6_169 * L_3 = ___newSkin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_Skin_10 = L_3;
		GUISkin_t6_169 * L_4 = ___newSkin;
		NullCheck(L_4);
		GUISkin_MakeCurrent_m6_1400(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m6_1204 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, String_t* ___text, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_2 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_3 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_Label_m6_1205(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m6_1205 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_0 = ___position;
		GUIContent_t6_171 * L_1 = ___content;
		GUIStyle_t6_176 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_DoLabel_m6_1229(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Box(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_Box_m6_1206 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_BoxHash_3;
		int32_t L_1 = GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_162 * L_2 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1164(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)7))))
		{
			goto IL_002a;
		}
	}
	{
		GUIStyle_t6_176 * L_4 = ___style;
		Rect_t6_51  L_5 = ___position;
		GUIContent_t6_171 * L_6 = ___content;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		GUIStyle_Draw_m6_1439(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m6_1207 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, String_t* ___text, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_2 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_3 = ___style;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_5 = GUI_DoButton_m6_1231(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m6_1208 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_0 = ___position;
		GUIContent_t6_171 * L_1 = ___content;
		GUIStyle_t6_176 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_4 = GUI_DoButton_m6_1231(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUI::DoRepeatButton(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.FocusType)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoRepeatButton_m6_1209 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, int32_t ___focusType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	int32_t G_B13_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		int32_t L_1 = ___focusType;
		Rect_t6_51  L_2 = ___position;
		int32_t L_3 = GUIUtility_GetControlID_m6_1497(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Event_t6_162 * L_4 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = Event_GetTypeForControl_m6_1166(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if (!L_7)
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)7)))
		{
			goto IL_008e;
		}
	}
	{
		goto IL_00b7;
	}

IL_0037:
	{
		Event_t6_162 * L_10 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector2_t6_47  L_11 = Event_get_mousePosition_m6_1131(L_10, /*hidden argument*/NULL);
		bool L_12 = Rect_Contains_m6_320((&___position), L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Event_t6_162 * L_14 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_Use_m6_1188(L_14, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return 0;
	}

IL_005f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_15 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Event_t6_162 * L_17 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		Event_Use_m6_1188(L_17, /*hidden argument*/NULL);
		Event_t6_162 * L_18 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t6_47  L_19 = Event_get_mousePosition_m6_1131(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m6_320((&___position), L_19, /*hidden argument*/NULL);
		return L_20;
	}

IL_008c:
	{
		return 0;
	}

IL_008e:
	{
		GUIStyle_t6_176 * L_21 = ___style;
		Rect_t6_51  L_22 = ___position;
		GUIContent_t6_171 * L_23 = ___content;
		int32_t L_24 = V_0;
		NullCheck(L_21);
		GUIStyle_Draw_m6_1439(L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_26 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_00b5;
		}
	}
	{
		Event_t6_162 * L_27 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector2_t6_47  L_28 = Event_get_mousePosition_m6_1131(L_27, /*hidden argument*/NULL);
		bool L_29 = Rect_Contains_m6_320((&___position), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((int32_t)(L_29));
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B13_0 = 0;
	}

IL_00b6:
	{
		return G_B13_0;
	}

IL_00b7:
	{
		return 0;
	}
}
// System.String UnityEngine.GUI::PasswordFieldGetStrToShow(System.String,System.Char)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* GUI_PasswordFieldGetStrToShow_m6_1210 (Object_t * __this /* static, unused */, String_t* ___password, uint16_t ___maskChar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B4_0 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_001f;
		}
	}
	{
		Event_t6_162 * L_2 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1164(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0035;
		}
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		String_t* L_5 = ___password;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1_571(L_5, /*hidden argument*/NULL);
		uint16_t L_7 = ___maskChar;
		NullCheck(L_4);
		String_t* L_8 = String_PadRight_m1_530(L_4, L_6, L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		goto IL_0036;
	}

IL_0035:
	{
		String_t* L_9 = ___password;
		G_B4_0 = L_9;
	}

IL_0036:
	{
		return G_B4_0;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m6_1211 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = ___position;
		int32_t L_1 = ___id;
		GUIContent_t6_171 * L_2 = ___content;
		bool L_3 = ___multiline;
		int32_t L_4 = ___maxLength;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_DoTextField_m6_1212(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m6_1212 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, String_t* ___secureText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = ___position;
		int32_t L_1 = ___id;
		GUIContent_t6_171 * L_2 = ___content;
		bool L_3 = ___multiline;
		int32_t L_4 = ___maxLength;
		GUIStyle_t6_176 * L_5 = ___style;
		String_t* L_6 = ___secureText;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_DoTextField_m6_1213(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DoTextField(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char)
extern const Il2CppType* TextEditor_t6_254_0_0_0_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextEditor_t6_254_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_DoTextField_m6_1213 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, String_t* ___secureText, uint16_t ___maskChar, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_254_0_0_0_var = il2cpp_codegen_type_from_index(1647);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TextEditor_t6_254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1647);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	TextEditor_t6_254 * V_0 = {0};
	{
		int32_t L_0 = ___maxLength;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		GUIContent_t6_171 * L_1 = ___content;
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m6_1243(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1_571(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___maxLength;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}
	{
		GUIContent_t6_171 * L_5 = ___content;
		GUIContent_t6_171 * L_6 = ___content;
		NullCheck(L_6);
		String_t* L_7 = GUIContent_get_text_m6_1243(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___maxLength;
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m1_455(L_7, 0, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m6_1244(L_5, L_9, /*hidden argument*/NULL);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(TextEditor_t6_254_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_11 = ___id;
		Object_t * L_12 = GUIUtility_GetStateObject_m6_1498(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = ((TextEditor_t6_254 *)CastclassClass(L_12, TextEditor_t6_254_il2cpp_TypeInfo_var));
		TextEditor_t6_254 * L_13 = V_0;
		NullCheck(L_13);
		GUIContent_t6_171 * L_14 = (L_13->___content_2);
		GUIContent_t6_171 * L_15 = ___content;
		NullCheck(L_15);
		String_t* L_16 = GUIContent_get_text_m6_1243(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIContent_set_text_m6_1244(L_14, L_16, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_17 = V_0;
		NullCheck(L_17);
		TextEditor_SaveBackup_m6_1749(L_17, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_18 = V_0;
		Rect_t6_51  L_19 = ___position;
		NullCheck(L_18);
		TextEditor_set_position_m6_1676(L_18, L_19, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_20 = V_0;
		GUIStyle_t6_176 * L_21 = ___style;
		NullCheck(L_20);
		L_20->___style_3 = L_21;
		TextEditor_t6_254 * L_22 = V_0;
		bool L_23 = ___multiline;
		NullCheck(L_22);
		L_22->___multiline_4 = L_23;
		TextEditor_t6_254 * L_24 = V_0;
		int32_t L_25 = ___id;
		NullCheck(L_24);
		L_24->___controlID_1 = L_25;
		TextEditor_t6_254 * L_26 = V_0;
		NullCheck(L_26);
		TextEditor_DetectFocusChange_m6_1756(L_26, /*hidden argument*/NULL);
		bool L_27 = TouchScreenKeyboard_get_isSupported_m6_197(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00a4;
		}
	}
	{
		Rect_t6_51  L_28 = ___position;
		int32_t L_29 = ___id;
		GUIContent_t6_171 * L_30 = ___content;
		bool L_31 = ___multiline;
		int32_t L_32 = ___maxLength;
		GUIStyle_t6_176 * L_33 = ___style;
		String_t* L_34 = ___secureText;
		uint16_t L_35 = ___maskChar;
		TextEditor_t6_254 * L_36 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_HandleTextFieldEventForTouchscreen_m6_1214(NULL /*static, unused*/, L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		goto IL_00b2;
	}

IL_00a4:
	{
		Rect_t6_51  L_37 = ___position;
		int32_t L_38 = ___id;
		GUIContent_t6_171 * L_39 = ___content;
		bool L_40 = ___multiline;
		int32_t L_41 = ___maxLength;
		GUIStyle_t6_176 * L_42 = ___style;
		TextEditor_t6_254 * L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_HandleTextFieldEventForDesktop_m6_1215(NULL /*static, unused*/, L_37, L_38, L_39, L_40, L_41, L_42, L_43, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		TextEditor_t6_254 * L_44 = V_0;
		NullCheck(L_44);
		TextEditor_UpdateScrollOffsetIfNeeded_m6_1745(L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::HandleTextFieldEventForTouchscreen(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,System.String,System.Char,UnityEngine.TextEditor)
extern const Il2CppType* TextEditor_t6_254_0_0_0_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TextEditor_t6_254_il2cpp_TypeInfo_var;
extern "C" void GUI_HandleTextFieldEventForTouchscreen_m6_1214 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, String_t* ___secureText, uint16_t ___maskChar, TextEditor_t6_254 * ___editor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TextEditor_t6_254_0_0_0_var = il2cpp_codegen_type_from_index(1647);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TextEditor_t6_254_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1647);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_162 * V_0 = {0};
	TextEditor_t6_254 * V_1 = {0};
	String_t* V_2 = {0};
	int32_t V_3 = {0};
	TextEditor_t6_254 * G_B11_0 = {0};
	TextEditor_t6_254 * G_B10_0 = {0};
	String_t* G_B12_0 = {0};
	TextEditor_t6_254 * G_B12_1 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t6_162 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m6_1164(L_1, /*hidden argument*/NULL);
		V_3 = L_2;
		int32_t L_3 = V_3;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_0159;
	}

IL_001f:
	{
		Event_t6_162 * L_5 = V_0;
		NullCheck(L_5);
		Vector2_t6_47  L_6 = Event_get_mousePosition_m6_1131(L_5, /*hidden argument*/NULL);
		bool L_7 = Rect_Contains_m6_320((&___position), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00b6;
		}
	}
	{
		int32_t L_8 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_9 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2;
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_10 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2;
		int32_t L_11 = ___id;
		if ((((int32_t)L_10) == ((int32_t)L_11)))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(TextEditor_t6_254_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_13 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		Object_t * L_14 = GUIUtility_GetStateObject_m6_1498(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_1 = ((TextEditor_t6_254 *)CastclassClass(L_14, TextEditor_t6_254_il2cpp_TypeInfo_var));
		TextEditor_t6_254 * L_15 = V_1;
		NullCheck(L_15);
		L_15->___keyboardOnScreen_0 = (TouchScreenKeyboard_t6_45 *)NULL;
	}

IL_006e:
	{
		int32_t L_16 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_HotTextField_2 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_17 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_18 = ___id;
		if ((((int32_t)L_17) == ((int32_t)L_18)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_19 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_keyboardControl_m6_1513(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_0085:
	{
		TextEditor_t6_254 * L_20 = ___editor;
		String_t* L_21 = ___secureText;
		G_B10_0 = L_20;
		if (!L_21)
		{
			G_B11_0 = L_20;
			goto IL_0095;
		}
	}
	{
		String_t* L_22 = ___secureText;
		G_B12_0 = L_22;
		G_B12_1 = G_B10_0;
		goto IL_009b;
	}

IL_0095:
	{
		GUIContent_t6_171 * L_23 = ___content;
		NullCheck(L_23);
		String_t* L_24 = GUIContent_get_text_m6_1243(L_23, /*hidden argument*/NULL);
		G_B12_0 = L_24;
		G_B12_1 = G_B11_0;
	}

IL_009b:
	{
		bool L_25 = ___multiline;
		String_t* L_26 = ___secureText;
		TouchScreenKeyboard_t6_45 * L_27 = TouchScreenKeyboard_Open_m6_198(NULL /*static, unused*/, G_B12_0, 0, 1, L_25, ((((int32_t)((((Object_t*)(String_t*)L_26) == ((Object_t*)(Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		NullCheck(G_B12_1);
		G_B12_1->___keyboardOnScreen_0 = L_27;
		Event_t6_162 * L_28 = V_0;
		NullCheck(L_28);
		Event_Use_m6_1188(L_28, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		goto IL_0159;
	}

IL_00bb:
	{
		TextEditor_t6_254 * L_29 = ___editor;
		NullCheck(L_29);
		TouchScreenKeyboard_t6_45 * L_30 = (L_29->___keyboardOnScreen_0);
		if (!L_30)
		{
			goto IL_0126;
		}
	}
	{
		GUIContent_t6_171 * L_31 = ___content;
		TextEditor_t6_254 * L_32 = ___editor;
		NullCheck(L_32);
		TouchScreenKeyboard_t6_45 * L_33 = (L_32->___keyboardOnScreen_0);
		NullCheck(L_33);
		String_t* L_34 = TouchScreenKeyboard_get_text_m6_201(L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		GUIContent_set_text_m6_1244(L_31, L_34, /*hidden argument*/NULL);
		int32_t L_35 = ___maxLength;
		if ((((int32_t)L_35) < ((int32_t)0)))
		{
			goto IL_0107;
		}
	}
	{
		GUIContent_t6_171 * L_36 = ___content;
		NullCheck(L_36);
		String_t* L_37 = GUIContent_get_text_m6_1243(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = String_get_Length_m1_571(L_37, /*hidden argument*/NULL);
		int32_t L_39 = ___maxLength;
		if ((((int32_t)L_38) <= ((int32_t)L_39)))
		{
			goto IL_0107;
		}
	}
	{
		GUIContent_t6_171 * L_40 = ___content;
		GUIContent_t6_171 * L_41 = ___content;
		NullCheck(L_41);
		String_t* L_42 = GUIContent_get_text_m6_1243(L_41, /*hidden argument*/NULL);
		int32_t L_43 = ___maxLength;
		NullCheck(L_42);
		String_t* L_44 = String_Substring_m1_455(L_42, 0, L_43, /*hidden argument*/NULL);
		NullCheck(L_40);
		GUIContent_set_text_m6_1244(L_40, L_44, /*hidden argument*/NULL);
	}

IL_0107:
	{
		TextEditor_t6_254 * L_45 = ___editor;
		NullCheck(L_45);
		TouchScreenKeyboard_t6_45 * L_46 = (L_45->___keyboardOnScreen_0);
		NullCheck(L_46);
		bool L_47 = TouchScreenKeyboard_get_done_m6_206(L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0126;
		}
	}
	{
		TextEditor_t6_254 * L_48 = ___editor;
		NullCheck(L_48);
		L_48->___keyboardOnScreen_0 = (TouchScreenKeyboard_t6_45 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1228(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0126:
	{
		GUIContent_t6_171 * L_49 = ___content;
		NullCheck(L_49);
		String_t* L_50 = GUIContent_get_text_m6_1243(L_49, /*hidden argument*/NULL);
		V_2 = L_50;
		String_t* L_51 = ___secureText;
		if (!L_51)
		{
			goto IL_0142;
		}
	}
	{
		GUIContent_t6_171 * L_52 = ___content;
		String_t* L_53 = V_2;
		uint16_t L_54 = ___maskChar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		String_t* L_55 = GUI_PasswordFieldGetStrToShow_m6_1210(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		GUIContent_set_text_m6_1244(L_52, L_55, /*hidden argument*/NULL);
	}

IL_0142:
	{
		GUIStyle_t6_176 * L_56 = ___style;
		Rect_t6_51  L_57 = ___position;
		GUIContent_t6_171 * L_58 = ___content;
		int32_t L_59 = ___id;
		NullCheck(L_56);
		GUIStyle_Draw_m6_1440(L_56, L_57, L_58, L_59, 0, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_60 = ___content;
		String_t* L_61 = V_2;
		NullCheck(L_60);
		GUIContent_set_text_m6_1244(L_60, L_61, /*hidden argument*/NULL);
		goto IL_0159;
	}

IL_0159:
	{
		return;
	}
}
// System.Void UnityEngine.GUI::HandleTextFieldEventForDesktop(UnityEngine.Rect,System.Int32,UnityEngine.GUIContent,System.Boolean,System.Int32,UnityEngine.GUIStyle,UnityEngine.TextEditor)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUI_HandleTextFieldEventForDesktop_m6_1215 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, GUIContent_t6_171 * ___content, bool ___multiline, int32_t ___maxLength, GUIStyle_t6_176 * ___style, TextEditor_t6_254 * ___editor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_162 * V_0 = {0};
	bool V_1 = false;
	uint16_t V_2 = 0x0;
	Font_t6_149 * V_3 = {0};
	int32_t V_4 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		Event_t6_162 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m6_1164(L_1, /*hidden argument*/NULL);
		V_4 = L_2;
		int32_t L_3 = V_4;
		if (L_3 == 0)
		{
			goto IL_003c;
		}
		if (L_3 == 1)
		{
			goto IL_013c;
		}
		if (L_3 == 2)
		{
			goto IL_0271;
		}
		if (L_3 == 3)
		{
			goto IL_00f4;
		}
		if (L_3 == 4)
		{
			goto IL_0160;
		}
		if (L_3 == 5)
		{
			goto IL_0271;
		}
		if (L_3 == 6)
		{
			goto IL_0271;
		}
		if (L_3 == 7)
		{
			goto IL_0244;
		}
	}
	{
		goto IL_0271;
	}

IL_003c:
	{
		Event_t6_162 * L_4 = V_0;
		NullCheck(L_4);
		Vector2_t6_47  L_5 = Event_get_mousePosition_m6_1131(L_4, /*hidden argument*/NULL);
		bool L_6 = Rect_Contains_m6_320((&___position), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00ef;
		}
	}
	{
		int32_t L_7 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = ___id;
		GUIUtility_set_keyboardControl_m6_1513(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_9 = ___editor;
		NullCheck(L_9);
		L_9->___m_HasFocus_7 = 1;
		TextEditor_t6_254 * L_10 = ___editor;
		Event_t6_162 * L_11 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector2_t6_47  L_12 = Event_get_mousePosition_m6_1131(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		TextEditor_MoveCursorToPosition_m6_1710(L_10, L_12, /*hidden argument*/NULL);
		Event_t6_162 * L_13 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = Event_get_clickCount_m6_1179(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_00ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_15 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		GUISettings_t6_183 * L_16 = GUISkin_get_settings_m6_1394(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		bool L_17 = GUISettings_get_doubleClickSelectsWord_m6_1338(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ae;
		}
	}
	{
		TextEditor_t6_254 * L_18 = ___editor;
		NullCheck(L_18);
		TextEditor_SelectCurrentWord_m6_1742(L_18, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_19 = ___editor;
		NullCheck(L_19);
		TextEditor_DblClickSnap_m6_1719(L_19, 0, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_20 = ___editor;
		NullCheck(L_20);
		TextEditor_MouseDragSelectsWholeWords_m6_1718(L_20, 1, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		Event_t6_162 * L_21 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_clickCount_m6_1179(L_21, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)3))))
		{
			goto IL_00e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_23 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		GUISettings_t6_183 * L_24 = GUISkin_get_settings_m6_1394(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		bool L_25 = GUISettings_get_tripleClickSelectsLine_m6_1339(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e9;
		}
	}
	{
		TextEditor_t6_254 * L_26 = ___editor;
		NullCheck(L_26);
		TextEditor_SelectCurrentParagraph_m6_1744(L_26, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_27 = ___editor;
		NullCheck(L_27);
		TextEditor_MouseDragSelectsWholeWords_m6_1718(L_27, 1, /*hidden argument*/NULL);
		TextEditor_t6_254 * L_28 = ___editor;
		NullCheck(L_28);
		TextEditor_DblClickSnap_m6_1719(L_28, 1, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		Event_t6_162 * L_29 = V_0;
		NullCheck(L_29);
		Event_Use_m6_1188(L_29, /*hidden argument*/NULL);
	}

IL_00ef:
	{
		goto IL_0271;
	}

IL_00f4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_30 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_31 = ___id;
		if ((!(((uint32_t)L_30) == ((uint32_t)L_31))))
		{
			goto IL_0137;
		}
	}
	{
		Event_t6_162 * L_32 = V_0;
		NullCheck(L_32);
		bool L_33 = Event_get_shift_m6_1137(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0120;
		}
	}
	{
		TextEditor_t6_254 * L_34 = ___editor;
		Event_t6_162 * L_35 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector2_t6_47  L_36 = Event_get_mousePosition_m6_1131(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		TextEditor_MoveCursorToPosition_m6_1710(L_34, L_36, /*hidden argument*/NULL);
		goto IL_0131;
	}

IL_0120:
	{
		TextEditor_t6_254 * L_37 = ___editor;
		Event_t6_162 * L_38 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector2_t6_47  L_39 = Event_get_mousePosition_m6_1131(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		TextEditor_SelectToPosition_m6_1711(L_37, L_39, /*hidden argument*/NULL);
	}

IL_0131:
	{
		Event_t6_162 * L_40 = V_0;
		NullCheck(L_40);
		Event_Use_m6_1188(L_40, /*hidden argument*/NULL);
	}

IL_0137:
	{
		goto IL_0271;
	}

IL_013c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_41 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_42 = ___id;
		if ((!(((uint32_t)L_41) == ((uint32_t)L_42))))
		{
			goto IL_015b;
		}
	}
	{
		TextEditor_t6_254 * L_43 = ___editor;
		NullCheck(L_43);
		TextEditor_MouseDragSelectsWholeWords_m6_1718(L_43, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m6_1500(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Event_t6_162 * L_44 = V_0;
		NullCheck(L_44);
		Event_Use_m6_1188(L_44, /*hidden argument*/NULL);
	}

IL_015b:
	{
		goto IL_0271;
	}

IL_0160:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_45 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_46 = ___id;
		if ((((int32_t)L_45) == ((int32_t)L_46)))
		{
			goto IL_016c;
		}
	}
	{
		return;
	}

IL_016c:
	{
		TextEditor_t6_254 * L_47 = ___editor;
		Event_t6_162 * L_48 = V_0;
		NullCheck(L_47);
		bool L_49 = TextEditor_HandleKeyEvent_m6_1685(L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0198;
		}
	}
	{
		Event_t6_162 * L_50 = V_0;
		NullCheck(L_50);
		Event_Use_m6_1188(L_50, /*hidden argument*/NULL);
		V_1 = 1;
		GUIContent_t6_171 * L_51 = ___content;
		TextEditor_t6_254 * L_52 = ___editor;
		NullCheck(L_52);
		GUIContent_t6_171 * L_53 = (L_52->___content_2);
		NullCheck(L_53);
		String_t* L_54 = GUIContent_get_text_m6_1243(L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		GUIContent_set_text_m6_1244(L_51, L_54, /*hidden argument*/NULL);
		goto IL_0271;
	}

IL_0198:
	{
		Event_t6_162 * L_55 = V_0;
		NullCheck(L_55);
		int32_t L_56 = Event_get_keyCode_m6_1185(L_55, /*hidden argument*/NULL);
		if ((((int32_t)L_56) == ((int32_t)((int32_t)9))))
		{
			goto IL_01b2;
		}
	}
	{
		Event_t6_162 * L_57 = V_0;
		NullCheck(L_57);
		uint16_t L_58 = Event_get_character_m6_1181(L_57, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_01b3;
		}
	}

IL_01b2:
	{
		return;
	}

IL_01b3:
	{
		Event_t6_162 * L_59 = V_0;
		NullCheck(L_59);
		uint16_t L_60 = Event_get_character_m6_1181(L_59, /*hidden argument*/NULL);
		V_2 = L_60;
		uint16_t L_61 = V_2;
		if ((!(((uint32_t)L_61) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_01d4;
		}
	}
	{
		bool L_62 = ___multiline;
		if (L_62)
		{
			goto IL_01d4;
		}
	}
	{
		Event_t6_162 * L_63 = V_0;
		NullCheck(L_63);
		bool L_64 = Event_get_alt_m6_1141(L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_01d4;
		}
	}
	{
		return;
	}

IL_01d4:
	{
		GUIStyle_t6_176 * L_65 = ___style;
		NullCheck(L_65);
		Font_t6_149 * L_66 = GUIStyle_get_font_m6_1435(L_65, /*hidden argument*/NULL);
		V_3 = L_66;
		Font_t6_149 * L_67 = V_3;
		bool L_68 = Object_op_Implicit_m6_720(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_01f2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_69 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_69);
		Font_t6_149 * L_70 = GUISkin_get_font_m6_1350(L_69, /*hidden argument*/NULL);
		V_3 = L_70;
	}

IL_01f2:
	{
		Font_t6_149 * L_71 = V_3;
		uint16_t L_72 = V_2;
		NullCheck(L_71);
		bool L_73 = Font_HasCharacter_m6_1004(L_71, L_72, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_0206;
		}
	}
	{
		uint16_t L_74 = V_2;
		if ((!(((uint32_t)L_74) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0215;
		}
	}

IL_0206:
	{
		TextEditor_t6_254 * L_75 = ___editor;
		uint16_t L_76 = V_2;
		NullCheck(L_75);
		TextEditor_Insert_m6_1696(L_75, L_76, /*hidden argument*/NULL);
		V_1 = 1;
		goto IL_0271;
	}

IL_0215:
	{
		uint16_t L_77 = V_2;
		if (L_77)
		{
			goto IL_023f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		String_t* L_78 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = String_get_Length_m1_571(L_78, /*hidden argument*/NULL);
		if ((((int32_t)L_79) <= ((int32_t)0)))
		{
			goto IL_0239;
		}
	}
	{
		TextEditor_t6_254 * L_80 = ___editor;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_80);
		TextEditor_ReplaceSelection_m6_1695(L_80, L_81, /*hidden argument*/NULL);
		V_1 = 1;
	}

IL_0239:
	{
		Event_t6_162 * L_82 = V_0;
		NullCheck(L_82);
		Event_Use_m6_1188(L_82, /*hidden argument*/NULL);
	}

IL_023f:
	{
		goto IL_0271;
	}

IL_0244:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_83 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_84 = ___id;
		if ((((int32_t)L_83) == ((int32_t)L_84)))
		{
			goto IL_025f;
		}
	}
	{
		GUIStyle_t6_176 * L_85 = ___style;
		Rect_t6_51  L_86 = ___position;
		GUIContent_t6_171 * L_87 = ___content;
		int32_t L_88 = ___id;
		NullCheck(L_85);
		GUIStyle_Draw_m6_1440(L_85, L_86, L_87, L_88, 0, /*hidden argument*/NULL);
		goto IL_026c;
	}

IL_025f:
	{
		TextEditor_t6_254 * L_89 = ___editor;
		GUIContent_t6_171 * L_90 = ___content;
		NullCheck(L_90);
		String_t* L_91 = GUIContent_get_text_m6_1243(L_90, /*hidden argument*/NULL);
		NullCheck(L_89);
		TextEditor_DrawCursor_m6_1747(L_89, L_91, /*hidden argument*/NULL);
	}

IL_026c:
	{
		goto IL_0271;
	}

IL_0271:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_92 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_93 = ___id;
		if ((!(((uint32_t)L_92) == ((uint32_t)L_93))))
		{
			goto IL_0282;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_textFieldInput_m6_1520(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0282:
	{
		bool L_94 = V_1;
		if (!L_94)
		{
			goto IL_02d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1228(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_95 = ___content;
		TextEditor_t6_254 * L_96 = ___editor;
		NullCheck(L_96);
		GUIContent_t6_171 * L_97 = (L_96->___content_2);
		NullCheck(L_97);
		String_t* L_98 = GUIContent_get_text_m6_1243(L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		GUIContent_set_text_m6_1244(L_95, L_98, /*hidden argument*/NULL);
		int32_t L_99 = ___maxLength;
		if ((((int32_t)L_99) < ((int32_t)0)))
		{
			goto IL_02ce;
		}
	}
	{
		GUIContent_t6_171 * L_100 = ___content;
		NullCheck(L_100);
		String_t* L_101 = GUIContent_get_text_m6_1243(L_100, /*hidden argument*/NULL);
		NullCheck(L_101);
		int32_t L_102 = String_get_Length_m1_571(L_101, /*hidden argument*/NULL);
		int32_t L_103 = ___maxLength;
		if ((((int32_t)L_102) <= ((int32_t)L_103)))
		{
			goto IL_02ce;
		}
	}
	{
		GUIContent_t6_171 * L_104 = ___content;
		GUIContent_t6_171 * L_105 = ___content;
		NullCheck(L_105);
		String_t* L_106 = GUIContent_get_text_m6_1243(L_105, /*hidden argument*/NULL);
		int32_t L_107 = ___maxLength;
		NullCheck(L_106);
		String_t* L_108 = String_Substring_m1_455(L_106, 0, L_107, /*hidden argument*/NULL);
		NullCheck(L_104);
		GUIContent_set_text_m6_1244(L_104, L_108, /*hidden argument*/NULL);
	}

IL_02ce:
	{
		Event_t6_162 * L_109 = V_0;
		NullCheck(L_109);
		Event_Use_m6_1188(L_109, /*hidden argument*/NULL);
	}

IL_02d4:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUI::Toggle(UnityEngine.Rect,System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUI_Toggle_m6_1216 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, bool ___value, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_0 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_1 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ToggleHash_5;
		Rect_t6_51  L_2 = ___position;
		int32_t L_3 = GUIUtility_GetControlID_m6_1497(NULL /*static, unused*/, L_1, 0, L_2, /*hidden argument*/NULL);
		bool L_4 = ___value;
		GUIContent_t6_171 * L_5 = ___content;
		GUIStyle_t6_176 * L_6 = ___style;
		NullCheck(L_6);
		IntPtr_t L_7 = (L_6->___m_Ptr_0);
		bool L_8 = GUI_DoToggle_m6_1233(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Single UnityEngine.GUI::Slider(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" float GUI_Slider_m6_1217 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___start, float ___end, GUIStyle_t6_176 * ___slider, GUIStyle_t6_176 * ___thumb, bool ___horiz, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	SliderHandler_t6_245  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___size;
		float L_3 = ___start;
		float L_4 = ___end;
		GUIStyle_t6_176 * L_5 = ___slider;
		GUIStyle_t6_176 * L_6 = ___thumb;
		bool L_7 = ___horiz;
		int32_t L_8 = ___id;
		SliderHandler__ctor_m6_1630((&V_0), L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		float L_9 = SliderHandler_Handle_m6_1631((&V_0), /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Single UnityEngine.GUI::HorizontalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4805;
extern Il2CppCodeGenString* _stringLiteral4806;
extern Il2CppCodeGenString* _stringLiteral4807;
extern "C" float GUI_HorizontalScrollbar_m6_1218 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4805 = il2cpp_codegen_string_literal_from_index(4805);
		_stringLiteral4806 = il2cpp_codegen_string_literal_from_index(4806);
		_stringLiteral4807 = il2cpp_codegen_string_literal_from_index(4807);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___size;
		float L_3 = ___leftValue;
		float L_4 = ___rightValue;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_6 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_7 = ___style;
		NullCheck(L_7);
		String_t* L_8 = GUIStyle_get_name_m6_1455(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_559(NULL /*static, unused*/, L_8, _stringLiteral4805, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_t6_176 * L_10 = GUISkin_GetStyle_m6_1398(L_6, L_9, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_11 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_12 = ___style;
		NullCheck(L_12);
		String_t* L_13 = GUIStyle_get_name_m6_1455(L_12, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_m1_559(NULL /*static, unused*/, L_13, _stringLiteral4806, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUIStyle_t6_176 * L_15 = GUISkin_GetStyle_m6_1398(L_11, L_14, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_16 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_17 = ___style;
		NullCheck(L_17);
		String_t* L_18 = GUIStyle_get_name_m6_1455(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m1_559(NULL /*static, unused*/, L_18, _stringLiteral4807, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_t6_176 * L_20 = GUISkin_GetStyle_m6_1398(L_16, L_19, /*hidden argument*/NULL);
		float L_21 = GUI_Scroller_m6_1221(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_10, L_15, L_20, 1, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Boolean UnityEngine.GUI::ScrollerRepeatButton(System.Int32,UnityEngine.Rect,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t1_150_il2cpp_TypeInfo_var;
extern "C" bool GUI_ScrollerRepeatButton_m6_1219 (Object_t * __this /* static, unused */, int32_t ___scrollerID, Rect_t6_51  ___rect, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		DateTime_t1_150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	DateTime_t1_150  V_2 = {0};
	DateTime_t1_150  V_3 = {0};
	{
		V_0 = 0;
		Rect_t6_51  L_0 = ___rect;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_1 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		GUIStyle_t6_176 * L_2 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_3 = GUI_DoRepeatButton_m6_1209(NULL /*static, unused*/, L_0, L_1, L_2, 2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_4 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollControlId_1;
		int32_t L_5 = ___scrollerID;
		V_1 = ((((int32_t)((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_6 = ___scrollerID;
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollControlId_1 = L_6;
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		V_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_8 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_8;
		DateTime_t1_150  L_9 = DateTime_AddMilliseconds_m1_13836((&V_2), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1198(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_10 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_11 = GUI_get_nextScrollStepTime_m6_1197(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_12 = DateTime_op_GreaterThanOrEqual_m1_13905(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007f;
		}
	}
	{
		V_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t1_150_il2cpp_TypeInfo_var);
		DateTime_t1_150  L_13 = DateTime_get_Now_m1_13826(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_13;
		DateTime_t1_150  L_14 = DateTime_AddMilliseconds_m1_13836((&V_3), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m6_1198(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_007f:
	{
		Event_t6_162 * L_15 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Event_get_type_m6_1164(L_15, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)7))))
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m6_1236(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0094:
	{
		bool L_17 = V_0;
		return L_17;
	}
}
// System.Single UnityEngine.GUI::VerticalScrollbar(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4805;
extern Il2CppCodeGenString* _stringLiteral4808;
extern Il2CppCodeGenString* _stringLiteral4809;
extern "C" float GUI_VerticalScrollbar_m6_1220 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___topValue, float ___bottomValue, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4805 = il2cpp_codegen_string_literal_from_index(4805);
		_stringLiteral4808 = il2cpp_codegen_string_literal_from_index(4808);
		_stringLiteral4809 = il2cpp_codegen_string_literal_from_index(4809);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = ___position;
		float L_1 = ___value;
		float L_2 = ___size;
		float L_3 = ___topValue;
		float L_4 = ___bottomValue;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_6 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_7 = ___style;
		NullCheck(L_7);
		String_t* L_8 = GUIStyle_get_name_m6_1455(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_559(NULL /*static, unused*/, L_8, _stringLiteral4805, /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_t6_176 * L_10 = GUISkin_GetStyle_m6_1398(L_6, L_9, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_11 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_12 = ___style;
		NullCheck(L_12);
		String_t* L_13 = GUIStyle_get_name_m6_1455(L_12, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_m1_559(NULL /*static, unused*/, L_13, _stringLiteral4808, /*hidden argument*/NULL);
		NullCheck(L_11);
		GUIStyle_t6_176 * L_15 = GUISkin_GetStyle_m6_1398(L_11, L_14, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_16 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_17 = ___style;
		NullCheck(L_17);
		String_t* L_18 = GUIStyle_get_name_m6_1455(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m1_559(NULL /*static, unused*/, L_18, _stringLiteral4809, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIStyle_t6_176 * L_20 = GUISkin_GetStyle_m6_1398(L_16, L_19, /*hidden argument*/NULL);
		float L_21 = GUI_Scroller_m6_1221(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_10, L_15, L_20, 0, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Single UnityEngine.GUI::Scroller(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float GUI_Scroller_m6_1221 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, float ___value, float ___size, float ___leftValue, float ___rightValue, GUIStyle_t6_176 * ___slider, GUIStyle_t6_176 * ___thumb, GUIStyle_t6_176 * ___leftButton, GUIStyle_t6_176 * ___rightButton, bool ___horiz, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Rect_t6_51  V_1 = {0};
	Rect_t6_51  V_2 = {0};
	Rect_t6_51  V_3 = {0};
	bool V_4 = false;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B14_0 = 0.0f;
	float G_B14_1 = 0.0f;
	float G_B14_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		Rect_t6_51  L_1 = ___position;
		int32_t L_2 = GUIUtility_GetControlID_m6_1497(NULL /*static, unused*/, L_0, 2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = ___horiz;
		if (!L_3)
		{
			goto IL_00a7;
		}
	}
	{
		float L_4 = Rect_get_x_m6_298((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_5 = ___leftButton;
		NullCheck(L_5);
		float L_6 = GUIStyle_get_fixedWidth_m6_1468(L_5, /*hidden argument*/NULL);
		float L_7 = Rect_get_y_m6_300((&___position), /*hidden argument*/NULL);
		float L_8 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_9 = ___leftButton;
		NullCheck(L_9);
		float L_10 = GUIStyle_get_fixedWidth_m6_1468(L_9, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_11 = ___rightButton;
		NullCheck(L_11);
		float L_12 = GUIStyle_get_fixedWidth_m6_1468(L_11, /*hidden argument*/NULL);
		float L_13 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_1), ((float)((float)L_4+(float)L_6)), L_7, ((float)((float)((float)((float)L_8-(float)L_10))-(float)L_12)), L_13, /*hidden argument*/NULL);
		float L_14 = Rect_get_x_m6_298((&___position), /*hidden argument*/NULL);
		float L_15 = Rect_get_y_m6_300((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_16 = ___leftButton;
		NullCheck(L_16);
		float L_17 = GUIStyle_get_fixedWidth_m6_1468(L_16, /*hidden argument*/NULL);
		float L_18 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_2), L_14, L_15, L_17, L_18, /*hidden argument*/NULL);
		float L_19 = Rect_get_xMax_m6_315((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_20 = ___rightButton;
		NullCheck(L_20);
		float L_21 = GUIStyle_get_fixedWidth_m6_1468(L_20, /*hidden argument*/NULL);
		float L_22 = Rect_get_y_m6_300((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_23 = ___rightButton;
		NullCheck(L_23);
		float L_24 = GUIStyle_get_fixedWidth_m6_1468(L_23, /*hidden argument*/NULL);
		float L_25 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_3), ((float)((float)L_19-(float)L_21)), L_22, L_24, L_25, /*hidden argument*/NULL);
		goto IL_0130;
	}

IL_00a7:
	{
		float L_26 = Rect_get_x_m6_298((&___position), /*hidden argument*/NULL);
		float L_27 = Rect_get_y_m6_300((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_28 = ___leftButton;
		NullCheck(L_28);
		float L_29 = GUIStyle_get_fixedHeight_m6_1469(L_28, /*hidden argument*/NULL);
		float L_30 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		float L_31 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_32 = ___leftButton;
		NullCheck(L_32);
		float L_33 = GUIStyle_get_fixedHeight_m6_1469(L_32, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_34 = ___rightButton;
		NullCheck(L_34);
		float L_35 = GUIStyle_get_fixedHeight_m6_1469(L_34, /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_1), L_26, ((float)((float)L_27+(float)L_29)), L_30, ((float)((float)((float)((float)L_31-(float)L_33))-(float)L_35)), /*hidden argument*/NULL);
		float L_36 = Rect_get_x_m6_298((&___position), /*hidden argument*/NULL);
		float L_37 = Rect_get_y_m6_300((&___position), /*hidden argument*/NULL);
		float L_38 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_39 = ___leftButton;
		NullCheck(L_39);
		float L_40 = GUIStyle_get_fixedHeight_m6_1469(L_39, /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_2), L_36, L_37, L_38, L_40, /*hidden argument*/NULL);
		float L_41 = Rect_get_x_m6_298((&___position), /*hidden argument*/NULL);
		float L_42 = Rect_get_yMax_m6_317((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_43 = ___rightButton;
		NullCheck(L_43);
		float L_44 = GUIStyle_get_fixedHeight_m6_1469(L_43, /*hidden argument*/NULL);
		float L_45 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_46 = ___rightButton;
		NullCheck(L_46);
		float L_47 = GUIStyle_get_fixedHeight_m6_1469(L_46, /*hidden argument*/NULL);
		Rect__ctor_m6_295((&V_3), L_41, ((float)((float)L_42-(float)L_44)), L_45, L_47, /*hidden argument*/NULL);
	}

IL_0130:
	{
		Rect_t6_51  L_48 = V_1;
		float L_49 = ___value;
		float L_50 = ___size;
		float L_51 = ___leftValue;
		float L_52 = ___rightValue;
		GUIStyle_t6_176 * L_53 = ___slider;
		GUIStyle_t6_176 * L_54 = ___thumb;
		bool L_55 = ___horiz;
		int32_t L_56 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		float L_57 = GUI_Slider_m6_1217(NULL /*static, unused*/, L_48, L_49, L_50, L_51, L_52, L_53, L_54, L_55, L_56, /*hidden argument*/NULL);
		___value = L_57;
		V_4 = 0;
		Event_t6_162 * L_58 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_58);
		int32_t L_59 = Event_get_type_m6_1164(L_58, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_59) == ((uint32_t)1))))
		{
			goto IL_015a;
		}
	}
	{
		V_4 = 1;
	}

IL_015a:
	{
		int32_t L_60 = V_0;
		Rect_t6_51  L_61 = V_2;
		GUIStyle_t6_176 * L_62 = ___leftButton;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_63 = GUI_ScrollerRepeatButton_m6_1219(NULL /*static, unused*/, L_60, L_61, L_62, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0189;
		}
	}
	{
		float L_64 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		float L_65 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0;
		float L_66 = ___leftValue;
		float L_67 = ___rightValue;
		G_B7_0 = L_65;
		G_B7_1 = L_64;
		if ((!(((float)L_66) < ((float)L_67))))
		{
			G_B8_0 = L_65;
			G_B8_1 = L_64;
			goto IL_0180;
		}
	}
	{
		G_B9_0 = (1.0f);
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_0185;
	}

IL_0180:
	{
		G_B9_0 = (-1.0f);
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_0185:
	{
		___value = ((float)((float)G_B9_2-(float)((float)((float)G_B9_1*(float)G_B9_0))));
	}

IL_0189:
	{
		int32_t L_68 = V_0;
		Rect_t6_51  L_69 = V_3;
		GUIStyle_t6_176 * L_70 = ___rightButton;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_71 = GUI_ScrollerRepeatButton_m6_1219(NULL /*static, unused*/, L_68, L_69, L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_01b8;
		}
	}
	{
		float L_72 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		float L_73 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollStepSize_0;
		float L_74 = ___leftValue;
		float L_75 = ___rightValue;
		G_B12_0 = L_73;
		G_B12_1 = L_72;
		if ((!(((float)L_74) < ((float)L_75))))
		{
			G_B13_0 = L_73;
			G_B13_1 = L_72;
			goto IL_01af;
		}
	}
	{
		G_B14_0 = (1.0f);
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_01b4;
	}

IL_01af:
	{
		G_B14_0 = (-1.0f);
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_01b4:
	{
		___value = ((float)((float)G_B14_2+(float)((float)((float)G_B14_1*(float)G_B14_0))));
	}

IL_01b8:
	{
		bool L_76 = V_4;
		if (!L_76)
		{
			goto IL_01d6;
		}
	}
	{
		Event_t6_162 * L_77 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_77);
		int32_t L_78 = Event_get_type_m6_1164(L_77, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_78) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_01d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollControlId_1 = 0;
	}

IL_01d6:
	{
		float L_79 = ___leftValue;
		float L_80 = ___rightValue;
		if ((!(((float)L_79) < ((float)L_80))))
		{
			goto IL_01f0;
		}
	}
	{
		float L_81 = ___value;
		float L_82 = ___leftValue;
		float L_83 = ___rightValue;
		float L_84 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_85 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_81, L_82, ((float)((float)L_83-(float)L_84)), /*hidden argument*/NULL);
		___value = L_85;
		goto IL_01fd;
	}

IL_01f0:
	{
		float L_86 = ___value;
		float L_87 = ___rightValue;
		float L_88 = ___leftValue;
		float L_89 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_90 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_86, L_87, ((float)((float)L_88-(float)L_89)), /*hidden argument*/NULL);
		___value = L_90;
	}

IL_01fd:
	{
		float L_91 = ___value;
		return L_91;
	}
}
// System.Void UnityEngine.GUI::BeginGroup(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUI_BeginGroup_m6_1222 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_BeginGroupHash_8;
		int32_t L_1 = GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIContent_t6_171 * L_2 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_3 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		if ((!(((Object_t*)(GUIContent_t6_171 *)L_2) == ((Object_t*)(GUIContent_t6_171 *)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		GUIStyle_t6_176 * L_4 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_5 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_4) == ((Object_t*)(GUIStyle_t6_176 *)L_5)))
		{
			goto IL_006d;
		}
	}

IL_0027:
	{
		Event_t6_162 * L_6 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = Event_get_type_m6_1164(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)7)))
		{
			goto IL_003e;
		}
	}
	{
		goto IL_004c;
	}

IL_003e:
	{
		GUIStyle_t6_176 * L_9 = ___style;
		Rect_t6_51  L_10 = ___position;
		GUIContent_t6_171 * L_11 = ___content;
		int32_t L_12 = V_0;
		NullCheck(L_9);
		GUIStyle_Draw_m6_1439(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		goto IL_006d;
	}

IL_004c:
	{
		Event_t6_162 * L_13 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector2_t6_47  L_14 = Event_get_mousePosition_m6_1131(L_13, /*hidden argument*/NULL);
		bool L_15 = Rect_Contains_m6_320((&___position), L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_set_mouseUsed_m6_1519(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0068:
	{
		goto IL_006d;
	}

IL_006d:
	{
		Rect_t6_51  L_16 = ___position;
		Vector2_t6_47  L_17 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t6_47  L_18 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Push_m6_1521(NULL /*static, unused*/, L_16, L_17, L_18, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::EndGroup()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void GUI_EndGroup_m6_1223 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Pop_m6_1523(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.GUI::BeginScrollView(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle)
extern const Il2CppType* ScrollViewState_t6_166_0_0_0_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ScrollViewState_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  GUI_BeginScrollView_m6_1224 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, Vector2_t6_47  ___scrollPosition, Rect_t6_51  ___viewRect, bool ___alwaysShowHorizontal, bool ___alwaysShowVertical, GUIStyle_t6_176 * ___horizontalScrollbar, GUIStyle_t6_176 * ___verticalScrollbar, GUIStyle_t6_176 * ___background, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScrollViewState_t6_166_0_0_0_var = il2cpp_codegen_type_from_index(1649);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ScrollViewState_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1649);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ScrollViewState_t6_166 * V_1 = {0};
	Rect_t6_51  V_2 = {0};
	bool V_3 = false;
	bool V_4 = false;
	Rect_t6_51  V_5 = {0};
	int32_t V_6 = {0};
	int32_t G_B19_0 = 0;
	bool G_B19_1 = false;
	Rect_t6_51  G_B19_2 = {0};
	GUIStyle_t6_176 * G_B19_3 = {0};
	int32_t G_B18_0 = 0;
	bool G_B18_1 = false;
	Rect_t6_51  G_B18_2 = {0};
	GUIStyle_t6_176 * G_B18_3 = {0};
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	bool G_B20_2 = false;
	Rect_t6_51  G_B20_3 = {0};
	GUIStyle_t6_176 * G_B20_4 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollviewHash_9;
		int32_t L_1 = GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(ScrollViewState_t6_166_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		Object_t * L_4 = GUIUtility_GetStateObject_m6_1498(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = ((ScrollViewState_t6_166 *)CastclassSealed(L_4, ScrollViewState_t6_166_il2cpp_TypeInfo_var));
		ScrollViewState_t6_166 * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = (L_5->___apply_4);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		ScrollViewState_t6_166 * L_7 = V_1;
		NullCheck(L_7);
		Vector2_t6_47  L_8 = (L_7->___scrollPosition_3);
		___scrollPosition = L_8;
		ScrollViewState_t6_166 * L_9 = V_1;
		NullCheck(L_9);
		L_9->___apply_4 = 0;
	}

IL_0041:
	{
		ScrollViewState_t6_166 * L_10 = V_1;
		Rect_t6_51  L_11 = ___position;
		NullCheck(L_10);
		L_10->___position_0 = L_11;
		ScrollViewState_t6_166 * L_12 = V_1;
		Vector2_t6_47  L_13 = ___scrollPosition;
		NullCheck(L_12);
		L_12->___scrollPosition_3 = L_13;
		ScrollViewState_t6_166 * L_14 = V_1;
		ScrollViewState_t6_166 * L_15 = V_1;
		Rect_t6_51  L_16 = ___viewRect;
		Rect_t6_51  L_17 = L_16;
		V_5 = L_17;
		NullCheck(L_15);
		L_15->___viewRect_2 = L_17;
		Rect_t6_51  L_18 = V_5;
		NullCheck(L_14);
		L_14->___visibleRect_1 = L_18;
		ScrollViewState_t6_166 * L_19 = V_1;
		NullCheck(L_19);
		Rect_t6_51 * L_20 = &(L_19->___visibleRect_1);
		float L_21 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_20, L_21, /*hidden argument*/NULL);
		ScrollViewState_t6_166 * L_22 = V_1;
		NullCheck(L_22);
		Rect_t6_51 * L_23 = &(L_22->___visibleRect_1);
		float L_24 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_23, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GenericStack_t6_170 * L_25 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12;
		ScrollViewState_t6_166 * L_26 = V_1;
		NullCheck(L_25);
		VirtActionInvoker1< Object_t * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_25, L_26);
		Rect_t6_51  L_27 = ___position;
		Rect__ctor_m6_296((&V_2), L_27, /*hidden argument*/NULL);
		Event_t6_162 * L_28 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = Event_get_type_m6_1164(L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		int32_t L_30 = V_6;
		if ((((int32_t)L_30) == ((int32_t)8)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_31 = V_6;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)12))))
		{
			goto IL_0107;
		}
	}
	{
		goto IL_010c;
	}

IL_00ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_32 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_32, 2, /*hidden argument*/NULL);
		int32_t L_33 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_33, 2, /*hidden argument*/NULL);
		int32_t L_34 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_34, 2, /*hidden argument*/NULL);
		int32_t L_35 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_35, 2, /*hidden argument*/NULL);
		int32_t L_36 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_36, 2, /*hidden argument*/NULL);
		int32_t L_37 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_37, 2, /*hidden argument*/NULL);
		goto IL_044a;
	}

IL_0107:
	{
		goto IL_044a;
	}

IL_010c:
	{
		bool L_38 = ___alwaysShowVertical;
		V_3 = L_38;
		bool L_39 = ___alwaysShowHorizontal;
		V_4 = L_39;
		bool L_40 = V_4;
		if (L_40)
		{
			goto IL_012c;
		}
	}
	{
		float L_41 = Rect_get_width_m6_306((&___viewRect), /*hidden argument*/NULL);
		float L_42 = Rect_get_width_m6_306((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_41) > ((float)L_42))))
		{
			goto IL_017a;
		}
	}

IL_012c:
	{
		ScrollViewState_t6_166 * L_43 = V_1;
		NullCheck(L_43);
		Rect_t6_51 * L_44 = &(L_43->___visibleRect_1);
		float L_45 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_46 = ___horizontalScrollbar;
		NullCheck(L_46);
		float L_47 = GUIStyle_get_fixedHeight_m6_1469(L_46, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_48 = ___horizontalScrollbar;
		NullCheck(L_48);
		RectOffset_t6_178 * L_49 = GUIStyle_get_margin_m6_1433(L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		int32_t L_50 = RectOffset_get_top_m6_1420(L_49, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_44, ((float)((float)((float)((float)L_45-(float)L_47))+(float)(((float)((float)L_50))))), /*hidden argument*/NULL);
		Rect_t6_51 * L_51 = (&V_2);
		float L_52 = Rect_get_height_m6_308(L_51, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_53 = ___horizontalScrollbar;
		NullCheck(L_53);
		float L_54 = GUIStyle_get_fixedHeight_m6_1469(L_53, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_55 = ___horizontalScrollbar;
		NullCheck(L_55);
		RectOffset_t6_178 * L_56 = GUIStyle_get_margin_m6_1433(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = RectOffset_get_top_m6_1420(L_56, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_51, ((float)((float)L_52-(float)((float)((float)L_54+(float)(((float)((float)L_57))))))), /*hidden argument*/NULL);
		V_4 = 1;
	}

IL_017a:
	{
		bool L_58 = V_3;
		if (L_58)
		{
			goto IL_0193;
		}
	}
	{
		float L_59 = Rect_get_height_m6_308((&___viewRect), /*hidden argument*/NULL);
		float L_60 = Rect_get_height_m6_308((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_59) > ((float)L_60))))
		{
			goto IL_0248;
		}
	}

IL_0193:
	{
		ScrollViewState_t6_166 * L_61 = V_1;
		NullCheck(L_61);
		Rect_t6_51 * L_62 = &(L_61->___visibleRect_1);
		float L_63 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_64 = ___verticalScrollbar;
		NullCheck(L_64);
		float L_65 = GUIStyle_get_fixedWidth_m6_1468(L_64, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_66 = ___verticalScrollbar;
		NullCheck(L_66);
		RectOffset_t6_178 * L_67 = GUIStyle_get_margin_m6_1433(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		int32_t L_68 = RectOffset_get_left_m6_1416(L_67, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_62, ((float)((float)((float)((float)L_63-(float)L_65))+(float)(((float)((float)L_68))))), /*hidden argument*/NULL);
		Rect_t6_51 * L_69 = (&V_2);
		float L_70 = Rect_get_width_m6_306(L_69, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_71 = ___verticalScrollbar;
		NullCheck(L_71);
		float L_72 = GUIStyle_get_fixedWidth_m6_1468(L_71, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_73 = ___verticalScrollbar;
		NullCheck(L_73);
		RectOffset_t6_178 * L_74 = GUIStyle_get_margin_m6_1433(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		int32_t L_75 = RectOffset_get_left_m6_1416(L_74, /*hidden argument*/NULL);
		Rect_set_width_m6_307(L_69, ((float)((float)L_70-(float)((float)((float)L_72+(float)(((float)((float)L_75))))))), /*hidden argument*/NULL);
		V_3 = 1;
		bool L_76 = V_4;
		if (L_76)
		{
			goto IL_0248;
		}
	}
	{
		float L_77 = Rect_get_width_m6_306((&___viewRect), /*hidden argument*/NULL);
		float L_78 = Rect_get_width_m6_306((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_77) > ((float)L_78))))
		{
			goto IL_0248;
		}
	}
	{
		ScrollViewState_t6_166 * L_79 = V_1;
		NullCheck(L_79);
		Rect_t6_51 * L_80 = &(L_79->___visibleRect_1);
		float L_81 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_82 = ___horizontalScrollbar;
		NullCheck(L_82);
		float L_83 = GUIStyle_get_fixedHeight_m6_1469(L_82, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_84 = ___horizontalScrollbar;
		NullCheck(L_84);
		RectOffset_t6_178 * L_85 = GUIStyle_get_margin_m6_1433(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		int32_t L_86 = RectOffset_get_top_m6_1420(L_85, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_80, ((float)((float)((float)((float)L_81-(float)L_83))+(float)(((float)((float)L_86))))), /*hidden argument*/NULL);
		Rect_t6_51 * L_87 = (&V_2);
		float L_88 = Rect_get_height_m6_308(L_87, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_89 = ___horizontalScrollbar;
		NullCheck(L_89);
		float L_90 = GUIStyle_get_fixedHeight_m6_1469(L_89, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_91 = ___horizontalScrollbar;
		NullCheck(L_91);
		RectOffset_t6_178 * L_92 = GUIStyle_get_margin_m6_1433(L_91, /*hidden argument*/NULL);
		NullCheck(L_92);
		int32_t L_93 = RectOffset_get_top_m6_1420(L_92, /*hidden argument*/NULL);
		Rect_set_height_m6_309(L_87, ((float)((float)L_88-(float)((float)((float)L_90+(float)(((float)((float)L_93))))))), /*hidden argument*/NULL);
		V_4 = 1;
	}

IL_0248:
	{
		Event_t6_162 * L_94 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_94);
		int32_t L_95 = Event_get_type_m6_1164(L_94, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_95) == ((uint32_t)7))))
		{
			goto IL_028a;
		}
	}
	{
		GUIStyle_t6_176 * L_96 = ___background;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_97 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_96) == ((Object_t*)(GUIStyle_t6_176 *)L_97)))
		{
			goto IL_028a;
		}
	}
	{
		GUIStyle_t6_176 * L_98 = ___background;
		Rect_t6_51  L_99 = ___position;
		Event_t6_162 * L_100 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_100);
		Vector2_t6_47  L_101 = Event_get_mousePosition_m6_1131(L_100, /*hidden argument*/NULL);
		bool L_102 = Rect_Contains_m6_320((&___position), L_101, /*hidden argument*/NULL);
		bool L_103 = V_4;
		G_B18_0 = 0;
		G_B18_1 = L_102;
		G_B18_2 = L_99;
		G_B18_3 = L_98;
		if (!L_103)
		{
			G_B19_0 = 0;
			G_B19_1 = L_102;
			G_B19_2 = L_99;
			G_B19_3 = L_98;
			goto IL_0283;
		}
	}
	{
		bool L_104 = V_3;
		G_B20_0 = ((int32_t)(L_104));
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		G_B20_4 = G_B18_3;
		goto IL_0284;
	}

IL_0283:
	{
		G_B20_0 = 0;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
		G_B20_4 = G_B19_3;
	}

IL_0284:
	{
		NullCheck(G_B20_4);
		GUIStyle_Draw_m6_1438(G_B20_4, G_B20_3, G_B20_2, G_B20_1, G_B20_0, 0, /*hidden argument*/NULL);
	}

IL_028a:
	{
		bool L_105 = V_4;
		if (!L_105)
		{
			goto IL_02f3;
		}
	}
	{
		GUIStyle_t6_176 * L_106 = ___horizontalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_107 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_106) == ((Object_t*)(GUIStyle_t6_176 *)L_107)))
		{
			goto IL_02f3;
		}
	}
	{
		float L_108 = Rect_get_x_m6_298((&___position), /*hidden argument*/NULL);
		float L_109 = Rect_get_yMax_m6_317((&___position), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_110 = ___horizontalScrollbar;
		NullCheck(L_110);
		float L_111 = GUIStyle_get_fixedHeight_m6_1469(L_110, /*hidden argument*/NULL);
		float L_112 = Rect_get_width_m6_306((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_113 = ___horizontalScrollbar;
		NullCheck(L_113);
		float L_114 = GUIStyle_get_fixedHeight_m6_1469(L_113, /*hidden argument*/NULL);
		Rect_t6_51  L_115 = {0};
		Rect__ctor_m6_295(&L_115, L_108, ((float)((float)L_109-(float)L_111)), L_112, L_114, /*hidden argument*/NULL);
		float L_116 = ((&___scrollPosition)->___x_1);
		float L_117 = Rect_get_width_m6_306((&V_2), /*hidden argument*/NULL);
		float L_118 = Rect_get_width_m6_306((&___viewRect), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_119 = ___horizontalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		float L_120 = GUI_HorizontalScrollbar_m6_1218(NULL /*static, unused*/, L_115, L_116, L_117, (0.0f), L_118, L_119, /*hidden argument*/NULL);
		(&___scrollPosition)->___x_1 = L_120;
		goto IL_0365;
	}

IL_02f3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_121 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_121, 2, /*hidden argument*/NULL);
		int32_t L_122 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_122, 2, /*hidden argument*/NULL);
		int32_t L_123 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_123, 2, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_124 = ___horizontalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_125 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_124) == ((Object_t*)(GUIStyle_t6_176 *)L_125)))
		{
			goto IL_0334;
		}
	}
	{
		(&___scrollPosition)->___x_1 = (0.0f);
		goto IL_0365;
	}

IL_0334:
	{
		float L_126 = ((&___scrollPosition)->___x_1);
		float L_127 = Rect_get_width_m6_306((&___viewRect), /*hidden argument*/NULL);
		float L_128 = Rect_get_width_m6_306((&___position), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_129 = Mathf_Max_m6_429(NULL /*static, unused*/, ((float)((float)L_127-(float)L_128)), (0.0f), /*hidden argument*/NULL);
		float L_130 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_126, (0.0f), L_129, /*hidden argument*/NULL);
		(&___scrollPosition)->___x_1 = L_130;
	}

IL_0365:
	{
		bool L_131 = V_3;
		if (!L_131)
		{
			goto IL_03d3;
		}
	}
	{
		GUIStyle_t6_176 * L_132 = ___verticalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_133 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_132) == ((Object_t*)(GUIStyle_t6_176 *)L_133)))
		{
			goto IL_03d3;
		}
	}
	{
		float L_134 = Rect_get_xMax_m6_315((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_135 = ___verticalScrollbar;
		NullCheck(L_135);
		RectOffset_t6_178 * L_136 = GUIStyle_get_margin_m6_1433(L_135, /*hidden argument*/NULL);
		NullCheck(L_136);
		int32_t L_137 = RectOffset_get_left_m6_1416(L_136, /*hidden argument*/NULL);
		float L_138 = Rect_get_y_m6_300((&V_2), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_139 = ___verticalScrollbar;
		NullCheck(L_139);
		float L_140 = GUIStyle_get_fixedWidth_m6_1468(L_139, /*hidden argument*/NULL);
		float L_141 = Rect_get_height_m6_308((&V_2), /*hidden argument*/NULL);
		Rect_t6_51  L_142 = {0};
		Rect__ctor_m6_295(&L_142, ((float)((float)L_134+(float)(((float)((float)L_137))))), L_138, L_140, L_141, /*hidden argument*/NULL);
		float L_143 = ((&___scrollPosition)->___y_2);
		float L_144 = Rect_get_height_m6_308((&V_2), /*hidden argument*/NULL);
		float L_145 = Rect_get_height_m6_308((&___viewRect), /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_146 = ___verticalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		float L_147 = GUI_VerticalScrollbar_m6_1220(NULL /*static, unused*/, L_142, L_143, L_144, (0.0f), L_145, L_146, /*hidden argument*/NULL);
		(&___scrollPosition)->___y_2 = L_147;
		goto IL_0445;
	}

IL_03d3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		int32_t L_148 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_SliderHash_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_148, 2, /*hidden argument*/NULL);
		int32_t L_149 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_149, 2, /*hidden argument*/NULL);
		int32_t L_150 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_RepeatButtonHash_4;
		GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, L_150, 2, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_151 = ___verticalScrollbar;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_152 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_151) == ((Object_t*)(GUIStyle_t6_176 *)L_152)))
		{
			goto IL_0414;
		}
	}
	{
		(&___scrollPosition)->___y_2 = (0.0f);
		goto IL_0445;
	}

IL_0414:
	{
		float L_153 = ((&___scrollPosition)->___y_2);
		float L_154 = Rect_get_height_m6_308((&___viewRect), /*hidden argument*/NULL);
		float L_155 = Rect_get_height_m6_308((&___position), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_156 = Mathf_Max_m6_429(NULL /*static, unused*/, ((float)((float)L_154-(float)L_155)), (0.0f), /*hidden argument*/NULL);
		float L_157 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_153, (0.0f), L_156, /*hidden argument*/NULL);
		(&___scrollPosition)->___y_2 = L_157;
	}

IL_0445:
	{
		goto IL_044a;
	}

IL_044a:
	{
		Rect_t6_51  L_158 = V_2;
		float L_159 = ((&___scrollPosition)->___x_1);
		float L_160 = Rect_get_x_m6_298((&___viewRect), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_161 = bankers_roundf(((float)((float)((-L_159))-(float)L_160)));
		float L_162 = ((&___scrollPosition)->___y_2);
		float L_163 = Rect_get_y_m6_300((&___viewRect), /*hidden argument*/NULL);
		float L_164 = bankers_roundf(((float)((float)((-L_162))-(float)L_163)));
		Vector2_t6_47  L_165 = {0};
		Vector2__ctor_m6_215(&L_165, L_161, L_164, /*hidden argument*/NULL);
		Vector2_t6_47  L_166 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIClip_Push_m6_1521(NULL /*static, unused*/, L_158, L_165, L_166, 0, /*hidden argument*/NULL);
		Vector2_t6_47  L_167 = ___scrollPosition;
		return L_167;
	}
}
// System.Void UnityEngine.GUI::EndScrollView(System.Boolean)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* ScrollViewState_t6_166_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void GUI_EndScrollView_m6_1225 (Object_t * __this /* static, unused */, bool ___handleScrollWheel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		ScrollViewState_t6_166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1649);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	ScrollViewState_t6_166 * V_0 = {0};
	Vector2_t6_47  V_1 = {0};
	Vector2_t6_47  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GenericStack_t6_170 * L_0 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(18 /* System.Object System.Collections.Stack::Peek() */, L_0);
		V_0 = ((ScrollViewState_t6_166 *)CastclassSealed(L_1, ScrollViewState_t6_166_il2cpp_TypeInfo_var));
		GUIClip_Pop_m6_1523(NULL /*static, unused*/, /*hidden argument*/NULL);
		GenericStack_t6_170 * L_2 = ((GUI_t6_168_StaticFields*)GUI_t6_168_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12;
		NullCheck(L_2);
		VirtFuncInvoker0< Object_t * >::Invoke(19 /* System.Object System.Collections.Stack::Pop() */, L_2);
		bool L_3 = ___handleScrollWheel;
		if (!L_3)
		{
			goto IL_0106;
		}
	}
	{
		Event_t6_162 * L_4 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Event_get_type_m6_1164(L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)6))))
		{
			goto IL_0106;
		}
	}
	{
		ScrollViewState_t6_166 * L_6 = V_0;
		NullCheck(L_6);
		Rect_t6_51 * L_7 = &(L_6->___position_0);
		Event_t6_162 * L_8 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector2_t6_47  L_9 = Event_get_mousePosition_m6_1131(L_8, /*hidden argument*/NULL);
		bool L_10 = Rect_Contains_m6_320(L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0106;
		}
	}
	{
		ScrollViewState_t6_166 * L_11 = V_0;
		NullCheck(L_11);
		Vector2_t6_47 * L_12 = &(L_11->___scrollPosition_3);
		ScrollViewState_t6_166 * L_13 = V_0;
		NullCheck(L_13);
		Vector2_t6_47 * L_14 = &(L_13->___scrollPosition_3);
		float L_15 = (L_14->___x_1);
		Event_t6_162 * L_16 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector2_t6_47  L_17 = Event_get_delta_m6_1133(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = ((&V_1)->___x_1);
		ScrollViewState_t6_166 * L_19 = V_0;
		NullCheck(L_19);
		Rect_t6_51 * L_20 = &(L_19->___viewRect_2);
		float L_21 = Rect_get_width_m6_306(L_20, /*hidden argument*/NULL);
		ScrollViewState_t6_166 * L_22 = V_0;
		NullCheck(L_22);
		Rect_t6_51 * L_23 = &(L_22->___visibleRect_1);
		float L_24 = Rect_get_width_m6_306(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_25 = Mathf_Clamp_m6_439(NULL /*static, unused*/, ((float)((float)L_15+(float)((float)((float)L_18*(float)(20.0f))))), (0.0f), ((float)((float)L_21-(float)L_24)), /*hidden argument*/NULL);
		L_12->___x_1 = L_25;
		ScrollViewState_t6_166 * L_26 = V_0;
		NullCheck(L_26);
		Vector2_t6_47 * L_27 = &(L_26->___scrollPosition_3);
		ScrollViewState_t6_166 * L_28 = V_0;
		NullCheck(L_28);
		Vector2_t6_47 * L_29 = &(L_28->___scrollPosition_3);
		float L_30 = (L_29->___y_2);
		Event_t6_162 * L_31 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector2_t6_47  L_32 = Event_get_delta_m6_1133(L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		float L_33 = ((&V_2)->___y_2);
		ScrollViewState_t6_166 * L_34 = V_0;
		NullCheck(L_34);
		Rect_t6_51 * L_35 = &(L_34->___viewRect_2);
		float L_36 = Rect_get_height_m6_308(L_35, /*hidden argument*/NULL);
		ScrollViewState_t6_166 * L_37 = V_0;
		NullCheck(L_37);
		Rect_t6_51 * L_38 = &(L_37->___visibleRect_1);
		float L_39 = Rect_get_height_m6_308(L_38, /*hidden argument*/NULL);
		float L_40 = Mathf_Clamp_m6_439(NULL /*static, unused*/, ((float)((float)L_30+(float)((float)((float)L_33*(float)(20.0f))))), (0.0f), ((float)((float)L_36-(float)L_39)), /*hidden argument*/NULL);
		L_27->___y_2 = L_40;
		ScrollViewState_t6_166 * L_41 = V_0;
		NullCheck(L_41);
		L_41->___apply_4 = 1;
		Event_t6_162 * L_42 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_42);
		Event_Use_m6_1188(L_42, /*hidden argument*/NULL);
	}

IL_0106:
	{
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUI_Window_m6_1226 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_51  ___clientRect, WindowFunction_t6_167 * ___func, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_0 = ___id;
		Rect_t6_51  L_1 = ___clientRect;
		WindowFunction_t6_167 * L_2 = ___func;
		String_t* L_3 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_4 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_5 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIStyle_t6_176 * L_6 = GUISkin_get_window_m6_1364(L_5, /*hidden argument*/NULL);
		GUISkin_t6_169 * L_7 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t6_51  L_8 = GUI_DoWindow_m6_1237(NULL /*static, unused*/, L_0, L_1, L_2, L_4, L_6, L_7, 1, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void GUI_CallWindowDelegate_m6_1227 (Object_t * __this /* static, unused */, WindowFunction_t6_167 * ___func, int32_t ___id, GUISkin_t6_169 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	GUISkin_t6_169 * V_0 = {0};
	GUILayoutOptionU5BU5D_t6_290* V_1 = {0};
	{
		int32_t L_0 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m6_1281(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_1 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_162 * L_2 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1164(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = ___forceRect;
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		GUILayoutOptionU5BU5D_t6_290* L_5 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 2));
		float L_6 = ___width;
		GUILayoutOption_t6_182 * L_7 = GUILayout_Width_m6_1273(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_7;
		GUILayoutOptionU5BU5D_t6_290* L_8 = L_5;
		float L_9 = ___height;
		GUILayoutOption_t6_182 * L_10 = GUILayout_Height_m6_1275(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_8, 1, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_10;
		V_1 = L_8;
		int32_t L_11 = ___id;
		GUIStyle_t6_176 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m6_1283(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_004d:
	{
		int32_t L_14 = ___id;
		GUIStyle_t6_176 * L_15 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m6_1283(NULL /*static, unused*/, L_14, L_15, (GUILayoutOptionU5BU5D_t6_290*)(GUILayoutOptionU5BU5D_t6_290*)NULL, /*hidden argument*/NULL);
	}

IL_0056:
	{
		GUISkin_t6_169 * L_16 = ____skin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1201(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		WindowFunction_t6_167 * L_17 = ___func;
		int32_t L_18 = ___id;
		NullCheck(L_17);
		WindowFunction_Invoke_m6_1193(L_17, L_18, /*hidden argument*/NULL);
		Event_t6_162 * L_19 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Event_get_type_m6_1164(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)8))))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_Layout_m6_1285(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0078:
	{
		GUISkin_t6_169 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1201(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m6_1228 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUI_set_changed_m6_1228_ftn) (bool);
	static GUI_set_changed_m6_1228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_set_changed_m6_1228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::set_changed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUI_DoLabel_m6_1229 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_INTERNAL_CALL_DoLabel_m6_1230(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m6_1230 (Object_t * __this /* static, unused */, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_CALL_DoLabel_m6_1230_ftn) (Rect_t6_51 *, GUIContent_t6_171 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoLabel_m6_1230_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoLabel_m6_1230_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	_il2cpp_icall_func(___position, ___content, ___style);
}
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoButton_m6_1231 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_2 = GUI_INTERNAL_CALL_DoButton_m6_1232(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m6_1232 (Object_t * __this /* static, unused */, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoButton_m6_1232_ftn) (Rect_t6_51 *, GUIContent_t6_171 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoButton_m6_1232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoButton_m6_1232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___content, ___style);
}
// System.Boolean UnityEngine.GUI::DoToggle(UnityEngine.Rect,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoToggle_m6_1233 (Object_t * __this /* static, unused */, Rect_t6_51  ___position, int32_t ___id, bool ___value, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		bool L_1 = ___value;
		GUIContent_t6_171 * L_2 = ___content;
		IntPtr_t L_3 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_4 = GUI_INTERNAL_CALL_DoToggle_m6_1234(NULL /*static, unused*/, (&___position), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoToggle_m6_1234 (Object_t * __this /* static, unused */, Rect_t6_51 * ___position, int32_t ___id, bool ___value, GUIContent_t6_171 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoToggle_m6_1234_ftn) (Rect_t6_51 *, int32_t, bool, GUIContent_t6_171 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoToggle_m6_1234_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoToggle_m6_1234_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___id, ___value, ___content, ___style);
}
// System.Boolean UnityEngine.GUI::get_usePageScrollbars()
extern "C" bool GUI_get_usePageScrollbars_m6_1235 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GUI_get_usePageScrollbars_m6_1235_ftn) ();
	static GUI_get_usePageScrollbars_m6_1235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_usePageScrollbars_m6_1235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_usePageScrollbars()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
extern "C" void GUI_InternalRepaintEditorWindow_m6_1236 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUI_InternalRepaintEditorWindow_m6_1236_ftn) ();
	static GUI_InternalRepaintEditorWindow_m6_1236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_InternalRepaintEditorWindow_m6_1236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::InternalRepaintEditorWindow()");
	_il2cpp_icall_func();
}
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUI_DoWindow_m6_1237 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_51  ___clientRect, WindowFunction_t6_167 * ___func, GUIContent_t6_171 * ___title, GUIStyle_t6_176 * ___style, GUISkin_t6_169 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___id;
		WindowFunction_t6_167 * L_1 = ___func;
		GUIContent_t6_171 * L_2 = ___title;
		GUIStyle_t6_176 * L_3 = ___style;
		GUISkin_t6_169 * L_4 = ___skin;
		bool L_5 = ___forceRectOnLayout;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		Rect_t6_51  L_6 = GUI_INTERNAL_CALL_DoWindow_m6_1238(NULL /*static, unused*/, L_0, (&___clientRect), L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
extern "C" Rect_t6_51  GUI_INTERNAL_CALL_DoWindow_m6_1238 (Object_t * __this /* static, unused */, int32_t ___id, Rect_t6_51 * ___clientRect, WindowFunction_t6_167 * ___func, GUIContent_t6_171 * ___title, GUIStyle_t6_176 * ___style, GUISkin_t6_169 * ___skin, bool ___forceRectOnLayout, const MethodInfo* method)
{
	typedef Rect_t6_51  (*GUI_INTERNAL_CALL_DoWindow_m6_1238_ftn) (int32_t, Rect_t6_51 *, WindowFunction_t6_167 *, GUIContent_t6_171 *, GUIStyle_t6_176 *, GUISkin_t6_169 *, bool);
	static GUI_INTERNAL_CALL_DoWindow_m6_1238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoWindow_m6_1238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)");
	return _il2cpp_icall_func(___id, ___clientRect, ___func, ___title, ___style, ___skin, ___forceRectOnLayout);
}
// System.Void UnityEngine.GUIContent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m6_1239 (GUIContent_t6_171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m6_1240 (GUIContent_t6_171 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___text;
		__this->___m_Text_0 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m6_1241 (GUIContent_t6_171 * __this, GUIContent_t6_171 * ___src, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_2 = ___src;
		NullCheck(L_2);
		String_t* L_3 = (L_2->___m_Text_0);
		__this->___m_Text_0 = L_3;
		GUIContent_t6_171 * L_4 = ___src;
		NullCheck(L_4);
		Texture_t6_32 * L_5 = (L_4->___m_Image_1);
		__this->___m_Image_1 = L_5;
		GUIContent_t6_171 * L_6 = ___src;
		NullCheck(L_6);
		String_t* L_7 = (L_6->___m_Tooltip_2);
		__this->___m_Tooltip_2 = L_7;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.cctor()
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__cctor_m6_1242 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1239(L_0, /*hidden argument*/NULL);
		((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Text_3 = L_0;
		GUIContent_t6_171 * L_1 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1239(L_1, /*hidden argument*/NULL);
		((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Image_4 = L_1;
		GUIContent_t6_171 * L_2 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1239(L_2, /*hidden argument*/NULL);
		((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		GUIContent_t6_171 * L_4 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1240(L_4, L_3, /*hidden argument*/NULL);
		((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6 = L_4;
		return;
	}
}
// System.String UnityEngine.GUIContent::get_text()
extern "C" String_t* GUIContent_get_text_m6_1243 (GUIContent_t6_171 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Text_0);
		return L_0;
	}
}
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" void GUIContent_set_text_m6_1244 (GUIContent_t6_171 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_Text_0 = L_0;
		return;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" GUIContent_t6_171 * GUIContent_Temp_m6_1245 (Object_t * __this /* static, unused */, String_t* ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_0 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		String_t* L_1 = ___t;
		NullCheck(L_0);
		L_0->___m_Text_0 = L_1;
		GUIContent_t6_171 * L_2 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_2);
		L_2->___m_Tooltip_2 = L_3;
		GUIContent_t6_171 * L_4 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		return L_4;
	}
}
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent_ClearStaticCache_m6_1246 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_0 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		NullCheck(L_0);
		L_0->___m_Text_0 = (String_t*)NULL;
		GUIContent_t6_171 * L_1 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Text_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_1);
		L_1->___m_Tooltip_2 = L_2;
		GUIContent_t6_171 * L_3 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		NullCheck(L_3);
		L_3->___m_Image_1 = (Texture_t6_32 *)NULL;
		GUIContent_t6_171 * L_4 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_Image_4;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_4);
		L_4->___m_Tooltip_2 = L_5;
		GUIContent_t6_171 * L_6 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5;
		NullCheck(L_6);
		L_6->___m_Text_0 = (String_t*)NULL;
		GUIContent_t6_171 * L_7 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_5;
		NullCheck(L_7);
		L_7->___m_Image_1 = (Texture_t6_32 *)NULL;
		return;
	}
}
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m6_1247 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_1 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_2 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_176 * L_3 = GUISkin_get_label_m6_1354(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_4 = ___options;
		GUILayout_DoLabel_m6_1249(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Label_m6_1248 (Object_t * __this /* static, unused */, String_t* ___text, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_1 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_2 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_3 = ___options;
		GUILayout_DoLabel_m6_1249(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::DoLabel(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_DoLabel_m6_1249 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		GUIStyle_t6_176 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_3 = GUILayoutUtility_GetRect_m6_1293(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_4 = ___content;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_Label_m6_1205(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::Box(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Box_m6_1250 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_1 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_2 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_176 * L_3 = GUISkin_get_box_m6_1352(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_4 = ___options;
		GUILayout_DoBox_m6_1251(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::DoBox(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_DoBox_m6_1251 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		GUIStyle_t6_176 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_3 = GUILayoutUtility_GetRect_m6_1293(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_4 = ___content;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_Box_m6_1206(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_Button_m6_1252 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_1 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_2 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_176 * L_3 = GUISkin_get_button_m6_1360(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_4 = ___options;
		bool L_5 = GUILayout_DoButton_m6_1253(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUILayout::DoButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoButton_m6_1253 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		GUIStyle_t6_176 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_3 = GUILayoutUtility_GetRect_m6_1293(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_4 = ___content;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_6 = GUI_Button_m6_1208(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String UnityEngine.GUILayout::TextArea(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" String_t* GUILayout_TextArea_m6_1254 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_1 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t6_176 * L_2 = GUISkin_get_textArea_m6_1358(L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_3 = ___options;
		String_t* L_4 = GUILayout_DoTextField_m6_1255(NULL /*static, unused*/, L_0, (-1), 1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.GUILayout::DoTextField(System.String,System.Int32,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t6_95_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" String_t* GUILayout_DoTextField_m6_1255 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___maxLength, bool ___multiline, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		Input_t6_95_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1613);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GUIContent_t6_171 * V_1 = {0};
	Rect_t6_51  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_GetControlID_m6_1496(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_2 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_6 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0036;
	}

IL_0025:
	{
		String_t* L_7 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t6_95_il2cpp_TypeInfo_var);
		String_t* L_8 = Input_get_compositionString_m6_698(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1_559(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_10 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_0036:
	{
		GUIContent_t6_171 * L_11 = V_1;
		GUIStyle_t6_176 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_13 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_14 = GUILayoutUtility_GetRect_m6_1293(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_15 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_17 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_18 = GUIContent_Temp_m6_1245(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
	}

IL_0052:
	{
		Rect_t6_51  L_19 = V_2;
		int32_t L_20 = V_0;
		GUIContent_t6_171 * L_21 = V_1;
		bool L_22 = ___multiline;
		int32_t L_23 = ___maxLength;
		GUIStyle_t6_176 * L_24 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_DoTextField_m6_1211(NULL /*static, unused*/, L_19, L_20, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_25 = V_1;
		NullCheck(L_25);
		String_t* L_26 = GUIContent_get_text_m6_1243(L_25, /*hidden argument*/NULL);
		return L_26;
	}
}
// System.Boolean UnityEngine.GUILayout::Toggle(System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" bool GUILayout_Toggle_m6_1256 (Object_t * __this /* static, unused */, bool ___value, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		GUIContent_t6_171 * L_1 = ___content;
		GUIStyle_t6_176 * L_2 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_3 = ___options;
		bool L_4 = GUILayout_DoToggle_m6_1257(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUILayout::DoToggle(System.Boolean,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoToggle_m6_1257 (Object_t * __this /* static, unused */, bool ___value, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		GUIStyle_t6_176 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_3 = GUILayoutUtility_GetRect_m6_1293(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		bool L_4 = ___value;
		GUIContent_t6_171 * L_5 = ___content;
		GUIStyle_t6_176 * L_6 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Toggle_m6_1216(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityEngine.GUILayout::Space(System.Single)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void GUILayout_Space_m6_1258 (Object_t * __this /* static, unused */, float ___pixels, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_0 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_174 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		bool L_2 = (L_1->___isVertical_11);
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		float L_3 = ___pixels;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_4 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_5 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 1));
		float L_6 = ___pixels;
		GUILayoutOption_t6_182 * L_7 = GUILayout_Height_m6_1275(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_5, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_7;
		GUILayoutUtility_GetRect_m6_1295(NULL /*static, unused*/, (0.0f), L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_003e:
	{
		float L_8 = ___pixels;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_9 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_10 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 1));
		float L_11 = ___pixels;
		GUILayoutOption_t6_182 * L_12 = GUILayout_Width_m6_1273(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_12);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_10, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_12;
		GUILayoutUtility_GetRect_m6_1295(NULL /*static, unused*/, L_8, (0.0f), L_9, L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::FlexibleSpace()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var;
extern "C" void GUILayout_FlexibleSpace_m6_1259 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1651);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t6_182 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_0 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_174 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		bool L_2 = (L_1->___isVertical_11);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		GUILayoutOption_t6_182 * L_3 = GUILayout_ExpandHeight_m6_1278(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_002c;
	}

IL_0025:
	{
		GUILayoutOption_t6_182 * L_4 = GUILayout_ExpandWidth_m6_1277(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002c:
	{
		GUILayoutOption_t6_182 * L_5 = V_0;
		int32_t L_6 = ((int32_t)10000);
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_5);
		L_5->___value_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_8 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_9 = ((GUILayoutOptionU5BU5D_t6_290*)SZArrayNew(GUILayoutOptionU5BU5D_t6_290_il2cpp_TypeInfo_var, 1));
		GUILayoutOption_t6_182 * L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		ArrayElementTypeCheck (L_9, L_10);
		*((GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_9, 0, sizeof(GUILayoutOption_t6_182 *))) = (GUILayoutOption_t6_182 *)L_10;
		GUILayoutUtility_GetRect_m6_1295(NULL /*static, unused*/, (0.0f), (0.0f), L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m6_1260 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_0 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_1 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		GUILayout_BeginHorizontal_m6_1261(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUILayoutGroup_t6_174_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginHorizontal_m6_1261 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_174_0_0_0_var = il2cpp_codegen_type_from_index(1653);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_174 * V_0 = {0};
	{
		GUIStyle_t6_176 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_174_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_174 * L_3 = GUILayoutUtility_BeginLayoutGroup_m6_1290(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t6_174 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 0;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_6 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t6_176 *)L_5) == ((Object_t*)(GUIStyle_t6_176 *)L_6))))
		{
			goto IL_002f;
		}
	}
	{
		GUIContent_t6_171 * L_7 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_8 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		if ((((Object_t*)(GUIContent_t6_171 *)L_7) == ((Object_t*)(GUIContent_t6_171 *)L_8)))
		{
			goto IL_003c;
		}
	}

IL_002f:
	{
		GUILayoutGroup_t6_174 * L_9 = V_0;
		NullCheck(L_9);
		Rect_t6_51  L_10 = (((GUILayoutEntry_t6_177 *)L_9)->___rect_4);
		GUIContent_t6_171 * L_11 = ___content;
		GUIStyle_t6_176 * L_12 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_Box_m6_1206(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4810;
extern "C" void GUILayout_EndHorizontal_m6_1262 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		_stringLiteral4810 = il2cpp_codegen_string_literal_from_index(4810);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m6_1284(NULL /*static, unused*/, _stringLiteral4810, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m6_1291(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginVertical_m6_1263 (Object_t * __this /* static, unused */, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_0 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_1 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		GUILayout_BeginVertical_m6_1264(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUILayoutGroup_t6_174_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginVertical_m6_1264 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_174_0_0_0_var = il2cpp_codegen_type_from_index(1653);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_174 * V_0 = {0};
	{
		GUIStyle_t6_176 * L_0 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_1 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_174_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_174 * L_3 = GUILayoutUtility_BeginLayoutGroup_m6_1290(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GUILayoutGroup_t6_174 * L_4 = V_0;
		NullCheck(L_4);
		L_4->___isVertical_11 = 1;
		GUIStyle_t6_176 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_6 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_5) == ((Object_t*)(GUIStyle_t6_176 *)L_6)))
		{
			goto IL_0031;
		}
	}
	{
		GUILayoutGroup_t6_174 * L_7 = V_0;
		NullCheck(L_7);
		Rect_t6_51  L_8 = (((GUILayoutEntry_t6_177 *)L_7)->___rect_4);
		GUIContent_t6_171 * L_9 = ___content;
		GUIStyle_t6_176 * L_10 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_Box_m6_1206(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndVertical()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4811;
extern "C" void GUILayout_EndVertical_m6_1265 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		_stringLiteral4811 = il2cpp_codegen_string_literal_from_index(4811);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m6_1284(NULL /*static, unused*/, _stringLiteral4811, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m6_1291(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect)
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m6_1266 (Object_t * __this /* static, unused */, Rect_t6_51  ___screenRect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = ___screenRect;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_1 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_2 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_BeginArea_m6_1267(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::BeginArea(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern const Il2CppType* GUILayoutGroup_t6_174_0_0_0_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_BeginArea_m6_1267 (Object_t * __this /* static, unused */, Rect_t6_51  ___screenRect, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_174_0_0_0_var = il2cpp_codegen_type_from_index(1653);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_174 * V_0 = {0};
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_0 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_174_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_174 * L_2 = GUILayoutUtility_BeginLayoutArea_m6_1292(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Event_t6_162 * L_3 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Event_get_type_m6_1164(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)8))))
		{
			goto IL_0088;
		}
	}
	{
		GUILayoutGroup_t6_174 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___resetCoords_12 = 1;
		GUILayoutGroup_t6_174 * L_6 = V_0;
		GUILayoutGroup_t6_174 * L_7 = V_0;
		float L_8 = Rect_get_width_m6_306((&___screenRect), /*hidden argument*/NULL);
		float L_9 = L_8;
		V_1 = L_9;
		NullCheck(L_7);
		((GUILayoutEntry_t6_177 *)L_7)->___maxWidth_1 = L_9;
		float L_10 = V_1;
		NullCheck(L_6);
		((GUILayoutEntry_t6_177 *)L_6)->___minWidth_0 = L_10;
		GUILayoutGroup_t6_174 * L_11 = V_0;
		GUILayoutGroup_t6_174 * L_12 = V_0;
		float L_13 = Rect_get_height_m6_308((&___screenRect), /*hidden argument*/NULL);
		float L_14 = L_13;
		V_1 = L_14;
		NullCheck(L_12);
		((GUILayoutEntry_t6_177 *)L_12)->___maxHeight_3 = L_14;
		float L_15 = V_1;
		NullCheck(L_11);
		((GUILayoutEntry_t6_177 *)L_11)->___minHeight_2 = L_15;
		GUILayoutGroup_t6_174 * L_16 = V_0;
		float L_17 = Rect_get_xMin_m6_311((&___screenRect), /*hidden argument*/NULL);
		float L_18 = Rect_get_yMin_m6_313((&___screenRect), /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_19 = V_0;
		NullCheck(L_19);
		Rect_t6_51 * L_20 = &(((GUILayoutEntry_t6_177 *)L_19)->___rect_4);
		float L_21 = Rect_get_xMax_m6_315(L_20, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_22 = V_0;
		NullCheck(L_22);
		Rect_t6_51 * L_23 = &(((GUILayoutEntry_t6_177 *)L_22)->___rect_4);
		float L_24 = Rect_get_yMax_m6_317(L_23, /*hidden argument*/NULL);
		Rect_t6_51  L_25 = Rect_MinMaxRect_m6_297(NULL /*static, unused*/, L_17, L_18, L_21, L_24, /*hidden argument*/NULL);
		NullCheck(L_16);
		((GUILayoutEntry_t6_177 *)L_16)->___rect_4 = L_25;
	}

IL_0088:
	{
		GUILayoutGroup_t6_174 * L_26 = V_0;
		NullCheck(L_26);
		Rect_t6_51  L_27 = (((GUILayoutEntry_t6_177 *)L_26)->___rect_4);
		GUIContent_t6_171 * L_28 = ___content;
		GUIStyle_t6_176 * L_29 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_BeginGroup_m6_1222(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndArea()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" void GUILayout_EndArea_m6_1268 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_2 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t6_170 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(19 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t6_173 * L_4 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_5 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t6_170 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(18 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t6_174 *)CastclassClass(L_7, GUILayoutGroup_t6_174_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_EndGroup_m6_1223(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4812;
extern Il2CppCodeGenString* _stringLiteral4813;
extern "C" Vector2_t6_47  GUILayout_BeginScrollView_m6_1269 (Object_t * __this /* static, unused */, Vector2_t6_47  ___scrollPosition, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4812 = il2cpp_codegen_string_literal_from_index(4812);
		_stringLiteral4813 = il2cpp_codegen_string_literal_from_index(4813);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	GUIStyle_t6_176 * V_1 = {0};
	GUIStyle_t6_176 * V_2 = {0};
	{
		GUIStyle_t6_176 * L_0 = ___style;
		NullCheck(L_0);
		String_t* L_1 = GUIStyle_get_name_m6_1455(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_2 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m1_559(NULL /*static, unused*/, L_3, _stringLiteral4812, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t6_176 * L_5 = GUISkin_FindStyle_m6_1399(L_2, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		GUIStyle_t6_176 * L_6 = V_1;
		if (L_6)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_7 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUIStyle_t6_176 * L_8 = GUISkin_get_verticalScrollbar_m6_1382(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_9 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m1_559(NULL /*static, unused*/, L_10, _stringLiteral4813, /*hidden argument*/NULL);
		NullCheck(L_9);
		GUIStyle_t6_176 * L_12 = GUISkin_FindStyle_m6_1399(L_9, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		GUIStyle_t6_176 * L_13 = V_2;
		if (L_13)
		{
			goto IL_0055;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_14 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		GUIStyle_t6_176 * L_15 = GUISkin_get_horizontalScrollbar_m6_1374(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
	}

IL_0055:
	{
		Vector2_t6_47  L_16 = ___scrollPosition;
		GUIStyle_t6_176 * L_17 = V_2;
		GUIStyle_t6_176 * L_18 = V_1;
		GUIStyle_t6_176 * L_19 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_20 = ___options;
		Vector2_t6_47  L_21 = GUILayout_BeginScrollView_m6_1270(NULL /*static, unused*/, L_16, 0, 0, L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern const Il2CppType* GUIScrollGroup_t6_179_0_0_0_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIScrollGroup_t6_179_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  GUILayout_BeginScrollView_m6_1270 (Object_t * __this /* static, unused */, Vector2_t6_47  ___scrollPosition, bool ___alwaysShowHorizontal, bool ___alwaysShowVertical, GUIStyle_t6_176 * ___horizontalScrollbar, GUIStyle_t6_176 * ___verticalScrollbar, GUIStyle_t6_176 * ___background, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIScrollGroup_t6_179_0_0_0_var = il2cpp_codegen_type_from_index(1654);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIScrollGroup_t6_179_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1654);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		s_Il2CppMethodIntialized = true;
	}
	GUIScrollGroup_t6_179 * V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_0 = ___background;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(GUIScrollGroup_t6_179_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_174 * L_2 = GUILayoutUtility_BeginLayoutGroup_m6_1290(NULL /*static, unused*/, L_0, (GUILayoutOptionU5BU5D_t6_290*)(GUILayoutOptionU5BU5D_t6_290*)NULL, L_1, /*hidden argument*/NULL);
		V_0 = ((GUIScrollGroup_t6_179 *)CastclassSealed(L_2, GUIScrollGroup_t6_179_il2cpp_TypeInfo_var));
		Event_t6_162 * L_3 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = Event_get_type_m6_1164(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)8)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_007a;
	}

IL_0034:
	{
		GUIScrollGroup_t6_179 * L_6 = V_0;
		NullCheck(L_6);
		((GUILayoutGroup_t6_174 *)L_6)->___resetCoords_12 = 1;
		GUIScrollGroup_t6_179 * L_7 = V_0;
		NullCheck(L_7);
		((GUILayoutGroup_t6_174 *)L_7)->___isVertical_11 = 1;
		GUIScrollGroup_t6_179 * L_8 = V_0;
		NullCheck(L_8);
		((GUILayoutEntry_t6_177 *)L_8)->___stretchWidth_5 = 1;
		GUIScrollGroup_t6_179 * L_9 = V_0;
		NullCheck(L_9);
		((GUILayoutEntry_t6_177 *)L_9)->___stretchHeight_6 = 1;
		GUIScrollGroup_t6_179 * L_10 = V_0;
		GUIStyle_t6_176 * L_11 = ___verticalScrollbar;
		NullCheck(L_10);
		L_10->___verticalScrollbar_38 = L_11;
		GUIScrollGroup_t6_179 * L_12 = V_0;
		GUIStyle_t6_176 * L_13 = ___horizontalScrollbar;
		NullCheck(L_12);
		L_12->___horizontalScrollbar_37 = L_13;
		GUIScrollGroup_t6_179 * L_14 = V_0;
		bool L_15 = ___alwaysShowVertical;
		NullCheck(L_14);
		L_14->___needsVerticalScrollbar_36 = L_15;
		GUIScrollGroup_t6_179 * L_16 = V_0;
		bool L_17 = ___alwaysShowHorizontal;
		NullCheck(L_16);
		L_16->___needsHorizontalScrollbar_35 = L_17;
		GUIScrollGroup_t6_179 * L_18 = V_0;
		GUILayoutOptionU5BU5D_t6_290* L_19 = ___options;
		NullCheck(L_18);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_290* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_18, L_19);
		goto IL_007f;
	}

IL_007a:
	{
		goto IL_007f;
	}

IL_007f:
	{
		GUIScrollGroup_t6_179 * L_20 = V_0;
		NullCheck(L_20);
		Rect_t6_51  L_21 = (((GUILayoutEntry_t6_177 *)L_20)->___rect_4);
		Vector2_t6_47  L_22 = ___scrollPosition;
		GUIScrollGroup_t6_179 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = (L_23->___clientWidth_31);
		GUIScrollGroup_t6_179 * L_25 = V_0;
		NullCheck(L_25);
		float L_26 = (L_25->___clientHeight_32);
		Rect_t6_51  L_27 = {0};
		Rect__ctor_m6_295(&L_27, (0.0f), (0.0f), L_24, L_26, /*hidden argument*/NULL);
		bool L_28 = ___alwaysShowHorizontal;
		bool L_29 = ___alwaysShowVertical;
		GUIStyle_t6_176 * L_30 = ___horizontalScrollbar;
		GUIStyle_t6_176 * L_31 = ___verticalScrollbar;
		GUIStyle_t6_176 * L_32 = ___background;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		Vector2_t6_47  L_33 = GUI_BeginScrollView_m6_1224(NULL /*static, unused*/, L_21, L_22, L_27, L_28, L_29, L_30, L_31, L_32, /*hidden argument*/NULL);
		return L_33;
	}
}
// System.Void UnityEngine.GUILayout::EndScrollView()
extern "C" void GUILayout_EndScrollView_m6_1271 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		GUILayout_EndScrollView_m6_1272(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayout::EndScrollView(System.Boolean)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4814;
extern "C" void GUILayout_EndScrollView_m6_1272 (Object_t * __this /* static, unused */, bool ___handleScrollWheel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		_stringLiteral4814 = il2cpp_codegen_string_literal_from_index(4814);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_EndGroup_m6_1284(NULL /*static, unused*/, _stringLiteral4814, /*hidden argument*/NULL);
		GUILayoutUtility_EndLayoutGroup_m6_1291(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = ___handleScrollWheel;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_EndScrollView_m6_1225(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_182_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_182 * GUILayout_Width_m6_1273 (Object_t * __this /* static, unused */, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutOption_t6_182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1652);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_182 * L_3 = (GUILayoutOption_t6_182 *)il2cpp_codegen_object_new (GUILayoutOption_t6_182_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1336(L_3, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MaxWidth(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_182_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_182 * GUILayout_MaxWidth_m6_1274 (Object_t * __this /* static, unused */, float ___maxWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutOption_t6_182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1652);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___maxWidth;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_182 * L_3 = (GUILayoutOption_t6_182 *)il2cpp_codegen_object_new (GUILayoutOption_t6_182_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1336(L_3, 3, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_182_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_182 * GUILayout_Height_m6_1275 (Object_t * __this /* static, unused */, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutOption_t6_182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1652);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___height;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_182 * L_3 = (GUILayoutOption_t6_182 *)il2cpp_codegen_object_new (GUILayoutOption_t6_182_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1336(L_3, 1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinHeight(System.Single)
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_182_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_182 * GUILayout_MinHeight_m6_1276 (Object_t * __this /* static, unused */, float ___minHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		GUILayoutOption_t6_182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1652);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___minHeight;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_182 * L_3 = (GUILayoutOption_t6_182 *)il2cpp_codegen_object_new (GUILayoutOption_t6_182_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1336(L_3, 4, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandWidth(System.Boolean)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_182_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_182 * GUILayout_ExpandWidth_m6_1277 (Object_t * __this /* static, unused */, bool ___expand, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOption_t6_182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1652);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		bool L_0 = ___expand;
		G_B1_0 = 6;
		if (!L_0)
		{
			G_B2_0 = 6;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		int32_t L_1 = G_B3_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_182 * L_3 = (GUILayoutOption_t6_182 *)il2cpp_codegen_object_new (GUILayoutOption_t6_182_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1336(L_3, G_B3_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandHeight(System.Boolean)
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t6_182_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t6_182 * GUILayout_ExpandHeight_m6_1278 (Object_t * __this /* static, unused */, bool ___expand, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GUILayoutOption_t6_182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1652);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	{
		bool L_0 = ___expand;
		G_B1_0 = 7;
		if (!L_0)
		{
			G_B2_0 = 7;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		int32_t L_1 = G_B3_0;
		Object_t * L_2 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t6_182 * L_3 = (GUILayoutOption_t6_182 *)il2cpp_codegen_object_new (GUILayoutOption_t6_182_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m6_1336(L_3, G_B3_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t6_170_il2cpp_TypeInfo_var;
extern "C" void LayoutCache__ctor_m6_1279 (LayoutCache_t6_173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		GenericStack_t6_170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1644);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutGroup_t6_174 * L_0 = (GUILayoutGroup_t6_174 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_174_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1315(L_0, /*hidden argument*/NULL);
		__this->___topLevel_0 = L_0;
		GenericStack_t6_170 * L_1 = (GenericStack_t6_170 *)il2cpp_codegen_object_new (GenericStack_t6_170_il2cpp_TypeInfo_var);
		GenericStack__ctor_m6_1823(L_1, /*hidden argument*/NULL);
		__this->___layoutGroups_1 = L_1;
		GUILayoutGroup_t6_174 * L_2 = (GUILayoutGroup_t6_174 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_174_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1315(L_2, /*hidden argument*/NULL);
		__this->___windows_2 = L_2;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GenericStack_t6_170 * L_3 = (__this->___layoutGroups_1);
		GUILayoutGroup_t6_174 * L_4 = (__this->___topLevel_0);
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_3, L_4);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::.cctor()
extern TypeInfo* Dictionary_2_t1_1827_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t6_173_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14956_MethodInfo_var;
extern "C" void GUILayoutUtility__cctor_m6_1280 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1_1827_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1656);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		LayoutCache_t6_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1655);
		Dictionary_2__ctor_m1_14956_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483797);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1827 * L_0 = (Dictionary_2_t1_1827 *)il2cpp_codegen_object_new (Dictionary_2_t1_1827_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14956(L_0, /*hidden argument*/Dictionary_2__ctor_m1_14956_MethodInfo_var);
		((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_StoredLayouts_0 = L_0;
		Dictionary_2_t1_1827 * L_1 = (Dictionary_2_t1_1827 *)il2cpp_codegen_object_new (Dictionary_2_t1_1827_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14956(L_1, /*hidden argument*/Dictionary_2__ctor_m1_14956_MethodInfo_var);
		((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_StoredWindows_1 = L_1;
		LayoutCache_t6_173 * L_2 = (LayoutCache_t6_173 *)il2cpp_codegen_object_new (LayoutCache_t6_173_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m6_1279(L_2, /*hidden argument*/NULL);
		((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2 = L_2;
		Rect_t6_51  L_3 = {0};
		Rect__ctor_m6_295(&L_3, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3 = L_3;
		return;
	}
}
// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::SelectIDList(System.Int32,System.Boolean)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t6_173_il2cpp_TypeInfo_var;
extern "C" LayoutCache_t6_173 * GUILayoutUtility_SelectIDList_m6_1281 (Object_t * __this /* static, unused */, int32_t ___instanceID, bool ___isWindow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		LayoutCache_t6_173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1655);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1_1827 * V_0 = {0};
	LayoutCache_t6_173 * V_1 = {0};
	Dictionary_2_t1_1827 * G_B3_0 = {0};
	{
		bool L_0 = ___isWindow;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1827 * L_1 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_StoredWindows_1;
		G_B3_0 = L_1;
		goto IL_0015;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Dictionary_2_t1_1827 * L_2 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_StoredLayouts_0;
		G_B3_0 = L_2;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		Dictionary_2_t1_1827 * L_3 = V_0;
		int32_t L_4 = ___instanceID;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker2< bool, int32_t, LayoutCache_t6_173 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::TryGetValue(!0,!1&) */, L_3, L_4, (&V_1));
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		LayoutCache_t6_173 * L_6 = (LayoutCache_t6_173 *)il2cpp_codegen_object_new (LayoutCache_t6_173_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m6_1279(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		Dictionary_2_t1_1827 * L_7 = V_0;
		int32_t L_8 = ___instanceID;
		LayoutCache_t6_173 * L_9 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, LayoutCache_t6_173 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Item(!0,!1) */, L_7, L_8, L_9);
		goto IL_0037;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_10 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_11 = V_1;
		NullCheck(L_11);
		GUILayoutGroup_t6_174 * L_12 = (L_11->___topLevel_0);
		NullCheck(L_10);
		L_10->___topLevel_0 = L_12;
		LayoutCache_t6_173 * L_13 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_14 = V_1;
		NullCheck(L_14);
		GenericStack_t6_170 * L_15 = (L_14->___layoutGroups_1);
		NullCheck(L_13);
		L_13->___layoutGroups_1 = L_15;
		LayoutCache_t6_173 * L_16 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_17 = V_1;
		NullCheck(L_17);
		GUILayoutGroup_t6_174 * L_18 = (L_17->___windows_2);
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		LayoutCache_t6_173 * L_19 = V_1;
		return L_19;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Begin(System.Int32)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Begin_m6_1282 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t6_173 * V_0 = {0};
	GUILayoutGroup_t6_174 * V_1 = {0};
	{
		int32_t L_0 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_1 = GUILayoutUtility_SelectIDList_m6_1281(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_162 * L_2 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1164(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_4 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_5 = V_0;
		GUILayoutGroup_t6_174 * L_6 = (GUILayoutGroup_t6_174 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_174_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1315(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t6_174 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t6_173 * L_9 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GenericStack_t6_170 * L_10 = (L_9->___layoutGroups_1);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Stack::Clear() */, L_10);
		LayoutCache_t6_173 * L_11 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GenericStack_t6_170 * L_12 = (L_11->___layoutGroups_1);
		LayoutCache_t6_173 * L_13 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t6_174 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_12);
		VirtActionInvoker1< Object_t * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_12, L_14);
		LayoutCache_t6_173 * L_15 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_16 = V_0;
		GUILayoutGroup_t6_174 * L_17 = (GUILayoutGroup_t6_174 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_174_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1315(L_17, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_18 = L_17;
		V_1 = L_18;
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		GUILayoutGroup_t6_174 * L_19 = V_1;
		NullCheck(L_15);
		L_15->___windows_2 = L_19;
		goto IL_00a5;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_20 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_t6_174 * L_22 = (L_21->___topLevel_0);
		NullCheck(L_20);
		L_20->___topLevel_0 = L_22;
		LayoutCache_t6_173 * L_23 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_24 = V_0;
		NullCheck(L_24);
		GenericStack_t6_170 * L_25 = (L_24->___layoutGroups_1);
		NullCheck(L_23);
		L_23->___layoutGroups_1 = L_25;
		LayoutCache_t6_173 * L_26 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_27 = V_0;
		NullCheck(L_27);
		GUILayoutGroup_t6_174 * L_28 = (L_27->___windows_2);
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::BeginWindow(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_BeginWindow_m6_1283 (Object_t * __this /* static, unused */, int32_t ___windowID, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t6_173 * V_0 = {0};
	GUILayoutGroup_t6_174 * V_1 = {0};
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_1 = GUILayoutUtility_SelectIDList_m6_1281(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t6_162 * L_2 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m6_1164(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00ab;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_4 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_5 = V_0;
		GUILayoutGroup_t6_174 * L_6 = (GUILayoutGroup_t6_174 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_174_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1315(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t6_174 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t6_173 * L_9 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t6_174 * L_10 = (L_9->___topLevel_0);
		GUIStyle_t6_176 * L_11 = ___style;
		NullCheck(L_10);
		GUILayoutEntry_set_style_m6_1306(L_10, L_11, /*hidden argument*/NULL);
		LayoutCache_t6_173 * L_12 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t6_174 * L_13 = (L_12->___topLevel_0);
		int32_t L_14 = ___windowID;
		NullCheck(L_13);
		L_13->___windowID_16 = L_14;
		GUILayoutOptionU5BU5D_t6_290* L_15 = ___options;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_16 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_16);
		GUILayoutGroup_t6_174 * L_17 = (L_16->___topLevel_0);
		GUILayoutOptionU5BU5D_t6_290* L_18 = ___options;
		NullCheck(L_17);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_290* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_17, L_18);
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_19 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GenericStack_t6_170 * L_20 = (L_19->___layoutGroups_1);
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Stack::Clear() */, L_20);
		LayoutCache_t6_173 * L_21 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GenericStack_t6_170 * L_22 = (L_21->___layoutGroups_1);
		LayoutCache_t6_173 * L_23 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t6_174 * L_24 = (L_23->___topLevel_0);
		NullCheck(L_22);
		VirtActionInvoker1< Object_t * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_22, L_24);
		LayoutCache_t6_173 * L_25 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_26 = V_0;
		GUILayoutGroup_t6_174 * L_27 = (GUILayoutGroup_t6_174 *)il2cpp_codegen_object_new (GUILayoutGroup_t6_174_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m6_1315(L_27, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_28 = L_27;
		V_1 = L_28;
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
		GUILayoutGroup_t6_174 * L_29 = V_1;
		NullCheck(L_25);
		L_25->___windows_2 = L_29;
		goto IL_00db;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_30 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_31 = V_0;
		NullCheck(L_31);
		GUILayoutGroup_t6_174 * L_32 = (L_31->___topLevel_0);
		NullCheck(L_30);
		L_30->___topLevel_0 = L_32;
		LayoutCache_t6_173 * L_33 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_34 = V_0;
		NullCheck(L_34);
		GenericStack_t6_170 * L_35 = (L_34->___layoutGroups_1);
		NullCheck(L_33);
		L_33->___layoutGroups_1 = L_35;
		LayoutCache_t6_173 * L_36 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_37 = V_0;
		NullCheck(L_37);
		GUILayoutGroup_t6_174 * L_38 = (L_37->___windows_2);
		NullCheck(L_36);
		L_36->___windows_2 = L_38;
	}

IL_00db:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndGroup(System.String)
extern "C" void GUILayoutUtility_EndGroup_m6_1284 (Object_t * __this /* static, unused */, String_t* ___groupName, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Layout()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Layout_m6_1285 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_0 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_174 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		int32_t L_2 = (L_1->___windowID_16);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_00af;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_3 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_3);
		GUILayoutGroup_t6_174 * L_4 = (L_3->___topLevel_0);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_4);
		LayoutCache_t6_173 * L_5 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t6_174 * L_6 = (L_5->___topLevel_0);
		int32_t L_7 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		float L_8 = GUIUtility_get_pixelsPerPoint_m6_1495(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t6_173 * L_9 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t6_174 * L_10 = (L_9->___topLevel_0);
		NullCheck(L_10);
		float L_11 = (((GUILayoutEntry_t6_177 *)L_10)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m6_427(NULL /*static, unused*/, ((float)((float)(((float)((float)L_7)))/(float)L_8)), L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_6, (0.0f), L_12);
		LayoutCache_t6_173 * L_13 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t6_174 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_14);
		LayoutCache_t6_173 * L_15 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t6_174 * L_16 = (L_15->___topLevel_0);
		int32_t L_17 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = GUIUtility_get_pixelsPerPoint_m6_1495(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t6_173 * L_19 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GUILayoutGroup_t6_174 * L_20 = (L_19->___topLevel_0);
		NullCheck(L_20);
		float L_21 = (((GUILayoutEntry_t6_177 *)L_20)->___maxHeight_3);
		float L_22 = Mathf_Min_m6_427(NULL /*static, unused*/, ((float)((float)(((float)((float)L_17)))/(float)L_18)), L_21, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_16, (0.0f), L_22);
		LayoutCache_t6_173 * L_23 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t6_174 * L_24 = (L_23->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m6_1287(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_25 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GUILayoutGroup_t6_174 * L_26 = (L_25->___topLevel_0);
		GUILayoutUtility_LayoutSingleGroup_m6_1288(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		LayoutCache_t6_173 * L_27 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_27);
		GUILayoutGroup_t6_174 * L_28 = (L_27->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m6_1287(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFromEditorWindow()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m6_1286 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_0 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t6_174 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_1);
		LayoutCache_t6_173 * L_2 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GUILayoutGroup_t6_174 * L_3 = (L_2->___topLevel_0);
		int32_t L_4 = Screen_get_width_m6_145(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		float L_5 = GUIUtility_get_pixelsPerPoint_m6_1495(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_3, (0.0f), ((float)((float)(((float)((float)L_4)))/(float)L_5)));
		LayoutCache_t6_173 * L_6 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t6_174 * L_7 = (L_6->___topLevel_0);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_7);
		LayoutCache_t6_173 * L_8 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t6_174 * L_9 = (L_8->___topLevel_0);
		int32_t L_10 = Screen_get_height_m6_146(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = GUIUtility_get_pixelsPerPoint_m6_1495(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_9, (0.0f), ((float)((float)(((float)((float)L_10)))/(float)L_11)));
		LayoutCache_t6_173 * L_12 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t6_174 * L_13 = (L_12->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m6_1287(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFreeGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1847_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14957_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14958_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14959_MethodInfo_var;
extern "C" void GUILayoutUtility_LayoutFreeGroup_m6_1287 (Object_t * __this /* static, unused */, GUILayoutGroup_t6_174 * ___toplevel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		Enumerator_t1_1847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1658);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14957_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		Enumerator_get_Current_m1_14958_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		Enumerator_MoveNext_m1_14959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_174 * V_0 = {0};
	Enumerator_t1_1847  V_1 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayoutGroup_t6_174 * L_0 = ___toplevel;
		NullCheck(L_0);
		List_1_t1_1828 * L_1 = (L_0->___entries_10);
		NullCheck(L_1);
		Enumerator_t1_1847  L_2 = List_1_GetEnumerator_m1_14957(L_1, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			GUILayoutEntry_t6_177 * L_3 = Enumerator_get_Current_m1_14958((&V_1), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_0 = ((GUILayoutGroup_t6_174 *)CastclassClass(L_3, GUILayoutGroup_t6_174_il2cpp_TypeInfo_var));
			GUILayoutGroup_t6_174 * L_4 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutSingleGroup_m6_1288(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m1_14959((&V_1), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_6 = V_1;
		Enumerator_t1_1847  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0041:
	{
		GUILayoutGroup_t6_174 * L_9 = ___toplevel;
		NullCheck(L_9);
		GUILayoutGroup_ResetCursor_m6_1319(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutSingleGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutSingleGroup_m6_1288 (Object_t * __this /* static, unused */, GUILayoutGroup_t6_174 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t6_51  V_4 = {0};
	{
		GUILayoutGroup_t6_174 * L_0 = ___i;
		NullCheck(L_0);
		bool L_1 = (L_0->___isWindow_15);
		if (L_1)
		{
			goto IL_0074;
		}
	}
	{
		GUILayoutGroup_t6_174 * L_2 = ___i;
		NullCheck(L_2);
		float L_3 = (((GUILayoutEntry_t6_177 *)L_2)->___minWidth_0);
		V_0 = L_3;
		GUILayoutGroup_t6_174 * L_4 = ___i;
		NullCheck(L_4);
		float L_5 = (((GUILayoutEntry_t6_177 *)L_4)->___maxWidth_1);
		V_1 = L_5;
		GUILayoutGroup_t6_174 * L_6 = ___i;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_6);
		GUILayoutGroup_t6_174 * L_7 = ___i;
		GUILayoutGroup_t6_174 * L_8 = ___i;
		NullCheck(L_8);
		Rect_t6_51 * L_9 = &(((GUILayoutEntry_t6_177 *)L_8)->___rect_4);
		float L_10 = Rect_get_x_m6_298(L_9, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_11 = ___i;
		NullCheck(L_11);
		float L_12 = (((GUILayoutEntry_t6_177 *)L_11)->___maxWidth_1);
		float L_13 = V_0;
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_7, L_10, L_15);
		GUILayoutGroup_t6_174 * L_16 = ___i;
		NullCheck(L_16);
		float L_17 = (((GUILayoutEntry_t6_177 *)L_16)->___minHeight_2);
		V_2 = L_17;
		GUILayoutGroup_t6_174 * L_18 = ___i;
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t6_177 *)L_18)->___maxHeight_3);
		V_3 = L_19;
		GUILayoutGroup_t6_174 * L_20 = ___i;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_20);
		GUILayoutGroup_t6_174 * L_21 = ___i;
		GUILayoutGroup_t6_174 * L_22 = ___i;
		NullCheck(L_22);
		Rect_t6_51 * L_23 = &(((GUILayoutEntry_t6_177 *)L_22)->___rect_4);
		float L_24 = Rect_get_y_m6_300(L_23, /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_25 = ___i;
		NullCheck(L_25);
		float L_26 = (((GUILayoutEntry_t6_177 *)L_25)->___maxHeight_3);
		float L_27 = V_2;
		float L_28 = V_3;
		float L_29 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_21, L_24, L_29);
		goto IL_00e8;
	}

IL_0074:
	{
		GUILayoutGroup_t6_174 * L_30 = ___i;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_30);
		GUILayoutGroup_t6_174 * L_31 = ___i;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___windowID_16);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_33 = GUILayoutUtility_Internal_GetWindowRect_m6_1299(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		GUILayoutGroup_t6_174 * L_34 = ___i;
		float L_35 = Rect_get_x_m6_298((&V_4), /*hidden argument*/NULL);
		float L_36 = Rect_get_width_m6_306((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_37 = ___i;
		NullCheck(L_37);
		float L_38 = (((GUILayoutEntry_t6_177 *)L_37)->___minWidth_0);
		GUILayoutGroup_t6_174 * L_39 = ___i;
		NullCheck(L_39);
		float L_40 = (((GUILayoutEntry_t6_177 *)L_39)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_41 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_34, L_35, L_41);
		GUILayoutGroup_t6_174 * L_42 = ___i;
		NullCheck(L_42);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_42);
		GUILayoutGroup_t6_174 * L_43 = ___i;
		float L_44 = Rect_get_y_m6_300((&V_4), /*hidden argument*/NULL);
		float L_45 = Rect_get_height_m6_308((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t6_174 * L_46 = ___i;
		NullCheck(L_46);
		float L_47 = (((GUILayoutEntry_t6_177 *)L_46)->___minHeight_2);
		GUILayoutGroup_t6_174 * L_48 = ___i;
		NullCheck(L_48);
		float L_49 = (((GUILayoutEntry_t6_177 *)L_48)->___maxHeight_3);
		float L_50 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_45, L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_43, L_44, L_50);
		GUILayoutGroup_t6_174 * L_51 = ___i;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___windowID_16);
		GUILayoutGroup_t6_174 * L_53 = ___i;
		NullCheck(L_53);
		Rect_t6_51  L_54 = (((GUILayoutEntry_t6_177 *)L_53)->___rect_4);
		GUILayoutUtility_Internal_MoveWindow_m6_1300(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::CreateGUILayoutGroupInstanceOfType(System.Type)
extern const Il2CppType* GUILayoutGroup_t6_174_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4815;
extern "C" GUILayoutGroup_t6_174 * GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1289 (Object_t * __this /* static, unused */, Type_t * ___LayoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t6_174_0_0_0_var = il2cpp_codegen_type_from_index(1653);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		_stringLiteral4815 = il2cpp_codegen_string_literal_from_index(4815);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(GUILayoutGroup_t6_174_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___LayoutType;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t1_1425 * L_3 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_3, _stringLiteral4815, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0020:
	{
		Type_t * L_4 = ___LayoutType;
		Object_t * L_5 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return ((GUILayoutGroup_t6_174 *)CastclassClass(L_5, GUILayoutGroup_t6_174_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutGroup(UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[],System.Type)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4816;
extern "C" GUILayoutGroup_t6_174 * GUILayoutUtility_BeginLayoutGroup_m6_1290 (Object_t * __this /* static, unused */, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, Type_t * ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		EventType_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1641);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4816 = il2cpp_codegen_string_literal_from_index(4816);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_174 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_004f;
	}

IL_001f:
	{
		Type_t * L_4 = ___layoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_174 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1289(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t6_174 * L_6 = V_0;
		GUIStyle_t6_176 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m6_1306(L_6, L_7, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_8 = ___options;
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		GUILayoutGroup_t6_174 * L_9 = V_0;
		GUILayoutOptionU5BU5D_t6_290* L_10 = ___options;
		NullCheck(L_9);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_290* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_9, L_10);
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_11 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t6_174 * L_12 = (L_11->___topLevel_0);
		GUILayoutGroup_t6_174 * L_13 = V_0;
		NullCheck(L_12);
		GUILayoutGroup_Add_m6_1322(L_12, L_13, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_004f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_14 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_14);
		GUILayoutGroup_t6_174 * L_15 = (L_14->___topLevel_0);
		NullCheck(L_15);
		GUILayoutEntry_t6_177 * L_16 = GUILayoutGroup_GetNext_m6_1320(L_15, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t6_174 *)IsInstClass(L_16, GUILayoutGroup_t6_174_il2cpp_TypeInfo_var));
		GUILayoutGroup_t6_174 * L_17 = V_0;
		if (L_17)
		{
			goto IL_0089;
		}
	}
	{
		Event_t6_162 * L_18 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = Event_get_type_m6_1164(L_18, /*hidden argument*/NULL);
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral4816, L_21, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_23 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_23, L_22, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_23);
	}

IL_0089:
	{
		GUILayoutGroup_t6_174 * L_24 = V_0;
		NullCheck(L_24);
		GUILayoutGroup_ResetCursor_m6_1319(L_24, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_0094:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_25 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GenericStack_t6_170 * L_26 = (L_25->___layoutGroups_1);
		GUILayoutGroup_t6_174 * L_27 = V_0;
		NullCheck(L_26);
		VirtActionInvoker1< Object_t * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_26, L_27);
		LayoutCache_t6_173 * L_28 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t6_174 * L_29 = V_0;
		NullCheck(L_28);
		L_28->___topLevel_0 = L_29;
		GUILayoutGroup_t6_174 * L_30 = V_0;
		return L_30;
	}
}
// System.Void UnityEngine.GUILayoutUtility::EndLayoutGroup()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_EndLayoutGroup_m6_1291 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_2 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GenericStack_t6_170 * L_3 = (L_2->___layoutGroups_1);
		NullCheck(L_3);
		VirtFuncInvoker0< Object_t * >::Invoke(19 /* System.Object System.Collections.Stack::Pop() */, L_3);
		LayoutCache_t6_173 * L_4 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t6_173 * L_5 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GenericStack_t6_170 * L_6 = (L_5->___layoutGroups_1);
		NullCheck(L_6);
		Object_t * L_7 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(18 /* System.Object System.Collections.Stack::Peek() */, L_6);
		NullCheck(L_4);
		L_4->___topLevel_0 = ((GUILayoutGroup_t6_174 *)CastclassClass(L_7, GUILayoutGroup_t6_174_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.GUILayoutGroup UnityEngine.GUILayoutUtility::BeginLayoutArea(UnityEngine.GUIStyle,System.Type)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t6_174_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4816;
extern "C" GUILayoutGroup_t6_174 * GUILayoutUtility_BeginLayoutArea_m6_1292 (Object_t * __this /* static, unused */, GUIStyle_t6_176 * ___style, Type_t * ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutGroup_t6_174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1653);
		EventType_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1641);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4816 = il2cpp_codegen_string_literal_from_index(4816);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t6_174 * V_0 = {0};
	int32_t V_1 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0042;
	}

IL_001f:
	{
		Type_t * L_4 = ___layoutType;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutGroup_t6_174 * L_5 = GUILayoutUtility_CreateGUILayoutGroupInstanceOfType_m6_1289(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		GUILayoutGroup_t6_174 * L_6 = V_0;
		GUIStyle_t6_176 * L_7 = ___style;
		NullCheck(L_6);
		GUILayoutEntry_set_style_m6_1306(L_6, L_7, /*hidden argument*/NULL);
		LayoutCache_t6_173 * L_8 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t6_174 * L_9 = (L_8->___windows_2);
		GUILayoutGroup_t6_174 * L_10 = V_0;
		NullCheck(L_9);
		GUILayoutGroup_Add_m6_1322(L_9, L_10, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_11 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GUILayoutGroup_t6_174 * L_12 = (L_11->___windows_2);
		NullCheck(L_12);
		GUILayoutEntry_t6_177 * L_13 = GUILayoutGroup_GetNext_m6_1320(L_12, /*hidden argument*/NULL);
		V_0 = ((GUILayoutGroup_t6_174 *)IsInstClass(L_13, GUILayoutGroup_t6_174_il2cpp_TypeInfo_var));
		GUILayoutGroup_t6_174 * L_14 = V_0;
		if (L_14)
		{
			goto IL_007c;
		}
	}
	{
		Event_t6_162 * L_15 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = Event_get_type_m6_1164(L_15, /*hidden argument*/NULL);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m1_556(NULL /*static, unused*/, _stringLiteral4816, L_18, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_20 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_20, L_19, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_007c:
	{
		GUILayoutGroup_t6_174 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_ResetCursor_m6_1319(L_21, /*hidden argument*/NULL);
		goto IL_0087;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_22 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_22);
		GenericStack_t6_170 * L_23 = (L_22->___layoutGroups_1);
		GUILayoutGroup_t6_174 * L_24 = V_0;
		NullCheck(L_23);
		VirtActionInvoker1< Object_t * >::Invoke(20 /* System.Void System.Collections.Stack::Push(System.Object) */, L_23, L_24);
		LayoutCache_t6_173 * L_25 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		GUILayoutGroup_t6_174 * L_26 = V_0;
		NullCheck(L_25);
		L_25->___topLevel_0 = L_26;
		GUILayoutGroup_t6_174 * L_27 = V_0;
		return L_27;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUILayoutUtility_GetRect_m6_1293 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		GUIStyle_t6_176 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_3 = GUILayoutUtility_DoGetRect_m6_1294(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIWordWrapSizer_t6_180_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUILayoutUtility_DoGetRect_m6_1294 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIWordWrapSizer_t6_180_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1659);
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m6_1505(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0091;
	}

IL_0024:
	{
		GUIStyle_t6_176 * L_4 = ___style;
		NullCheck(L_4);
		bool L_5 = GUIStyle_get_isHeightDependantOnWidth_m6_1449(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_6 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t6_174 * L_7 = (L_6->___topLevel_0);
		GUIStyle_t6_176 * L_8 = ___style;
		GUIContent_t6_171 * L_9 = ___content;
		GUILayoutOptionU5BU5D_t6_290* L_10 = ___options;
		GUIWordWrapSizer_t6_180 * L_11 = (GUIWordWrapSizer_t6_180 *)il2cpp_codegen_object_new (GUIWordWrapSizer_t6_180_il2cpp_TypeInfo_var);
		GUIWordWrapSizer__ctor_m6_1333(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUILayoutGroup_Add_m6_1322(L_7, L_11, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_004b:
	{
		GUIStyle_t6_176 * L_12 = ___style;
		GUIContent_t6_171 * L_13 = ___content;
		NullCheck(L_12);
		Vector2_t6_47  L_14 = GUIStyle_CalcSize_m6_1447(L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_15 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t6_174 * L_16 = (L_15->___topLevel_0);
		float L_17 = ((&V_0)->___x_1);
		float L_18 = ((&V_0)->___x_1);
		float L_19 = ((&V_0)->___y_2);
		float L_20 = ((&V_0)->___y_2);
		GUIStyle_t6_176 * L_21 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_22 = ___options;
		GUILayoutEntry_t6_177 * L_23 = (GUILayoutEntry_t6_177 *)il2cpp_codegen_object_new (GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1303(L_23, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUILayoutGroup_Add_m6_1322(L_16, L_23, /*hidden argument*/NULL);
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_24 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_24;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_25 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_25;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_26 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_26);
		GUILayoutGroup_t6_174 * L_27 = (L_26->___topLevel_0);
		NullCheck(L_27);
		GUILayoutEntry_t6_177 * L_28 = GUILayoutGroup_GetNext_m6_1320(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rect_t6_51  L_29 = (L_28->___rect_4);
		return L_29;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUILayoutUtility_GetRect_m6_1295 (Object_t * __this /* static, unused */, float ___width, float ___height, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = ___width;
		float L_2 = ___height;
		float L_3 = ___height;
		GUIStyle_t6_176 * L_4 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_5 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_6 = GUILayoutUtility_DoGetRect_m6_1296(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUILayoutUtility_DoGetRect_m6_1296 (Object_t * __this /* static, unused */, float ___minWidth, float ___maxWidth, float ___minHeight, float ___maxHeight, GUIStyle_t6_176 * ___style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_0041;
		}
	}
	{
		goto IL_0047;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_4 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_4);
		GUILayoutGroup_t6_174 * L_5 = (L_4->___topLevel_0);
		float L_6 = ___minWidth;
		float L_7 = ___maxWidth;
		float L_8 = ___minHeight;
		float L_9 = ___maxHeight;
		GUIStyle_t6_176 * L_10 = ___style;
		GUILayoutOptionU5BU5D_t6_290* L_11 = ___options;
		GUILayoutEntry_t6_177 * L_12 = (GUILayoutEntry_t6_177 *)il2cpp_codegen_object_new (GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1303(L_12, L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_5);
		GUILayoutGroup_Add_m6_1322(L_5, L_12, /*hidden argument*/NULL);
		Rect_t6_51  L_13 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_13;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_14 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_14;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_15 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t6_174 * L_16 = (L_15->___topLevel_0);
		NullCheck(L_16);
		GUILayoutEntry_t6_177 * L_17 = GUILayoutGroup_GetNext_m6_1320(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Rect_t6_51  L_18 = (L_17->___rect_4);
		return L_18;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetLastRect()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" Rect_t6_51  GUILayoutUtility_GetLastRect_m6_1297 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002b;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_4 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_4;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		Rect_t6_51  L_5 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_5;
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		LayoutCache_t6_173 * L_6 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t6_174 * L_7 = (L_6->___topLevel_0);
		NullCheck(L_7);
		Rect_t6_51  L_8 = GUILayoutGroup_GetLast_m6_1321(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutUtility::get_spaceStyle()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t6_176 * GUILayoutUtility_get_spaceStyle_m6_1298 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_2 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		NullCheck(L_2);
		GUIStyle_set_stretchWidth_m6_1471(L_2, 0, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_3 = ((GUILayoutUtility_t6_175_StaticFields*)GUILayoutUtility_t6_175_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)
extern "C" Rect_t6_51  GUILayoutUtility_Internal_GetWindowRect_m6_1299 (Object_t * __this /* static, unused */, int32_t ___windowID, const MethodInfo* method)
{
	typedef Rect_t6_51  (*GUILayoutUtility_Internal_GetWindowRect_m6_1299_ftn) (int32_t);
	static GUILayoutUtility_Internal_GetWindowRect_m6_1299_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_Internal_GetWindowRect_m6_1299_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)");
	return _il2cpp_icall_func(___windowID);
}
// System.Void UnityEngine.GUILayoutUtility::Internal_MoveWindow(System.Int32,UnityEngine.Rect)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Internal_MoveWindow_m6_1300 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t6_51  ___r, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1301(NULL /*static, unused*/, L_0, (&___r), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1301 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t6_51 * ___r, const MethodInfo* method)
{
	typedef void (*GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1301_ftn) (int32_t, Rect_t6_51 *);
	static GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1301_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m6_1301_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)");
	_il2cpp_icall_func(___windowID, ___r);
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m6_1302 (GUILayoutEntry_t6_177 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t6_176 * ____style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = {0};
		Rect__ctor_m6_295(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_1 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t6_176 * L_6 = ____style;
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_7 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		____style = L_7;
	}

IL_005b:
	{
		GUIStyle_t6_176 * L_8 = ____style;
		GUILayoutEntry_set_style_m6_1306(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m6_1303 (GUILayoutEntry_t6_177 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t6_176 * ____style, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = {0};
		Rect__ctor_m6_295(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_1 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t6_176 * L_6 = ____style;
		GUILayoutEntry_set_style_m6_1306(__this, L_6, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_7 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_290* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_7);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.cctor()
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__cctor_m6_1304 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t6_51  L_0 = {0};
		Rect__ctor_m6_295(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8 = L_0;
		((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9 = 0;
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::get_style()
extern "C" GUIStyle_t6_176 * GUILayoutEntry_get_style_m6_1305 (GUILayoutEntry_t6_177 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_Style_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutEntry::set_style(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_set_style_m6_1306 (GUILayoutEntry_t6_177 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_Style_7 = L_0;
		GUIStyle_t6_176 * L_1 = ___value;
		VirtActionInvoker1< GUIStyle_t6_176 * >::Invoke(9 /* System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle) */, __this, L_1);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin()
extern "C" RectOffset_t6_178 * GUILayoutEntry_get_margin_m6_1307 (GUILayoutEntry_t6_177 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectOffset_t6_178 * L_1 = GUIStyle_get_margin_m6_1433(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcWidth()
extern "C" void GUILayoutEntry_CalcWidth_m6_1308 (GUILayoutEntry_t6_177 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcHeight()
extern "C" void GUILayoutEntry_CalcHeight_m6_1309 (GUILayoutEntry_t6_177 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetHorizontal_m6_1310 (GUILayoutEntry_t6_177 * __this, float ___x, float ___width, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___rect_4);
		float L_1 = ___x;
		Rect_set_x_m6_299(L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___rect_4);
		float L_3 = ___width;
		Rect_set_width_m6_307(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetVertical_m6_1311 (GUILayoutEntry_t6_177 * __this, float ___y, float ___height, const MethodInfo* method)
{
	{
		Rect_t6_51 * L_0 = &(__this->___rect_4);
		float L_1 = ___y;
		Rect_set_y_m6_301(L_0, L_1, /*hidden argument*/NULL);
		Rect_t6_51 * L_2 = &(__this->___rect_4);
		float L_3 = ___height;
		Rect_set_height_m6_309(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_ApplyStyleSettings_m6_1312 (GUILayoutEntry_t6_177 * __this, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	GUILayoutEntry_t6_177 * G_B3_0 = {0};
	GUILayoutEntry_t6_177 * G_B1_0 = {0};
	GUILayoutEntry_t6_177 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	GUILayoutEntry_t6_177 * G_B4_1 = {0};
	GUILayoutEntry_t6_177 * G_B7_0 = {0};
	GUILayoutEntry_t6_177 * G_B5_0 = {0};
	GUILayoutEntry_t6_177 * G_B6_0 = {0};
	int32_t G_B8_0 = 0;
	GUILayoutEntry_t6_177 * G_B8_1 = {0};
	{
		GUIStyle_t6_176 * L_0 = ___style;
		NullCheck(L_0);
		float L_1 = GUIStyle_get_fixedWidth_m6_1468(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			G_B3_0 = __this;
			goto IL_0022;
		}
	}
	{
		GUIStyle_t6_176 * L_2 = ___style;
		NullCheck(L_2);
		bool L_3 = GUIStyle_get_stretchWidth_m6_1470(L_2, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0022;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		G_B4_1->___stretchWidth_5 = G_B4_0;
		GUIStyle_t6_176 * L_4 = ___style;
		NullCheck(L_4);
		float L_5 = GUIStyle_get_fixedHeight_m6_1469(L_4, /*hidden argument*/NULL);
		G_B5_0 = __this;
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_004a;
		}
	}
	{
		GUIStyle_t6_176 * L_6 = ___style;
		NullCheck(L_6);
		bool L_7 = GUIStyle_get_stretchHeight_m6_1472(L_6, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
		if (!L_7)
		{
			G_B7_0 = G_B5_0;
			goto IL_004a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_004b:
	{
		NullCheck(G_B8_1);
		G_B8_1->___stretchHeight_6 = G_B8_0;
		GUIStyle_t6_176 * L_8 = ___style;
		__this->___m_Style_7 = L_8;
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry_ApplyOptions_m6_1313 (GUILayoutEntry_t6_177 * __this, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t6_182 * V_0 = {0};
	GUILayoutOptionU5BU5D_t6_290* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	float V_4 = 0.0f;
	{
		GUILayoutOptionU5BU5D_t6_290* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t6_290* L_1 = ___options;
		V_1 = L_1;
		V_2 = 0;
		goto IL_01a0;
	}

IL_0010:
	{
		GUILayoutOptionU5BU5D_t6_290* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_2, L_4, sizeof(GUILayoutOption_t6_182 *)));
		GUILayoutOption_t6_182 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___type_0);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0046;
		}
		if (L_7 == 1)
		{
			goto IL_006e;
		}
		if (L_7 == 2)
		{
			goto IL_0096;
		}
		if (L_7 == 3)
		{
			goto IL_00c9;
		}
		if (L_7 == 4)
		{
			goto IL_0103;
		}
		if (L_7 == 5)
		{
			goto IL_0136;
		}
		if (L_7 == 6)
		{
			goto IL_0170;
		}
		if (L_7 == 7)
		{
			goto IL_0186;
		}
	}
	{
		goto IL_019c;
	}

IL_0046:
	{
		GUILayoutOption_t6_182 * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (L_8->___value_1);
		float L_10 = ((*(float*)((float*)UnBox (L_9, Single_t1_17_il2cpp_TypeInfo_var))));
		V_4 = L_10;
		__this->___maxWidth_1 = L_10;
		float L_11 = V_4;
		__this->___minWidth_0 = L_11;
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_006e:
	{
		GUILayoutOption_t6_182 * L_12 = V_0;
		NullCheck(L_12);
		Object_t * L_13 = (L_12->___value_1);
		float L_14 = ((*(float*)((float*)UnBox (L_13, Single_t1_17_il2cpp_TypeInfo_var))));
		V_4 = L_14;
		__this->___maxHeight_3 = L_14;
		float L_15 = V_4;
		__this->___minHeight_2 = L_15;
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0096:
	{
		GUILayoutOption_t6_182 * L_16 = V_0;
		NullCheck(L_16);
		Object_t * L_17 = (L_16->___value_1);
		__this->___minWidth_0 = ((*(float*)((float*)UnBox (L_17, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_18 = (__this->___maxWidth_1);
		float L_19 = (__this->___minWidth_0);
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00c4;
		}
	}
	{
		float L_20 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_20;
	}

IL_00c4:
	{
		goto IL_019c;
	}

IL_00c9:
	{
		GUILayoutOption_t6_182 * L_21 = V_0;
		NullCheck(L_21);
		Object_t * L_22 = (L_21->___value_1);
		__this->___maxWidth_1 = ((*(float*)((float*)UnBox (L_22, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_23 = (__this->___minWidth_0);
		float L_24 = (__this->___maxWidth_1);
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00f7;
		}
	}
	{
		float L_25 = (__this->___maxWidth_1);
		__this->___minWidth_0 = L_25;
	}

IL_00f7:
	{
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_0103:
	{
		GUILayoutOption_t6_182 * L_26 = V_0;
		NullCheck(L_26);
		Object_t * L_27 = (L_26->___value_1);
		__this->___minHeight_2 = ((*(float*)((float*)UnBox (L_27, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_28 = (__this->___maxHeight_3);
		float L_29 = (__this->___minHeight_2);
		if ((!(((float)L_28) < ((float)L_29))))
		{
			goto IL_0131;
		}
	}
	{
		float L_30 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_30;
	}

IL_0131:
	{
		goto IL_019c;
	}

IL_0136:
	{
		GUILayoutOption_t6_182 * L_31 = V_0;
		NullCheck(L_31);
		Object_t * L_32 = (L_31->___value_1);
		__this->___maxHeight_3 = ((*(float*)((float*)UnBox (L_32, Single_t1_17_il2cpp_TypeInfo_var))));
		float L_33 = (__this->___minHeight_2);
		float L_34 = (__this->___maxHeight_3);
		if ((!(((float)L_33) > ((float)L_34))))
		{
			goto IL_0164;
		}
	}
	{
		float L_35 = (__this->___maxHeight_3);
		__this->___minHeight_2 = L_35;
	}

IL_0164:
	{
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0170:
	{
		GUILayoutOption_t6_182 * L_36 = V_0;
		NullCheck(L_36);
		Object_t * L_37 = (L_36->___value_1);
		__this->___stretchWidth_5 = ((*(int32_t*)((int32_t*)UnBox (L_37, Int32_t1_3_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_0186:
	{
		GUILayoutOption_t6_182 * L_38 = V_0;
		NullCheck(L_38);
		Object_t * L_39 = (L_38->___value_1);
		__this->___stretchHeight_6 = ((*(int32_t*)((int32_t*)UnBox (L_39, Int32_t1_3_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_019c:
	{
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_41 = V_2;
		GUILayoutOptionU5BU5D_t6_290* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		float L_43 = (__this->___maxWidth_1);
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_44 = (__this->___maxWidth_1);
		float L_45 = (__this->___minWidth_0);
		if ((!(((float)L_44) < ((float)L_45))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_46 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_46;
	}

IL_01d6:
	{
		float L_47 = (__this->___maxHeight_3);
		if ((((float)L_47) == ((float)(0.0f))))
		{
			goto IL_0203;
		}
	}
	{
		float L_48 = (__this->___maxHeight_3);
		float L_49 = (__this->___minHeight_2);
		if ((!(((float)L_48) < ((float)L_49))))
		{
			goto IL_0203;
		}
	}
	{
		float L_50 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_50;
	}

IL_0203:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutEntry::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral268;
extern Il2CppCodeGenString* _stringLiteral4817;
extern Il2CppCodeGenString* _stringLiteral4818;
extern Il2CppCodeGenString* _stringLiteral4819;
extern Il2CppCodeGenString* _stringLiteral636;
extern Il2CppCodeGenString* _stringLiteral1117;
extern Il2CppCodeGenString* _stringLiteral4820;
extern "C" String_t* GUILayoutEntry_ToString_m6_1314 (GUILayoutEntry_t6_177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		_stringLiteral4817 = il2cpp_codegen_string_literal_from_index(4817);
		_stringLiteral4818 = il2cpp_codegen_string_literal_from_index(4818);
		_stringLiteral4819 = il2cpp_codegen_string_literal_from_index(4819);
		_stringLiteral636 = il2cpp_codegen_string_literal_from_index(636);
		_stringLiteral1117 = il2cpp_codegen_string_literal_from_index(1117);
		_stringLiteral4820 = il2cpp_codegen_string_literal_from_index(4820);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t1_272* G_B5_1 = {0};
	ObjectU5BU5D_t1_272* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B5_4 = 0;
	ObjectU5BU5D_t1_272* G_B5_5 = {0};
	ObjectU5BU5D_t1_272* G_B5_6 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t1_272* G_B4_1 = {0};
	ObjectU5BU5D_t1_272* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B4_4 = 0;
	ObjectU5BU5D_t1_272* G_B4_5 = {0};
	ObjectU5BU5D_t1_272* G_B4_6 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t1_272* G_B6_2 = {0};
	ObjectU5BU5D_t1_272* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	int32_t G_B6_5 = 0;
	ObjectU5BU5D_t1_272* G_B6_6 = {0};
	ObjectU5BU5D_t1_272* G_B6_7 = {0};
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t1_272* G_B8_1 = {0};
	ObjectU5BU5D_t1_272* G_B8_2 = {0};
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t1_272* G_B7_1 = {0};
	ObjectU5BU5D_t1_272* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t1_272* G_B9_2 = {0};
	ObjectU5BU5D_t1_272* G_B9_3 = {0};
	int32_t G_B11_0 = 0;
	ObjectU5BU5D_t1_272* G_B11_1 = {0};
	ObjectU5BU5D_t1_272* G_B11_2 = {0};
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t1_272* G_B10_1 = {0};
	ObjectU5BU5D_t1_272* G_B10_2 = {0};
	String_t* G_B12_0 = {0};
	int32_t G_B12_1 = 0;
	ObjectU5BU5D_t1_272* G_B12_2 = {0};
	ObjectU5BU5D_t1_272* G_B12_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		goto IL_001d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m1_559(NULL /*static, unused*/, L_1, _stringLiteral268, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		int32_t L_5 = ((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_6 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, ((int32_t)12)));
		String_t* L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_6;
		ObjectU5BU5D_t1_272* L_9 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 6));
		GUIStyle_t6_176 * L_10 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		G_B4_0 = 0;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		G_B4_3 = _stringLiteral4817;
		G_B4_4 = 1;
		G_B4_5 = L_8;
		G_B4_6 = L_8;
		if (!L_10)
		{
			G_B5_0 = 0;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			G_B5_3 = _stringLiteral4817;
			G_B5_4 = 1;
			G_B5_5 = L_8;
			G_B5_6 = L_8;
			goto IL_005d;
		}
	}
	{
		GUIStyle_t6_176 * L_11 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = GUIStyle_get_name_m6_1455(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		G_B6_5 = G_B4_4;
		G_B6_6 = G_B4_5;
		G_B6_7 = G_B4_6;
		goto IL_0062;
	}

IL_005d:
	{
		G_B6_0 = _stringLiteral4818;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
		G_B6_5 = G_B5_4;
		G_B6_6 = G_B5_5;
		G_B6_7 = G_B5_6;
	}

IL_0062:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t1_272* L_13 = G_B6_3;
		Type_t * L_14 = Object_GetType_m1_5(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1, sizeof(Object_t *))) = (Object_t *)L_14;
		ObjectU5BU5D_t1_272* L_15 = L_13;
		Rect_t6_51 * L_16 = &(__this->___rect_4);
		float L_17 = Rect_get_x_m6_298(L_16, /*hidden argument*/NULL);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2, sizeof(Object_t *))) = (Object_t *)L_19;
		ObjectU5BU5D_t1_272* L_20 = L_15;
		Rect_t6_51 * L_21 = &(__this->___rect_4);
		float L_22 = Rect_get_xMax_m6_315(L_21, /*hidden argument*/NULL);
		float L_23 = L_22;
		Object_t * L_24 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 3, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_272* L_25 = L_20;
		Rect_t6_51 * L_26 = &(__this->___rect_4);
		float L_27 = Rect_get_y_m6_300(L_26, /*hidden argument*/NULL);
		float L_28 = L_27;
		Object_t * L_29 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4, sizeof(Object_t *))) = (Object_t *)L_29;
		ObjectU5BU5D_t1_272* L_30 = L_25;
		Rect_t6_51 * L_31 = &(__this->___rect_4);
		float L_32 = Rect_get_yMax_m6_317(L_31, /*hidden argument*/NULL);
		float L_33 = L_32;
		Object_t * L_34 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 5);
		ArrayElementTypeCheck (L_30, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 5, sizeof(Object_t *))) = (Object_t *)L_34;
		String_t* L_35 = UnityString_Format_m6_573(NULL /*static, unused*/, G_B6_4, L_30, /*hidden argument*/NULL);
		NullCheck(G_B6_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_6, G_B6_5);
		ArrayElementTypeCheck (G_B6_6, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_6, G_B6_5, sizeof(Object_t *))) = (Object_t *)L_35;
		ObjectU5BU5D_t1_272* L_36 = G_B6_7;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		ArrayElementTypeCheck (L_36, _stringLiteral4819);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4819;
		ObjectU5BU5D_t1_272* L_37 = L_36;
		float L_38 = (__this->___minWidth_0);
		float L_39 = L_38;
		Object_t * L_40 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 3);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 3, sizeof(Object_t *))) = (Object_t *)L_40;
		ObjectU5BU5D_t1_272* L_41 = L_37;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 4);
		ArrayElementTypeCheck (L_41, _stringLiteral636);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral636;
		ObjectU5BU5D_t1_272* L_42 = L_41;
		float L_43 = (__this->___maxWidth_1);
		float L_44 = L_43;
		Object_t * L_45 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 5);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 5, sizeof(Object_t *))) = (Object_t *)L_45;
		ObjectU5BU5D_t1_272* L_46 = L_42;
		int32_t L_47 = (__this->___stretchWidth_5);
		G_B7_0 = 6;
		G_B7_1 = L_46;
		G_B7_2 = L_46;
		if (!L_47)
		{
			G_B8_0 = 6;
			G_B8_1 = L_46;
			G_B8_2 = L_46;
			goto IL_0101;
		}
	}
	{
		G_B9_0 = _stringLiteral1117;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0106;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B9_0 = L_48;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0106:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B9_2, G_B9_1, sizeof(Object_t *))) = (Object_t *)G_B9_0;
		ObjectU5BU5D_t1_272* L_49 = G_B9_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 7);
		ArrayElementTypeCheck (L_49, _stringLiteral4820);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_49, 7, sizeof(Object_t *))) = (Object_t *)_stringLiteral4820;
		ObjectU5BU5D_t1_272* L_50 = L_49;
		float L_51 = (__this->___minHeight_2);
		float L_52 = L_51;
		Object_t * L_53 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 8);
		ArrayElementTypeCheck (L_50, L_53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 8, sizeof(Object_t *))) = (Object_t *)L_53;
		ObjectU5BU5D_t1_272* L_54 = L_50;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)9));
		ArrayElementTypeCheck (L_54, _stringLiteral636);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_54, ((int32_t)9), sizeof(Object_t *))) = (Object_t *)_stringLiteral636;
		ObjectU5BU5D_t1_272* L_55 = L_54;
		float L_56 = (__this->___maxHeight_3);
		float L_57 = L_56;
		Object_t * L_58 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)10));
		ArrayElementTypeCheck (L_55, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, ((int32_t)10), sizeof(Object_t *))) = (Object_t *)L_58;
		ObjectU5BU5D_t1_272* L_59 = L_55;
		int32_t L_60 = (__this->___stretchHeight_6);
		G_B10_0 = ((int32_t)11);
		G_B10_1 = L_59;
		G_B10_2 = L_59;
		if (!L_60)
		{
			G_B11_0 = ((int32_t)11);
			G_B11_1 = L_59;
			G_B11_2 = L_59;
			goto IL_014d;
		}
	}
	{
		G_B12_0 = _stringLiteral1117;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0152;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B12_0 = L_61;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0152:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B12_2, G_B12_1, sizeof(Object_t *))) = (Object_t *)G_B12_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m1_562(NULL /*static, unused*/, G_B12_3, /*hidden argument*/NULL);
		return L_62;
	}
}
// System.Void UnityEngine.GUILayoutGroup::.ctor()
extern TypeInfo* List_1_t1_1828_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t6_178_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14960_MethodInfo_var;
extern "C" void GUILayoutGroup__ctor_m6_1315 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_1828_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1660);
		RectOffset_t6_178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1661);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		List_1__ctor_m1_14960_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483801);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_1828 * L_0 = (List_1_t1_1828 *)il2cpp_codegen_object_new (List_1_t1_1828_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14960(L_0, /*hidden argument*/List_1__ctor_m1_14960_MethodInfo_var);
		__this->___entries_10 = L_0;
		__this->___isVertical_11 = 1;
		__this->___sameSize_14 = 1;
		__this->___windowID_16 = (-1);
		__this->___m_StretchableCountX_18 = ((int32_t)100);
		__this->___m_StretchableCountY_19 = ((int32_t)100);
		__this->___m_ChildMinWidth_22 = (100.0f);
		__this->___m_ChildMaxWidth_23 = (100.0f);
		__this->___m_ChildMinHeight_24 = (100.0f);
		__this->___m_ChildMaxHeight_25 = (100.0f);
		RectOffset_t6_178 * L_1 = (RectOffset_t6_178 *)il2cpp_codegen_object_new (RectOffset_t6_178_il2cpp_TypeInfo_var);
		RectOffset__ctor_m6_1410(L_1, /*hidden argument*/NULL);
		__this->___m_Margin_26 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_2 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1302(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
extern "C" RectOffset_t6_178 * GUILayoutGroup_get_margin_m6_1316 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	{
		RectOffset_t6_178 * L_0 = (__this->___m_Margin_26);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern "C" void GUILayoutGroup_ApplyOptions_m6_1317 (GUILayoutGroup_t6_174 * __this, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t6_182 * V_0 = {0};
	GUILayoutOptionU5BU5D_t6_290* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		GUILayoutOptionU5BU5D_t6_290* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t6_290* L_1 = ___options;
		GUILayoutEntry_ApplyOptions_m6_1313(__this, L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t6_290* L_2 = ___options;
		V_1 = L_2;
		V_2 = 0;
		goto IL_0098;
	}

IL_0017:
	{
		GUILayoutOptionU5BU5D_t6_290* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = (*(GUILayoutOption_t6_182 **)(GUILayoutOption_t6_182 **)SZArrayLdElema(L_3, L_5, sizeof(GUILayoutOption_t6_182 *)));
		GUILayoutOption_t6_182 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___type_0);
		V_3 = L_7;
		int32_t L_8 = V_3;
		if (L_8 == 0)
		{
			goto IL_0065;
		}
		if (L_8 == 1)
		{
			goto IL_0071;
		}
		if (L_8 == 2)
		{
			goto IL_0065;
		}
		if (L_8 == 3)
		{
			goto IL_0065;
		}
		if (L_8 == 4)
		{
			goto IL_0071;
		}
		if (L_8 == 5)
		{
			goto IL_0071;
		}
		if (L_8 == 6)
		{
			goto IL_0094;
		}
		if (L_8 == 7)
		{
			goto IL_0094;
		}
		if (L_8 == 8)
		{
			goto IL_0094;
		}
		if (L_8 == 9)
		{
			goto IL_0094;
		}
		if (L_8 == 10)
		{
			goto IL_0094;
		}
		if (L_8 == 11)
		{
			goto IL_0094;
		}
		if (L_8 == 12)
		{
			goto IL_0094;
		}
		if (L_8 == 13)
		{
			goto IL_007d;
		}
	}
	{
		goto IL_0094;
	}

IL_0065:
	{
		__this->___m_UserSpecifiedHeight_21 = 1;
		goto IL_0094;
	}

IL_0071:
	{
		__this->___m_UserSpecifiedWidth_20 = 1;
		goto IL_0094;
	}

IL_007d:
	{
		GUILayoutOption_t6_182 * L_9 = V_0;
		NullCheck(L_9);
		Object_t * L_10 = (L_9->___value_1);
		__this->___spacing_13 = (((float)((float)((*(int32_t*)((int32_t*)UnBox (L_10, Int32_t1_3_il2cpp_TypeInfo_var)))))));
		goto IL_0094;
	}

IL_0094:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_12 = V_2;
		GUILayoutOptionU5BU5D_t6_290* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_13)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutGroup_ApplyStyleSettings_m6_1318 (GUILayoutGroup_t6_174 * __this, GUIStyle_t6_176 * ___style, const MethodInfo* method)
{
	RectOffset_t6_178 * V_0 = {0};
	{
		GUIStyle_t6_176 * L_0 = ___style;
		GUILayoutEntry_ApplyStyleSettings_m6_1312(__this, L_0, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_1 = ___style;
		NullCheck(L_1);
		RectOffset_t6_178 * L_2 = GUIStyle_get_margin_m6_1433(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		RectOffset_t6_178 * L_3 = (__this->___m_Margin_26);
		RectOffset_t6_178 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_left_m6_1416(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_set_left_m6_1417(L_3, L_5, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_6 = (__this->___m_Margin_26);
		RectOffset_t6_178 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = RectOffset_get_right_m6_1418(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectOffset_set_right_m6_1419(L_6, L_8, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_9 = (__this->___m_Margin_26);
		RectOffset_t6_178 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m6_1420(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_top_m6_1421(L_9, L_11, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_12 = (__this->___m_Margin_26);
		RectOffset_t6_178 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_bottom_m6_1422(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectOffset_set_bottom_m6_1423(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
extern "C" void GUILayoutGroup_ResetCursor_m6_1319 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	{
		__this->___m_Cursor_17 = 0;
		return;
	}
}
// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::GetNext()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4821;
extern Il2CppCodeGenString* _stringLiteral4822;
extern Il2CppCodeGenString* _stringLiteral4823;
extern Il2CppCodeGenString* _stringLiteral4824;
extern "C" GUILayoutEntry_t6_177 * GUILayoutGroup_GetNext_m6_1320 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		EventType_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1641);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4821 = il2cpp_codegen_string_literal_from_index(4821);
		_stringLiteral4822 = il2cpp_codegen_string_literal_from_index(4822);
		_stringLiteral4823 = il2cpp_codegen_string_literal_from_index(4823);
		_stringLiteral4824 = il2cpp_codegen_string_literal_from_index(4824);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutEntry_t6_177 * V_0 = {0};
	{
		int32_t L_0 = (__this->___m_Cursor_17);
		List_1_t1_1828 * L_1 = (__this->___entries_10);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_1);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_0038;
		}
	}
	{
		List_1_t1_1828 * L_3 = (__this->___entries_10);
		int32_t L_4 = (__this->___m_Cursor_17);
		NullCheck(L_3);
		GUILayoutEntry_t6_177 * L_5 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_3, L_4);
		V_0 = L_5;
		int32_t L_6 = (__this->___m_Cursor_17);
		__this->___m_Cursor_17 = ((int32_t)((int32_t)L_6+(int32_t)1));
		GUILayoutEntry_t6_177 * L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		ObjectU5BU5D_t1_272* L_8 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 7));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, _stringLiteral4821);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4821;
		ObjectU5BU5D_t1_272* L_9 = L_8;
		int32_t L_10 = (__this->___m_Cursor_17);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 1, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t1_272* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral4822);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4822;
		ObjectU5BU5D_t1_272* L_14 = L_13;
		List_1_t1_1828 * L_15 = (__this->___entries_10);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_15);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3, sizeof(Object_t *))) = (Object_t *)L_18;
		ObjectU5BU5D_t1_272* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral4823);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4823;
		ObjectU5BU5D_t1_272* L_20 = L_19;
		Event_t6_162 * L_21 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_rawType_m6_1163(L_21, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t1_272* L_25 = L_20;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
		ArrayElementTypeCheck (L_25, _stringLiteral4824);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral4824;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m1_562(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_27 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_27, L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_27);
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutGroup::GetLast()
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4825;
extern Il2CppCodeGenString* _stringLiteral4821;
extern Il2CppCodeGenString* _stringLiteral4822;
extern Il2CppCodeGenString* _stringLiteral4823;
extern "C" Rect_t6_51  GUILayoutGroup_GetLast_m6_1321 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		EventType_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1641);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4825 = il2cpp_codegen_string_literal_from_index(4825);
		_stringLiteral4821 = il2cpp_codegen_string_literal_from_index(4821);
		_stringLiteral4822 = il2cpp_codegen_string_literal_from_index(4822);
		_stringLiteral4823 = il2cpp_codegen_string_literal_from_index(4823);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutEntry_t6_177 * V_0 = {0};
	{
		int32_t L_0 = (__this->___m_Cursor_17);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4825, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		Rect_t6_51  L_1 = ((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8;
		return L_1;
	}

IL_001b:
	{
		int32_t L_2 = (__this->___m_Cursor_17);
		List_1_t1_1828 * L_3 = (__this->___entries_10);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_3);
		if ((((int32_t)L_2) > ((int32_t)L_4)))
		{
			goto IL_004c;
		}
	}
	{
		List_1_t1_1828 * L_5 = (__this->___entries_10);
		int32_t L_6 = (__this->___m_Cursor_17);
		NullCheck(L_5);
		GUILayoutEntry_t6_177 * L_7 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_5, ((int32_t)((int32_t)L_6-(int32_t)1)));
		V_0 = L_7;
		GUILayoutEntry_t6_177 * L_8 = V_0;
		NullCheck(L_8);
		Rect_t6_51  L_9 = (L_8->___rect_4);
		return L_9;
	}

IL_004c:
	{
		ObjectU5BU5D_t1_272* L_10 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 6));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, _stringLiteral4821);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4821;
		ObjectU5BU5D_t1_272* L_11 = L_10;
		int32_t L_12 = (__this->___m_Cursor_17);
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 1, sizeof(Object_t *))) = (Object_t *)L_14;
		ObjectU5BU5D_t1_272* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, _stringLiteral4822);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4822;
		ObjectU5BU5D_t1_272* L_16 = L_15;
		List_1_t1_1828 * L_17 = (__this->___entries_10);
		NullCheck(L_17);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_17);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 3, sizeof(Object_t *))) = (Object_t *)L_20;
		ObjectU5BU5D_t1_272* L_21 = L_16;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 4);
		ArrayElementTypeCheck (L_21, _stringLiteral4823);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4823;
		ObjectU5BU5D_t1_272* L_22 = L_21;
		Event_t6_162 * L_23 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = Event_get_type_m6_1164(L_23, /*hidden argument*/NULL);
		int32_t L_25 = L_24;
		Object_t * L_26 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 5);
		ArrayElementTypeCheck (L_22, L_26);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 5, sizeof(Object_t *))) = (Object_t *)L_26;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m1_562(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Debug_LogError_m6_632(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		Rect_t6_51  L_28 = ((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8;
		return L_28;
	}
}
// System.Void UnityEngine.GUILayoutGroup::Add(UnityEngine.GUILayoutEntry)
extern "C" void GUILayoutGroup_Add_m6_1322 (GUILayoutGroup_t6_174 * __this, GUILayoutEntry_t6_177 * ___e, const MethodInfo* method)
{
	{
		List_1_t1_1828 * L_0 = (__this->___entries_10);
		GUILayoutEntry_t6_177 * L_1 = ___e;
		NullCheck(L_0);
		VirtActionInvoker1< GUILayoutEntry_t6_177 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1847_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14957_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14958_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14959_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcWidth_m6_1323 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		Enumerator_t1_1847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1658);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		List_1_GetEnumerator_m1_14957_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		Enumerator_get_Current_m1_14958_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		Enumerator_MoveNext_m1_14959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	GUILayoutEntry_t6_177 * V_3 = {0};
	Enumerator_t1_1847  V_4 = {0};
	RectOffset_t6_178 * V_5 = {0};
	int32_t V_6 = 0;
	GUILayoutEntry_t6_177 * V_7 = {0};
	Enumerator_t1_1847  V_8 = {0};
	RectOffset_t6_178 * V_9 = {0};
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B39_1 = 0;
	GUILayoutGroup_t6_174 * G_B39_2 = {0};
	int32_t G_B38_0 = 0;
	int32_t G_B38_1 = 0;
	GUILayoutGroup_t6_174 * G_B38_2 = {0};
	int32_t G_B40_0 = 0;
	int32_t G_B40_1 = 0;
	int32_t G_B40_2 = 0;
	GUILayoutGroup_t6_174 * G_B40_3 = {0};
	{
		List_1_t1_1828 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t6_176 * L_2 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t6_178 * L_3 = GUIStyle_get_padding_m6_1434(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_horizontal_m6_1424(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)((float)L_4)));
		V_13 = L_5;
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_5;
		float L_6 = V_13;
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_6;
		return;
	}

IL_0033:
	{
		V_0 = 0;
		V_1 = 0;
		__this->___m_ChildMinWidth_22 = (0.0f);
		__this->___m_ChildMaxWidth_23 = (0.0f);
		__this->___m_StretchableCountX_18 = 0;
		V_2 = 1;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_016a;
		}
	}
	{
		List_1_t1_1828 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1_1847  L_9 = List_1_GetEnumerator_m1_14957(L_8, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_4 = L_9;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0125;
		}

IL_0073:
		{
			GUILayoutEntry_t6_177 * L_10 = Enumerator_get_Current_m1_14958((&V_4), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_3 = L_10;
			GUILayoutEntry_t6_177 * L_11 = V_3;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_11);
			GUILayoutEntry_t6_177 * L_12 = V_3;
			NullCheck(L_12);
			RectOffset_t6_178 * L_13 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_5 = L_13;
			GUILayoutEntry_t6_177 * L_14 = V_3;
			NullCheck(L_14);
			GUIStyle_t6_176 * L_15 = GUILayoutEntry_get_style_m6_1305(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUIStyle_t6_176 * L_16 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_176 *)L_15) == ((Object_t*)(GUIStyle_t6_176 *)L_16)))
			{
				goto IL_0112;
			}
		}

IL_0099:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00c0;
			}
		}

IL_009f:
		{
			RectOffset_t6_178 * L_18 = V_5;
			NullCheck(L_18);
			int32_t L_19 = RectOffset_get_left_m6_1416(L_18, /*hidden argument*/NULL);
			int32_t L_20 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Min_m6_428(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			V_0 = L_21;
			RectOffset_t6_178 * L_22 = V_5;
			NullCheck(L_22);
			int32_t L_23 = RectOffset_get_right_m6_1418(L_22, /*hidden argument*/NULL);
			int32_t L_24 = V_1;
			int32_t L_25 = Mathf_Min_m6_428(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
			V_1 = L_25;
			goto IL_00d2;
		}

IL_00c0:
		{
			RectOffset_t6_178 * L_26 = V_5;
			NullCheck(L_26);
			int32_t L_27 = RectOffset_get_left_m6_1416(L_26, /*hidden argument*/NULL);
			V_0 = L_27;
			RectOffset_t6_178 * L_28 = V_5;
			NullCheck(L_28);
			int32_t L_29 = RectOffset_get_right_m6_1418(L_28, /*hidden argument*/NULL);
			V_1 = L_29;
			V_2 = 0;
		}

IL_00d2:
		{
			GUILayoutEntry_t6_177 * L_30 = V_3;
			NullCheck(L_30);
			float L_31 = (L_30->___minWidth_0);
			RectOffset_t6_178 * L_32 = V_5;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_horizontal_m6_1424(L_32, /*hidden argument*/NULL);
			float L_34 = (__this->___m_ChildMinWidth_22);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_35 = Mathf_Max_m6_429(NULL /*static, unused*/, ((float)((float)L_31+(float)(((float)((float)L_33))))), L_34, /*hidden argument*/NULL);
			__this->___m_ChildMinWidth_22 = L_35;
			GUILayoutEntry_t6_177 * L_36 = V_3;
			NullCheck(L_36);
			float L_37 = (L_36->___maxWidth_1);
			RectOffset_t6_178 * L_38 = V_5;
			NullCheck(L_38);
			int32_t L_39 = RectOffset_get_horizontal_m6_1424(L_38, /*hidden argument*/NULL);
			float L_40 = (__this->___m_ChildMaxWidth_23);
			float L_41 = Mathf_Max_m6_429(NULL /*static, unused*/, ((float)((float)L_37+(float)(((float)((float)L_39))))), L_40, /*hidden argument*/NULL);
			__this->___m_ChildMaxWidth_23 = L_41;
		}

IL_0112:
		{
			int32_t L_42 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t6_177 * L_43 = V_3;
			NullCheck(L_43);
			int32_t L_44 = (L_43->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_42+(int32_t)L_44));
		}

IL_0125:
		{
			bool L_45 = Enumerator_MoveNext_m1_14959((&V_4), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_45)
			{
				goto IL_0073;
			}
		}

IL_0131:
		{
			IL2CPP_LEAVE(0x143, FINALLY_0136);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0136;
	}

FINALLY_0136:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_46 = V_4;
		Enumerator_t1_1847  L_47 = L_46;
		Object_t * L_48 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_48);
		IL2CPP_END_FINALLY(310)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(310)
	{
		IL2CPP_JUMP_TBL(0x143, IL_0143)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0143:
	{
		float L_49 = (__this->___m_ChildMinWidth_22);
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		__this->___m_ChildMinWidth_22 = ((float)((float)L_49-(float)(((float)((float)((int32_t)((int32_t)L_50+(int32_t)L_51)))))));
		float L_52 = (__this->___m_ChildMaxWidth_23);
		int32_t L_53 = V_0;
		int32_t L_54 = V_1;
		__this->___m_ChildMaxWidth_23 = ((float)((float)L_52-(float)(((float)((float)((int32_t)((int32_t)L_53+(int32_t)L_54)))))));
		goto IL_02ea;
	}

IL_016a:
	{
		V_6 = 0;
		List_1_t1_1828 * L_55 = (__this->___entries_10);
		NullCheck(L_55);
		Enumerator_t1_1847  L_56 = List_1_GetEnumerator_m1_14957(L_55, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_8 = L_56;
	}

IL_017a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0273;
		}

IL_017f:
		{
			GUILayoutEntry_t6_177 * L_57 = Enumerator_get_Current_m1_14958((&V_8), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_7 = L_57;
			GUILayoutEntry_t6_177 * L_58 = V_7;
			NullCheck(L_58);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_58);
			GUILayoutEntry_t6_177 * L_59 = V_7;
			NullCheck(L_59);
			RectOffset_t6_178 * L_60 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
			V_9 = L_60;
			GUILayoutEntry_t6_177 * L_61 = V_7;
			NullCheck(L_61);
			GUIStyle_t6_176 * L_62 = GUILayoutEntry_get_style_m6_1305(L_61, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUIStyle_t6_176 * L_63 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_176 *)L_62) == ((Object_t*)(GUIStyle_t6_176 *)L_63)))
			{
				goto IL_0237;
			}
		}

IL_01a9:
		{
			bool L_64 = V_2;
			if (L_64)
			{
				goto IL_01d2;
			}
		}

IL_01af:
		{
			int32_t L_65 = V_6;
			RectOffset_t6_178 * L_66 = V_9;
			NullCheck(L_66);
			int32_t L_67 = RectOffset_get_left_m6_1416(L_66, /*hidden argument*/NULL);
			if ((((int32_t)L_65) <= ((int32_t)L_67)))
			{
				goto IL_01c4;
			}
		}

IL_01bd:
		{
			int32_t L_68 = V_6;
			G_B22_0 = L_68;
			goto IL_01cb;
		}

IL_01c4:
		{
			RectOffset_t6_178 * L_69 = V_9;
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_left_m6_1416(L_69, /*hidden argument*/NULL);
			G_B22_0 = L_70;
		}

IL_01cb:
		{
			V_10 = G_B22_0;
			goto IL_01d7;
		}

IL_01d2:
		{
			V_10 = 0;
			V_2 = 0;
		}

IL_01d7:
		{
			float L_71 = (__this->___m_ChildMinWidth_22);
			GUILayoutEntry_t6_177 * L_72 = V_7;
			NullCheck(L_72);
			float L_73 = (L_72->___minWidth_0);
			float L_74 = (__this->___spacing_13);
			int32_t L_75 = V_10;
			__this->___m_ChildMinWidth_22 = ((float)((float)L_71+(float)((float)((float)((float)((float)L_73+(float)L_74))+(float)(((float)((float)L_75)))))));
			float L_76 = (__this->___m_ChildMaxWidth_23);
			GUILayoutEntry_t6_177 * L_77 = V_7;
			NullCheck(L_77);
			float L_78 = (L_77->___maxWidth_1);
			float L_79 = (__this->___spacing_13);
			int32_t L_80 = V_10;
			__this->___m_ChildMaxWidth_23 = ((float)((float)L_76+(float)((float)((float)((float)((float)L_78+(float)L_79))+(float)(((float)((float)L_80)))))));
			RectOffset_t6_178 * L_81 = V_9;
			NullCheck(L_81);
			int32_t L_82 = RectOffset_get_right_m6_1418(L_81, /*hidden argument*/NULL);
			V_6 = L_82;
			int32_t L_83 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t6_177 * L_84 = V_7;
			NullCheck(L_84);
			int32_t L_85 = (L_84->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_83+(int32_t)L_85));
			goto IL_0273;
		}

IL_0237:
		{
			float L_86 = (__this->___m_ChildMinWidth_22);
			GUILayoutEntry_t6_177 * L_87 = V_7;
			NullCheck(L_87);
			float L_88 = (L_87->___minWidth_0);
			__this->___m_ChildMinWidth_22 = ((float)((float)L_86+(float)L_88));
			float L_89 = (__this->___m_ChildMaxWidth_23);
			GUILayoutEntry_t6_177 * L_90 = V_7;
			NullCheck(L_90);
			float L_91 = (L_90->___maxWidth_1);
			__this->___m_ChildMaxWidth_23 = ((float)((float)L_89+(float)L_91));
			int32_t L_92 = (__this->___m_StretchableCountX_18);
			GUILayoutEntry_t6_177 * L_93 = V_7;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchWidth_5);
			__this->___m_StretchableCountX_18 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0273:
		{
			bool L_95 = Enumerator_MoveNext_m1_14959((&V_8), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_95)
			{
				goto IL_017f;
			}
		}

IL_027f:
		{
			IL2CPP_LEAVE(0x291, FINALLY_0284);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0284;
	}

FINALLY_0284:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_96 = V_8;
		Enumerator_t1_1847  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(644)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(644)
	{
		IL2CPP_JUMP_TBL(0x291, IL_0291)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0291:
	{
		float L_99 = (__this->___m_ChildMinWidth_22);
		float L_100 = (__this->___spacing_13);
		__this->___m_ChildMinWidth_22 = ((float)((float)L_99-(float)L_100));
		float L_101 = (__this->___m_ChildMaxWidth_23);
		float L_102 = (__this->___spacing_13);
		__this->___m_ChildMaxWidth_23 = ((float)((float)L_101-(float)L_102));
		List_1_t1_1828 * L_103 = (__this->___entries_10);
		NullCheck(L_103);
		int32_t L_104 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_103);
		if (!L_104)
		{
			goto IL_02e6;
		}
	}
	{
		List_1_t1_1828 * L_105 = (__this->___entries_10);
		NullCheck(L_105);
		GUILayoutEntry_t6_177 * L_106 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_105, 0);
		NullCheck(L_106);
		RectOffset_t6_178 * L_107 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_106);
		NullCheck(L_107);
		int32_t L_108 = RectOffset_get_left_m6_1416(L_107, /*hidden argument*/NULL);
		V_0 = L_108;
		int32_t L_109 = V_6;
		V_1 = L_109;
		goto IL_02ea;
	}

IL_02e6:
	{
		int32_t L_110 = 0;
		V_1 = L_110;
		V_0 = L_110;
	}

IL_02ea:
	{
		V_11 = (0.0f);
		V_12 = (0.0f);
		GUIStyle_t6_176 * L_111 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_112 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t6_176 *)L_111) == ((Object_t*)(GUIStyle_t6_176 *)L_112))))
		{
			goto IL_0313;
		}
	}
	{
		bool L_113 = (__this->___m_UserSpecifiedWidth_20);
		if (!L_113)
		{
			goto IL_034a;
		}
	}

IL_0313:
	{
		GUIStyle_t6_176 * L_114 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		RectOffset_t6_178 * L_115 = GUIStyle_get_padding_m6_1434(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		int32_t L_116 = RectOffset_get_left_m6_1416(L_115, /*hidden argument*/NULL);
		int32_t L_117 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_118 = Mathf_Max_m6_430(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		V_11 = (((float)((float)L_118)));
		GUIStyle_t6_176 * L_119 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_119);
		RectOffset_t6_178 * L_120 = GUIStyle_get_padding_m6_1434(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = RectOffset_get_right_m6_1418(L_120, /*hidden argument*/NULL);
		int32_t L_122 = V_1;
		int32_t L_123 = Mathf_Max_m6_430(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		V_12 = (((float)((float)L_123)));
		goto IL_036c;
	}

IL_034a:
	{
		RectOffset_t6_178 * L_124 = (__this->___m_Margin_26);
		int32_t L_125 = V_0;
		NullCheck(L_124);
		RectOffset_set_left_m6_1417(L_124, L_125, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_126 = (__this->___m_Margin_26);
		int32_t L_127 = V_1;
		NullCheck(L_126);
		RectOffset_set_right_m6_1419(L_126, L_127, /*hidden argument*/NULL);
		float L_128 = (0.0f);
		V_12 = L_128;
		V_11 = L_128;
	}

IL_036c:
	{
		float L_129 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		float L_130 = (__this->___m_ChildMinWidth_22);
		float L_131 = V_11;
		float L_132 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_133 = Mathf_Max_m6_429(NULL /*static, unused*/, L_129, ((float)((float)((float)((float)L_130+(float)L_131))+(float)L_132)), /*hidden argument*/NULL);
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_133;
		float L_134 = (((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1);
		if ((!(((float)L_134) == ((float)(0.0f)))))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_135 = (((GUILayoutEntry_t6_177 *)__this)->___stretchWidth_5);
		int32_t L_136 = (__this->___m_StretchableCountX_18);
		GUIStyle_t6_176 * L_137 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		bool L_138 = GUIStyle_get_stretchWidth_m6_1470(L_137, /*hidden argument*/NULL);
		G_B38_0 = L_136;
		G_B38_1 = L_135;
		G_B38_2 = __this;
		if (!L_138)
		{
			G_B39_0 = L_136;
			G_B39_1 = L_135;
			G_B39_2 = __this;
			goto IL_03bc;
		}
	}
	{
		G_B40_0 = 1;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		G_B40_3 = G_B38_2;
		goto IL_03bd;
	}

IL_03bc:
	{
		G_B40_0 = 0;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
		G_B40_3 = G_B39_2;
	}

IL_03bd:
	{
		NullCheck(G_B40_3);
		((GUILayoutEntry_t6_177 *)G_B40_3)->___stretchWidth_5 = ((int32_t)((int32_t)G_B40_2+(int32_t)((int32_t)((int32_t)G_B40_1+(int32_t)G_B40_0))));
		float L_139 = (__this->___m_ChildMaxWidth_23);
		float L_140 = V_11;
		float L_141 = V_12;
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = ((float)((float)((float)((float)L_139+(float)L_140))+(float)L_141));
		goto IL_03e2;
	}

IL_03db:
	{
		((GUILayoutEntry_t6_177 *)__this)->___stretchWidth_5 = 0;
	}

IL_03e2:
	{
		float L_142 = (((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1);
		float L_143 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_144 = Mathf_Max_m6_429(NULL /*static, unused*/, L_142, L_143, /*hidden argument*/NULL);
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_144;
		GUIStyle_t6_176 * L_145 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_145);
		float L_146 = GUIStyle_get_fixedWidth_m6_1468(L_145, /*hidden argument*/NULL);
		if ((((float)L_146) == ((float)(0.0f))))
		{
			goto IL_0431;
		}
	}
	{
		GUIStyle_t6_176 * L_147 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_147);
		float L_148 = GUIStyle_get_fixedWidth_m6_1468(L_147, /*hidden argument*/NULL);
		float L_149 = L_148;
		V_13 = L_149;
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_149;
		float L_150 = V_13;
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_150;
		((GUILayoutEntry_t6_177 *)__this)->___stretchWidth_5 = 0;
	}

IL_0431:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1847_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14957_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14958_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14959_MethodInfo_var;
extern "C" void GUILayoutGroup_SetHorizontal_m6_1324 (GUILayoutGroup_t6_174 * __this, float ___x, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		Enumerator_t1_1847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1658);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		List_1_GetEnumerator_m1_14957_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		Enumerator_get_Current_m1_14958_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		Enumerator_MoveNext_m1_14959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t6_178 * V_0 = {0};
	GUILayoutEntry_t6_177 * V_1 = {0};
	Enumerator_t1_1847  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	GUILayoutEntry_t6_177 * V_8 = {0};
	Enumerator_t1_1847  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	bool V_16 = false;
	GUILayoutEntry_t6_177 * V_17 = {0};
	Enumerator_t1_1847  V_18 = {0};
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B39_0 = 0;
	{
		float L_0 = ___x;
		float L_1 = ___width;
		GUILayoutEntry_SetHorizontal_m6_1310(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___resetCoords_12);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		___x = (0.0f);
	}

IL_001a:
	{
		GUIStyle_t6_176 * L_3 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_t6_178 * L_4 = GUIStyle_get_padding_m6_1434(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = (__this->___isVertical_11);
		if (!L_5)
		{
			goto IL_01bb;
		}
	}
	{
		GUIStyle_t6_176 * L_6 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_7 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_6) == ((Object_t*)(GUIStyle_t6_176 *)L_7)))
		{
			goto IL_00eb;
		}
	}
	{
		List_1_t1_1828 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1_1847  L_9 = List_1_GetEnumerator_m1_14957(L_8, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_2 = L_9;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_0052:
		{
			GUILayoutEntry_t6_177 * L_10 = Enumerator_get_Current_m1_14958((&V_2), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_1 = L_10;
			GUILayoutEntry_t6_177 * L_11 = V_1;
			NullCheck(L_11);
			RectOffset_t6_178 * L_12 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_11);
			NullCheck(L_12);
			int32_t L_13 = RectOffset_get_left_m6_1416(L_12, /*hidden argument*/NULL);
			RectOffset_t6_178 * L_14 = V_0;
			NullCheck(L_14);
			int32_t L_15 = RectOffset_get_left_m6_1416(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_16 = Mathf_Max_m6_430(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
			V_3 = (((float)((float)L_16)));
			float L_17 = ___x;
			float L_18 = V_3;
			V_4 = ((float)((float)L_17+(float)L_18));
			float L_19 = ___width;
			GUILayoutEntry_t6_177 * L_20 = V_1;
			NullCheck(L_20);
			RectOffset_t6_178 * L_21 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_20);
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_right_m6_1418(L_21, /*hidden argument*/NULL);
			RectOffset_t6_178 * L_23 = V_0;
			NullCheck(L_23);
			int32_t L_24 = RectOffset_get_right_m6_1418(L_23, /*hidden argument*/NULL);
			int32_t L_25 = Mathf_Max_m6_430(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
			float L_26 = V_3;
			V_5 = ((float)((float)((float)((float)L_19-(float)(((float)((float)L_25)))))-(float)L_26));
			GUILayoutEntry_t6_177 * L_27 = V_1;
			NullCheck(L_27);
			int32_t L_28 = (L_27->___stretchWidth_5);
			if (!L_28)
			{
				goto IL_00ae;
			}
		}

IL_009f:
		{
			GUILayoutEntry_t6_177 * L_29 = V_1;
			float L_30 = V_4;
			float L_31 = V_5;
			NullCheck(L_29);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_29, L_30, L_31);
			goto IL_00c9;
		}

IL_00ae:
		{
			GUILayoutEntry_t6_177 * L_32 = V_1;
			float L_33 = V_4;
			float L_34 = V_5;
			GUILayoutEntry_t6_177 * L_35 = V_1;
			NullCheck(L_35);
			float L_36 = (L_35->___minWidth_0);
			GUILayoutEntry_t6_177 * L_37 = V_1;
			NullCheck(L_37);
			float L_38 = (L_37->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_39 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_34, L_36, L_38, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_32, L_33, L_39);
		}

IL_00c9:
		{
			bool L_40 = Enumerator_MoveNext_m1_14959((&V_2), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_40)
			{
				goto IL_0052;
			}
		}

IL_00d5:
		{
			IL2CPP_LEAVE(0xE6, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_41 = V_2;
		Enumerator_t1_1847  L_42 = L_41;
		Object_t * L_43 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_43);
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE6, IL_00e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		float L_44 = ___x;
		RectOffset_t6_178 * L_45 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_45);
		int32_t L_46 = RectOffset_get_left_m6_1416(L_45, /*hidden argument*/NULL);
		V_6 = ((float)((float)L_44-(float)(((float)((float)L_46)))));
		float L_47 = ___width;
		RectOffset_t6_178 * L_48 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_horizontal_m6_1424(L_48, /*hidden argument*/NULL);
		V_7 = ((float)((float)L_47+(float)(((float)((float)L_49)))));
		List_1_t1_1828 * L_50 = (__this->___entries_10);
		NullCheck(L_50);
		Enumerator_t1_1847  L_51 = List_1_GetEnumerator_m1_14957(L_50, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_9 = L_51;
	}

IL_0118:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0198;
		}

IL_011d:
		{
			GUILayoutEntry_t6_177 * L_52 = Enumerator_get_Current_m1_14958((&V_9), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_8 = L_52;
			GUILayoutEntry_t6_177 * L_53 = V_8;
			NullCheck(L_53);
			int32_t L_54 = (L_53->___stretchWidth_5);
			if (!L_54)
			{
				goto IL_015e;
			}
		}

IL_0132:
		{
			GUILayoutEntry_t6_177 * L_55 = V_8;
			float L_56 = V_6;
			GUILayoutEntry_t6_177 * L_57 = V_8;
			NullCheck(L_57);
			RectOffset_t6_178 * L_58 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
			NullCheck(L_58);
			int32_t L_59 = RectOffset_get_left_m6_1416(L_58, /*hidden argument*/NULL);
			float L_60 = V_7;
			GUILayoutEntry_t6_177 * L_61 = V_8;
			NullCheck(L_61);
			RectOffset_t6_178 * L_62 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_61);
			NullCheck(L_62);
			int32_t L_63 = RectOffset_get_horizontal_m6_1424(L_62, /*hidden argument*/NULL);
			NullCheck(L_55);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_55, ((float)((float)L_56+(float)(((float)((float)L_59))))), ((float)((float)L_60-(float)(((float)((float)L_63))))));
			goto IL_0198;
		}

IL_015e:
		{
			GUILayoutEntry_t6_177 * L_64 = V_8;
			float L_65 = V_6;
			GUILayoutEntry_t6_177 * L_66 = V_8;
			NullCheck(L_66);
			RectOffset_t6_178 * L_67 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			NullCheck(L_67);
			int32_t L_68 = RectOffset_get_left_m6_1416(L_67, /*hidden argument*/NULL);
			float L_69 = V_7;
			GUILayoutEntry_t6_177 * L_70 = V_8;
			NullCheck(L_70);
			RectOffset_t6_178 * L_71 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_70);
			NullCheck(L_71);
			int32_t L_72 = RectOffset_get_horizontal_m6_1424(L_71, /*hidden argument*/NULL);
			GUILayoutEntry_t6_177 * L_73 = V_8;
			NullCheck(L_73);
			float L_74 = (L_73->___minWidth_0);
			GUILayoutEntry_t6_177 * L_75 = V_8;
			NullCheck(L_75);
			float L_76 = (L_75->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_77 = Mathf_Clamp_m6_439(NULL /*static, unused*/, ((float)((float)L_69-(float)(((float)((float)L_72))))), L_74, L_76, /*hidden argument*/NULL);
			NullCheck(L_64);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_64, ((float)((float)L_65+(float)(((float)((float)L_68))))), L_77);
		}

IL_0198:
		{
			bool L_78 = Enumerator_MoveNext_m1_14959((&V_9), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_78)
			{
				goto IL_011d;
			}
		}

IL_01a4:
		{
			IL2CPP_LEAVE(0x1B6, FINALLY_01a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_01a9;
	}

FINALLY_01a9:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_79 = V_9;
		Enumerator_t1_1847  L_80 = L_79;
		Object_t * L_81 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_81);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_81);
		IL2CPP_END_FINALLY(425)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(425)
	{
		IL2CPP_JUMP_TBL(0x1B6, IL_01b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_01b6:
	{
		goto IL_03b0;
	}

IL_01bb:
	{
		GUIStyle_t6_176 * L_82 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_83 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_82) == ((Object_t*)(GUIStyle_t6_176 *)L_83)))
		{
			goto IL_0248;
		}
	}
	{
		RectOffset_t6_178 * L_84 = V_0;
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_left_m6_1416(L_84, /*hidden argument*/NULL);
		V_10 = (((float)((float)L_85)));
		RectOffset_t6_178 * L_86 = V_0;
		NullCheck(L_86);
		int32_t L_87 = RectOffset_get_right_m6_1418(L_86, /*hidden argument*/NULL);
		V_11 = (((float)((float)L_87)));
		List_1_t1_1828 * L_88 = (__this->___entries_10);
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_88);
		if (!L_89)
		{
			goto IL_0239;
		}
	}
	{
		float L_90 = V_10;
		List_1_t1_1828 * L_91 = (__this->___entries_10);
		NullCheck(L_91);
		GUILayoutEntry_t6_177 * L_92 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_91, 0);
		NullCheck(L_92);
		RectOffset_t6_178 * L_93 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_92);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_left_m6_1416(L_93, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_95 = Mathf_Max_m6_429(NULL /*static, unused*/, L_90, (((float)((float)L_94))), /*hidden argument*/NULL);
		V_10 = L_95;
		float L_96 = V_11;
		List_1_t1_1828 * L_97 = (__this->___entries_10);
		List_1_t1_1828 * L_98 = (__this->___entries_10);
		NullCheck(L_98);
		int32_t L_99 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_98);
		NullCheck(L_97);
		GUILayoutEntry_t6_177 * L_100 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_97, ((int32_t)((int32_t)L_99-(int32_t)1)));
		NullCheck(L_100);
		RectOffset_t6_178 * L_101 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_100);
		NullCheck(L_101);
		int32_t L_102 = RectOffset_get_right_m6_1418(L_101, /*hidden argument*/NULL);
		float L_103 = Mathf_Max_m6_429(NULL /*static, unused*/, L_96, (((float)((float)L_102))), /*hidden argument*/NULL);
		V_11 = L_103;
	}

IL_0239:
	{
		float L_104 = ___x;
		float L_105 = V_10;
		___x = ((float)((float)L_104+(float)L_105));
		float L_106 = ___width;
		float L_107 = V_11;
		float L_108 = V_10;
		___width = ((float)((float)L_106-(float)((float)((float)L_107+(float)L_108))));
	}

IL_0248:
	{
		float L_109 = ___width;
		float L_110 = (__this->___spacing_13);
		List_1_t1_1828 * L_111 = (__this->___entries_10);
		NullCheck(L_111);
		int32_t L_112 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_111);
		V_12 = ((float)((float)L_109-(float)((float)((float)L_110*(float)(((float)((float)((int32_t)((int32_t)L_112-(int32_t)1)))))))));
		V_13 = (0.0f);
		float L_113 = (__this->___m_ChildMinWidth_22);
		float L_114 = (__this->___m_ChildMaxWidth_23);
		if ((((float)L_113) == ((float)L_114)))
		{
			goto IL_02a1;
		}
	}
	{
		float L_115 = V_12;
		float L_116 = (__this->___m_ChildMinWidth_22);
		float L_117 = (__this->___m_ChildMaxWidth_23);
		float L_118 = (__this->___m_ChildMinWidth_22);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_119 = Mathf_Clamp_m6_439(NULL /*static, unused*/, ((float)((float)((float)((float)L_115-(float)L_116))/(float)((float)((float)L_117-(float)L_118)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_13 = L_119;
	}

IL_02a1:
	{
		V_14 = (0.0f);
		float L_120 = V_12;
		float L_121 = (__this->___m_ChildMaxWidth_23);
		if ((!(((float)L_120) > ((float)L_121))))
		{
			goto IL_02d4;
		}
	}
	{
		int32_t L_122 = (__this->___m_StretchableCountX_18);
		if ((((int32_t)L_122) <= ((int32_t)0)))
		{
			goto IL_02d4;
		}
	}
	{
		float L_123 = V_12;
		float L_124 = (__this->___m_ChildMaxWidth_23);
		int32_t L_125 = (__this->___m_StretchableCountX_18);
		V_14 = ((float)((float)((float)((float)L_123-(float)L_124))/(float)(((float)((float)L_125)))));
	}

IL_02d4:
	{
		V_15 = 0;
		V_16 = 1;
		List_1_t1_1828 * L_126 = (__this->___entries_10);
		NullCheck(L_126);
		Enumerator_t1_1847  L_127 = List_1_GetEnumerator_m1_14957(L_126, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_18 = L_127;
	}

IL_02e7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0392;
		}

IL_02ec:
		{
			GUILayoutEntry_t6_177 * L_128 = Enumerator_get_Current_m1_14958((&V_18), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_17 = L_128;
			GUILayoutEntry_t6_177 * L_129 = V_17;
			NullCheck(L_129);
			float L_130 = (L_129->___minWidth_0);
			GUILayoutEntry_t6_177 * L_131 = V_17;
			NullCheck(L_131);
			float L_132 = (L_131->___maxWidth_1);
			float L_133 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_134 = Mathf_Lerp_m6_442(NULL /*static, unused*/, L_130, L_132, L_133, /*hidden argument*/NULL);
			V_19 = L_134;
			float L_135 = V_19;
			float L_136 = V_14;
			GUILayoutEntry_t6_177 * L_137 = V_17;
			NullCheck(L_137);
			int32_t L_138 = (L_137->___stretchWidth_5);
			V_19 = ((float)((float)L_135+(float)((float)((float)L_136*(float)(((float)((float)L_138)))))));
			GUILayoutEntry_t6_177 * L_139 = V_17;
			NullCheck(L_139);
			GUIStyle_t6_176 * L_140 = GUILayoutEntry_get_style_m6_1305(L_139, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUIStyle_t6_176 * L_141 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_176 *)L_140) == ((Object_t*)(GUIStyle_t6_176 *)L_141)))
			{
				goto IL_0371;
			}
		}

IL_032d:
		{
			GUILayoutEntry_t6_177 * L_142 = V_17;
			NullCheck(L_142);
			RectOffset_t6_178 * L_143 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_142);
			NullCheck(L_143);
			int32_t L_144 = RectOffset_get_left_m6_1416(L_143, /*hidden argument*/NULL);
			V_20 = L_144;
			bool L_145 = V_16;
			if (!L_145)
			{
				goto IL_0348;
			}
		}

IL_0342:
		{
			V_20 = 0;
			V_16 = 0;
		}

IL_0348:
		{
			int32_t L_146 = V_15;
			int32_t L_147 = V_20;
			if ((((int32_t)L_146) <= ((int32_t)L_147)))
			{
				goto IL_0358;
			}
		}

IL_0351:
		{
			int32_t L_148 = V_15;
			G_B39_0 = L_148;
			goto IL_035a;
		}

IL_0358:
		{
			int32_t L_149 = V_20;
			G_B39_0 = L_149;
		}

IL_035a:
		{
			V_21 = G_B39_0;
			float L_150 = ___x;
			int32_t L_151 = V_21;
			___x = ((float)((float)L_150+(float)(((float)((float)L_151)))));
			GUILayoutEntry_t6_177 * L_152 = V_17;
			NullCheck(L_152);
			RectOffset_t6_178 * L_153 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_152);
			NullCheck(L_153);
			int32_t L_154 = RectOffset_get_right_m6_1418(L_153, /*hidden argument*/NULL);
			V_15 = L_154;
		}

IL_0371:
		{
			GUILayoutEntry_t6_177 * L_155 = V_17;
			float L_156 = ___x;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_157 = bankers_roundf(L_156);
			float L_158 = V_19;
			float L_159 = bankers_roundf(L_158);
			NullCheck(L_155);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_155, L_157, L_159);
			float L_160 = ___x;
			float L_161 = V_19;
			float L_162 = (__this->___spacing_13);
			___x = ((float)((float)L_160+(float)((float)((float)L_161+(float)L_162))));
		}

IL_0392:
		{
			bool L_163 = Enumerator_MoveNext_m1_14959((&V_18), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_163)
			{
				goto IL_02ec;
			}
		}

IL_039e:
		{
			IL2CPP_LEAVE(0x3B0, FINALLY_03a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_03a3;
	}

FINALLY_03a3:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_164 = V_18;
		Enumerator_t1_1847  L_165 = L_164;
		Object_t * L_166 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_165);
		NullCheck(L_166);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_166);
		IL2CPP_END_FINALLY(931)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(931)
	{
		IL2CPP_JUMP_TBL(0x3B0, IL_03b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_03b0:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1847_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14957_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14958_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14959_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcHeight_m6_1325 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		Enumerator_t1_1847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1658);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		List_1_GetEnumerator_m1_14957_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		Enumerator_get_Current_m1_14958_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		Enumerator_MoveNext_m1_14959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	GUILayoutEntry_t6_177 * V_4 = {0};
	Enumerator_t1_1847  V_5 = {0};
	RectOffset_t6_178 * V_6 = {0};
	int32_t V_7 = 0;
	bool V_8 = false;
	GUILayoutEntry_t6_177 * V_9 = {0};
	Enumerator_t1_1847  V_10 = {0};
	RectOffset_t6_178 * V_11 = {0};
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	GUILayoutGroup_t6_174 * G_B36_2 = {0};
	int32_t G_B35_0 = 0;
	int32_t G_B35_1 = 0;
	GUILayoutGroup_t6_174 * G_B35_2 = {0};
	int32_t G_B37_0 = 0;
	int32_t G_B37_1 = 0;
	int32_t G_B37_2 = 0;
	GUILayoutGroup_t6_174 * G_B37_3 = {0};
	{
		List_1_t1_1828 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t6_176 * L_2 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t6_178 * L_3 = GUIStyle_get_padding_m6_1434(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m6_1425(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)((float)L_4)));
		V_14 = L_5;
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_5;
		float L_6 = V_14;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_6;
		return;
	}

IL_0033:
	{
		V_0 = 0;
		V_1 = 0;
		__this->___m_ChildMinHeight_24 = (0.0f);
		__this->___m_ChildMaxHeight_25 = (0.0f);
		__this->___m_StretchableCountY_19 = 0;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_01d4;
		}
	}
	{
		V_2 = 0;
		V_3 = 1;
		List_1_t1_1828 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1_1847  L_9 = List_1_GetEnumerator_m1_14957(L_8, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_5 = L_9;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0159;
		}

IL_0075:
		{
			GUILayoutEntry_t6_177 * L_10 = Enumerator_get_Current_m1_14958((&V_5), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_4 = L_10;
			GUILayoutEntry_t6_177 * L_11 = V_4;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_11);
			GUILayoutEntry_t6_177 * L_12 = V_4;
			NullCheck(L_12);
			RectOffset_t6_178 * L_13 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_6 = L_13;
			GUILayoutEntry_t6_177 * L_14 = V_4;
			NullCheck(L_14);
			GUIStyle_t6_176 * L_15 = GUILayoutEntry_get_style_m6_1305(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUIStyle_t6_176 * L_16 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_176 *)L_15) == ((Object_t*)(GUIStyle_t6_176 *)L_16)))
			{
				goto IL_011d;
			}
		}

IL_009f:
		{
			bool L_17 = V_3;
			if (L_17)
			{
				goto IL_00b9;
			}
		}

IL_00a5:
		{
			int32_t L_18 = V_2;
			RectOffset_t6_178 * L_19 = V_6;
			NullCheck(L_19);
			int32_t L_20 = RectOffset_get_top_m6_1420(L_19, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Max_m6_430(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
			V_7 = L_21;
			goto IL_00be;
		}

IL_00b9:
		{
			V_7 = 0;
			V_3 = 0;
		}

IL_00be:
		{
			float L_22 = (__this->___m_ChildMinHeight_24);
			GUILayoutEntry_t6_177 * L_23 = V_4;
			NullCheck(L_23);
			float L_24 = (L_23->___minHeight_2);
			float L_25 = (__this->___spacing_13);
			int32_t L_26 = V_7;
			__this->___m_ChildMinHeight_24 = ((float)((float)L_22+(float)((float)((float)((float)((float)L_24+(float)L_25))+(float)(((float)((float)L_26)))))));
			float L_27 = (__this->___m_ChildMaxHeight_25);
			GUILayoutEntry_t6_177 * L_28 = V_4;
			NullCheck(L_28);
			float L_29 = (L_28->___maxHeight_3);
			float L_30 = (__this->___spacing_13);
			int32_t L_31 = V_7;
			__this->___m_ChildMaxHeight_25 = ((float)((float)L_27+(float)((float)((float)((float)((float)L_29+(float)L_30))+(float)(((float)((float)L_31)))))));
			RectOffset_t6_178 * L_32 = V_6;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_bottom_m6_1422(L_32, /*hidden argument*/NULL);
			V_2 = L_33;
			int32_t L_34 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t6_177 * L_35 = V_4;
			NullCheck(L_35);
			int32_t L_36 = (L_35->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_34+(int32_t)L_36));
			goto IL_0159;
		}

IL_011d:
		{
			float L_37 = (__this->___m_ChildMinHeight_24);
			GUILayoutEntry_t6_177 * L_38 = V_4;
			NullCheck(L_38);
			float L_39 = (L_38->___minHeight_2);
			__this->___m_ChildMinHeight_24 = ((float)((float)L_37+(float)L_39));
			float L_40 = (__this->___m_ChildMaxHeight_25);
			GUILayoutEntry_t6_177 * L_41 = V_4;
			NullCheck(L_41);
			float L_42 = (L_41->___maxHeight_3);
			__this->___m_ChildMaxHeight_25 = ((float)((float)L_40+(float)L_42));
			int32_t L_43 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t6_177 * L_44 = V_4;
			NullCheck(L_44);
			int32_t L_45 = (L_44->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_43+(int32_t)L_45));
		}

IL_0159:
		{
			bool L_46 = Enumerator_MoveNext_m1_14959((&V_5), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_46)
			{
				goto IL_0075;
			}
		}

IL_0165:
		{
			IL2CPP_LEAVE(0x177, FINALLY_016a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_016a;
	}

FINALLY_016a:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_47 = V_5;
		Enumerator_t1_1847  L_48 = L_47;
		Object_t * L_49 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_49);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_49);
		IL2CPP_END_FINALLY(362)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(362)
	{
		IL2CPP_JUMP_TBL(0x177, IL_0177)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0177:
	{
		float L_50 = (__this->___m_ChildMinHeight_24);
		float L_51 = (__this->___spacing_13);
		__this->___m_ChildMinHeight_24 = ((float)((float)L_50-(float)L_51));
		float L_52 = (__this->___m_ChildMaxHeight_25);
		float L_53 = (__this->___spacing_13);
		__this->___m_ChildMaxHeight_25 = ((float)((float)L_52-(float)L_53));
		List_1_t1_1828 * L_54 = (__this->___entries_10);
		NullCheck(L_54);
		int32_t L_55 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_54);
		if (!L_55)
		{
			goto IL_01cb;
		}
	}
	{
		List_1_t1_1828 * L_56 = (__this->___entries_10);
		NullCheck(L_56);
		GUILayoutEntry_t6_177 * L_57 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_56, 0);
		NullCheck(L_57);
		RectOffset_t6_178 * L_58 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
		NullCheck(L_58);
		int32_t L_59 = RectOffset_get_top_m6_1420(L_58, /*hidden argument*/NULL);
		V_0 = L_59;
		int32_t L_60 = V_2;
		V_1 = L_60;
		goto IL_01cf;
	}

IL_01cb:
	{
		int32_t L_61 = 0;
		V_0 = L_61;
		V_1 = L_61;
	}

IL_01cf:
	{
		goto IL_02b0;
	}

IL_01d4:
	{
		V_8 = 1;
		List_1_t1_1828 * L_62 = (__this->___entries_10);
		NullCheck(L_62);
		Enumerator_t1_1847  L_63 = List_1_GetEnumerator_m1_14957(L_62, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_10 = L_63;
	}

IL_01e4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0292;
		}

IL_01e9:
		{
			GUILayoutEntry_t6_177 * L_64 = Enumerator_get_Current_m1_14958((&V_10), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_9 = L_64;
			GUILayoutEntry_t6_177 * L_65 = V_9;
			NullCheck(L_65);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_65);
			GUILayoutEntry_t6_177 * L_66 = V_9;
			NullCheck(L_66);
			RectOffset_t6_178 * L_67 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			V_11 = L_67;
			GUILayoutEntry_t6_177 * L_68 = V_9;
			NullCheck(L_68);
			GUIStyle_t6_176 * L_69 = GUILayoutEntry_get_style_m6_1305(L_68, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUIStyle_t6_176 * L_70 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_176 *)L_69) == ((Object_t*)(GUIStyle_t6_176 *)L_70)))
			{
				goto IL_027e;
			}
		}

IL_0213:
		{
			bool L_71 = V_8;
			if (L_71)
			{
				goto IL_023b;
			}
		}

IL_021a:
		{
			RectOffset_t6_178 * L_72 = V_11;
			NullCheck(L_72);
			int32_t L_73 = RectOffset_get_top_m6_1420(L_72, /*hidden argument*/NULL);
			int32_t L_74 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_75 = Mathf_Min_m6_428(NULL /*static, unused*/, L_73, L_74, /*hidden argument*/NULL);
			V_0 = L_75;
			RectOffset_t6_178 * L_76 = V_11;
			NullCheck(L_76);
			int32_t L_77 = RectOffset_get_bottom_m6_1422(L_76, /*hidden argument*/NULL);
			int32_t L_78 = V_1;
			int32_t L_79 = Mathf_Min_m6_428(NULL /*static, unused*/, L_77, L_78, /*hidden argument*/NULL);
			V_1 = L_79;
			goto IL_024e;
		}

IL_023b:
		{
			RectOffset_t6_178 * L_80 = V_11;
			NullCheck(L_80);
			int32_t L_81 = RectOffset_get_top_m6_1420(L_80, /*hidden argument*/NULL);
			V_0 = L_81;
			RectOffset_t6_178 * L_82 = V_11;
			NullCheck(L_82);
			int32_t L_83 = RectOffset_get_bottom_m6_1422(L_82, /*hidden argument*/NULL);
			V_1 = L_83;
			V_8 = 0;
		}

IL_024e:
		{
			GUILayoutEntry_t6_177 * L_84 = V_9;
			NullCheck(L_84);
			float L_85 = (L_84->___minHeight_2);
			float L_86 = (__this->___m_ChildMinHeight_24);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_87 = Mathf_Max_m6_429(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
			__this->___m_ChildMinHeight_24 = L_87;
			GUILayoutEntry_t6_177 * L_88 = V_9;
			NullCheck(L_88);
			float L_89 = (L_88->___maxHeight_3);
			float L_90 = (__this->___m_ChildMaxHeight_25);
			float L_91 = Mathf_Max_m6_429(NULL /*static, unused*/, L_89, L_90, /*hidden argument*/NULL);
			__this->___m_ChildMaxHeight_25 = L_91;
		}

IL_027e:
		{
			int32_t L_92 = (__this->___m_StretchableCountY_19);
			GUILayoutEntry_t6_177 * L_93 = V_9;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchHeight_6);
			__this->___m_StretchableCountY_19 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0292:
		{
			bool L_95 = Enumerator_MoveNext_m1_14959((&V_10), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_95)
			{
				goto IL_01e9;
			}
		}

IL_029e:
		{
			IL2CPP_LEAVE(0x2B0, FINALLY_02a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_02a3;
	}

FINALLY_02a3:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_96 = V_10;
		Enumerator_t1_1847  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(675)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(675)
	{
		IL2CPP_JUMP_TBL(0x2B0, IL_02b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_02b0:
	{
		V_12 = (0.0f);
		V_13 = (0.0f);
		GUIStyle_t6_176 * L_99 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_100 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t6_176 *)L_99) == ((Object_t*)(GUIStyle_t6_176 *)L_100))))
		{
			goto IL_02d9;
		}
	}
	{
		bool L_101 = (__this->___m_UserSpecifiedHeight_21);
		if (!L_101)
		{
			goto IL_0310;
		}
	}

IL_02d9:
	{
		GUIStyle_t6_176 * L_102 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_102);
		RectOffset_t6_178 * L_103 = GUIStyle_get_padding_m6_1434(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		int32_t L_104 = RectOffset_get_top_m6_1420(L_103, /*hidden argument*/NULL);
		int32_t L_105 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		int32_t L_106 = Mathf_Max_m6_430(NULL /*static, unused*/, L_104, L_105, /*hidden argument*/NULL);
		V_12 = (((float)((float)L_106)));
		GUIStyle_t6_176 * L_107 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_107);
		RectOffset_t6_178 * L_108 = GUIStyle_get_padding_m6_1434(L_107, /*hidden argument*/NULL);
		NullCheck(L_108);
		int32_t L_109 = RectOffset_get_bottom_m6_1422(L_108, /*hidden argument*/NULL);
		int32_t L_110 = V_1;
		int32_t L_111 = Mathf_Max_m6_430(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		V_13 = (((float)((float)L_111)));
		goto IL_0332;
	}

IL_0310:
	{
		RectOffset_t6_178 * L_112 = (__this->___m_Margin_26);
		int32_t L_113 = V_0;
		NullCheck(L_112);
		RectOffset_set_top_m6_1421(L_112, L_113, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_114 = (__this->___m_Margin_26);
		int32_t L_115 = V_1;
		NullCheck(L_114);
		RectOffset_set_bottom_m6_1423(L_114, L_115, /*hidden argument*/NULL);
		float L_116 = (0.0f);
		V_13 = L_116;
		V_12 = L_116;
	}

IL_0332:
	{
		float L_117 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		float L_118 = (__this->___m_ChildMinHeight_24);
		float L_119 = V_12;
		float L_120 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_121 = Mathf_Max_m6_429(NULL /*static, unused*/, L_117, ((float)((float)((float)((float)L_118+(float)L_119))+(float)L_120)), /*hidden argument*/NULL);
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_121;
		float L_122 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		if ((!(((float)L_122) == ((float)(0.0f)))))
		{
			goto IL_03a1;
		}
	}
	{
		int32_t L_123 = (((GUILayoutEntry_t6_177 *)__this)->___stretchHeight_6);
		int32_t L_124 = (__this->___m_StretchableCountY_19);
		GUIStyle_t6_176 * L_125 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_125);
		bool L_126 = GUIStyle_get_stretchHeight_m6_1472(L_125, /*hidden argument*/NULL);
		G_B35_0 = L_124;
		G_B35_1 = L_123;
		G_B35_2 = __this;
		if (!L_126)
		{
			G_B36_0 = L_124;
			G_B36_1 = L_123;
			G_B36_2 = __this;
			goto IL_0382;
		}
	}
	{
		G_B37_0 = 1;
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_0383;
	}

IL_0382:
	{
		G_B37_0 = 0;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_0383:
	{
		NullCheck(G_B37_3);
		((GUILayoutEntry_t6_177 *)G_B37_3)->___stretchHeight_6 = ((int32_t)((int32_t)G_B37_2+(int32_t)((int32_t)((int32_t)G_B37_1+(int32_t)G_B37_0))));
		float L_127 = (__this->___m_ChildMaxHeight_25);
		float L_128 = V_12;
		float L_129 = V_13;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = ((float)((float)((float)((float)L_127+(float)L_128))+(float)L_129));
		goto IL_03a8;
	}

IL_03a1:
	{
		((GUILayoutEntry_t6_177 *)__this)->___stretchHeight_6 = 0;
	}

IL_03a8:
	{
		float L_130 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		float L_131 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_132 = Mathf_Max_m6_429(NULL /*static, unused*/, L_130, L_131, /*hidden argument*/NULL);
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_132;
		GUIStyle_t6_176 * L_133 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_133);
		float L_134 = GUIStyle_get_fixedHeight_m6_1469(L_133, /*hidden argument*/NULL);
		if ((((float)L_134) == ((float)(0.0f))))
		{
			goto IL_03f7;
		}
	}
	{
		GUIStyle_t6_176 * L_135 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_135);
		float L_136 = GUIStyle_get_fixedHeight_m6_1469(L_135, /*hidden argument*/NULL);
		float L_137 = L_136;
		V_14 = L_137;
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_137;
		float L_138 = V_14;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_138;
		((GUILayoutEntry_t6_177 *)__this)->___stretchHeight_6 = 0;
	}

IL_03f7:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1847_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14957_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14958_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14959_MethodInfo_var;
extern "C" void GUILayoutGroup_SetVertical_m6_1326 (GUILayoutGroup_t6_174 * __this, float ___y, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		Enumerator_t1_1847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1658);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14957_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		Enumerator_get_Current_m1_14958_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		Enumerator_MoveNext_m1_14959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t6_178 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	bool V_7 = false;
	GUILayoutEntry_t6_177 * V_8 = {0};
	Enumerator_t1_1847  V_9 = {0};
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	GUILayoutEntry_t6_177 * V_13 = {0};
	Enumerator_t1_1847  V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	GUILayoutEntry_t6_177 * V_20 = {0};
	Enumerator_t1_1847  V_21 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	{
		float L_0 = ___y;
		float L_1 = ___height;
		GUILayoutEntry_SetVertical_m6_1311(__this, L_0, L_1, /*hidden argument*/NULL);
		List_1_t1_1828 * L_2 = (__this->___entries_10);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GUIStyle_t6_176 * L_4 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectOffset_t6_178 * L_5 = GUIStyle_get_padding_m6_1434(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = (__this->___resetCoords_12);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		___y = (0.0f);
	}

IL_0037:
	{
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_022f;
		}
	}
	{
		GUIStyle_t6_176 * L_8 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_9 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_8) == ((Object_t*)(GUIStyle_t6_176 *)L_9)))
		{
			goto IL_00c6;
		}
	}
	{
		RectOffset_t6_178 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m6_1420(L_10, /*hidden argument*/NULL);
		V_1 = (((float)((float)L_11)));
		RectOffset_t6_178 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_bottom_m6_1422(L_12, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_13)));
		List_1_t1_1828 * L_14 = (__this->___entries_10);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_14);
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		float L_16 = V_1;
		List_1_t1_1828 * L_17 = (__this->___entries_10);
		NullCheck(L_17);
		GUILayoutEntry_t6_177 * L_18 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_17, 0);
		NullCheck(L_18);
		RectOffset_t6_178 * L_19 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_18);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m6_1420(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Max_m6_429(NULL /*static, unused*/, L_16, (((float)((float)L_20))), /*hidden argument*/NULL);
		V_1 = L_21;
		float L_22 = V_2;
		List_1_t1_1828 * L_23 = (__this->___entries_10);
		List_1_t1_1828 * L_24 = (__this->___entries_10);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_24);
		NullCheck(L_23);
		GUILayoutEntry_t6_177 * L_26 = (GUILayoutEntry_t6_177 *)VirtFuncInvoker1< GUILayoutEntry_t6_177 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_23, ((int32_t)((int32_t)L_25-(int32_t)1)));
		NullCheck(L_26);
		RectOffset_t6_178 * L_27 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_26);
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_bottom_m6_1422(L_27, /*hidden argument*/NULL);
		float L_29 = Mathf_Max_m6_429(NULL /*static, unused*/, L_22, (((float)((float)L_28))), /*hidden argument*/NULL);
		V_2 = L_29;
	}

IL_00ba:
	{
		float L_30 = ___y;
		float L_31 = V_1;
		___y = ((float)((float)L_30+(float)L_31));
		float L_32 = ___height;
		float L_33 = V_2;
		float L_34 = V_1;
		___height = ((float)((float)L_32-(float)((float)((float)L_33+(float)L_34))));
	}

IL_00c6:
	{
		float L_35 = ___height;
		float L_36 = (__this->___spacing_13);
		List_1_t1_1828 * L_37 = (__this->___entries_10);
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_37);
		V_3 = ((float)((float)L_35-(float)((float)((float)L_36*(float)(((float)((float)((int32_t)((int32_t)L_38-(int32_t)1)))))))));
		V_4 = (0.0f);
		float L_39 = (__this->___m_ChildMinHeight_24);
		float L_40 = (__this->___m_ChildMaxHeight_25);
		if ((((float)L_39) == ((float)L_40)))
		{
			goto IL_011d;
		}
	}
	{
		float L_41 = V_3;
		float L_42 = (__this->___m_ChildMinHeight_24);
		float L_43 = (__this->___m_ChildMaxHeight_25);
		float L_44 = (__this->___m_ChildMinHeight_24);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Clamp_m6_439(NULL /*static, unused*/, ((float)((float)((float)((float)L_41-(float)L_42))/(float)((float)((float)L_43-(float)L_44)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_45;
	}

IL_011d:
	{
		V_5 = (0.0f);
		float L_46 = V_3;
		float L_47 = (__this->___m_ChildMaxHeight_25);
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_48 = (__this->___m_StretchableCountY_19);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		float L_49 = V_3;
		float L_50 = (__this->___m_ChildMaxHeight_25);
		int32_t L_51 = (__this->___m_StretchableCountY_19);
		V_5 = ((float)((float)((float)((float)L_49-(float)L_50))/(float)(((float)((float)L_51)))));
	}

IL_014e:
	{
		V_6 = 0;
		V_7 = 1;
		List_1_t1_1828 * L_52 = (__this->___entries_10);
		NullCheck(L_52);
		Enumerator_t1_1847  L_53 = List_1_GetEnumerator_m1_14957(L_52, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_9 = L_53;
	}

IL_0161:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020c;
		}

IL_0166:
		{
			GUILayoutEntry_t6_177 * L_54 = Enumerator_get_Current_m1_14958((&V_9), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_8 = L_54;
			GUILayoutEntry_t6_177 * L_55 = V_8;
			NullCheck(L_55);
			float L_56 = (L_55->___minHeight_2);
			GUILayoutEntry_t6_177 * L_57 = V_8;
			NullCheck(L_57);
			float L_58 = (L_57->___maxHeight_3);
			float L_59 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_60 = Mathf_Lerp_m6_442(NULL /*static, unused*/, L_56, L_58, L_59, /*hidden argument*/NULL);
			V_10 = L_60;
			float L_61 = V_10;
			float L_62 = V_5;
			GUILayoutEntry_t6_177 * L_63 = V_8;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___stretchHeight_6);
			V_10 = ((float)((float)L_61+(float)((float)((float)L_62*(float)(((float)((float)L_64)))))));
			GUILayoutEntry_t6_177 * L_65 = V_8;
			NullCheck(L_65);
			GUIStyle_t6_176 * L_66 = GUILayoutEntry_get_style_m6_1305(L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUIStyle_t6_176 * L_67 = GUILayoutUtility_get_spaceStyle_m6_1298(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t6_176 *)L_66) == ((Object_t*)(GUIStyle_t6_176 *)L_67)))
			{
				goto IL_01eb;
			}
		}

IL_01a7:
		{
			GUILayoutEntry_t6_177 * L_68 = V_8;
			NullCheck(L_68);
			RectOffset_t6_178 * L_69 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_top_m6_1420(L_69, /*hidden argument*/NULL);
			V_11 = L_70;
			bool L_71 = V_7;
			if (!L_71)
			{
				goto IL_01c2;
			}
		}

IL_01bc:
		{
			V_11 = 0;
			V_7 = 0;
		}

IL_01c2:
		{
			int32_t L_72 = V_6;
			int32_t L_73 = V_11;
			if ((((int32_t)L_72) <= ((int32_t)L_73)))
			{
				goto IL_01d2;
			}
		}

IL_01cb:
		{
			int32_t L_74 = V_6;
			G_B22_0 = L_74;
			goto IL_01d4;
		}

IL_01d2:
		{
			int32_t L_75 = V_11;
			G_B22_0 = L_75;
		}

IL_01d4:
		{
			V_12 = G_B22_0;
			float L_76 = ___y;
			int32_t L_77 = V_12;
			___y = ((float)((float)L_76+(float)(((float)((float)L_77)))));
			GUILayoutEntry_t6_177 * L_78 = V_8;
			NullCheck(L_78);
			RectOffset_t6_178 * L_79 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_78);
			NullCheck(L_79);
			int32_t L_80 = RectOffset_get_bottom_m6_1422(L_79, /*hidden argument*/NULL);
			V_6 = L_80;
		}

IL_01eb:
		{
			GUILayoutEntry_t6_177 * L_81 = V_8;
			float L_82 = ___y;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_83 = bankers_roundf(L_82);
			float L_84 = V_10;
			float L_85 = bankers_roundf(L_84);
			NullCheck(L_81);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_81, L_83, L_85);
			float L_86 = ___y;
			float L_87 = V_10;
			float L_88 = (__this->___spacing_13);
			___y = ((float)((float)L_86+(float)((float)((float)L_87+(float)L_88))));
		}

IL_020c:
		{
			bool L_89 = Enumerator_MoveNext_m1_14959((&V_9), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_89)
			{
				goto IL_0166;
			}
		}

IL_0218:
		{
			IL2CPP_LEAVE(0x22A, FINALLY_021d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_021d;
	}

FINALLY_021d:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_90 = V_9;
		Enumerator_t1_1847  L_91 = L_90;
		Object_t * L_92 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_92);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_92);
		IL2CPP_END_FINALLY(541)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(541)
	{
		IL2CPP_JUMP_TBL(0x22A, IL_022a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_022a:
	{
		goto IL_03c1;
	}

IL_022f:
	{
		GUIStyle_t6_176 * L_93 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_94 = GUIStyle_get_none_m6_1444(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t6_176 *)L_93) == ((Object_t*)(GUIStyle_t6_176 *)L_94)))
		{
			goto IL_02f6;
		}
	}
	{
		List_1_t1_1828 * L_95 = (__this->___entries_10);
		NullCheck(L_95);
		Enumerator_t1_1847  L_96 = List_1_GetEnumerator_m1_14957(L_95, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_14 = L_96;
	}

IL_024c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02d3;
		}

IL_0251:
		{
			GUILayoutEntry_t6_177 * L_97 = Enumerator_get_Current_m1_14958((&V_14), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_13 = L_97;
			GUILayoutEntry_t6_177 * L_98 = V_13;
			NullCheck(L_98);
			RectOffset_t6_178 * L_99 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_98);
			NullCheck(L_99);
			int32_t L_100 = RectOffset_get_top_m6_1420(L_99, /*hidden argument*/NULL);
			RectOffset_t6_178 * L_101 = V_0;
			NullCheck(L_101);
			int32_t L_102 = RectOffset_get_top_m6_1420(L_101, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			int32_t L_103 = Mathf_Max_m6_430(NULL /*static, unused*/, L_100, L_102, /*hidden argument*/NULL);
			V_15 = (((float)((float)L_103)));
			float L_104 = ___y;
			float L_105 = V_15;
			V_16 = ((float)((float)L_104+(float)L_105));
			float L_106 = ___height;
			GUILayoutEntry_t6_177 * L_107 = V_13;
			NullCheck(L_107);
			RectOffset_t6_178 * L_108 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_107);
			NullCheck(L_108);
			int32_t L_109 = RectOffset_get_bottom_m6_1422(L_108, /*hidden argument*/NULL);
			RectOffset_t6_178 * L_110 = V_0;
			NullCheck(L_110);
			int32_t L_111 = RectOffset_get_bottom_m6_1422(L_110, /*hidden argument*/NULL);
			int32_t L_112 = Mathf_Max_m6_430(NULL /*static, unused*/, L_109, L_111, /*hidden argument*/NULL);
			float L_113 = V_15;
			V_17 = ((float)((float)((float)((float)L_106-(float)(((float)((float)L_112)))))-(float)L_113));
			GUILayoutEntry_t6_177 * L_114 = V_13;
			NullCheck(L_114);
			int32_t L_115 = (L_114->___stretchHeight_6);
			if (!L_115)
			{
				goto IL_02b5;
			}
		}

IL_02a5:
		{
			GUILayoutEntry_t6_177 * L_116 = V_13;
			float L_117 = V_16;
			float L_118 = V_17;
			NullCheck(L_116);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_116, L_117, L_118);
			goto IL_02d3;
		}

IL_02b5:
		{
			GUILayoutEntry_t6_177 * L_119 = V_13;
			float L_120 = V_16;
			float L_121 = V_17;
			GUILayoutEntry_t6_177 * L_122 = V_13;
			NullCheck(L_122);
			float L_123 = (L_122->___minHeight_2);
			GUILayoutEntry_t6_177 * L_124 = V_13;
			NullCheck(L_124);
			float L_125 = (L_124->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_126 = Mathf_Clamp_m6_439(NULL /*static, unused*/, L_121, L_123, L_125, /*hidden argument*/NULL);
			NullCheck(L_119);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_119, L_120, L_126);
		}

IL_02d3:
		{
			bool L_127 = Enumerator_MoveNext_m1_14959((&V_14), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_127)
			{
				goto IL_0251;
			}
		}

IL_02df:
		{
			IL2CPP_LEAVE(0x2F1, FINALLY_02e4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_02e4;
	}

FINALLY_02e4:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_128 = V_14;
		Enumerator_t1_1847  L_129 = L_128;
		Object_t * L_130 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_130);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_130);
		IL2CPP_END_FINALLY(740)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(740)
	{
		IL2CPP_JUMP_TBL(0x2F1, IL_02f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_02f1:
	{
		goto IL_03c1;
	}

IL_02f6:
	{
		float L_131 = ___y;
		RectOffset_t6_178 * L_132 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_132);
		int32_t L_133 = RectOffset_get_top_m6_1420(L_132, /*hidden argument*/NULL);
		V_18 = ((float)((float)L_131-(float)(((float)((float)L_133)))));
		float L_134 = ___height;
		RectOffset_t6_178 * L_135 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_135);
		int32_t L_136 = RectOffset_get_vertical_m6_1425(L_135, /*hidden argument*/NULL);
		V_19 = ((float)((float)L_134+(float)(((float)((float)L_136)))));
		List_1_t1_1828 * L_137 = (__this->___entries_10);
		NullCheck(L_137);
		Enumerator_t1_1847  L_138 = List_1_GetEnumerator_m1_14957(L_137, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_21 = L_138;
	}

IL_0323:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a3;
		}

IL_0328:
		{
			GUILayoutEntry_t6_177 * L_139 = Enumerator_get_Current_m1_14958((&V_21), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_20 = L_139;
			GUILayoutEntry_t6_177 * L_140 = V_20;
			NullCheck(L_140);
			int32_t L_141 = (L_140->___stretchHeight_6);
			if (!L_141)
			{
				goto IL_0369;
			}
		}

IL_033d:
		{
			GUILayoutEntry_t6_177 * L_142 = V_20;
			float L_143 = V_18;
			GUILayoutEntry_t6_177 * L_144 = V_20;
			NullCheck(L_144);
			RectOffset_t6_178 * L_145 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_144);
			NullCheck(L_145);
			int32_t L_146 = RectOffset_get_top_m6_1420(L_145, /*hidden argument*/NULL);
			float L_147 = V_19;
			GUILayoutEntry_t6_177 * L_148 = V_20;
			NullCheck(L_148);
			RectOffset_t6_178 * L_149 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_148);
			NullCheck(L_149);
			int32_t L_150 = RectOffset_get_vertical_m6_1425(L_149, /*hidden argument*/NULL);
			NullCheck(L_142);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_142, ((float)((float)L_143+(float)(((float)((float)L_146))))), ((float)((float)L_147-(float)(((float)((float)L_150))))));
			goto IL_03a3;
		}

IL_0369:
		{
			GUILayoutEntry_t6_177 * L_151 = V_20;
			float L_152 = V_18;
			GUILayoutEntry_t6_177 * L_153 = V_20;
			NullCheck(L_153);
			RectOffset_t6_178 * L_154 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_153);
			NullCheck(L_154);
			int32_t L_155 = RectOffset_get_top_m6_1420(L_154, /*hidden argument*/NULL);
			float L_156 = V_19;
			GUILayoutEntry_t6_177 * L_157 = V_20;
			NullCheck(L_157);
			RectOffset_t6_178 * L_158 = (RectOffset_t6_178 *)VirtFuncInvoker0< RectOffset_t6_178 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_157);
			NullCheck(L_158);
			int32_t L_159 = RectOffset_get_vertical_m6_1425(L_158, /*hidden argument*/NULL);
			GUILayoutEntry_t6_177 * L_160 = V_20;
			NullCheck(L_160);
			float L_161 = (L_160->___minHeight_2);
			GUILayoutEntry_t6_177 * L_162 = V_20;
			NullCheck(L_162);
			float L_163 = (L_162->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
			float L_164 = Mathf_Clamp_m6_439(NULL /*static, unused*/, ((float)((float)L_156-(float)(((float)((float)L_159))))), L_161, L_163, /*hidden argument*/NULL);
			NullCheck(L_151);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_151, ((float)((float)L_152+(float)(((float)((float)L_155))))), L_164);
		}

IL_03a3:
		{
			bool L_165 = Enumerator_MoveNext_m1_14959((&V_21), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_165)
			{
				goto IL_0328;
			}
		}

IL_03af:
		{
			IL2CPP_LEAVE(0x3C1, FINALLY_03b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_03b4;
	}

FINALLY_03b4:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_166 = V_21;
		Enumerator_t1_1847  L_167 = L_166;
		Object_t * L_168 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_167);
		NullCheck(L_168);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_168);
		IL2CPP_END_FINALLY(948)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(948)
	{
		IL2CPP_JUMP_TBL(0x3C1, IL_03c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_03c1:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutGroup::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t1_17_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1_1847_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1_14957_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1_14958_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1_14959_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral268;
extern Il2CppCodeGenString* _stringLiteral4826;
extern Il2CppCodeGenString* _stringLiteral4827;
extern Il2CppCodeGenString* _stringLiteral4828;
extern Il2CppCodeGenString* _stringLiteral4447;
extern "C" String_t* GUILayoutGroup_ToString_m6_1327 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t1_17_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		Enumerator_t1_1847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1658);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		List_1_GetEnumerator_m1_14957_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483798);
		Enumerator_get_Current_m1_14958_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483799);
		Enumerator_MoveNext_m1_14959_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483800);
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		_stringLiteral4826 = il2cpp_codegen_string_literal_from_index(4826);
		_stringLiteral4827 = il2cpp_codegen_string_literal_from_index(4827);
		_stringLiteral4828 = il2cpp_codegen_string_literal_from_index(4828);
		_stringLiteral4447 = il2cpp_codegen_string_literal_from_index(4447);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	GUILayoutEntry_t6_177 * V_3 = {0};
	Enumerator_t1_1847  V_4 = {0};
	String_t* V_5 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0023;
	}

IL_0013:
	{
		String_t* L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1_559(NULL /*static, unused*/, L_2, _stringLiteral268, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		int32_t L_6 = ((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_7 = V_0;
		V_5 = L_7;
		ObjectU5BU5D_t1_272* L_8 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		String_t* L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_8;
		String_t* L_11 = GUILayoutEntry_ToString_m6_1314(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 1, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral4826);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4826;
		ObjectU5BU5D_t1_272* L_13 = L_12;
		float L_14 = (__this->___m_ChildMinHeight_24);
		float L_15 = L_14;
		Object_t * L_16 = Box(Single_t1_17_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t1_272* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral4827);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4827;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1_562(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		int32_t L_19 = ((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_19+(int32_t)4));
		List_1_t1_1828 * L_20 = (__this->___entries_10);
		NullCheck(L_20);
		Enumerator_t1_1847  L_21 = List_1_GetEnumerator_m1_14957(L_20, /*hidden argument*/List_1_GetEnumerator_m1_14957_MethodInfo_var);
		V_4 = L_21;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a1;
		}

IL_0087:
		{
			GUILayoutEntry_t6_177 * L_22 = Enumerator_get_Current_m1_14958((&V_4), /*hidden argument*/Enumerator_get_Current_m1_14958_MethodInfo_var);
			V_3 = L_22;
			String_t* L_23 = V_0;
			GUILayoutEntry_t6_177 * L_24 = V_3;
			NullCheck(L_24);
			String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.GUILayoutEntry::ToString() */, L_24);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m1_560(NULL /*static, unused*/, L_23, L_25, _stringLiteral4828, /*hidden argument*/NULL);
			V_0 = L_26;
		}

IL_00a1:
		{
			bool L_27 = Enumerator_MoveNext_m1_14959((&V_4), /*hidden argument*/Enumerator_MoveNext_m1_14959_MethodInfo_var);
			if (L_27)
			{
				goto IL_0087;
			}
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Enumerator_t1_1847  L_28 = V_4;
		Enumerator_t1_1847  L_29 = L_28;
		Object_t * L_30 = Box(Enumerator_t1_1847_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_30);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, L_30);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_00bf:
	{
		String_t* L_31 = V_0;
		String_t* L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m1_560(NULL /*static, unused*/, L_31, L_32, _stringLiteral4447, /*hidden argument*/NULL);
		V_0 = L_33;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		int32_t L_34 = ((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t6_177_StaticFields*)GUILayoutEntry_t6_177_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_34-(int32_t)4));
		String_t* L_35 = V_0;
		return L_35;
	}
}
// System.Void UnityEngine.GUIScrollGroup::.ctor()
extern "C" void GUIScrollGroup__ctor_m6_1328 (GUIScrollGroup_t6_179 * __this, const MethodInfo* method)
{
	{
		__this->___allowHorizontalScroll_33 = 1;
		__this->___allowVerticalScroll_34 = 1;
		GUILayoutGroup__ctor_m6_1315(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcWidth()
extern "C" void GUIScrollGroup_CalcWidth_m6_1329 (GUIScrollGroup_t6_179 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1);
		V_1 = L_1;
		bool L_2 = (__this->___allowHorizontalScroll_33);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = (0.0f);
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcWidth_m6_1323(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		__this->___calcMinWidth_27 = L_3;
		float L_4 = (((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1);
		__this->___calcMaxWidth_28 = L_4;
		bool L_5 = (__this->___allowHorizontalScroll_33);
		if (!L_5)
		{
			goto IL_009e;
		}
	}
	{
		float L_6 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		if ((!(((float)L_6) > ((float)(32.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = (32.0f);
	}

IL_0073:
	{
		float L_7 = V_0;
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_0085;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_8;
	}

IL_0085:
	{
		float L_9 = V_1;
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_009e;
		}
	}
	{
		float L_10 = V_1;
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_10;
		((GUILayoutEntry_t6_177 *)__this)->___stretchWidth_5 = 0;
	}

IL_009e:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetHorizontal(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetHorizontal_m6_1330 (GUIScrollGroup_t6_179 * __this, float ___x, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (__this->___needsVerticalScrollbar_36);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		float L_1 = ___width;
		GUIStyle_t6_176 * L_2 = (__this->___verticalScrollbar_38);
		NullCheck(L_2);
		float L_3 = GUIStyle_get_fixedWidth_m6_1468(L_2, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_4 = (__this->___verticalScrollbar_38);
		NullCheck(L_4);
		RectOffset_t6_178 * L_5 = GUIStyle_get_margin_m6_1433(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m6_1416(L_5, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_3))-(float)(((float)((float)L_6)))));
		goto IL_0030;
	}

IL_002f:
	{
		float L_7 = ___width;
		G_B3_0 = L_7;
	}

IL_0030:
	{
		V_0 = G_B3_0;
		bool L_8 = (__this->___allowHorizontalScroll_33);
		if (!L_8)
		{
			goto IL_0091;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinWidth_27);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0091;
		}
	}
	{
		__this->___needsHorizontalScrollbar_35 = 1;
		float L_11 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_11;
		float L_12 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_12;
		float L_13 = ___x;
		float L_14 = (__this->___calcMinWidth_27);
		GUILayoutGroup_SetHorizontal_m6_1324(__this, L_13, L_14, /*hidden argument*/NULL);
		Rect_t6_51 * L_15 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_16 = ___width;
		Rect_set_width_m6_307(L_15, L_16, /*hidden argument*/NULL);
		float L_17 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_17;
		goto IL_00d6;
	}

IL_0091:
	{
		__this->___needsHorizontalScrollbar_35 = 0;
		bool L_18 = (__this->___allowHorizontalScroll_33);
		if (!L_18)
		{
			goto IL_00bb;
		}
	}
	{
		float L_19 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_19;
		float L_20 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_20;
	}

IL_00bb:
	{
		float L_21 = ___x;
		float L_22 = V_0;
		GUILayoutGroup_SetHorizontal_m6_1324(__this, L_21, L_22, /*hidden argument*/NULL);
		Rect_t6_51 * L_23 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_24 = ___width;
		Rect_set_width_m6_307(L_23, L_24, /*hidden argument*/NULL);
		float L_25 = V_0;
		__this->___clientWidth_31 = L_25;
	}

IL_00d6:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcHeight()
extern "C" void GUIScrollGroup_CalcHeight_m6_1331 (GUIScrollGroup_t6_179 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		V_1 = L_1;
		bool L_2 = (__this->___allowVerticalScroll_34);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = (0.0f);
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcHeight_m6_1325(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		__this->___calcMinHeight_29 = L_3;
		float L_4 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		__this->___calcMaxHeight_30 = L_4;
		bool L_5 = (__this->___needsHorizontalScrollbar_35);
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		GUIStyle_t6_176 * L_6 = (__this->___horizontalScrollbar_37);
		NullCheck(L_6);
		float L_7 = GUIStyle_get_fixedHeight_m6_1469(L_6, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_8 = (__this->___horizontalScrollbar_37);
		NullCheck(L_8);
		RectOffset_t6_178 * L_9 = GUIStyle_get_margin_m6_1433(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_top_m6_1420(L_9, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_7+(float)(((float)((float)L_10)))));
		float L_11 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		float L_12 = V_2;
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = ((float)((float)L_11+(float)L_12));
		float L_13 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		float L_14 = V_2;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = ((float)((float)L_13+(float)L_14));
	}

IL_0092:
	{
		bool L_15 = (__this->___allowVerticalScroll_34);
		if (!L_15)
		{
			goto IL_00e3;
		}
	}
	{
		float L_16 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		if ((!(((float)L_16) > ((float)(32.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = (32.0f);
	}

IL_00b8:
	{
		float L_17 = V_0;
		if ((((float)L_17) == ((float)(0.0f))))
		{
			goto IL_00ca;
		}
	}
	{
		float L_18 = V_0;
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_18;
	}

IL_00ca:
	{
		float L_19 = V_1;
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_1;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_20;
		((GUILayoutEntry_t6_177 *)__this)->___stretchHeight_6 = 0;
	}

IL_00e3:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetVertical(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetVertical_m6_1332 (GUIScrollGroup_t6_179 * __this, float ___y, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___height;
		V_0 = L_0;
		bool L_1 = (__this->___needsHorizontalScrollbar_35);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		float L_2 = V_0;
		GUIStyle_t6_176 * L_3 = (__this->___horizontalScrollbar_37);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedHeight_m6_1469(L_3, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_5 = (__this->___horizontalScrollbar_37);
		NullCheck(L_5);
		RectOffset_t6_178 * L_6 = GUIStyle_get_margin_m6_1433(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_top_m6_1420(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2-(float)((float)((float)L_4+(float)(((float)((float)L_7)))))));
	}

IL_002d:
	{
		bool L_8 = (__this->___allowVerticalScroll_34);
		if (!L_8)
		{
			goto IL_0139;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinHeight_29);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0139;
		}
	}
	{
		bool L_11 = (__this->___needsHorizontalScrollbar_35);
		if (L_11)
		{
			goto IL_00db;
		}
	}
	{
		bool L_12 = (__this->___needsVerticalScrollbar_36);
		if (L_12)
		{
			goto IL_00db;
		}
	}
	{
		Rect_t6_51 * L_13 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_14 = Rect_get_width_m6_306(L_13, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_15 = (__this->___verticalScrollbar_38);
		NullCheck(L_15);
		float L_16 = GUIStyle_get_fixedWidth_m6_1468(L_15, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_17 = (__this->___verticalScrollbar_38);
		NullCheck(L_17);
		RectOffset_t6_178 * L_18 = GUIStyle_get_margin_m6_1433(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = RectOffset_get_left_m6_1416(L_18, /*hidden argument*/NULL);
		__this->___clientWidth_31 = ((float)((float)((float)((float)L_14-(float)L_16))-(float)(((float)((float)L_19)))));
		float L_20 = (__this->___clientWidth_31);
		float L_21 = (__this->___calcMinWidth_27);
		if ((!(((float)L_20) < ((float)L_21))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_22 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_22;
	}

IL_00a6:
	{
		Rect_t6_51 * L_23 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_24 = Rect_get_width_m6_306(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Rect_t6_51 * L_25 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_26 = Rect_get_x_m6_298(L_25, /*hidden argument*/NULL);
		float L_27 = (__this->___clientWidth_31);
		GUIScrollGroup_SetHorizontal_m6_1330(__this, L_26, L_27, /*hidden argument*/NULL);
		GUIScrollGroup_CalcHeight_m6_1331(__this, /*hidden argument*/NULL);
		Rect_t6_51 * L_28 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_29 = V_1;
		Rect_set_width_m6_307(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00db:
	{
		float L_30 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		V_2 = L_30;
		float L_31 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		V_3 = L_31;
		float L_32 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_32;
		float L_33 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_33;
		float L_34 = ___y;
		float L_35 = (__this->___calcMinHeight_29);
		GUILayoutGroup_SetVertical_m6_1326(__this, L_34, L_35, /*hidden argument*/NULL);
		float L_36 = V_2;
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_36;
		float L_37 = V_3;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_37;
		Rect_t6_51 * L_38 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_39 = ___height;
		Rect_set_height_m6_309(L_38, L_39, /*hidden argument*/NULL);
		float L_40 = (__this->___calcMinHeight_29);
		__this->___clientHeight_32 = L_40;
		goto IL_0177;
	}

IL_0139:
	{
		bool L_41 = (__this->___allowVerticalScroll_34);
		if (!L_41)
		{
			goto IL_015c;
		}
	}
	{
		float L_42 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_42;
		float L_43 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_43;
	}

IL_015c:
	{
		float L_44 = ___y;
		float L_45 = V_0;
		GUILayoutGroup_SetVertical_m6_1326(__this, L_44, L_45, /*hidden argument*/NULL);
		Rect_t6_51 * L_46 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_47 = ___height;
		Rect_set_height_m6_309(L_46, L_47, /*hidden argument*/NULL);
		float L_48 = V_0;
		__this->___clientHeight_32 = L_48;
	}

IL_0177:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutEntry_t6_177_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern "C" void GUIWordWrapSizer__ctor_m6_1333 (GUIWordWrapSizer_t6_180 * __this, GUIStyle_t6_176 * ___style, GUIContent_t6_171 * ___content, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t6_177_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1657);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t6_176 * L_0 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t6_177_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m6_1302(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_0, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_1 = ___content;
		GUIContent_t6_171 * L_2 = (GUIContent_t6_171 *)il2cpp_codegen_object_new (GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent__ctor_m6_1241(L_2, L_1, /*hidden argument*/NULL);
		__this->___m_Content_10 = L_2;
		GUILayoutOptionU5BU5D_t6_290* L_3 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t6_290* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_3);
		float L_4 = (((GUILayoutEntry_t6_177 *)__this)->___minHeight_2);
		__this->___m_ForcedMinHeight_11 = L_4;
		float L_5 = (((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3);
		__this->___m_ForcedMaxHeight_12 = L_5;
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m6_1334 (GUIWordWrapSizer_t6_180 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}

IL_0020:
	{
		GUIStyle_t6_176 * L_2 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_3 = (__this->___m_Content_10);
		NullCheck(L_2);
		GUIStyle_CalcMinMaxWidth_m6_1450(L_2, L_3, (&V_0), (&V_1), /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t6_177 *)__this)->___minWidth_0);
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		float L_5 = V_0;
		((GUILayoutEntry_t6_177 *)__this)->___minWidth_0 = L_5;
	}

IL_004c:
	{
		float L_6 = (((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1);
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}
	{
		float L_7 = V_1;
		((GUILayoutEntry_t6_177 *)__this)->___maxWidth_1 = L_7;
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m6_1335 (GUIWordWrapSizer_t6_180 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___m_ForcedMinHeight_11);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (__this->___m_ForcedMaxHeight_12);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}

IL_0020:
	{
		GUIStyle_t6_176 * L_2 = GUILayoutEntry_get_style_m6_1305(__this, /*hidden argument*/NULL);
		GUIContent_t6_171 * L_3 = (__this->___m_Content_10);
		Rect_t6_51 * L_4 = &(((GUILayoutEntry_t6_177 *)__this)->___rect_4);
		float L_5 = Rect_get_width_m6_306(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_6 = GUIStyle_CalcHeight_m6_1448(L_2, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (__this->___m_ForcedMinHeight_11);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_8;
		goto IL_0065;
	}

IL_0059:
	{
		float L_9 = (__this->___m_ForcedMinHeight_11);
		((GUILayoutEntry_t6_177 *)__this)->___minHeight_2 = L_9;
	}

IL_0065:
	{
		float L_10 = (__this->___m_ForcedMaxHeight_12);
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = V_0;
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_11;
		goto IL_008d;
	}

IL_0081:
	{
		float L_12 = (__this->___m_ForcedMaxHeight_12);
		((GUILayoutEntry_t6_177 *)__this)->___maxHeight_3 = L_12;
	}

IL_008d:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutOption::.ctor(UnityEngine.GUILayoutOption/Type,System.Object)
extern "C" void GUILayoutOption__ctor_m6_1336 (GUILayoutOption_t6_182 * __this, int32_t ___type, Object_t * ___value, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		__this->___type_0 = L_0;
		Object_t * L_1 = ___value;
		__this->___value_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m6_1337 (GUISettings_t6_183 * __this, const MethodInfo* method)
{
	{
		__this->___m_DoubleClickSelectsWord_0 = 1;
		__this->___m_TripleClickSelectsLine_1 = 1;
		Color_t6_40  L_0 = Color_get_white_m6_276(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_CursorColor_2 = L_0;
		__this->___m_CursorFlashSpeed_3 = (-1.0f);
		Color_t6_40  L_1 = {0};
		Color__ctor_m6_270(&L_1, (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		__this->___m_SelectionColor_4 = L_1;
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.GUISettings::get_doubleClickSelectsWord()
extern "C" bool GUISettings_get_doubleClickSelectsWord_m6_1338 (GUISettings_t6_183 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_DoubleClickSelectsWord_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.GUISettings::get_tripleClickSelectsLine()
extern "C" bool GUISettings_get_tripleClickSelectsLine_m6_1339 (GUISettings_t6_183 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_TripleClickSelectsLine_1);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.GUISettings::get_cursorColor()
extern "C" Color_t6_40  GUISettings_get_cursorColor_m6_1340 (GUISettings_t6_183 * __this, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = (__this->___m_CursorColor_2);
		return L_0;
	}
}
// System.Single UnityEngine.GUISettings::get_cursorFlashSpeed()
extern "C" float GUISettings_get_cursorFlashSpeed_m6_1341 (GUISettings_t6_183 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_CursorFlashSpeed_3);
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = (__this->___m_CursorFlashSpeed_3);
		return L_1;
	}

IL_0017:
	{
		float L_2 = GUISettings_Internal_GetCursorFlashSpeed_m6_1343(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.GUISettings::get_selectionColor()
extern "C" Color_t6_40  GUISettings_get_selectionColor_m6_1342 (GUISettings_t6_183 * __this, const MethodInfo* method)
{
	{
		Color_t6_40  L_0 = (__this->___m_SelectionColor_4);
		return L_0;
	}
}
// System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
extern "C" float GUISettings_Internal_GetCursorFlashSpeed_m6_1343 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUISettings_Internal_GetCursorFlashSpeed_m6_1343_ftn) ();
	static GUISettings_Internal_GetCursorFlashSpeed_m6_1343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUISettings_Internal_GetCursorFlashSpeed_m6_1343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void SkinChangedDelegate__ctor_m6_1344 (SkinChangedDelegate_t6_184 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::Invoke()
extern "C" void SkinChangedDelegate_Invoke_m6_1345 (SkinChangedDelegate_t6_184 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SkinChangedDelegate_Invoke_m6_1345((SkinChangedDelegate_t6_184 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t6_184(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.GUISkin/SkinChangedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * SkinChangedDelegate_BeginInvoke_m6_1346 (SkinChangedDelegate_t6_184 * __this, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void SkinChangedDelegate_EndInvoke_m6_1347 (SkinChangedDelegate_t6_184 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.GUISkin::.ctor()
extern TypeInfo* GUISettings_t6_183_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyleU5BU5D_t6_185_il2cpp_TypeInfo_var;
extern "C" void GUISkin__ctor_m6_1348 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISettings_t6_183_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1662);
		GUIStyleU5BU5D_t6_185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1663);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISettings_t6_183 * L_0 = (GUISettings_t6_183 *)il2cpp_codegen_object_new (GUISettings_t6_183_il2cpp_TypeInfo_var);
		GUISettings__ctor_m6_1337(L_0, /*hidden argument*/NULL);
		__this->___m_Settings_24 = L_0;
		ScriptableObject__ctor_m6_16(__this, /*hidden argument*/NULL);
		__this->___m_CustomStyles_23 = ((GUIStyleU5BU5D_t6_185*)SZArrayNew(GUIStyleU5BU5D_t6_185_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Void UnityEngine.GUISkin::OnEnable()
extern "C" void GUISkin_OnEnable_m6_1349 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Font UnityEngine.GUISkin::get_font()
extern "C" Font_t6_149 * GUISkin_get_font_m6_1350 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		Font_t6_149 * L_0 = (__this->___m_Font_2);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_font(UnityEngine.Font)
extern TypeInfo* GUISkin_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUISkin_set_font_m6_1351 (GUISkin_t6_169 * __this, Font_t6_149 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1664);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t6_149 * L_0 = ___value;
		__this->___m_Font_2 = L_0;
		GUISkin_t6_169 * L_1 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___current_28;
		bool L_2 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Font_t6_149 * L_3 = (__this->___m_Font_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m6_1485(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
extern "C" GUIStyle_t6_176 * GUISkin_get_box_m6_1352 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_box_3);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_box(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_box_m6_1353 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_box_3 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C" GUIStyle_t6_176 * GUISkin_get_label_m6_1354 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_label_6);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_label(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_label_m6_1355 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_label_6 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textField()
extern "C" GUIStyle_t6_176 * GUISkin_get_textField_m6_1356 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_textField_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textField(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textField_m6_1357 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_textField_7 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textArea()
extern "C" GUIStyle_t6_176 * GUISkin_get_textArea_m6_1358 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_textArea_8);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textArea(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textArea_m6_1359 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_textArea_8 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C" GUIStyle_t6_176 * GUISkin_get_button_m6_1360 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_button_4);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_button(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_button_m6_1361 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_button_4 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_toggle()
extern "C" GUIStyle_t6_176 * GUISkin_get_toggle_m6_1362 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_toggle_5);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_toggle(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_toggle_m6_1363 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_toggle_5 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_window()
extern "C" GUIStyle_t6_176 * GUISkin_get_window_m6_1364 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_window_9);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_window(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_window_m6_1365 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_window_9 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSlider()
extern "C" GUIStyle_t6_176 * GUISkin_get_horizontalSlider_m6_1366 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_horizontalSlider_10);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSlider_m6_1367 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_horizontalSlider_10 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSliderThumb()
extern "C" GUIStyle_t6_176 * GUISkin_get_horizontalSliderThumb_m6_1368 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_horizontalSliderThumb_11);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSliderThumb_m6_1369 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_horizontalSliderThumb_11 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSlider()
extern "C" GUIStyle_t6_176 * GUISkin_get_verticalSlider_m6_1370 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_verticalSlider_12);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSlider_m6_1371 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_verticalSlider_12 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSliderThumb()
extern "C" GUIStyle_t6_176 * GUISkin_get_verticalSliderThumb_m6_1372 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_verticalSliderThumb_13);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSliderThumb_m6_1373 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_verticalSliderThumb_13 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbar()
extern "C" GUIStyle_t6_176 * GUISkin_get_horizontalScrollbar_m6_1374 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_horizontalScrollbar_14);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbar_m6_1375 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_horizontalScrollbar_14 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarThumb()
extern "C" GUIStyle_t6_176 * GUISkin_get_horizontalScrollbarThumb_m6_1376 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_horizontalScrollbarThumb_15);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarThumb_m6_1377 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_horizontalScrollbarThumb_15 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarLeftButton()
extern "C" GUIStyle_t6_176 * GUISkin_get_horizontalScrollbarLeftButton_m6_1378 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_horizontalScrollbarLeftButton_16);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarLeftButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m6_1379 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_horizontalScrollbarLeftButton_16 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarRightButton()
extern "C" GUIStyle_t6_176 * GUISkin_get_horizontalScrollbarRightButton_m6_1380 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_horizontalScrollbarRightButton_17);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarRightButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m6_1381 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_horizontalScrollbarRightButton_17 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbar()
extern "C" GUIStyle_t6_176 * GUISkin_get_verticalScrollbar_m6_1382 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_verticalScrollbar_18);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbar_m6_1383 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_verticalScrollbar_18 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarThumb()
extern "C" GUIStyle_t6_176 * GUISkin_get_verticalScrollbarThumb_m6_1384 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_verticalScrollbarThumb_19);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarThumb_m6_1385 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_verticalScrollbarThumb_19 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarUpButton()
extern "C" GUIStyle_t6_176 * GUISkin_get_verticalScrollbarUpButton_m6_1386 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_verticalScrollbarUpButton_20);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarUpButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarUpButton_m6_1387 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_verticalScrollbarUpButton_20 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarDownButton()
extern "C" GUIStyle_t6_176 * GUISkin_get_verticalScrollbarDownButton_m6_1388 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_verticalScrollbarDownButton_21);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarDownButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarDownButton_m6_1389 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_verticalScrollbarDownButton_21 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_scrollView()
extern "C" GUIStyle_t6_176 * GUISkin_get_scrollView_m6_1390 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_ScrollView_22);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_scrollView(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_scrollView_m6_1391 (GUISkin_t6_169 * __this, GUIStyle_t6_176 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t6_176 * L_0 = ___value;
		__this->___m_ScrollView_22 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C" GUIStyleU5BU5D_t6_185* GUISkin_get_customStyles_m6_1392 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t6_185* L_0 = (__this->___m_CustomStyles_23);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_customStyles(UnityEngine.GUIStyle[])
extern "C" void GUISkin_set_customStyles_m6_1393 (GUISkin_t6_169 * __this, GUIStyleU5BU5D_t6_185* ___value, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t6_185* L_0 = ___value;
		__this->___m_CustomStyles_23 = L_0;
		GUISkin_Apply_m6_1396(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISettings UnityEngine.GUISkin::get_settings()
extern "C" GUISettings_t6_183 * GUISkin_get_settings_m6_1394 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	{
		GUISettings_t6_183 * L_0 = (__this->___m_Settings_24);
		return L_0;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_error()
extern TypeInfo* GUISkin_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t6_176 * GUISkin_get_error_m6_1395 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1664);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t6_176 * L_0 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_1, /*hidden argument*/NULL);
		((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25 = L_1;
	}

IL_0014:
	{
		GUIStyle_t6_176 * L_2 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		return L_2;
	}
}
// System.Void UnityEngine.GUISkin::Apply()
extern Il2CppCodeGenString* _stringLiteral4829;
extern "C" void GUISkin_Apply_m6_1396 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4829 = il2cpp_codegen_string_literal_from_index(4829);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleU5BU5D_t6_185* L_0 = (__this->___m_CustomStyles_23);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4829, /*hidden argument*/NULL);
	}

IL_0015:
	{
		GUISkin_BuildStyleCache_m6_1397(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUISkin::BuildStyleCache()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* StringComparer_t1_1598_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1_1829_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1_14961_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1764;
extern Il2CppCodeGenString* _stringLiteral4830;
extern Il2CppCodeGenString* _stringLiteral4831;
extern Il2CppCodeGenString* _stringLiteral4832;
extern Il2CppCodeGenString* _stringLiteral4833;
extern Il2CppCodeGenString* _stringLiteral4834;
extern Il2CppCodeGenString* _stringLiteral4835;
extern Il2CppCodeGenString* _stringLiteral4836;
extern Il2CppCodeGenString* _stringLiteral4837;
extern Il2CppCodeGenString* _stringLiteral4838;
extern Il2CppCodeGenString* _stringLiteral4839;
extern Il2CppCodeGenString* _stringLiteral4840;
extern Il2CppCodeGenString* _stringLiteral4841;
extern Il2CppCodeGenString* _stringLiteral4842;
extern Il2CppCodeGenString* _stringLiteral4843;
extern Il2CppCodeGenString* _stringLiteral4844;
extern Il2CppCodeGenString* _stringLiteral4845;
extern Il2CppCodeGenString* _stringLiteral4846;
extern Il2CppCodeGenString* _stringLiteral4847;
extern Il2CppCodeGenString* _stringLiteral4848;
extern "C" void GUISkin_BuildStyleCache_m6_1397 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		StringComparer_t1_1598_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(580);
		Dictionary_2_t1_1829_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1665);
		Dictionary_2__ctor_m1_14961_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483802);
		_stringLiteral1764 = il2cpp_codegen_string_literal_from_index(1764);
		_stringLiteral4830 = il2cpp_codegen_string_literal_from_index(4830);
		_stringLiteral4831 = il2cpp_codegen_string_literal_from_index(4831);
		_stringLiteral4832 = il2cpp_codegen_string_literal_from_index(4832);
		_stringLiteral4833 = il2cpp_codegen_string_literal_from_index(4833);
		_stringLiteral4834 = il2cpp_codegen_string_literal_from_index(4834);
		_stringLiteral4835 = il2cpp_codegen_string_literal_from_index(4835);
		_stringLiteral4836 = il2cpp_codegen_string_literal_from_index(4836);
		_stringLiteral4837 = il2cpp_codegen_string_literal_from_index(4837);
		_stringLiteral4838 = il2cpp_codegen_string_literal_from_index(4838);
		_stringLiteral4839 = il2cpp_codegen_string_literal_from_index(4839);
		_stringLiteral4840 = il2cpp_codegen_string_literal_from_index(4840);
		_stringLiteral4841 = il2cpp_codegen_string_literal_from_index(4841);
		_stringLiteral4842 = il2cpp_codegen_string_literal_from_index(4842);
		_stringLiteral4843 = il2cpp_codegen_string_literal_from_index(4843);
		_stringLiteral4844 = il2cpp_codegen_string_literal_from_index(4844);
		_stringLiteral4845 = il2cpp_codegen_string_literal_from_index(4845);
		_stringLiteral4846 = il2cpp_codegen_string_literal_from_index(4846);
		_stringLiteral4847 = il2cpp_codegen_string_literal_from_index(4847);
		_stringLiteral4848 = il2cpp_codegen_string_literal_from_index(4848);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIStyle_t6_176 * L_0 = (__this->___m_box_3);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_1, /*hidden argument*/NULL);
		__this->___m_box_3 = L_1;
	}

IL_0016:
	{
		GUIStyle_t6_176 * L_2 = (__this->___m_button_4);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		GUIStyle_t6_176 * L_3 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_3, /*hidden argument*/NULL);
		__this->___m_button_4 = L_3;
	}

IL_002c:
	{
		GUIStyle_t6_176 * L_4 = (__this->___m_toggle_5);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t6_176 * L_5 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_5, /*hidden argument*/NULL);
		__this->___m_toggle_5 = L_5;
	}

IL_0042:
	{
		GUIStyle_t6_176 * L_6 = (__this->___m_label_6);
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t6_176 * L_7 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_7, /*hidden argument*/NULL);
		__this->___m_label_6 = L_7;
	}

IL_0058:
	{
		GUIStyle_t6_176 * L_8 = (__this->___m_window_9);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		GUIStyle_t6_176 * L_9 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_9, /*hidden argument*/NULL);
		__this->___m_window_9 = L_9;
	}

IL_006e:
	{
		GUIStyle_t6_176 * L_10 = (__this->___m_textField_7);
		if (L_10)
		{
			goto IL_0084;
		}
	}
	{
		GUIStyle_t6_176 * L_11 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_11, /*hidden argument*/NULL);
		__this->___m_textField_7 = L_11;
	}

IL_0084:
	{
		GUIStyle_t6_176 * L_12 = (__this->___m_textArea_8);
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		GUIStyle_t6_176 * L_13 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_13, /*hidden argument*/NULL);
		__this->___m_textArea_8 = L_13;
	}

IL_009a:
	{
		GUIStyle_t6_176 * L_14 = (__this->___m_horizontalSlider_10);
		if (L_14)
		{
			goto IL_00b0;
		}
	}
	{
		GUIStyle_t6_176 * L_15 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_15, /*hidden argument*/NULL);
		__this->___m_horizontalSlider_10 = L_15;
	}

IL_00b0:
	{
		GUIStyle_t6_176 * L_16 = (__this->___m_horizontalSliderThumb_11);
		if (L_16)
		{
			goto IL_00c6;
		}
	}
	{
		GUIStyle_t6_176 * L_17 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_17, /*hidden argument*/NULL);
		__this->___m_horizontalSliderThumb_11 = L_17;
	}

IL_00c6:
	{
		GUIStyle_t6_176 * L_18 = (__this->___m_verticalSlider_12);
		if (L_18)
		{
			goto IL_00dc;
		}
	}
	{
		GUIStyle_t6_176 * L_19 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_19, /*hidden argument*/NULL);
		__this->___m_verticalSlider_12 = L_19;
	}

IL_00dc:
	{
		GUIStyle_t6_176 * L_20 = (__this->___m_verticalSliderThumb_13);
		if (L_20)
		{
			goto IL_00f2;
		}
	}
	{
		GUIStyle_t6_176 * L_21 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_21, /*hidden argument*/NULL);
		__this->___m_verticalSliderThumb_13 = L_21;
	}

IL_00f2:
	{
		GUIStyle_t6_176 * L_22 = (__this->___m_horizontalScrollbar_14);
		if (L_22)
		{
			goto IL_0108;
		}
	}
	{
		GUIStyle_t6_176 * L_23 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_23, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbar_14 = L_23;
	}

IL_0108:
	{
		GUIStyle_t6_176 * L_24 = (__this->___m_horizontalScrollbarThumb_15);
		if (L_24)
		{
			goto IL_011e;
		}
	}
	{
		GUIStyle_t6_176 * L_25 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_25, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarThumb_15 = L_25;
	}

IL_011e:
	{
		GUIStyle_t6_176 * L_26 = (__this->___m_horizontalScrollbarLeftButton_16);
		if (L_26)
		{
			goto IL_0134;
		}
	}
	{
		GUIStyle_t6_176 * L_27 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_27, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarLeftButton_16 = L_27;
	}

IL_0134:
	{
		GUIStyle_t6_176 * L_28 = (__this->___m_horizontalScrollbarRightButton_17);
		if (L_28)
		{
			goto IL_014a;
		}
	}
	{
		GUIStyle_t6_176 * L_29 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_29, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarRightButton_17 = L_29;
	}

IL_014a:
	{
		GUIStyle_t6_176 * L_30 = (__this->___m_verticalScrollbar_18);
		if (L_30)
		{
			goto IL_0160;
		}
	}
	{
		GUIStyle_t6_176 * L_31 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_31, /*hidden argument*/NULL);
		__this->___m_verticalScrollbar_18 = L_31;
	}

IL_0160:
	{
		GUIStyle_t6_176 * L_32 = (__this->___m_verticalScrollbarThumb_19);
		if (L_32)
		{
			goto IL_0176;
		}
	}
	{
		GUIStyle_t6_176 * L_33 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_33, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarThumb_19 = L_33;
	}

IL_0176:
	{
		GUIStyle_t6_176 * L_34 = (__this->___m_verticalScrollbarUpButton_20);
		if (L_34)
		{
			goto IL_018c;
		}
	}
	{
		GUIStyle_t6_176 * L_35 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_35, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarUpButton_20 = L_35;
	}

IL_018c:
	{
		GUIStyle_t6_176 * L_36 = (__this->___m_verticalScrollbarDownButton_21);
		if (L_36)
		{
			goto IL_01a2;
		}
	}
	{
		GUIStyle_t6_176 * L_37 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_37, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarDownButton_21 = L_37;
	}

IL_01a2:
	{
		GUIStyle_t6_176 * L_38 = (__this->___m_ScrollView_22);
		if (L_38)
		{
			goto IL_01b8;
		}
	}
	{
		GUIStyle_t6_176 * L_39 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_39, /*hidden argument*/NULL);
		__this->___m_ScrollView_22 = L_39;
	}

IL_01b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1_1598_il2cpp_TypeInfo_var);
		StringComparer_t1_1598 * L_40 = StringComparer_get_OrdinalIgnoreCase_m1_14591(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_41 = (Dictionary_2_t1_1829 *)il2cpp_codegen_object_new (Dictionary_2_t1_1829_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1_14961(L_41, L_40, /*hidden argument*/Dictionary_2__ctor_m1_14961_MethodInfo_var);
		__this->___m_Styles_26 = L_41;
		Dictionary_2_t1_1829 * L_42 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_43 = (__this->___m_box_3);
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_42, _stringLiteral1764, L_43);
		GUIStyle_t6_176 * L_44 = (__this->___m_box_3);
		NullCheck(L_44);
		GUIStyle_set_name_m6_1456(L_44, _stringLiteral1764, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_45 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_46 = (__this->___m_button_4);
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_45, _stringLiteral4830, L_46);
		GUIStyle_t6_176 * L_47 = (__this->___m_button_4);
		NullCheck(L_47);
		GUIStyle_set_name_m6_1456(L_47, _stringLiteral4830, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_48 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_49 = (__this->___m_toggle_5);
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_48, _stringLiteral4831, L_49);
		GUIStyle_t6_176 * L_50 = (__this->___m_toggle_5);
		NullCheck(L_50);
		GUIStyle_set_name_m6_1456(L_50, _stringLiteral4831, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_51 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_52 = (__this->___m_label_6);
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_51, _stringLiteral4832, L_52);
		GUIStyle_t6_176 * L_53 = (__this->___m_label_6);
		NullCheck(L_53);
		GUIStyle_set_name_m6_1456(L_53, _stringLiteral4832, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_54 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_55 = (__this->___m_window_9);
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_54, _stringLiteral4833, L_55);
		GUIStyle_t6_176 * L_56 = (__this->___m_window_9);
		NullCheck(L_56);
		GUIStyle_set_name_m6_1456(L_56, _stringLiteral4833, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_57 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_58 = (__this->___m_textField_7);
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_57, _stringLiteral4834, L_58);
		GUIStyle_t6_176 * L_59 = (__this->___m_textField_7);
		NullCheck(L_59);
		GUIStyle_set_name_m6_1456(L_59, _stringLiteral4834, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_60 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_61 = (__this->___m_textArea_8);
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_60, _stringLiteral4835, L_61);
		GUIStyle_t6_176 * L_62 = (__this->___m_textArea_8);
		NullCheck(L_62);
		GUIStyle_set_name_m6_1456(L_62, _stringLiteral4835, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_63 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_64 = (__this->___m_horizontalSlider_10);
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_63, _stringLiteral4836, L_64);
		GUIStyle_t6_176 * L_65 = (__this->___m_horizontalSlider_10);
		NullCheck(L_65);
		GUIStyle_set_name_m6_1456(L_65, _stringLiteral4836, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_66 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_67 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_66, _stringLiteral4837, L_67);
		GUIStyle_t6_176 * L_68 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_68);
		GUIStyle_set_name_m6_1456(L_68, _stringLiteral4837, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_69 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_70 = (__this->___m_verticalSlider_12);
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_69, _stringLiteral4838, L_70);
		GUIStyle_t6_176 * L_71 = (__this->___m_verticalSlider_12);
		NullCheck(L_71);
		GUIStyle_set_name_m6_1456(L_71, _stringLiteral4838, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_72 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_73 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_72, _stringLiteral4839, L_73);
		GUIStyle_t6_176 * L_74 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_74);
		GUIStyle_set_name_m6_1456(L_74, _stringLiteral4839, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_75 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_76 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_75, _stringLiteral4840, L_76);
		GUIStyle_t6_176 * L_77 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_77);
		GUIStyle_set_name_m6_1456(L_77, _stringLiteral4840, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_78 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_79 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_78, _stringLiteral4841, L_79);
		GUIStyle_t6_176 * L_80 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_80);
		GUIStyle_set_name_m6_1456(L_80, _stringLiteral4841, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_81 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_82 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_81, _stringLiteral4842, L_82);
		GUIStyle_t6_176 * L_83 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_83);
		GUIStyle_set_name_m6_1456(L_83, _stringLiteral4842, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_84 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_85 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_84, _stringLiteral4843, L_85);
		GUIStyle_t6_176 * L_86 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_86);
		GUIStyle_set_name_m6_1456(L_86, _stringLiteral4843, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_87 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_88 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_87, _stringLiteral4844, L_88);
		GUIStyle_t6_176 * L_89 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_89);
		GUIStyle_set_name_m6_1456(L_89, _stringLiteral4844, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_90 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_91 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_90);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_90, _stringLiteral4845, L_91);
		GUIStyle_t6_176 * L_92 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_92);
		GUIStyle_set_name_m6_1456(L_92, _stringLiteral4845, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_93 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_94 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_93);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_93, _stringLiteral4846, L_94);
		GUIStyle_t6_176 * L_95 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_95);
		GUIStyle_set_name_m6_1456(L_95, _stringLiteral4846, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_96 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_97 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_96);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_96, _stringLiteral4847, L_97);
		GUIStyle_t6_176 * L_98 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_98);
		GUIStyle_set_name_m6_1456(L_98, _stringLiteral4847, /*hidden argument*/NULL);
		Dictionary_2_t1_1829 * L_99 = (__this->___m_Styles_26);
		GUIStyle_t6_176 * L_100 = (__this->___m_ScrollView_22);
		NullCheck(L_99);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_99, _stringLiteral4848, L_100);
		GUIStyle_t6_176 * L_101 = (__this->___m_ScrollView_22);
		NullCheck(L_101);
		GUIStyle_set_name_m6_1456(L_101, _stringLiteral4848, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t6_185* L_102 = (__this->___m_CustomStyles_23);
		if (!L_102)
		{
			goto IL_0516;
		}
	}
	{
		V_0 = 0;
		goto IL_0508;
	}

IL_04d2:
	{
		GUIStyleU5BU5D_t6_185* L_103 = (__this->___m_CustomStyles_23);
		int32_t L_104 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		int32_t L_105 = L_104;
		if ((*(GUIStyle_t6_176 **)(GUIStyle_t6_176 **)SZArrayLdElema(L_103, L_105, sizeof(GUIStyle_t6_176 *))))
		{
			goto IL_04e4;
		}
	}
	{
		goto IL_0504;
	}

IL_04e4:
	{
		Dictionary_2_t1_1829 * L_106 = (__this->___m_Styles_26);
		GUIStyleU5BU5D_t6_185* L_107 = (__this->___m_CustomStyles_23);
		int32_t L_108 = V_0;
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, L_108);
		int32_t L_109 = L_108;
		NullCheck((*(GUIStyle_t6_176 **)(GUIStyle_t6_176 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t6_176 *))));
		String_t* L_110 = GUIStyle_get_name_m6_1455((*(GUIStyle_t6_176 **)(GUIStyle_t6_176 **)SZArrayLdElema(L_107, L_109, sizeof(GUIStyle_t6_176 *))), /*hidden argument*/NULL);
		GUIStyleU5BU5D_t6_185* L_111 = (__this->___m_CustomStyles_23);
		int32_t L_112 = V_0;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		int32_t L_113 = L_112;
		NullCheck(L_106);
		VirtActionInvoker2< String_t*, GUIStyle_t6_176 * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_106, L_110, (*(GUIStyle_t6_176 **)(GUIStyle_t6_176 **)SZArrayLdElema(L_111, L_113, sizeof(GUIStyle_t6_176 *))));
	}

IL_0504:
	{
		int32_t L_114 = V_0;
		V_0 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_0508:
	{
		int32_t L_115 = V_0;
		GUIStyleU5BU5D_t6_185* L_116 = (__this->___m_CustomStyles_23);
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_116)->max_length)))))))
		{
			goto IL_04d2;
		}
	}

IL_0516:
	{
		GUIStyle_t6_176 * L_117 = GUISkin_get_error_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		GUIStyle_set_stretchHeight_m6_1473(L_117, 1, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_118 = GUISkin_get_error_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_118);
		GUIStyleState_t6_186 * L_119 = GUIStyle_get_normal_m6_1432(L_118, /*hidden argument*/NULL);
		Color_t6_40  L_120 = Color_get_red_m6_275(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_119);
		GUIStyleState_set_textColor_m6_1408(L_119, L_120, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::GetStyle(System.String)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t6_164_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4849;
extern Il2CppCodeGenString* _stringLiteral4850;
extern Il2CppCodeGenString* _stringLiteral4851;
extern "C" GUIStyle_t6_176 * GUISkin_GetStyle_m6_1398 (GUISkin_t6_169 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		EventType_t6_164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1641);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral4849 = il2cpp_codegen_string_literal_from_index(4849);
		_stringLiteral4850 = il2cpp_codegen_string_literal_from_index(4850);
		_stringLiteral4851 = il2cpp_codegen_string_literal_from_index(4851);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_176 * V_0 = {0};
	{
		String_t* L_0 = ___styleName;
		GUIStyle_t6_176 * L_1 = GUISkin_FindStyle_m6_1399(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIStyle_t6_176 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		GUIStyle_t6_176 * L_3 = V_0;
		return L_3;
	}

IL_0010:
	{
		ObjectU5BU5D_t1_272* L_4 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 6));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral4849);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4849;
		ObjectU5BU5D_t1_272* L_5 = L_4;
		String_t* L_6 = ___styleName;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_272* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral4850);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4850;
		ObjectU5BU5D_t1_272* L_8 = L_7;
		String_t* L_9 = Object_get_name_m6_708(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t1_272* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral4851);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4851;
		ObjectU5BU5D_t1_272* L_11 = L_10;
		Event_t6_162 * L_12 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = Event_get_type_m6_1164(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(EventType_t6_164_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5, sizeof(Object_t *))) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_562(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Debug_LogWarning_m6_636(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_17 = GUISkin_get_error_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::FindStyle(System.String)
extern Il2CppCodeGenString* _stringLiteral4852;
extern "C" GUIStyle_t6_176 * GUISkin_FindStyle_m6_1399 (GUISkin_t6_169 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4852 = il2cpp_codegen_string_literal_from_index(4852);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t6_176 * V_0 = {0};
	{
		bool L_0 = Object_op_Equality_m6_721(NULL /*static, unused*/, __this, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4852, /*hidden argument*/NULL);
		return (GUIStyle_t6_176 *)NULL;
	}

IL_0018:
	{
		Dictionary_2_t1_1829 * L_1 = (__this->___m_Styles_26);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		GUISkin_BuildStyleCache_m6_1397(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Dictionary_2_t1_1829 * L_2 = (__this->___m_Styles_26);
		String_t* L_3 = ___styleName;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, GUIStyle_t6_176 ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::TryGetValue(!0,!1&) */, L_2, L_3, (&V_0));
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		GUIStyle_t6_176 * L_5 = V_0;
		return L_5;
	}

IL_003e:
	{
		return (GUIStyle_t6_176 *)NULL;
	}
}
// System.Void UnityEngine.GUISkin::MakeCurrent()
extern TypeInfo* GUISkin_t6_169_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUISkin_MakeCurrent_m6_1400 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1664);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___current_28 = __this;
		Font_t6_149 * L_0 = GUISkin_get_font_m6_1350(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m6_1485(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SkinChangedDelegate_t6_184 * L_1 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		SkinChangedDelegate_t6_184 * L_2 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		NullCheck(L_2);
		SkinChangedDelegate_Invoke_m6_1345(L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.GUISkin::GetEnumerator()
extern TypeInfo* Enumerator_t1_1849_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1_14962_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m1_14963_MethodInfo_var;
extern "C" Object_t * GUISkin_GetEnumerator_m6_1401 (GUISkin_t6_169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1_1849_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1666);
		Dictionary_2_get_Values_m1_14962_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483803);
		ValueCollection_GetEnumerator_m1_14963_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483804);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_1829 * L_0 = (__this->___m_Styles_26);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GUISkin_BuildStyleCache_m6_1397(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Dictionary_2_t1_1829 * L_1 = (__this->___m_Styles_26);
		NullCheck(L_1);
		ValueCollection_t1_1848 * L_2 = Dictionary_2_get_Values_m1_14962(L_1, /*hidden argument*/Dictionary_2_get_Values_m1_14962_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1_1849  L_3 = ValueCollection_GetEnumerator_m1_14963(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m1_14963_MethodInfo_var);
		Enumerator_t1_1849  L_4 = L_3;
		Object_t * L_5 = Box(Enumerator_t1_1849_il2cpp_TypeInfo_var, &L_4);
		return (Object_t *)L_5;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor()
extern "C" void GUIStyleState__ctor_m6_1402 (GUIStyleState_t6_186 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyleState_Init_m6_1405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void GUIStyleState__ctor_m6_1403 (GUIStyleState_t6_186 * __this, GUIStyle_t6_176 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		Texture2D_t6_33 * L_2 = GUIStyleState_GetBackgroundInternal_m6_1407(__this, /*hidden argument*/NULL);
		__this->___m_Background_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Finalize()
extern "C" void GUIStyleState_Finalize_m6_1404 (GUIStyleState_t6_186 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t6_176 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			GUIStyleState_Cleanup_m6_1406(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Init()
extern "C" void GUIStyleState_Init_m6_1405 (GUIStyleState_t6_186 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Init_m6_1405_ftn) (GUIStyleState_t6_186 *);
	static GUIStyleState_Init_m6_1405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Init_m6_1405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::Cleanup()
extern "C" void GUIStyleState_Cleanup_m6_1406 (GUIStyleState_t6_186 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Cleanup_m6_1406_ftn) (GUIStyleState_t6_186 *);
	static GUIStyleState_Cleanup_m6_1406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Cleanup_m6_1406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
extern "C" Texture2D_t6_33 * GUIStyleState_GetBackgroundInternal_m6_1407 (GUIStyleState_t6_186 * __this, const MethodInfo* method)
{
	typedef Texture2D_t6_33 * (*GUIStyleState_GetBackgroundInternal_m6_1407_ftn) (GUIStyleState_t6_186 *);
	static GUIStyleState_GetBackgroundInternal_m6_1407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_GetBackgroundInternal_m6_1407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::GetBackgroundInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C" void GUIStyleState_set_textColor_m6_1408 (GUIStyleState_t6_186 * __this, Color_t6_40  ___value, const MethodInfo* method)
{
	{
		GUIStyleState_INTERNAL_set_textColor_m6_1409(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
extern "C" void GUIStyleState_INTERNAL_set_textColor_m6_1409 (GUIStyleState_t6_186 * __this, Color_t6_40 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyleState_INTERNAL_set_textColor_m6_1409_ftn) (GUIStyleState_t6_186 *, Color_t6_40 *);
	static GUIStyleState_INTERNAL_set_textColor_m6_1409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_INTERNAL_set_textColor_m6_1409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C" void RectOffset__ctor_m6_1410 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		RectOffset_Init_m6_1414(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void RectOffset__ctor_m6_1411 (RectOffset_t6_178 * __this, GUIStyle_t6_176 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		return;
	}
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C" void RectOffset_Finalize_m6_1412 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t6_176 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m6_1415(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4853;
extern "C" String_t* RectOffset_ToString_m6_1413 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral4853 = il2cpp_codegen_string_literal_from_index(4853);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = RectOffset_get_left_m6_1416(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m6_1418(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m6_1420(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t1_272* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m6_1422(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4853, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C" void RectOffset_Init_m6_1414 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m6_1414_ftn) (RectOffset_t6_178 *);
	static RectOffset_Init_m6_1414_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m6_1414_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C" void RectOffset_Cleanup_m6_1415 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m6_1415_ftn) (RectOffset_t6_178 *);
	static RectOffset_Cleanup_m6_1415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m6_1415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C" int32_t RectOffset_get_left_m6_1416 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m6_1416_ftn) (RectOffset_t6_178 *);
	static RectOffset_get_left_m6_1416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m6_1416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C" void RectOffset_set_left_m6_1417 (RectOffset_t6_178 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m6_1417_ftn) (RectOffset_t6_178 *, int32_t);
	static RectOffset_set_left_m6_1417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m6_1417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C" int32_t RectOffset_get_right_m6_1418 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m6_1418_ftn) (RectOffset_t6_178 *);
	static RectOffset_get_right_m6_1418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m6_1418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C" void RectOffset_set_right_m6_1419 (RectOffset_t6_178 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m6_1419_ftn) (RectOffset_t6_178 *, int32_t);
	static RectOffset_set_right_m6_1419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m6_1419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C" int32_t RectOffset_get_top_m6_1420 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m6_1420_ftn) (RectOffset_t6_178 *);
	static RectOffset_get_top_m6_1420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m6_1420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C" void RectOffset_set_top_m6_1421 (RectOffset_t6_178 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m6_1421_ftn) (RectOffset_t6_178 *, int32_t);
	static RectOffset_set_top_m6_1421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m6_1421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C" int32_t RectOffset_get_bottom_m6_1422 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m6_1422_ftn) (RectOffset_t6_178 *);
	static RectOffset_get_bottom_m6_1422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m6_1422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C" void RectOffset_set_bottom_m6_1423 (RectOffset_t6_178 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m6_1423_ftn) (RectOffset_t6_178 *, int32_t);
	static RectOffset_set_bottom_m6_1423_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m6_1423_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C" int32_t RectOffset_get_horizontal_m6_1424 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m6_1424_ftn) (RectOffset_t6_178 *);
	static RectOffset_get_horizontal_m6_1424_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m6_1424_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C" int32_t RectOffset_get_vertical_m6_1425 (RectOffset_t6_178 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m6_1425_ftn) (RectOffset_t6_178 *);
	static RectOffset_get_vertical_m6_1425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m6_1425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C" Rect_t6_51  RectOffset_Remove_m6_1426 (RectOffset_t6_178 * __this, Rect_t6_51  ___rect, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = RectOffset_INTERNAL_CALL_Remove_m6_1427(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
extern "C" Rect_t6_51  RectOffset_INTERNAL_CALL_Remove_m6_1427 (Object_t * __this /* static, unused */, RectOffset_t6_178 * ___self, Rect_t6_51 * ___rect, const MethodInfo* method)
{
	typedef Rect_t6_51  (*RectOffset_INTERNAL_CALL_Remove_m6_1427_ftn) (RectOffset_t6_178 *, Rect_t6_51 *);
	static RectOffset_INTERNAL_CALL_Remove_m6_1427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m6_1427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___self, ___rect);
}
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C" void GUIStyle__ctor_m6_1428 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_Init_m6_1452(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.ctor(UnityEngine.GUIStyle)
extern "C" void GUIStyle__ctor_m6_1429 (GUIStyle_t6_176 * __this, GUIStyle_t6_176 * ___other, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_0 = ___other;
		GUIStyle_InitCopy_m6_1453(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.cctor()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle__cctor_m6_1430 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUIStyle_t6_176_StaticFields*)GUIStyle_t6_176_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14 = 1;
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Finalize()
extern "C" void GUIStyle_Finalize_m6_1431 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GUIStyle_Cleanup_m6_1454(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m1_3(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern TypeInfo* GUIStyleState_t6_186_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t6_186 * GUIStyle_get_normal_m6_1432 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t6_186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1667);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t6_186 * L_0 = (__this->___m_Normal_1);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m6_1457(__this, 0, /*hidden argument*/NULL);
		GUIStyleState_t6_186 * L_2 = (GUIStyleState_t6_186 *)il2cpp_codegen_object_new (GUIStyleState_t6_186_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m6_1403(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Normal_1 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t6_186 * L_3 = (__this->___m_Normal_1);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_margin()
extern TypeInfo* RectOffset_t6_178_il2cpp_TypeInfo_var;
extern "C" RectOffset_t6_178 * GUIStyle_get_margin_m6_1433 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t6_178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1661);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t6_178 * L_0 = (__this->___m_Margin_11);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m6_1458(__this, 1, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_2 = (RectOffset_t6_178 *)il2cpp_codegen_object_new (RectOffset_t6_178_il2cpp_TypeInfo_var);
		RectOffset__ctor_m6_1411(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Margin_11 = L_2;
	}

IL_001e:
	{
		RectOffset_t6_178 * L_3 = (__this->___m_Margin_11);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_padding()
extern TypeInfo* RectOffset_t6_178_il2cpp_TypeInfo_var;
extern "C" RectOffset_t6_178 * GUIStyle_get_padding_m6_1434 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t6_178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1661);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t6_178 * L_0 = (__this->___m_Padding_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m6_1458(__this, 2, /*hidden argument*/NULL);
		RectOffset_t6_178 * L_2 = (RectOffset_t6_178 *)il2cpp_codegen_object_new (RectOffset_t6_178_il2cpp_TypeInfo_var);
		RectOffset__ctor_m6_1411(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Padding_10 = L_2;
	}

IL_001e:
	{
		RectOffset_t6_178 * L_3 = (__this->___m_Padding_10);
		return L_3;
	}
}
// UnityEngine.Font UnityEngine.GUIStyle::get_font()
extern "C" Font_t6_149 * GUIStyle_get_font_m6_1435 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	{
		Font_t6_149 * L_0 = GUIStyle_GetFontInternal_m6_1475(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.GUIStyle::get_lineHeight()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t6_58_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_get_lineHeight_m6_1436 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		Mathf_t6_58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1589);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		float L_1 = GUIStyle_Internal_GetLineHeight_m6_1474(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t6_58_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_Draw(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* Internal_DrawArguments_t6_193_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw_m6_1437 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Internal_DrawArguments_t6_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1668);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	Internal_DrawArguments_t6_193  V_0 = {0};
	Internal_DrawArguments_t6_193 * G_B2_0 = {0};
	Internal_DrawArguments_t6_193 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Internal_DrawArguments_t6_193 * G_B3_1 = {0};
	Internal_DrawArguments_t6_193 * G_B5_0 = {0};
	Internal_DrawArguments_t6_193 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Internal_DrawArguments_t6_193 * G_B6_1 = {0};
	Internal_DrawArguments_t6_193 * G_B8_0 = {0};
	Internal_DrawArguments_t6_193 * G_B7_0 = {0};
	int32_t G_B9_0 = 0;
	Internal_DrawArguments_t6_193 * G_B9_1 = {0};
	Internal_DrawArguments_t6_193 * G_B11_0 = {0};
	Internal_DrawArguments_t6_193 * G_B10_0 = {0};
	int32_t G_B12_0 = 0;
	Internal_DrawArguments_t6_193 * G_B12_1 = {0};
	{
		Initobj (Internal_DrawArguments_t6_193_il2cpp_TypeInfo_var, (&V_0));
		IntPtr_t L_0 = ___target;
		(&V_0)->___target_0 = L_0;
		Rect_t6_51  L_1 = ___position;
		(&V_0)->___position_1 = L_1;
		bool L_2 = ___isHover;
		G_B1_0 = (&V_0);
		if (!L_2)
		{
			G_B2_0 = (&V_0);
			goto IL_0026;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		G_B3_1->___isHover_2 = G_B3_0;
		bool L_3 = ___isActive;
		G_B4_0 = (&V_0);
		if (!L_3)
		{
			G_B5_0 = (&V_0);
			goto IL_003b;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_003c;
	}

IL_003b:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_003c:
	{
		G_B6_1->___isActive_3 = G_B6_0;
		bool L_4 = ___on;
		G_B7_0 = (&V_0);
		if (!L_4)
		{
			G_B8_0 = (&V_0);
			goto IL_0050;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_0051;
	}

IL_0050:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_0051:
	{
		G_B9_1->___on_4 = G_B9_0;
		bool L_5 = ___hasKeyboardFocus;
		G_B10_0 = (&V_0);
		if (!L_5)
		{
			G_B11_0 = (&V_0);
			goto IL_0065;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		goto IL_0066;
	}

IL_0065:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
	}

IL_0066:
	{
		G_B12_1->___hasKeyboardFocus_5 = G_B12_0;
		GUIContent_t6_171 * L_6 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m6_1478(NULL /*static, unused*/, L_6, (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Draw_m6_1438 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, bool ___isHover, bool ___isActive, bool ___on, bool ___hasKeyboardFocus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_51  L_1 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
		GUIContent_t6_171 * L_2 = ((GUIContent_t6_171_StaticFields*)GUIContent_t6_171_il2cpp_TypeInfo_var->static_fields)->___none_6;
		bool L_3 = ___isHover;
		bool L_4 = ___isActive;
		bool L_5 = ___on;
		bool L_6 = ___hasKeyboardFocus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw_m6_1437(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern "C" void GUIStyle_Draw_m6_1439 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = ___position;
		GUIContent_t6_171 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		GUIStyle_Draw_m6_1440(__this, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Draw(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4854;
extern "C" void GUIStyle_Draw_m6_1440 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		_stringLiteral4854 = il2cpp_codegen_string_literal_from_index(4854);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t6_171 * L_0 = ___content;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_1 = (__this->___m_Ptr_0);
		Rect_t6_51  L_2 = ___position;
		GUIContent_t6_171 * L_3 = ___content;
		int32_t L_4 = ___controlID;
		bool L_5 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_Draw2_m6_1479(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0025;
	}

IL_001b:
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4854, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::DrawCursor(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_DrawCursor_m6_1441 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, int32_t ___Character, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_162 * V_0 = {0};
	Color_t6_40  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t6_162 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m6_1164(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)7))))
		{
			goto IL_0083;
		}
	}
	{
		Color__ctor_m6_269((&V_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_3 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GUISettings_t6_183 * L_4 = GUISkin_get_settings_m6_1394(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		float L_5 = GUISettings_get_cursorFlashSpeed_m6_1341(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Time_get_realtimeSinceStartup_m6_796(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		float L_7 = GUIStyle_Internal_GetCursorFlashOffset_m6_1481(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = V_2;
		float L_9 = V_2;
		V_3 = ((float)((float)(fmodf(((float)((float)L_6-(float)L_7)), L_8))/(float)L_9));
		float L_10 = V_2;
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0063;
		}
	}
	{
		float L_11 = V_3;
		if ((!(((float)L_11) < ((float)(0.5f)))))
		{
			goto IL_0073;
		}
	}

IL_0063:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_12 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUISettings_t6_183 * L_13 = GUISkin_get_settings_m6_1394(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Color_t6_40  L_14 = GUISettings_get_cursorColor_m6_1340(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
	}

IL_0073:
	{
		IntPtr_t L_15 = (__this->___m_Ptr_0);
		Rect_t6_51  L_16 = ___position;
		GUIContent_t6_171 * L_17 = ___content;
		int32_t L_18 = ___Character;
		Color_t6_40  L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_DrawCursor_m6_1482(NULL /*static, unused*/, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::DrawWithTextSelection(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32,System.Int32,System.Boolean)
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern TypeInfo* Internal_DrawWithTextSelectionArguments_t6_194_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_DrawWithTextSelection_m6_1442 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, int32_t ___firstSelectedCharacter, int32_t ___lastSelectedCharacter, bool ___drawSelectionAsComposition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		Internal_DrawWithTextSelectionArguments_t6_194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1669);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	Event_t6_162 * V_0 = {0};
	Color_t6_40  V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Internal_DrawWithTextSelectionArguments_t6_194  V_4 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B5_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B6_1 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B8_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B7_0 = {0};
	int32_t G_B9_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B9_1 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B12_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B10_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B11_0 = {0};
	int32_t G_B13_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B13_1 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B15_0 = {0};
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B14_0 = {0};
	int32_t G_B16_0 = 0;
	Internal_DrawWithTextSelectionArguments_t6_194 * G_B16_1 = {0};
	{
		Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Color__ctor_m6_269((&V_1), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_1 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUISettings_t6_183 * L_2 = GUISkin_get_settings_m6_1394(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_3 = GUISettings_get_cursorFlashSpeed_m6_1341(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		float L_4 = Time_get_realtimeSinceStartup_m6_796(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		float L_5 = GUIStyle_Internal_GetCursorFlashOffset_m6_1481(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = V_2;
		float L_7 = V_2;
		V_3 = ((float)((float)(fmodf(((float)((float)L_4-(float)L_5)), L_6))/(float)L_7));
		float L_8 = V_2;
		if ((((float)L_8) == ((float)(0.0f))))
		{
			goto IL_0057;
		}
	}
	{
		float L_9 = V_3;
		if ((!(((float)L_9) < ((float)(0.5f)))))
		{
			goto IL_0067;
		}
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_10 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUISettings_t6_183 * L_11 = GUISkin_get_settings_m6_1394(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Color_t6_40  L_12 = GUISettings_get_cursorColor_m6_1340(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0067:
	{
		Initobj (Internal_DrawWithTextSelectionArguments_t6_194_il2cpp_TypeInfo_var, (&V_4));
		IntPtr_t L_13 = (__this->___m_Ptr_0);
		(&V_4)->___target_0 = L_13;
		Rect_t6_51  L_14 = ___position;
		(&V_4)->___position_1 = L_14;
		int32_t L_15 = ___firstSelectedCharacter;
		(&V_4)->___firstPos_2 = L_15;
		int32_t L_16 = ___lastSelectedCharacter;
		(&V_4)->___lastPos_3 = L_16;
		Color_t6_40  L_17 = V_1;
		(&V_4)->___cursorColor_4 = L_17;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUISkin_t6_169 * L_18 = GUI_get_skin_m6_1202(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		GUISettings_t6_183 * L_19 = GUISkin_get_settings_m6_1394(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Color_t6_40  L_20 = GUISettings_get_selectionColor_m6_1342(L_19, /*hidden argument*/NULL);
		(&V_4)->___selectionColor_5 = L_20;
		Event_t6_162 * L_21 = V_0;
		NullCheck(L_21);
		Vector2_t6_47  L_22 = Event_get_mousePosition_m6_1131(L_21, /*hidden argument*/NULL);
		bool L_23 = Rect_Contains_m6_320((&___position), L_22, /*hidden argument*/NULL);
		G_B4_0 = (&V_4);
		if (!L_23)
		{
			G_B5_0 = (&V_4);
			goto IL_00ce;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_00cf;
	}

IL_00ce:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_00cf:
	{
		G_B6_1->___isHover_6 = G_B6_0;
		int32_t L_24 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_25 = GUIUtility_get_hotControl_m6_1499(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = (&V_4);
		if ((!(((uint32_t)L_24) == ((uint32_t)L_25))))
		{
			G_B8_0 = (&V_4);
			goto IL_00e7;
		}
	}
	{
		G_B9_0 = 1;
		G_B9_1 = G_B7_0;
		goto IL_00e8;
	}

IL_00e7:
	{
		G_B9_0 = 0;
		G_B9_1 = G_B8_0;
	}

IL_00e8:
	{
		G_B9_1->___isActive_7 = G_B9_0;
		(&V_4)->___on_8 = 0;
		int32_t L_26 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_27 = GUIUtility_get_keyboardControl_m6_1512(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B10_0 = (&V_4);
		if ((!(((uint32_t)L_26) == ((uint32_t)L_27))))
		{
			G_B12_0 = (&V_4);
			goto IL_0112;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		bool L_28 = ((GUIStyle_t6_176_StaticFields*)GUIStyle_t6_176_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14;
		G_B11_0 = G_B10_0;
		if (!L_28)
		{
			G_B12_0 = G_B10_0;
			goto IL_0112;
		}
	}
	{
		G_B13_0 = 1;
		G_B13_1 = G_B11_0;
		goto IL_0113;
	}

IL_0112:
	{
		G_B13_0 = 0;
		G_B13_1 = G_B12_0;
	}

IL_0113:
	{
		G_B13_1->___hasKeyboardFocus_9 = G_B13_0;
		bool L_29 = ___drawSelectionAsComposition;
		G_B14_0 = (&V_4);
		if (!L_29)
		{
			G_B15_0 = (&V_4);
			goto IL_0127;
		}
	}
	{
		G_B16_0 = 1;
		G_B16_1 = G_B14_0;
		goto IL_0128;
	}

IL_0127:
	{
		G_B16_0 = 0;
		G_B16_1 = G_B15_0;
	}

IL_0128:
	{
		G_B16_1->___drawSelectionAsComposition_10 = G_B16_0;
		GUIContent_t6_171 * L_30 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_DrawWithTextSelection_m6_1484(NULL /*static, unused*/, L_30, (&V_4), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::DrawWithTextSelection(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Int32,System.Int32)
extern "C" void GUIStyle_DrawWithTextSelection_m6_1443 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, int32_t ___firstSelectedCharacter, int32_t ___lastSelectedCharacter, const MethodInfo* method)
{
	{
		Rect_t6_51  L_0 = ___position;
		GUIContent_t6_171 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		int32_t L_3 = ___firstSelectedCharacter;
		int32_t L_4 = ___lastSelectedCharacter;
		GUIStyle_DrawWithTextSelection_m6_1442(__this, L_0, L_1, L_2, L_3, L_4, 0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t6_176 * GUIStyle_get_none_m6_1444 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_0 = ((GUIStyle_t6_176_StaticFields*)GUIStyle_t6_176_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t6_176 * L_1 = (GUIStyle_t6_176 *)il2cpp_codegen_object_new (GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m6_1428(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		((GUIStyle_t6_176_StaticFields*)GUIStyle_t6_176_il2cpp_TypeInfo_var->static_fields)->___s_None_15 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_t6_176 * L_2 = ((GUIStyle_t6_176_StaticFields*)GUIStyle_t6_176_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::GetCursorPixelPosition(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  GUIStyle_GetCursorPixelPosition_m6_1445 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___cursorStringIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_51  L_1 = ___position;
		GUIContent_t6_171 * L_2 = ___content;
		int32_t L_3 = ___cursorStringIndex;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_GetCursorPixelPosition_m6_1486(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_4 = V_0;
		return L_4;
	}
}
// System.Int32 UnityEngine.GUIStyle::GetCursorStringIndex(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.Vector2)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" int32_t GUIStyle_GetCursorStringIndex_m6_1446 (GUIStyle_t6_176 * __this, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, Vector2_t6_47  ___cursorPixelPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t6_51  L_1 = ___position;
		GUIContent_t6_171 * L_2 = ___content;
		Vector2_t6_47  L_3 = ___cursorPixelPosition;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		int32_t L_4 = GUIStyle_Internal_GetCursorStringIndex_m6_1488(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" Vector2_t6_47  GUIStyle_CalcSize_m6_1447 (GUIStyle_t6_176 * __this, GUIContent_t6_171 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t6_47  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t6_171 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcSize_m6_1490(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.GUIStyle::CalcHeight(UnityEngine.GUIContent,System.Single)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_CalcHeight_m6_1448 (GUIStyle_t6_176 * __this, GUIContent_t6_171 * ___content, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t6_171 * L_1 = ___content;
		float L_2 = ___width;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		float L_3 = GUIStyle_Internal_CalcHeight_m6_1491(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.GUIStyle::get_isHeightDependantOnWidth()
extern "C" bool GUIStyle_get_isHeightDependantOnWidth_m6_1449 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		float L_0 = GUIStyle_get_fixedHeight_m6_1469(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		bool L_1 = GUIStyle_get_wordWrap_m6_1461(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = GUIStyle_get_imagePosition_m6_1459(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 0;
	}

IL_002a:
	{
		G_B6_0 = G_B4_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = 0;
	}

IL_002d:
	{
		return G_B6_0;
	}
}
// System.Void UnityEngine.GUIStyle::CalcMinMaxWidth(UnityEngine.GUIContent,System.Single&,System.Single&)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_CalcMinMaxWidth_m6_1450 (GUIStyle_t6_176 * __this, GUIContent_t6_171 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t6_171 * L_1 = ___content;
		float* L_2 = ___minWidth;
		float* L_3 = ___maxWidth;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcMinMaxWidth_m6_1492(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.GUIStyle::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4855;
extern "C" String_t* GUIStyle_ToString_m6_1451 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		_stringLiteral4855 = il2cpp_codegen_string_literal_from_index(4855);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = GUIStyle_get_name_m6_1455(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		String_t* L_2 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4855, L_0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Init()
extern "C" void GUIStyle_Init_m6_1452 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Init_m6_1452_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_Init_m6_1452_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Init_m6_1452_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)
extern "C" void GUIStyle_InitCopy_m6_1453 (GUIStyle_t6_176 * __this, GUIStyle_t6_176 * ___other, const MethodInfo* method)
{
	typedef void (*GUIStyle_InitCopy_m6_1453_ftn) (GUIStyle_t6_176 *, GUIStyle_t6_176 *);
	static GUIStyle_InitCopy_m6_1453_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_InitCopy_m6_1453_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)");
	_il2cpp_icall_func(__this, ___other);
}
// System.Void UnityEngine.GUIStyle::Cleanup()
extern "C" void GUIStyle_Cleanup_m6_1454 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Cleanup_m6_1454_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_Cleanup_m6_1454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Cleanup_m6_1454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.String UnityEngine.GUIStyle::get_name()
extern "C" String_t* GUIStyle_get_name_m6_1455 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef String_t* (*GUIStyle_get_name_m6_1455_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_name_m6_1455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_name_m6_1455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_name(System.String)
extern "C" void GUIStyle_set_name_m6_1456 (GUIStyle_t6_176 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_name_m6_1456_ftn) (GUIStyle_t6_176 *, String_t*);
	static GUIStyle_set_name_m6_1456_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_name_m6_1456_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetStyleStatePtr_m6_1457 (GUIStyle_t6_176 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetStyleStatePtr_m6_1457_ftn) (GUIStyle_t6_176 *, int32_t);
	static GUIStyle_GetStyleStatePtr_m6_1457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetStyleStatePtr_m6_1457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetRectOffsetPtr_m6_1458 (GUIStyle_t6_176 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetRectOffsetPtr_m6_1458_ftn) (GUIStyle_t6_176 *, int32_t);
	static GUIStyle_GetRectOffsetPtr_m6_1458_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetRectOffsetPtr_m6_1458_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
extern "C" int32_t GUIStyle_get_imagePosition_m6_1459 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_get_imagePosition_m6_1459_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_imagePosition_m6_1459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_imagePosition_m6_1459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_imagePosition()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C" void GUIStyle_set_alignment_m6_1460 (GUIStyle_t6_176 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_alignment_m6_1460_ftn) (GUIStyle_t6_176 *, int32_t);
	static GUIStyle_set_alignment_m6_1460_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_alignment_m6_1460_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_wordWrap()
extern "C" bool GUIStyle_get_wordWrap_m6_1461 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_wordWrap_m6_1461_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_wordWrap_m6_1461_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_wordWrap_m6_1461_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_wordWrap()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::get_contentOffset()
extern "C" Vector2_t6_47  GUIStyle_get_contentOffset_m6_1462 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	Vector2_t6_47  V_0 = {0};
	{
		GUIStyle_INTERNAL_get_contentOffset_m6_1464(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t6_47  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GUIStyle::set_contentOffset(UnityEngine.Vector2)
extern "C" void GUIStyle_set_contentOffset_m6_1463 (GUIStyle_t6_176 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		GUIStyle_INTERNAL_set_contentOffset_m6_1465(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_get_contentOffset_m6_1464 (GUIStyle_t6_176 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_get_contentOffset_m6_1464_ftn) (GUIStyle_t6_176 *, Vector2_t6_47 *);
	static GUIStyle_INTERNAL_get_contentOffset_m6_1464_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_get_contentOffset_m6_1464_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_set_contentOffset_m6_1465 (GUIStyle_t6_176 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_set_contentOffset_m6_1465_ftn) (GUIStyle_t6_176 *, Vector2_t6_47 *);
	static GUIStyle_INTERNAL_set_contentOffset_m6_1465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_set_contentOffset_m6_1465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::set_Internal_clipOffset(UnityEngine.Vector2)
extern "C" void GUIStyle_set_Internal_clipOffset_m6_1466 (GUIStyle_t6_176 * __this, Vector2_t6_47  ___value, const MethodInfo* method)
{
	{
		GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1467(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_set_Internal_clipOffset(UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1467 (GUIStyle_t6_176 * __this, Vector2_t6_47 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1467_ftn) (GUIStyle_t6_176 *, Vector2_t6_47 *);
	static GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1467_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_set_Internal_clipOffset_m6_1467_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_set_Internal_clipOffset(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::get_fixedWidth()
extern "C" float GUIStyle_get_fixedWidth_m6_1468 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedWidth_m6_1468_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_fixedWidth_m6_1468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedWidth_m6_1468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.GUIStyle::get_fixedHeight()
extern "C" float GUIStyle_get_fixedHeight_m6_1469 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedHeight_m6_1469_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_fixedHeight_m6_1469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedHeight_m6_1469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
extern "C" bool GUIStyle_get_stretchWidth_m6_1470 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchWidth_m6_1470_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_stretchWidth_m6_1470_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchWidth_m6_1470_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
extern "C" void GUIStyle_set_stretchWidth_m6_1471 (GUIStyle_t6_176 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchWidth_m6_1471_ftn) (GUIStyle_t6_176 *, bool);
	static GUIStyle_set_stretchWidth_m6_1471_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchWidth_m6_1471_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
extern "C" bool GUIStyle_get_stretchHeight_m6_1472 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchHeight_m6_1472_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_get_stretchHeight_m6_1472_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchHeight_m6_1472_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
extern "C" void GUIStyle_set_stretchHeight_m6_1473 (GUIStyle_t6_176 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchHeight_m6_1473_ftn) (GUIStyle_t6_176 *, bool);
	static GUIStyle_set_stretchHeight_m6_1473_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchHeight_m6_1473_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
extern "C" float GUIStyle_Internal_GetLineHeight_m6_1474 (Object_t * __this /* static, unused */, IntPtr_t ___target, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_GetLineHeight_m6_1474_ftn) (IntPtr_t);
	static GUIStyle_Internal_GetLineHeight_m6_1474_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_GetLineHeight_m6_1474_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)");
	return _il2cpp_icall_func(___target);
}
// UnityEngine.Font UnityEngine.GUIStyle::GetFontInternal()
extern "C" Font_t6_149 * GUIStyle_GetFontInternal_m6_1475 (GUIStyle_t6_176 * __this, const MethodInfo* method)
{
	typedef Font_t6_149 * (*GUIStyle_GetFontInternal_m6_1475_ftn) (GUIStyle_t6_176 *);
	static GUIStyle_GetFontInternal_m6_1475_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetFontInternal_m6_1475_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetFontInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C" void GUIStyle_set_fontSize_m6_1476 (GUIStyle_t6_176 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_fontSize_m6_1476_ftn) (GUIStyle_t6_176 *, int32_t);
	static GUIStyle_set_fontSize_m6_1476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_fontSize_m6_1476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_fontSize(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::set_richText(System.Boolean)
extern "C" void GUIStyle_set_richText_m6_1477 (GUIStyle_t6_176 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_richText_m6_1477_ftn) (GUIStyle_t6_176 *, bool);
	static GUIStyle_set_richText_m6_1477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_richText_m6_1477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_richText(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)
extern "C" void GUIStyle_Internal_Draw_m6_1478 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, Internal_DrawArguments_t6_193 * ___arguments, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_Draw_m6_1478_ftn) (GUIContent_t6_171 *, Internal_DrawArguments_t6_193 *);
	static GUIStyle_Internal_Draw_m6_1478_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_Draw_m6_1478_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)");
	_il2cpp_icall_func(___content, ___arguments);
}
// System.Void UnityEngine.GUIStyle::Internal_Draw2(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_Draw2_m6_1479 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___style;
		GUIContent_t6_171 * L_1 = ___content;
		int32_t L_2 = ___controlID;
		bool L_3 = ___on;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1480(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1480 (Object_t * __this /* static, unused */, IntPtr_t ___style, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, int32_t ___controlID, bool ___on, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1480_ftn) (IntPtr_t, Rect_t6_51 *, GUIContent_t6_171 *, int32_t, bool);
	static GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_Draw2_m6_1480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___style, ___position, ___content, ___controlID, ___on);
}
// System.Single UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()
extern "C" float GUIStyle_Internal_GetCursorFlashOffset_m6_1481 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_GetCursorFlashOffset_m6_1481_ftn) ();
	static GUIStyle_Internal_GetCursorFlashOffset_m6_1481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_GetCursorFlashOffset_m6_1481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIStyle::Internal_DrawCursor(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,UnityEngine.Color)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_DrawCursor_m6_1482 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___pos, Color_t6_40  ___cursorColor, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t6_171 * L_1 = ___content;
		int32_t L_2 = ___pos;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1483(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, (&___cursorColor), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawCursor(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1483 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, int32_t ___pos, Color_t6_40 * ___cursorColor, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1483_ftn) (IntPtr_t, Rect_t6_51 *, GUIContent_t6_171 *, int32_t, Color_t6_40 *);
	static GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_DrawCursor_m6_1483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawCursor(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___target, ___position, ___content, ___pos, ___cursorColor);
}
// System.Void UnityEngine.GUIStyle::Internal_DrawWithTextSelection(UnityEngine.GUIContent,UnityEngine.Internal_DrawWithTextSelectionArguments&)
extern "C" void GUIStyle_Internal_DrawWithTextSelection_m6_1484 (Object_t * __this /* static, unused */, GUIContent_t6_171 * ___content, Internal_DrawWithTextSelectionArguments_t6_194 * ___arguments, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_DrawWithTextSelection_m6_1484_ftn) (GUIContent_t6_171 *, Internal_DrawWithTextSelectionArguments_t6_194 *);
	static GUIStyle_Internal_DrawWithTextSelection_m6_1484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_DrawWithTextSelection_m6_1484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_DrawWithTextSelection(UnityEngine.GUIContent,UnityEngine.Internal_DrawWithTextSelectionArguments&)");
	_il2cpp_icall_func(___content, ___arguments);
}
// System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
extern "C" void GUIStyle_SetDefaultFont_m6_1485 (Object_t * __this /* static, unused */, Font_t6_149 * ___font, const MethodInfo* method)
{
	typedef void (*GUIStyle_SetDefaultFont_m6_1485_ftn) (Font_t6_149 *);
	static GUIStyle_SetDefaultFont_m6_1485_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_SetDefaultFont_m6_1485_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)");
	_il2cpp_icall_func(___font);
}
// System.Void UnityEngine.GUIStyle::Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_GetCursorPixelPosition_m6_1486 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, int32_t ___cursorStringIndex, Vector2_t6_47 * ___ret, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t6_171 * L_1 = ___content;
		int32_t L_2 = ___cursorStringIndex;
		Vector2_t6_47 * L_3 = ___ret;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1487(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1487 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, int32_t ___cursorStringIndex, Vector2_t6_47 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1487_ftn) (IntPtr_t, Rect_t6_51 *, GUIContent_t6_171 *, int32_t, Vector2_t6_47 *);
	static GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m6_1487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___position, ___content, ___cursorStringIndex, ___ret);
}
// System.Int32 UnityEngine.GUIStyle::Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.Vector2)
extern TypeInfo* GUIStyle_t6_176_il2cpp_TypeInfo_var;
extern "C" int32_t GUIStyle_Internal_GetCursorStringIndex_m6_1488 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51  ___position, GUIContent_t6_171 * ___content, Vector2_t6_47  ___cursorPixelPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t6_176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1648);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t6_171 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t6_176_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1489(NULL /*static, unused*/, L_0, (&___position), L_1, (&___cursorPixelPosition), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" int32_t GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1489 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t6_51 * ___position, GUIContent_t6_171 * ___content, Vector2_t6_47 * ___cursorPixelPosition, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1489_ftn) (IntPtr_t, Rect_t6_51 *, GUIContent_t6_171 *, Vector2_t6_47 *);
	static GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex_m6_1489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	return _il2cpp_icall_func(___target, ___position, ___content, ___cursorPixelPosition);
}
// System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" void GUIStyle_Internal_CalcSize_m6_1490 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t6_171 * ___content, Vector2_t6_47 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcSize_m6_1490_ftn) (IntPtr_t, GUIContent_t6_171 *, Vector2_t6_47 *);
	static GUIStyle_Internal_CalcSize_m6_1490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcSize_m6_1490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___content, ___ret);
}
// System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
extern "C" float GUIStyle_Internal_CalcHeight_m6_1491 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t6_171 * ___content, float ___width, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_CalcHeight_m6_1491_ftn) (IntPtr_t, GUIContent_t6_171 *, float);
	static GUIStyle_Internal_CalcHeight_m6_1491_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcHeight_m6_1491_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)");
	return _il2cpp_icall_func(___target, ___content, ___width);
}
// System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
extern "C" void GUIStyle_Internal_CalcMinMaxWidth_m6_1492 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t6_171 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcMinMaxWidth_m6_1492_ftn) (IntPtr_t, GUIContent_t6_171 *, float*, float*);
	static GUIStyle_Internal_CalcMinMaxWidth_m6_1492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcMinMaxWidth_m6_1492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)");
	_il2cpp_icall_func(___target, ___content, ___minWidth, ___maxWidth);
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::op_Implicit(System.String)
extern TypeInfo* GUISkin_t6_169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4856;
extern "C" GUIStyle_t6_176 * GUIStyle_op_Implicit_m6_1493 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t6_169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1664);
		_stringLiteral4856 = il2cpp_codegen_string_literal_from_index(4856);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISkin_t6_169 * L_0 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___current_28;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, _stringLiteral4856, /*hidden argument*/NULL);
		GUIStyle_t6_176 * L_2 = GUISkin_get_error_m6_1395(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0020:
	{
		GUISkin_t6_169 * L_3 = ((GUISkin_t6_169_StaticFields*)GUISkin_t6_169_il2cpp_TypeInfo_var->static_fields)->___current_28;
		String_t* L_4 = ___str;
		NullCheck(L_3);
		GUIStyle_t6_176 * L_5 = GUISkin_GetStyle_m6_1398(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.GUIUtility::.cctor()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void GUIUtility__cctor_m6_1494 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t6_47  L_0 = Vector2_get_zero_m6_225(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GUIUtility_t6_191_StaticFields*)GUIUtility_t6_191_il2cpp_TypeInfo_var->static_fields)->___s_EditorScreenPointOffset_2 = L_0;
		((GUIUtility_t6_191_StaticFields*)GUIUtility_t6_191_il2cpp_TypeInfo_var->static_fields)->___s_HasKeyboardFocus_3 = 0;
		return;
	}
}
// System.Single UnityEngine.GUIUtility::get_pixelsPerPoint()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" float GUIUtility_get_pixelsPerPoint_m6_1495 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		float L_0 = GUIUtility_Internal_GetPixelsPerPoint_m6_1506(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(UnityEngine.FocusType)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_GetControlID_m6_1496 (Object_t * __this /* static, unused */, int32_t ___focus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___focus;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_1 = GUIUtility_GetControlID_m6_1507(NULL /*static, unused*/, 0, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType,UnityEngine.Rect)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_GetControlID_m6_1497 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focus, Rect_t6_51  ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___hint;
		int32_t L_1 = ___focus;
		Rect_t6_51  L_2 = ___position;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_3 = GUIUtility_Internal_GetNextControlID2_m6_1508(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Object UnityEngine.GUIUtility::GetStateObject(System.Type,System.Int32)
extern TypeInfo* GUIStateObjects_t6_220_il2cpp_TypeInfo_var;
extern "C" Object_t * GUIUtility_GetStateObject_m6_1498 (Object_t * __this /* static, unused */, Type_t * ___t, int32_t ___controlID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStateObjects_t6_220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1670);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___t;
		int32_t L_1 = ___controlID;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStateObjects_t6_220_il2cpp_TypeInfo_var);
		Object_t * L_2 = GUIStateObjects_GetStateObject_m6_1553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.GUIUtility::get_hotControl()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_get_hotControl_m6_1499 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetHotControl_m6_1510(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.GUIUtility::set_hotControl(System.Int32)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_set_hotControl_m6_1500 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_Internal_SetHotControl_m6_1511(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::GetDefaultSkin()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" GUISkin_t6_169 * GUIUtility_GetDefaultSkin_m6_1501 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUIUtility_t6_191_StaticFields*)GUIUtility_t6_191_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0;
		GUISkin_t6_169 * L_1 = GUIUtility_Internal_GetDefaultSkin_m6_1516(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUIUtility::BeginGUI(System.Int32,System.Int32,System.Int32)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t6_168_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_BeginGUI_m6_1502 (Object_t * __this /* static, unused */, int32_t ___skinMode, int32_t ___instanceID, int32_t ___useGUILayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUI_t6_168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1643);
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___skinMode;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		((GUIUtility_t6_191_StaticFields*)GUIUtility_t6_191_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0 = L_0;
		int32_t L_1 = ___instanceID;
		((GUIUtility_t6_191_StaticFields*)GUIUtility_t6_191_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_skin_m6_1201(NULL /*static, unused*/, (GUISkin_t6_169 *)NULL, /*hidden argument*/NULL);
		int32_t L_2 = ___useGUILayout;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m6_1281(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = ___instanceID;
		GUILayoutUtility_Begin_m6_1282(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t6_168_il2cpp_TypeInfo_var);
		GUI_set_changed_m6_1228(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIUtility::EndGUI(System.Int32)
extern TypeInfo* GUILayoutUtility_t6_175_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t6_171_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_EndGUI_m6_1503 (Object_t * __this /* static, unused */, int32_t ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t6_175_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1650);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		GUIContent_t6_171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1646);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Event_t6_162 * L_0 = Event_get_current_m6_1150(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_0);
			int32_t L_1 = Event_get_type_m6_1164(L_0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_1) == ((uint32_t)8))))
			{
				goto IL_0042;
			}
		}

IL_0010:
		{
			int32_t L_2 = ___layoutType;
			V_0 = L_2;
			int32_t L_3 = V_0;
			if (L_3 == 0)
			{
				goto IL_0029;
			}
			if (L_3 == 1)
			{
				goto IL_002e;
			}
			if (L_3 == 2)
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			goto IL_0042;
		}

IL_0029:
		{
			goto IL_0042;
		}

IL_002e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUILayoutUtility_Layout_m6_1285(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutFromEditorWindow_m6_1286(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0042:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
			int32_t L_4 = ((GUIUtility_t6_191_StaticFields*)GUIUtility_t6_191_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t6_175_il2cpp_TypeInfo_var);
			GUILayoutUtility_SelectIDList_m6_1281(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t6_171_il2cpp_TypeInfo_var);
			GUIContent_ClearStaticCache_m6_1246(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x5E, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m6_1517(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUIUtility::EndGUIFromException(System.Exception)
extern TypeInfo* ExitGUIException_t6_189_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" bool GUIUtility_EndGUIFromException_m6_1504 (Object_t * __this /* static, unused */, Exception_t1_33 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExitGUIException_t6_189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1671);
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t1_33 * L_0 = ___exception;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Exception_t1_33 * L_1 = ___exception;
		if (((ExitGUIException_t6_189 *)IsInstSealed(L_1, ExitGUIException_t6_189_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		Exception_t1_33 * L_2 = ___exception;
		NullCheck(L_2);
		Exception_t1_33 * L_3 = (Exception_t1_33 *)VirtFuncInvoker0< Exception_t1_33 * >::Invoke(7 /* System.Exception System.Exception::get_InnerException() */, L_2);
		if (((ExitGUIException_t6_189 *)IsInstSealed(L_3, ExitGUIException_t6_189_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		return 0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m6_1517(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.GUIUtility::CheckOnGUI()
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4857;
extern "C" void GUIUtility_CheckOnGUI_m6_1505 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4857 = il2cpp_codegen_string_literal_from_index(4857);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetGUIDepth_m6_1518(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_1, _stringLiteral4857, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
extern "C" float GUIUtility_Internal_GetPixelsPerPoint_m6_1506 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*GUIUtility_Internal_GetPixelsPerPoint_m6_1506_ftn) ();
	static GUIUtility_Internal_GetPixelsPerPoint_m6_1506_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetPixelsPerPoint_m6_1506_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
extern "C" int32_t GUIUtility_GetControlID_m6_1507 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focus, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_GetControlID_m6_1507_ftn) (int32_t, int32_t);
	static GUIUtility_GetControlID_m6_1507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_GetControlID_m6_1507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)");
	return _il2cpp_icall_func(___hint, ___focus);
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect)
extern TypeInfo* GUIUtility_t6_191_il2cpp_TypeInfo_var;
extern "C" int32_t GUIUtility_Internal_GetNextControlID2_m6_1508 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focusType, Rect_t6_51  ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t6_191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1645);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___hint;
		int32_t L_1 = ___focusType;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t6_191_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1509(NULL /*static, unused*/, L_0, L_1, (&___rect), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
extern "C" int32_t GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1509 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___focusType, Rect_t6_51 * ___rect, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1509_ftn) (int32_t, int32_t, Rect_t6_51 *);
	static GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1509_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2_m6_1509_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___hint, ___focusType, ___rect);
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
extern "C" int32_t GUIUtility_Internal_GetHotControl_m6_1510 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetHotControl_m6_1510_ftn) ();
	static GUIUtility_Internal_GetHotControl_m6_1510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetHotControl_m6_1510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetHotControl()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
extern "C" void GUIUtility_Internal_SetHotControl_m6_1511 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_SetHotControl_m6_1511_ftn) (int32_t);
	static GUIUtility_Internal_SetHotControl_m6_1511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_SetHotControl_m6_1511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Int32 UnityEngine.GUIUtility::get_keyboardControl()
extern "C" int32_t GUIUtility_get_keyboardControl_m6_1512 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_get_keyboardControl_m6_1512_ftn) ();
	static GUIUtility_get_keyboardControl_m6_1512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_keyboardControl_m6_1512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_keyboardControl()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_keyboardControl(System.Int32)
extern "C" void GUIUtility_set_keyboardControl_m6_1513 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_keyboardControl_m6_1513_ftn) (int32_t);
	static GUIUtility_set_keyboardControl_m6_1513_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_keyboardControl_m6_1513_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_keyboardControl(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
extern "C" String_t* GUIUtility_get_systemCopyBuffer_m6_1514 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GUIUtility_get_systemCopyBuffer_m6_1514_ftn) ();
	static GUIUtility_get_systemCopyBuffer_m6_1514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_systemCopyBuffer_m6_1514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_systemCopyBuffer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
extern "C" void GUIUtility_set_systemCopyBuffer_m6_1515 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_systemCopyBuffer_m6_1515_ftn) (String_t*);
	static GUIUtility_set_systemCopyBuffer_m6_1515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_systemCopyBuffer_m6_1515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
extern "C" GUISkin_t6_169 * GUIUtility_Internal_GetDefaultSkin_m6_1516 (Object_t * __this /* static, unused */, int32_t ___skinMode, const MethodInfo* method)
{
	typedef GUISkin_t6_169 * (*GUIUtility_Internal_GetDefaultSkin_m6_1516_ftn) (int32_t);
	static GUIUtility_Internal_GetDefaultSkin_m6_1516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetDefaultSkin_m6_1516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)");
	return _il2cpp_icall_func(___skinMode);
}
// System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
extern "C" void GUIUtility_Internal_ExitGUI_m6_1517 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_ExitGUI_m6_1517_ftn) ();
	static GUIUtility_Internal_ExitGUI_m6_1517_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_ExitGUI_m6_1517_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_ExitGUI()");
	_il2cpp_icall_func();
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
extern "C" int32_t GUIUtility_Internal_GetGUIDepth_m6_1518 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetGUIDepth_m6_1518_ftn) ();
	static GUIUtility_Internal_GetGUIDepth_m6_1518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetGUIDepth_m6_1518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetGUIDepth()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
extern "C" void GUIUtility_set_mouseUsed_m6_1519 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_mouseUsed_m6_1519_ftn) (bool);
	static GUIUtility_set_mouseUsed_m6_1519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_mouseUsed_m6_1519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)
extern "C" void GUIUtility_set_textFieldInput_m6_1520 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_textFieldInput_m6_1520_ftn) (bool);
	static GUIUtility_set_textFieldInput_m6_1520_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_textFieldInput_m6_1520_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUIClip::Push(UnityEngine.Rect,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" void GUIClip_Push_m6_1521 (Object_t * __this /* static, unused */, Rect_t6_51  ___screenRect, Vector2_t6_47  ___scrollOffset, Vector2_t6_47  ___renderOffset, bool ___resetOffset, const MethodInfo* method)
{
	{
		bool L_0 = ___resetOffset;
		GUIClip_INTERNAL_CALL_Push_m6_1522(NULL /*static, unused*/, (&___screenRect), (&___scrollOffset), (&___renderOffset), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
extern "C" void GUIClip_INTERNAL_CALL_Push_m6_1522 (Object_t * __this /* static, unused */, Rect_t6_51 * ___screenRect, Vector2_t6_47 * ___scrollOffset, Vector2_t6_47 * ___renderOffset, bool ___resetOffset, const MethodInfo* method)
{
	typedef void (*GUIClip_INTERNAL_CALL_Push_m6_1522_ftn) (Rect_t6_51 *, Vector2_t6_47 *, Vector2_t6_47 *, bool);
	static GUIClip_INTERNAL_CALL_Push_m6_1522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIClip_INTERNAL_CALL_Push_m6_1522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIClip::INTERNAL_CALL_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)");
	_il2cpp_icall_func(___screenRect, ___scrollOffset, ___renderOffset, ___resetOffset);
}
// System.Void UnityEngine.GUIClip::Pop()
extern "C" void GUIClip_Pop_m6_1523 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIClip_Pop_m6_1523_ftn) ();
	static GUIClip_Pop_m6_1523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIClip_Pop_m6_1523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIClip::Pop()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m6_1524 (WrapperlessIcall_t6_195 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m6_1525 (IL2CPPStructAlignmentAttribute_t6_196 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		__this->___Align_0 = 1;
		return;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern TypeInfo* DisallowMultipleComponentU5BU5D_t6_198_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeHelperEngine_t6_197_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditModeU5BU5D_t6_199_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t6_200_il2cpp_TypeInfo_var;
extern "C" void AttributeHelperEngine__cctor_m6_1526 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponentU5BU5D_t6_198_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1672);
		AttributeHelperEngine_t6_197_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1674);
		ExecuteInEditModeU5BU5D_t6_199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1675);
		RequireComponentU5BU5D_t6_200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1677);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t6_197_StaticFields*)AttributeHelperEngine_t6_197_il2cpp_TypeInfo_var->static_fields)->____disallowMultipleComponentArray_0 = ((DisallowMultipleComponentU5BU5D_t6_198*)SZArrayNew(DisallowMultipleComponentU5BU5D_t6_198_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t6_197_StaticFields*)AttributeHelperEngine_t6_197_il2cpp_TypeInfo_var->static_fields)->____executeInEditModeArray_1 = ((ExecuteInEditModeU5BU5D_t6_199*)SZArrayNew(ExecuteInEditModeU5BU5D_t6_199_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t6_197_StaticFields*)AttributeHelperEngine_t6_197_il2cpp_TypeInfo_var->static_fields)->____requireComponentArray_2 = ((RequireComponentU5BU5D_t6_200*)SZArrayNew(RequireComponentU5BU5D_t6_200_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t6_91_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t6_201_0_0_0_var;
extern TypeInfo* Stack_1_t3_247_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m3_1806_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m3_1807_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m3_1808_MethodInfo_var;
extern "C" Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6_1527 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t6_91_0_0_0_var = il2cpp_codegen_type_from_index(1679);
		DisallowMultipleComponent_t6_201_0_0_0_var = il2cpp_codegen_type_from_index(1673);
		Stack_1_t3_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1680);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Stack_1__ctor_m3_1806_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483805);
		Stack_1_Push_m3_1807_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483806);
		Stack_1_Pop_m3_1808_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483807);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t3_247 * V_0 = {0};
	Type_t * V_1 = {0};
	ObjectU5BU5D_t1_272* V_2 = {0};
	int32_t V_3 = 0;
	{
		Stack_1_t3_247 * L_0 = (Stack_1_t3_247 *)il2cpp_codegen_object_new (Stack_1_t3_247_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3_1806(L_0, /*hidden argument*/Stack_1__ctor_m3_1806_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t3_247 * L_1 = V_0;
		Type_t * L_2 = ___type;
		NullCheck(L_1);
		Stack_1_Push_m3_1807(L_1, L_2, /*hidden argument*/Stack_1_Push_m3_1807_MethodInfo_var);
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_3);
		___type = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_91_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005c;
	}

IL_0037:
	{
		Stack_1_t3_247 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m3_1808(L_8, /*hidden argument*/Stack_1_Pop_m3_1808_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t6_201_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t1_272* L_12 = (ObjectU5BU5D_t1_272*)VirtFuncInvoker2< ObjectU5BU5D_t1_272*, Type_t *, bool >::Invoke(29 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, 0);
		V_2 = L_12;
		ObjectU5BU5D_t1_272* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((Array_t *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_15 = V_1;
		return L_15;
	}

IL_005c:
	{
		Stack_1_t3_247 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count() */, L_16);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t6_202_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t6_91_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t6_200_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t1_1850_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_14964_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1_14965_MethodInfo_var;
extern "C" TypeU5BU5D_t1_31* AttributeHelperEngine_GetRequiredComponents_m6_1528 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RequireComponent_t6_202_0_0_0_var = il2cpp_codegen_type_from_index(1678);
		MonoBehaviour_t6_91_0_0_0_var = il2cpp_codegen_type_from_index(1679);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		RequireComponentU5BU5D_t6_200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1677);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		List_1_t1_1850_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1681);
		List_1__ctor_m1_14964_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483808);
		List_1_ToArray_m1_14965_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483809);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1_1850 * V_0 = {0};
	RequireComponentU5BU5D_t6_200* V_1 = {0};
	Type_t * V_2 = {0};
	RequireComponent_t6_202 * V_3 = {0};
	RequireComponentU5BU5D_t6_200* V_4 = {0};
	int32_t V_5 = 0;
	TypeU5BU5D_t1_31* V_6 = {0};
	{
		V_0 = (List_1_t1_1850 *)NULL;
		goto IL_00e0;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t6_202_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1_272* L_2 = (ObjectU5BU5D_t1_272*)VirtFuncInvoker2< ObjectU5BU5D_t1_272*, Type_t *, bool >::Invoke(29 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_1 = ((RequireComponentU5BU5D_t6_200*)Castclass(L_2, RequireComponentU5BU5D_t6_200_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t6_200* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00d2;
	}

IL_0030:
	{
		RequireComponentU5BU5D_t6_200* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(RequireComponent_t6_202 **)(RequireComponent_t6_202 **)SZArrayLdElema(L_6, L_8, sizeof(RequireComponent_t6_202 *)));
		List_1_t1_1850 * L_9 = V_0;
		if (L_9)
		{
			goto IL_007b;
		}
	}
	{
		RequireComponentU5BU5D_t6_200* L_10 = V_1;
		NullCheck(L_10);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_91_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_11) == ((Object_t*)(Type_t *)L_12))))
		{
			goto IL_007b;
		}
	}
	{
		TypeU5BU5D_t1_31* L_13 = ((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 3));
		RequireComponent_t6_202 * L_14 = V_3;
		NullCheck(L_14);
		Type_t * L_15 = (L_14->___m_Type0_0);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_15);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_13, 0, sizeof(Type_t *))) = (Type_t *)L_15;
		TypeU5BU5D_t1_31* L_16 = L_13;
		RequireComponent_t6_202 * L_17 = V_3;
		NullCheck(L_17);
		Type_t * L_18 = (L_17->___m_Type1_1);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_18);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_16, 1, sizeof(Type_t *))) = (Type_t *)L_18;
		TypeU5BU5D_t1_31* L_19 = L_16;
		RequireComponent_t6_202 * L_20 = V_3;
		NullCheck(L_20);
		Type_t * L_21 = (L_20->___m_Type2_2);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_21);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_19, 2, sizeof(Type_t *))) = (Type_t *)L_21;
		V_6 = L_19;
		TypeU5BU5D_t1_31* L_22 = V_6;
		return L_22;
	}

IL_007b:
	{
		List_1_t1_1850 * L_23 = V_0;
		if (L_23)
		{
			goto IL_0087;
		}
	}
	{
		List_1_t1_1850 * L_24 = (List_1_t1_1850 *)il2cpp_codegen_object_new (List_1_t1_1850_il2cpp_TypeInfo_var);
		List_1__ctor_m1_14964(L_24, /*hidden argument*/List_1__ctor_m1_14964_MethodInfo_var);
		V_0 = L_24;
	}

IL_0087:
	{
		RequireComponent_t6_202 * L_25 = V_3;
		NullCheck(L_25);
		Type_t * L_26 = (L_25->___m_Type0_0);
		if (!L_26)
		{
			goto IL_009e;
		}
	}
	{
		List_1_t1_1850 * L_27 = V_0;
		RequireComponent_t6_202 * L_28 = V_3;
		NullCheck(L_28);
		Type_t * L_29 = (L_28->___m_Type0_0);
		NullCheck(L_27);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_27, L_29);
	}

IL_009e:
	{
		RequireComponent_t6_202 * L_30 = V_3;
		NullCheck(L_30);
		Type_t * L_31 = (L_30->___m_Type1_1);
		if (!L_31)
		{
			goto IL_00b5;
		}
	}
	{
		List_1_t1_1850 * L_32 = V_0;
		RequireComponent_t6_202 * L_33 = V_3;
		NullCheck(L_33);
		Type_t * L_34 = (L_33->___m_Type1_1);
		NullCheck(L_32);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_32, L_34);
	}

IL_00b5:
	{
		RequireComponent_t6_202 * L_35 = V_3;
		NullCheck(L_35);
		Type_t * L_36 = (L_35->___m_Type2_2);
		if (!L_36)
		{
			goto IL_00cc;
		}
	}
	{
		List_1_t1_1850 * L_37 = V_0;
		RequireComponent_t6_202 * L_38 = V_3;
		NullCheck(L_38);
		Type_t * L_39 = (L_38->___m_Type2_2);
		NullCheck(L_37);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_37, L_39);
	}

IL_00cc:
	{
		int32_t L_40 = V_5;
		V_5 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00d2:
	{
		int32_t L_41 = V_5;
		RequireComponentU5BU5D_t6_200* L_42 = V_4;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_42)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_43 = V_2;
		___klass = L_43;
	}

IL_00e0:
	{
		Type_t * L_44 = ___klass;
		if (!L_44)
		{
			goto IL_00f6;
		}
	}
	{
		Type_t * L_45 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_46 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_91_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_45) == ((Object_t*)(Type_t *)L_46))))
		{
			goto IL_0007;
		}
	}

IL_00f6:
	{
		List_1_t1_1850 * L_47 = V_0;
		if (L_47)
		{
			goto IL_00fe;
		}
	}
	{
		return (TypeU5BU5D_t1_31*)NULL;
	}

IL_00fe:
	{
		List_1_t1_1850 * L_48 = V_0;
		NullCheck(L_48);
		TypeU5BU5D_t1_31* L_49 = List_1_ToArray_m1_14965(L_48, /*hidden argument*/List_1_ToArray_m1_14965_MethodInfo_var);
		return L_49;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t6_204_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t6_91_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool AttributeHelperEngine_CheckIsEditorScript_m6_1529 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t6_204_0_0_0_var = il2cpp_codegen_type_from_index(1676);
		MonoBehaviour_t6_91_0_0_0_var = il2cpp_codegen_type_from_index(1679);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_272* V_0 = {0};
	int32_t V_1 = 0;
	{
		goto IL_002b;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t6_204_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t1_272* L_2 = (ObjectU5BU5D_t1_272*)VirtFuncInvoker2< ObjectU5BU5D_t1_272*, Type_t *, bool >::Invoke(29 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_0 = L_2;
		ObjectU5BU5D_t1_272* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Array_t *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		return 1;
	}

IL_0023:
	{
		Type_t * L_5 = ___klass;
		NullCheck(L_5);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(158 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass = L_6;
	}

IL_002b:
	{
		Type_t * L_7 = ___klass;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Type_t * L_8 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t6_91_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_8) == ((Object_t*)(Type_t *)L_9))))
		{
			goto IL_0005;
		}
	}

IL_0041:
	{
		return 0;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C" void DisallowMultipleComponent__ctor_m6_1530 (DisallowMultipleComponent_t6_201 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C" void RequireComponent__ctor_m6_1531 (RequireComponent_t6_202 * __this, Type_t * ___requiredComponent, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent;
		__this->___m_Type0_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m6_1532 (AddComponentMenu_t6_203 * __this, String_t* ___menuName, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		__this->___m_Ordering_1 = 0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C" void AddComponentMenu__ctor_m6_1533 (AddComponentMenu_t6_203 * __this, String_t* ___menuName, int32_t ___order, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		int32_t L_1 = ___order;
		__this->___m_Ordering_1 = L_1;
		return;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m6_1534 (ExecuteInEditMode_t6_204 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern "C" void SetupCoroutine__ctor_m6_1535 (SetupCoroutine_t6_205 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1_0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeMember_m6_1536 (Object_t * __this /* static, unused */, Object_t * ___behaviour, String_t* ___name, Object_t * ___variable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_272* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t1_272*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t1_272* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_2;
	}

IL_0013:
	{
		Object_t * L_3 = ___behaviour;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1_5(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name;
		Object_t * L_6 = ___behaviour;
		ObjectU5BU5D_t1_272* L_7 = V_0;
		NullCheck(L_4);
		Object_t * L_8 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1_585 *, Object_t *, ObjectU5BU5D_t1_272*, ParameterModifierU5BU5D_t1_1669*, CultureInfo_t1_277 *, StringU5BU5D_t1_238* >::Invoke(204 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t1_585 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t1_1669*)(ParameterModifierU5BU5D_t1_1669*)NULL, (CultureInfo_t1_277 *)NULL, (StringU5BU5D_t1_238*)(StringU5BU5D_t1_238*)NULL);
		return L_8;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern "C" Object_t * SetupCoroutine_InvokeStatic_m6_1537 (Object_t * __this /* static, unused */, Type_t * ___klass, String_t* ___name, Object_t * ___variable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1_272* V_0 = {0};
	{
		V_0 = (ObjectU5BU5D_t1_272*)NULL;
		Object_t * L_0 = ___variable;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		ObjectU5BU5D_t1_272* L_1 = V_0;
		Object_t * L_2 = ___variable;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_2;
	}

IL_0013:
	{
		Type_t * L_3 = ___klass;
		String_t* L_4 = ___name;
		ObjectU5BU5D_t1_272* L_5 = V_0;
		NullCheck(L_3);
		Object_t * L_6 = (Object_t *)VirtFuncInvoker8< Object_t *, String_t*, int32_t, Binder_t1_585 *, Object_t *, ObjectU5BU5D_t1_272*, ParameterModifierU5BU5D_t1_1669*, CultureInfo_t1_277 *, StringU5BU5D_t1_238* >::Invoke(204 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_3, L_4, ((int32_t)312), (Binder_t1_585 *)NULL, NULL, L_5, (ParameterModifierU5BU5D_t1_1669*)(ParameterModifierU5BU5D_t1_1669*)NULL, (CultureInfo_t1_277 *)NULL, (StringU5BU5D_t1_238*)(StringU5BU5D_t1_238*)NULL);
		return L_6;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m6_1538 (WritableAttribute_t6_206 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C" void AssemblyIsEditorAssembly__ctor_m6_1539 (AssemblyIsEditorAssembly_t6_207 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1_20(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern TypeInfo* UserProfile_t6_221_il2cpp_TypeInfo_var;
extern "C" UserProfile_t6_221 * GcUserProfileData_ToUserProfile_m6_1540 (GcUserProfileData_t6_208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfile_t6_221_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1562);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	{
		String_t* L_0 = (__this->___userName_0);
		String_t* L_1 = (__this->___userID_1);
		int32_t L_2 = (__this->___isFriend_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t6_33 * L_3 = (__this->___image_3);
		UserProfile_t6_221 * L_4 = (UserProfile_t6_221 *)il2cpp_codegen_object_new (UserProfile_t6_221_il2cpp_TypeInfo_var);
		UserProfile__ctor_m6_1560(L_4, G_B3_2, G_B3_1, G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppCodeGenString* _stringLiteral4858;
extern "C" void GcUserProfileData_AddToArray_m6_1541 (GcUserProfileData_t6_208 * __this, UserProfileU5BU5D_t6_19** ___array, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral4858 = il2cpp_codegen_string_literal_from_index(4858);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t6_19** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t6_19**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t6_19**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t6_19** L_3 = ___array;
		int32_t L_4 = ___number;
		UserProfile_t6_221 * L_5 = GcUserProfileData_ToUserProfile_m6_1540(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t6_19**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t6_19**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t6_19**)L_3)), L_5);
		*((UserProfile_t6_221 **)(UserProfile_t6_221 **)SZArrayLdElema((*((UserProfileU5BU5D_t6_19**)L_3)), L_4, sizeof(UserProfile_t6_221 *))) = (UserProfile_t6_221 *)L_5;
		goto IL_002a;
	}

IL_0020:
	{
		Debug_Log_m6_631(NULL /*static, unused*/, _stringLiteral4858, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern TypeInfo* AchievementDescription_t6_224_il2cpp_TypeInfo_var;
extern "C" AchievementDescription_t6_224 * GcAchievementDescriptionData_ToAchievementDescription_m6_1542 (GcAchievementDescriptionData_t6_209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescription_t6_224_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1559);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	Texture2D_t6_33 * G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	String_t* G_B2_4 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	Texture2D_t6_33 * G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B1_4 = {0};
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	Texture2D_t6_33 * G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	String_t* G_B3_5 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		String_t* L_1 = (__this->___m_Title_1);
		Texture2D_t6_33 * L_2 = (__this->___m_Image_2);
		String_t* L_3 = (__this->___m_AchievedDescription_3);
		String_t* L_4 = (__this->___m_UnachievedDescription_4);
		int32_t L_5 = (__this->___m_Hidden_5);
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = (__this->___m_Points_6);
		AchievementDescription_t6_224 * L_7 = (AchievementDescription_t6_224 *)il2cpp_codegen_object_new (AchievementDescription_t6_224_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m6_1580(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern TypeInfo* Achievement_t6_223_il2cpp_TypeInfo_var;
extern "C" Achievement_t6_223 * GcAchievementData_ToAchievement_m6_1543 (GcAchievementData_t6_210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t6_223_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1569);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = {0};
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = {0};
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = {0};
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = {0};
	{
		String_t* L_0 = (__this->___m_Identifier_0);
		double L_1 = (__this->___m_PercentCompleted_1);
		int32_t L_2 = (__this->___m_Completed_2);
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___m_Hidden_3);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m1_13787((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_LastReportedDate_4);
		DateTime_t1_150  L_5 = DateTime_AddSeconds_m1_13840((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t6_223 * L_6 = (Achievement_t6_223 *)il2cpp_codegen_object_new (Achievement_t6_223_il2cpp_TypeInfo_var);
		Achievement__ctor_m6_1569(L_6, G_B6_3, G_B6_2, G_B6_1, G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t6_210_marshal(const GcAchievementData_t6_210& unmarshaled, GcAchievementData_t6_210_marshaled& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Identifier_0);
	marshaled.___m_PercentCompleted_1 = unmarshaled.___m_PercentCompleted_1;
	marshaled.___m_Completed_2 = unmarshaled.___m_Completed_2;
	marshaled.___m_Hidden_3 = unmarshaled.___m_Hidden_3;
	marshaled.___m_LastReportedDate_4 = unmarshaled.___m_LastReportedDate_4;
}
extern "C" void GcAchievementData_t6_210_marshal_back(const GcAchievementData_t6_210_marshaled& marshaled, GcAchievementData_t6_210& unmarshaled)
{
	unmarshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0);
	unmarshaled.___m_PercentCompleted_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.___m_Completed_2 = marshaled.___m_Completed_2;
	unmarshaled.___m_Hidden_3 = marshaled.___m_Hidden_3;
	unmarshaled.___m_LastReportedDate_4 = marshaled.___m_LastReportedDate_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t6_210_marshal_cleanup(GcAchievementData_t6_210_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern TypeInfo* Score_t6_225_il2cpp_TypeInfo_var;
extern "C" Score_t6_225 * GcScoreData_ToScore_m6_1544 (GcScoreData_t6_211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Score_t6_225_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1573);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t1_150  V_0 = {0};
	{
		String_t* L_0 = (__this->___m_Category_0);
		int32_t L_1 = (__this->___m_ValueHigh_2);
		int32_t L_2 = (__this->___m_ValueLow_1);
		String_t* L_3 = (__this->___m_PlayerID_5);
		DateTime__ctor_m1_13787((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___m_Date_3);
		DateTime_t1_150  L_5 = DateTime_AddSeconds_m1_13840((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = (__this->___m_FormattedValue_4);
		int32_t L_7 = (__this->___m_Rank_6);
		Score_t6_225 * L_8 = (Score_t6_225 *)il2cpp_codegen_object_new (Score_t6_225_il2cpp_TypeInfo_var);
		Score__ctor_m6_1591(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t6_211_marshal(const GcScoreData_t6_211& unmarshaled, GcScoreData_t6_211_marshaled& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Category_0);
	marshaled.___m_ValueLow_1 = unmarshaled.___m_ValueLow_1;
	marshaled.___m_ValueHigh_2 = unmarshaled.___m_ValueHigh_2;
	marshaled.___m_Date_3 = unmarshaled.___m_Date_3;
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.___m_FormattedValue_4);
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.___m_PlayerID_5);
	marshaled.___m_Rank_6 = unmarshaled.___m_Rank_6;
}
extern "C" void GcScoreData_t6_211_marshal_back(const GcScoreData_t6_211_marshaled& marshaled, GcScoreData_t6_211& unmarshaled)
{
	unmarshaled.___m_Category_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0);
	unmarshaled.___m_ValueLow_1 = marshaled.___m_ValueLow_1;
	unmarshaled.___m_ValueHigh_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.___m_Date_3 = marshaled.___m_Date_3;
	unmarshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4);
	unmarshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5);
	unmarshaled.___m_Rank_6 = marshaled.___m_Rank_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t6_211_marshal_cleanup(GcScoreData_t6_211_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C" int32_t Resolution_get_width_m6_1545 (Resolution_t6_212 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Width_0);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern "C" void Resolution_set_width_m6_1546 (Resolution_t6_212 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Width_0 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C" int32_t Resolution_get_height_m6_1547 (Resolution_t6_212 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Height_1);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern "C" void Resolution_set_height_m6_1548 (Resolution_t6_212 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Height_1 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern "C" int32_t Resolution_get_refreshRate_m6_1549 (Resolution_t6_212 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_RefreshRate_2);
		return L_0;
	}
}
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern "C" void Resolution_set_refreshRate_m6_1550 (Resolution_t6_212 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_RefreshRate_2 = L_0;
		return;
	}
}
// System.String UnityEngine.Resolution::ToString()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4859;
extern "C" String_t* Resolution_ToString_m6_1551 (Resolution_t6_212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral4859 = il2cpp_codegen_string_literal_from_index(4859);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 3));
		int32_t L_1 = (__this->___m_Width_0);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = L_0;
		int32_t L_5 = (__this->___m_Height_1);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t1_272* L_8 = L_4;
		int32_t L_9 = (__this->___m_RefreshRate_2);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m6_573(NULL /*static, unused*/, _stringLiteral4859, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
