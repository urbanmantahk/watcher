﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DebuggerStepperBoundaryAttribute
struct DebuggerStepperBoundaryAttribute_t1_331;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.DebuggerStepperBoundaryAttribute::.ctor()
extern "C" void DebuggerStepperBoundaryAttribute__ctor_m1_3600 (DebuggerStepperBoundaryAttribute_t1_331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
