﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"

// ExifLibrary.MathEx/UFraction32
struct  UFraction32_t8_121 
{
	// System.UInt32 ExifLibrary.MathEx/UFraction32::mNumerator
	uint32_t ___mNumerator_1;
	// System.UInt32 ExifLibrary.MathEx/UFraction32::mDenominator
	uint32_t ___mDenominator_2;
};
struct UFraction32_t8_121_StaticFields{
	// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::NaN
	UFraction32_t8_121  ___NaN_3;
	// ExifLibrary.MathEx/UFraction32 ExifLibrary.MathEx/UFraction32::Infinity
	UFraction32_t8_121  ___Infinity_4;
};
