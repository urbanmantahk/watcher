﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ChannelServices
struct ChannelServices_t1_871;
// System.Runtime.Remoting.Contexts.CrossContextChannel
struct CrossContextChannel_t1_872;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Runtime.Remoting.Channels.IChannelSender
struct IChannelSender_t1_1705;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Channels.IChannel[]
struct IChannelU5BU5D_t1_1704;
// System.Runtime.Remoting.Channels.IServerChannelSink
struct IServerChannelSink_t1_1706;
// System.Runtime.Remoting.Channels.IServerChannelSinkProvider
struct IServerChannelSinkProvider_t1_1707;
// System.Runtime.Remoting.Channels.IChannelReceiver
struct IChannelReceiver_t1_1708;
// System.Runtime.Remoting.Channels.IServerChannelSinkStack
struct IServerChannelSinkStack_t1_1709;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Channels.IChannel
struct IChannel_t1_1710;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String[]
struct StringU5BU5D_t1_238;
// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;
// System.Runtime.Remoting.ChannelData
struct ChannelData_t1_1022;
// System.Runtime.Remoting.ProviderData
struct ProviderData_t1_1023;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.ReturnMessage
struct ReturnMessage_t1_960;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerProcessing.h"

// System.Void System.Runtime.Remoting.Channels.ChannelServices::.ctor()
extern "C" void ChannelServices__ctor_m1_8037 (ChannelServices_t1_871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelServices::.cctor()
extern "C" void ChannelServices__cctor_m1_8038 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.CrossContextChannel System.Runtime.Remoting.Channels.ChannelServices::get_CrossContextChannel()
extern "C" CrossContextChannel_t1_872 * ChannelServices_get_CrossContextChannel_m1_8039 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ChannelServices::CreateClientChannelSinkChain(System.String,System.Object,System.String&)
extern "C" Object_t * ChannelServices_CreateClientChannelSinkChain_m1_8040 (Object_t * __this /* static, unused */, String_t* ___url, Object_t * ___remoteChannelData, String_t** ___objectUri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ChannelServices::CreateClientChannelSinkChain(System.Runtime.Remoting.Channels.IChannelSender,System.String,System.Object[],System.String&)
extern "C" Object_t * ChannelServices_CreateClientChannelSinkChain_m1_8041 (Object_t * __this /* static, unused */, Object_t * ___sender, String_t* ___url, ObjectU5BU5D_t1_272* ___channelDataArray, String_t** ___objectUri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.IChannel[] System.Runtime.Remoting.Channels.ChannelServices::get_RegisteredChannels()
extern "C" IChannelU5BU5D_t1_1704* ChannelServices_get_RegisteredChannels_m1_8042 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.ChannelServices::CreateServerChannelSinkChain(System.Runtime.Remoting.Channels.IServerChannelSinkProvider,System.Runtime.Remoting.Channels.IChannelReceiver)
extern "C" Object_t * ChannelServices_CreateServerChannelSinkChain_m1_8043 (Object_t * __this /* static, unused */, Object_t * ___provider, Object_t * ___channel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.ServerProcessing System.Runtime.Remoting.Channels.ChannelServices::DispatchMessage(System.Runtime.Remoting.Channels.IServerChannelSinkStack,System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessage&)
extern "C" int32_t ChannelServices_DispatchMessage_m1_8044 (Object_t * __this /* static, unused */, Object_t * ___sinkStack, Object_t * ___msg, Object_t ** ___replyMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.IChannel System.Runtime.Remoting.Channels.ChannelServices::GetChannel(System.String)
extern "C" Object_t * ChannelServices_GetChannel_m1_8045 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.ChannelServices::GetChannelSinkProperties(System.Object)
extern "C" Object_t * ChannelServices_GetChannelSinkProperties_m1_8046 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Runtime.Remoting.Channels.ChannelServices::GetUrlsForObject(System.MarshalByRefObject)
extern "C" StringU5BU5D_t1_238* ChannelServices_GetUrlsForObject_m1_8047 (Object_t * __this /* static, unused */, MarshalByRefObject_t1_69 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern "C" void ChannelServices_RegisterChannel_m1_8048 (Object_t * __this /* static, unused */, Object_t * ___chnl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannel(System.Runtime.Remoting.Channels.IChannel,System.Boolean)
extern "C" void ChannelServices_RegisterChannel_m1_8049 (Object_t * __this /* static, unused */, Object_t * ___chnl, bool ___ensureSecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelServices::RegisterChannelConfig(System.Runtime.Remoting.ChannelData)
extern "C" void ChannelServices_RegisterChannelConfig_m1_8050 (Object_t * __this /* static, unused */, ChannelData_t1_1022 * ___channel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.ChannelServices::CreateProvider(System.Runtime.Remoting.ProviderData)
extern "C" Object_t * ChannelServices_CreateProvider_m1_8051 (Object_t * __this /* static, unused */, ProviderData_t1_1023 * ___prov, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.ChannelServices::SyncDispatchMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ChannelServices_SyncDispatchMessage_m1_8052 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Channels.ChannelServices::AsyncDispatchMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * ChannelServices_AsyncDispatchMessage_m1_8053 (Object_t * __this /* static, unused */, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.ReturnMessage System.Runtime.Remoting.Channels.ChannelServices::CheckIncomingMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" ReturnMessage_t1_960 * ChannelServices_CheckIncomingMessage_m1_8054 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.ChannelServices::CheckReturnMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ChannelServices_CheckReturnMessage_m1_8055 (Object_t * __this /* static, unused */, Object_t * ___callMsg, Object_t * ___retMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Channels.ChannelServices::IsLocalCall(System.Runtime.Remoting.Messaging.IMessage)
extern "C" bool ChannelServices_IsLocalCall_m1_8056 (Object_t * __this /* static, unused */, Object_t * ___callMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ChannelServices::UnregisterChannel(System.Runtime.Remoting.Channels.IChannel)
extern "C" void ChannelServices_UnregisterChannel_m1_8057 (Object_t * __this /* static, unused */, Object_t * ___chnl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Channels.ChannelServices::GetCurrentChannelInfo()
extern "C" ObjectU5BU5D_t1_272* ChannelServices_GetCurrentChannelInfo_m1_8058 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
