﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6_91;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.MonoBehaviourExtensions::.cctor()
extern "C" void MonoBehaviourExtensions__cctor_m8_209 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.MonoBehaviourExtensions::PauseUnity(UnityEngine.MonoBehaviour)
extern "C" void MonoBehaviourExtensions_PauseUnity_m8_210 (Object_t * __this /* static, unused */, MonoBehaviour_t6_91 * ____monoTarget, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.MonoBehaviourExtensions::ResumeUnity(UnityEngine.MonoBehaviour)
extern "C" void MonoBehaviourExtensions_ResumeUnity_m8_211 (Object_t * __this /* static, unused */, MonoBehaviour_t6_91 * ____monoTarget, const MethodInfo* method) IL2CPP_METHOD_ATTR;
