﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.GUIExtensions
struct GUIExtensions_t8_138;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Action`1<System.String>
struct Action_1_t1_1917;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_290;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void VoxelBusters.Utility.GUIExtensions::.ctor()
extern "C" void GUIExtensions__ctor_m8_814 (GUIExtensions_t8_138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.GUIExtensions::TextArea(System.String,UnityEngine.Rect)
extern "C" String_t* GUIExtensions_TextArea_m8_815 (Object_t * __this /* static, unused */, String_t* ____text, Rect_t6_51  ____normalisedBounds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIExtensions::Buttons(System.Collections.ArrayList,System.Action`1<System.String>,UnityEngine.Rect)
extern "C" void GUIExtensions_Buttons_m8_816 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ____buttonsList, Action_1_t1_1917 * ____callbackOnPress, Rect_t6_51  ____normalisedBounds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.GUIExtensions::DrawButtonsLayout(System.Collections.ArrayList,System.Action`1<System.String>,System.Int32,System.Int32,UnityEngine.GUILayoutOption[])
extern "C" void GUIExtensions_DrawButtonsLayout_m8_817 (Object_t * __this /* static, unused */, ArrayList_t1_170 * ____buttonsList, Action_1_t1_1917 * ____callbackOnPress, int32_t ____startingIndex, int32_t ____buttonsPerColumn, GUILayoutOptionU5BU5D_t6_290* ____layoutOptions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect VoxelBusters.Utility.GUIExtensions::GetScreenSpaceBounds(UnityEngine.Rect)
extern "C" Rect_t6_51  GUIExtensions_GetScreenSpaceBounds_m8_818 (Object_t * __this /* static, unused */, Rect_t6_51  ____normalisedBounds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
