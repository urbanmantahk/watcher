﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.PKCS12
struct PKCS12_t1_187;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1_148;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1_166;
// Mono.Security.PKCS7/EncryptedData
struct EncryptedData_t1_223;
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
struct PrivateKeyInfo_t1_169;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t1_222;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"

// System.Void Mono.Security.X509.PKCS12::.ctor()
extern "C" void PKCS12__ctor_m1_2141 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::.ctor(System.Byte[])
extern "C" void PKCS12__ctor_m1_2142 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::.ctor(System.Byte[],System.String)
extern "C" void PKCS12__ctor_m1_2143 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___data, String_t* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::.ctor(System.Byte[],System.Byte[])
extern "C" void PKCS12__ctor_m1_2144 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___data, ByteU5BU5D_t1_109* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::.cctor()
extern "C" void PKCS12__cctor_m1_2145 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::Decode(System.Byte[])
extern "C" void PKCS12_Decode_m1_2146 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::Finalize()
extern "C" void PKCS12_Finalize_m1_2147 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::set_Password(System.String)
extern "C" void PKCS12_set_Password_m1_2148 (PKCS12_t1_187 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.PKCS12::get_IterationCount()
extern "C" int32_t PKCS12_get_IterationCount_m1_2149 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::set_IterationCount(System.Int32)
extern "C" void PKCS12_set_IterationCount_m1_2150 (PKCS12_t1_187 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.PKCS12::get_Keys()
extern "C" ArrayList_t1_170 * PKCS12_get_Keys_m1_2151 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.X509.PKCS12::get_Secrets()
extern "C" ArrayList_t1_170 * PKCS12_get_Secrets_m1_2152 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.PKCS12::get_Certificates()
extern "C" X509CertificateCollection_t1_148 * PKCS12_get_Certificates_m1_2153 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RandomNumberGenerator Mono.Security.X509.PKCS12::get_RNG()
extern "C" RandomNumberGenerator_t1_143 * PKCS12_get_RNG_m1_2154 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.PKCS12::Compare(System.Byte[],System.Byte[])
extern "C" bool PKCS12_Compare_m1_2155 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___expected, ByteU5BU5D_t1_109* ___actual, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.X509.PKCS12::GetSymmetricAlgorithm(System.String,System.Byte[],System.Int32)
extern "C" SymmetricAlgorithm_t1_166 * PKCS12_GetSymmetricAlgorithm_m1_2156 (PKCS12_t1_187 * __this, String_t* ___algorithmOid, ByteU5BU5D_t1_109* ___salt, int32_t ___iterationCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::Decrypt(System.String,System.Byte[],System.Int32,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS12_Decrypt_m1_2157 (PKCS12_t1_187 * __this, String_t* ___algorithmOid, ByteU5BU5D_t1_109* ___salt, int32_t ___iterationCount, ByteU5BU5D_t1_109* ___encryptedData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::Decrypt(Mono.Security.PKCS7/EncryptedData)
extern "C" ByteU5BU5D_t1_109* PKCS12_Decrypt_m1_2158 (PKCS12_t1_187 * __this, EncryptedData_t1_223 * ___ed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::Encrypt(System.String,System.Byte[],System.Int32,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS12_Encrypt_m1_2159 (PKCS12_t1_187 * __this, String_t* ___algorithmOid, ByteU5BU5D_t1_109* ___salt, int32_t ___iterationCount, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSAParameters Mono.Security.X509.PKCS12::GetExistingParameters(System.Boolean&)
extern "C" DSAParameters_t1_1204  PKCS12_GetExistingParameters_m1_2160 (PKCS12_t1_187 * __this, bool* ___found, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddPrivateKey(Mono.Security.Cryptography.PKCS8/PrivateKeyInfo)
extern "C" void PKCS12_AddPrivateKey_m1_2161 (PKCS12_t1_187 * __this, PrivateKeyInfo_t1_169 * ___pki, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::ReadSafeBag(Mono.Security.ASN1)
extern "C" void PKCS12_ReadSafeBag_m1_2162 (PKCS12_t1_187 * __this, ASN1_t1_149 * ___safeBag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.PKCS12::Pkcs8ShroudedKeyBagSafeBag(System.Security.Cryptography.AsymmetricAlgorithm,System.Collections.IDictionary)
extern "C" ASN1_t1_149 * PKCS12_Pkcs8ShroudedKeyBagSafeBag_m1_2163 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.PKCS12::KeyBagSafeBag(System.Security.Cryptography.AsymmetricAlgorithm,System.Collections.IDictionary)
extern "C" ASN1_t1_149 * PKCS12_KeyBagSafeBag_m1_2164 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.PKCS12::SecretBagSafeBag(System.Byte[],System.Collections.IDictionary)
extern "C" ASN1_t1_149 * PKCS12_SecretBagSafeBag_m1_2165 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___secret, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.X509.PKCS12::CertificateSafeBag(Mono.Security.X509.X509Certificate,System.Collections.IDictionary)
extern "C" ASN1_t1_149 * PKCS12_CertificateSafeBag_m1_2166 (PKCS12_t1_187 * __this, X509Certificate_t1_151 * ___x509, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::MAC(System.Byte[],System.Byte[],System.Int32,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS12_MAC_m1_2167 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___password, ByteU5BU5D_t1_109* ___salt, int32_t ___iterations, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::GetBytes()
extern "C" ByteU5BU5D_t1_109* PKCS12_GetBytes_m1_2168 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.PKCS7/ContentInfo Mono.Security.X509.PKCS12::EncryptedContentInfo(Mono.Security.ASN1,System.String)
extern "C" ContentInfo_t1_222 * PKCS12_EncryptedContentInfo_m1_2169 (PKCS12_t1_187 * __this, ASN1_t1_149 * ___safeBags, String_t* ___algorithmOid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddCertificate(Mono.Security.X509.X509Certificate)
extern "C" void PKCS12_AddCertificate_m1_2170 (PKCS12_t1_187 * __this, X509Certificate_t1_151 * ___cert, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddCertificate(Mono.Security.X509.X509Certificate,System.Collections.IDictionary)
extern "C" void PKCS12_AddCertificate_m1_2171 (PKCS12_t1_187 * __this, X509Certificate_t1_151 * ___cert, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::RemoveCertificate(Mono.Security.X509.X509Certificate)
extern "C" void PKCS12_RemoveCertificate_m1_2172 (PKCS12_t1_187 * __this, X509Certificate_t1_151 * ___cert, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::RemoveCertificate(Mono.Security.X509.X509Certificate,System.Collections.IDictionary)
extern "C" void PKCS12_RemoveCertificate_m1_2173 (PKCS12_t1_187 * __this, X509Certificate_t1_151 * ___cert, Object_t * ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.PKCS12::CompareAsymmetricAlgorithm(System.Security.Cryptography.AsymmetricAlgorithm,System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" bool PKCS12_CompareAsymmetricAlgorithm_m1_2174 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___a1, AsymmetricAlgorithm_t1_228 * ___a2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddPkcs8ShroudedKeyBag(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void PKCS12_AddPkcs8ShroudedKeyBag_m1_2175 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddPkcs8ShroudedKeyBag(System.Security.Cryptography.AsymmetricAlgorithm,System.Collections.IDictionary)
extern "C" void PKCS12_AddPkcs8ShroudedKeyBag_m1_2176 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::RemovePkcs8ShroudedKeyBag(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void PKCS12_RemovePkcs8ShroudedKeyBag_m1_2177 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddKeyBag(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void PKCS12_AddKeyBag_m1_2178 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddKeyBag(System.Security.Cryptography.AsymmetricAlgorithm,System.Collections.IDictionary)
extern "C" void PKCS12_AddKeyBag_m1_2179 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::RemoveKeyBag(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void PKCS12_RemoveKeyBag_m1_2180 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddSecretBag(System.Byte[])
extern "C" void PKCS12_AddSecretBag_m1_2181 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___secret, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::AddSecretBag(System.Byte[],System.Collections.IDictionary)
extern "C" void PKCS12_AddSecretBag_m1_2182 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___secret, Object_t * ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::RemoveSecretBag(System.Byte[])
extern "C" void PKCS12_RemoveSecretBag_m1_2183 (PKCS12_t1_187 * __this, ByteU5BU5D_t1_109* ___secret, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.X509.PKCS12::GetAsymmetricAlgorithm(System.Collections.IDictionary)
extern "C" AsymmetricAlgorithm_t1_228 * PKCS12_GetAsymmetricAlgorithm_m1_2184 (PKCS12_t1_187 * __this, Object_t * ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::GetSecret(System.Collections.IDictionary)
extern "C" ByteU5BU5D_t1_109* PKCS12_GetSecret_m1_2185 (PKCS12_t1_187 * __this, Object_t * ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.X509.PKCS12::GetCertificate(System.Collections.IDictionary)
extern "C" X509Certificate_t1_151 * PKCS12_GetCertificate_m1_2186 (PKCS12_t1_187 * __this, Object_t * ___attrs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Mono.Security.X509.PKCS12::GetAttributes(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" Object_t * PKCS12_GetAttributes_m1_2187 (PKCS12_t1_187 * __this, AsymmetricAlgorithm_t1_228 * ___aa, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary Mono.Security.X509.PKCS12::GetAttributes(Mono.Security.X509.X509Certificate)
extern "C" Object_t * PKCS12_GetAttributes_m1_2188 (PKCS12_t1_187 * __this, X509Certificate_t1_151 * ___cert, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::SaveToFile(System.String)
extern "C" void PKCS12_SaveToFile_m1_2189 (PKCS12_t1_187 * __this, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.X509.PKCS12::Clone()
extern "C" Object_t * PKCS12_Clone_m1_2190 (PKCS12_t1_187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.X509.PKCS12::get_MaximumPasswordLength()
extern "C" int32_t PKCS12_get_MaximumPasswordLength_m1_2191 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.PKCS12::set_MaximumPasswordLength(System.Int32)
extern "C" void PKCS12_set_MaximumPasswordLength_m1_2192 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.PKCS12::LoadFile(System.String)
extern "C" ByteU5BU5D_t1_109* PKCS12_LoadFile_m1_2193 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.PKCS12 Mono.Security.X509.PKCS12::LoadFromFile(System.String)
extern "C" PKCS12_t1_187 * PKCS12_LoadFromFile_m1_2194 (Object_t * __this /* static, unused */, String_t* ___filename, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.PKCS12 Mono.Security.X509.PKCS12::LoadFromFile(System.String,System.String)
extern "C" PKCS12_t1_187 * PKCS12_LoadFromFile_m1_2195 (Object_t * __this /* static, unused */, String_t* ___filename, String_t* ___password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
