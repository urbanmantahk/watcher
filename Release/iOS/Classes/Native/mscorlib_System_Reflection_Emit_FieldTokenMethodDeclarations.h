﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_FieldToken.h"

// System.Void System.Reflection.Emit.FieldToken::.ctor(System.Int32)
extern "C" void FieldToken__ctor_m1_5873 (FieldToken_t1_507 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldToken::.cctor()
extern "C" void FieldToken__cctor_m1_5874 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.FieldToken::Equals(System.Object)
extern "C" bool FieldToken_Equals_m1_5875 (FieldToken_t1_507 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.FieldToken::Equals(System.Reflection.Emit.FieldToken)
extern "C" bool FieldToken_Equals_m1_5876 (FieldToken_t1_507 * __this, FieldToken_t1_507  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.FieldToken::GetHashCode()
extern "C" int32_t FieldToken_GetHashCode_m1_5877 (FieldToken_t1_507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.FieldToken::get_Token()
extern "C" int32_t FieldToken_get_Token_m1_5878 (FieldToken_t1_507 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.FieldToken::op_Equality(System.Reflection.Emit.FieldToken,System.Reflection.Emit.FieldToken)
extern "C" bool FieldToken_op_Equality_m1_5879 (Object_t * __this /* static, unused */, FieldToken_t1_507  ___a, FieldToken_t1_507  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.FieldToken::op_Inequality(System.Reflection.Emit.FieldToken,System.Reflection.Emit.FieldToken)
extern "C" bool FieldToken_op_Inequality_m1_5880 (Object_t * __this /* static, unused */, FieldToken_t1_507  ___a, FieldToken_t1_507  ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
