﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_IDLDESC.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_PARAMDESC.h"

// System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION
#pragma pack(push, tp, 1)
struct  DESCUNION_t1_726 
{
	union
	{
		// System.Runtime.InteropServices.ComTypes.IDLDESC System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION::idldesc
		struct
		{
			IDLDESC_t1_727  ___idldesc_0;
		};
		// System.Runtime.InteropServices.ComTypes.PARAMDESC System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION::paramdesc
		struct
		{
			PARAMDESC_t1_728  ___paramdesc_1;
		};
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: System.Runtime.InteropServices.ComTypes.ELEMDESC/DESCUNION
#pragma pack(push, tp, 1)
struct DESCUNION_t1_726_marshaled
{
	union
	{
		struct
		{
			IDLDESC_t1_727_marshaled ___idldesc_0;
		};
		struct
		{
			PARAMDESC_t1_728_marshaled ___paramdesc_1;
		};
	};
};
#pragma pack(pop, tp)
