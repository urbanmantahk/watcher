﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>
struct GetEnumeratorU3Ec__Iterator2_t3_280;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator2__ctor_m3_2185_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2__ctor_m3_2185(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2__ctor_m3_2185_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2186_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2186(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2186_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3_2187_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3_2187(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3_2187_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator2_MoveNext_m3_2188_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_MoveNext_m3_2188(__this, method) (( bool (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_MoveNext_m3_2188_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator2_Dispose_m3_2189_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_Dispose_m3_2189(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_Dispose_m3_2189_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::Reset()
extern "C" void GetEnumeratorU3Ec__Iterator2_Reset_m3_2190_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator2_Reset_m3_2190(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator2_Reset_m3_2190_gshared)(__this, method)
