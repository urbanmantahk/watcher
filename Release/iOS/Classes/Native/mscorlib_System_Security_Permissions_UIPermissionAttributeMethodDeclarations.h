﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.UIPermissionAttribute
struct UIPermissionAttribute_t1_1318;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_UIPermissionClipboard.h"
#include "mscorlib_System_Security_Permissions_UIPermissionWindow.h"

// System.Void System.Security.Permissions.UIPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void UIPermissionAttribute__ctor_m1_11274 (UIPermissionAttribute_t1_1318 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.UIPermissionClipboard System.Security.Permissions.UIPermissionAttribute::get_Clipboard()
extern "C" int32_t UIPermissionAttribute_get_Clipboard_m1_11275 (UIPermissionAttribute_t1_1318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermissionAttribute::set_Clipboard(System.Security.Permissions.UIPermissionClipboard)
extern "C" void UIPermissionAttribute_set_Clipboard_m1_11276 (UIPermissionAttribute_t1_1318 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.UIPermissionWindow System.Security.Permissions.UIPermissionAttribute::get_Window()
extern "C" int32_t UIPermissionAttribute_get_Window_m1_11277 (UIPermissionAttribute_t1_1318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.UIPermissionAttribute::set_Window(System.Security.Permissions.UIPermissionWindow)
extern "C" void UIPermissionAttribute_set_Window_m1_11278 (UIPermissionAttribute_t1_1318 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.UIPermissionAttribute::CreatePermission()
extern "C" Object_t * UIPermissionAttribute_CreatePermission_m1_11279 (UIPermissionAttribute_t1_1318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
