﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>
struct KeyCollection_t1_2668;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Collections.Generic.IEnumerator`1<ExifLibrary.ExifTag>
struct IEnumerator_1_t1_2878;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// ExifLibrary.ExifTag[]
struct ExifTagU5BU5D_t8_379;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m1_26444_gshared (KeyCollection_t1_2668 * __this, Dictionary_2_t1_2663 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m1_26444(__this, ___dictionary, method) (( void (*) (KeyCollection_t1_2668 *, Dictionary_2_t1_2663 *, const MethodInfo*))KeyCollection__ctor_m1_26444_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26445_gshared (KeyCollection_t1_2668 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26445(__this, ___item, method) (( void (*) (KeyCollection_t1_2668 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26445_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26446_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26446(__this, method) (( void (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26447_gshared (KeyCollection_t1_2668 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26447(__this, ___item, method) (( bool (*) (KeyCollection_t1_2668 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26447_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26448_gshared (KeyCollection_t1_2668 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26448(__this, ___item, method) (( bool (*) (KeyCollection_t1_2668 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26448_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26449_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26449(__this, method) (( Object_t* (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26449_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m1_26450_gshared (KeyCollection_t1_2668 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1_26450(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_2668 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1_26450_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26451_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26451(__this, method) (( Object_t * (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26452_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26452(__this, method) (( bool (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26452_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26453_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26453(__this, method) (( bool (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26453_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26454_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26454(__this, method) (( Object_t * (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26454_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m1_26455_gshared (KeyCollection_t1_2668 * __this, ExifTagU5BU5D_t8_379* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m1_26455(__this, ___array, ___index, method) (( void (*) (KeyCollection_t1_2668 *, ExifTagU5BU5D_t8_379*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1_26455_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2669  KeyCollection_GetEnumerator_m1_26456_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1_26456(__this, method) (( Enumerator_t1_2669  (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_GetEnumerator_m1_26456_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m1_26457_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1_26457(__this, method) (( int32_t (*) (KeyCollection_t1_2668 *, const MethodInfo*))KeyCollection_get_Count_m1_26457_gshared)(__this, method)
