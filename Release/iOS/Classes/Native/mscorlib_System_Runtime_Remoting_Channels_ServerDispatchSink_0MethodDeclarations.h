﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.ServerDispatchSinkProvider
struct ServerDispatchSinkProvider_t1_887;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Runtime.Remoting.Channels.IServerChannelSinkProvider
struct IServerChannelSinkProvider_t1_1707;
// System.Runtime.Remoting.Channels.IServerChannelSink
struct IServerChannelSink_t1_1706;
// System.Runtime.Remoting.Channels.IChannelReceiver
struct IChannelReceiver_t1_1708;
// System.Runtime.Remoting.Channels.IChannelDataStore
struct IChannelDataStore_t1_1714;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::.ctor()
extern "C" void ServerDispatchSinkProvider__ctor_m1_8114 (ServerDispatchSinkProvider_t1_887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::.ctor(System.Collections.IDictionary,System.Collections.ICollection)
extern "C" void ServerDispatchSinkProvider__ctor_m1_8115 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___properties, Object_t * ___providerData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.IServerChannelSinkProvider System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::get_Next()
extern "C" Object_t * ServerDispatchSinkProvider_get_Next_m1_8116 (ServerDispatchSinkProvider_t1_887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::set_Next(System.Runtime.Remoting.Channels.IServerChannelSinkProvider)
extern "C" void ServerDispatchSinkProvider_set_Next_m1_8117 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Channels.IServerChannelSink System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::CreateSink(System.Runtime.Remoting.Channels.IChannelReceiver)
extern "C" Object_t * ServerDispatchSinkProvider_CreateSink_m1_8118 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___channel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.ServerDispatchSinkProvider::GetChannelData(System.Runtime.Remoting.Channels.IChannelDataStore)
extern "C" void ServerDispatchSinkProvider_GetChannelData_m1_8119 (ServerDispatchSinkProvider_t1_887 * __this, Object_t * ___channelData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
