﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.UIDemo
struct UIDemo_t8_188;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::.ctor()
extern "C" void UIDemo__ctor_m8_1094 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::DisplayFeatureFunctionalities()
extern "C" void UIDemo_DisplayFeatureFunctionalities_m8_1095 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowAlertDialogWithSingleButton()
extern "C" void UIDemo_ShowAlertDialogWithSingleButton_m8_1096 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowAlertDialogWithMultipleButtons()
extern "C" void UIDemo_ShowAlertDialogWithMultipleButtons_m8_1097 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowPlainTextPromptDialog()
extern "C" void UIDemo_ShowPlainTextPromptDialog_m8_1098 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowSecuredTextPromptDialog()
extern "C" void UIDemo_ShowSecuredTextPromptDialog_m8_1099 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowLoginPromptDialog()
extern "C" void UIDemo_ShowLoginPromptDialog_m8_1100 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowShortDurationToast()
extern "C" void UIDemo_ShowShortDurationToast_m8_1101 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::ShowLongDurationToast()
extern "C" void UIDemo_ShowLongDurationToast_m8_1102 (UIDemo_t8_188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::MultipleButtonsAlertClosed(System.String)
extern "C" void UIDemo_MultipleButtonsAlertClosed_m8_1103 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::SingleFieldPromptDialogClosed(System.String,System.String)
extern "C" void UIDemo_SingleFieldPromptDialogClosed_m8_1104 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, String_t* ____input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::LoginPromptDialogClosed(System.String,System.String,System.String)
extern "C" void UIDemo_LoginPromptDialogClosed_m8_1105 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, String_t* ____username, String_t* ____password, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.UIDemo::<ShowAlertDialogWithSingleButton>m__7(System.String)
extern "C" void UIDemo_U3CShowAlertDialogWithSingleButtonU3Em__7_m8_1106 (UIDemo_t8_188 * __this, String_t* ____buttonPressed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
