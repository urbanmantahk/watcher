﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_IO_DriveInfo__DriveType.h"

// System.IO.DriveInfo
struct  DriveInfo_t1_415  : public Object_t
{
	// System.IO.DriveInfo/_DriveType System.IO.DriveInfo::_drive_type
	int32_t ____drive_type_0;
	// System.String System.IO.DriveInfo::drive_format
	String_t* ___drive_format_1;
	// System.String System.IO.DriveInfo::path
	String_t* ___path_2;
};
