﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.JSONWriter
struct JSONWriter_t8_58;
// System.Text.StringBuilder
struct StringBuilder_t1_247;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Array
struct Array_t;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.JSONWriter::.ctor(System.Int32)
extern "C" void JSONWriter__ctor_m8_300 (JSONWriter_t8_58 * __this, int32_t ____bufferLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder VoxelBusters.Utility.JSONWriter::get_StringBuilder()
extern "C" StringBuilder_t1_247 * JSONWriter_get_StringBuilder_m8_301 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::set_StringBuilder(System.Text.StringBuilder)
extern "C" void JSONWriter_set_StringBuilder_m8_302 (JSONWriter_t8_58 * __this, StringBuilder_t1_247 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.JSONWriter::Serialise(System.Object)
extern "C" String_t* JSONWriter_Serialise_m8_303 (JSONWriter_t8_58 * __this, Object_t * ____objectValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteObjectValue(System.Object)
extern "C" void JSONWriter_WriteObjectValue_m8_304 (JSONWriter_t8_58 * __this, Object_t * ____objectVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteDictionary(System.Collections.IDictionary)
extern "C" void JSONWriter_WriteDictionary_m8_305 (JSONWriter_t8_58 * __this, Object_t * ____dict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteArray(System.Array)
extern "C" void JSONWriter_WriteArray_m8_306 (JSONWriter_t8_58 * __this, Array_t * ____array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteList(System.Collections.IList)
extern "C" void JSONWriter_WriteList_m8_307 (JSONWriter_t8_58 * __this, Object_t * ____list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WritePrimitive(System.Object)
extern "C" void JSONWriter_WritePrimitive_m8_308 (JSONWriter_t8_58 * __this, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteEnum(System.Object)
extern "C" void JSONWriter_WriteEnum_m8_309 (JSONWriter_t8_58 * __this, Object_t * ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteNullValue()
extern "C" void JSONWriter_WriteNullValue_m8_310 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteString(System.String)
extern "C" void JSONWriter_WriteString_m8_311 (JSONWriter_t8_58 * __this, String_t* ____stringVal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteDictionaryStart()
extern "C" void JSONWriter_WriteDictionaryStart_m8_312 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteKeyValuePair(System.String,System.Object,System.Boolean)
extern "C" void JSONWriter_WriteKeyValuePair_m8_313 (JSONWriter_t8_58 * __this, String_t* ____key, Object_t * ____value, bool ____appendSeperator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteKeyValuePairSeperator()
extern "C" void JSONWriter_WriteKeyValuePairSeperator_m8_314 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteDictionaryEnd()
extern "C" void JSONWriter_WriteDictionaryEnd_m8_315 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteArrayStart()
extern "C" void JSONWriter_WriteArrayStart_m8_316 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteArrayEnd()
extern "C" void JSONWriter_WriteArrayEnd_m8_317 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONWriter::WriteElementSeperator()
extern "C" void JSONWriter_WriteElementSeperator_m8_318 (JSONWriter_t8_58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
