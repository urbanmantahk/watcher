﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.SortedList/SynchedSortedList
struct SynchedSortedList_t1_307;
// System.Collections.SortedList
struct SortedList_t1_304;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Array
struct Array_t;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.SortedList/SynchedSortedList::.ctor(System.Collections.SortedList)
extern "C" void SynchedSortedList__ctor_m1_3425 (SynchedSortedList_t1_307 * __this, SortedList_t1_304 * ___host, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/SynchedSortedList::get_Capacity()
extern "C" int32_t SynchedSortedList_get_Capacity_m1_3426 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::set_Capacity(System.Int32)
extern "C" void SynchedSortedList_set_Capacity_m1_3427 (SynchedSortedList_t1_307 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/SynchedSortedList::get_Count()
extern "C" int32_t SynchedSortedList_get_Count_m1_3428 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/SynchedSortedList::get_IsSynchronized()
extern "C" bool SynchedSortedList_get_IsSynchronized_m1_3429 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/SynchedSortedList::get_SyncRoot()
extern "C" Object_t * SynchedSortedList_get_SyncRoot_m1_3430 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/SynchedSortedList::get_IsFixedSize()
extern "C" bool SynchedSortedList_get_IsFixedSize_m1_3431 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/SynchedSortedList::get_IsReadOnly()
extern "C" bool SynchedSortedList_get_IsReadOnly_m1_3432 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Collections.SortedList/SynchedSortedList::get_Keys()
extern "C" Object_t * SynchedSortedList_get_Keys_m1_3433 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Collections.SortedList/SynchedSortedList::get_Values()
extern "C" Object_t * SynchedSortedList_get_Values_m1_3434 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/SynchedSortedList::get_Item(System.Object)
extern "C" Object_t * SynchedSortedList_get_Item_m1_3435 (SynchedSortedList_t1_307 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::set_Item(System.Object,System.Object)
extern "C" void SynchedSortedList_set_Item_m1_3436 (SynchedSortedList_t1_307 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::CopyTo(System.Array,System.Int32)
extern "C" void SynchedSortedList_CopyTo_m1_3437 (SynchedSortedList_t1_307 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::Add(System.Object,System.Object)
extern "C" void SynchedSortedList_Add_m1_3438 (SynchedSortedList_t1_307 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::Clear()
extern "C" void SynchedSortedList_Clear_m1_3439 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/SynchedSortedList::Contains(System.Object)
extern "C" bool SynchedSortedList_Contains_m1_3440 (SynchedSortedList_t1_307 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator System.Collections.SortedList/SynchedSortedList::GetEnumerator()
extern "C" Object_t * SynchedSortedList_GetEnumerator_m1_3441 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::Remove(System.Object)
extern "C" void SynchedSortedList_Remove_m1_3442 (SynchedSortedList_t1_307 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/SynchedSortedList::ContainsKey(System.Object)
extern "C" bool SynchedSortedList_ContainsKey_m1_3443 (SynchedSortedList_t1_307 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/SynchedSortedList::ContainsValue(System.Object)
extern "C" bool SynchedSortedList_ContainsValue_m1_3444 (SynchedSortedList_t1_307 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/SynchedSortedList::Clone()
extern "C" Object_t * SynchedSortedList_Clone_m1_3445 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/SynchedSortedList::GetByIndex(System.Int32)
extern "C" Object_t * SynchedSortedList_GetByIndex_m1_3446 (SynchedSortedList_t1_307 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/SynchedSortedList::GetKey(System.Int32)
extern "C" Object_t * SynchedSortedList_GetKey_m1_3447 (SynchedSortedList_t1_307 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Collections.SortedList/SynchedSortedList::GetKeyList()
extern "C" Object_t * SynchedSortedList_GetKeyList_m1_3448 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Collections.SortedList/SynchedSortedList::GetValueList()
extern "C" Object_t * SynchedSortedList_GetValueList_m1_3449 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::RemoveAt(System.Int32)
extern "C" void SynchedSortedList_RemoveAt_m1_3450 (SynchedSortedList_t1_307 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/SynchedSortedList::IndexOfKey(System.Object)
extern "C" int32_t SynchedSortedList_IndexOfKey_m1_3451 (SynchedSortedList_t1_307 * __this, Object_t * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.SortedList/SynchedSortedList::IndexOfValue(System.Object)
extern "C" int32_t SynchedSortedList_IndexOfValue_m1_3452 (SynchedSortedList_t1_307 * __this, Object_t * ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::SetByIndex(System.Int32,System.Object)
extern "C" void SynchedSortedList_SetByIndex_m1_3453 (SynchedSortedList_t1_307 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/SynchedSortedList::TrimToSize()
extern "C" void SynchedSortedList_TrimToSize_m1_3454 (SynchedSortedList_t1_307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
