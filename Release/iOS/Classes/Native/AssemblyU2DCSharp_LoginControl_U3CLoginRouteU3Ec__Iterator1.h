﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t6_79;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t6_78;
// Boomlagoon.JSON.JSONObject
struct JSONObject_t8_5;
// System.Object
struct Object_t;
// LoginControl
struct LoginControl_t8_11;

#include "mscorlib_System_Object.h"

// LoginControl/<LoginRoute>c__Iterator1
struct  U3CLoginRouteU3Ec__Iterator1_t8_10  : public Object_t
{
	// UnityEngine.WWWForm LoginControl/<LoginRoute>c__Iterator1::<_f>__0
	WWWForm_t6_79 * ___U3C_fU3E__0_0;
	// System.String LoginControl/<LoginRoute>c__Iterator1::_phone
	String_t* ____phone_1;
	// System.String LoginControl/<LoginRoute>c__Iterator1::_password
	String_t* ____password_2;
	// UnityEngine.WWW LoginControl/<LoginRoute>c__Iterator1::<_w>__1
	WWW_t6_78 * ___U3C_wU3E__1_3;
	// System.Single LoginControl/<LoginRoute>c__Iterator1::<_elapse>__2
	float ___U3C_elapseU3E__2_4;
	// System.Single LoginControl/<LoginRoute>c__Iterator1::<_time>__3
	float ___U3C_timeU3E__3_5;
	// System.Single LoginControl/<LoginRoute>c__Iterator1::<_timeout>__4
	float ___U3C_timeoutU3E__4_6;
	// Boomlagoon.JSON.JSONObject LoginControl/<LoginRoute>c__Iterator1::<_json>__5
	JSONObject_t8_5 * ___U3C_jsonU3E__5_7;
	// System.Int32 LoginControl/<LoginRoute>c__Iterator1::$PC
	int32_t ___U24PC_8;
	// System.Object LoginControl/<LoginRoute>c__Iterator1::$current
	Object_t * ___U24current_9;
	// System.String LoginControl/<LoginRoute>c__Iterator1::<$>_phone
	String_t* ___U3CU24U3E_phone_10;
	// System.String LoginControl/<LoginRoute>c__Iterator1::<$>_password
	String_t* ___U3CU24U3E_password_11;
	// LoginControl LoginControl/<LoginRoute>c__Iterator1::<>f__this
	LoginControl_t8_11 * ___U3CU3Ef__this_12;
};
