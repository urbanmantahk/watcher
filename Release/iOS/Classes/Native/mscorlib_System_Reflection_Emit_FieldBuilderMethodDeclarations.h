﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1_499;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Exception
struct Exception_t1_33;
// System.Reflection.Module
struct Module_t1_495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_Reflection_Emit_FieldToken.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Emit.FieldBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.String,System.Type,System.Reflection.FieldAttributes,System.Type[],System.Type[])
extern "C" void FieldBuilder__ctor_m1_5831 (FieldBuilder_t1_499 * __this, TypeBuilder_t1_481 * ___tb, String_t* ___fieldName, Type_t * ___type, int32_t ___attributes, TypeU5BU5D_t1_31* ___modReq, TypeU5BU5D_t1_31* ___modOpt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::System.Runtime.InteropServices._FieldBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void FieldBuilder_System_Runtime_InteropServices__FieldBuilder_GetIDsOfNames_m1_5832 (FieldBuilder_t1_499 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::System.Runtime.InteropServices._FieldBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void FieldBuilder_System_Runtime_InteropServices__FieldBuilder_GetTypeInfo_m1_5833 (FieldBuilder_t1_499 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::System.Runtime.InteropServices._FieldBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void FieldBuilder_System_Runtime_InteropServices__FieldBuilder_GetTypeInfoCount_m1_5834 (FieldBuilder_t1_499 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::System.Runtime.InteropServices._FieldBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void FieldBuilder_System_Runtime_InteropServices__FieldBuilder_Invoke_m1_5835 (FieldBuilder_t1_499 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldAttributes System.Reflection.Emit.FieldBuilder::get_Attributes()
extern "C" int32_t FieldBuilder_get_Attributes_m1_5836 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.FieldBuilder::get_DeclaringType()
extern "C" Type_t * FieldBuilder_get_DeclaringType_m1_5837 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeFieldHandle System.Reflection.Emit.FieldBuilder::get_FieldHandle()
extern "C" RuntimeFieldHandle_t1_36  FieldBuilder_get_FieldHandle_m1_5838 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.FieldBuilder::get_FieldType()
extern "C" Type_t * FieldBuilder_get_FieldType_m1_5839 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.FieldBuilder::get_Name()
extern "C" String_t* FieldBuilder_get_Name_m1_5840 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.FieldBuilder::get_ReflectedType()
extern "C" Type_t * FieldBuilder_get_ReflectedType_m1_5841 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.FieldBuilder::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* FieldBuilder_GetCustomAttributes_m1_5842 (FieldBuilder_t1_499 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.FieldBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* FieldBuilder_GetCustomAttributes_m1_5843 (FieldBuilder_t1_499 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldToken System.Reflection.Emit.FieldBuilder::GetToken()
extern "C" FieldToken_t1_507  FieldBuilder_GetToken_m1_5844 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.FieldBuilder::GetValue(System.Object)
extern "C" Object_t * FieldBuilder_GetValue_m1_5845 (FieldBuilder_t1_499 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.FieldBuilder::IsDefined(System.Type,System.Boolean)
extern "C" bool FieldBuilder_IsDefined_m1_5846 (FieldBuilder_t1_499 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.FieldBuilder::GetFieldOffset()
extern "C" int32_t FieldBuilder_GetFieldOffset_m1_5847 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetRVAData(System.Byte[])
extern "C" void FieldBuilder_SetRVAData_m1_5848 (FieldBuilder_t1_499 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetConstant(System.Object)
extern "C" void FieldBuilder_SetConstant_m1_5849 (FieldBuilder_t1_499 * __this, Object_t * ___defaultValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void FieldBuilder_SetCustomAttribute_m1_5850 (FieldBuilder_t1_499 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void FieldBuilder_SetCustomAttribute_m1_5851 (FieldBuilder_t1_499 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetMarshal(System.Reflection.Emit.UnmanagedMarshal)
extern "C" void FieldBuilder_SetMarshal_m1_5852 (FieldBuilder_t1_499 * __this, UnmanagedMarshal_t1_505 * ___unmanagedMarshal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetOffset(System.Int32)
extern "C" void FieldBuilder_SetOffset_m1_5853 (FieldBuilder_t1_499 * __this, int32_t ___iOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Globalization.CultureInfo)
extern "C" void FieldBuilder_SetValue_m1_5854 (FieldBuilder_t1_499 * __this, Object_t * ___obj, Object_t * ___val, int32_t ___invokeAttr, Binder_t1_585 * ___binder, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.FieldBuilder::get_UMarshal()
extern "C" UnmanagedMarshal_t1_505 * FieldBuilder_get_UMarshal_m1_5855 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.FieldBuilder::CreateNotSupportedException()
extern "C" Exception_t1_33 * FieldBuilder_CreateNotSupportedException_m1_5856 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.FieldBuilder::RejectIfCreated()
extern "C" void FieldBuilder_RejectIfCreated_m1_5857 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.FieldBuilder::get_Module()
extern "C" Module_t1_495 * FieldBuilder_get_Module_m1_5858 (FieldBuilder_t1_499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
