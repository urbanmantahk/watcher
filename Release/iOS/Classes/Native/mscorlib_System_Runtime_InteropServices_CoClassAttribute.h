﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.CoClassAttribute
struct  CoClassAttribute_t1_52  : public Attribute_t1_2
{
	// System.Type System.Runtime.InteropServices.CoClassAttribute::klass
	Type_t * ___klass_0;
};
