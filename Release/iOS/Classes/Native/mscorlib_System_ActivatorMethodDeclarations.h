﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Activator
struct Activator_t1_1492;
// System.Runtime.Remoting.ObjectHandle
struct ObjectHandle_t1_1019;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.String[]
struct StringU5BU5D_t1_238;
// System.AppDomain
struct AppDomain_t1_1403;
// System.Object
struct Object_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Activator::.ctor()
extern "C" void Activator__ctor_m1_13015 (Activator_t1_1492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Activator::System.Runtime.InteropServices._Activator.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Activator_System_Runtime_InteropServices__Activator_GetIDsOfNames_m1_13016 (Activator_t1_1492 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Activator::System.Runtime.InteropServices._Activator.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Activator_System_Runtime_InteropServices__Activator_GetTypeInfo_m1_13017 (Activator_t1_1492 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Activator::System.Runtime.InteropServices._Activator.GetTypeInfoCount(System.UInt32&)
extern "C" void Activator_System_Runtime_InteropServices__Activator_GetTypeInfoCount_m1_13018 (Activator_t1_1492 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Activator::System.Runtime.InteropServices._Activator.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void Activator_System_Runtime_InteropServices__Activator_Invoke_m1_13019 (Activator_t1_1492 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateComInstanceFrom(System.String,System.String)
extern "C" ObjectHandle_t1_1019 * Activator_CreateComInstanceFrom_m1_13020 (Object_t * __this /* static, unused */, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateComInstanceFrom(System.String,System.String,System.Byte[],System.Configuration.Assemblies.AssemblyHashAlgorithm)
extern "C" ObjectHandle_t1_1019 * Activator_CreateComInstanceFrom_m1_13021 (Object_t * __this /* static, unused */, String_t* ___assemblyName, String_t* ___typeName, ByteU5BU5D_t1_109* ___hashValue, int32_t ___hashAlgorithm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstanceFrom(System.String,System.String)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstanceFrom_m1_13022 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstanceFrom(System.String,System.String,System.Object[])
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstanceFrom_m1_13023 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, String_t* ___typeName, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstanceFrom(System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstanceFrom_m1_13024 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.String,System.String)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13025 (Object_t * __this /* static, unused */, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.String,System.String,System.Object[])
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13026 (Object_t * __this /* static, unused */, String_t* ___assemblyName, String_t* ___typeName, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13027 (Object_t * __this /* static, unused */, String_t* ___assemblyName, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.ActivationContext)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13028 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___activationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.ActivationContext,System.String[])
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13029 (Object_t * __this /* static, unused */, ActivationContext_t1_717 * ___activationContext, StringU5BU5D_t1_238* ___activationCustomData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstanceFrom(System.AppDomain,System.String,System.String)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstanceFrom_m1_13030 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, String_t* ___assemblyFile, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstanceFrom(System.AppDomain,System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstanceFrom_m1_13031 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, String_t* ___assemblyFile, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.AppDomain,System.String,System.String)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13032 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Activator::CreateInstance(System.AppDomain,System.String,System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[],System.Security.Policy.Evidence)
extern "C" ObjectHandle_t1_1019 * Activator_CreateInstance_m1_13033 (Object_t * __this /* static, unused */, AppDomain_t1_1403 * ___domain, String_t* ___assemblyName, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, Evidence_t1_398 * ___securityAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type)
extern "C" Object_t * Activator_CreateInstance_m1_13034 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Object[])
extern "C" Object_t * Activator_CreateInstance_m1_13035 (Object_t * __this /* static, unused */, Type_t * ___type, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Object[],System.Object[])
extern "C" Object_t * Activator_CreateInstance_m1_13036 (Object_t * __this /* static, unused */, Type_t * ___type, ObjectU5BU5D_t1_272* ___args, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * Activator_CreateInstance_m1_13037 (Object_t * __this /* static, unused */, Type_t * ___type, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[])
extern "C" Object_t * Activator_CreateInstance_m1_13038 (Object_t * __this /* static, unused */, Type_t * ___type, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstance(System.Type,System.Boolean)
extern "C" Object_t * Activator_CreateInstance_m1_13039 (Object_t * __this /* static, unused */, Type_t * ___type, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Activator::CheckType(System.Type)
extern "C" void Activator_CheckType_m1_13040 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Activator::CheckAbstractType(System.Type)
extern "C" void Activator_CheckAbstractType_m1_13041 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::GetObject(System.Type,System.String)
extern "C" Object_t * Activator_GetObject_m1_13042 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::GetObject(System.Type,System.String,System.Object)
extern "C" Object_t * Activator_GetObject_m1_13043 (Object_t * __this /* static, unused */, Type_t * ___type, String_t* ___url, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Activator::CreateInstanceInternal(System.Type)
extern "C" Object_t * Activator_CreateInstanceInternal_m1_13044 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
