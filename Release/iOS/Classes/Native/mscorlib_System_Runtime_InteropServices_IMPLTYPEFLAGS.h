﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_IMPLTYPEFLAGS.h"

// System.Runtime.InteropServices.IMPLTYPEFLAGS
struct  IMPLTYPEFLAGS_t1_801 
{
	// System.Int32 System.Runtime.InteropServices.IMPLTYPEFLAGS::value__
	int32_t ___value___1;
};
