﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCHebrewCalendar
struct CCHebrewCalendar_t1_348;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.CCHebrewCalendar::.ctor()
extern "C" void CCHebrewCalendar__ctor_m1_3750 (CCHebrewCalendar_t1_348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCHebrewCalendar::is_leap_year(System.Int32)
extern "C" bool CCHebrewCalendar_is_leap_year_m1_3751 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::last_month_of_year(System.Int32)
extern "C" int32_t CCHebrewCalendar_last_month_of_year_m1_3752 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::elapsed_days(System.Int32)
extern "C" int32_t CCHebrewCalendar_elapsed_days_m1_3753 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::new_year_delay(System.Int32)
extern "C" int32_t CCHebrewCalendar_new_year_delay_m1_3754 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::last_day_of_month(System.Int32,System.Int32)
extern "C" int32_t CCHebrewCalendar_last_day_of_month_m1_3755 (Object_t * __this /* static, unused */, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCHebrewCalendar::long_heshvan(System.Int32)
extern "C" bool CCHebrewCalendar_long_heshvan_m1_3756 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCHebrewCalendar::short_kislev(System.Int32)
extern "C" bool CCHebrewCalendar_short_kislev_m1_3757 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::days_in_year(System.Int32)
extern "C" int32_t CCHebrewCalendar_days_in_year_m1_3758 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::fixed_from_dmy(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHebrewCalendar_fixed_from_dmy_m1_3759 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::year_from_fixed(System.Int32)
extern "C" int32_t CCHebrewCalendar_year_from_fixed_m1_3760 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCHebrewCalendar::my_from_fixed(System.Int32&,System.Int32&,System.Int32)
extern "C" void CCHebrewCalendar_my_from_fixed_m1_3761 (Object_t * __this /* static, unused */, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCHebrewCalendar::dmy_from_fixed(System.Int32&,System.Int32&,System.Int32&,System.Int32)
extern "C" void CCHebrewCalendar_dmy_from_fixed_m1_3762 (Object_t * __this /* static, unused */, int32_t* ___day, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::month_from_fixed(System.Int32)
extern "C" int32_t CCHebrewCalendar_month_from_fixed_m1_3763 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::day_from_fixed(System.Int32)
extern "C" int32_t CCHebrewCalendar_day_from_fixed_m1_3764 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::date_difference(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHebrewCalendar_date_difference_m1_3765 (Object_t * __this /* static, unused */, int32_t ___dayA, int32_t ___monthA, int32_t ___yearA, int32_t ___dayB, int32_t ___monthB, int32_t ___yearB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::day_number(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHebrewCalendar_day_number_m1_3766 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCHebrewCalendar::days_remaining(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCHebrewCalendar_days_remaining_m1_3767 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
