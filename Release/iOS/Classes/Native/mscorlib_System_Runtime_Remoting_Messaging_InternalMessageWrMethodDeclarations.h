﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.InternalMessageWrapper
struct InternalMessageWrapper_t1_940;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.InternalMessageWrapper::.ctor(System.Runtime.Remoting.Messaging.IMessage)
extern "C" void InternalMessageWrapper__ctor_m1_8399 (InternalMessageWrapper_t1_940 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
