﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1_2704;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Collections.Generic.IDictionary`2<System.Object,System.Single>
struct IDictionary_2_t1_2890;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.Generic.ICollection`1<System.Single>
struct ICollection_1_t1_2891;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>[]
struct KeyValuePair_2U5BU5D_t1_2889;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>
struct IEnumerator_1_t1_2892;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Single>
struct KeyCollection_t1_2708;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Single>
struct ValueCollection_t1_2712;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor()
extern "C" void Dictionary_2__ctor_m1_26901_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26901(__this, method) (( void (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2__ctor_m1_26901_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26903_gshared (Dictionary_2_t1_2704 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26903(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26903_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_26905_gshared (Dictionary_2_t1_2704 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26905(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26905_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_26907_gshared (Dictionary_2_t1_2704 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26907(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_2704 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_26907_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26909_gshared (Dictionary_2_t1_2704 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26909(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26909_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26911_gshared (Dictionary_2_t1_2704 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26911(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_26911_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_26913_gshared (Dictionary_2_t1_2704 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_26913(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2704 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_26913_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26915_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26915(__this, method) (( Object_t* (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26915_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26917_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26917(__this, method) (( Object_t* (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26917_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26919_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26919(__this, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26919_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_26921_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_26921(__this, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_26921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26923_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26923(__this, method) (( bool (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26923_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26925_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26925(__this, method) (( bool (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26925_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_26927_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_26927(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_26927_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_26929_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_26929(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_26929_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_26931_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_26931(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_26931_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_26933_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_26933(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_26933_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_26935_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_26935(__this, ___key, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_26935_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26937_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26937(__this, method) (( bool (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26937_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26939_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26939(__this, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26939_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26941_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26941(__this, method) (( bool (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26941_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26943_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26943(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2_t1_2706 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26943_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26945_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26945(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2704 *, KeyValuePair_2_t1_2706 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26945_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26947_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2U5BU5D_t1_2889* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26947(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26947_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26949_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26949(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_2704 *, KeyValuePair_2_t1_2706 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26949_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_26951_gshared (Dictionary_2_t1_2704 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_26951(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_26951_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26953_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26953(__this, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26953_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26955_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26955(__this, method) (( Object_t* (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26955_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Single>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26957_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26957(__this, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26957_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_26959_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_26959(__this, method) (( int32_t (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_get_Count_m1_26959_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Item(TKey)
extern "C" float Dictionary_2_get_Item_m1_26961_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_26961(__this, ___key, method) (( float (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m1_26961_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_26963_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, float ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_26963(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t *, float, const MethodInfo*))Dictionary_2_set_Item_m1_26963_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_26965_gshared (Dictionary_2_t1_2704 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_26965(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_2704 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_26965_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_26967_gshared (Dictionary_2_t1_2704 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_26967(__this, ___size, method) (( void (*) (Dictionary_2_t1_2704 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_26967_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_26969_gshared (Dictionary_2_t1_2704 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_26969(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2704 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_26969_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2706  Dictionary_2_make_pair_m1_26971_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_26971(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_2706  (*) (Object_t * /* static, unused */, Object_t *, float, const MethodInfo*))Dictionary_2_make_pair_m1_26971_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Single>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m1_26973_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_26973(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, float, const MethodInfo*))Dictionary_2_pick_key_m1_26973_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::pick_value(TKey,TValue)
extern "C" float Dictionary_2_pick_value_m1_26975_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_26975(__this /* static, unused */, ___key, ___value, method) (( float (*) (Object_t * /* static, unused */, Object_t *, float, const MethodInfo*))Dictionary_2_pick_value_m1_26975_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_26977_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2U5BU5D_t1_2889* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_26977(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_2704 *, KeyValuePair_2U5BU5D_t1_2889*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_26977_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Resize()
extern "C" void Dictionary_2_Resize_m1_26979_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_26979(__this, method) (( void (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_Resize_m1_26979_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_26981_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, float ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_26981(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t *, float, const MethodInfo*))Dictionary_2_Add_m1_26981_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_26983_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_26983(__this, method) (( Object_t* (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_26983_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Clear()
extern "C" void Dictionary_2_Clear_m1_26985_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_26985(__this, method) (( void (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_Clear_m1_26985_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_26987_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_26987(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m1_26987_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_26989_gshared (Dictionary_2_t1_2704 * __this, float ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_26989(__this, ___value, method) (( bool (*) (Dictionary_2_t1_2704 *, float, const MethodInfo*))Dictionary_2_ContainsValue_m1_26989_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_26991_gshared (Dictionary_2_t1_2704 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_26991(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_2704 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_26991_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Single>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_26993_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_26993(__this, ___sender, method) (( void (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_26993_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_26995_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_26995(__this, ___key, method) (( bool (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m1_26995_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_26997_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, float* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_26997(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_2704 *, Object_t *, float*, const MethodInfo*))Dictionary_2_TryGetValue_m1_26997_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Keys()
extern "C" KeyCollection_t1_2708 * Dictionary_2_get_Keys_m1_26999_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_26999(__this, method) (( KeyCollection_t1_2708 * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_get_Keys_m1_26999_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::get_Values()
extern "C" ValueCollection_t1_2712 * Dictionary_2_get_Values_m1_27001_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_27001(__this, method) (( ValueCollection_t1_2712 * (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_get_Values_m1_27001_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m1_27003_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_27003(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_27003_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ToTValue(System.Object)
extern "C" float Dictionary_2_ToTValue_m1_27005_gshared (Dictionary_2_t1_2704 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_27005(__this, ___value, method) (( float (*) (Dictionary_2_t1_2704 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_27005_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Single>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_27007_gshared (Dictionary_2_t1_2704 * __this, KeyValuePair_2_t1_2706  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_27007(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_2704 *, KeyValuePair_2_t1_2706 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_27007_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Single>::GetEnumerator()
extern "C" Enumerator_t1_2710  Dictionary_2_GetEnumerator_m1_27009_gshared (Dictionary_2_t1_2704 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_27009(__this, method) (( Enumerator_t1_2710  (*) (Dictionary_2_t1_2704 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_27009_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Single>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_27011_gshared (Object_t * __this /* static, unused */, Object_t * ___key, float ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_27011(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, Object_t *, float, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_27011_gshared)(__this /* static, unused */, ___key, ___value, method)
