﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID_ePla.h"

// VoxelBusters.NativePlugins.PlatformID
struct  PlatformID_t8_331  : public Object_t
{
	// VoxelBusters.NativePlugins.PlatformID/ePlatform VoxelBusters.NativePlugins.PlatformID::m_platform
	int32_t ___m_platform_0;
	// System.String VoxelBusters.NativePlugins.PlatformID::m_value
	String_t* ___m_value_1;
};
