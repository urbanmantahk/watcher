﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520
struct X520_t1_218;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520::.ctor()
extern "C" void X520__ctor_m1_2418 (X520_t1_218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
