﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_eJSONToken.h"

// VoxelBusters.Utility.eJSONToken
struct  eJSONToken_t8_54 
{
	// System.Int32 VoxelBusters.Utility.eJSONToken::value__
	int32_t ___value___1;
};
