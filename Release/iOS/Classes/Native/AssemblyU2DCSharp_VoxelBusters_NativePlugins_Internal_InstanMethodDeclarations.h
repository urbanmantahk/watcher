﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.InstanceIDGenerator
struct InstanceIDGenerator_t8_324;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.InstanceIDGenerator::.ctor()
extern "C" void InstanceIDGenerator__ctor_m8_1900 (InstanceIDGenerator_t8_324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Internal.InstanceIDGenerator::Create()
extern "C" String_t* InstanceIDGenerator_Create_m8_1901 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
