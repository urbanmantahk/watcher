﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t3_253;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m3_1861_gshared (Enumerator_t3_254 * __this, Queue_1_t3_253 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m3_1861(__this, ___q, method) (( void (*) (Enumerator_t3_254 *, Queue_1_t3_253 *, const MethodInfo*))Enumerator__ctor_m3_1861_gshared)(__this, ___q, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m3_1862_gshared (Enumerator_t3_254 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3_1862(__this, method) (( void (*) (Enumerator_t3_254 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3_1862_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m3_1863_gshared (Enumerator_t3_254 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3_1863(__this, method) (( Object_t * (*) (Enumerator_t3_254 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3_1863_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m3_1864_gshared (Enumerator_t3_254 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3_1864(__this, method) (( void (*) (Enumerator_t3_254 *, const MethodInfo*))Enumerator_Dispose_m3_1864_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m3_1865_gshared (Enumerator_t3_254 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3_1865(__this, method) (( bool (*) (Enumerator_t3_254 *, const MethodInfo*))Enumerator_MoveNext_m3_1865_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m3_1866_gshared (Enumerator_t3_254 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3_1866(__this, method) (( Object_t * (*) (Enumerator_t3_254 *, const MethodInfo*))Enumerator_get_Current_m3_1866_gshared)(__this, method)
