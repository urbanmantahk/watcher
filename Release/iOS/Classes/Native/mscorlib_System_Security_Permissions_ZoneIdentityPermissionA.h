﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Security.Permissions.ZoneIdentityPermissionAttribute
struct  ZoneIdentityPermissionAttribute_t1_1324  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.SecurityZone System.Security.Permissions.ZoneIdentityPermissionAttribute::zone
	int32_t ___zone_2;
};
