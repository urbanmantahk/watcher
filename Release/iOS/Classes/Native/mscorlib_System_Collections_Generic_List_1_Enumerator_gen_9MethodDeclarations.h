﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1_15415(__this, ___l, method) (( void (*) (Enumerator_t1_1952 *, List_1_t1_1742 *, const MethodInfo*))Enumerator__ctor_m1_15258_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_15416(__this, method) (( void (*) (Enumerator_t1_1952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_15259_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_15417(__this, method) (( Object_t * (*) (Enumerator_t1_1952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_15260_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
#define Enumerator_Dispose_m1_15418(__this, method) (( void (*) (Enumerator_t1_1952 *, const MethodInfo*))Enumerator_Dispose_m1_15261_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::VerifyState()
#define Enumerator_VerifyState_m1_15419(__this, method) (( void (*) (Enumerator_t1_1952 *, const MethodInfo*))Enumerator_VerifyState_m1_15262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
#define Enumerator_MoveNext_m1_15420(__this, method) (( bool (*) (Enumerator_t1_1952 *, const MethodInfo*))Enumerator_MoveNext_m1_15263_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
#define Enumerator_get_Current_m1_15421(__this, method) (( String_t* (*) (Enumerator_t1_1952 *, const MethodInfo*))Enumerator_get_Current_m1_15264_gshared)(__this, method)
