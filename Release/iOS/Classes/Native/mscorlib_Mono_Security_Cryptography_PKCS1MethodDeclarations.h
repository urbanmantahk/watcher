﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Cryptography.PKCS1
struct PKCS1_t1_167;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Security.Cryptography.RSA
struct RSA_t1_175;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1_143;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Cryptography.PKCS1::.ctor()
extern "C" void PKCS1__ctor_m1_1994 (PKCS1_t1_167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.PKCS1::.cctor()
extern "C" void PKCS1__cctor_m1_1995 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.PKCS1::Compare(System.Byte[],System.Byte[])
extern "C" bool PKCS1_Compare_m1_1996 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___array1, ByteU5BU5D_t1_109* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::xor(System.Byte[],System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_xor_m1_1997 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___array1, ByteU5BU5D_t1_109* ___array2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::GetEmptyHash(System.Security.Cryptography.HashAlgorithm)
extern "C" ByteU5BU5D_t1_109* PKCS1_GetEmptyHash_m1_1998 (Object_t * __this /* static, unused */, HashAlgorithm_t1_162 * ___hash, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::I2OSP(System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* PKCS1_I2OSP_m1_1999 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::I2OSP(System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t1_109* PKCS1_I2OSP_m1_2000 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___x, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::OS2IP(System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_OS2IP_m1_2001 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::RSAEP(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_RSAEP_m1_2002 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, ByteU5BU5D_t1_109* ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::RSADP(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_RSADP_m1_2003 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, ByteU5BU5D_t1_109* ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::RSASP1(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_RSASP1_m1_2004 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, ByteU5BU5D_t1_109* ___m, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::RSAVP1(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_RSAVP1_m1_2005 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, ByteU5BU5D_t1_109* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Encrypt_OAEP(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Security.Cryptography.RandomNumberGenerator,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_Encrypt_OAEP_m1_2006 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, HashAlgorithm_t1_162 * ___hash, RandomNumberGenerator_t1_143 * ___rng, ByteU5BU5D_t1_109* ___M, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Decrypt_OAEP(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_Decrypt_OAEP_m1_2007 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___C, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Encrypt_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.RandomNumberGenerator,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_Encrypt_v15_m1_2008 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, RandomNumberGenerator_t1_143 * ___rng, ByteU5BU5D_t1_109* ___M, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Decrypt_v15(System.Security.Cryptography.RSA,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_Decrypt_v15_m1_2009 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, ByteU5BU5D_t1_109* ___C, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Sign_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[])
extern "C" ByteU5BU5D_t1_109* PKCS1_Sign_v15_m1_2010 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___hashValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.PKCS1::Verify_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Byte[])
extern "C" bool PKCS1_Verify_v15_m1_2011 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___hashValue, ByteU5BU5D_t1_109* ___signature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Cryptography.PKCS1::Verify_v15(System.Security.Cryptography.RSA,System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Byte[],System.Boolean)
extern "C" bool PKCS1_Verify_v15_m1_2012 (Object_t * __this /* static, unused */, RSA_t1_175 * ___rsa, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___hashValue, ByteU5BU5D_t1_109* ___signature, bool ___tryNonStandardEncoding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::Encode_v15(System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t1_109* PKCS1_Encode_v15_m1_2013 (Object_t * __this /* static, unused */, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___hashValue, int32_t ___emLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.PKCS1::MGF1(System.Security.Cryptography.HashAlgorithm,System.Byte[],System.Int32)
extern "C" ByteU5BU5D_t1_109* PKCS1_MGF1_m1_2014 (Object_t * __this /* static, unused */, HashAlgorithm_t1_162 * ___hash, ByteU5BU5D_t1_109* ___mgfSeed, int32_t ___maskLen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
