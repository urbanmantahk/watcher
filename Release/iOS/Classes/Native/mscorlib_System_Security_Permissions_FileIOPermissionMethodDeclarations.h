﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.FileIOPermission
struct FileIOPermission_t1_1274;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Object
struct Object_t;
// System.Collections.IList
struct IList_t1_262;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"
#include "mscorlib_System_Security_AccessControl_AccessControlActions.h"

// System.Void System.Security.Permissions.FileIOPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void FileIOPermission__ctor_m1_10822 (FileIOPermission_t1_1274 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::.ctor(System.Security.Permissions.FileIOPermissionAccess,System.String)
extern "C" void FileIOPermission__ctor_m1_10823 (FileIOPermission_t1_1274 * __this, int32_t ___access, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::.ctor(System.Security.Permissions.FileIOPermissionAccess,System.String[])
extern "C" void FileIOPermission__ctor_m1_10824 (FileIOPermission_t1_1274 * __this, int32_t ___access, StringU5BU5D_t1_238* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::.ctor(System.Security.Permissions.FileIOPermissionAccess,System.Security.AccessControl.AccessControlActions,System.String)
extern "C" void FileIOPermission__ctor_m1_10825 (FileIOPermission_t1_1274 * __this, int32_t ___access, int32_t ___control, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::.ctor(System.Security.Permissions.FileIOPermissionAccess,System.Security.AccessControl.AccessControlActions,System.String[])
extern "C" void FileIOPermission__ctor_m1_10826 (FileIOPermission_t1_1274 * __this, int32_t ___access, int32_t ___control, StringU5BU5D_t1_238* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::.cctor()
extern "C" void FileIOPermission__cctor_m1_10827 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.FileIOPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t FileIOPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_10828 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::CreateLists()
extern "C" void FileIOPermission_CreateLists_m1_10829 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermission::get_AllFiles()
extern "C" int32_t FileIOPermission_get_AllFiles_m1_10830 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::set_AllFiles(System.Security.Permissions.FileIOPermissionAccess)
extern "C" void FileIOPermission_set_AllFiles_m1_10831 (FileIOPermission_t1_1274 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermission::get_AllLocalFiles()
extern "C" int32_t FileIOPermission_get_AllLocalFiles_m1_10832 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::set_AllLocalFiles(System.Security.Permissions.FileIOPermissionAccess)
extern "C" void FileIOPermission_set_AllLocalFiles_m1_10833 (FileIOPermission_t1_1274 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::AddPathList(System.Security.Permissions.FileIOPermissionAccess,System.String)
extern "C" void FileIOPermission_AddPathList_m1_10834 (FileIOPermission_t1_1274 * __this, int32_t ___access, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::AddPathList(System.Security.Permissions.FileIOPermissionAccess,System.String[])
extern "C" void FileIOPermission_AddPathList_m1_10835 (FileIOPermission_t1_1274 * __this, int32_t ___access, StringU5BU5D_t1_238* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::AddPathInternal(System.Security.Permissions.FileIOPermissionAccess,System.String)
extern "C" void FileIOPermission_AddPathInternal_m1_10836 (FileIOPermission_t1_1274 * __this, int32_t ___access, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileIOPermission::Copy()
extern "C" Object_t * FileIOPermission_Copy_m1_10837 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::FromXml(System.Security.SecurityElement)
extern "C" void FileIOPermission_FromXml_m1_10838 (FileIOPermission_t1_1274 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Security.Permissions.FileIOPermission::GetPathList(System.Security.Permissions.FileIOPermissionAccess)
extern "C" StringU5BU5D_t1_238* FileIOPermission_GetPathList_m1_10839 (FileIOPermission_t1_1274 * __this, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileIOPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * FileIOPermission_Intersect_m1_10840 (FileIOPermission_t1_1274 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileIOPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool FileIOPermission_IsSubsetOf_m1_10841 (FileIOPermission_t1_1274 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileIOPermission::IsUnrestricted()
extern "C" bool FileIOPermission_IsUnrestricted_m1_10842 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::SetPathList(System.Security.Permissions.FileIOPermissionAccess,System.String)
extern "C" void FileIOPermission_SetPathList_m1_10843 (FileIOPermission_t1_1274 * __this, int32_t ___access, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::SetPathList(System.Security.Permissions.FileIOPermissionAccess,System.String[])
extern "C" void FileIOPermission_SetPathList_m1_10844 (FileIOPermission_t1_1274 * __this, int32_t ___access, StringU5BU5D_t1_238* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.FileIOPermission::ToXml()
extern "C" SecurityElement_t1_242 * FileIOPermission_ToXml_m1_10845 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileIOPermission::Union(System.Security.IPermission)
extern "C" Object_t * FileIOPermission_Union_m1_10846 (FileIOPermission_t1_1274 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileIOPermission::Equals(System.Object)
extern "C" bool FileIOPermission_Equals_m1_10847 (FileIOPermission_t1_1274 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.FileIOPermission::GetHashCode()
extern "C" int32_t FileIOPermission_GetHashCode_m1_10848 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileIOPermission::IsEmpty()
extern "C" bool FileIOPermission_IsEmpty_m1_10849 (FileIOPermission_t1_1274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileIOPermission System.Security.Permissions.FileIOPermission::Cast(System.Security.IPermission)
extern "C" FileIOPermission_t1_1274 * FileIOPermission_Cast_m1_10850 (Object_t * __this /* static, unused */, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::ThrowInvalidFlag(System.Security.Permissions.FileIOPermissionAccess,System.Boolean)
extern "C" void FileIOPermission_ThrowInvalidFlag_m1_10851 (Object_t * __this /* static, unused */, int32_t ___access, bool ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::ThrowIfInvalidPath(System.String)
extern "C" void FileIOPermission_ThrowIfInvalidPath_m1_10852 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::ThrowIfInvalidPath(System.String[])
extern "C" void FileIOPermission_ThrowIfInvalidPath_m1_10853 (Object_t * __this /* static, unused */, StringU5BU5D_t1_238* ___paths, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::Clear(System.Security.Permissions.FileIOPermissionAccess)
extern "C" void FileIOPermission_Clear_m1_10854 (FileIOPermission_t1_1274 * __this, int32_t ___access, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileIOPermission::KeyIsSubsetOf(System.Collections.IList,System.Collections.IList)
extern "C" bool FileIOPermission_KeyIsSubsetOf_m1_10855 (Object_t * __this /* static, unused */, Object_t * ___local, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::UnionKeys(System.Collections.IList,System.String[])
extern "C" void FileIOPermission_UnionKeys_m1_10856 (Object_t * __this /* static, unused */, Object_t * ___list, StringU5BU5D_t1_238* ___paths, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermission::IntersectKeys(System.Collections.IList,System.Collections.IList,System.Collections.IList)
extern "C" void FileIOPermission_IntersectKeys_m1_10857 (Object_t * __this /* static, unused */, Object_t * ___local, Object_t * ___target, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
