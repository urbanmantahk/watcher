﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.FileIOPermissionAttribute
struct FileIOPermissionAttribute_t1_1276;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Security_Permissions_FileIOPermissionAccess.h"

// System.Void System.Security.Permissions.FileIOPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void FileIOPermissionAttribute__ctor_m1_10858 (FileIOPermissionAttribute_t1_1276 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_All()
extern "C" String_t* FileIOPermissionAttribute_get_All_m1_10859 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_All(System.String)
extern "C" void FileIOPermissionAttribute_set_All_m1_10860 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_Append()
extern "C" String_t* FileIOPermissionAttribute_get_Append_m1_10861 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_Append(System.String)
extern "C" void FileIOPermissionAttribute_set_Append_m1_10862 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_PathDiscovery()
extern "C" String_t* FileIOPermissionAttribute_get_PathDiscovery_m1_10863 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_PathDiscovery(System.String)
extern "C" void FileIOPermissionAttribute_set_PathDiscovery_m1_10864 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_Read()
extern "C" String_t* FileIOPermissionAttribute_get_Read_m1_10865 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_Read(System.String)
extern "C" void FileIOPermissionAttribute_set_Read_m1_10866 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_Write()
extern "C" String_t* FileIOPermissionAttribute_get_Write_m1_10867 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_Write(System.String)
extern "C" void FileIOPermissionAttribute_set_Write_m1_10868 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermissionAttribute::get_AllFiles()
extern "C" int32_t FileIOPermissionAttribute_get_AllFiles_m1_10869 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_AllFiles(System.Security.Permissions.FileIOPermissionAccess)
extern "C" void FileIOPermissionAttribute_set_AllFiles_m1_10870 (FileIOPermissionAttribute_t1_1276 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.FileIOPermissionAccess System.Security.Permissions.FileIOPermissionAttribute::get_AllLocalFiles()
extern "C" int32_t FileIOPermissionAttribute_get_AllLocalFiles_m1_10871 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_AllLocalFiles(System.Security.Permissions.FileIOPermissionAccess)
extern "C" void FileIOPermissionAttribute_set_AllLocalFiles_m1_10872 (FileIOPermissionAttribute_t1_1276 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_ChangeAccessControl()
extern "C" String_t* FileIOPermissionAttribute_get_ChangeAccessControl_m1_10873 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_ChangeAccessControl(System.String)
extern "C" void FileIOPermissionAttribute_set_ChangeAccessControl_m1_10874 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_ViewAccessControl()
extern "C" String_t* FileIOPermissionAttribute_get_ViewAccessControl_m1_10875 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_ViewAccessControl(System.String)
extern "C" void FileIOPermissionAttribute_set_ViewAccessControl_m1_10876 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.FileIOPermissionAttribute::get_ViewAndModify()
extern "C" String_t* FileIOPermissionAttribute_get_ViewAndModify_m1_10877 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileIOPermissionAttribute::set_ViewAndModify(System.String)
extern "C" void FileIOPermissionAttribute_set_ViewAndModify_m1_10878 (FileIOPermissionAttribute_t1_1276 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileIOPermissionAttribute::CreatePermission()
extern "C" Object_t * FileIOPermissionAttribute_CreatePermission_m1_10879 (FileIOPermissionAttribute_t1_1276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
