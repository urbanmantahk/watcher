﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_1830;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1_2383;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Object>
struct IDictionary_2_t1_2826;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t1_2771;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2825;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t1_2827;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t1_2387;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t1_2391;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m1_14966_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_14966(__this, method) (( void (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2__ctor_m1_14966_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_20815_gshared (Dictionary_2_t1_1830 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_20815(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_20815_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_20817_gshared (Dictionary_2_t1_1830 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_20817(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_20817_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_20819_gshared (Dictionary_2_t1_1830 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_20819(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_1830 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_20819_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_20821_gshared (Dictionary_2_t1_1830 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_20821(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_20821_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_20823_gshared (Dictionary_2_t1_1830 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_20823(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_1830 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_20823_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_20825_gshared (Dictionary_2_t1_1830 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_20825(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_1830 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_20825_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_20827_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_20827(__this, method) (( Object_t* (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_20827_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_20829_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_20829(__this, method) (( Object_t* (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_20829_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_20831_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_20831(__this, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_20831_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_20833_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_20833(__this, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_20833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_20835_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_20835(__this, method) (( bool (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_20835_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_20837_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_20837(__this, method) (( bool (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_20837_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_20839_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_20839(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_20839_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_20841_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_20841(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_20841_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_20843_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_20843(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_20843_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_20845_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_20845(__this, ___key, method) (( bool (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_20845_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_20847_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_20847(__this, ___key, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_20847_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_20849_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_20849(__this, method) (( bool (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_20849_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_20851_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_20851(__this, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_20851_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_20853_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_20853(__this, method) (( bool (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_20853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_20855_gshared (Dictionary_2_t1_1830 * __this, KeyValuePair_2_t1_2385  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_20855(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_1830 *, KeyValuePair_2_t1_2385 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_20855_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_20857_gshared (Dictionary_2_t1_1830 * __this, KeyValuePair_2_t1_2385  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_20857(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_1830 *, KeyValuePair_2_t1_2385 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_20857_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_20859_gshared (Dictionary_2_t1_1830 * __this, KeyValuePair_2U5BU5D_t1_2825* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_20859(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1830 *, KeyValuePair_2U5BU5D_t1_2825*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_20859_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_20861_gshared (Dictionary_2_t1_1830 * __this, KeyValuePair_2_t1_2385  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_20861(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_1830 *, KeyValuePair_2_t1_2385 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_20861_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_20863_gshared (Dictionary_2_t1_1830 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_20863(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1830 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_20863_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_20865_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_20865(__this, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_20865_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_20867_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_20867(__this, method) (( Object_t* (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_20867_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_20869_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_20869(__this, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_20869_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_20871_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_20871(__this, method) (( int32_t (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_get_Count_m1_20871_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m1_20873_gshared (Dictionary_2_t1_1830 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_20873(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1_20873_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_20875_gshared (Dictionary_2_t1_1830 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_20875(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1830 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m1_20875_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_20877_gshared (Dictionary_2_t1_1830 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_20877(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_1830 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_20877_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_20879_gshared (Dictionary_2_t1_1830 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_20879(__this, ___size, method) (( void (*) (Dictionary_2_t1_1830 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_20879_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_20881_gshared (Dictionary_2_t1_1830 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_20881(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1830 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_20881_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2385  Dictionary_2_make_pair_m1_20883_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_20883(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_2385  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m1_20883_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m1_20885_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_20885(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m1_20885_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m1_20887_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_20887(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m1_20887_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_20889_gshared (Dictionary_2_t1_1830 * __this, KeyValuePair_2U5BU5D_t1_2825* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_20889(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1830 *, KeyValuePair_2U5BU5D_t1_2825*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_20889_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m1_20891_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_20891(__this, method) (( void (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_Resize_m1_20891_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_20893_gshared (Dictionary_2_t1_1830 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_20893(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1830 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m1_20893_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_20895_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_20895(__this, method) (( Object_t* (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_20895_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m1_20897_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_20897(__this, method) (( void (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_Clear_m1_20897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_20899_gshared (Dictionary_2_t1_1830 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_20899(__this, ___key, method) (( bool (*) (Dictionary_2_t1_1830 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1_20899_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_20901_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_20901(__this, ___value, method) (( bool (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m1_20901_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_20903_gshared (Dictionary_2_t1_1830 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_20903(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_1830 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_20903_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_20905_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_20905(__this, ___sender, method) (( void (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_20905_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_20907_gshared (Dictionary_2_t1_1830 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_20907(__this, ___key, method) (( bool (*) (Dictionary_2_t1_1830 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1_20907_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_20909_gshared (Dictionary_2_t1_1830 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_20909(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_1830 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m1_20909_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t1_2387 * Dictionary_2_get_Keys_m1_20911_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_20911(__this, method) (( KeyCollection_t1_2387 * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_get_Keys_m1_20911_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t1_2391 * Dictionary_2_get_Values_m1_20913_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_20913(__this, method) (( ValueCollection_t1_2391 * (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_get_Values_m1_20913_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m1_20915_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_20915(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_20915_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m1_20917_gshared (Dictionary_2_t1_1830 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_20917(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1_1830 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_20917_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_20919_gshared (Dictionary_2_t1_1830 * __this, KeyValuePair_2_t1_2385  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_20919(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_1830 *, KeyValuePair_2_t1_2385 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_20919_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2389  Dictionary_2_GetEnumerator_m1_20921_gshared (Dictionary_2_t1_1830 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_20921(__this, method) (( Enumerator_t1_2389  (*) (Dictionary_2_t1_1830 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_20921_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_20923_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_20923(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_20923_gshared)(__this /* static, unused */, ___key, ___value, method)
