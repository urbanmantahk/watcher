﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_ShareI.h"

// VoxelBusters.NativePlugins.ShareSheet
struct  ShareSheet_t8_289  : public ShareImageUtility_t8_280
{
	// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.ShareSheet::m_excludedShareOptions
	eShareOptionsU5BU5D_t8_186* ___m_excludedShareOptions_3;
	// System.String VoxelBusters.NativePlugins.ShareSheet::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_4;
	// System.String VoxelBusters.NativePlugins.ShareSheet::<URL>k__BackingField
	String_t* ___U3CURLU3Ek__BackingField_5;
	// System.Byte[] VoxelBusters.NativePlugins.ShareSheet::<ImageData>k__BackingField
	ByteU5BU5D_t1_109* ___U3CImageDataU3Ek__BackingField_6;
};
