﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Encoding
struct Encoding_t1_406;
// System.String
struct String_t;
// System.Text.DecoderFallback
struct DecoderFallback_t1_1420;
// System.Text.EncoderFallback
struct EncoderFallback_t1_1419;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Text.Decoder
struct Decoder_t1_407;
// System.Text.Encoder
struct Encoder_t1_1428;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Text.EncodingInfo[]
struct EncodingInfoU5BU5D_t1_1437;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_NormalizationForm.h"

// System.Void System.Text.Encoding::.ctor()
extern "C" void Encoding__ctor_m1_12309 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoding::.ctor(System.Int32)
extern "C" void Encoding__ctor_m1_12310 (Encoding_t1_406 * __this, int32_t ___codePage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoding::.cctor()
extern "C" void Encoding__cctor_m1_12311 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::_(System.String)
extern "C" String_t* Encoding___m1_12312 (Object_t * __this /* static, unused */, String_t* ___arg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::get_IsReadOnly()
extern "C" bool Encoding_get_IsReadOnly_m1_12313 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::get_IsSingleByte()
extern "C" bool Encoding_get_IsSingleByte_m1_12314 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.Encoding::get_DecoderFallback()
extern "C" DecoderFallback_t1_1420 * Encoding_get_DecoderFallback_m1_12315 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoding::set_DecoderFallback(System.Text.DecoderFallback)
extern "C" void Encoding_set_DecoderFallback_m1_12316 (Encoding_t1_406 * __this, DecoderFallback_t1_1420 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.Encoding::get_EncoderFallback()
extern "C" EncoderFallback_t1_1419 * Encoding_get_EncoderFallback_m1_12317 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoding::set_EncoderFallback(System.Text.EncoderFallback)
extern "C" void Encoding_set_EncoderFallback_m1_12318 (Encoding_t1_406 * __this, EncoderFallback_t1_1419 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoding::SetFallbackInternal(System.Text.EncoderFallback,System.Text.DecoderFallback)
extern "C" void Encoding_SetFallbackInternal_m1_12319 (Encoding_t1_406 * __this, EncoderFallback_t1_1419 * ___e, DecoderFallback_t1_1420 * ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.Encoding::Convert(System.Text.Encoding,System.Text.Encoding,System.Byte[])
extern "C" ByteU5BU5D_t1_109* Encoding_Convert_m1_12320 (Object_t * __this /* static, unused */, Encoding_t1_406 * ___srcEncoding, Encoding_t1_406 * ___dstEncoding, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.Encoding::Convert(System.Text.Encoding,System.Text.Encoding,System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* Encoding_Convert_m1_12321 (Object_t * __this /* static, unused */, Encoding_t1_406 * ___srcEncoding, Encoding_t1_406 * ___dstEncoding, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::Equals(System.Object)
extern "C" bool Encoding_Equals_m1_12322 (Encoding_t1_406 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetByteCount(System.String)
extern "C" int32_t Encoding_GetByteCount_m1_12323 (Encoding_t1_406 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetByteCount(System.Char[])
extern "C" int32_t Encoding_GetByteCount_m1_12324 (Encoding_t1_406 * __this, CharU5BU5D_t1_16* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t Encoding_GetBytes_m1_12325 (Encoding_t1_406 * __this, String_t* ___s, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.Encoding::GetBytes(System.String)
extern "C" ByteU5BU5D_t1_109* Encoding_GetBytes_m1_12326 (Encoding_t1_406 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.Encoding::GetBytes(System.Char[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t1_109* Encoding_GetBytes_m1_12327 (Encoding_t1_406 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.Encoding::GetBytes(System.Char[])
extern "C" ByteU5BU5D_t1_109* Encoding_GetBytes_m1_12328 (Encoding_t1_406 * __this, CharU5BU5D_t1_16* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetCharCount(System.Byte[])
extern "C" int32_t Encoding_GetCharCount_m1_12329 (Encoding_t1_406 * __this, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.Text.Encoding::GetChars(System.Byte[],System.Int32,System.Int32)
extern "C" CharU5BU5D_t1_16* Encoding_GetChars_m1_12330 (Encoding_t1_406 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.Text.Encoding::GetChars(System.Byte[])
extern "C" CharU5BU5D_t1_16* Encoding_GetChars_m1_12331 (Encoding_t1_406 * __this, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Decoder System.Text.Encoding::GetDecoder()
extern "C" Decoder_t1_407 * Encoding_GetDecoder_m1_12332 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoder System.Text.Encoding::GetEncoder()
extern "C" Encoder_t1_1428 * Encoding_GetEncoder_m1_12333 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.Encoding::InvokeI18N(System.String,System.Object[])
extern "C" Object_t * Encoding_InvokeI18N_m1_12334 (Object_t * __this /* static, unused */, String_t* ___name, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::GetEncoding(System.Int32)
extern "C" Encoding_t1_406 * Encoding_GetEncoding_m1_12335 (Object_t * __this /* static, unused */, int32_t ___codepage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.Encoding::Clone()
extern "C" Object_t * Encoding_Clone_m1_12336 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::GetEncoding(System.Int32,System.Text.EncoderFallback,System.Text.DecoderFallback)
extern "C" Encoding_t1_406 * Encoding_GetEncoding_m1_12337 (Object_t * __this /* static, unused */, int32_t ___codepage, EncoderFallback_t1_1419 * ___encoderFallback, DecoderFallback_t1_1420 * ___decoderFallback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::GetEncoding(System.String,System.Text.EncoderFallback,System.Text.DecoderFallback)
extern "C" Encoding_t1_406 * Encoding_GetEncoding_m1_12338 (Object_t * __this /* static, unused */, String_t* ___name, EncoderFallback_t1_1419 * ___encoderFallback, DecoderFallback_t1_1420 * ___decoderFallback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncodingInfo[] System.Text.Encoding::GetEncodings()
extern "C" EncodingInfoU5BU5D_t1_1437* Encoding_GetEncodings_m1_12339 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::IsAlwaysNormalized()
extern "C" bool Encoding_IsAlwaysNormalized_m1_12340 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::IsAlwaysNormalized(System.Text.NormalizationForm)
extern "C" bool Encoding_IsAlwaysNormalized_m1_12341 (Encoding_t1_406 * __this, int32_t ___form, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::GetEncoding(System.String)
extern "C" Encoding_t1_406 * Encoding_GetEncoding_m1_12342 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetHashCode()
extern "C" int32_t Encoding_GetHashCode_m1_12343 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Text.Encoding::GetPreamble()
extern "C" ByteU5BU5D_t1_109* Encoding_GetPreamble_m1_12344 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* Encoding_GetString_m1_12345 (Encoding_t1_406 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::GetString(System.Byte[])
extern "C" String_t* Encoding_GetString_m1_12346 (Encoding_t1_406 * __this, ByteU5BU5D_t1_109* ___bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::get_BodyName()
extern "C" String_t* Encoding_get_BodyName_m1_12347 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::get_CodePage()
extern "C" int32_t Encoding_get_CodePage_m1_12348 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::get_EncodingName()
extern "C" String_t* Encoding_get_EncodingName_m1_12349 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::get_HeaderName()
extern "C" String_t* Encoding_get_HeaderName_m1_12350 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::get_IsBrowserDisplay()
extern "C" bool Encoding_get_IsBrowserDisplay_m1_12351 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::get_IsBrowserSave()
extern "C" bool Encoding_get_IsBrowserSave_m1_12352 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::get_IsMailNewsDisplay()
extern "C" bool Encoding_get_IsMailNewsDisplay_m1_12353 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.Encoding::get_IsMailNewsSave()
extern "C" bool Encoding_get_IsMailNewsSave_m1_12354 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::get_WebName()
extern "C" String_t* Encoding_get_WebName_m1_12355 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::get_WindowsCodePage()
extern "C" int32_t Encoding_get_WindowsCodePage_m1_12356 (Encoding_t1_406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_ASCII()
extern "C" Encoding_t1_406 * Encoding_get_ASCII_m1_12357 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_BigEndianUnicode()
extern "C" Encoding_t1_406 * Encoding_get_BigEndianUnicode_m1_12358 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.Encoding::InternalCodePage(System.Int32&)
extern "C" String_t* Encoding_InternalCodePage_m1_12359 (Object_t * __this /* static, unused */, int32_t* ___code_page, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_Default()
extern "C" Encoding_t1_406 * Encoding_get_Default_m1_12360 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_ISOLatin1()
extern "C" Encoding_t1_406 * Encoding_get_ISOLatin1_m1_12361 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF7()
extern "C" Encoding_t1_406 * Encoding_get_UTF7_m1_12362 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8()
extern "C" Encoding_t1_406 * Encoding_get_UTF8_m1_12363 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8Unmarked()
extern "C" Encoding_t1_406 * Encoding_get_UTF8Unmarked_m1_12364 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF8UnmarkedUnsafe()
extern "C" Encoding_t1_406 * Encoding_get_UTF8UnmarkedUnsafe_m1_12365 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_Unicode()
extern "C" Encoding_t1_406 * Encoding_get_Unicode_m1_12366 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_UTF32()
extern "C" Encoding_t1_406 * Encoding_get_UTF32_m1_12367 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_BigEndianUTF32()
extern "C" Encoding_t1_406 * Encoding_get_BigEndianUTF32_m1_12368 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetByteCount(System.Char*,System.Int32)
extern "C" int32_t Encoding_GetByteCount_m1_12369 (Encoding_t1_406 * __this, uint16_t* ___chars, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetCharCount(System.Byte*,System.Int32)
extern "C" int32_t Encoding_GetCharCount_m1_12370 (Encoding_t1_406 * __this, uint8_t* ___bytes, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetChars(System.Byte*,System.Int32,System.Char*,System.Int32)
extern "C" int32_t Encoding_GetChars_m1_12371 (Encoding_t1_406 * __this, uint8_t* ___bytes, int32_t ___byteCount, uint16_t* ___chars, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoding::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32)
extern "C" int32_t Encoding_GetBytes_m1_12372 (Encoding_t1_406 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
