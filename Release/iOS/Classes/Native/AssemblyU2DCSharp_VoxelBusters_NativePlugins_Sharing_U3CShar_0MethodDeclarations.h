﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10
struct U3CShareScreenShotU3Ec__AnonStorey10_t8_277;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::.ctor()
extern "C" void U3CShareScreenShotU3Ec__AnonStorey10__ctor_m8_1582 (U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenShot>c__AnonStorey10::<>m__11(UnityEngine.Texture2D)
extern "C" void U3CShareScreenShotU3Ec__AnonStorey10_U3CU3Em__11_m8_1583 (U3CShareScreenShotU3Ec__AnonStorey10_t8_277 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
