﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_GUIModalWindow.h"

// VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow
struct  DemoGUIWindow_t8_14  : public GUIModalWindow_t8_15
{
};
