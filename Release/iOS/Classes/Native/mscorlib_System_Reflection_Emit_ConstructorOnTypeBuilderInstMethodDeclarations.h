﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ConstructorOnTypeBuilderInst
struct ConstructorOnTypeBuilderInst_t1_484;
// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t1_477;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Type[]
struct TypeU5BU5D_t1_31;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Void System.Reflection.Emit.ConstructorOnTypeBuilderInst::.ctor(System.Reflection.MonoGenericClass,System.Reflection.Emit.ConstructorBuilder)
extern "C" void ConstructorOnTypeBuilderInst__ctor_m1_5563 (ConstructorOnTypeBuilderInst_t1_484 * __this, MonoGenericClass_t1_485 * ___instantiation, ConstructorBuilder_t1_477 * ___cb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_DeclaringType()
extern "C" Type_t * ConstructorOnTypeBuilderInst_get_DeclaringType_m1_5564 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_Name()
extern "C" String_t* ConstructorOnTypeBuilderInst_get_Name_m1_5565 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_ReflectedType()
extern "C" Type_t * ConstructorOnTypeBuilderInst_get_ReflectedType_m1_5566 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorOnTypeBuilderInst::IsDefined(System.Type,System.Boolean)
extern "C" bool ConstructorOnTypeBuilderInst_IsDefined_m1_5567 (ConstructorOnTypeBuilderInst_t1_484 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.ConstructorOnTypeBuilderInst::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* ConstructorOnTypeBuilderInst_GetCustomAttributes_m1_5568 (ConstructorOnTypeBuilderInst_t1_484 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.ConstructorOnTypeBuilderInst::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* ConstructorOnTypeBuilderInst_GetCustomAttributes_m1_5569 (ConstructorOnTypeBuilderInst_t1_484 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodImplAttributes System.Reflection.Emit.ConstructorOnTypeBuilderInst::GetMethodImplementationFlags()
extern "C" int32_t ConstructorOnTypeBuilderInst_GetMethodImplementationFlags_m1_5570 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.ConstructorOnTypeBuilderInst::GetParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* ConstructorOnTypeBuilderInst_GetParameters_m1_5571 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_MetadataToken()
extern "C" int32_t ConstructorOnTypeBuilderInst_get_MetadataToken_m1_5572 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ConstructorOnTypeBuilderInst::GetParameterCount()
extern "C" int32_t ConstructorOnTypeBuilderInst_GetParameterCount_m1_5573 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.ConstructorOnTypeBuilderInst::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * ConstructorOnTypeBuilderInst_Invoke_m1_5574 (ConstructorOnTypeBuilderInst_t1_484 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_MethodHandle()
extern "C" RuntimeMethodHandle_t1_479  ConstructorOnTypeBuilderInst_get_MethodHandle_m1_5575 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodAttributes System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_Attributes()
extern "C" int32_t ConstructorOnTypeBuilderInst_get_Attributes_m1_5576 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CallingConventions System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_CallingConvention()
extern "C" int32_t ConstructorOnTypeBuilderInst_get_CallingConvention_m1_5577 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.ConstructorOnTypeBuilderInst::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* ConstructorOnTypeBuilderInst_GetGenericArguments_m1_5578 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_ContainsGenericParameters()
extern "C" bool ConstructorOnTypeBuilderInst_get_ContainsGenericParameters_m1_5579 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_IsGenericMethodDefinition()
extern "C" bool ConstructorOnTypeBuilderInst_get_IsGenericMethodDefinition_m1_5580 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.ConstructorOnTypeBuilderInst::get_IsGenericMethod()
extern "C" bool ConstructorOnTypeBuilderInst_get_IsGenericMethod_m1_5581 (ConstructorOnTypeBuilderInst_t1_484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.ConstructorOnTypeBuilderInst::Invoke(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * ConstructorOnTypeBuilderInst_Invoke_m1_5582 (ConstructorOnTypeBuilderInst_t1_484 * __this, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
