﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.FileOperations
struct FileOperations_t8_48;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.IO.StreamWriter
struct StreamWriter_t1_448;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.FileOperations::.ctor()
extern "C" void FileOperations__ctor_m8_249 (FileOperations_t8_48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.FileOperations::Delete(System.String)
extern "C" void FileOperations_Delete_m8_250 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.FileOperations::Move(System.String,System.String)
extern "C" void FileOperations_Move_m8_251 (Object_t * __this /* static, unused */, String_t* ____sourcePath, String_t* ____destinationPath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.FileOperations::Exists(System.String)
extern "C" bool FileOperations_Exists_m8_252 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] VoxelBusters.Utility.FileOperations::ReadAllBytes(System.String)
extern "C" ByteU5BU5D_t1_109* FileOperations_ReadAllBytes_m8_253 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.FileOperations::WriteAllBytes(System.String,System.Byte[])
extern "C" void FileOperations_WriteAllBytes_m8_254 (Object_t * __this /* static, unused */, String_t* ____filePath, ByteU5BU5D_t1_109* ____bytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.StreamWriter VoxelBusters.Utility.FileOperations::CreateText(System.String)
extern "C" StreamWriter_t1_448 * FileOperations_CreateText_m8_255 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.FileOperations::ReadAllText(System.String)
extern "C" String_t* FileOperations_ReadAllText_m8_256 (Object_t * __this /* static, unused */, String_t* ____filePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.FileOperations::Rename(System.String,System.String)
extern "C" void FileOperations_Rename_m8_257 (Object_t * __this /* static, unused */, String_t* ____filePath, String_t* ____newFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
