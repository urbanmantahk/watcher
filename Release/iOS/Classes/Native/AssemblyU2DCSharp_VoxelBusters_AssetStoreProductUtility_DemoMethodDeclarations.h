﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow
struct DemoGUIWindow_t8_14;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow::.ctor()
extern "C" void DemoGUIWindow__ctor_m8_121 (DemoGUIWindow_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow::OnGUIWindow()
extern "C" void DemoGUIWindow_OnGUIWindow_m8_122 (DemoGUIWindow_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Demo.DemoGUIWindow::AdjustFontBasedOnScreen()
extern "C" void DemoGUIWindow_AdjustFontBasedOnScreen_m8_123 (DemoGUIWindow_t8_14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
