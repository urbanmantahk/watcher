﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t1_566;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
extern "C" void AssemblyConfigurationAttribute__ctor_m1_6672 (AssemblyConfigurationAttribute_t1_566 * __this, String_t* ___configuration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.AssemblyConfigurationAttribute::get_Configuration()
extern "C" String_t* AssemblyConfigurationAttribute_get_Configuration_m1_6673 (AssemblyConfigurationAttribute_t1_566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
