﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.EncoderFallback
struct EncoderFallback_t1_1419;
// System.Text.DecoderFallback
struct DecoderFallback_t1_1420;
// System.Text.Encoding
struct Encoding_t1_406;

#include "mscorlib_System_Object.h"

// System.Text.CodePageEncoding
struct  CodePageEncoding_t1_1418  : public Object_t
{
	// System.Int32 System.Text.CodePageEncoding::codePage
	int32_t ___codePage_0;
	// System.Boolean System.Text.CodePageEncoding::isReadOnly
	bool ___isReadOnly_1;
	// System.Text.EncoderFallback System.Text.CodePageEncoding::encoderFallback
	EncoderFallback_t1_1419 * ___encoderFallback_2;
	// System.Text.DecoderFallback System.Text.CodePageEncoding::decoderFallback
	DecoderFallback_t1_1420 * ___decoderFallback_3;
	// System.Text.Encoding System.Text.CodePageEncoding::realObject
	Encoding_t1_406 * ___realObject_4;
};
