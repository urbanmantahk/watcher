﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// VoxelBusters.NativePlugins.Sharing
struct Sharing_t8_273;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE
struct  U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275  : public Object_t
{
	// System.String VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::_subject
	String_t* ____subject_0;
	// System.String VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::_body
	String_t* ____body_1;
	// System.Boolean VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::_isHTMLBody
	bool ____isHTMLBody_2;
	// System.String[] VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::_recipients
	StringU5BU5D_t1_238* ____recipients_3;
	// VoxelBusters.NativePlugins.Sharing/SharingCompletion VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::_onCompletion
	SharingCompletion_t8_271 * ____onCompletion_4;
	// VoxelBusters.NativePlugins.Sharing VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::<>f__this
	Sharing_t8_273 * ___U3CU3Ef__this_5;
};
