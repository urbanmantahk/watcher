﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>
struct  ListKeys_t3_279  : public Object_t
{
	// System.Collections.Generic.SortedList`2<TKey,TValue> System.Collections.Generic.SortedList`2/ListKeys::host
	SortedList_2_t3_248 * ___host_0;
};
