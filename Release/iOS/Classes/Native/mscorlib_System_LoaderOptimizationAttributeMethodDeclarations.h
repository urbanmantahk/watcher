﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.LoaderOptimizationAttribute
struct LoaderOptimizationAttribute_t1_1563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_LoaderOptimization.h"

// System.Void System.LoaderOptimizationAttribute::.ctor(System.Byte)
extern "C" void LoaderOptimizationAttribute__ctor_m1_14181 (LoaderOptimizationAttribute_t1_1563 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.LoaderOptimizationAttribute::.ctor(System.LoaderOptimization)
extern "C" void LoaderOptimizationAttribute__ctor_m1_14182 (LoaderOptimizationAttribute_t1_1563 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.LoaderOptimization System.LoaderOptimizationAttribute::get_Value()
extern "C" int32_t LoaderOptimizationAttribute_get_Value_m1_14183 (LoaderOptimizationAttribute_t1_1563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
