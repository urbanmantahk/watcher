﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Object
struct Object_t;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;

#include "mscorlib_System_Reflection_PropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"

// System.Reflection.Emit.PropertyBuilder
struct  PropertyBuilder_t1_547  : public PropertyInfo_t
{
	// System.Reflection.PropertyAttributes System.Reflection.Emit.PropertyBuilder::attrs
	int32_t ___attrs_0;
	// System.String System.Reflection.Emit.PropertyBuilder::name
	String_t* ___name_1;
	// System.Type System.Reflection.Emit.PropertyBuilder::type
	Type_t * ___type_2;
	// System.Type[] System.Reflection.Emit.PropertyBuilder::parameters
	TypeU5BU5D_t1_31* ___parameters_3;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.PropertyBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_4;
	// System.Object System.Reflection.Emit.PropertyBuilder::def_value
	Object_t * ___def_value_5;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.PropertyBuilder::set_method
	MethodBuilder_t1_501 * ___set_method_6;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.PropertyBuilder::get_method
	MethodBuilder_t1_501 * ___get_method_7;
	// System.Int32 System.Reflection.Emit.PropertyBuilder::table_idx
	int32_t ___table_idx_8;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.PropertyBuilder::typeb
	TypeBuilder_t1_481 * ___typeb_9;
	// System.Type[] System.Reflection.Emit.PropertyBuilder::returnModReq
	TypeU5BU5D_t1_31* ___returnModReq_10;
	// System.Type[] System.Reflection.Emit.PropertyBuilder::returnModOpt
	TypeU5BU5D_t1_31* ___returnModOpt_11;
	// System.Type[][] System.Reflection.Emit.PropertyBuilder::paramModReq
	TypeU5BU5DU5BU5D_t1_483* ___paramModReq_12;
	// System.Type[][] System.Reflection.Emit.PropertyBuilder::paramModOpt
	TypeU5BU5DU5BU5D_t1_483* ___paramModOpt_13;
};
