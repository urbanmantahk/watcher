﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Stack/SyncStack
struct SyncStack_t1_309;
// System.Collections.Stack
struct Stack_t1_243;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Stack/SyncStack::.ctor(System.Collections.Stack)
extern "C" void SyncStack__ctor_m1_3500 (SyncStack_t1_309 * __this, Stack_t1_243 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Stack/SyncStack::get_Count()
extern "C" int32_t SyncStack_get_Count_m1_3501 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Stack/SyncStack::get_IsSynchronized()
extern "C" bool SyncStack_get_IsSynchronized_m1_3502 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Stack/SyncStack::get_SyncRoot()
extern "C" Object_t * SyncStack_get_SyncRoot_m1_3503 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Stack/SyncStack::Clear()
extern "C" void SyncStack_Clear_m1_3504 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Stack/SyncStack::Clone()
extern "C" Object_t * SyncStack_Clone_m1_3505 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Stack/SyncStack::Contains(System.Object)
extern "C" bool SyncStack_Contains_m1_3506 (SyncStack_t1_309 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Stack/SyncStack::CopyTo(System.Array,System.Int32)
extern "C" void SyncStack_CopyTo_m1_3507 (SyncStack_t1_309 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Stack/SyncStack::GetEnumerator()
extern "C" Object_t * SyncStack_GetEnumerator_m1_3508 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Stack/SyncStack::Peek()
extern "C" Object_t * SyncStack_Peek_m1_3509 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Stack/SyncStack::Pop()
extern "C" Object_t * SyncStack_Pop_m1_3510 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Stack/SyncStack::Push(System.Object)
extern "C" void SyncStack_Push_m1_3511 (SyncStack_t1_309 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Collections.Stack/SyncStack::ToArray()
extern "C" ObjectU5BU5D_t1_272* SyncStack_ToArray_m1_3512 (SyncStack_t1_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
