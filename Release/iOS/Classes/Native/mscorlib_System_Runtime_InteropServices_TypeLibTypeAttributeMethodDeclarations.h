﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.TypeLibTypeAttribute
struct TypeLibTypeAttribute_t1_837;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeFlags.h"

// System.Void System.Runtime.InteropServices.TypeLibTypeAttribute::.ctor(System.Int16)
extern "C" void TypeLibTypeAttribute__ctor_m1_7927 (TypeLibTypeAttribute_t1_837 * __this, int16_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.TypeLibTypeAttribute::.ctor(System.Runtime.InteropServices.TypeLibTypeFlags)
extern "C" void TypeLibTypeAttribute__ctor_m1_7928 (TypeLibTypeAttribute_t1_837 * __this, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.TypeLibTypeFlags System.Runtime.InteropServices.TypeLibTypeAttribute::get_Value()
extern "C" int32_t TypeLibTypeAttribute_get_Value_m1_7929 (TypeLibTypeAttribute_t1_837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
