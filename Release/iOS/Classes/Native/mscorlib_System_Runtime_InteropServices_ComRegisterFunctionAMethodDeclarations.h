﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComRegisterFunctionAttribute
struct ComRegisterFunctionAttribute_t1_774;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComRegisterFunctionAttribute::.ctor()
extern "C" void ComRegisterFunctionAttribute__ctor_m1_7612 (ComRegisterFunctionAttribute_t1_774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
