﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifURationalArray.h"

// ExifLibrary.GPSTimeStamp
struct  GPSTimeStamp_t8_108  : public ExifURationalArray_t8_107
{
};
