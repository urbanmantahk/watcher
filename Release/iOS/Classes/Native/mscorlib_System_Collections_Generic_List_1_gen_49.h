﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.BillingProduct[]
struct BillingProductU5BU5D_t8_337;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<VoxelBusters.NativePlugins.BillingProduct>
struct  List_1_t1_1910  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	BillingProductU5BU5D_t8_337* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1910_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	BillingProductU5BU5D_t8_337* ___EmptyArray_4;
};
