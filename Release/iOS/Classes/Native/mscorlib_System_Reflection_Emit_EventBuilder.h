﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1_502;

#include "mscorlib_System_Object.h"
#include "mscorlib_System_Reflection_EventAttributes.h"

// System.Reflection.Emit.EventBuilder
struct  EventBuilder_t1_500  : public Object_t
{
	// System.String System.Reflection.Emit.EventBuilder::name
	String_t* ___name_0;
	// System.Type System.Reflection.Emit.EventBuilder::type
	Type_t * ___type_1;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.EventBuilder::typeb
	TypeBuilder_t1_481 * ___typeb_2;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.EventBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_3;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.EventBuilder::add_method
	MethodBuilder_t1_501 * ___add_method_4;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.EventBuilder::remove_method
	MethodBuilder_t1_501 * ___remove_method_5;
	// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.EventBuilder::raise_method
	MethodBuilder_t1_501 * ___raise_method_6;
	// System.Reflection.Emit.MethodBuilder[] System.Reflection.Emit.EventBuilder::other_methods
	MethodBuilderU5BU5D_t1_502* ___other_methods_7;
	// System.Reflection.EventAttributes System.Reflection.Emit.EventBuilder::attrs
	int32_t ___attrs_8;
	// System.Int32 System.Reflection.Emit.EventBuilder::table_idx
	int32_t ___table_idx_9;
};
