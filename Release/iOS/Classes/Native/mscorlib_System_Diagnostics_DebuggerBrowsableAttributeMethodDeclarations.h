﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t1_326;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Diagnostics_DebuggerBrowsableState.h"

// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
extern "C" void DebuggerBrowsableAttribute__ctor_m1_3586 (DebuggerBrowsableAttribute_t1_326 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::get_State()
extern "C" int32_t DebuggerBrowsableAttribute_get_State_m1_3587 (DebuggerBrowsableAttribute_t1_326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
