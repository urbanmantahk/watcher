﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_126.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_18062_gshared (InternalEnumerator_1_t1_2208 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_18062(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2208 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_18062_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18063_gshared (InternalEnumerator_1_t1_2208 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18063(__this, method) (( void (*) (InternalEnumerator_1_t1_2208 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_18063_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18064_gshared (InternalEnumerator_1_t1_2208 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18064(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2208 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_18064_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_18065_gshared (InternalEnumerator_1_t1_2208 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_18065(__this, method) (( void (*) (InternalEnumerator_1_t1_2208 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_18065_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_18066_gshared (InternalEnumerator_1_t1_2208 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_18066(__this, method) (( bool (*) (InternalEnumerator_1_t1_2208 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_18066_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern "C" TagName_t4_171  InternalEnumerator_1_get_Current_m1_18067_gshared (InternalEnumerator_1_t1_2208 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_18067(__this, method) (( TagName_t4_171  (*) (InternalEnumerator_1_t1_2208 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_18067_gshared)(__this, method)
