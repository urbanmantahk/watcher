﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_ePickImageFinis.h"

// VoxelBusters.NativePlugins.MediaLibrary/PickImageCompletion
struct  PickImageCompletion_t8_240  : public MulticastDelegate_t1_21
{
};
