﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.GacMembershipCondition
struct GacMembershipCondition_t1_1349;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.GacMembershipCondition::.ctor()
extern "C" void GacMembershipCondition__ctor_m1_11506 (GacMembershipCondition_t1_1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.GacMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool GacMembershipCondition_Check_m1_11507 (GacMembershipCondition_t1_1349 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.GacMembershipCondition::Copy()
extern "C" Object_t * GacMembershipCondition_Copy_m1_11508 (GacMembershipCondition_t1_1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.GacMembershipCondition::Equals(System.Object)
extern "C" bool GacMembershipCondition_Equals_m1_11509 (GacMembershipCondition_t1_1349 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.GacMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void GacMembershipCondition_FromXml_m1_11510 (GacMembershipCondition_t1_1349 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.GacMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void GacMembershipCondition_FromXml_m1_11511 (GacMembershipCondition_t1_1349 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.GacMembershipCondition::GetHashCode()
extern "C" int32_t GacMembershipCondition_GetHashCode_m1_11512 (GacMembershipCondition_t1_1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.GacMembershipCondition::ToString()
extern "C" String_t* GacMembershipCondition_ToString_m1_11513 (GacMembershipCondition_t1_1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.GacMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * GacMembershipCondition_ToXml_m1_11514 (GacMembershipCondition_t1_1349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.GacMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * GacMembershipCondition_ToXml_m1_11515 (GacMembershipCondition_t1_1349 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
