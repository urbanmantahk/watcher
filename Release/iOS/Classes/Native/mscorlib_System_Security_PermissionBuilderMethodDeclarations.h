﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.IPermission
struct IPermission_t1_1400;
// System.String
struct String_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.PermissionBuilder::.cctor()
extern "C" void PermissionBuilder__cctor_m1_11956 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionBuilder::Create(System.String,System.Security.Permissions.PermissionState)
extern "C" Object_t * PermissionBuilder_Create_m1_11957 (Object_t * __this /* static, unused */, String_t* ___fullname, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionBuilder::Create(System.Security.SecurityElement)
extern "C" Object_t * PermissionBuilder_Create_m1_11958 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___se, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionBuilder::Create(System.String,System.Security.SecurityElement)
extern "C" Object_t * PermissionBuilder_Create_m1_11959 (Object_t * __this /* static, unused */, String_t* ___fullname, SecurityElement_t1_242 * ___se, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionBuilder::Create(System.Type)
extern "C" Object_t * PermissionBuilder_Create_m1_11960 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.PermissionBuilder::CreatePermission(System.String,System.Security.SecurityElement)
extern "C" Object_t * PermissionBuilder_CreatePermission_m1_11961 (Object_t * __this /* static, unused */, String_t* ___fullname, SecurityElement_t1_242 * ___se, const MethodInfo* method) IL2CPP_METHOD_ATTR;
