﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Cryptography_KeyNumber.h"

// System.Security.Cryptography.KeyNumber
struct  KeyNumber_t1_1218 
{
	// System.Int32 System.Security.Cryptography.KeyNumber::value__
	int32_t ___value___1;
};
