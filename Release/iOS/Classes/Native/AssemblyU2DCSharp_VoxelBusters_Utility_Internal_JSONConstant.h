﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.Utility.Internal.JSONConstants
struct  JSONConstants_t8_53  : public MonoBehaviour_t6_91
{
};
