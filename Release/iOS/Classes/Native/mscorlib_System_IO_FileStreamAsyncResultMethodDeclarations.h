﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.FileStreamAsyncResult
struct FileStreamAsyncResult_t1_430;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.Exception
struct Exception_t1_33;
// System.Threading.WaitHandle
struct WaitHandle_t1_917;

#include "codegen/il2cpp-codegen.h"

// System.Void System.IO.FileStreamAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern "C" void FileStreamAsyncResult__ctor_m1_5004 (FileStreamAsyncResult_t1_430 * __this, AsyncCallback_t1_28 * ___cb, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStreamAsyncResult::CBWrapper(System.IAsyncResult)
extern "C" void FileStreamAsyncResult_CBWrapper_m1_5005 (Object_t * __this /* static, unused */, Object_t * ___ares, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStreamAsyncResult::SetComplete(System.Exception)
extern "C" void FileStreamAsyncResult_SetComplete_m1_5006 (FileStreamAsyncResult_t1_430 * __this, Exception_t1_33 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStreamAsyncResult::SetComplete(System.Exception,System.Int32)
extern "C" void FileStreamAsyncResult_SetComplete_m1_5007 (FileStreamAsyncResult_t1_430 * __this, Exception_t1_33 * ___e, int32_t ___nbytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStreamAsyncResult::SetComplete(System.Exception,System.Int32,System.Boolean)
extern "C" void FileStreamAsyncResult_SetComplete_m1_5008 (FileStreamAsyncResult_t1_430 * __this, Exception_t1_33 * ___e, int32_t ___nbytes, bool ___synch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.IO.FileStreamAsyncResult::get_AsyncState()
extern "C" Object_t * FileStreamAsyncResult_get_AsyncState_m1_5009 (FileStreamAsyncResult_t1_430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.FileStreamAsyncResult::get_CompletedSynchronously()
extern "C" bool FileStreamAsyncResult_get_CompletedSynchronously_m1_5010 (FileStreamAsyncResult_t1_430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle System.IO.FileStreamAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t1_917 * FileStreamAsyncResult_get_AsyncWaitHandle_m1_5011 (FileStreamAsyncResult_t1_430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.FileStreamAsyncResult::get_IsCompleted()
extern "C" bool FileStreamAsyncResult_get_IsCompleted_m1_5012 (FileStreamAsyncResult_t1_430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.IO.FileStreamAsyncResult::get_Exception()
extern "C" Exception_t1_33 * FileStreamAsyncResult_get_Exception_m1_5013 (FileStreamAsyncResult_t1_430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.FileStreamAsyncResult::get_Done()
extern "C" bool FileStreamAsyncResult_get_Done_m1_5014 (FileStreamAsyncResult_t1_430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.FileStreamAsyncResult::set_Done(System.Boolean)
extern "C" void FileStreamAsyncResult_set_Done_m1_5015 (FileStreamAsyncResult_t1_430 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
