﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"

// VoxelBusters.NativePlugins.Demo.RemoteNotificationTest
struct  RemoteNotificationTest_t8_184  : public MonoBehaviour_t6_91
{
	// VoxelBusters.NativePlugins.NotificationType VoxelBusters.NativePlugins.Demo.RemoteNotificationTest::m_notificationType
	int32_t ___m_notificationType_2;
};
