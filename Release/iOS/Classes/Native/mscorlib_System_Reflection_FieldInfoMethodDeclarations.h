﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Object
struct Object_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_TypedReference.h"

// System.Void System.Reflection.FieldInfo::.ctor()
extern "C" void FieldInfo__ctor_m1_6841 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::System.Runtime.InteropServices._FieldInfo.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void FieldInfo_System_Runtime_InteropServices__FieldInfo_GetIDsOfNames_m1_6842 (FieldInfo_t * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::System.Runtime.InteropServices._FieldInfo.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void FieldInfo_System_Runtime_InteropServices__FieldInfo_GetTypeInfo_m1_6843 (FieldInfo_t * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::System.Runtime.InteropServices._FieldInfo.GetTypeInfoCount(System.UInt32&)
extern "C" void FieldInfo_System_Runtime_InteropServices__FieldInfo_GetTypeInfoCount_m1_6844 (FieldInfo_t * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::System.Runtime.InteropServices._FieldInfo.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void FieldInfo_System_Runtime_InteropServices__FieldInfo_Invoke_m1_6845 (FieldInfo_t * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberTypes System.Reflection.FieldInfo::get_MemberType()
extern "C" int32_t FieldInfo_get_MemberType_m1_6846 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsLiteral()
extern "C" bool FieldInfo_get_IsLiteral_m1_6847 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsStatic()
extern "C" bool FieldInfo_get_IsStatic_m1_6848 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsInitOnly()
extern "C" bool FieldInfo_get_IsInitOnly_m1_6849 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsPublic()
extern "C" bool FieldInfo_get_IsPublic_m1_6850 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsPrivate()
extern "C" bool FieldInfo_get_IsPrivate_m1_6851 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsFamily()
extern "C" bool FieldInfo_get_IsFamily_m1_6852 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsAssembly()
extern "C" bool FieldInfo_get_IsAssembly_m1_6853 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsFamilyAndAssembly()
extern "C" bool FieldInfo_get_IsFamilyAndAssembly_m1_6854 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsFamilyOrAssembly()
extern "C" bool FieldInfo_get_IsFamilyOrAssembly_m1_6855 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsPinvokeImpl()
extern "C" bool FieldInfo_get_IsPinvokeImpl_m1_6856 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsSpecialName()
extern "C" bool FieldInfo_get_IsSpecialName_m1_6857 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.FieldInfo::get_IsNotSerialized()
extern "C" bool FieldInfo_get_IsNotSerialized_m1_6858 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
extern "C" void FieldInfo_SetValue_m1_6859 (FieldInfo_t * __this, Object_t * ___obj, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.FieldInfo::internal_from_handle_type(System.IntPtr,System.IntPtr)
extern "C" FieldInfo_t * FieldInfo_internal_from_handle_type_m1_6860 (Object_t * __this /* static, unused */, IntPtr_t ___field_handle, IntPtr_t ___type_handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.FieldInfo::GetFieldFromHandle(System.RuntimeFieldHandle)
extern "C" FieldInfo_t * FieldInfo_GetFieldFromHandle_m1_6861 (Object_t * __this /* static, unused */, RuntimeFieldHandle_t1_36  ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.FieldInfo::GetFieldFromHandle(System.RuntimeFieldHandle,System.RuntimeTypeHandle)
extern "C" FieldInfo_t * FieldInfo_GetFieldFromHandle_m1_6862 (Object_t * __this /* static, unused */, RuntimeFieldHandle_t1_36  ___handle, RuntimeTypeHandle_t1_30  ___declaringType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.FieldInfo::GetFieldOffset()
extern "C" int32_t FieldInfo_GetFieldOffset_m1_6863 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.FieldInfo::GetValueDirect(System.TypedReference)
extern "C" Object_t * FieldInfo_GetValueDirect_m1_6864 (FieldInfo_t * __this, TypedReference_t1_67  ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.FieldInfo::SetValueDirect(System.TypedReference,System.Object)
extern "C" void FieldInfo_SetValueDirect_m1_6865 (FieldInfo_t * __this, TypedReference_t1_67  ___obj, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::GetUnmanagedMarshal()
extern "C" UnmanagedMarshal_t1_505 * FieldInfo_GetUnmanagedMarshal_m1_6866 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.FieldInfo::get_UMarshal()
extern "C" UnmanagedMarshal_t1_505 * FieldInfo_get_UMarshal_m1_6867 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.FieldInfo::GetPseudoCustomAttributes()
extern "C" ObjectU5BU5D_t1_272* FieldInfo_GetPseudoCustomAttributes_m1_6868 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.FieldInfo::GetTypeModifiers(System.Boolean)
extern "C" TypeU5BU5D_t1_31* FieldInfo_GetTypeModifiers_m1_6869 (FieldInfo_t * __this, bool ___optional, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.FieldInfo::GetOptionalCustomModifiers()
extern "C" TypeU5BU5D_t1_31* FieldInfo_GetOptionalCustomModifiers_m1_6870 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.FieldInfo::GetRequiredCustomModifiers()
extern "C" TypeU5BU5D_t1_31* FieldInfo_GetRequiredCustomModifiers_m1_6871 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.FieldInfo::GetRawConstantValue()
extern "C" Object_t * FieldInfo_GetRawConstantValue_m1_6872 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.FieldInfo::System.Runtime.InteropServices._FieldInfo.GetType()
extern "C" Type_t * FieldInfo_System_Runtime_InteropServices__FieldInfo_GetType_m1_6873 (FieldInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
