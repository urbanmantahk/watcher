﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t1_2092;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1_1749;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1_2781;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m1_16704_gshared (ArrayReadOnlyList_1_t1_2092 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m1_16704(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m1_16704_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_16705_gshared (ArrayReadOnlyList_1_t1_2092 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_16705(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1_2092 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1_16705_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1_593  ArrayReadOnlyList_1_get_Item_m1_16706_gshared (ArrayReadOnlyList_1_t1_2092 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m1_16706(__this, ___index, method) (( CustomAttributeNamedArgument_t1_593  (*) (ArrayReadOnlyList_1_t1_2092 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m1_16706_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m1_16707_gshared (ArrayReadOnlyList_1_t1_2092 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m1_16707(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m1_16707_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m1_16708_gshared (ArrayReadOnlyList_1_t1_2092 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m1_16708(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_2092 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m1_16708_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m1_16709_gshared (ArrayReadOnlyList_1_t1_2092 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m1_16709(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1_2092 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m1_16709_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m1_16710_gshared (ArrayReadOnlyList_1_t1_2092 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m1_16710(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ArrayReadOnlyList_1_Add_m1_16710_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m1_16711_gshared (ArrayReadOnlyList_1_t1_2092 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m1_16711(__this, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m1_16711_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m1_16712_gshared (ArrayReadOnlyList_1_t1_2092 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m1_16712(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_2092 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m1_16712_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m1_16713_gshared (ArrayReadOnlyList_1_t1_2092 * __this, CustomAttributeNamedArgumentU5BU5D_t1_1749* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m1_16713(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, CustomAttributeNamedArgumentU5BU5D_t1_1749*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m1_16713_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m1_16714_gshared (ArrayReadOnlyList_1_t1_2092 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m1_16714(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1_2092 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m1_16714_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m1_16715_gshared (ArrayReadOnlyList_1_t1_2092 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m1_16715(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1_2092 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m1_16715_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m1_16716_gshared (ArrayReadOnlyList_1_t1_2092 * __this, int32_t ___index, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m1_16716(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, int32_t, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m1_16716_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m1_16717_gshared (ArrayReadOnlyList_1_t1_2092 * __this, CustomAttributeNamedArgument_t1_593  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m1_16717(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1_2092 *, CustomAttributeNamedArgument_t1_593 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m1_16717_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m1_16718_gshared (ArrayReadOnlyList_1_t1_2092 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m1_16718(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1_2092 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m1_16718_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C" Exception_t1_33 * ArrayReadOnlyList_1_ReadOnlyError_m1_16719_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m1_16719(__this /* static, unused */, method) (( Exception_t1_33 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m1_16719_gshared)(__this /* static, unused */, method)
