﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
struct Collection_1_t1_2471;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t1_2470;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t7_205;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t1_2841;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void Collection_1__ctor_m1_22392_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_22392(__this, method) (( void (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1__ctor_m1_22392_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_22393_gshared (Collection_1_t1_2471 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_22393(__this, ___list, method) (( void (*) (Collection_1_t1_2471 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_22393_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22394_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22394(__this, method) (( bool (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_22394_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_22395_gshared (Collection_1_t1_2471 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_22395(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2471 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_22395_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_22396_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_22396(__this, method) (( Object_t * (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_22396_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_22397_gshared (Collection_1_t1_2471 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_22397(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2471 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_22397_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_22398_gshared (Collection_1_t1_2471 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_22398(__this, ___value, method) (( bool (*) (Collection_1_t1_2471 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_22398_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_22399_gshared (Collection_1_t1_2471 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_22399(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2471 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_22399_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_22400_gshared (Collection_1_t1_2471 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_22400(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2471 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_22400_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_22401_gshared (Collection_1_t1_2471 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_22401(__this, ___value, method) (( void (*) (Collection_1_t1_2471 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_22401_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_22402_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_22402(__this, method) (( bool (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_22402_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_22403_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_22403(__this, method) (( Object_t * (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_22403_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_22404_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_22404(__this, method) (( bool (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_22404_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_22405_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_22405(__this, method) (( bool (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_22405_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_22406_gshared (Collection_1_t1_2471 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_22406(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2471 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_22406_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_22407_gshared (Collection_1_t1_2471 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_22407(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2471 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_22407_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void Collection_1_Add_m1_22408_gshared (Collection_1_t1_2471 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_22408(__this, ___item, method) (( void (*) (Collection_1_t1_2471 *, RaycastResult_t7_31 , const MethodInfo*))Collection_1_Add_m1_22408_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void Collection_1_Clear_m1_22409_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_22409(__this, method) (( void (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_Clear_m1_22409_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_22410_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_22410(__this, method) (( void (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_ClearItems_m1_22410_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool Collection_1_Contains_m1_22411_gshared (Collection_1_t1_2471 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_22411(__this, ___item, method) (( bool (*) (Collection_1_t1_2471 *, RaycastResult_t7_31 , const MethodInfo*))Collection_1_Contains_m1_22411_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_22412_gshared (Collection_1_t1_2471 * __this, RaycastResultU5BU5D_t7_205* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_22412(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2471 *, RaycastResultU5BU5D_t7_205*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_22412_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_22413_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_22413(__this, method) (( Object_t* (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_GetEnumerator_m1_22413_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_22414_gshared (Collection_1_t1_2471 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_22414(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2471 *, RaycastResult_t7_31 , const MethodInfo*))Collection_1_IndexOf_m1_22414_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_22415_gshared (Collection_1_t1_2471 * __this, int32_t ___index, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_22415(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2471 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))Collection_1_Insert_m1_22415_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_22416_gshared (Collection_1_t1_2471 * __this, int32_t ___index, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_22416(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2471 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))Collection_1_InsertItem_m1_22416_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_22417_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_22417(__this, method) (( Object_t* (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_get_Items_m1_22417_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool Collection_1_Remove_m1_22418_gshared (Collection_1_t1_2471 * __this, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_22418(__this, ___item, method) (( bool (*) (Collection_1_t1_2471 *, RaycastResult_t7_31 , const MethodInfo*))Collection_1_Remove_m1_22418_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_22419_gshared (Collection_1_t1_2471 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_22419(__this, ___index, method) (( void (*) (Collection_1_t1_2471 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_22419_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_22420_gshared (Collection_1_t1_2471 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_22420(__this, ___index, method) (( void (*) (Collection_1_t1_2471 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_22420_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_22421_gshared (Collection_1_t1_2471 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_22421(__this, method) (( int32_t (*) (Collection_1_t1_2471 *, const MethodInfo*))Collection_1_get_Count_m1_22421_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t7_31  Collection_1_get_Item_m1_22422_gshared (Collection_1_t1_2471 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_22422(__this, ___index, method) (( RaycastResult_t7_31  (*) (Collection_1_t1_2471 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_22422_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_22423_gshared (Collection_1_t1_2471 * __this, int32_t ___index, RaycastResult_t7_31  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_22423(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2471 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))Collection_1_set_Item_m1_22423_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_22424_gshared (Collection_1_t1_2471 * __this, int32_t ___index, RaycastResult_t7_31  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_22424(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2471 *, int32_t, RaycastResult_t7_31 , const MethodInfo*))Collection_1_SetItem_m1_22424_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_22425_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_22425(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_22425_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ConvertItem(System.Object)
extern "C" RaycastResult_t7_31  Collection_1_ConvertItem_m1_22426_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_22426(__this /* static, unused */, ___item, method) (( RaycastResult_t7_31  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_22426_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_22427_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_22427(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_22427_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_22428_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_22428(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_22428_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_22429_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_22429(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_22429_gshared)(__this /* static, unused */, ___list, method)
