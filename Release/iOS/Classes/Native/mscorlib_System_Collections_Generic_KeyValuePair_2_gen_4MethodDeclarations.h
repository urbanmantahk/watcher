﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_15677_gshared (KeyValuePair_2_t1_1981 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1_15677(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_1981 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_15677_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m1_15678_gshared (KeyValuePair_2_t1_1981 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1_15678(__this, method) (( Object_t * (*) (KeyValuePair_2_t1_1981 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_15678_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_15679_gshared (KeyValuePair_2_t1_1981 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1_15679(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1981 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m1_15679_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m1_15680_gshared (KeyValuePair_2_t1_1981 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1_15680(__this, method) (( int32_t (*) (KeyValuePair_2_t1_1981 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_15680_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_15681_gshared (KeyValuePair_2_t1_1981 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1_15681(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_1981 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_15681_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m1_15682_gshared (KeyValuePair_2_t1_1981 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1_15682(__this, method) (( String_t* (*) (KeyValuePair_2_t1_1981 *, const MethodInfo*))KeyValuePair_2_ToString_m1_15682_gshared)(__this, method)
