﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.XmlDocumentFragment
struct XmlDocumentFragment_t4_133;
// System.Xml.XmlDocument
struct XmlDocument_t4_123;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4_118;
// System.String
struct String_t;
// System.Xml.XmlNode
struct XmlNode_t4_116;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNodeType.h"

// System.Void System.Xml.XmlDocumentFragment::.ctor(System.Xml.XmlDocument)
extern "C" void XmlDocumentFragment__ctor_m4_456 (XmlDocumentFragment_t4_133 * __this, XmlDocument_t4_123 * ___doc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::System.Xml.IHasXmlChildNode.get_LastLinkedChild()
extern "C" XmlLinkedNode_t4_118 * XmlDocumentFragment_System_Xml_IHasXmlChildNode_get_LastLinkedChild_m4_457 (XmlDocumentFragment_t4_133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Xml.XmlDocumentFragment::System.Xml.IHasXmlChildNode.set_LastLinkedChild(System.Xml.XmlLinkedNode)
extern "C" void XmlDocumentFragment_System_Xml_IHasXmlChildNode_set_LastLinkedChild_m4_458 (XmlDocumentFragment_t4_133 * __this, XmlLinkedNode_t4_118 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentFragment::get_LocalName()
extern "C" String_t* XmlDocumentFragment_get_LocalName_m4_459 (XmlDocumentFragment_t4_133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlDocumentFragment::get_Name()
extern "C" String_t* XmlDocumentFragment_get_Name_m4_460 (XmlDocumentFragment_t4_133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType System.Xml.XmlDocumentFragment::get_NodeType()
extern "C" int32_t XmlDocumentFragment_get_NodeType_m4_461 (XmlDocumentFragment_t4_133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument System.Xml.XmlDocumentFragment::get_OwnerDocument()
extern "C" XmlDocument_t4_123 * XmlDocumentFragment_get_OwnerDocument_m4_462 (XmlDocumentFragment_t4_133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentFragment::get_ParentNode()
extern "C" XmlNode_t4_116 * XmlDocumentFragment_get_ParentNode_m4_463 (XmlDocumentFragment_t4_133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNode System.Xml.XmlDocumentFragment::CloneNode(System.Boolean)
extern "C" XmlNode_t4_116 * XmlDocumentFragment_CloneNode_m4_464 (XmlDocumentFragment_t4_133 * __this, bool ___deep, const MethodInfo* method) IL2CPP_METHOD_ATTR;
