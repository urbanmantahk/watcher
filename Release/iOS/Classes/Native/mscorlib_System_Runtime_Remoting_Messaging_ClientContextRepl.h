﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.ClientContextReplySink
struct  ClientContextReplySink_t1_929  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.ClientContextReplySink::_replySink
	Object_t * ____replySink_0;
	// System.Runtime.Remoting.Contexts.Context System.Runtime.Remoting.Messaging.ClientContextReplySink::_context
	Context_t1_891 * ____context_1;
};
