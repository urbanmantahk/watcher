﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.AddressBookContact[]
struct AddressBookContactU5BU5D_t8_177;
// UnityEngine.Texture[]
struct TextureU5BU5D_t6_314;
// VoxelBusters.Utility.GUIScrollView
struct GUIScrollView_t8_19;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"

// VoxelBusters.NativePlugins.Demo.AddressBookDemo
struct  AddressBookDemo_t8_176  : public NPDisabledFeatureDemo_t8_175
{
	// System.Single VoxelBusters.NativePlugins.Demo.AddressBookDemo::m_eachColumnWidth
	float ___m_eachColumnWidth_10;
	// System.Single VoxelBusters.NativePlugins.Demo.AddressBookDemo::m_eachRowHeight
	float ___m_eachRowHeight_11;
	// System.Int32 VoxelBusters.NativePlugins.Demo.AddressBookDemo::m_maxContactsToRender
	int32_t ___m_maxContactsToRender_12;
	// VoxelBusters.NativePlugins.AddressBookContact[] VoxelBusters.NativePlugins.Demo.AddressBookDemo::m_contactsInfo
	AddressBookContactU5BU5D_t8_177* ___m_contactsInfo_13;
	// UnityEngine.Texture[] VoxelBusters.NativePlugins.Demo.AddressBookDemo::m_contactPictures
	TextureU5BU5D_t6_314* ___m_contactPictures_14;
	// VoxelBusters.Utility.GUIScrollView VoxelBusters.NativePlugins.Demo.AddressBookDemo::m_contactsScrollView
	GUIScrollView_t8_19 * ___m_contactsScrollView_15;
};
