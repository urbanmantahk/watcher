﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Versioning.ResourceExposureAttribute
struct ResourceExposureAttribute_t1_1101;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Versioning_ResourceScope.h"

// System.Void System.Runtime.Versioning.ResourceExposureAttribute::.ctor(System.Runtime.Versioning.ResourceScope)
extern "C" void ResourceExposureAttribute__ctor_m1_9690 (ResourceExposureAttribute_t1_1101 * __this, int32_t ___exposureLevel, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Versioning.ResourceScope System.Runtime.Versioning.ResourceExposureAttribute::get_ResourceExposureLevel()
extern "C" int32_t ResourceExposureAttribute_get_ResourceExposureLevel_m1_9691 (ResourceExposureAttribute_t1_1101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
