﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.PrincipalPermission
struct PrincipalPermission_t1_1297;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.Permissions.PrincipalPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void PrincipalPermission__ctor_m1_11036 (PrincipalPermission_t1_1297 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermission::.ctor(System.String,System.String)
extern "C" void PrincipalPermission__ctor_m1_11037 (PrincipalPermission_t1_1297 * __this, String_t* ___name, String_t* ___role, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermission::.ctor(System.String,System.String,System.Boolean)
extern "C" void PrincipalPermission__ctor_m1_11038 (PrincipalPermission_t1_1297 * __this, String_t* ___name, String_t* ___role, bool ___isAuthenticated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermission::.ctor(System.Collections.ArrayList)
extern "C" void PrincipalPermission__ctor_m1_11039 (PrincipalPermission_t1_1297 * __this, ArrayList_t1_170 * ___principals, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.PrincipalPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t PrincipalPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_11040 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PrincipalPermission::Copy()
extern "C" Object_t * PrincipalPermission_Copy_m1_11041 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermission::Demand()
extern "C" void PrincipalPermission_Demand_m1_11042 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PrincipalPermission::FromXml(System.Security.SecurityElement)
extern "C" void PrincipalPermission_FromXml_m1_11043 (PrincipalPermission_t1_1297 * __this, SecurityElement_t1_242 * ___elem, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PrincipalPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * PrincipalPermission_Intersect_m1_11044 (PrincipalPermission_t1_1297 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PrincipalPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool PrincipalPermission_IsSubsetOf_m1_11045 (PrincipalPermission_t1_1297 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PrincipalPermission::IsUnrestricted()
extern "C" bool PrincipalPermission_IsUnrestricted_m1_11046 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PrincipalPermission::ToString()
extern "C" String_t* PrincipalPermission_ToString_m1_11047 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.PrincipalPermission::ToXml()
extern "C" SecurityElement_t1_242 * PrincipalPermission_ToXml_m1_11048 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PrincipalPermission::Union(System.Security.IPermission)
extern "C" Object_t * PrincipalPermission_Union_m1_11049 (PrincipalPermission_t1_1297 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PrincipalPermission::Equals(System.Object)
extern "C" bool PrincipalPermission_Equals_m1_11050 (PrincipalPermission_t1_1297 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.PrincipalPermission::GetHashCode()
extern "C" int32_t PrincipalPermission_GetHashCode_m1_11051 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.PrincipalPermission System.Security.Permissions.PrincipalPermission::Cast(System.Security.IPermission)
extern "C" PrincipalPermission_t1_1297 * PrincipalPermission_Cast_m1_11052 (PrincipalPermission_t1_1297 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PrincipalPermission::IsEmpty()
extern "C" bool PrincipalPermission_IsEmpty_m1_11053 (PrincipalPermission_t1_1297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.PrincipalPermission::CheckSecurityElement(System.Security.SecurityElement,System.String,System.Int32,System.Int32)
extern "C" int32_t PrincipalPermission_CheckSecurityElement_m1_11054 (PrincipalPermission_t1_1297 * __this, SecurityElement_t1_242 * ___se, String_t* ___parameterName, int32_t ___minimumVersion, int32_t ___maximumVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
