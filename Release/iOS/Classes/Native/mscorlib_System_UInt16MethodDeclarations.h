﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IFormatProvider
struct IFormatProvider_t1_455;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "mscorlib_System_TypeCode.h"

// System.Boolean System.UInt16::System.IConvertible.ToBoolean(System.IFormatProvider)
extern "C" bool UInt16_System_IConvertible_ToBoolean_m1_310 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.UInt16::System.IConvertible.ToByte(System.IFormatProvider)
extern "C" uint8_t UInt16_System_IConvertible_ToByte_m1_311 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.UInt16::System.IConvertible.ToChar(System.IFormatProvider)
extern "C" uint16_t UInt16_System_IConvertible_ToChar_m1_312 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.UInt16::System.IConvertible.ToDateTime(System.IFormatProvider)
extern "C" DateTime_t1_150  UInt16_System_IConvertible_ToDateTime_m1_313 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.UInt16::System.IConvertible.ToDecimal(System.IFormatProvider)
extern "C" Decimal_t1_19  UInt16_System_IConvertible_ToDecimal_m1_314 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.UInt16::System.IConvertible.ToDouble(System.IFormatProvider)
extern "C" double UInt16_System_IConvertible_ToDouble_m1_315 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.UInt16::System.IConvertible.ToInt16(System.IFormatProvider)
extern "C" int16_t UInt16_System_IConvertible_ToInt16_m1_316 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::System.IConvertible.ToInt32(System.IFormatProvider)
extern "C" int32_t UInt16_System_IConvertible_ToInt32_m1_317 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.UInt16::System.IConvertible.ToInt64(System.IFormatProvider)
extern "C" int64_t UInt16_System_IConvertible_ToInt64_m1_318 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.UInt16::System.IConvertible.ToSByte(System.IFormatProvider)
extern "C" int8_t UInt16_System_IConvertible_ToSByte_m1_319 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.UInt16::System.IConvertible.ToSingle(System.IFormatProvider)
extern "C" float UInt16_System_IConvertible_ToSingle_m1_320 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.UInt16::System.IConvertible.ToType(System.Type,System.IFormatProvider)
extern "C" Object_t * UInt16_System_IConvertible_ToType_m1_321 (uint16_t* __this, Type_t * ___targetType, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::System.IConvertible.ToUInt16(System.IFormatProvider)
extern "C" uint16_t UInt16_System_IConvertible_ToUInt16_m1_322 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.UInt16::System.IConvertible.ToUInt32(System.IFormatProvider)
extern "C" uint32_t UInt16_System_IConvertible_ToUInt32_m1_323 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.UInt16::System.IConvertible.ToUInt64(System.IFormatProvider)
extern "C" uint64_t UInt16_System_IConvertible_ToUInt64_m1_324 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.Object)
extern "C" int32_t UInt16_CompareTo_m1_325 (uint16_t* __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.Object)
extern "C" bool UInt16_Equals_m1_326 (uint16_t* __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::GetHashCode()
extern "C" int32_t UInt16_GetHashCode_m1_327 (uint16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.UInt16)
extern "C" int32_t UInt16_CompareTo_m1_328 (uint16_t* __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.UInt16)
extern "C" bool UInt16_Equals_m1_329 (uint16_t* __this, uint16_t ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.IFormatProvider)
extern "C" uint16_t UInt16_Parse_m1_330 (Object_t * __this /* static, unused */, String_t* ___s, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.Globalization.NumberStyles)
extern "C" uint16_t UInt16_Parse_m1_331 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.Globalization.NumberStyles,System.IFormatProvider)
extern "C" uint16_t UInt16_Parse_m1_332 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String)
extern "C" uint16_t UInt16_Parse_m1_333 (Object_t * __this /* static, unused */, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::TryParse(System.String,System.UInt16&)
extern "C" bool UInt16_TryParse_m1_334 (Object_t * __this /* static, unused */, String_t* ___s, uint16_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.UInt16&)
extern "C" bool UInt16_TryParse_m1_335 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, uint16_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString()
extern "C" String_t* UInt16_ToString_m1_336 (uint16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.IFormatProvider)
extern "C" String_t* UInt16_ToString_m1_337 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.String)
extern "C" String_t* UInt16_ToString_m1_338 (uint16_t* __this, String_t* ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.String,System.IFormatProvider)
extern "C" String_t* UInt16_ToString_m1_339 (uint16_t* __this, String_t* ___format, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TypeCode System.UInt16::GetTypeCode()
extern "C" int32_t UInt16_GetTypeCode_m1_340 (uint16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
