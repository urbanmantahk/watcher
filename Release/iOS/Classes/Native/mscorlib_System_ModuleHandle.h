﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_ModuleHandle.h"

// System.ModuleHandle
struct  ModuleHandle_t1_1572 
{
	// System.IntPtr System.ModuleHandle::value
	IntPtr_t ___value_0;
};
struct ModuleHandle_t1_1572_StaticFields{
	// System.ModuleHandle System.ModuleHandle::EmptyHandle
	ModuleHandle_t1_1572  ___EmptyHandle_1;
};
