﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_Swapper_t1_26 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t1_28 ();
extern "C" void pinvoke_delegate_wrapper_TryCode_t1_71 ();
extern "C" void pinvoke_delegate_wrapper_CleanupCode_t1_72 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_157 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_173 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1_428 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1_429 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t1_596 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1_623 ();
extern "C" void pinvoke_delegate_wrapper_RenewalDelegate_t1_906 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1_1091 ();
extern "C" void pinvoke_delegate_wrapper_ExceptionFromErrorCode_t1_1162 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1_1619 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1_32 ();
extern "C" void pinvoke_delegate_wrapper_ModuleResolveEventHandler_t1_561 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1_1620 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t1_616 ();
extern "C" void pinvoke_delegate_wrapper_ObjectCreationDelegate_t1_1621 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1_1622 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t1_1623 ();
extern "C" void pinvoke_delegate_wrapper_MessageSurrogateFilter_t1_958 ();
extern "C" void pinvoke_delegate_wrapper_ContextCallback_t1_1624 ();
extern "C" void pinvoke_delegate_wrapper_IOCompletionCallback_t1_1625 ();
extern "C" void pinvoke_delegate_wrapper_ParameterizedThreadStart_t1_1626 ();
extern "C" void pinvoke_delegate_wrapper_SendOrPostCallback_t1_1627 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1_1628 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1_1488 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1_1629 ();
extern "C" void pinvoke_delegate_wrapper_WaitOrTimerCallback_t1_1474 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1_1498 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1_1494 ();
extern "C" void pinvoke_delegate_wrapper_ConsoleCancelEventHandler_t1_1630 ();
extern "C" void pinvoke_delegate_wrapper_CrossAppDomainDelegate_t1_1631 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t1_461 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t1_1495 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1_1496 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t2_26 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t2_111 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t2_90 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t2_91 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t2_78 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t2_79 ();
extern "C" void pinvoke_delegate_wrapper_CharGetter_t4_164 ();
extern "C" void pinvoke_delegate_wrapper_XmlNodeChangedEventHandler_t4_132 ();
extern "C" void pinvoke_delegate_wrapper_Action_t5_11 ();
extern "C" void pinvoke_delegate_wrapper_UnmanagedReadOrWrite_t3_40 ();
extern "C" void pinvoke_delegate_wrapper_ReadMethod_t3_41 ();
extern "C" void pinvoke_delegate_wrapper_WriteMethod_t3_42 ();
extern "C" void pinvoke_delegate_wrapper_SocketAsyncCall_t3_64 ();
extern "C" void pinvoke_delegate_wrapper_GetResponseCallback_t3_94 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t3_100 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t3_101 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t3_174 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t3_208 ();
extern "C" void pinvoke_delegate_wrapper_LocalCertificateSelectionCallback_t3_50 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t3_5 ();
extern "C" void pinvoke_delegate_wrapper_BindIPEndPoint_t3_125 ();
extern "C" void pinvoke_delegate_wrapper_HttpContinueDelegate_t3_115 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t3_240 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t6_37 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t6_63 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t6_83 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t6_85 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t6_88 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t6_124 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t6_126 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t6_127 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t6_148 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t6_155 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t6_167 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t6_184 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t6_106 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t6_259 ();
extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t7_96 ();
extern "C" void pinvoke_delegate_wrapper_CallbackFunction_t8_151 ();
extern "C" void pinvoke_delegate_wrapper_JSONResponse_t8_157 ();
extern "C" void pinvoke_delegate_wrapper_Completion_t8_159 ();
extern "C" void pinvoke_delegate_wrapper_Completion_t8_161 ();
extern "C" void pinvoke_delegate_wrapper_RequestAccessCompletion_t8_193 ();
extern "C" void pinvoke_delegate_wrapper_ReadContactsCompletion_t8_194 ();
extern "C" void pinvoke_delegate_wrapper_LoadAchievementsCompletion_t8_218 ();
extern "C" void pinvoke_delegate_wrapper_ReportProgressCompletion_t8_220 ();
extern "C" void pinvoke_delegate_wrapper_LoadAchievementDescriptionsCompletion_t8_223 ();
extern "C" void pinvoke_delegate_wrapper_LoadScoreCompletion_t8_229 ();
extern "C" void pinvoke_delegate_wrapper_ReportScoreCompletion_t8_232 ();
extern "C" void pinvoke_delegate_wrapper_LoadUsersCompletion_t8_233 ();
extern "C" void pinvoke_delegate_wrapper_PickImageCompletion_t8_240 ();
extern "C" void pinvoke_delegate_wrapper_SaveImageToGalleryCompletion_t8_241 ();
extern "C" void pinvoke_delegate_wrapper_PickVideoCompletion_t8_242 ();
extern "C" void pinvoke_delegate_wrapper_PlayVideoCompletion_t8_243 ();
extern "C" void pinvoke_delegate_wrapper_RegisterForRemoteNotificationCompletion_t8_257 ();
extern "C" void pinvoke_delegate_wrapper_ReceivedNotificationResponse_t8_258 ();
extern "C" void pinvoke_delegate_wrapper_SharingCompletion_t8_271 ();
extern "C" void pinvoke_delegate_wrapper_AlertDialogCompletion_t8_300 ();
extern "C" void pinvoke_delegate_wrapper_SingleFieldPromptCompletion_t8_301 ();
extern "C" void pinvoke_delegate_wrapper_LoginPromptCompletion_t8_302 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[97] = 
{
	pinvoke_delegate_wrapper_Swapper_t1_26,
	pinvoke_delegate_wrapper_AsyncCallback_t1_28,
	pinvoke_delegate_wrapper_TryCode_t1_71,
	pinvoke_delegate_wrapper_CleanupCode_t1_72,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_157,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1_173,
	pinvoke_delegate_wrapper_ReadDelegate_t1_428,
	pinvoke_delegate_wrapper_WriteDelegate_t1_429,
	pinvoke_delegate_wrapper_AddEventAdapter_t1_596,
	pinvoke_delegate_wrapper_GetterAdapter_t1_623,
	pinvoke_delegate_wrapper_RenewalDelegate_t1_906,
	pinvoke_delegate_wrapper_CallbackHandler_t1_1091,
	pinvoke_delegate_wrapper_ExceptionFromErrorCode_t1_1162,
	pinvoke_delegate_wrapper_PrimalityTest_t1_1619,
	pinvoke_delegate_wrapper_MemberFilter_t1_32,
	pinvoke_delegate_wrapper_ModuleResolveEventHandler_t1_561,
	pinvoke_delegate_wrapper_GetterAdapter_t1_1620,
	pinvoke_delegate_wrapper_TypeFilter_t1_616,
	pinvoke_delegate_wrapper_ObjectCreationDelegate_t1_1621,
	pinvoke_delegate_wrapper_CrossContextDelegate_t1_1622,
	pinvoke_delegate_wrapper_HeaderHandler_t1_1623,
	pinvoke_delegate_wrapper_MessageSurrogateFilter_t1_958,
	pinvoke_delegate_wrapper_ContextCallback_t1_1624,
	pinvoke_delegate_wrapper_IOCompletionCallback_t1_1625,
	pinvoke_delegate_wrapper_ParameterizedThreadStart_t1_1626,
	pinvoke_delegate_wrapper_SendOrPostCallback_t1_1627,
	pinvoke_delegate_wrapper_ThreadStart_t1_1628,
	pinvoke_delegate_wrapper_TimerCallback_t1_1488,
	pinvoke_delegate_wrapper_WaitCallback_t1_1629,
	pinvoke_delegate_wrapper_WaitOrTimerCallback_t1_1474,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1_1498,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1_1494,
	pinvoke_delegate_wrapper_ConsoleCancelEventHandler_t1_1630,
	pinvoke_delegate_wrapper_CrossAppDomainDelegate_t1_1631,
	pinvoke_delegate_wrapper_EventHandler_t1_461,
	pinvoke_delegate_wrapper_ResolveEventHandler_t1_1495,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t1_1496,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t2_26,
	pinvoke_delegate_wrapper_PrimalityTest_t2_111,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t2_90,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t2_91,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t2_78,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t2_79,
	pinvoke_delegate_wrapper_CharGetter_t4_164,
	pinvoke_delegate_wrapper_XmlNodeChangedEventHandler_t4_132,
	pinvoke_delegate_wrapper_Action_t5_11,
	pinvoke_delegate_wrapper_UnmanagedReadOrWrite_t3_40,
	pinvoke_delegate_wrapper_ReadMethod_t3_41,
	pinvoke_delegate_wrapper_WriteMethod_t3_42,
	pinvoke_delegate_wrapper_SocketAsyncCall_t3_64,
	pinvoke_delegate_wrapper_GetResponseCallback_t3_94,
	pinvoke_delegate_wrapper_WriteDelegate_t3_100,
	pinvoke_delegate_wrapper_ReadDelegate_t3_101,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t3_174,
	pinvoke_delegate_wrapper_CostDelegate_t3_208,
	pinvoke_delegate_wrapper_LocalCertificateSelectionCallback_t3_50,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t3_5,
	pinvoke_delegate_wrapper_BindIPEndPoint_t3_125,
	pinvoke_delegate_wrapper_HttpContinueDelegate_t3_115,
	pinvoke_delegate_wrapper_MatchEvaluator_t3_240,
	pinvoke_delegate_wrapper_StateChanged_t6_37,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t6_63,
	pinvoke_delegate_wrapper_LogCallback_t6_83,
	pinvoke_delegate_wrapper_CameraCallback_t6_85,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t6_88,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t6_124,
	pinvoke_delegate_wrapper_PCMReaderCallback_t6_126,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t6_127,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t6_148,
	pinvoke_delegate_wrapper_WillRenderCanvases_t6_155,
	pinvoke_delegate_wrapper_WindowFunction_t6_167,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t6_184,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t6_106,
	pinvoke_delegate_wrapper_UnityAction_t6_259,
	pinvoke_delegate_wrapper_OnValidateInput_t7_96,
	pinvoke_delegate_wrapper_CallbackFunction_t8_151,
	pinvoke_delegate_wrapper_JSONResponse_t8_157,
	pinvoke_delegate_wrapper_Completion_t8_159,
	pinvoke_delegate_wrapper_Completion_t8_161,
	pinvoke_delegate_wrapper_RequestAccessCompletion_t8_193,
	pinvoke_delegate_wrapper_ReadContactsCompletion_t8_194,
	pinvoke_delegate_wrapper_LoadAchievementsCompletion_t8_218,
	pinvoke_delegate_wrapper_ReportProgressCompletion_t8_220,
	pinvoke_delegate_wrapper_LoadAchievementDescriptionsCompletion_t8_223,
	pinvoke_delegate_wrapper_LoadScoreCompletion_t8_229,
	pinvoke_delegate_wrapper_ReportScoreCompletion_t8_232,
	pinvoke_delegate_wrapper_LoadUsersCompletion_t8_233,
	pinvoke_delegate_wrapper_PickImageCompletion_t8_240,
	pinvoke_delegate_wrapper_SaveImageToGalleryCompletion_t8_241,
	pinvoke_delegate_wrapper_PickVideoCompletion_t8_242,
	pinvoke_delegate_wrapper_PlayVideoCompletion_t8_243,
	pinvoke_delegate_wrapper_RegisterForRemoteNotificationCompletion_t8_257,
	pinvoke_delegate_wrapper_ReceivedNotificationResponse_t8_258,
	pinvoke_delegate_wrapper_SharingCompletion_t8_271,
	pinvoke_delegate_wrapper_AlertDialogCompletion_t8_300,
	pinvoke_delegate_wrapper_SingleFieldPromptCompletion_t8_301,
	pinvoke_delegate_wrapper_LoginPromptCompletion_t8_302,
};
