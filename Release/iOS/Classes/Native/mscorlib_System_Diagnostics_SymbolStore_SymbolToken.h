﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Diagnostics.SymbolStore.SymbolToken
struct  SymbolToken_t1_322 
{
	// System.Int32 System.Diagnostics.SymbolStore.SymbolToken::_val
	int32_t ____val_0;
};
