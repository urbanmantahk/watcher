﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings
struct  AndroidSettings_t8_255  : public Object_t
{
	// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::m_port
	int32_t ___m_port_0;
	// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::m_timeOutPeriod
	int32_t ___m_timeOutPeriod_1;
	// System.Int32 VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::m_maxRetryCount
	int32_t ___m_maxRetryCount_2;
	// System.Single VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings::m_timeGapBetweenPolling
	float ___m_timeGapBetweenPolling_3;
};
