﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName.h"

// System.Void Mono.Xml2.XmlTextReader/TagName::.ctor(System.String,System.String,System.String)
extern "C" void TagName__ctor_m4_790 (TagName_t4_171 * __this, String_t* ___n, String_t* ___l, String_t* ___p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void TagName_t4_171_marshal(const TagName_t4_171& unmarshaled, TagName_t4_171_marshaled& marshaled);
extern "C" void TagName_t4_171_marshal_back(const TagName_t4_171_marshaled& marshaled, TagName_t4_171& unmarshaled);
extern "C" void TagName_t4_171_marshal_cleanup(TagName_t4_171_marshaled& marshaled);
