﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.MutableBillingProduct
struct MutableBillingProduct_t8_206;
// VoxelBusters.NativePlugins.BillingProduct
struct BillingProduct_t8_207;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::.ctor()
extern "C" void MutableBillingProduct__ctor_m8_1185 (MutableBillingProduct_t8_206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::.ctor(VoxelBusters.NativePlugins.BillingProduct)
extern "C" void MutableBillingProduct__ctor_m8_1186 (MutableBillingProduct_t8_206 * __this, BillingProduct_t8_207 * ____product, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetIsConsumable(System.Boolean)
extern "C" void MutableBillingProduct_SetIsConsumable_m8_1187 (MutableBillingProduct_t8_206 * __this, bool ____isConsumable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetLocalizePrice(System.String)
extern "C" void MutableBillingProduct_SetLocalizePrice_m8_1188 (MutableBillingProduct_t8_206 * __this, String_t* ____locPrice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetCurrencyCode(System.String)
extern "C" void MutableBillingProduct_SetCurrencyCode_m8_1189 (MutableBillingProduct_t8_206 * __this, String_t* ____code, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.MutableBillingProduct::SetCurrencySymbol(System.String)
extern "C" void MutableBillingProduct_SetCurrencySymbol_m8_1190 (MutableBillingProduct_t8_206 * __this, String_t* ____symbol, const MethodInfo* method) IL2CPP_METHOD_ATTR;
