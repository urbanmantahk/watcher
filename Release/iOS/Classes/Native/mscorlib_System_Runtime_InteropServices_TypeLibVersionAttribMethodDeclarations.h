﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.TypeLibVersionAttribute
struct TypeLibVersionAttribute_t1_841;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
extern "C" void TypeLibVersionAttribute__ctor_m1_7933 (TypeLibVersionAttribute_t1_841 * __this, int32_t ___major, int32_t ___minor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.TypeLibVersionAttribute::get_MajorVersion()
extern "C" int32_t TypeLibVersionAttribute_get_MajorVersion_m1_7934 (TypeLibVersionAttribute_t1_841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.TypeLibVersionAttribute::get_MinorVersion()
extern "C" int32_t TypeLibVersionAttribute_get_MinorVersion_m1_7935 (TypeLibVersionAttribute_t1_841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
