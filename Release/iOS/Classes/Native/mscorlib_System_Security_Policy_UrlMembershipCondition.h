﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Policy.Url
struct Url_t1_1368;
// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Security.Policy.UrlMembershipCondition
struct  UrlMembershipCondition_t1_1369  : public Object_t
{
	// System.Int32 System.Security.Policy.UrlMembershipCondition::version
	int32_t ___version_0;
	// System.Security.Policy.Url System.Security.Policy.UrlMembershipCondition::url
	Url_t1_1368 * ___url_1;
	// System.String System.Security.Policy.UrlMembershipCondition::userUrl
	String_t* ___userUrl_2;
};
