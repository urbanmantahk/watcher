﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;

#include "mscorlib_System_Globalization_EastAsianLunisolarCalendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.TaiwanLunisolarCalendar
struct  TaiwanLunisolarCalendar_t1_388  : public EastAsianLunisolarCalendar_t1_358
{
};
struct TaiwanLunisolarCalendar_t1_388_StaticFields{
	// System.Globalization.CCEastAsianLunisolarEraHandler System.Globalization.TaiwanLunisolarCalendar::era_handler
	CCEastAsianLunisolarEraHandler_t1_355 * ___era_handler_9;
	// System.DateTime System.Globalization.TaiwanLunisolarCalendar::TaiwanMin
	DateTime_t1_150  ___TaiwanMin_10;
	// System.DateTime System.Globalization.TaiwanLunisolarCalendar::TaiwanMax
	DateTime_t1_150  ___TaiwanMax_11;
};
