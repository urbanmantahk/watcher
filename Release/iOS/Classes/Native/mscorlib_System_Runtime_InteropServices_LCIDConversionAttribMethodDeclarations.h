﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.LCIDConversionAttribute
struct LCIDConversionAttribute_t1_808;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.LCIDConversionAttribute::.ctor(System.Int32)
extern "C" void LCIDConversionAttribute__ctor_m1_7693 (LCIDConversionAttribute_t1_808 * __this, int32_t ___lcid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.LCIDConversionAttribute::get_Value()
extern "C" int32_t LCIDConversionAttribute_get_Value_m1_7694 (LCIDConversionAttribute_t1_808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
