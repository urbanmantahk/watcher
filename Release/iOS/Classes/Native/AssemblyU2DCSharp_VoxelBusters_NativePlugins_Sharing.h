﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// VoxelBusters.NativePlugins.Sharing/SharingCompletion
struct SharingCompletion_t8_271;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.NativePlugins.Sharing
struct  Sharing_t8_273  : public MonoBehaviour_t6_91
{
	// VoxelBusters.NativePlugins.Sharing/SharingCompletion VoxelBusters.NativePlugins.Sharing::OnSharingFinished
	SharingCompletion_t8_271 * ___OnSharingFinished_3;
	// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.Sharing::m_socialNetworkExcludedList
	eShareOptionsU5BU5D_t8_186* ___m_socialNetworkExcludedList_4;
};
