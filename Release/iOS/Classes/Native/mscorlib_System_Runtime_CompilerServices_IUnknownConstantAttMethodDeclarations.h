﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.IUnknownConstantAttribute
struct IUnknownConstantAttribute_t1_689;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.IUnknownConstantAttribute::.ctor()
extern "C" void IUnknownConstantAttribute__ctor_m1_7548 (IUnknownConstantAttribute_t1_689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.CompilerServices.IUnknownConstantAttribute::get_Value()
extern "C" Object_t * IUnknownConstantAttribute_get_Value_m1_7549 (IUnknownConstantAttribute_t1_689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
