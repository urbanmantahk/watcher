﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE
struct U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::.ctor()
extern "C" void U3CSendMailWithScreenshotU3Ec__AnonStoreyE__ctor_m8_1578 (U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/<SendMailWithScreenshot>c__AnonStoreyE::<>m__F(UnityEngine.Texture2D)
extern "C" void U3CSendMailWithScreenshotU3Ec__AnonStoreyE_U3CU3Em__F_m8_1579 (U3CSendMailWithScreenshotU3Ec__AnonStoreyE_t8_275 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
