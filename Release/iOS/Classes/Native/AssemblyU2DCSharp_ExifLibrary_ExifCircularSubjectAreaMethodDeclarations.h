﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifCircularSubjectArea
struct ExifCircularSubjectArea_t8_104;
// System.UInt16[]
struct UInt16U5BU5D_t1_1231;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void ExifLibrary.ExifCircularSubjectArea::.ctor(ExifLibrary.ExifTag,System.UInt16[])
extern "C" void ExifCircularSubjectArea__ctor_m8_444 (ExifCircularSubjectArea_t8_104 * __this, int32_t ___tag, UInt16U5BU5D_t1_1231* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.ExifCircularSubjectArea::get_Diamater()
extern "C" uint16_t ExifCircularSubjectArea_get_Diamater_m8_445 (ExifCircularSubjectArea_t8_104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifCircularSubjectArea::set_Diamater(System.UInt16)
extern "C" void ExifCircularSubjectArea_set_Diamater_m8_446 (ExifCircularSubjectArea_t8_104 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifCircularSubjectArea::ToString()
extern "C" String_t* ExifCircularSubjectArea_ToString_m8_447 (ExifCircularSubjectArea_t8_104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
