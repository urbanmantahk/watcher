﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.WebViewNative
struct WebViewNative_t8_311;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.WebView
struct WebView_t8_191;
// VoxelBusters.NativePlugins.WebViewMessage
struct WebViewMessage_t8_314;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eWebviewControl.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::.ctor()
extern "C" void WebViewNative__ctor_m8_1808 (WebViewNative_t8_311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidShow(System.String)
extern "C" void WebViewNative_WebViewDidShow_m8_1809 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidShow(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_WebViewDidShow_m8_1810 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidHide(System.String)
extern "C" void WebViewNative_WebViewDidHide_m8_1811 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidHide(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_WebViewDidHide_m8_1812 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidDestroy(System.String)
extern "C" void WebViewNative_WebViewDidDestroy_m8_1813 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidDestroy(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_WebViewDidDestroy_m8_1814 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidStartLoad(System.String)
extern "C" void WebViewNative_WebViewDidStartLoad_m8_1815 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidStartLoad(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_WebViewDidStartLoad_m8_1816 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishLoad(System.String)
extern "C" void WebViewNative_WebViewDidFinishLoad_m8_1817 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishLoad(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_WebViewDidFinishLoad_m8_1818 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFailLoadWithError(System.String)
extern "C" void WebViewNative_WebViewDidFailLoadWithError_m8_1819 (WebViewNative_t8_311 * __this, String_t* ____errorJsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFailLoadWithError(VoxelBusters.NativePlugins.WebView,System.String)
extern "C" void WebViewNative_WebViewDidFailLoadWithError_m8_1820 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishEvaluatingJS(System.String)
extern "C" void WebViewNative_WebViewDidFinishEvaluatingJS_m8_1821 (WebViewNative_t8_311 * __this, String_t* ____resultJsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidFinishEvaluatingJS(VoxelBusters.NativePlugins.WebView,System.String)
extern "C" void WebViewNative_WebViewDidFinishEvaluatingJS_m8_1822 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, String_t* ____result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidReceiveMessage(System.String)
extern "C" void WebViewNative_WebViewDidReceiveMessage_m8_1823 (WebViewNative_t8_311 * __this, String_t* ____dataJsonStr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::WebViewDidReceiveMessage(VoxelBusters.NativePlugins.WebView,VoxelBusters.NativePlugins.WebViewMessage)
extern "C" void WebViewNative_WebViewDidReceiveMessage_m8_1824 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, WebViewMessage_t8_314 * ____message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseLoadErrorData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void WebViewNative_ParseLoadErrorData_m8_1825 (WebViewNative_t8_311 * __this, Object_t * ____dataDict, String_t** ____tag, String_t** ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseEvalJSData(System.Collections.IDictionary,System.String&,System.String&)
extern "C" void WebViewNative_ParseEvalJSData_m8_1826 (WebViewNative_t8_311 * __this, Object_t * ____resultData, String_t** ____tag, String_t** ____result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ParseMessageData(System.Collections.IDictionary,System.String&,VoxelBusters.NativePlugins.WebViewMessage&)
extern "C" void WebViewNative_ParseMessageData_m8_1827 (WebViewNative_t8_311 * __this, Object_t * ____dataDict, String_t** ____tag, WebViewMessage_t8_314 ** ____message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Create(VoxelBusters.NativePlugins.WebView,UnityEngine.Rect)
extern "C" void WebViewNative_Create_m8_1828 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, Rect_t6_51  ____frame, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Destroy(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Destroy_m8_1829 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Show(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Show_m8_1830 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Hide(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Hide_m8_1831 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::LoadRequest(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_LoadRequest_m8_1832 (WebViewNative_t8_311 * __this, String_t* ____URL, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::LoadHTMLString(System.String,System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_LoadHTMLString_m8_1833 (WebViewNative_t8_311 * __this, String_t* ____HTMLString, String_t* ____baseURL, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::LoadData(System.Byte[],System.String,System.String,System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_LoadData_m8_1834 (WebViewNative_t8_311 * __this, ByteU5BU5D_t1_109* ____byteArray, String_t* ____MIMEType, String_t* ____textEncodingName, String_t* ____baseURL, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::EvaluateJavaScriptFromString(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_EvaluateJavaScriptFromString_m8_1835 (WebViewNative_t8_311 * __this, String_t* ____javaScript, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::Reload(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_Reload_m8_1836 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::StopLoading(VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_StopLoading_m8_1837 (WebViewNative_t8_311 * __this, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetCanHide(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetCanHide_m8_1838 (WebViewNative_t8_311 * __this, bool ____canHide, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetCanBounce(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetCanBounce_m8_1839 (WebViewNative_t8_311 * __this, bool ____canBounce, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetControlType(VoxelBusters.NativePlugins.eWebviewControlType,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetControlType_m8_1840 (WebViewNative_t8_311 * __this, int32_t ____type, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetShowSpinnerOnLoad(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetShowSpinnerOnLoad_m8_1841 (WebViewNative_t8_311 * __this, bool ____showSpinner, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetAutoShowOnLoadFinish(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetAutoShowOnLoadFinish_m8_1842 (WebViewNative_t8_311 * __this, bool ____autoShow, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetScalesPageToFit(System.Boolean,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetScalesPageToFit_m8_1843 (WebViewNative_t8_311 * __this, bool ____scaleToFit, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetFrame(UnityEngine.Rect,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetFrame_m8_1844 (WebViewNative_t8_311 * __this, Rect_t6_51  ____frame, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::SetBackgroundColor(UnityEngine.Color,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_SetBackgroundColor_m8_1845 (WebViewNative_t8_311 * __this, Color_t6_40  ____color, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::AddNewURLSchemeName(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_AddNewURLSchemeName_m8_1846 (WebViewNative_t8_311 * __this, String_t* ____newURLScheme, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::ClearCache()
extern "C" void WebViewNative_ClearCache_m8_1847 (WebViewNative_t8_311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.WebView VoxelBusters.NativePlugins.Internal.WebViewNative::GetWebViewWithTag(System.String)
extern "C" WebView_t8_191 * WebViewNative_GetWebViewWithTag_m8_1848 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::AddWebViewToCollection(System.String,VoxelBusters.NativePlugins.WebView)
extern "C" void WebViewNative_AddWebViewToCollection_m8_1849 (WebViewNative_t8_311 * __this, String_t* ____tag, WebView_t8_191 * ____webview, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.WebViewNative::RemoveWebViewFromCollection(System.String)
extern "C" void WebViewNative_RemoveWebViewFromCollection_m8_1850 (WebViewNative_t8_311 * __this, String_t* ____tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
