﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Xml.Serialization.XmlAttributeAttribute
struct XmlAttributeAttribute_t4_76;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.Serialization.XmlAttributeAttribute::.ctor(System.String)
extern "C" void XmlAttributeAttribute__ctor_m4_110 (XmlAttributeAttribute_t4_76 * __this, String_t* ___attributeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
