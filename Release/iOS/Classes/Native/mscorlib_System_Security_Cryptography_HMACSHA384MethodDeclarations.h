﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.HMACSHA384
struct HMACSHA384_t1_1216;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
extern "C" void HMACSHA384__ctor_m1_10366 (HMACSHA384_t1_1216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
extern "C" void HMACSHA384__ctor_m1_10367 (HMACSHA384_t1_1216 * __this, ByteU5BU5D_t1_109* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
extern "C" void HMACSHA384__cctor_m1_10368 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.HMACSHA384::get_ProduceLegacyHmacValues()
extern "C" bool HMACSHA384_get_ProduceLegacyHmacValues_m1_10369 (HMACSHA384_t1_1216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m1_10370 (HMACSHA384_t1_1216 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
