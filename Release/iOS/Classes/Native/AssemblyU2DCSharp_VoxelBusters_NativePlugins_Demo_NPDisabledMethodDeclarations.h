﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo
struct NPDisabledFeatureDemo_t8_175;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::.ctor()
extern "C" void NPDisabledFeatureDemo__ctor_m8_1053 (NPDisabledFeatureDemo_t8_175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::Start()
extern "C" void NPDisabledFeatureDemo_Start_m8_1054 (NPDisabledFeatureDemo_t8_175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDisabledFeatureDemo::OnGUIWindow()
extern "C" void NPDisabledFeatureDemo_OnGUIWindow_m8_1055 (NPDisabledFeatureDemo_t8_175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
