﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.SecurityTransparentAttribute
struct SecurityTransparentAttribute_t1_1409;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.SecurityTransparentAttribute::.ctor()
extern "C" void SecurityTransparentAttribute__ctor_m1_12160 (SecurityTransparentAttribute_t1_1409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
