﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibTypeFlags.h"

// System.Runtime.InteropServices.TypeLibTypeFlags
struct  TypeLibTypeFlags_t1_838 
{
	// System.Int32 System.Runtime.InteropServices.TypeLibTypeFlags::value__
	int32_t ___value___1;
};
