﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void Escape_t1_121_marshal(const Escape_t1_121& unmarshaled, Escape_t1_121_marshaled& marshaled);
extern "C" void Escape_t1_121_marshal_back(const Escape_t1_121_marshaled& marshaled, Escape_t1_121& unmarshaled);
extern "C" void Escape_t1_121_marshal_cleanup(Escape_t1_121_marshaled& marshaled);
