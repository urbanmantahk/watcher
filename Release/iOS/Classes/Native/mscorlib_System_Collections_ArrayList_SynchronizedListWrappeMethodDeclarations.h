﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/SynchronizedListWrapper
struct SynchronizedListWrapper_t1_268;
// System.Collections.IList
struct IList_t1_262;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/SynchronizedListWrapper::.ctor(System.Collections.IList)
extern "C" void SynchronizedListWrapper__ctor_m1_3002 (SynchronizedListWrapper_t1_268 * __this, Object_t * ___innerList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/SynchronizedListWrapper::get_Count()
extern "C" int32_t SynchronizedListWrapper_get_Count_m1_3003 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/SynchronizedListWrapper::get_IsSynchronized()
extern "C" bool SynchronizedListWrapper_get_IsSynchronized_m1_3004 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/SynchronizedListWrapper::get_SyncRoot()
extern "C" Object_t * SynchronizedListWrapper_get_SyncRoot_m1_3005 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/SynchronizedListWrapper::get_IsFixedSize()
extern "C" bool SynchronizedListWrapper_get_IsFixedSize_m1_3006 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/SynchronizedListWrapper::get_IsReadOnly()
extern "C" bool SynchronizedListWrapper_get_IsReadOnly_m1_3007 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/SynchronizedListWrapper::get_Item(System.Int32)
extern "C" Object_t * SynchronizedListWrapper_get_Item_m1_3008 (SynchronizedListWrapper_t1_268 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SynchronizedListWrapper::set_Item(System.Int32,System.Object)
extern "C" void SynchronizedListWrapper_set_Item_m1_3009 (SynchronizedListWrapper_t1_268 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/SynchronizedListWrapper::Add(System.Object)
extern "C" int32_t SynchronizedListWrapper_Add_m1_3010 (SynchronizedListWrapper_t1_268 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SynchronizedListWrapper::Clear()
extern "C" void SynchronizedListWrapper_Clear_m1_3011 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/SynchronizedListWrapper::Contains(System.Object)
extern "C" bool SynchronizedListWrapper_Contains_m1_3012 (SynchronizedListWrapper_t1_268 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/SynchronizedListWrapper::IndexOf(System.Object)
extern "C" int32_t SynchronizedListWrapper_IndexOf_m1_3013 (SynchronizedListWrapper_t1_268 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SynchronizedListWrapper::Insert(System.Int32,System.Object)
extern "C" void SynchronizedListWrapper_Insert_m1_3014 (SynchronizedListWrapper_t1_268 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SynchronizedListWrapper::Remove(System.Object)
extern "C" void SynchronizedListWrapper_Remove_m1_3015 (SynchronizedListWrapper_t1_268 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SynchronizedListWrapper::RemoveAt(System.Int32)
extern "C" void SynchronizedListWrapper_RemoveAt_m1_3016 (SynchronizedListWrapper_t1_268 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SynchronizedListWrapper::CopyTo(System.Array,System.Int32)
extern "C" void SynchronizedListWrapper_CopyTo_m1_3017 (SynchronizedListWrapper_t1_268 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.ArrayList/SynchronizedListWrapper::GetEnumerator()
extern "C" Object_t * SynchronizedListWrapper_GetEnumerator_m1_3018 (SynchronizedListWrapper_t1_268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
