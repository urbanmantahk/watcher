﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Threading.HostExecutionContext
struct  HostExecutionContext_t1_1463  : public Object_t
{
	// System.Object System.Threading.HostExecutionContext::_state
	Object_t * ____state_0;
};
