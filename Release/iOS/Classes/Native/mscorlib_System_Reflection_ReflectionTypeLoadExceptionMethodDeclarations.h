﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ReflectionTypeLoadException
struct ReflectionTypeLoadException_t1_633;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Exception[]
struct ExceptionU5BU5D_t1_634;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Reflection.ReflectionTypeLoadException::.ctor(System.Type[],System.Exception[])
extern "C" void ReflectionTypeLoadException__ctor_m1_7275 (ReflectionTypeLoadException_t1_633 * __this, TypeU5BU5D_t1_31* ___classes, ExceptionU5BU5D_t1_634* ___exceptions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ReflectionTypeLoadException::.ctor(System.Type[],System.Exception[],System.String)
extern "C" void ReflectionTypeLoadException__ctor_m1_7276 (ReflectionTypeLoadException_t1_633 * __this, TypeU5BU5D_t1_31* ___classes, ExceptionU5BU5D_t1_634* ___exceptions, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ReflectionTypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ReflectionTypeLoadException__ctor_m1_7277 (ReflectionTypeLoadException_t1_633 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.ReflectionTypeLoadException::get_Types()
extern "C" TypeU5BU5D_t1_31* ReflectionTypeLoadException_get_Types_m1_7278 (ReflectionTypeLoadException_t1_633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception[] System.Reflection.ReflectionTypeLoadException::get_LoaderExceptions()
extern "C" ExceptionU5BU5D_t1_634* ReflectionTypeLoadException_get_LoaderExceptions_m1_7279 (ReflectionTypeLoadException_t1_633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ReflectionTypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ReflectionTypeLoadException_GetObjectData_m1_7280 (ReflectionTypeLoadException_t1_633 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
