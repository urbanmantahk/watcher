﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.SurrogateEncoder
struct SurrogateEncoder_t1_1444;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Text.SurrogateEncoder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SurrogateEncoder__ctor_m1_12483 (SurrogateEncoder_t1_1444 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.SurrogateEncoder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void SurrogateEncoder_GetObjectData_m1_12484 (SurrogateEncoder_t1_1444 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.SurrogateEncoder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern "C" Object_t * SurrogateEncoder_GetRealObject_m1_12485 (SurrogateEncoder_t1_1444 * __this, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
