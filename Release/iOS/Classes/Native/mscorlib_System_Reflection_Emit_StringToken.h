﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Reflection.Emit.StringToken
struct  StringToken_t1_554 
{
	// System.Int32 System.Reflection.Emit.StringToken::tokValue
	int32_t ___tokValue_0;
};
