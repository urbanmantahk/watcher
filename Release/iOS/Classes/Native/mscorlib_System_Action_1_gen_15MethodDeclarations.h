﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_14MethodDeclarations.h"

// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1_18019(__this, ___object, ___method, method) (( void (*) (Action_1_t1_2201 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1_17973_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::Invoke(T)
#define Action_1_Invoke_m1_18020(__this, ___obj, method) (( void (*) (Action_1_t1_2201 *, KeyValuePair_2_t1_1805 , const MethodInfo*))Action_1_Invoke_m1_17974_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m1_18021(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t1_2201 *, KeyValuePair_2_t1_1805 , AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m1_17975_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m1_18022(__this, ___result, method) (( void (*) (Action_1_t1_2201 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m1_17976_gshared)(__this, ___result, method)
