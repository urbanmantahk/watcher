﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.FileDialogPermissionAttribute
struct FileDialogPermissionAttribute_t1_1273;
// System.Security.IPermission
struct IPermission_t1_1400;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.FileDialogPermissionAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void FileDialogPermissionAttribute__ctor_m1_10816 (FileDialogPermissionAttribute_t1_1273 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileDialogPermissionAttribute::get_Open()
extern "C" bool FileDialogPermissionAttribute_get_Open_m1_10817 (FileDialogPermissionAttribute_t1_1273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileDialogPermissionAttribute::set_Open(System.Boolean)
extern "C" void FileDialogPermissionAttribute_set_Open_m1_10818 (FileDialogPermissionAttribute_t1_1273 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.FileDialogPermissionAttribute::get_Save()
extern "C" bool FileDialogPermissionAttribute_get_Save_m1_10819 (FileDialogPermissionAttribute_t1_1273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.FileDialogPermissionAttribute::set_Save(System.Boolean)
extern "C" void FileDialogPermissionAttribute_set_Save_m1_10820 (FileDialogPermissionAttribute_t1_1273 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.FileDialogPermissionAttribute::CreatePermission()
extern "C" Object_t * FileDialogPermissionAttribute_CreatePermission_m1_10821 (FileDialogPermissionAttribute_t1_1273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
