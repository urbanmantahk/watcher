﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Specialized.ListDictionary
struct ListDictionary_t3_16;

#include "mscorlib_System_Object.h"

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t4_65  : public Object_t
{
	// System.Collections.Specialized.ListDictionary System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	ListDictionary_t3_16 * ___namespaces_0;
};
