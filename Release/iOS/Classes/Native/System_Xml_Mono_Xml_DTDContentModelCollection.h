﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "mscorlib_System_Object.h"

// Mono.Xml.DTDContentModelCollection
struct  DTDContentModelCollection_t4_93  : public Object_t
{
	// System.Collections.ArrayList Mono.Xml.DTDContentModelCollection::contentModel
	ArrayList_t1_170 * ___contentModel_0;
};
