﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.SynchronizedWriter
struct SynchronizedWriter_t1_456;
// System.IO.TextWriter
struct TextWriter_t1_449;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Text.Encoding
struct Encoding_t1_406;
// System.IFormatProvider
struct IFormatProvider_t1_455;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.IO.SynchronizedWriter::.ctor(System.IO.TextWriter)
extern "C" void SynchronizedWriter__ctor_m1_5357 (SynchronizedWriter_t1_456 * __this, TextWriter_t1_449 * ___writer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::.ctor(System.IO.TextWriter,System.Boolean)
extern "C" void SynchronizedWriter__ctor_m1_5358 (SynchronizedWriter_t1_456 * __this, TextWriter_t1_449 * ___writer, bool ___neverClose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Close()
extern "C" void SynchronizedWriter_Close_m1_5359 (SynchronizedWriter_t1_456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Flush()
extern "C" void SynchronizedWriter_Flush_m1_5360 (SynchronizedWriter_t1_456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Boolean)
extern "C" void SynchronizedWriter_Write_m1_5361 (SynchronizedWriter_t1_456 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Char)
extern "C" void SynchronizedWriter_Write_m1_5362 (SynchronizedWriter_t1_456 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Char[])
extern "C" void SynchronizedWriter_Write_m1_5363 (SynchronizedWriter_t1_456 * __this, CharU5BU5D_t1_16* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Decimal)
extern "C" void SynchronizedWriter_Write_m1_5364 (SynchronizedWriter_t1_456 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Int32)
extern "C" void SynchronizedWriter_Write_m1_5365 (SynchronizedWriter_t1_456 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Int64)
extern "C" void SynchronizedWriter_Write_m1_5366 (SynchronizedWriter_t1_456 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Object)
extern "C" void SynchronizedWriter_Write_m1_5367 (SynchronizedWriter_t1_456 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Single)
extern "C" void SynchronizedWriter_Write_m1_5368 (SynchronizedWriter_t1_456 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.String)
extern "C" void SynchronizedWriter_Write_m1_5369 (SynchronizedWriter_t1_456 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.UInt32)
extern "C" void SynchronizedWriter_Write_m1_5370 (SynchronizedWriter_t1_456 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.UInt64)
extern "C" void SynchronizedWriter_Write_m1_5371 (SynchronizedWriter_t1_456 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.String,System.Object)
extern "C" void SynchronizedWriter_Write_m1_5372 (SynchronizedWriter_t1_456 * __this, String_t* ___format, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.String,System.Object[])
extern "C" void SynchronizedWriter_Write_m1_5373 (SynchronizedWriter_t1_456 * __this, String_t* ___format, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.Char[],System.Int32,System.Int32)
extern "C" void SynchronizedWriter_Write_m1_5374 (SynchronizedWriter_t1_456 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.String,System.Object,System.Object)
extern "C" void SynchronizedWriter_Write_m1_5375 (SynchronizedWriter_t1_456 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::Write(System.String,System.Object,System.Object,System.Object)
extern "C" void SynchronizedWriter_Write_m1_5376 (SynchronizedWriter_t1_456 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine()
extern "C" void SynchronizedWriter_WriteLine_m1_5377 (SynchronizedWriter_t1_456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Boolean)
extern "C" void SynchronizedWriter_WriteLine_m1_5378 (SynchronizedWriter_t1_456 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Char)
extern "C" void SynchronizedWriter_WriteLine_m1_5379 (SynchronizedWriter_t1_456 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Char[])
extern "C" void SynchronizedWriter_WriteLine_m1_5380 (SynchronizedWriter_t1_456 * __this, CharU5BU5D_t1_16* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Decimal)
extern "C" void SynchronizedWriter_WriteLine_m1_5381 (SynchronizedWriter_t1_456 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Double)
extern "C" void SynchronizedWriter_WriteLine_m1_5382 (SynchronizedWriter_t1_456 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Int32)
extern "C" void SynchronizedWriter_WriteLine_m1_5383 (SynchronizedWriter_t1_456 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Int64)
extern "C" void SynchronizedWriter_WriteLine_m1_5384 (SynchronizedWriter_t1_456 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Object)
extern "C" void SynchronizedWriter_WriteLine_m1_5385 (SynchronizedWriter_t1_456 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Single)
extern "C" void SynchronizedWriter_WriteLine_m1_5386 (SynchronizedWriter_t1_456 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.String)
extern "C" void SynchronizedWriter_WriteLine_m1_5387 (SynchronizedWriter_t1_456 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.UInt32)
extern "C" void SynchronizedWriter_WriteLine_m1_5388 (SynchronizedWriter_t1_456 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.UInt64)
extern "C" void SynchronizedWriter_WriteLine_m1_5389 (SynchronizedWriter_t1_456 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.String,System.Object)
extern "C" void SynchronizedWriter_WriteLine_m1_5390 (SynchronizedWriter_t1_456 * __this, String_t* ___format, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.String,System.Object[])
extern "C" void SynchronizedWriter_WriteLine_m1_5391 (SynchronizedWriter_t1_456 * __this, String_t* ___format, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.Char[],System.Int32,System.Int32)
extern "C" void SynchronizedWriter_WriteLine_m1_5392 (SynchronizedWriter_t1_456 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.String,System.Object,System.Object)
extern "C" void SynchronizedWriter_WriteLine_m1_5393 (SynchronizedWriter_t1_456 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::WriteLine(System.String,System.Object,System.Object,System.Object)
extern "C" void SynchronizedWriter_WriteLine_m1_5394 (SynchronizedWriter_t1_456 * __this, String_t* ___format, Object_t * ___arg0, Object_t * ___arg1, Object_t * ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.IO.SynchronizedWriter::get_Encoding()
extern "C" Encoding_t1_406 * SynchronizedWriter_get_Encoding_m1_5395 (SynchronizedWriter_t1_456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IFormatProvider System.IO.SynchronizedWriter::get_FormatProvider()
extern "C" Object_t * SynchronizedWriter_get_FormatProvider_m1_5396 (SynchronizedWriter_t1_456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.IO.SynchronizedWriter::get_NewLine()
extern "C" String_t* SynchronizedWriter_get_NewLine_m1_5397 (SynchronizedWriter_t1_456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SynchronizedWriter::set_NewLine(System.String)
extern "C" void SynchronizedWriter_set_NewLine_m1_5398 (SynchronizedWriter_t1_456 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
