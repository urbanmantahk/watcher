﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_53.h"
#include "mscorlib_System_Reflection_Emit_MonoWin32Resource.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16161_gshared (InternalEnumerator_1_t1_2045 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16161(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2045 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16161_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16162_gshared (InternalEnumerator_1_t1_2045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16162(__this, method) (( void (*) (InternalEnumerator_1_t1_2045 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16162_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16163_gshared (InternalEnumerator_1_t1_2045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16163(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2045 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16163_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16164_gshared (InternalEnumerator_1_t1_2045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16164(__this, method) (( void (*) (InternalEnumerator_1_t1_2045 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16164_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16165_gshared (InternalEnumerator_1_t1_2045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16165(__this, method) (( bool (*) (InternalEnumerator_1_t1_2045 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16165_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoWin32Resource>::get_Current()
extern "C" MonoWin32Resource_t1_465  InternalEnumerator_1_get_Current_m1_16166_gshared (InternalEnumerator_1_t1_2045 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16166(__this, method) (( MonoWin32Resource_t1_465  (*) (InternalEnumerator_1_t1_2045 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16166_gshared)(__this, method)
