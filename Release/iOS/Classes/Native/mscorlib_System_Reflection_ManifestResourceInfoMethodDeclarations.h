﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ManifestResourceInfo
struct ManifestResourceInfo_t1_606;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ResourceLocation.h"

// System.Void System.Reflection.ManifestResourceInfo::.ctor()
extern "C" void ManifestResourceInfo__ctor_m1_6883 (ManifestResourceInfo_t1_606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ManifestResourceInfo::.ctor(System.Reflection.Assembly,System.String,System.Reflection.ResourceLocation)
extern "C" void ManifestResourceInfo__ctor_m1_6884 (ManifestResourceInfo_t1_606 * __this, Assembly_t1_467 * ___assembly, String_t* ___filename, int32_t ___location, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.ManifestResourceInfo::get_FileName()
extern "C" String_t* ManifestResourceInfo_get_FileName_m1_6885 (ManifestResourceInfo_t1_606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.ManifestResourceInfo::get_ReferencedAssembly()
extern "C" Assembly_t1_467 * ManifestResourceInfo_get_ReferencedAssembly_m1_6886 (ManifestResourceInfo_t1_606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ResourceLocation System.Reflection.ManifestResourceInfo::get_ResourceLocation()
extern "C" int32_t ManifestResourceInfo_get_ResourceLocation_m1_6887 (ManifestResourceInfo_t1_606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
