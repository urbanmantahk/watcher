﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct List_1_t1_1908;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_27198_gshared (Enumerator_t1_2721 * __this, List_1_t1_1908 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_27198(__this, ___l, method) (( void (*) (Enumerator_t1_2721 *, List_1_t1_1908 *, const MethodInfo*))Enumerator__ctor_m1_27198_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_27199_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_27199(__this, method) (( void (*) (Enumerator_t1_2721 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_27199_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_27200_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_27200(__this, method) (( Object_t * (*) (Enumerator_t1_2721 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_27200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Dispose()
extern "C" void Enumerator_Dispose_m1_27201_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_27201(__this, method) (( void (*) (Enumerator_t1_2721 *, const MethodInfo*))Enumerator_Dispose_m1_27201_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_27202_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_27202(__this, method) (( void (*) (Enumerator_t1_2721 *, const MethodInfo*))Enumerator_VerifyState_m1_27202_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_27203_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_27203(__this, method) (( bool (*) (Enumerator_t1_2721 *, const MethodInfo*))Enumerator_MoveNext_m1_27203_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Current()
extern "C" ConsoleLog_t8_170  Enumerator_get_Current_m1_27204_gshared (Enumerator_t1_2721 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_27204(__this, method) (( ConsoleLog_t8_170  (*) (Enumerator_t1_2721 *, const MethodInfo*))Enumerator_get_Current_m1_27204_gshared)(__this, method)
