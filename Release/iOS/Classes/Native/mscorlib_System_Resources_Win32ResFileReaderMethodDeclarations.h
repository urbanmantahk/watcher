﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32ResFileReader
struct Win32ResFileReader_t1_670;
// System.IO.Stream
struct Stream_t1_405;
// System.Resources.NameOrId
struct NameOrId_t1_663;
// System.Collections.ICollection
struct ICollection_t1_280;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.Win32ResFileReader::.ctor(System.IO.Stream)
extern "C" void Win32ResFileReader__ctor_m1_7514 (Win32ResFileReader_t1_670 * __this, Stream_t1_405 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.Win32ResFileReader::read_int16()
extern "C" int32_t Win32ResFileReader_read_int16_m1_7515 (Win32ResFileReader_t1_670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Resources.Win32ResFileReader::read_int32()
extern "C" int32_t Win32ResFileReader_read_int32_m1_7516 (Win32ResFileReader_t1_670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Resources.Win32ResFileReader::read_padding()
extern "C" void Win32ResFileReader_read_padding_m1_7517 (Win32ResFileReader_t1_670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.NameOrId System.Resources.Win32ResFileReader::read_ordinal()
extern "C" NameOrId_t1_663 * Win32ResFileReader_read_ordinal_m1_7518 (Win32ResFileReader_t1_670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection System.Resources.Win32ResFileReader::ReadResources()
extern "C" Object_t * Win32ResFileReader_ReadResources_m1_7519 (Win32ResFileReader_t1_670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
