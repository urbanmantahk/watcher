﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.DeriveBytes
struct DeriveBytes_t1_1207;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.DeriveBytes::.ctor()
extern "C" void DeriveBytes__ctor_m1_10326 (DeriveBytes_t1_1207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
