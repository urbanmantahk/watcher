﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_ReflectionPermissionFla.h"

// System.Security.Permissions.ReflectionPermission
struct  ReflectionPermission_t1_1301  : public CodeAccessPermission_t1_1268
{
	// System.Security.Permissions.ReflectionPermissionFlag System.Security.Permissions.ReflectionPermission::flags
	int32_t ___flags_1;
};
