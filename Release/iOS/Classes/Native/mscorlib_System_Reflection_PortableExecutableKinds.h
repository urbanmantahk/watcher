﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds.h"

// System.Reflection.PortableExecutableKinds
struct  PortableExecutableKinds_t1_630 
{
	// System.Int32 System.Reflection.PortableExecutableKinds::value__
	int32_t ___value___1;
};
