﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.IsolatedStorageFilePermission
struct IsolatedStorageFilePermission_t1_1284;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"

// System.Void System.Security.Permissions.IsolatedStorageFilePermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void IsolatedStorageFilePermission__ctor_m1_10928 (IsolatedStorageFilePermission_t1_1284 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.IsolatedStorageFilePermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t IsolatedStorageFilePermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_10929 (IsolatedStorageFilePermission_t1_1284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.IsolatedStorageFilePermission::Copy()
extern "C" Object_t * IsolatedStorageFilePermission_Copy_m1_10930 (IsolatedStorageFilePermission_t1_1284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.IsolatedStorageFilePermission::Intersect(System.Security.IPermission)
extern "C" Object_t * IsolatedStorageFilePermission_Intersect_m1_10931 (IsolatedStorageFilePermission_t1_1284 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.IsolatedStorageFilePermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool IsolatedStorageFilePermission_IsSubsetOf_m1_10932 (IsolatedStorageFilePermission_t1_1284 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.IsolatedStorageFilePermission::Union(System.Security.IPermission)
extern "C" Object_t * IsolatedStorageFilePermission_Union_m1_10933 (IsolatedStorageFilePermission_t1_1284 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.IsolatedStorageFilePermission::ToXml()
extern "C" SecurityElement_t1_242 * IsolatedStorageFilePermission_ToXml_m1_10934 (IsolatedStorageFilePermission_t1_1284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.IsolatedStorageFilePermission System.Security.Permissions.IsolatedStorageFilePermission::Cast(System.Security.IPermission)
extern "C" IsolatedStorageFilePermission_t1_1284 * IsolatedStorageFilePermission_Cast_m1_10935 (IsolatedStorageFilePermission_t1_1284 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
