﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AllowPartiallyTrustedCallersAttribute
struct AllowPartiallyTrustedCallersAttribute_t1_1387;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.AllowPartiallyTrustedCallersAttribute::.ctor()
extern "C" void AllowPartiallyTrustedCallersAttribute__ctor_m1_11899 (AllowPartiallyTrustedCallersAttribute_t1_1387 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
