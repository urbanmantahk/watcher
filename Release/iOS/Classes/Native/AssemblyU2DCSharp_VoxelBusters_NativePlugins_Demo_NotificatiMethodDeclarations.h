﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.NotificationDemo
struct NotificationDemo_t8_183;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_NotificationTyp.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"
#include "mscorlib_System_DateTime.h"

// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::.ctor()
extern "C" void NotificationDemo__ctor_m8_1061 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::Start()
extern "C" void NotificationDemo_Start_m8_1062 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::OnEnable()
extern "C" void NotificationDemo_OnEnable_m8_1063 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::OnDisable()
extern "C" void NotificationDemo_OnDisable_m8_1064 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DisplayFeatureFunctionalities()
extern "C" void NotificationDemo_DisplayFeatureFunctionalities_m8_1065 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DrawRegisterAPI()
extern "C" void NotificationDemo_DrawRegisterAPI_m8_1066 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DrawScheduleNotificationAPI()
extern "C" void NotificationDemo_DrawScheduleNotificationAPI_m8_1067 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DrawCancelNotificationAPI()
extern "C" void NotificationDemo_DrawCancelNotificationAPI_m8_1068 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::RegisterNotificationTypes(VoxelBusters.NativePlugins.NotificationType)
extern "C" void NotificationDemo_RegisterNotificationTypes_m8_1069 (NotificationDemo_t8_183 * __this, int32_t ____notificationTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::RegisterForRemoteNotifications()
extern "C" void NotificationDemo_RegisterForRemoteNotifications_m8_1070 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::UnregisterForRemoteNotifications()
extern "C" void NotificationDemo_UnregisterForRemoteNotifications_m8_1071 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Demo.NotificationDemo::ScheduleLocalNotification(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" String_t* NotificationDemo_ScheduleLocalNotification_m8_1072 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::CancelLocalNotification(System.String)
extern "C" void NotificationDemo_CancelLocalNotification_m8_1073 (NotificationDemo_t8_183 * __this, String_t* ____notificationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::CancelAllLocalNotifications()
extern "C" void NotificationDemo_CancelAllLocalNotifications_m8_1074 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::ClearNotifications()
extern "C" void NotificationDemo_ClearNotifications_m8_1075 (NotificationDemo_t8_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidLaunchWithLocalNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationDemo_DidLaunchWithLocalNotificationEvent_m8_1076 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidLaunchWithRemoteNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationDemo_DidLaunchWithRemoteNotificationEvent_m8_1077 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidReceiveLocalNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationDemo_DidReceiveLocalNotificationEvent_m8_1078 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidReceiveRemoteNotificationEvent(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationDemo_DidReceiveRemoteNotificationEvent_m8_1079 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::DidFinishRegisterForRemoteNotificationEvent(System.String,System.String)
extern "C" void NotificationDemo_DidFinishRegisterForRemoteNotificationEvent_m8_1080 (NotificationDemo_t8_183 * __this, String_t* ____deviceToken, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NotificationDemo::AppendNotificationResult(VoxelBusters.NativePlugins.CrossPlatformNotification)
extern "C" void NotificationDemo_AppendNotificationResult_m8_1081 (NotificationDemo_t8_183 * __this, CrossPlatformNotification_t8_259 * ____notification, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.Demo.NotificationDemo::CreateNotification(System.Int64,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" CrossPlatformNotification_t8_259 * NotificationDemo_CreateNotification_m8_1082 (NotificationDemo_t8_183 * __this, int64_t ____fireAfterSec, int32_t ____repeatInterval, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.Demo.NotificationDemo::CreateNotification(System.DateTime,VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" CrossPlatformNotification_t8_259 * NotificationDemo_CreateNotification_m8_1083 (NotificationDemo_t8_183 * __this, DateTime_t1_150  ____time, int32_t ____repeatInterval, const MethodInfo* method) IL2CPP_METHOD_ATTR;
