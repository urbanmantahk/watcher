﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.NPDemoBase
struct NPDemoBase_t8_174;
// System.String[]
struct StringU5BU5D_t1_238;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::.ctor()
extern "C" void NPDemoBase__ctor_m8_1046 (NPDemoBase_t8_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::Start()
extern "C" void NPDemoBase_Start_m8_1047 (NPDemoBase_t8_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::OnGUIWindow()
extern "C" void NPDemoBase_OnGUIWindow_m8_1048 (NPDemoBase_t8_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.NativePlugins.Demo.NPDemoBase::DisplayThingsToKnow()
extern "C" bool NPDemoBase_DisplayThingsToKnow_m8_1049 (NPDemoBase_t8_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::DisplayFeatureFunctionalities()
extern "C" void NPDemoBase_DisplayFeatureFunctionalities_m8_1050 (NPDemoBase_t8_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::AddExtraInfoTexts(System.String[])
extern "C" void NPDemoBase_AddExtraInfoTexts_m8_1051 (NPDemoBase_t8_174 * __this, StringU5BU5D_t1_238* ____infoTexts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Demo.NPDemoBase::SetFeatureInterfaceInfoText(System.String)
extern "C" void NPDemoBase_SetFeatureInterfaceInfoText_m8_1052 (NPDemoBase_t8_174 * __this, String_t* ____newText, const MethodInfo* method) IL2CPP_METHOD_ATTR;
