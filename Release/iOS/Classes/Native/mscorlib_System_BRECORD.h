﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.BRECORD
struct  BRECORD_t1_1618 
{
	// System.IntPtr System.BRECORD::pvRecord
	IntPtr_t ___pvRecord_0;
	// System.IntPtr System.BRECORD::pRecInfo
	IntPtr_t ___pRecInfo_1;
};
