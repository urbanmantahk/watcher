﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlNameTable
struct XmlNameTable_t4_67;

#include "mscorlib_System_Object.h"

// System.Xml.XmlImplementation
struct  XmlImplementation_t4_130  : public Object_t
{
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::InternalNameTable
	XmlNameTable_t4_67 * ___InternalNameTable_0;
};
