﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.AndroidTwitterUser
struct AndroidTwitterUser_t8_297;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Internal.AndroidTwitterUser::.ctor(System.Collections.IDictionary)
extern "C" void AndroidTwitterUser__ctor_m8_1729 (AndroidTwitterUser_t8_297 * __this, Object_t * ____userJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
