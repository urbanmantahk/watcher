﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t6_307;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t6_355;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t6_257;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t6_356;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Func`2<System.Object,System.Single>
struct Func_2_t5_22;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1_1834;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1_1837;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1_1836;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1_1835;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1_1838;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1_1824;
// VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>
struct ObserverPattern_1_t8_371;
// VoxelBusters.DesignPatterns.IObserver
struct IObserver_t8_374;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>
struct SingletonPattern_1_t8_372;
// UnityEngine.GameObject
struct GameObject_t6_97;
// UnityEngine.Transform
struct Transform_t6_65;
// VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>
struct AdvancedScriptableObject_1_t8_375;
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t1_2656;
// System.Collections.Generic.GenericEqualityComparer`1<System.Byte>
struct GenericEqualityComparer_1_t1_2657;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>
struct DefaultComparer_t1_2658;
// System.Array
struct Array_t;
// ExifLibrary.ExifEnumProperty`1<System.Object>
struct ExifEnumProperty_1_t8_378;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>
struct IEqualityComparer_1_t1_2662;
// System.Collections.Generic.IDictionary`2<ExifLibrary.ExifTag,System.Object>
struct IDictionary_2_t1_2875;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<ExifLibrary.ExifTag>
struct ICollection_1_t1_2876;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>[]
struct KeyValuePair_2U5BU5D_t1_2874;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1_869;
// System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1_2664;
// System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>
struct Transform_1_t1_2675;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>
struct IEnumerator_1_t1_2877;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>
struct KeyCollection_t1_2668;
// System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>
struct ValueCollection_t1_2672;
// System.Collections.Generic.IEnumerator`1<ExifLibrary.ExifTag>
struct IEnumerator_1_t1_2878;
// System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>
struct Transform_1_t1_2671;
// ExifLibrary.ExifTag[]
struct ExifTagU5BU5D_t8_379;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Object>
struct Transform_1_t1_2674;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>
struct ShimEnumerator_t1_2676;
// System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>
struct EqualityComparer_1_t1_2677;
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExifLibrary.ExifTag>
struct DefaultComparer_t1_2678;
// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t1_2682;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t1_2771;
// System.Collections.Generic.ICollection`1<ExifLibrary.IFD>
struct ICollection_1_t1_2880;
// System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>[]
struct KeyValuePair_2U5BU5D_t1_2680;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>
struct IEnumerator_1_t1_2881;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t1_1922;
// System.Collections.Generic.IList`1<ExifLibrary.IFD>
struct IList_1_t1_1923;
// System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>
struct ListKeys_t3_279;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t1_2773;
// System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>
struct GetEnumeratorU3Ec__Iterator2_t3_280;
// System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>
struct ListValues_t3_282;
// ExifLibrary.IFD[]
struct IFDU5BU5D_t8_387;
// System.Collections.Generic.IEnumerator`1<ExifLibrary.IFD>
struct IEnumerator_1_t1_2882;
// System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>
struct GetEnumeratorU3Ec__Iterator3_t3_283;
// System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>
struct Comparer_1_t1_2685;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>
struct DefaultComparer_t1_2686;
// System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>
struct GetEnumeratorU3Ec__Iterator0_t3_285;
// System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>
struct Enumerator_t3_286;
// System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>
struct U3CGetEnumeratorU3Ec__Iterator1_t3_288;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>
struct ExifEnumProperty_1_t8_338;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>
struct ExifEnumProperty_1_t8_339;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>
struct ExifEnumProperty_1_t8_340;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>
struct ExifEnumProperty_1_t8_341;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>
struct ExifEnumProperty_1_t8_342;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>
struct ExifEnumProperty_1_t8_343;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>
struct ExifEnumProperty_1_t8_344;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>
struct ExifEnumProperty_1_t8_345;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>
struct ExifEnumProperty_1_t8_346;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>
struct ExifEnumProperty_1_t8_347;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>
struct ExifEnumProperty_1_t8_348;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>
struct ExifEnumProperty_1_t8_349;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>
struct ExifEnumProperty_1_t8_350;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>
struct ExifEnumProperty_1_t8_351;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>
struct ExifEnumProperty_1_t8_352;
// ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>
struct ExifEnumProperty_1_t8_353;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_5.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_5MethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_12.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtensionsMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_12MethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "mscorlib_System_Boolean.h"
#include "System_Core_System_Func_2_gen_4.h"
#include "System_Core_System_Func_2_gen_4MethodDeclarations.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_1.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_13.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_7.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_13MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_5.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_5MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_7MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_2.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_14.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_10.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_14MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_6.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_6MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_10MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_3.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_15.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_15MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_7.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_4.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_16.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_8.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_16MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_8.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_8MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_8MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_5.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_5MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_17.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_17MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_9.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_9MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_6.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen_6MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_18.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_15.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_18MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_10.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_10MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_15MethodDeclarations.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_ObserverPatter.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_ObserverPatterMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_55MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_55.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatte_0.h"
#include "AssemblyU2DCSharp_VoxelBusters_DesignPatterns_SingletonPatte_0MethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj_1.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Char.h"
#include "UnityEngine_UnityEngine_Resources.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_18.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_18MethodDeclarations.h"
#include "mscorlib_System_ActivatorMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_18.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_18MethodDeclarations.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__6.h"
#include "mscorlib_System_Collections_Generic_GenericEqualityComparer__6MethodDeclarations.h"
#include "mscorlib_System_ByteMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_176MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"
#include "mscorlib_System_ArrayMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_177.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_177MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_Fraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_31.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_31MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifPropertyMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTagFactoryMethodDeclarations.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperabilityMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterExMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownEnumTypeExceptionMethodDeclarations.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_UInt32.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"
#include "AssemblyU2DCSharp_ExifLibrary_UnknownEnumTypeException.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_24.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_24MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_34.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_34MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_35.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_35MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_4MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_19MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_19.h"
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_27MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_178.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_178MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_179.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_179MethodDeclarations.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_32MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_33.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_33MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_19.h"
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_19MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_gen.h"
#include "System_System_Collections_Generic_SortedList_2_genMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"
#include "System_System_Collections_Generic_SortedList_2_ListKeys_gen_0.h"
#include "System_System_Collections_Generic_SortedList_2_ListKeys_gen_0MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_ListValues_ge_0.h"
#include "System_System_Collections_Generic_SortedList_2_ListValues_ge_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_16MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_16.h"
#include "System_System_Collections_Generic_SortedList_2_U3CIEnumerabl_0.h"
#include "System_System_Collections_Generic_SortedList_2_U3CIEnumerabl_0MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_Enumerator_ge_0.h"
#include "System_System_Collections_Generic_SortedList_2_Enumerator_ge_0MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_EnumeratorMod_0.h"
#include "System_System_Collections_Generic_SortedList_2_U3CGetEnumera_0.h"
#include "System_System_Collections_Generic_SortedList_2_U3CGetEnumera_0MethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_7MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_7.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_180.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_180MethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_181.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_181MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_ListKeys_U3CI_0.h"
#include "System_System_Collections_Generic_SortedList_2_ListKeys_U3CI_0MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_KeyEnumerator_0.h"
#include "System_System_Collections_Generic_SortedList_2_KeyEnumerator_0MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_ListValues_U3_0.h"
#include "System_System_Collections_Generic_SortedList_2_ListValues_U3_0MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_ValueEnumerat_0.h"
#include "System_System_Collections_Generic_SortedList_2_ValueEnumerat_0MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_16.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_16MethodDeclarations.h"
#include "System_System_Collections_Generic_SortedList_2_EnumeratorMod_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_genMethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Compression.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_0.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_0MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_PhotometricInterpretation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_1.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_1MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Orientation.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_2.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_2MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_PlanarConfiguration.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_3.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_3MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_YCbCrPositioning.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_4.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_4MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ResolutionUnit.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_5.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_5MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ColorSpace.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_6.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_6MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureProgram.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_7.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_7MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_MeteringMode.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_8.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_8MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_LightSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_9.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_9MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_Flash.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_10.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_10MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SensingMethod.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_11.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_11MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_FileSource.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_12.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_12MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_SceneType.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_13.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_13MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_CustomRendered.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_14.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifEnumProperty_1_gen_14MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExposureMode.h"

// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector2>(System.Object)
extern "C" void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t6_47_m6_2128_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t6_47_m6_2128(__this /* static, unused */, p0, method) (( void (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t6_47_m6_2128_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m6_1911_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m6_1911(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m6_1911_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t6_97_m6_1880(__this /* static, unused */, p0, method) (( GameObject_t6_97 * (*) (Object_t * /* static, unused */, GameObject_t6_97 *, const MethodInfo*))Object_Instantiate_TisObject_t_m6_1911_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m6_1905_gshared (GameObject_t6_97 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m6_1905(__this, method) (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m6_1905_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m6_1906_gshared (GameObject_t6_97 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m6_1906(__this, method) (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m6_1906_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C" Object_t * Resources_Load_TisObject_t_m6_2117_gshared (Object_t * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisObject_t_m6_2117(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisObject_t_m6_2117_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Array::InternalArray__get_Item<ExifLibrary.MathEx/UFraction32>(System.Int32)
extern "C" UFraction32_t8_121  Array_InternalArray__get_Item_TisUFraction32_t8_121_m1_29048_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUFraction32_t8_121_m1_29048(__this, p0, method) (( UFraction32_t8_121  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUFraction32_t8_121_m1_29048_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ExifLibrary.MathEx/Fraction32>(System.Int32)
extern "C" Fraction32_t8_127  Array_InternalArray__get_Item_TisFraction32_t8_127_m1_29057_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFraction32_t8_127_m1_29057(__this, p0, method) (( Fraction32_t8_127  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFraction32_t8_127_m1_29057_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_CopyTo<System.Collections.DictionaryEntry,System.Collections.DictionaryEntry>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1_284_TisDictionaryEntry_t1_284_m1_29089_gshared (Dictionary_2_t1_2663 * __this, DictionaryEntryU5BU5D_t1_869* p0, int32_t p1, Transform_1_t1_2664 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1_284_TisDictionaryEntry_t1_284_m1_29089(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, DictionaryEntryU5BU5D_t1_869*, int32_t, Transform_1_t1_2664 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisDictionaryEntry_t1_284_TisDictionaryEntry_t1_284_m1_29089_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_ICollectionCopyTo<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1_2665_m1_29090_gshared (Dictionary_2_t1_2663 * __this, Array_t * p0, int32_t p1, Transform_1_t1_2675 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1_2665_m1_29090(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, Transform_1_t1_2675 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisKeyValuePair_2_t1_2665_m1_29090_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_CopyTo<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>,System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1_2665_TisKeyValuePair_2_t1_2665_m1_29092_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2U5BU5D_t1_2874* p0, int32_t p1, Transform_1_t1_2675 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1_2665_TisKeyValuePair_2_t1_2665_m1_29092(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, Transform_1_t1_2675 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisKeyValuePair_2_t1_2665_TisKeyValuePair_2_t1_2665_m1_29092_gshared)(__this, p0, p1, p2, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>(System.Int32)
extern "C" KeyValuePair_2_t1_2665  Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2665_m1_29066_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2665_m1_29066(__this, p0, method) (( KeyValuePair_2_t1_2665  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2665_m1_29066_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ExifLibrary.ExifTag>(System.Int32)
extern "C" int32_t Array_InternalArray__get_Item_TisExifTag_t8_132_m1_29075_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisExifTag_t8_132_m1_29075(__this, p0, method) (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisExifTag_t8_132_m1_29075_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_ICollectionCopyTo<ExifLibrary.ExifTag>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisExifTag_t8_132_m1_29084_gshared (Dictionary_2_t1_2663 * __this, Array_t * p0, int32_t p1, Transform_1_t1_2671 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisExifTag_t8_132_m1_29084(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, Transform_1_t1_2671 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisExifTag_t8_132_m1_29084_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_CopyTo<ExifLibrary.ExifTag,ExifLibrary.ExifTag>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisExifTag_t8_132_TisExifTag_t8_132_m1_29086_gshared (Dictionary_2_t1_2663 * __this, ExifTagU5BU5D_t8_379* p0, int32_t p1, Transform_1_t1_2671 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisExifTag_t8_132_TisExifTag_t8_132_m1_29086(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, ExifTagU5BU5D_t8_379*, int32_t, Transform_1_t1_2671 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisExifTag_t8_132_TisExifTag_t8_132_m1_29086_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m1_29087_gshared (Dictionary_2_t1_2663 * __this, Array_t * p0, int32_t p1, Transform_1_t1_2674 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m1_29087(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, Transform_1_t1_2674 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisObject_t_m1_29087_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m1_29088_gshared (Dictionary_2_t1_2663 * __this, ObjectU5BU5D_t1_272* p0, int32_t p1, Transform_1_t1_2674 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m1_29088(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t1_2663 *, ObjectU5BU5D_t1_272*, int32_t, Transform_1_t1_2674 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisObject_t_TisObject_t_m1_29088_gshared)(__this, p0, p1, p2, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>(System.Int32)
extern "C" KeyValuePair_2_t1_2681  Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2681_m1_29093_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2681_m1_29093(__this, p0, method) (( KeyValuePair_2_t1_2681  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1_2681_m1_29093_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ExifLibrary.IFD>(System.Int32)
extern "C" int32_t Array_InternalArray__get_Item_TisIFD_t8_131_m1_29102_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIFD_t8_131_m1_29102(__this, p0, method) (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIFD_t8_131_m1_29102_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern "C" void UnityEvent_1__ctor_m6_1898_gshared (UnityEvent_1_t6_307 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 1));
		NullCheck((UnityEventBase_t6_264 *)__this);
		UnityEventBase__ctor_m6_1796((UnityEventBase_t6_264 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m6_2075_gshared (UnityEvent_1_t6_307 * __this, UnityAction_1_t6_355 * ___call, const MethodInfo* method)
{
	{
		UnityAction_1_t6_355 * L_0 = ___call;
		BaseInvokableCall_t6_257 * L_1 = (( BaseInvokableCall_t6_257 * (*) (Object_t * /* static, unused */, UnityAction_1_t6_355 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(NULL /*static, unused*/, (UnityAction_1_t6_355 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NullCheck((UnityEventBase_t6_264 *)__this);
		UnityEventBase_AddCall_m6_1803((UnityEventBase_t6_264 *)__this, (BaseInvokableCall_t6_257 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m6_2076_gshared (UnityEvent_1_t6_307 * __this, UnityAction_1_t6_355 * ___call, const MethodInfo* method)
{
	{
		UnityAction_1_t6_355 * L_0 = ___call;
		NullCheck((Delegate_t1_22 *)L_0);
		Object_t * L_1 = Delegate_get_Target_m1_884((Delegate_t1_22 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t6_355 * L_2 = ___call;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m6_1825(NULL /*static, unused*/, (Delegate_t1_22 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t6_264 *)__this);
		UnityEventBase_RemoveListener_m6_1804((UnityEventBase_t6_264 *)__this, (Object_t *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::FindMethod_Impl(System.String,System.Object)
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m6_2077_gshared (UnityEvent_1_t6_307 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___targetObj;
		String_t* L_1 = ___name;
		TypeU5BU5D_t1_31* L_2 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 0, sizeof(Type_t *))) = (Type_t *)L_3;
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m6_1807(NULL /*static, unused*/, (Object_t *)L_0, (String_t*)L_1, (TypeU5BU5D_t1_31*)L_2, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t6_257 * UnityEvent_1_GetDelegate_m6_2078_gshared (UnityEvent_1_t6_307 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___target;
		MethodInfo_t * L_1 = ___theFunction;
		InvokableCall_1_t6_356 * L_2 = (InvokableCall_1_t6_356 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		(( void (*) (InvokableCall_1_t6_356 *, Object_t *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_2, (Object_t *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t6_257 * UnityEvent_1_GetDelegate_m6_2079_gshared (Object_t * __this /* static, unused */, UnityAction_1_t6_355 * ___action, const MethodInfo* method)
{
	{
		UnityAction_1_t6_355 * L_0 = ___action;
		InvokableCall_1_t6_356 * L_1 = (InvokableCall_1_t6_356 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		(( void (*) (InvokableCall_1_t6_356 *, UnityAction_1_t6_355 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_1, (UnityAction_1_t6_355 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m6_1900_gshared (UnityEvent_1_t6_307 * __this, Vector2_t6_47  ___arg0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t1_272* L_0 = (ObjectU5BU5D_t1_272*)(__this->___m_InvokeArray_4);
		Vector2_t6_47  L_1 = ___arg0;
		Vector2_t6_47  L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = (ObjectU5BU5D_t1_272*)(__this->___m_InvokeArray_4);
		NullCheck((UnityEventBase_t6_264 *)__this);
		UnityEventBase_Invoke_m6_1805((UnityEventBase_t6_264 *)__this, (ObjectU5BU5D_t1_272*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAction_1__ctor_m6_2080_gshared (UnityAction_1_t6_355 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C" void UnityAction_1_Invoke_m6_2081_gshared (UnityAction_1_t6_355 * __this, Vector2_t6_47  ___arg0, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAction_1_Invoke_m6_2081((UnityAction_1_t6_355 *)__this->___prev_9,___arg0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Vector2_t6_47  ___arg0, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Vector2_t6_47  ___arg0, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern TypeInfo* Vector2_t6_47_il2cpp_TypeInfo_var;
extern "C" Object_t * UnityAction_1_BeginInvoke_m6_2082_gshared (UnityAction_1_t6_355 * __this, Vector2_t6_47  ___arg0, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t6_47_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1588);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t6_47_il2cpp_TypeInfo_var, &___arg0);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" void UnityAction_1_EndInvoke_m6_2083_gshared (UnityAction_1_t6_355 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void InvokableCall_1__ctor_m6_2084_gshared (InvokableCall_1_t6_356 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___target;
		MethodInfo_t * L_1 = ___theFunction;
		NullCheck((BaseInvokableCall_t6_257 *)__this);
		BaseInvokableCall__ctor_m6_1774((BaseInvokableCall_t6_257 *)__this, (Object_t *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		UnityAction_1_t6_355 * L_2 = (UnityAction_1_t6_355 *)(__this->___Delegate_0);
		MethodInfo_t * L_3 = ___theFunction;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t * L_5 = ___target;
		Delegate_t1_22 * L_6 = NetFxCoreExtensions_CreateDelegate_m6_1824(NULL /*static, unused*/, (MethodInfo_t *)L_3, (Type_t *)L_4, (Object_t *)L_5, /*hidden argument*/NULL);
		Delegate_t1_22 * L_7 = Delegate_Combine_m1_908(NULL /*static, unused*/, (Delegate_t1_22 *)L_2, (Delegate_t1_22 *)((UnityAction_1_t6_355 *)Castclass(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), /*hidden argument*/NULL);
		__this->___Delegate_0 = ((UnityAction_1_t6_355 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m6_2085_gshared (InvokableCall_1_t6_356 * __this, UnityAction_1_t6_355 * ___action, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t6_257 *)__this);
		BaseInvokableCall__ctor_m6_1773((BaseInvokableCall_t6_257 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t6_355 * L_0 = (UnityAction_1_t6_355 *)(__this->___Delegate_0);
		UnityAction_1_t6_355 * L_1 = ___action;
		Delegate_t1_22 * L_2 = Delegate_Combine_m1_908(NULL /*static, unused*/, (Delegate_t1_22 *)L_0, (Delegate_t1_22 *)L_1, /*hidden argument*/NULL);
		__this->___Delegate_0 = ((UnityAction_1_t6_355 *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4966;
extern "C" void InvokableCall_1_Invoke_m6_2086_gshared (InvokableCall_1_t6_356 * __this, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4966 = il2cpp_codegen_string_literal_from_index(4966);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1_272* L_0 = ___args;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t1_1425 * L_1 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_1, (String_t*)_stringLiteral4966, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0014:
	{
		ObjectU5BU5D_t1_272* L_2 = ___args;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		(( void (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_2, L_3, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		UnityAction_1_t6_355 * L_4 = (UnityAction_1_t6_355 *)(__this->___Delegate_0);
		bool L_5 = BaseInvokableCall_AllowInvoke_m6_1775(NULL /*static, unused*/, (Delegate_t1_22 *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t6_355 * L_6 = (UnityAction_1_t6_355 *)(__this->___Delegate_0);
		ObjectU5BU5D_t1_272* L_7 = ___args;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		int32_t L_8 = 0;
		NullCheck((UnityAction_1_t6_355 *)L_6);
		(( void (*) (UnityAction_1_t6_355 *, Vector2_t6_47 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((UnityAction_1_t6_355 *)L_6, (Vector2_t6_47 )((*(Vector2_t6_47 *)((Vector2_t6_47 *)UnBox ((*(Object_t **)(Object_t **)SZArrayLdElema(L_7, L_8, sizeof(Object_t *))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m6_2087_gshared (InvokableCall_1_t6_356 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t6_355 * L_0 = (UnityAction_1_t6_355 *)(__this->___Delegate_0);
		NullCheck((Delegate_t1_22 *)L_0);
		Object_t * L_1 = Delegate_get_Target_m1_884((Delegate_t1_22 *)L_0, /*hidden argument*/NULL);
		Object_t * L_2 = ___targetObj;
		if ((!(((Object_t*)(Object_t *)L_1) == ((Object_t*)(Object_t *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t6_355 * L_3 = (UnityAction_1_t6_355 *)(__this->___Delegate_0);
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m6_1825(NULL /*static, unused*/, (Delegate_t1_22 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method;
		G_B3_0 = ((((Object_t*)(MethodInfo_t *)L_4) == ((Object_t*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Void System.Func`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void Func_2__ctor_m5_81_gshared (Func_2_t5_22 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C" float Func_2_Invoke_m5_82_gshared (Func_2_t5_22 * __this, Object_t * ___arg1, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Func_2_Invoke_m5_82((Func_2_t5_22 *)__this->___prev_9,___arg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg1, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t * __this, Object_t * ___arg1, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef float (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Func_2_BeginInvoke_m5_84_gshared (Func_2_t5_22 * __this, Object_t * ___arg1, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg1;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`2<System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C" float Func_2_EndInvoke_m5_86_gshared (Func_2_t5_22 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C" void ListPool_1__cctor_m7_1699_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t6_359 * L_0 = ((ListPool_1_t7_192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1) };
		UnityAction_1_t6_359 * L_2 = (UnityAction_1_t6_359 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (UnityAction_1_t6_359 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Object_t *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t7_192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t6_359 * L_3 = ((ListPool_1_t7_192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t7_227 * L_4 = (ObjectPool_1_t7_227 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (ObjectPool_1_t7_227 *, UnityAction_1_t6_359 *, UnityAction_1_t6_359 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t6_359 *)G_B2_0, (UnityAction_1_t6_359 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t7_192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C" List_1_t1_1834 * ListPool_1_Get_m7_1507_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_227 * L_0 = ((ListPool_1_t7_192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		NullCheck((ObjectPool_1_t7_227 *)L_0);
		List_1_t1_1834 * L_1 = (( List_1_t1_1834 * (*) (ObjectPool_1_t7_227 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t7_227 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m7_1512_gshared (Object_t * __this /* static, unused */, List_1_t1_1834 * ___toRelease, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_227 * L_0 = ((ListPool_1_t7_192_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		List_1_t1_1834 * L_1 = ___toRelease;
		NullCheck((ObjectPool_1_t7_227 *)L_0);
		(( void (*) (ObjectPool_1_t7_227 *, List_1_t1_1834 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t7_227 *)L_0, (List_1_t1_1834 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m7_1700_gshared (Object_t * __this /* static, unused */, List_1_t1_1834 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1834 * L_0 = ___l;
		NullCheck((List_1_t1_1834 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear() */, (List_1_t1_1834 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C" void ListPool_1__cctor_m7_1708_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t6_360 * L_0 = ((ListPool_1_t7_193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1) };
		UnityAction_1_t6_360 * L_2 = (UnityAction_1_t6_360 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (UnityAction_1_t6_360 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Object_t *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t7_193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t6_360 * L_3 = ((ListPool_1_t7_193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t7_228 * L_4 = (ObjectPool_1_t7_228 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (ObjectPool_1_t7_228 *, UnityAction_1_t6_360 *, UnityAction_1_t6_360 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t6_360 *)G_B2_0, (UnityAction_1_t6_360 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t7_193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C" List_1_t1_1837 * ListPool_1_Get_m7_1508_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_228 * L_0 = ((ListPool_1_t7_193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		NullCheck((ObjectPool_1_t7_228 *)L_0);
		List_1_t1_1837 * L_1 = (( List_1_t1_1837 * (*) (ObjectPool_1_t7_228 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t7_228 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m7_1513_gshared (Object_t * __this /* static, unused */, List_1_t1_1837 * ___toRelease, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_228 * L_0 = ((ListPool_1_t7_193_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		List_1_t1_1837 * L_1 = ___toRelease;
		NullCheck((ObjectPool_1_t7_228 *)L_0);
		(( void (*) (ObjectPool_1_t7_228 *, List_1_t1_1837 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t7_228 *)L_0, (List_1_t1_1837 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m7_1709_gshared (Object_t * __this /* static, unused */, List_1_t1_1837 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1837 * L_0 = ___l;
		NullCheck((List_1_t1_1837 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Clear() */, (List_1_t1_1837 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C" void ListPool_1__cctor_m7_1717_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t6_361 * L_0 = ((ListPool_1_t7_194_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1) };
		UnityAction_1_t6_361 * L_2 = (UnityAction_1_t6_361 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (UnityAction_1_t6_361 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Object_t *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t7_194_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t6_361 * L_3 = ((ListPool_1_t7_194_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t7_229 * L_4 = (ObjectPool_1_t7_229 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (ObjectPool_1_t7_229 *, UnityAction_1_t6_361 *, UnityAction_1_t6_361 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t6_361 *)G_B2_0, (UnityAction_1_t6_361 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t7_194_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C" List_1_t1_1836 * ListPool_1_Get_m7_1509_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_229 * L_0 = ((ListPool_1_t7_194_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		NullCheck((ObjectPool_1_t7_229 *)L_0);
		List_1_t1_1836 * L_1 = (( List_1_t1_1836 * (*) (ObjectPool_1_t7_229 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t7_229 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m7_1514_gshared (Object_t * __this /* static, unused */, List_1_t1_1836 * ___toRelease, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_229 * L_0 = ((ListPool_1_t7_194_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		List_1_t1_1836 * L_1 = ___toRelease;
		NullCheck((ObjectPool_1_t7_229 *)L_0);
		(( void (*) (ObjectPool_1_t7_229 *, List_1_t1_1836 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t7_229 *)L_0, (List_1_t1_1836 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m7_1718_gshared (Object_t * __this /* static, unused */, List_1_t1_1836 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1836 * L_0 = ___l;
		NullCheck((List_1_t1_1836 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Clear() */, (List_1_t1_1836 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
extern "C" void ListPool_1__cctor_m7_1726_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t6_362 * L_0 = ((ListPool_1_t7_195_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1) };
		UnityAction_1_t6_362 * L_2 = (UnityAction_1_t6_362 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (UnityAction_1_t6_362 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Object_t *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t7_195_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t6_362 * L_3 = ((ListPool_1_t7_195_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t7_230 * L_4 = (ObjectPool_1_t7_230 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (ObjectPool_1_t7_230 *, UnityAction_1_t6_362 *, UnityAction_1_t6_362 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t6_362 *)G_B2_0, (UnityAction_1_t6_362 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t7_195_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
extern "C" List_1_t1_1835 * ListPool_1_Get_m7_1510_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_230 * L_0 = ((ListPool_1_t7_195_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		NullCheck((ObjectPool_1_t7_230 *)L_0);
		List_1_t1_1835 * L_1 = (( List_1_t1_1835 * (*) (ObjectPool_1_t7_230 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t7_230 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m7_1515_gshared (Object_t * __this /* static, unused */, List_1_t1_1835 * ___toRelease, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_230 * L_0 = ((ListPool_1_t7_195_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		List_1_t1_1835 * L_1 = ___toRelease;
		NullCheck((ObjectPool_1_t7_230 *)L_0);
		(( void (*) (ObjectPool_1_t7_230 *, List_1_t1_1835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t7_230 *)L_0, (List_1_t1_1835 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m7_1727_gshared (Object_t * __this /* static, unused */, List_1_t1_1835 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1835 * L_0 = ___l;
		NullCheck((List_1_t1_1835 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Clear() */, (List_1_t1_1835 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C" void ListPool_1__cctor_m7_1735_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t6_363 * L_0 = ((ListPool_1_t7_196_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1) };
		UnityAction_1_t6_363 * L_2 = (UnityAction_1_t6_363 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (UnityAction_1_t6_363 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Object_t *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t7_196_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t6_363 * L_3 = ((ListPool_1_t7_196_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t7_231 * L_4 = (ObjectPool_1_t7_231 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (ObjectPool_1_t7_231 *, UnityAction_1_t6_363 *, UnityAction_1_t6_363 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t6_363 *)G_B2_0, (UnityAction_1_t6_363 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t7_196_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C" List_1_t1_1838 * ListPool_1_Get_m7_1511_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_231 * L_0 = ((ListPool_1_t7_196_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		NullCheck((ObjectPool_1_t7_231 *)L_0);
		List_1_t1_1838 * L_1 = (( List_1_t1_1838 * (*) (ObjectPool_1_t7_231 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t7_231 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m7_1516_gshared (Object_t * __this /* static, unused */, List_1_t1_1838 * ___toRelease, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_231 * L_0 = ((ListPool_1_t7_196_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		List_1_t1_1838 * L_1 = ___toRelease;
		NullCheck((ObjectPool_1_t7_231 *)L_0);
		(( void (*) (ObjectPool_1_t7_231 *, List_1_t1_1838 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t7_231 *)L_0, (List_1_t1_1838 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m7_1736_gshared (Object_t * __this /* static, unused */, List_1_t1_1838 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1838 * L_0 = ___l;
		NullCheck((List_1_t1_1838 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Int32>::Clear() */, (List_1_t1_1838 *)L_0);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C" void ListPool_1__cctor_m7_1744_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		UnityAction_1_t6_364 * L_0 = ((ListPool_1_t7_197_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1) };
		UnityAction_1_t6_364 * L_2 = (UnityAction_1_t6_364 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (UnityAction_1_t6_364 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, (Object_t *)NULL, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((ListPool_1_t7_197_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t6_364 * L_3 = ((ListPool_1_t7_197_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___U3CU3Ef__amU24cache1_1;
		ObjectPool_1_t7_232 * L_4 = (ObjectPool_1_t7_232 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (ObjectPool_1_t7_232 *, UnityAction_1_t6_364 *, UnityAction_1_t6_364 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_4, (UnityAction_1_t6_364 *)G_B2_0, (UnityAction_1_t6_364 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((ListPool_1_t7_197_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0 = L_4;
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C" List_1_t1_1824 * ListPool_1_Get_m7_1517_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_232 * L_0 = ((ListPool_1_t7_197_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		NullCheck((ObjectPool_1_t7_232 *)L_0);
		List_1_t1_1824 * L_1 = (( List_1_t1_1824 * (*) (ObjectPool_1_t7_232 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((ObjectPool_1_t7_232 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m7_1518_gshared (Object_t * __this /* static, unused */, List_1_t1_1824 * ___toRelease, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t7_232 * L_0 = ((ListPool_1_t7_197_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___s_ListPool_0;
		List_1_t1_1824 * L_1 = ___toRelease;
		NullCheck((ObjectPool_1_t7_232 *)L_0);
		(( void (*) (ObjectPool_1_t7_232 *, List_1_t1_1824 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((ObjectPool_1_t7_232 *)L_0, (List_1_t1_1824 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m7_1745_gshared (Object_t * __this /* static, unused */, List_1_t1_1824 * ___l, const MethodInfo* method)
{
	{
		List_1_t1_1824 * L_0 = ___l;
		NullCheck((List_1_t1_1824 *)L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, (List_1_t1_1824 *)L_0);
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::.ctor()
extern TypeInfo* List_1_t1_2632_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1_25784_MethodInfo_var;
extern "C" void ObserverPattern_1__ctor_m8_2028_gshared (ObserverPattern_1_t8_371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t1_2632_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4342);
		List_1__ctor_m1_25784_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484267);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_2632 * L_0 = (List_1_t1_2632 *)il2cpp_codegen_object_new (List_1_t1_2632_il2cpp_TypeInfo_var);
		List_1__ctor_m1_25784(L_0, /*hidden argument*/List_1__ctor_m1_25784_MethodInfo_var);
		__this->___m_observers_9 = L_0;
		NullCheck((SingletonPattern_1_t8_372 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((SingletonPattern_1_t8_372 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::AddObserver(VoxelBusters.DesignPatterns.IObserver)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5151;
extern "C" void ObserverPattern_1_AddObserver_m8_2029_gshared (ObserverPattern_1_t8_371 * __this, Object_t * ____observer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5151 = il2cpp_codegen_string_literal_from_index(5151);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_2632 * L_0 = (List_1_t1_2632 *)(__this->___m_observers_9);
		Object_t * L_1 = ____observer;
		NullCheck((List_1_t1_2632 *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>::Contains(!0) */, (List_1_t1_2632 *)L_0, (Object_t *)L_1);
		if (L_2)
		{
			goto IL_0032;
		}
	}
	{
		Object_t * L_3 = ____observer;
		NullCheck((Object_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1_548(NULL /*static, unused*/, (String_t*)_stringLiteral5151, (Object_t *)L_4, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, (Object_t *)L_5, /*hidden argument*/NULL);
		List_1_t1_2632 * L_6 = (List_1_t1_2632 *)(__this->___m_observers_9);
		Object_t * L_7 = ____observer;
		NullCheck((List_1_t1_2632 *)L_6);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>::Add(!0) */, (List_1_t1_2632 *)L_6, (Object_t *)L_7);
	}

IL_0032:
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::RemoveObserver(VoxelBusters.DesignPatterns.IObserver)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5152;
extern "C" void ObserverPattern_1_RemoveObserver_m8_2030_gshared (ObserverPattern_1_t8_371 * __this, Object_t * ____observer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5152 = il2cpp_codegen_string_literal_from_index(5152);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1_2632 * L_0 = (List_1_t1_2632 *)(__this->___m_observers_9);
		Object_t * L_1 = ____observer;
		NullCheck((List_1_t1_2632 *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>::Contains(!0) */, (List_1_t1_2632 *)L_0, (Object_t *)L_1);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		Object_t * L_3 = ____observer;
		NullCheck((Object_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1_548(NULL /*static, unused*/, (String_t*)_stringLiteral5152, (Object_t *)L_4, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, (Object_t *)L_5, /*hidden argument*/NULL);
		List_1_t1_2632 * L_6 = (List_1_t1_2632 *)(__this->___m_observers_9);
		Object_t * L_7 = ____observer;
		NullCheck((List_1_t1_2632 *)L_6);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>::Remove(!0) */, (List_1_t1_2632 *)L_6, (Object_t *)L_7);
	}

IL_0033:
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.ObserverPattern`1<System.Object>::NotifyObservers(System.String,System.Collections.ArrayList)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IObserver_t8_374_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5153;
extern "C" void ObserverPattern_1_NotifyObservers_m8_2031_gshared (ObserverPattern_1_t8_371 * __this, String_t* ____key, ArrayList_t1_170 * ____data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		IObserver_t8_374_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4343);
		_stringLiteral5153 = il2cpp_codegen_string_literal_from_index(5153);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		NullCheck((Object_t6_5 *)__this);
		String_t* L_0 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Object::ToString() */, (Object_t6_5 *)__this);
		String_t* L_1 = ____key;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1_549(NULL /*static, unused*/, (String_t*)_stringLiteral5153, (Object_t *)L_0, (Object_t *)L_1, /*hidden argument*/NULL);
		Debug_Log_m6_631(NULL /*static, unused*/, (Object_t *)L_2, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_001d:
	{
		List_1_t1_2632 * L_3 = (List_1_t1_2632 *)(__this->___m_observers_9);
		int32_t L_4 = V_0;
		NullCheck((List_1_t1_2632 *)L_3);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>::get_Item(System.Int32) */, (List_1_t1_2632 *)L_3, (int32_t)L_4);
		String_t* L_6 = ____key;
		ArrayList_t1_170 * L_7 = ____data;
		NullCheck((Object_t *)L_5);
		InterfaceActionInvoker2< String_t*, ArrayList_t1_170 * >::Invoke(0 /* System.Void VoxelBusters.DesignPatterns.IObserver::OnPropertyChange(System.String,System.Collections.ArrayList) */, IObserver_t8_374_il2cpp_TypeInfo_var, (Object_t *)L_5, (String_t*)L_6, (ArrayList_t1_170 *)L_7);
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_9 = V_0;
		List_1_t1_2632 * L_10 = (List_1_t1_2632 *)(__this->___m_observers_9);
		NullCheck((List_1_t1_2632 *)L_10);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<VoxelBusters.DesignPatterns.IObserver>::get_Count() */, (List_1_t1_2632 *)L_10);
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_001d;
		}
	}
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::.ctor()
extern "C" void SingletonPattern_1__ctor_m8_2032_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		NullCheck((MonoBehaviour_t6_91 *)__this);
		MonoBehaviour__ctor_m6_671((MonoBehaviour_t6_91 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::.cctor()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" void SingletonPattern_1__cctor_m8_2033_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	{
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = ((Object_t *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (Object_t_il2cpp_TypeInfo_var);
		Object__ctor_m1_0(L_0, /*hidden argument*/NULL);
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instanceLock_3 = L_0;
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___destroyedOnApplicationQuit_4 = 0;
		return;
	}
}
// T VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::get_Instance()
extern const Il2CppType* GameObject_t6_97_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t6_97_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t6_97_m6_1880_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral5154;
extern Il2CppCodeGenString* _stringLiteral5155;
extern "C" Object_t * SingletonPattern_1_get_Instance_m8_2034_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t6_97_0_0_0_var = il2cpp_codegen_type_from_index(1770);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameObject_t6_97_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1770);
		Object_Instantiate_TisGameObject_t6_97_m6_1880_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483964);
		_stringLiteral5154 = il2cpp_codegen_string_literal_from_index(5154);
		_stringLiteral5155 = il2cpp_codegen_string_literal_from_index(5155);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Object_t * V_1 = {0};
	ObjectU5BU5D_t1_272* V_2 = {0};
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	GameObject_t6_97 * V_5 = {0};
	SingletonPattern_1_t8_372 * V_6 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		bool L_1 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___destroyedOnApplicationQuit_4;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		return ((Object_t *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t * L_2 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instanceLock_3;
		V_1 = (Object_t *)L_2;
		Object_t * L_3 = V_1;
		Monitor_Enter_m1_12697(NULL /*static, unused*/, (Object_t *)L_3, /*hidden argument*/NULL);
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			Object_t * L_4 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
			bool L_5 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_4, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0116;
			}
		}

IL_003d:
		{
			Type_t * L_6 = V_0;
			ObjectU5BU5D_t6_272* L_7 = Object_FindObjectsOfType_m6_707(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
			V_2 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)IsInst(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
			ObjectU5BU5D_t1_272* L_8 = V_2;
			NullCheck(L_8);
			if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_8)->max_length))))) <= ((int32_t)0)))
			{
				goto IL_0089;
			}
		}

IL_0052:
		{
			ObjectU5BU5D_t1_272* L_9 = V_2;
			NullCheck(L_9);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
			int32_t L_10 = 0;
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_9, L_10, sizeof(Object_t *)));
			V_3 = (int32_t)1;
			goto IL_0080;
		}

IL_0065:
		{
			ObjectU5BU5D_t1_272* L_11 = V_2;
			int32_t L_12 = V_3;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
			NullCheck((Component_t6_26 *)(*((Object_t **)(Object_t **)SZArrayLdElema(L_11, L_12, sizeof(Object_t *)))));
			GameObject_t6_97 * L_13 = Component_get_gameObject_m6_725((Component_t6_26 *)(*((Object_t **)(Object_t **)SZArrayLdElema(L_11, L_12, sizeof(Object_t *)))), /*hidden argument*/NULL);
			Object_Destroy_m6_704(NULL /*static, unused*/, (Object_t6_5 *)L_13, /*hidden argument*/NULL);
			int32_t L_14 = V_3;
			V_3 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		}

IL_0080:
		{
			int32_t L_15 = V_3;
			ObjectU5BU5D_t1_272* L_16 = V_2;
			NullCheck(L_16);
			if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
			{
				goto IL_0065;
			}
		}

IL_0089:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			Object_t * L_17 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
			bool L_18 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_17, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0116;
			}
		}

IL_009e:
		{
			Type_t * L_19 = V_0;
			NullCheck((MemberInfo_t *)L_19);
			String_t* L_20 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_19);
			V_4 = (String_t*)L_20;
			String_t* L_21 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_22 = String_Concat_m1_559(NULL /*static, unused*/, (String_t*)_stringLiteral5154, (String_t*)L_21, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_23 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GameObject_t6_97_0_0_0_var), /*hidden argument*/NULL);
			Object_t6_5 * L_24 = Resources_Load_m6_488(NULL /*static, unused*/, (String_t*)L_22, (Type_t *)L_23, /*hidden argument*/NULL);
			V_5 = (GameObject_t6_97 *)((GameObject_t6_97 *)IsInst(L_24, GameObject_t6_97_il2cpp_TypeInfo_var));
			GameObject_t6_97 * L_25 = V_5;
			bool L_26 = Object_op_Inequality_m6_722(NULL /*static, unused*/, (Object_t6_5 *)L_25, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
			if (!L_26)
			{
				goto IL_00f5;
			}
		}

IL_00d5:
		{
			Debug_Log_m6_631(NULL /*static, unused*/, (Object_t *)_stringLiteral5155, /*hidden argument*/NULL);
			GameObject_t6_97 * L_27 = V_5;
			GameObject_t6_97 * L_28 = Object_Instantiate_TisGameObject_t6_97_m6_1880(NULL /*static, unused*/, (GameObject_t6_97 *)L_27, /*hidden argument*/Object_Instantiate_TisGameObject_t6_97_m6_1880_MethodInfo_var);
			NullCheck((GameObject_t6_97 *)L_28);
			Object_t * L_29 = (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((GameObject_t6_97 *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = L_29;
			goto IL_0104;
		}

IL_00f5:
		{
			GameObject_t6_97 * L_30 = (GameObject_t6_97 *)il2cpp_codegen_object_new (GameObject_t6_97_il2cpp_TypeInfo_var);
			GameObject__ctor_m6_733(L_30, /*hidden argument*/NULL);
			NullCheck((GameObject_t6_97 *)L_30);
			Object_t * L_31 = (( Object_t * (*) (GameObject_t6_97 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((GameObject_t6_97 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = L_31;
		}

IL_0104:
		{
			IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
			String_t* L_32 = V_4;
			NullCheck((Object_t6_5 *)(*(&((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2)));
			Object_set_name_m6_709((Object_t6_5 *)(*(&((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2)), (String_t*)L_32, /*hidden argument*/NULL);
		}

IL_0116:
		{
			IL2CPP_LEAVE(0x122, FINALLY_011b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_011b;
	}

FINALLY_011b:
	{ // begin finally (depth: 1)
		Object_t * L_33 = V_1;
		Monitor_Exit_m1_12698(NULL /*static, unused*/, (Object_t *)L_33, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(283)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(283)
	{
		IL2CPP_JUMP_TBL(0x122, IL_0122)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0122:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t * L_34 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
		V_6 = (SingletonPattern_1_t8_372 *)((SingletonPattern_1_t8_372 *)Castclass(L_34, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)));
		SingletonPattern_1_t8_372 * L_35 = V_6;
		NullCheck(L_35);
		bool L_36 = (bool)(L_35->___m_isInitialized_7);
		if (L_36)
		{
			goto IL_0146;
		}
	}
	{
		SingletonPattern_1_t8_372 * L_37 = V_6;
		NullCheck((SingletonPattern_1_t8_372 *)L_37);
		VirtActionInvoker0::Invoke(10 /* System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Init() */, (SingletonPattern_1_t8_372 *)L_37);
	}

IL_0146:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t * L_38 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
		return L_38;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::set_Instance(T)
extern "C" void SingletonPattern_1_set_Instance_m8_2035_gshared (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = L_0;
		return;
	}
}
// UnityEngine.Transform VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::get_CachedTransform()
extern "C" Transform_t6_65 * SingletonPattern_1_get_CachedTransform_m8_2036_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		Transform_t6_65 * L_0 = (Transform_t6_65 *)(__this->___m_transform_5);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		NullCheck((Component_t6_26 *)__this);
		Transform_t6_65 * L_2 = Component_get_transform_m6_724((Component_t6_26 *)__this, /*hidden argument*/NULL);
		__this->___m_transform_5 = L_2;
	}

IL_001d:
	{
		Transform_t6_65 * L_3 = (Transform_t6_65 *)(__this->___m_transform_5);
		return L_3;
	}
}
// UnityEngine.GameObject VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::get_CachedGameObject()
extern "C" GameObject_t6_97 * SingletonPattern_1_get_CachedGameObject_m8_2037_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		GameObject_t6_97 * L_0 = (GameObject_t6_97 *)(__this->___m_gameObject_6);
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		NullCheck((Component_t6_26 *)__this);
		GameObject_t6_97 * L_2 = Component_get_gameObject_m6_725((Component_t6_26 *)__this, /*hidden argument*/NULL);
		__this->___m_gameObject_6 = L_2;
	}

IL_001d:
	{
		GameObject_t6_97 * L_3 = (GameObject_t6_97 *)(__this->___m_gameObject_6);
		return L_3;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::ResetStaticProperties()
extern "C" void SingletonPattern_1_ResetStaticProperties_m8_2038_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = ((Object_t *)Castclass(NULL, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___destroyedOnApplicationQuit_4 = 0;
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Awake()
extern "C" void SingletonPattern_1_Awake_m8_2039_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___m_isInitialized_7);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullCheck((SingletonPattern_1_t8_372 *)__this);
		VirtActionInvoker0::Invoke(10 /* System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Init() */, (SingletonPattern_1_t8_372 *)__this);
	}

IL_0011:
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Start()
extern "C" void SingletonPattern_1_Start_m8_2040_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Reset()
extern "C" void SingletonPattern_1_Reset_m8_2041_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		__this->___m_isInitialized_7 = 0;
		__this->___m_isForcefullyDestroyed_8 = 0;
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::OnEnable()
extern "C" void SingletonPattern_1_OnEnable_m8_2042_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::OnDisable()
extern "C" void SingletonPattern_1_OnDisable_m8_2043_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::OnDestroy()
extern "C" void SingletonPattern_1_OnDestroy_m8_2044_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t * L_0 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_0, (Object_t6_5 *)__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (bool)(__this->___m_isForcefullyDestroyed_8);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___destroyedOnApplicationQuit_4 = 1;
	}

IL_0026:
	{
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::Init()
extern "C" void SingletonPattern_1_Init_m8_2045_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		__this->___m_isInitialized_7 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t * L_0 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2 = ((Object_t *)Castclass(((Object_t *)IsInst(__this, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)));
		goto IL_0057;
	}

IL_0036:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t * L_2 = ((SingletonPattern_1_t8_372_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->static_fields)->___instance_2;
		bool L_3 = Object_op_Inequality_m6_722(NULL /*static, unused*/, (Object_t6_5 *)L_2, (Object_t6_5 *)__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0057;
		}
	}
	{
		NullCheck((SingletonPattern_1_t8_372 *)__this);
		GameObject_t6_97 * L_4 = (( GameObject_t6_97 * (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SingletonPattern_1_t8_372 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Object_Destroy_m6_704(NULL /*static, unused*/, (Object_t6_5 *)L_4, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		NullCheck((SingletonPattern_1_t8_372 *)__this);
		GameObject_t6_97 * L_5 = (( GameObject_t6_97 * (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SingletonPattern_1_t8_372 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Object_DontDestroyOnLoad_m6_710(NULL /*static, unused*/, (Object_t6_5 *)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>::ForceDestroy()
extern "C" void SingletonPattern_1_ForceDestroy_m8_2046_gshared (SingletonPattern_1_t8_372 * __this, const MethodInfo* method)
{
	{
		__this->___m_isForcefullyDestroyed_8 = 1;
		NullCheck((SingletonPattern_1_t8_372 *)__this);
		GameObject_t6_97 * L_0 = (( GameObject_t6_97 * (*) (SingletonPattern_1_t8_372 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SingletonPattern_1_t8_372 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Object_Destroy_m6_704(NULL /*static, unused*/, (Object_t6_5 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::.ctor()
extern "C" void AdvancedScriptableObject_1__ctor_m8_2047_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method)
{
	{
		NullCheck((ScriptableObject_t6_15 *)__this);
		ScriptableObject__ctor_m6_16((ScriptableObject_t6_15 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::.cctor()
extern TypeInfo* CharU5BU5D_t1_16_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t1_247_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5170;
extern Il2CppCodeGenString* _stringLiteral5171;
extern "C" void AdvancedScriptableObject_1__cctor_m8_2049_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t1_16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		StringBuilder_t1_247_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		_stringLiteral5170 = il2cpp_codegen_string_literal_from_index(5170);
		_stringLiteral5171 = il2cpp_codegen_string_literal_from_index(5171);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	StringU5BU5D_t1_238* V_1 = {0};
	int32_t V_2 = 0;
	StringBuilder_t1_247 * V_3 = {0};
	int32_t V_4 = 0;
	{
		CharU5BU5D_t1_16* L_0 = (CharU5BU5D_t1_16*)((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck((String_t*)_stringLiteral5170);
		String_t* L_1 = String_TrimStart_m1_459((String_t*)_stringLiteral5170, (CharU5BU5D_t1_16*)L_0, /*hidden argument*/NULL);
		V_0 = (String_t*)L_1;
		String_t* L_2 = V_0;
		CharU5BU5D_t1_16* L_3 = (CharU5BU5D_t1_16*)((CharU5BU5D_t1_16*)SZArrayNew(CharU5BU5D_t1_16_il2cpp_TypeInfo_var, 1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_3, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)47);
		NullCheck((String_t*)L_2);
		StringU5BU5D_t1_238* L_4 = String_Split_m1_448((String_t*)L_2, (CharU5BU5D_t1_16*)L_3, /*hidden argument*/NULL);
		V_1 = (StringU5BU5D_t1_238*)L_4;
		StringU5BU5D_t1_238* L_5 = V_1;
		NullCheck(L_5);
		V_2 = (int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length))));
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) >= ((int32_t)2)))
		{
			goto IL_003e;
		}
	}
	{
		Debug_LogError_m6_632(NULL /*static, unused*/, (Object_t *)_stringLiteral5171, /*hidden argument*/NULL);
		return;
	}

IL_003e:
	{
		int32_t L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_0050;
		}
	}
	{
		((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___assetGroupFolderName_7 = (String_t*)NULL;
		goto IL_0093;
	}

IL_0050:
	{
		StringBuilder_t1_247 * L_8 = (StringBuilder_t1_247 *)il2cpp_codegen_object_new (StringBuilder_t1_247_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1_12414(L_8, /*hidden argument*/NULL);
		V_3 = (StringBuilder_t1_247 *)L_8;
		V_4 = (int32_t)2;
		goto IL_0080;
	}

IL_005e:
	{
		int32_t L_9 = V_4;
		if ((((int32_t)L_9) == ((int32_t)2)))
		{
			goto IL_006f;
		}
	}
	{
		StringBuilder_t1_247 * L_10 = V_3;
		NullCheck((StringBuilder_t1_247 *)L_10);
		StringBuilder_Append_m1_12452((StringBuilder_t1_247 *)L_10, (uint16_t)((int32_t)47), /*hidden argument*/NULL);
	}

IL_006f:
	{
		StringBuilder_t1_247 * L_11 = V_3;
		StringU5BU5D_t1_238* L_12 = V_1;
		int32_t L_13 = V_4;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck((StringBuilder_t1_247 *)L_11);
		StringBuilder_Append_m1_12438((StringBuilder_t1_247 *)L_11, (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_12, L_14, sizeof(String_t*))), /*hidden argument*/NULL);
		int32_t L_15 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_16 = V_4;
		int32_t L_17 = V_2;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_005e;
		}
	}
	{
		StringBuilder_t1_247 * L_18 = V_3;
		NullCheck((StringBuilder_t1_247 *)L_18);
		String_t* L_19 = StringBuilder_ToString_m1_12428((StringBuilder_t1_247 *)L_18, /*hidden argument*/NULL);
		((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___assetGroupFolderName_7 = L_19;
	}

IL_0093:
	{
		return;
	}
}
// T VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::get_Instance()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Object_t * AdvancedScriptableObject_1_get_Instance_m8_2051_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_0 = ((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___instance_6;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(24 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_2);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_4 = (( Object_t * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (String_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___instance_6 = L_4;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_5 = ((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___instance_6;
		return L_5;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::Reset()
extern "C" void AdvancedScriptableObject_1_Reset_m8_2053_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::OnEnable()
extern "C" void AdvancedScriptableObject_1_OnEnable_m8_2054_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_0 = ((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___instance_6;
		bool L_1 = Object_op_Equality_m6_721(NULL /*static, unused*/, (Object_t6_5 *)L_0, (Object_t6_5 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___instance_6 = ((Object_t *)Castclass(((Object_t *)IsInst(__this, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
	}

IL_002a:
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::OnDisable()
extern "C" void AdvancedScriptableObject_1_OnDisable_m8_2056_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::OnDestroy()
extern "C" void AdvancedScriptableObject_1_OnDestroy_m8_2058_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::Save()
extern "C" void AdvancedScriptableObject_1_Save_m8_2060_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// T VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::GetAsset(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5172;
extern "C" Object_t * AdvancedScriptableObject_1_GetAsset_m8_2062_gshared (Object_t * __this /* static, unused */, String_t* ____assetName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral5172 = il2cpp_codegen_string_literal_from_index(5172);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		String_t* L_0 = ((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___assetGroupFolderName_7;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ____assetName;
		Object_t * L_2 = (( Object_t * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (String_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		String_t* L_3 = ((AdvancedScriptableObject_1_t8_375_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___assetGroupFolderName_7;
		String_t* L_4 = ____assetName;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1_549(NULL /*static, unused*/, (String_t*)_stringLiteral5172, (Object_t *)L_3, (Object_t *)L_4, /*hidden argument*/NULL);
		Object_t * L_6 = (( Object_t * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (String_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.ctor()
extern "C" void EqualityComparer_1__ctor_m1_26294_gshared (EqualityComparer_1_t1_2656 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<System.Byte>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t1_3064_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m1_26295_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t1_3064_0_0_0_var = il2cpp_codegen_type_from_index(4654);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GenericEqualityComparer_1_t1_3064_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1_31* L_4 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1_31*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1_2656_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t1_2656 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1_2658 * L_8 = (DefaultComparer_t1_2658 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t1_2658 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1_2656_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_26296_gshared (EqualityComparer_1_t1_2656 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t1_2656 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, uint8_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<System.Byte>::GetHashCode(T) */, (EqualityComparer_1_t1_2656 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_26297_gshared (EqualityComparer_1_t1_2656 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t1_2656 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, uint8_t, uint8_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Byte>::Equals(T,T) */, (EqualityComparer_1_t1_2656 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::get_Default()
extern "C" EqualityComparer_1_t1_2656 * EqualityComparer_1_get_Default_m1_26298_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1_2656 * L_0 = ((EqualityComparer_1_t1_2656_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m1_26299_gshared (GenericEqualityComparer_1_t1_2657 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1_2656 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t1_2656 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t1_2656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m1_26300_gshared (GenericEqualityComparer_1_t1_2657 * __this, uint8_t ___obj, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((uint8_t*)(&___obj));
		int32_t L_1 = Byte_GetHashCode_m1_232((uint8_t*)(&___obj), NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Byte>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m1_26301_gshared (GenericEqualityComparer_1_t1_2657 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___x;
		goto IL_0015;
	}
	{
		uint8_t L_1 = ___y;
		uint8_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint8_t L_4 = ___y;
		NullCheck((uint8_t*)(&___x));
		bool L_5 = Byte_Equals_m1_234((uint8_t*)(&___x), (uint8_t)L_4, NULL);
		return L_5;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::.ctor()
extern "C" void DefaultComparer__ctor_m1_26302_gshared (DefaultComparer_t1_2658 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1_2656 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t1_2656 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t1_2656 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_26303_gshared (DefaultComparer_t1_2658 * __this, uint8_t ___obj, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((uint8_t*)(&___obj));
		int32_t L_1 = Byte_GetHashCode_m1_232((uint8_t*)(&___obj), NULL);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Byte>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_26304_gshared (DefaultComparer_t1_2658 * __this, uint8_t ___x, uint8_t ___y, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___x;
		goto IL_0015;
	}
	{
		uint8_t L_1 = ___y;
		uint8_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		uint8_t L_4 = ___y;
		uint8_t L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		NullCheck((uint8_t*)(&___x));
		bool L_7 = Byte_Equals_m1_231((uint8_t*)(&___x), (Object_t *)L_6, NULL);
		return L_7;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26305_gshared (InternalEnumerator_1_t1_2659 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26306_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26307_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method)
{
	{
		UFraction32_t8_121  L_0 = (( UFraction32_t8_121  (*) (InternalEnumerator_1_t1_2659 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2659 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UFraction32_t8_121  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26308_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26309_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ExifLibrary.MathEx/UFraction32>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" UFraction32_t8_121  InternalEnumerator_1_get_Current_m1_26310_gshared (InternalEnumerator_1_t1_2659 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		UFraction32_t8_121  L_8 = (( UFraction32_t8_121  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26311_gshared (InternalEnumerator_1_t1_2660 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26312_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26313_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method)
{
	{
		Fraction32_t8_127  L_0 = (( Fraction32_t8_127  (*) (InternalEnumerator_1_t1_2660 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Fraction32_t8_127  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26314_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26315_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ExifLibrary.MathEx/Fraction32>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" Fraction32_t8_127  InternalEnumerator_1_get_Current_m1_26316_gshared (InternalEnumerator_1_t1_2660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		Fraction32_t8_127  L_8 = (( Fraction32_t8_127  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2063_gshared (ExifEnumProperty_1_t8_378 * __this, int32_t ___tag, Object_t * ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2064_gshared (ExifEnumProperty_1_t8_378 * __this, int32_t ___tag, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		Object_t * L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_378 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_378 *, int32_t, Object_t *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_378 *)__this, (int32_t)L_0, (Object_t *)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<System.Object>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2065_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_378 *)__this);
		Object_t * L_0 = (( Object_t * (*) (ExifEnumProperty_1_t8_378 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_378 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2066_gshared (ExifEnumProperty_1_t8_378 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_378 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_378 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_378 *)__this, (Object_t *)((Object_t *)Castclass(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<System.Object>::get_Value()
extern "C" Object_t * ExifEnumProperty_1_get_Value_m8_2067_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<System.Object>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2068_gshared (ExifEnumProperty_1_t8_378 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<System.Object>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2069_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<System.Object>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2070_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method)
{
	{
		Object_t ** L_0 = (Object_t **)&(__this->___mValue_3);
		NullCheck((Object_t *)(*L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)(*L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<System.Object>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2071_gshared (ExifEnumProperty_1_t8_378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		Object_t * L_11 = (Object_t *)(__this->___mValue_3);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_11, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_12 = {0};
		ExifInterOperability__ctor_m8_496(&L_12, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_12;
	}

IL_0060:
	{
		Type_t * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_13) == ((Object_t*)(Type_t *)L_14)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_27 = V_0;
		ByteU5BU5D_t1_109* L_28 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		Object_t * L_29 = (Object_t *)(__this->___mValue_3);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_28, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_29, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_30 = {0};
		ExifInterOperability__ctor_m8_496(&L_30, (uint16_t)L_27, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_28, /*hidden argument*/NULL);
		return L_30;
	}

IL_00f2:
	{
		Type_t * L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_31) == ((Object_t*)(Type_t *)L_32))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_33 = V_0;
		ByteU5BU5D_t1_109* L_34 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		Object_t * L_35 = (Object_t *)(__this->___mValue_3);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_34, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_35, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_36 = {0};
		ExifInterOperability__ctor_m8_496(&L_36, (uint16_t)L_33, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_34, /*hidden argument*/NULL);
		return L_36;
	}

IL_0124:
	{
		Type_t * L_37 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_38 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_37) == ((Object_t*)(Type_t *)L_38))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_39 = V_0;
		Object_t * L_40 = (Object_t *)(__this->___mValue_3);
		ByteU5BU5D_t1_109* L_41 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_40, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_39, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_41, /*hidden argument*/NULL);
		return L_42;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_43 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_43, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_43);
	}
}
// T ExifLibrary.ExifEnumProperty`1<System.Object>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" Object_t * ExifEnumProperty_1_op_Implicit_m8_2072_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_378 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_378 * L_0 = ___obj;
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m1_26317_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)((int32_t)10), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26319_gshared (Dictionary_2_t1_2663 * __this, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___comparer;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)((int32_t)10), (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_26321_gshared (Dictionary_2_t1_2663 * __this, Object_t* ___dictionary, const MethodInfo* method)
{
	{
		Object_t* L_0 = ___dictionary;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, Object_t*, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Dictionary_2_t1_2663 *)__this, (Object_t*)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_26323_gshared (Dictionary_2_t1_2663 * __this, int32_t ___capacity, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral792;
extern "C" void Dictionary_2__ctor_m1_26325_gshared (Dictionary_2_t1_2663 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1_2665  V_1 = {0};
	Object_t* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral792, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Object_t* L_2 = ___dictionary;
		NullCheck((Object_t*)L_2);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		Object_t* L_5 = ___comparer;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)L_4, (Object_t*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t* L_6 = ___dictionary;
		NullCheck((Object_t*)L_6);
		Object_t* L_7 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_6);
		V_2 = (Object_t*)L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0032:
		{
			Object_t* L_8 = V_2;
			NullCheck((Object_t*)L_8);
			KeyValuePair_2_t1_2665  L_9 = (KeyValuePair_2_t1_2665 )InterfaceFuncInvoker0< KeyValuePair_2_t1_2665  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_8);
			V_1 = (KeyValuePair_2_t1_2665 )L_9;
			int32_t L_10 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
			Object_t * L_11 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2665 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
			NullCheck((Dictionary_2_t1_2663 *)__this);
			VirtActionInvoker2< int32_t, Object_t * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_10, (Object_t *)L_11);
		}

IL_004d:
		{
			Object_t* L_12 = V_2;
			NullCheck((Object_t *)L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_12);
			if (L_13)
			{
				goto IL_0032;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Object_t* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Object_t* L_15 = V_2;
			NullCheck((Object_t *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, (Object_t *)L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_26327_gshared (Dictionary_2_t1_2663 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		Object_t* L_1 = ___comparer;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)L_0, (Object_t*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_26329_gshared (Dictionary_2_t1_2663 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_0 = ___info;
		__this->___serialization_info_13 = L_0;
		return;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_26331_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2663 *)__this);
		KeyCollection_t1_2668 * L_0 = (( KeyCollection_t1_2668 * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_26333_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2663 *)__this);
		ValueCollection_t1_2672 * L_0 = (( ValueCollection_t1_2672 * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_26335_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2663 *)__this);
		KeyCollection_t1_2668 * L_0 = (( KeyCollection_t1_2668 * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_26337_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		NullCheck((Dictionary_2_t1_2663 *)__this);
		ValueCollection_t1_2672 * L_0 = (( ValueCollection_t1_2672 * (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_26339_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_26341_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_26343_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		if (!((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_1 = ___key;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t1_2663 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))));
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		int32_t L_4 = (( int32_t (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t1_2663 *)__this, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(22 /* TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Item(TKey) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_4);
		return L_5;
	}

IL_002f:
	{
		return NULL;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_26345_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t1_2663 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		Object_t * L_3 = (( Object_t * (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Dictionary_2_t1_2663 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(23 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::set_Item(TKey,TValue) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_1, (Object_t *)L_3);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_26347_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		int32_t L_1 = (( int32_t (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((Dictionary_2_t1_2663 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		Object_t * L_2 = ___value;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		Object_t * L_3 = (( Object_t * (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((Dictionary_2_t1_2663 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_1, (Object_t *)L_3);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_26349_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		bool L_4 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t1_2663 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))));
		return L_4;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_26351_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (!((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = ___key;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Remove(TKey) */, (Dictionary_2_t1_2663 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))));
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_26353_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_26355_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_26357_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_26359_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___keyValuePair, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2665 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_0, (Object_t *)L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_26361_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2665  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t1_2663 *, KeyValuePair_2_t1_2665 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Dictionary_2_t1_2663 *)__this, (KeyValuePair_2_t1_2665 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_26363_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2U5BU5D_t1_2874* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t1_2874* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Dictionary_2_t1_2663 *)__this, (KeyValuePair_2U5BU5D_t1_2874*)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_26365_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___keyValuePair, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2665  L_0 = ___keyValuePair;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		bool L_1 = (( bool (*) (Dictionary_2_t1_2663 *, KeyValuePair_2_t1_2665 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((Dictionary_2_t1_2663 *)__this, (KeyValuePair_2_t1_2665 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(20 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Remove(TKey) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_26367_gshared (Dictionary_2_t1_2663 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(625);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2874* V_0 = {0};
	DictionaryEntryU5BU5D_t1_869* V_1 = {0};
	int32_t G_B5_0 = 0;
	DictionaryEntryU5BU5D_t1_869* G_B5_1 = {0};
	Dictionary_2_t1_2663 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	DictionaryEntryU5BU5D_t1_869* G_B4_1 = {0};
	Dictionary_2_t1_2663 * G_B4_2 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (KeyValuePair_2U5BU5D_t1_2874*)((KeyValuePair_2U5BU5D_t1_2874*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)));
		KeyValuePair_2U5BU5D_t1_2874* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2874* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Dictionary_2_t1_2663 *)__this, (KeyValuePair_2U5BU5D_t1_2874*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}

IL_0016:
	{
		Array_t * L_4 = ___array;
		int32_t L_5 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t1_2663 *)__this, (Array_t *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		Array_t * L_6 = ___array;
		V_1 = (DictionaryEntryU5BU5D_t1_869*)((DictionaryEntryU5BU5D_t1_869*)IsInst(L_6, DictionaryEntryU5BU5D_t1_869_il2cpp_TypeInfo_var));
		DictionaryEntryU5BU5D_t1_869* L_7 = V_1;
		if (!L_7)
		{
			goto IL_0051;
		}
	}
	{
		DictionaryEntryU5BU5D_t1_869* L_8 = V_1;
		int32_t L_9 = ___index;
		Transform_1_t1_2664 * L_10 = ((Dictionary_2_t1_2663_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		G_B4_0 = L_9;
		G_B4_1 = L_8;
		G_B4_2 = ((Dictionary_2_t1_2663 *)(__this));
		if (L_10)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_8;
			G_B5_2 = ((Dictionary_2_t1_2663 *)(__this));
			goto IL_0046;
		}
	}
	{
		IntPtr_t L_11 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23) };
		Transform_1_t1_2664 * L_12 = (Transform_1_t1_2664 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		(( void (*) (Transform_1_t1_2664 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(L_12, (Object_t *)NULL, (IntPtr_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		((Dictionary_2_t1_2663_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->static_fields)->___U3CU3Ef__amU24cacheB_15 = L_12;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((Dictionary_2_t1_2663 *)(G_B4_2));
	}

IL_0046:
	{
		Transform_1_t1_2664 * L_13 = ((Dictionary_2_t1_2663_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->static_fields)->___U3CU3Ef__amU24cacheB_15;
		NullCheck((Dictionary_2_t1_2663 *)G_B5_2);
		(( void (*) (Dictionary_2_t1_2663 *, DictionaryEntryU5BU5D_t1_869*, int32_t, Transform_1_t1_2664 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((Dictionary_2_t1_2663 *)G_B5_2, (DictionaryEntryU5BU5D_t1_869*)G_B5_1, (int32_t)G_B5_0, (Transform_1_t1_2664 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return;
	}

IL_0051:
	{
		Array_t * L_14 = ___array;
		int32_t L_15 = ___index;
		IntPtr_t L_16 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27) };
		Transform_1_t1_2675 * L_17 = (Transform_1_t1_2675 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (Transform_1_t1_2675 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_17, (Object_t *)NULL, (IntPtr_t)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, Transform_1_t1_2675 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30)->method)((Dictionary_2_t1_2663 *)__this, (Array_t *)L_14, (int32_t)L_15, (Transform_1_t1_2675 *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_26369_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670  L_0 = {0};
		(( void (*) (Enumerator_t1_2670 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(&L_0, (Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		Enumerator_t1_2670  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_26371_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670  L_0 = {0};
		(( void (*) (Enumerator_t1_2670 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(&L_0, (Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		Enumerator_t1_2670  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_26373_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		ShimEnumerator_t1_2676 * L_0 = (ShimEnumerator_t1_2676 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		(( void (*) (ShimEnumerator_t1_2676 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(L_0, (Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_26375_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___count_10);
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Item(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1_257_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" Object_t * Dictionary_2_get_Item_m1_26377_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		KeyNotFoundException_t1_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1969);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t1_275* L_5 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_0048:
	{
		LinkU5BU5D_t1_1975* L_9 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_14 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0089;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_19 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		return (*(Object_t **)(Object_t **)SZArrayLdElema(L_19, L_21, sizeof(Object_t *)));
	}

IL_0089:
	{
		LinkU5BU5D_t1_1975* L_22 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_23 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1_256 )))->___Next_1);
		V_1 = (int32_t)L_24;
	}

IL_009b:
	{
		int32_t L_25 = V_1;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		KeyNotFoundException_t1_257 * L_26 = (KeyNotFoundException_t1_257 *)il2cpp_codegen_object_new (KeyNotFoundException_t1_257_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m1_2781(L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_26);
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::set_Item(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void Dictionary_2_set_Item_m1_26379_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))));
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		V_3 = (int32_t)(-1);
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_00a2;
		}
	}

IL_004e:
	{
		LinkU5BU5D_t1_1975* L_11 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0087;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_16 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0087;
		}
	}
	{
		goto IL_00a2;
	}

IL_0087:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t1_1975* L_22 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}

IL_00a2:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		int32_t L_28 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
		V_4 = (int32_t)L_28;
		__this->___count_10 = L_28;
		int32_t L_29 = V_4;
		int32_t L_30 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00de;
		}
	}
	{
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		int32_t L_31 = V_0;
		Int32U5BU5D_t1_275* L_32 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_32);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_31&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_32)->max_length))))));
	}

IL_00de:
	{
		int32_t L_33 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_33;
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		int32_t L_35 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_36 = (int32_t)L_35;
		V_4 = (int32_t)L_36;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_36+(int32_t)1));
		int32_t L_37 = V_4;
		V_2 = (int32_t)L_37;
		goto IL_011c;
	}

IL_0105:
	{
		LinkU5BU5D_t1_1975* L_38 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_38, L_39, sizeof(Link_t1_256 )))->___Next_1);
		__this->___emptySlot_9 = L_40;
	}

IL_011c:
	{
		LinkU5BU5D_t1_1975* L_41 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_42 = V_2;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		Int32U5BU5D_t1_275* L_43 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_44 = V_1;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_41, L_42, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_43, L_45, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_46 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_47 = V_1;
		int32_t L_48 = V_2;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_46, L_47, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_48+(int32_t)1));
		LinkU5BU5D_t1_1975* L_49 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_50 = V_2;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = V_0;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_49, L_50, sizeof(Link_t1_256 )))->___HashCode_0 = L_51;
		ExifTagU5BU5D_t8_379* L_52 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_53 = V_2;
		int32_t L_54 = ___key;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_52, L_53, sizeof(int32_t))) = (int32_t)L_54;
		goto IL_01b5;
	}

IL_0166:
	{
		int32_t L_55 = V_3;
		if ((((int32_t)L_55) == ((int32_t)(-1))))
		{
			goto IL_01b5;
		}
	}
	{
		LinkU5BU5D_t1_1975* L_56 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_57 = V_3;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		LinkU5BU5D_t1_1975* L_58 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_59 = V_2;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		int32_t L_60 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_58, L_59, sizeof(Link_t1_256 )))->___Next_1);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_56, L_57, sizeof(Link_t1_256 )))->___Next_1 = L_60;
		LinkU5BU5D_t1_1975* L_61 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_62 = V_2;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		Int32U5BU5D_t1_275* L_63 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_64 = V_1;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		int32_t L_65 = L_64;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_61, L_62, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_63, L_65, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_66 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_67 = V_1;
		int32_t L_68 = V_2;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_66, L_67, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_01b5:
	{
		ObjectU5BU5D_t1_272* L_69 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		int32_t L_70 = V_2;
		Object_t * L_71 = ___value;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, L_70);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_69, L_70, sizeof(Object_t *))) = (Object_t *)L_71;
		int32_t L_72 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_72+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral794;
extern "C" void Dictionary_2_Init_m1_26381_gshared (Dictionary_2_t1_2663 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral794 = il2cpp_codegen_string_literal_from_index(794);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	Dictionary_2_t1_2663 * G_B4_0 = {0};
	Dictionary_2_t1_2663 * G_B3_0 = {0};
	Object_t* G_B5_0 = {0};
	Dictionary_2_t1_2663 * G_B5_1 = {0};
	{
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, (String_t*)_stringLiteral794, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Object_t* L_2 = ___hcp;
		G_B3_0 = ((Dictionary_2_t1_2663 *)(__this));
		if (!L_2)
		{
			G_B4_0 = ((Dictionary_2_t1_2663 *)(__this));
			goto IL_0021;
		}
	}
	{
		Object_t* L_3 = ___hcp;
		V_0 = (Object_t*)L_3;
		Object_t* L_4 = V_0;
		G_B5_0 = L_4;
		G_B5_1 = ((Dictionary_2_t1_2663 *)(G_B3_0));
		goto IL_0026;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		EqualityComparer_1_t1_2677 * L_5 = (( EqualityComparer_1_t1_2677 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		G_B5_0 = ((Object_t*)(L_5));
		G_B5_1 = ((Dictionary_2_t1_2663 *)(G_B4_0));
	}

IL_0026:
	{
		NullCheck(G_B5_1);
		G_B5_1->___hcp_12 = G_B5_0;
		int32_t L_6 = ___capacity;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		___capacity = (int32_t)((int32_t)10);
	}

IL_0035:
	{
		int32_t L_7 = ___capacity;
		___capacity = (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)((float)((float)(((float)((float)L_7)))/(float)(0.9f))))))+(int32_t)1));
		int32_t L_8 = ___capacity;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		__this->___generation_14 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::InitArrays(System.Int32)
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_InitArrays_m1_26383_gshared (Dictionary_2_t1_2663 * __this, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4655);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		__this->___table_4 = ((Int32U5BU5D_t1_275*)SZArrayNew(Int32U5BU5D_t1_275_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = ___size;
		__this->___linkSlots_5 = ((LinkU5BU5D_t1_1975*)SZArrayNew(LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var, L_1));
		__this->___emptySlot_9 = (-1);
		int32_t L_2 = ___size;
		__this->___keySlots_6 = ((ExifTagU5BU5D_t8_379*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40), L_2));
		int32_t L_3 = ___size;
		__this->___valueSlots_7 = ((ObjectU5BU5D_t1_272*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41), L_3));
		__this->___touchedSlots_8 = 0;
		Int32U5BU5D_t1_275* L_4 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_4);
		__this->___threshold_11 = (((int32_t)((int32_t)((float)((float)(((float)((float)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))))))*(float)(0.9f))))));
		int32_t L_5 = (int32_t)(__this->___threshold_11);
		if (L_5)
		{
			goto IL_0074;
		}
	}
	{
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0074;
		}
	}
	{
		__this->___threshold_11 = 1;
	}

IL_0074:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::CopyToCheck(System.Array,System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral173;
extern Il2CppCodeGenString* _stringLiteral170;
extern Il2CppCodeGenString* _stringLiteral795;
extern Il2CppCodeGenString* _stringLiteral796;
extern "C" void Dictionary_2_CopyToCheck_m1_26385_gshared (Dictionary_2_t1_2663 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		_stringLiteral795 = il2cpp_codegen_string_literal_from_index(795);
		_stringLiteral796 = il2cpp_codegen_string_literal_from_index(796);
		s_Il2CppMethodIntialized = true;
	}
	{
		Array_t * L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral173, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_3 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_3, (String_t*)_stringLiteral170, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index;
		Array_t * L_5 = ___array;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_003a;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_7, (String_t*)_stringLiteral795, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003a:
	{
		Array_t * L_8 = ___array;
		NullCheck((Array_t *)L_8);
		int32_t L_9 = Array_get_Length_m1_992((Array_t *)L_8, /*hidden argument*/NULL);
		int32_t L_10 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Count() */, (Dictionary_2_t1_2663 *)__this);
		if ((((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10))) >= ((int32_t)L_11)))
		{
			goto IL_0058;
		}
	}
	{
		ArgumentException_t1_1425 * L_12 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_12, (String_t*)_stringLiteral796, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_0058:
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_2665  Dictionary_2_make_pair_m1_26387_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		Object_t * L_1 = ___value;
		KeyValuePair_2_t1_2665  L_2 = {0};
		(( void (*) (KeyValuePair_2_t1_2665 *, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44)->method)(&L_2, (int32_t)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 44));
		return L_2;
	}
}
// TKey System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m1_26389_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		return L_0;
	}
}
// TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m1_26391_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_26393_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2U5BU5D_t1_2874* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t1_2874* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->method)((Dictionary_2_t1_2663 *)__this, (Array_t *)(Array_t *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		KeyValuePair_2U5BU5D_t1_2874* L_2 = ___array;
		int32_t L_3 = ___index;
		IntPtr_t L_4 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27) };
		Transform_1_t1_2675 * L_5 = (Transform_1_t1_2675 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		(( void (*) (Transform_1_t1_2675 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)(L_5, (Object_t *)NULL, (IntPtr_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, Transform_1_t1_2675 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45)->method)((Dictionary_2_t1_2663 *)__this, (KeyValuePair_2U5BU5D_t1_2874*)L_2, (int32_t)L_3, (Transform_1_t1_2675 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 45));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Resize()
extern TypeInfo* Hashtable_t1_100_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t1_275_il2cpp_TypeInfo_var;
extern TypeInfo* LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var;
extern "C" void Dictionary_2_Resize_m1_26395_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Hashtable_t1_100_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		Int32U5BU5D_t1_275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(51);
		LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4655);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t1_275* V_1 = {0};
	LinkU5BU5D_t1_1975* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ExifTagU5BU5D_t8_379* V_7 = {0};
	ObjectU5BU5D_t1_272* V_8 = {0};
	int32_t V_9 = 0;
	{
		Int32U5BU5D_t1_275* L_0 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Hashtable_t1_100_il2cpp_TypeInfo_var);
		int32_t L_1 = Hashtable_ToPrime_m1_3334(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_0)->max_length))))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t1_275*)((Int32U5BU5D_t1_275*)SZArrayNew(Int32U5BU5D_t1_275_il2cpp_TypeInfo_var, L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t1_1975*)((LinkU5BU5D_t1_1975*)SZArrayNew(LinkU5BU5D_t1_1975_il2cpp_TypeInfo_var, L_3));
		V_3 = (int32_t)0;
		goto IL_00b1;
	}

IL_0027:
	{
		Int32U5BU5D_t1_275* L_4 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a5;
	}

IL_0038:
	{
		LinkU5BU5D_t1_1975* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Object_t* L_9 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_10 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_11 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((Object_t*)L_9);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_9, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_12, sizeof(int32_t))));
		int32_t L_14 = (int32_t)((int32_t)((int32_t)L_13|(int32_t)((int32_t)-2147483648)));
		V_9 = (int32_t)L_14;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_7, L_8, sizeof(Link_t1_256 )))->___HashCode_0 = L_14;
		int32_t L_15 = V_9;
		V_5 = (int32_t)L_15;
		int32_t L_16 = V_5;
		int32_t L_17 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)2147483647)))%(int32_t)L_17));
		LinkU5BU5D_t1_1975* L_18 = V_2;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		Int32U5BU5D_t1_275* L_20 = V_1;
		int32_t L_21 = V_6;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_18, L_19, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_20, L_22, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_23 = V_1;
		int32_t L_24 = V_6;
		int32_t L_25 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_23, L_24, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		LinkU5BU5D_t1_1975* L_26 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_27 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_26, L_27, sizeof(Link_t1_256 )))->___Next_1);
		V_4 = (int32_t)L_28;
	}

IL_00a5:
	{
		int32_t L_29 = V_4;
		if ((!(((uint32_t)L_29) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_31 = V_3;
		Int32U5BU5D_t1_275* L_32 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_32)->max_length)))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t1_275* L_33 = V_1;
		__this->___table_4 = L_33;
		LinkU5BU5D_t1_1975* L_34 = V_2;
		__this->___linkSlots_5 = L_34;
		int32_t L_35 = V_0;
		V_7 = (ExifTagU5BU5D_t8_379*)((ExifTagU5BU5D_t8_379*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40), L_35));
		int32_t L_36 = V_0;
		V_8 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41), L_36));
		ExifTagU5BU5D_t8_379* L_37 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		ExifTagU5BU5D_t8_379* L_38 = V_7;
		int32_t L_39 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_37, (int32_t)0, (Array_t *)(Array_t *)L_38, (int32_t)0, (int32_t)L_39, /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_40 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		ObjectU5BU5D_t1_272* L_41 = V_8;
		int32_t L_42 = (int32_t)(__this->___touchedSlots_8);
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_40, (int32_t)0, (Array_t *)(Array_t *)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		ExifTagU5BU5D_t8_379* L_43 = V_7;
		__this->___keySlots_6 = L_43;
		ObjectU5BU5D_t1_272* L_44 = V_8;
		__this->___valueSlots_7 = L_44;
		int32_t L_45 = V_0;
		__this->___threshold_11 = (((int32_t)((int32_t)((float)((float)(((float)((float)L_45)))*(float)(0.9f))))));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Add(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern Il2CppCodeGenString* _stringLiteral798;
extern "C" void Dictionary_2_Add_m1_26397_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		_stringLiteral798 = il2cpp_codegen_string_literal_from_index(798);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))));
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		goto IL_009b;
	}

IL_004a:
	{
		LinkU5BU5D_t1_1975* L_10 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_13 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_14 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_15 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = ___key;
		NullCheck((Object_t*)L_14);
		bool L_19 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_14, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_15, L_17, sizeof(int32_t))), (int32_t)L_18);
		if (!L_19)
		{
			goto IL_0089;
		}
	}
	{
		ArgumentException_t1_1425 * L_20 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_20, (String_t*)_stringLiteral798, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_20);
	}

IL_0089:
	{
		LinkU5BU5D_t1_1975* L_21 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_21, L_22, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_23;
	}

IL_009b:
	{
		int32_t L_24 = V_2;
		if ((!(((uint32_t)L_24) == ((uint32_t)(-1)))))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_25 = (int32_t)(__this->___count_10);
		int32_t L_26 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		V_3 = (int32_t)L_26;
		__this->___count_10 = L_26;
		int32_t L_27 = V_3;
		int32_t L_28 = (int32_t)(__this->___threshold_11);
		if ((((int32_t)L_27) <= ((int32_t)L_28)))
		{
			goto IL_00d5;
		}
	}
	{
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		int32_t L_29 = V_0;
		Int32U5BU5D_t1_275* L_30 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_30);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_30)->max_length))))));
	}

IL_00d5:
	{
		int32_t L_31 = (int32_t)(__this->___emptySlot_9);
		V_2 = (int32_t)L_31;
		int32_t L_32 = V_2;
		if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_33 = (int32_t)(__this->___touchedSlots_8);
		int32_t L_34 = (int32_t)L_33;
		V_3 = (int32_t)L_34;
		__this->___touchedSlots_8 = ((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_3;
		V_2 = (int32_t)L_35;
		goto IL_0111;
	}

IL_00fa:
	{
		LinkU5BU5D_t1_1975* L_36 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1_256 )))->___Next_1);
		__this->___emptySlot_9 = L_38;
	}

IL_0111:
	{
		LinkU5BU5D_t1_1975* L_39 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = V_0;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1_256 )))->___HashCode_0 = L_41;
		LinkU5BU5D_t1_1975* L_42 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_43 = V_2;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		Int32U5BU5D_t1_275* L_44 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_45 = V_1;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = L_45;
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_42, L_43, sizeof(Link_t1_256 )))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_44, L_46, sizeof(int32_t)))-(int32_t)1));
		Int32U5BU5D_t1_275* L_47 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_48 = V_1;
		int32_t L_49 = V_2;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_47, L_48, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_49+(int32_t)1));
		ExifTagU5BU5D_t8_379* L_50 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_51 = V_2;
		int32_t L_52 = ___key;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_51);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_50, L_51, sizeof(int32_t))) = (int32_t)L_52;
		ObjectU5BU5D_t1_272* L_53 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		int32_t L_54 = V_2;
		Object_t * L_55 = ___value;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, L_54, sizeof(Object_t *))) = (Object_t *)L_55;
		int32_t L_56 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_56+(int32_t)1));
		return;
	}
}
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_26399_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___hcp_12);
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m1_26401_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		__this->___count_10 = 0;
		Int32U5BU5D_t1_275* L_0 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		Int32U5BU5D_t1_275* L_1 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_1);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_1)->max_length)))), /*hidden argument*/NULL);
		ExifTagU5BU5D_t8_379* L_2 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		ExifTagU5BU5D_t8_379* L_3 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		NullCheck(L_3);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_3)->max_length)))), /*hidden argument*/NULL);
		ObjectU5BU5D_t1_272* L_4 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		ObjectU5BU5D_t1_272* L_5 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		NullCheck(L_5);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_5)->max_length)))), /*hidden argument*/NULL);
		LinkU5BU5D_t1_1975* L_6 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		LinkU5BU5D_t1_1975* L_7 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		NullCheck(L_7);
		Array_Clear_m1_1037(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))), /*hidden argument*/NULL);
		__this->___emptySlot_9 = (-1);
		__this->___touchedSlots_8 = 0;
		int32_t L_8 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_8+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKey(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_ContainsKey_m1_26403_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t1_275* L_5 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_0090;
	}

IL_0048:
	{
		LinkU5BU5D_t1_1975* L_9 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_007e;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_14 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_007e;
		}
	}
	{
		return 1;
	}

IL_007e:
	{
		LinkU5BU5D_t1_1975* L_19 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_19, L_20, sizeof(Link_t1_256 )))->___Next_1);
		V_1 = (int32_t)L_21;
	}

IL_0090:
	{
		int32_t L_22 = V_1;
		if ((!(((uint32_t)L_22) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_26405_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___value, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		EqualityComparer_1_t1_1942 * L_0 = (( EqualityComparer_1_t1_1942 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		V_0 = (Object_t*)L_0;
		V_1 = (int32_t)0;
		goto IL_0054;
	}

IL_000d:
	{
		Int32U5BU5D_t1_275* L_1 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3, sizeof(int32_t)))-(int32_t)1));
		goto IL_0049;
	}

IL_001d:
	{
		Object_t* L_4 = V_0;
		ObjectU5BU5D_t1_272* L_5 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		Object_t * L_8 = ___value;
		NullCheck((Object_t*)L_4);
		bool L_9 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 48), (Object_t*)L_4, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_5, L_7, sizeof(Object_t *))), (Object_t *)L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		return 1;
	}

IL_0037:
	{
		LinkU5BU5D_t1_1975* L_10 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_10, L_11, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_12;
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_15 = V_1;
		Int32U5BU5D_t1_275* L_16 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_16)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral135;
extern Il2CppCodeGenString* _stringLiteral799;
extern Il2CppCodeGenString* _stringLiteral800;
extern Il2CppCodeGenString* _stringLiteral801;
extern Il2CppCodeGenString* _stringLiteral802;
extern "C" void Dictionary_2_GetObjectData_m1_26407_gshared (Dictionary_2_t1_2663 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral135 = il2cpp_codegen_string_literal_from_index(135);
		_stringLiteral799 = il2cpp_codegen_string_literal_from_index(799);
		_stringLiteral800 = il2cpp_codegen_string_literal_from_index(800);
		_stringLiteral801 = il2cpp_codegen_string_literal_from_index(801);
		_stringLiteral802 = il2cpp_codegen_string_literal_from_index(802);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2874* V_0 = {0};
	{
		SerializationInfo_t1_293 * L_0 = ___info;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		SerializationInfo_t1_293 * L_2 = ___info;
		int32_t L_3 = (int32_t)(__this->___generation_14);
		NullCheck((SerializationInfo_t1_293 *)L_2);
		SerializationInfo_AddValue_m1_9630((SerializationInfo_t1_293 *)L_2, (String_t*)_stringLiteral799, (int32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_4 = ___info;
		Object_t* L_5 = (Object_t*)(__this->___hcp_12);
		NullCheck((SerializationInfo_t1_293 *)L_4);
		SerializationInfo_AddValue_m1_9642((SerializationInfo_t1_293 *)L_4, (String_t*)_stringLiteral800, (Object_t *)L_5, /*hidden argument*/NULL);
		V_0 = (KeyValuePair_2U5BU5D_t1_2874*)NULL;
		int32_t L_6 = (int32_t)(__this->___count_10);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_7 = (int32_t)(__this->___count_10);
		V_0 = (KeyValuePair_2U5BU5D_t1_2874*)((KeyValuePair_2U5BU5D_t1_2874*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 49), L_7));
		KeyValuePair_2U5BU5D_t1_2874* L_8 = V_0;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, KeyValuePair_2U5BU5D_t1_2874*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((Dictionary_2_t1_2663 *)__this, (KeyValuePair_2U5BU5D_t1_2874*)L_8, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0055:
	{
		SerializationInfo_t1_293 * L_9 = ___info;
		Int32U5BU5D_t1_275* L_10 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_10);
		NullCheck((SerializationInfo_t1_293 *)L_9);
		SerializationInfo_AddValue_m1_9630((SerializationInfo_t1_293 *)L_9, (String_t*)_stringLiteral801, (int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length)))), /*hidden argument*/NULL);
		SerializationInfo_t1_293 * L_11 = ___info;
		KeyValuePair_2U5BU5D_t1_2874* L_12 = V_0;
		NullCheck((SerializationInfo_t1_293 *)L_11);
		SerializationInfo_AddValue_m1_9642((SerializationInfo_t1_293 *)L_11, (String_t*)_stringLiteral802, (Object_t *)(Object_t *)L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::OnDeserialization(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral799;
extern Il2CppCodeGenString* _stringLiteral800;
extern Il2CppCodeGenString* _stringLiteral801;
extern Il2CppCodeGenString* _stringLiteral802;
extern "C" void Dictionary_2_OnDeserialization_m1_26409_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral799 = il2cpp_codegen_string_literal_from_index(799);
		_stringLiteral800 = il2cpp_codegen_string_literal_from_index(800);
		_stringLiteral801 = il2cpp_codegen_string_literal_from_index(801);
		_stringLiteral802 = il2cpp_codegen_string_literal_from_index(802);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2U5BU5D_t1_2874* V_1 = {0};
	int32_t V_2 = 0;
	{
		SerializationInfo_t1_293 * L_0 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		SerializationInfo_t1_293 * L_1 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1_293 *)L_1);
		int32_t L_2 = SerializationInfo_GetInt32_m1_9650((SerializationInfo_t1_293 *)L_1, (String_t*)_stringLiteral799, /*hidden argument*/NULL);
		__this->___generation_14 = L_2;
		SerializationInfo_t1_293 * L_3 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 50)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1_293 *)L_3);
		Object_t * L_5 = SerializationInfo_GetValue_m1_9625((SerializationInfo_t1_293 *)L_3, (String_t*)_stringLiteral800, (Type_t *)L_4, /*hidden argument*/NULL);
		__this->___hcp_12 = ((Object_t*)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)));
		SerializationInfo_t1_293 * L_6 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		NullCheck((SerializationInfo_t1_293 *)L_6);
		int32_t L_7 = SerializationInfo_GetInt32_m1_9650((SerializationInfo_t1_293 *)L_6, (String_t*)_stringLiteral801, /*hidden argument*/NULL);
		V_0 = (int32_t)L_7;
		SerializationInfo_t1_293 * L_8 = (SerializationInfo_t1_293 *)(__this->___serialization_info_13);
		Type_t * L_9 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 51)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t1_293 *)L_8);
		Object_t * L_10 = SerializationInfo_GetValue_m1_9625((SerializationInfo_t1_293 *)L_8, (String_t*)_stringLiteral802, (Type_t *)L_9, /*hidden argument*/NULL);
		V_1 = (KeyValuePair_2U5BU5D_t1_2874*)((KeyValuePair_2U5BU5D_t1_2874*)Castclass(L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)));
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		V_0 = (int32_t)((int32_t)10);
	}

IL_0083:
	{
		int32_t L_12 = V_0;
		NullCheck((Dictionary_2_t1_2663 *)__this);
		(( void (*) (Dictionary_2_t1_2663 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)((Dictionary_2_t1_2663 *)__this, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		__this->___count_10 = 0;
		KeyValuePair_2U5BU5D_t1_2874* L_13 = V_1;
		if (!L_13)
		{
			goto IL_00c9;
		}
	}
	{
		V_2 = (int32_t)0;
		goto IL_00c0;
	}

IL_009e:
	{
		KeyValuePair_2U5BU5D_t1_2874* L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)((KeyValuePair_2_t1_2665 *)(KeyValuePair_2_t1_2665 *)SZArrayLdElema(L_14, L_15, sizeof(KeyValuePair_2_t1_2665 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		KeyValuePair_2U5BU5D_t1_2874* L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Object_t * L_19 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2665 *)((KeyValuePair_2_t1_2665 *)(KeyValuePair_2_t1_2665 *)SZArrayLdElema(L_17, L_18, sizeof(KeyValuePair_2_t1_2665 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Add(TKey,TValue) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_16, (Object_t *)L_19);
		int32_t L_20 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_21 = V_2;
		KeyValuePair_2U5BU5D_t1_2874* L_22 = V_1;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_22)->max_length)))))))
		{
			goto IL_009e;
		}
	}

IL_00c9:
	{
		int32_t L_23 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_23+(int32_t)1));
		__this->___serialization_info_13 = (SerializationInfo_t1_293 *)NULL;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::Remove(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_Remove_m1_26411_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = {0};
	Object_t * V_5 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		int32_t L_5 = V_0;
		Int32U5BU5D_t1_275* L_6 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_6);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length))))));
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_7, L_9, sizeof(int32_t)))-(int32_t)1));
		int32_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
		{
			goto IL_004e;
		}
	}
	{
		return 0;
	}

IL_004e:
	{
		V_3 = (int32_t)(-1);
	}

IL_0050:
	{
		LinkU5BU5D_t1_1975* L_11 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_11, L_12, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_14 = V_0;
		if ((!(((uint32_t)L_13) == ((uint32_t)L_14))))
		{
			goto IL_0089;
		}
	}
	{
		Object_t* L_15 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_16 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_17 = V_2;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = ___key;
		NullCheck((Object_t*)L_15);
		bool L_20 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_15, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_16, L_18, sizeof(int32_t))), (int32_t)L_19);
		if (!L_20)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_00a4;
	}

IL_0089:
	{
		int32_t L_21 = V_2;
		V_3 = (int32_t)L_21;
		LinkU5BU5D_t1_1975* L_22 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_22, L_23, sizeof(Link_t1_256 )))->___Next_1);
		V_2 = (int32_t)L_24;
		int32_t L_25 = V_2;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_0050;
		}
	}

IL_00a4:
	{
		int32_t L_26 = V_2;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_00ad;
		}
	}
	{
		return 0;
	}

IL_00ad:
	{
		int32_t L_27 = (int32_t)(__this->___count_10);
		__this->___count_10 = ((int32_t)((int32_t)L_27-(int32_t)1));
		int32_t L_28 = V_3;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_00e2;
		}
	}
	{
		Int32U5BU5D_t1_275* L_29 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_30 = V_1;
		LinkU5BU5D_t1_1975* L_31 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_32 = V_2;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_31, L_32, sizeof(Link_t1_256 )))->___Next_1);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, L_30, sizeof(int32_t))) = (int32_t)((int32_t)((int32_t)L_33+(int32_t)1));
		goto IL_0104;
	}

IL_00e2:
	{
		LinkU5BU5D_t1_1975* L_34 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_35 = V_3;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		LinkU5BU5D_t1_1975* L_36 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_37 = V_2;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_36, L_37, sizeof(Link_t1_256 )))->___Next_1);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_34, L_35, sizeof(Link_t1_256 )))->___Next_1 = L_38;
	}

IL_0104:
	{
		LinkU5BU5D_t1_1975* L_39 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = (int32_t)(__this->___emptySlot_9);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_39, L_40, sizeof(Link_t1_256 )))->___Next_1 = L_41;
		int32_t L_42 = V_2;
		__this->___emptySlot_9 = L_42;
		LinkU5BU5D_t1_1975* L_43 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_43, L_44, sizeof(Link_t1_256 )))->___HashCode_0 = 0;
		ExifTagU5BU5D_t8_379* L_45 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_46 = V_2;
		Initobj (ExifTag_t8_132_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_47 = V_4;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_45, L_46, sizeof(int32_t))) = (int32_t)L_47;
		ObjectU5BU5D_t1_272* L_48 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		int32_t L_49 = V_2;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_5));
		Object_t * L_50 = V_5;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, L_49, sizeof(Object_t *))) = (Object_t *)L_50;
		int32_t L_51 = (int32_t)(__this->___generation_14);
		__this->___generation_14 = ((int32_t)((int32_t)L_51+(int32_t)1));
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::TryGetValue(TKey,TValue&)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool Dictionary_2_TryGetValue_m1_26413_gshared (Dictionary_2_t1_2663 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		Object_t* L_2 = (Object_t*)(__this->___hcp_12);
		int32_t L_3 = ___key;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_2, (int32_t)L_3);
		V_0 = (int32_t)((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
		Int32U5BU5D_t1_275* L_5 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		int32_t L_6 = V_0;
		Int32U5BU5D_t1_275* L_7 = (Int32U5BU5D_t1_275*)(__this->___table_4);
		NullCheck(L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))));
		int32_t L_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length))))));
		V_1 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_8, sizeof(int32_t)))-(int32_t)1));
		goto IL_00a2;
	}

IL_0048:
	{
		LinkU5BU5D_t1_1975* L_9 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_9, L_10, sizeof(Link_t1_256 )))->___HashCode_0);
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_0090;
		}
	}
	{
		Object_t* L_13 = (Object_t*)(__this->___hcp_12);
		ExifTagU5BU5D_t8_379* L_14 = (ExifTagU5BU5D_t8_379*)(__this->___keySlots_6);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = ___key;
		NullCheck((Object_t*)L_13);
		bool L_18 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35), (Object_t*)L_13, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))), (int32_t)L_17);
		if (!L_18)
		{
			goto IL_0090;
		}
	}
	{
		Object_t ** L_19 = ___value;
		ObjectU5BU5D_t1_272* L_20 = (ObjectU5BU5D_t1_272*)(__this->___valueSlots_7);
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		(*(Object_t **)L_19) = (*(Object_t **)(Object_t **)SZArrayLdElema(L_20, L_22, sizeof(Object_t *)));
		return 1;
	}

IL_0090:
	{
		LinkU5BU5D_t1_1975* L_23 = (LinkU5BU5D_t1_1975*)(__this->___linkSlots_5);
		int32_t L_24 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_23, L_24, sizeof(Link_t1_256 )))->___Next_1);
		V_1 = (int32_t)L_25;
	}

IL_00a2:
	{
		int32_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)(-1)))))
		{
			goto IL_0048;
		}
	}
	{
		Object_t ** L_27 = ___value;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_2));
		Object_t * L_28 = V_2;
		(*(Object_t **)L_27) = L_28;
		return 0;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Keys()
extern "C" KeyCollection_t1_2668 * Dictionary_2_get_Keys_m1_26415_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		KeyCollection_t1_2668 * L_0 = (KeyCollection_t1_2668 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 52));
		(( void (*) (KeyCollection_t1_2668 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53)->method)(L_0, (Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 53));
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Values()
extern "C" ValueCollection_t1_2672 * Dictionary_2_get_Values_m1_26416_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		ValueCollection_t1_2672 * L_0 = (ValueCollection_t1_2672 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 54));
		(( void (*) (ValueCollection_t1_2672 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55)->method)(L_0, (Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 55));
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ToTKey(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern Il2CppCodeGenString* _stringLiteral803;
extern "C" int32_t Dictionary_2_ToTKey_m1_26418_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		_stringLiteral803 = il2cpp_codegen_string_literal_from_index(803);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 56)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1_559(NULL /*static, unused*/, (String_t*)_stringLiteral803, (String_t*)L_4, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_6 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_6, (String_t*)L_5, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}

IL_0040:
	{
		Object_t * L_7 = ___key;
		return ((*(int32_t*)((int32_t*)UnBox (L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)))));
	}
}
// TValue System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ToTValue(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral803;
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" Object_t * Dictionary_2_ToTValue_m1_26420_gshared (Dictionary_2_t1_2663 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral803 = il2cpp_codegen_string_literal_from_index(803);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___value;
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(147 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_1);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_0));
		Object_t * L_3 = V_0;
		return L_3;
	}

IL_0024:
	{
		Object_t * L_4 = ___value;
		if (((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14))))
		{
			goto IL_0053;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 57)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, (Type_t *)L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1_559(NULL /*static, unused*/, (String_t*)_stringLiteral803, (String_t*)L_6, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_8 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_8, (String_t*)L_7, (String_t*)_stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0053:
	{
		Object_t * L_9 = ___value;
		return ((Object_t *)Castclass(L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)));
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_26422_gshared (Dictionary_2_t1_2663 * __this, KeyValuePair_2_t1_2665  ___pair, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		NullCheck((Dictionary_2_t1_2663 *)__this);
		bool L_1 = (bool)VirtFuncInvoker2< bool, int32_t, Object_t ** >::Invoke(21 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::TryGetValue(TKey,TValue&) */, (Dictionary_2_t1_2663 *)__this, (int32_t)L_0, (Object_t **)(&V_0));
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 47));
		EqualityComparer_1_t1_1942 * L_2 = (( EqualityComparer_1_t1_1942 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 46));
		Object_t * L_3 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2665 *)(&___pair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Object_t * L_4 = V_0;
		NullCheck((EqualityComparer_1_t1_1942 *)L_2);
		bool L_5 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(T,T) */, (EqualityComparer_1_t1_1942 *)L_2, (Object_t *)L_3, (Object_t *)L_4);
		return L_5;
	}
}
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2670  Dictionary_2_GetEnumerator_m1_26423_gshared (Dictionary_2_t1_2663 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670  L_0 = {0};
		(( void (*) (Enumerator_t1_2670 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(&L_0, (Dictionary_2_t1_2663 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		return L_0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_26425_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), &L_1);
		Object_t * L_3 = ___value;
		DictionaryEntry_t1_284  L_4 = {0};
		DictionaryEntry__ctor_m1_3228(&L_4, (Object_t *)L_2, (Object_t *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26426_gshared (InternalEnumerator_1_t1_2666 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26427_gshared (InternalEnumerator_1_t1_2666 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26428_gshared (InternalEnumerator_1_t1_2666 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2665  L_0 = (( KeyValuePair_2_t1_2665  (*) (InternalEnumerator_1_t1_2666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2666 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2665  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26429_gshared (InternalEnumerator_1_t1_2666 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26430_gshared (InternalEnumerator_1_t1_2666 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" KeyValuePair_2_t1_2665  InternalEnumerator_1_get_Current_m1_26431_gshared (InternalEnumerator_1_t1_2666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		KeyValuePair_2_t1_2665  L_8 = (( KeyValuePair_2_t1_2665  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_26432_gshared (KeyValuePair_2_t1_2665 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		(( void (*) (KeyValuePair_2_t1_2665 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t1_2665 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = ___value;
		(( void (*) (KeyValuePair_2_t1_2665 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1_2665 *)__this, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m1_26433_gshared (KeyValuePair_2_t1_2665 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___key_0);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_26434_gshared (KeyValuePair_2_t1_2665 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___key_0 = L_0;
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m1_26435_gshared (KeyValuePair_2_t1_2665 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___value_1);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_26436_gshared (KeyValuePair_2_t1_2665 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___value_1 = L_0;
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>::ToString()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral264;
extern Il2CppCodeGenString* _stringLiteral167;
extern Il2CppCodeGenString* _stringLiteral266;
extern "C" String_t* KeyValuePair_2_ToString_m1_26437_gshared (KeyValuePair_2_t1_2665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral264 = il2cpp_codegen_string_literal_from_index(264);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1_238* G_B2_1 = {0};
	StringU5BU5D_t1_238* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1_238* G_B1_1 = {0};
	StringU5BU5D_t1_238* G_B1_2 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1_238* G_B3_2 = {0};
	StringU5BU5D_t1_238* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1_238* G_B5_1 = {0};
	StringU5BU5D_t1_238* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1_238* G_B4_1 = {0};
	StringU5BU5D_t1_238* G_B4_2 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1_238* G_B6_2 = {0};
	StringU5BU5D_t1_238* G_B6_3 = {0};
	{
		StringU5BU5D_t1_238* L_0 = (StringU5BU5D_t1_238*)((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral264);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral264;
		StringU5BU5D_t1_238* L_1 = (StringU5BU5D_t1_238*)L_0;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0)));
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(String_t*))) = (String_t*)G_B3_0;
		StringU5BU5D_t1_238* L_6 = (StringU5BU5D_t1_238*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral167);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 2, sizeof(String_t*))) = (String_t*)_stringLiteral167;
		StringU5BU5D_t1_238* L_7 = (StringU5BU5D_t1_238*)L_6;
		Object_t * L_8 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1_2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Object_t * L_9 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1_2665 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Object_t *)L_9;
		NullCheck((Object_t *)(*(&V_1)));
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(String_t*))) = (String_t*)G_B6_0;
		StringU5BU5D_t1_238* L_12 = (StringU5BU5D_t1_238*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral266);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 4, sizeof(String_t*))) = (String_t*)_stringLiteral266;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_563(NULL /*static, unused*/, (StringU5BU5D_t1_238*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26438_gshared (InternalEnumerator_1_t1_2667 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26439_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26440_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (InternalEnumerator_1_t1_2667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26441_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26442_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ExifLibrary.ExifTag>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" int32_t InternalEnumerator_1_get_Current_m1_26443_gshared (InternalEnumerator_1_t1_2667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		int32_t L_8 = (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral792;
extern "C" void KeyCollection__ctor_m1_26444_gshared (KeyCollection_t1_2668 * __this, Dictionary_2_t1_2663 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1_2663 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral792, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t1_2663 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1_26445_gshared (KeyCollection_t1_2668 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1_26446_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1_26447_gshared (KeyCollection_t1_2668 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		int32_t L_1 = ___item;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::ContainsKey(TKey) */, (Dictionary_2_t1_2663 *)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1_26448_gshared (KeyCollection_t1_2668 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1_26449_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1_2668 *)__this);
		Enumerator_t1_2669  L_0 = (( Enumerator_t1_2669  (*) (KeyCollection_t1_2668 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t1_2668 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2669  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m1_26450_gshared (KeyCollection_t1_2668 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ExifTagU5BU5D_t8_379* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ExifTagU5BU5D_t8_379*)((ExifTagU5BU5D_t8_379*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ExifTagU5BU5D_t8_379* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ExifTagU5BU5D_t8_379* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((KeyCollection_t1_2668 *)__this);
		(( void (*) (KeyCollection_t1_2668 *, ExifTagU5BU5D_t8_379*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyCollection_t1_2668 *)__this, (ExifTagU5BU5D_t8_379*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1_2663 * L_4 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)L_4);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2663 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2663 * L_7 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2671 * L_11 = (Transform_1_t1_2671 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2671 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2663 *)L_7);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, Transform_1_t1_2671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2663 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t1_2671 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1_26451_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1_2668 *)__this);
		Enumerator_t1_2669  L_0 = (( Enumerator_t1_2669  (*) (KeyCollection_t1_2668 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyCollection_t1_2668 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2669  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1_26452_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1_26453_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1_26454_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m1_26455_gshared (KeyCollection_t1_2668 * __this, ExifTagU5BU5D_t8_379* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		ExifTagU5BU5D_t8_379* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2663 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2663 * L_3 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		ExifTagU5BU5D_t8_379* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2671 * L_7 = (Transform_1_t1_2671 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2671 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2663 *)L_3);
		(( void (*) (Dictionary_2_t1_2663 *, ExifTagU5BU5D_t8_379*, int32_t, Transform_1_t1_2671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t1_2663 *)L_3, (ExifTagU5BU5D_t8_379*)L_4, (int32_t)L_5, (Transform_1_t1_2671 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2669  KeyCollection_GetEnumerator_m1_26456_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Enumerator_t1_2669  L_1 = {0};
		(( void (*) (Enumerator_t1_2669 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t1_2663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<ExifLibrary.ExifTag,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m1_26457_gshared (KeyCollection_t1_2668 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Count() */, (Dictionary_2_t1_2663 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26458_gshared (Enumerator_t1_2669 * __this, Dictionary_2_t1_2663 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = ___host;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		Enumerator_t1_2670  L_1 = (( Enumerator_t1_2670  (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26459_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		int32_t L_1 = (( int32_t (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26460_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_26461_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26462_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m1_26463_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2665 * L_1 = (KeyValuePair_2_t1_2665 *)&(L_0->___current_3);
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2665 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26464_gshared (Enumerator_t1_2670 * __this, Dictionary_2_t1_2663 * ___dictionary, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = ___dictionary;
		__this->___dictionary_0 = L_0;
		Dictionary_2_t1_2663 * L_1 = ___dictionary;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___generation_14);
		__this->___stamp_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26465_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2665  L_0 = (KeyValuePair_2_t1_2665 )(__this->___current_3);
		KeyValuePair_2_t1_2665  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26466_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26467_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2665 * L_0 = (KeyValuePair_2_t1_2665 *)&(__this->___current_3);
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1_2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1_2665 * L_4 = (KeyValuePair_2_t1_2665 *)&(__this->___current_3);
		Object_t * L_5 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t1_284  L_6 = {0};
		DictionaryEntry__ctor_m1_3228(&L_6, (Object_t *)L_3, (Object_t *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26468_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26469_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26470_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->___next_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1_2663 * L_4 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck(L_4);
		LinkU5BU5D_t1_1975* L_5 = (LinkU5BU5D_t1_1975*)(L_4->___linkSlots_5);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)(((Link_t1_256 *)(Link_t1_256 *)SZArrayLdElema(L_5, L_6, sizeof(Link_t1_256 )))->___HashCode_0);
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1_2663 * L_8 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck(L_8);
		ExifTagU5BU5D_t8_379* L_9 = (ExifTagU5BU5D_t8_379*)(L_8->___keySlots_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t1_2663 * L_12 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck(L_12);
		ObjectU5BU5D_t1_272* L_13 = (ObjectU5BU5D_t1_272*)(L_12->___valueSlots_7);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t1_2665  L_16 = {0};
		(( void (*) (KeyValuePair_2_t1_2665 *, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(&L_16, (int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_9, L_11, sizeof(int32_t))), (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15, sizeof(Object_t *))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->___current_3 = L_16;
		return 1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)(__this->___next_1);
		Dictionary_2_t1_2663 * L_18 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)(L_18->___touchedSlots_8);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1_2665  Enumerator_get_Current_m1_26471_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2665  L_0 = (KeyValuePair_2_t1_2665 )(__this->___current_3);
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m1_26472_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2665 * L_0 = (KeyValuePair_2_t1_2665 *)&(__this->___current_3);
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1_2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m1_26473_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2665 * L_0 = (KeyValuePair_2_t1_2665 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::Reset()
extern "C" void Enumerator_Reset_m1_26474_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->___next_1 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::VerifyState()
extern TypeInfo* ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral804;
extern "C" void Enumerator_VerifyState_m1_26475_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral804 = il2cpp_codegen_string_literal_from_index(804);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1_1588 * L_1 = (ObjectDisposedException_t1_1588 *)il2cpp_codegen_object_new (ObjectDisposedException_t1_1588_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1_14523(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		Dictionary_2_t1_2663 * L_2 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->___generation_14);
		int32_t L_4 = (int32_t)(__this->___stamp_2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)_stringLiteral804, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::VerifyCurrent()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral805;
extern "C" void Enumerator_VerifyCurrent_m1_26476_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral805 = il2cpp_codegen_string_literal_from_index(805);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1_2670 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral805, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_26477_gshared (Enumerator_t1_2670 * __this, const MethodInfo* method)
{
	{
		__this->___dictionary_0 = (Dictionary_2_t1_2663 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_26478_gshared (Transform_1_t1_2671 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m1_26479_gshared (Transform_1_t1_2671 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_26479((Transform_1_t1_2671 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_26480_gshared (Transform_1_t1_2671 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ExifTag_t8_132_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,ExifLibrary.ExifTag>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m1_26481_gshared (Transform_1_t1_2671 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral792;
extern "C" void ValueCollection__ctor_m1_26482_gshared (ValueCollection_t1_2672 * __this, Dictionary_2_t1_2663 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral792 = il2cpp_codegen_string_literal_from_index(792);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1_2663 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral792, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0017:
	{
		Dictionary_2_t1_2663 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26483_gshared (ValueCollection_t1_2672 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26484_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26485_gshared (ValueCollection_t1_2672 * __this, Object_t * ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Object_t * L_1 = ___item;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		bool L_2 = (( bool (*) (Dictionary_2_t1_2663 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)L_0, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26486_gshared (ValueCollection_t1_2672 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral806, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26487_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1_2672 *)__this);
		Enumerator_t1_2673  L_0 = (( Enumerator_t1_2673  (*) (ValueCollection_t1_2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t1_2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2673  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_26488_gshared (ValueCollection_t1_2672 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ObjectU5BU5D_t1_272* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ObjectU5BU5D_t1_272* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((ValueCollection_t1_2672 *)__this);
		(( void (*) (ValueCollection_t1_2672 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ValueCollection_t1_2672 *)__this, (ObjectU5BU5D_t1_272*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1_2663 * L_4 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)L_4);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2663 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2663 * L_7 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2674 * L_11 = (Transform_1_t1_2674 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2674 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2663 *)L_7);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, Transform_1_t1_2674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t1_2663 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t1_2674 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26489_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t1_2672 *)__this);
		Enumerator_t1_2673  L_0 = (( Enumerator_t1_2673  (*) (ValueCollection_t1_2672 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t1_2672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t1_2673  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26490_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26491_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26492_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_26493_gshared (ValueCollection_t1_2672 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		ObjectU5BU5D_t1_272* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		(( void (*) (Dictionary_2_t1_2663 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t1_2663 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t1_2663 * L_3 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		ObjectU5BU5D_t1_272* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t1_2674 * L_7 = (Transform_1_t1_2674 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t1_2674 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t1_2663 *)L_3);
		(( void (*) (Dictionary_2_t1_2663 *, ObjectU5BU5D_t1_272*, int32_t, Transform_1_t1_2674 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t1_2663 *)L_3, (ObjectU5BU5D_t1_272*)L_4, (int32_t)L_5, (Transform_1_t1_2674 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2673  ValueCollection_GetEnumerator_m1_26494_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		Enumerator_t1_2673  L_1 = {0};
		(( void (*) (Enumerator_t1_2673 *, Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t1_2663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_26495_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = (Dictionary_2_t1_2663 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>::get_Count() */, (Dictionary_2_t1_2663 *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26496_gshared (Enumerator_t1_2673 * __this, Dictionary_2_t1_2663 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t1_2663 * L_0 = ___host;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		Enumerator_t1_2670  L_1 = (( Enumerator_t1_2670  (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26497_gshared (Enumerator_t1_2673 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		Object_t * L_1 = (( Object_t * (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26498_gshared (Enumerator_t1_2673 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_26499_gshared (Enumerator_t1_2673 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26500_gshared (Enumerator_t1_2673 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_26501_gshared (Enumerator_t1_2673 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2665 * L_1 = (KeyValuePair_2_t1_2665 *)&(L_0->___current_3);
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t1_2665 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_26502_gshared (Transform_1_t1_2674 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m1_26503_gshared (Transform_1_t1_2674 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_26503((Transform_1_t1_2674 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_26504_gshared (Transform_1_t1_2674 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ExifTag_t8_132_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m1_26505_gshared (Transform_1_t1_2674 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_26506_gshared (Transform_1_t1_2664 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Transform_1_Invoke_m1_26507_gshared (Transform_1_t1_2664 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_26507((Transform_1_t1_2664 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t1_284  (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef DictionaryEntry_t1_284  (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_26508_gshared (Transform_1_t1_2664 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ExifTag_t8_132_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t1_284  Transform_1_EndInvoke_m1_26509_gshared (Transform_1_t1_2664 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(DictionaryEntry_t1_284 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m1_26510_gshared (Transform_1_t1_2675 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t1_2665  Transform_1_Invoke_m1_26511_gshared (Transform_1_t1_2675 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m1_26511((Transform_1_t1_2675 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1_2665  (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef KeyValuePair_2_t1_2665  (*FunctionPointerType) (Object_t * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ExifTag_t8_132_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m1_26512_gshared (Transform_1_t1_2675 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExifTag_t8_132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1983);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ExifTag_t8_132_il2cpp_TypeInfo_var, &___key);
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ExifLibrary.ExifTag,System.Object,System.Collections.Generic.KeyValuePair`2<ExifLibrary.ExifTag,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t1_2665  Transform_1_EndInvoke_m1_26513_gshared (Transform_1_t1_2675 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(KeyValuePair_2_t1_2665 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_26514_gshared (ShimEnumerator_t1_2676 * __this, Dictionary_2_t1_2663 * ___host, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1_2663 * L_0 = ___host;
		NullCheck((Dictionary_2_t1_2663 *)L_0);
		Enumerator_t1_2670  L_1 = (( Enumerator_t1_2670  (*) (Dictionary_2_t1_2663 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t1_2663 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_26515_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_26516_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Entry()
extern TypeInfo* IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_26517_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1_2670  L_0 = (Enumerator_t1_2670 )(__this->___host_enumerator_0);
		Enumerator_t1_2670  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		NullCheck((Object_t *)L_2);
		DictionaryEntry_t1_284  L_3 = (DictionaryEntry_t1_284 )InterfaceFuncInvoker0< DictionaryEntry_t1_284  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t1_863_il2cpp_TypeInfo_var, (Object_t *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_26518_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1_2665  V_0 = {0};
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2665  L_1 = (( KeyValuePair_2_t1_2665  (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (KeyValuePair_2_t1_2665 )L_1;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2665 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_26519_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1_2665  V_0 = {0};
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t1_2665  L_1 = (( KeyValuePair_2_t1_2665  (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_0 = (KeyValuePair_2_t1_2665 )L_1;
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t1_2665 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((KeyValuePair_2_t1_2665 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern TypeInfo* DictionaryEntry_t1_284_il2cpp_TypeInfo_var;
extern "C" Object_t * ShimEnumerator_get_Current_m1_26520_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntry_t1_284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1_2676 *)__this);
		DictionaryEntry_t1_284  L_0 = (DictionaryEntry_t1_284 )VirtFuncInvoker0< DictionaryEntry_t1_284  >::Invoke(7 /* System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Entry() */, (ShimEnumerator_t1_2676 *)__this);
		DictionaryEntry_t1_284  L_1 = L_0;
		Object_t * L_2 = Box(DictionaryEntry_t1_284_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_26521_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1_2670 * L_0 = (Enumerator_t1_2670 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t1_2670 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Enumerator_t1_2670 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::.ctor()
extern "C" void EqualityComparer_1__ctor_m1_26522_gshared (EqualityComparer_1_t1_2677 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t1_3064_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m1_26523_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t1_3064_0_0_0_var = il2cpp_codegen_type_from_index(4654);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GenericEqualityComparer_1_t1_3064_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1_31* L_4 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1_31*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t1_2677_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t1_2677 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1_2678 * L_8 = (DefaultComparer_t1_2678 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t1_2678 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t1_2677_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1_26524_gshared (EqualityComparer_1_t1_2677 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t1_2677 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::GetHashCode(T) */, (EqualityComparer_1_t1_2677 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1_26525_gshared (EqualityComparer_1_t1_2677 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t1_2677 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::Equals(T,T) */, (EqualityComparer_1_t1_2677 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<ExifLibrary.ExifTag>::get_Default()
extern "C" EqualityComparer_1_t1_2677 * EqualityComparer_1_get_Default_m1_26526_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t1_2677 * L_0 = ((EqualityComparer_1_t1_2677_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExifLibrary.ExifTag>::.ctor()
extern "C" void DefaultComparer__ctor_m1_26527_gshared (DefaultComparer_t1_2678 * __this, const MethodInfo* method)
{
	{
		NullCheck((EqualityComparer_1_t1_2677 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (EqualityComparer_1_t1_2677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t1_2677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExifLibrary.ExifTag>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m1_26528_gshared (DefaultComparer_t1_2678 * __this, int32_t ___obj, const MethodInfo* method)
{
	{
		int32_t L_0 = ___obj;
		goto IL_000d;
	}
	{
		return 0;
	}

IL_000d:
	{
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___obj)));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ExifLibrary.ExifTag>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m1_26529_gshared (DefaultComparer_t1_2678 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		goto IL_0015;
	}
	{
		int32_t L_1 = ___y;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return ((((Object_t*)(Object_t *)L_3) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0015:
	{
		int32_t L_4 = ___y;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)));
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (&___x)), (Object_t *)L_6);
		return L_7;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void SortedList_2__ctor_m3_1809_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = ((SortedList_2_t3_248_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___INITIAL_SIZE_0;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_0, (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::.ctor(System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral910;
extern "C" void SortedList_2__ctor_m3_2121_gshared (SortedList_2_t3_248 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral910 = il2cpp_codegen_string_literal_from_index(910);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_1 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_1, (String_t*)_stringLiteral910, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		__this->___defaultCapacity_5 = 0;
		goto IL_0035;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_3 = ((SortedList_2_t3_248_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___INITIAL_SIZE_0;
		__this->___defaultCapacity_5 = L_3;
	}

IL_0035:
	{
		Object_t* L_4 = ___comparer;
		int32_t L_5 = ___capacity;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, Object_t*, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((SortedList_2_t3_248 *)__this, (Object_t*)L_4, (int32_t)L_5, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::.cctor()
extern "C" void SortedList_2__cctor_m3_2122_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		((SortedList_2_t3_248_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___INITIAL_SIZE_0 = ((int32_t)16);
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool SortedList_2_System_Collections_ICollection_get_IsSynchronized_m3_2123_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * SortedList_2_System_Collections_ICollection_get_SyncRoot_m3_2124_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool SortedList_2_System_Collections_IDictionary_get_IsFixedSize_m3_2125_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool SortedList_2_System_Collections_IDictionary_get_IsReadOnly_m3_2126_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Item_m3_2127_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		if (((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_000d;
		}
	}
	{
		return NULL;
	}

IL_000d:
	{
		Object_t * L_1 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(32 /* TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Item(TKey) */, (SortedList_2_t3_248 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_set_Item_m3_2128_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (( int32_t (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((SortedList_2_t3_248 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Object_t * L_2 = ___value;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((SortedList_2_t3_248 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((SortedList_2_t3_248 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(33 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::set_Item(TKey,TValue) */, (SortedList_2_t3_248 *)__this, (int32_t)L_1, (int32_t)L_3);
		return;
	}
}
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Keys_m3_2129_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		ListKeys_t3_279 * L_0 = (ListKeys_t3_279 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		(( void (*) (ListKeys_t3_279 *, SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, (SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_0;
	}
}
// System.Collections.ICollection System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_get_Values_m3_2130_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		ListValues_t3_282 * L_0 = (ListValues_t3_282 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		(( void (*) (ListValues_t3_282 *, SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, (SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3_2131_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		Object_t* L_0 = (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)((SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_0;
	}
}
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3_2132_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		Object_t* L_0 = (( Object_t* (*) (SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3_2133_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Clear()
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Clear_m3_2134_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = ((SortedList_2_t3_248_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___INITIAL_SIZE_0;
		__this->___defaultCapacity_5 = L_0;
		int32_t L_1 = (int32_t)(__this->___defaultCapacity_5);
		__this->___table_3 = ((KeyValuePair_2U5BU5D_t1_2680*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), L_1));
		__this->___inUse_1 = 0;
		int32_t L_2 = (int32_t)(__this->___modificationCount_2);
		__this->___modificationCount_2 = ((int32_t)((int32_t)L_2+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1_1035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral892;
extern Il2CppCodeGenString* _stringLiteral893;
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3_2135_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2U5BU5D_t1_2680* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		IDisposable_t1_1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral892 = il2cpp_codegen_string_literal_from_index(892);
		_stringLiteral893 = il2cpp_codegen_string_literal_from_index(893);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t1_2681  V_1 = {0};
	Object_t* V_2 = {0};
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_1 = ___array;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13268(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0018:
	{
		int32_t L_3 = ___arrayIndex;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_4 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0025:
	{
		int32_t L_5 = ___arrayIndex;
		KeyValuePair_2U5BU5D_t1_2680* L_6 = ___array;
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_6)->max_length)))))))
		{
			goto IL_0039;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_7 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_7, (String_t*)_stringLiteral892, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0039:
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		KeyValuePair_2U5BU5D_t1_2680* L_9 = ___array;
		NullCheck(L_9);
		int32_t L_10 = ___arrayIndex;
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))-(int32_t)L_10)))))
		{
			goto IL_0054;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_11 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_11, (String_t*)_stringLiteral893, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0054:
	{
		int32_t L_12 = ___arrayIndex;
		V_0 = (int32_t)L_12;
		NullCheck((SortedList_2_t3_248 *)__this);
		Object_t* L_13 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(36 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::GetEnumerator() */, (SortedList_2_t3_248 *)__this);
		V_2 = (Object_t*)L_13;
	}

IL_005d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007a;
		}

IL_0062:
		{
			Object_t* L_14 = V_2;
			NullCheck((Object_t*)L_14);
			KeyValuePair_2_t1_2681  L_15 = (KeyValuePair_2_t1_2681 )InterfaceFuncInvoker0< KeyValuePair_2_t1_2681  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), (Object_t*)L_14);
			V_1 = (KeyValuePair_2_t1_2681 )L_15;
			KeyValuePair_2U5BU5D_t1_2680* L_16 = ___array;
			int32_t L_17 = V_0;
			int32_t L_18 = (int32_t)L_17;
			V_0 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_18);
			KeyValuePair_2_t1_2681  L_19 = V_1;
			(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_16, L_18, sizeof(KeyValuePair_2_t1_2681 )))) = L_19;
		}

IL_007a:
		{
			Object_t* L_20 = V_2;
			NullCheck((Object_t *)L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_20);
			if (L_21)
			{
				goto IL_0062;
			}
		}

IL_0085:
		{
			IL2CPP_LEAVE(0x95, FINALLY_008a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1_33 *)e.ex;
		goto FINALLY_008a;
	}

FINALLY_008a:
	{ // begin finally (depth: 1)
		{
			Object_t* L_22 = V_2;
			if (L_22)
			{
				goto IL_008e;
			}
		}

IL_008d:
		{
			IL2CPP_END_FINALLY(138)
		}

IL_008e:
		{
			Object_t* L_23 = V_2;
			NullCheck((Object_t *)L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1_1035_il2cpp_TypeInfo_var, (Object_t *)L_23);
			IL2CPP_END_FINALLY(138)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(138)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1_33 *)
	}

IL_0095:
	{
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3_2136_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2_t1_2681  ___keyValuePair, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((KeyValuePair_2_t1_2681 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		int32_t L_1 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((KeyValuePair_2_t1_2681 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		NullCheck((SortedList_2_t3_248 *)__this);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(28 /* System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(TKey,TValue) */, (SortedList_2_t3_248 *)__this, (int32_t)L_0, (int32_t)L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3_2137_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2_t1_2681  ___keyValuePair, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((KeyValuePair_2_t1_2681 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		Comparer_1_t1_2685 * L_3 = (( Comparer_1_t1_2685 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		KeyValuePair_2U5BU5D_t1_2680* L_4 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		KeyValuePair_2_t1_2681  L_6 = ___keyValuePair;
		NullCheck((Comparer_1_t1_2685 *)L_3);
		int32_t L_7 = (int32_t)VirtFuncInvoker2< int32_t, KeyValuePair_2_t1_2681 , KeyValuePair_2_t1_2681  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::Compare(!0,!0) */, (Comparer_1_t1_2685 *)L_3, (KeyValuePair_2_t1_2681 )(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_4, L_5, sizeof(KeyValuePair_2_t1_2681 )))), (KeyValuePair_2_t1_2681 )L_6);
		return ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
	}

IL_0035:
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool SortedList_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3_2138_gshared (SortedList_2_t3_248 * __this, KeyValuePair_2_t1_2681  ___keyValuePair, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((KeyValuePair_2_t1_2681 *)(&___keyValuePair), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		Comparer_1_t1_2685 * L_3 = (( Comparer_1_t1_2685 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		KeyValuePair_2U5BU5D_t1_2680* L_4 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		KeyValuePair_2_t1_2681  L_6 = ___keyValuePair;
		NullCheck((Comparer_1_t1_2685 *)L_3);
		int32_t L_7 = (int32_t)VirtFuncInvoker2< int32_t, KeyValuePair_2_t1_2681 , KeyValuePair_2_t1_2681  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::Compare(!0,!0) */, (Comparer_1_t1_2685 *)L_3, (KeyValuePair_2_t1_2681 )(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_4, L_5, sizeof(KeyValuePair_2_t1_2681 )))), (KeyValuePair_2_t1_2681 )L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_8 = V_0;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return 1;
	}

IL_003f:
	{
		return 0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* SortedList_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3_2139_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	GetEnumeratorU3Ec__Iterator0_t3_285 * V_0 = {0};
	{
		GetEnumeratorU3Ec__Iterator0_t3_285 * L_0 = (GetEnumeratorU3Ec__Iterator0_t3_285 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		(( void (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		V_0 = (GetEnumeratorU3Ec__Iterator0_t3_285 *)L_0;
		GetEnumeratorU3Ec__Iterator0_t3_285 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		GetEnumeratorU3Ec__Iterator0_t3_285 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * SortedList_2_System_Collections_IEnumerable_GetEnumerator_m3_2140_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		Object_t* L_0 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(36 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::GetEnumerator() */, (SortedList_2_t3_248 *)__this);
		return L_0;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void SortedList_2_System_Collections_IDictionary_Add_m3_2141_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (( int32_t (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((SortedList_2_t3_248 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Object_t * L_2 = ___value;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((SortedList_2_t3_248 *)__this, (Object_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_1, (int32_t)L_3, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.Contains(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern "C" bool SortedList_2_System_Collections_IDictionary_Contains_m3_2142_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13268(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_000c:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_0019;
		}
	}
	{
		return 0;
	}

IL_0019:
	{
		Object_t * L_3 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_4 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return ((((int32_t)((((int32_t)L_4) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.IDictionaryEnumerator System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * SortedList_2_System_Collections_IDictionary_GetEnumerator_m3_2143_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3_286 * L_0 = (Enumerator_t3_286 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		(( void (*) (Enumerator_t3_286 *, SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_0, (SortedList_2_t3_248 *)__this, (int32_t)2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		return L_0;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.IDictionary.Remove(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void SortedList_2_System_Collections_IDictionary_Remove_m3_2144_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		Object_t * L_3 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_4 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((SortedList_2_t3_248 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		V_0 = (int32_t)L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_6 = V_0;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
	}

IL_0038:
	{
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral891;
extern Il2CppCodeGenString* _stringLiteral892;
extern Il2CppCodeGenString* _stringLiteral893;
extern "C" void SortedList_2_System_Collections_ICollection_CopyTo_m3_2145_gshared (SortedList_2_t3_248 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		_stringLiteral891 = il2cpp_codegen_string_literal_from_index(891);
		_stringLiteral892 = il2cpp_codegen_string_literal_from_index(892);
		_stringLiteral893 = il2cpp_codegen_string_literal_from_index(893);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Array_t * L_1 = ___array;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_2 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13268(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0018:
	{
		int32_t L_3 = ___arrayIndex;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_4 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
	}

IL_0025:
	{
		Array_t * L_5 = ___array;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Rank_m1_994((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)1)))
		{
			goto IL_003c;
		}
	}
	{
		ArgumentException_t1_1425 * L_7 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_7, (String_t*)_stringLiteral891, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_003c:
	{
		int32_t L_8 = ___arrayIndex;
		Array_t * L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_get_Length_m1_992((Array_t *)L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0053;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_11 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_11, (String_t*)_stringLiteral892, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0053:
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		Array_t * L_13 = ___array;
		NullCheck((Array_t *)L_13);
		int32_t L_14 = Array_get_Length_m1_992((Array_t *)L_13, /*hidden argument*/NULL);
		int32_t L_15 = ___arrayIndex;
		if ((((int32_t)L_12) <= ((int32_t)((int32_t)((int32_t)L_14-(int32_t)L_15)))))
		{
			goto IL_0071;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_16 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_16, (String_t*)_stringLiteral893, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_16);
	}

IL_0071:
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		Object_t* L_17 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(36 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::GetEnumerator() */, (SortedList_2_t3_248 *)__this);
		V_0 = (Object_t*)L_17;
		int32_t L_18 = ___arrayIndex;
		V_1 = (int32_t)L_18;
		goto IL_0095;
	}

IL_007f:
	{
		Array_t * L_19 = ___array;
		Object_t* L_20 = V_0;
		NullCheck((Object_t*)L_20);
		KeyValuePair_2_t1_2681  L_21 = (KeyValuePair_2_t1_2681 )InterfaceFuncInvoker0< KeyValuePair_2_t1_2681  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), (Object_t*)L_20);
		KeyValuePair_2_t1_2681  L_22 = L_21;
		Object_t * L_23 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33), &L_22);
		int32_t L_24 = V_1;
		int32_t L_25 = (int32_t)L_24;
		V_1 = (int32_t)((int32_t)((int32_t)L_25+(int32_t)1));
		NullCheck((Array_t *)L_19);
		Array_SetValue_m1_1020((Array_t *)L_19, (Object_t *)L_23, (int32_t)L_25, /*hidden argument*/NULL);
	}

IL_0095:
	{
		Object_t* L_26 = V_0;
		NullCheck((Object_t *)L_26);
		bool L_27 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_26);
		if (L_27)
		{
			goto IL_007f;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count()
extern "C" int32_t SortedList_2_get_Count_m3_2146_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___inUse_1);
		return L_0;
	}
}
// TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Item(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* KeyNotFoundException_t1_257_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" int32_t SortedList_2_get_Item_m3_2147_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		KeyNotFoundException_t1_257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1969);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_5 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_5, L_6, sizeof(KeyValuePair_2_t1_2681 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		return L_7;
	}

IL_0037:
	{
		KeyNotFoundException_t1_257 * L_8 = (KeyNotFoundException_t1_257 *)il2cpp_codegen_object_new (KeyNotFoundException_t1_257_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m1_2781(L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::set_Item(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void SortedList_2_set_Item_m3_2148_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___key;
		int32_t L_3 = ___value;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, (int32_t)L_3, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return;
	}
}
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Capacity()
extern "C" int32_t SortedList_2_get_Capacity_m3_2149_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t1_2680* L_0 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Array_t *)L_0)->max_length))));
	}
}
// System.Collections.Generic.IList`1<TKey> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Keys()
extern "C" Object_t* SortedList_2_get_Keys_m3_1810_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		ListKeys_t3_279 * L_0 = (ListKeys_t3_279 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		(( void (*) (ListKeys_t3_279 *, SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_0, (SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return L_0;
	}
}
// System.Collections.Generic.IList`1<TValue> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Values()
extern "C" Object_t* SortedList_2_get_Values_m3_1811_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		ListValues_t3_282 * L_0 = (ListValues_t3_282 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		(( void (*) (ListValues_t3_282 *, SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(L_0, (SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_0;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Add(TKey,TValue)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" void SortedList_2_Add_m3_2150_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___key;
		int32_t L_3 = ___value;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, (int32_t)L_3, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ContainsKey(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool SortedList_2_ContainsKey_m3_2151_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return ((((int32_t)((((int32_t)L_3) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::GetEnumerator()
extern "C" Object_t* SortedList_2_GetEnumerator_m3_2152_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator1_t3_288 * V_0 = {0};
	{
		U3CGetEnumeratorU3Ec__Iterator1_t3_288 * L_0 = (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		(( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 35));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator1_t3_288 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator1_t3_288 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CGetEnumeratorU3Ec__Iterator1_t3_288 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Remove(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool SortedList_2_Remove_m3_2153_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_5 = V_0;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Clear()
extern "C" void SortedList_2_Clear_m3_2154_gshared (SortedList_2_t3_248 * __this, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = ((SortedList_2_t3_248_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___INITIAL_SIZE_0;
		__this->___defaultCapacity_5 = L_0;
		int32_t L_1 = (int32_t)(__this->___defaultCapacity_5);
		__this->___table_3 = ((KeyValuePair_2U5BU5D_t1_2680*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), L_1));
		__this->___inUse_1 = 0;
		int32_t L_2 = (int32_t)(__this->___modificationCount_2);
		__this->___modificationCount_2 = ((int32_t)((int32_t)L_2+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::RemoveAt(System.Int32)
extern TypeInfo* KeyValuePair_2_t1_2681_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral894;
extern "C" void SortedList_2_RemoveAt_m3_1812_gshared (SortedList_2_t3_248 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyValuePair_2_t1_2681_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4454);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral894 = il2cpp_codegen_string_literal_from_index(894);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2680* V_0 = {0};
	int32_t V_1 = 0;
	KeyValuePair_2_t1_2681  V_2 = {0};
	{
		KeyValuePair_2U5BU5D_t1_2680* L_0 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		V_0 = (KeyValuePair_2U5BU5D_t1_2680*)L_0;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		V_1 = (int32_t)L_1;
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_3 = ___index;
		int32_t L_4 = V_1;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_5 = ___index;
		int32_t L_6 = V_1;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)((int32_t)L_6-(int32_t)1)))))
		{
			goto IL_003a;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_7 = V_0;
		int32_t L_8 = ___index;
		KeyValuePair_2U5BU5D_t1_2680* L_9 = V_0;
		int32_t L_10 = ___index;
		int32_t L_11 = V_1;
		int32_t L_12 = ___index;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)1)), (Array_t *)(Array_t *)L_9, (int32_t)L_10, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11-(int32_t)1))-(int32_t)L_12)), /*hidden argument*/NULL);
		goto IL_004f;
	}

IL_003a:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_13 = V_0;
		int32_t L_14 = ___index;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		Initobj (KeyValuePair_2_t1_2681_il2cpp_TypeInfo_var, (&V_2));
		KeyValuePair_2_t1_2681  L_15 = V_2;
		(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_13, L_14, sizeof(KeyValuePair_2_t1_2681 )))) = L_15;
	}

IL_004f:
	{
		int32_t L_16 = (int32_t)(__this->___inUse_1);
		__this->___inUse_1 = ((int32_t)((int32_t)L_16-(int32_t)1));
		int32_t L_17 = (int32_t)(__this->___modificationCount_2);
		__this->___modificationCount_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
		goto IL_007b;
	}

IL_0070:
	{
		ArgumentOutOfRangeException_t1_1501 * L_18 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_18, (String_t*)_stringLiteral894, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_18);
	}

IL_007b:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::IndexOfKey(TKey)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" int32_t SortedList_2_IndexOfKey_m3_2155_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		V_0 = (int32_t)0;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		int32_t L_2 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_0 = (int32_t)L_3;
		goto IL_0031;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_0025:
	{ // begin catch(System.Exception)
		{
			InvalidOperationException_t1_1559 * L_4 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1_14170(L_4, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_4);
		}

IL_002c:
		{
			goto IL_0031;
		}
	} // end catch (depth: 1)

IL_0031:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = V_0;
		return ((int32_t)((int32_t)L_5|(int32_t)((int32_t)((int32_t)L_6>>(int32_t)((int32_t)31)))));
	}
}
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::IndexOfValue(TValue)
extern "C" int32_t SortedList_2_IndexOfValue_m3_2156_gshared (SortedList_2_t3_248 * __this, int32_t ___value, const MethodInfo* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t1_2681  V_1 = {0};
	{
		int32_t L_0 = (int32_t)(__this->___inUse_1);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_0048;
	}

IL_0014:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_1 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		V_1 = (KeyValuePair_2_t1_2681 )(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_1, L_2, sizeof(KeyValuePair_2_t1_2681 ))));
		int32_t L_3 = ___value;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		int32_t L_6 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((KeyValuePair_2_t1_2681 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_7);
		bool L_9 = Object_Equals_m1_2(NULL /*static, unused*/, (Object_t *)L_5, (Object_t *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_10 = V_0;
		return L_10;
	}

IL_0044:
	{
		int32_t L_11 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = (int32_t)(__this->___inUse_1);
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::TryGetValue(TKey,TValue&)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* IFD_t8_131_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern "C" bool SortedList_2_TryGetValue_m3_2157_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IFD_t8_131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1976);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		int32_t L_2 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_3 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t* L_5 = ___value;
		KeyValuePair_2U5BU5D_t1_2680* L_6 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_6, L_7, sizeof(KeyValuePair_2_t1_2681 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		(*(int32_t*)L_5) = L_8;
		return 1;
	}

IL_003e:
	{
		int32_t* L_9 = ___value;
		Initobj (IFD_t8_131_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_10 = V_1;
		(*(int32_t*)L_9) = L_10;
		return 0;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::EnsureCapacity(System.Int32,System.Int32)
extern "C" void SortedList_2_EnsureCapacity_m3_2158_gshared (SortedList_2_t3_248 * __this, int32_t ___n, int32_t ___free, const MethodInfo* method)
{
	KeyValuePair_2U5BU5D_t1_2680* V_0 = {0};
	KeyValuePair_2U5BU5D_t1_2680* V_1 = {0};
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	int32_t G_B3_0 = 0;
	{
		KeyValuePair_2U5BU5D_t1_2680* L_0 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		V_0 = (KeyValuePair_2U5BU5D_t1_2680*)L_0;
		V_1 = (KeyValuePair_2U5BU5D_t1_2680*)NULL;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (( int32_t (*) (SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		V_2 = (int32_t)L_1;
		int32_t L_2 = ___free;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_3 = ___free;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		G_B3_0 = ((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_3 = (bool)G_B3_0;
		int32_t L_5 = ___n;
		int32_t L_6 = V_2;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_7 = ___n;
		V_1 = (KeyValuePair_2U5BU5D_t1_2680*)((KeyValuePair_2U5BU5D_t1_2680*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), ((int32_t)((int32_t)L_7<<(int32_t)1))));
	}

IL_0034:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_8 = V_1;
		if (!L_8)
		{
			goto IL_0093;
		}
	}
	{
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_10 = ___free;
		V_4 = (int32_t)L_10;
		int32_t L_11 = V_4;
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_12 = V_0;
		KeyValuePair_2U5BU5D_t1_2680* L_13 = V_1;
		int32_t L_14 = V_4;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_12, (int32_t)0, (Array_t *)(Array_t *)L_13, (int32_t)0, (int32_t)L_14, /*hidden argument*/NULL);
	}

IL_0056:
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		int32_t L_16 = ___free;
		V_4 = (int32_t)((int32_t)((int32_t)L_15-(int32_t)L_16));
		int32_t L_17 = V_4;
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0075;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_18 = V_0;
		int32_t L_19 = ___free;
		KeyValuePair_2U5BU5D_t1_2680* L_20 = V_1;
		int32_t L_21 = ___free;
		int32_t L_22 = V_4;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_18, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)((int32_t)((int32_t)L_21+(int32_t)1)), (int32_t)L_22, /*hidden argument*/NULL);
	}

IL_0075:
	{
		goto IL_0087;
	}

IL_007a:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_23 = V_0;
		KeyValuePair_2U5BU5D_t1_2680* L_24 = V_1;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		Array_Copy_m1_1040(NULL /*static, unused*/, (Array_t *)(Array_t *)L_23, (Array_t *)(Array_t *)L_24, (int32_t)L_25, /*hidden argument*/NULL);
	}

IL_0087:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_26 = V_1;
		__this->___table_3 = L_26;
		goto IL_00ac;
	}

IL_0093:
	{
		bool L_27 = V_3;
		if (!L_27)
		{
			goto IL_00ac;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_28 = V_0;
		int32_t L_29 = ___free;
		KeyValuePair_2U5BU5D_t1_2680* L_30 = V_0;
		int32_t L_31 = ___free;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		int32_t L_33 = ___free;
		Array_Copy_m1_1041(NULL /*static, unused*/, (Array_t *)(Array_t *)L_28, (int32_t)L_29, (Array_t *)(Array_t *)L_30, (int32_t)((int32_t)((int32_t)L_31+(int32_t)1)), (int32_t)((int32_t)((int32_t)L_32-(int32_t)L_33)), /*hidden argument*/NULL);
	}

IL_00ac:
	{
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::PutImpl(TKey,TValue,System.Boolean)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t1_33_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t1_3_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral867;
extern Il2CppCodeGenString* _stringLiteral4131;
extern Il2CppCodeGenString* _stringLiteral897;
extern Il2CppCodeGenString* _stringLiteral167;
extern Il2CppCodeGenString* _stringLiteral898;
extern Il2CppCodeGenString* _stringLiteral266;
extern "C" void SortedList_2_PutImpl_m3_2159_gshared (SortedList_2_t3_248 * __this, int32_t ___key, int32_t ___value, bool ___overwrite, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Exception_t1_33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t1_3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral867 = il2cpp_codegen_string_literal_from_index(867);
		_stringLiteral4131 = il2cpp_codegen_string_literal_from_index(4131);
		_stringLiteral897 = il2cpp_codegen_string_literal_from_index(897);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		_stringLiteral898 = il2cpp_codegen_string_literal_from_index(898);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2680* V_0 = {0};
	int32_t V_1 = 0;
	Exception_t1_33 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1_33 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___key;
		goto IL_0016;
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral867, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0016:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_2 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		V_0 = (KeyValuePair_2U5BU5D_t1_2680*)L_2;
		V_1 = (int32_t)(-1);
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		int32_t L_3 = ___key;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_4 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)((SortedList_2_t3_248 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		V_1 = (int32_t)L_4;
		goto IL_0038;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1_33 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1_33_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002c;
		throw e;
	}

CATCH_002c:
	{ // begin catch(System.Exception)
		{
			InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
			InvalidOperationException__ctor_m1_14170(L_5, /*hidden argument*/NULL);
			il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
		}

IL_0033:
		{
			goto IL_0038;
		}
	} // end catch (depth: 1)

IL_0038:
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		bool L_7 = ___overwrite;
		if (L_7)
		{
			goto IL_0050;
		}
	}
	{
		ArgumentException_t1_1425 * L_8 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_8, (String_t*)_stringLiteral4131, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0050:
	{
		KeyValuePair_2U5BU5D_t1_2680* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = ___key;
		int32_t L_12 = ___value;
		KeyValuePair_2_t1_2681  L_13 = {0};
		(( void (*) (KeyValuePair_2_t1_2681 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)(&L_13, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_9, L_10, sizeof(KeyValuePair_2_t1_2681 )))) = L_13;
		int32_t L_14 = (int32_t)(__this->___modificationCount_2);
		__this->___modificationCount_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
		return;
	}

IL_0072:
	{
		int32_t L_15 = V_1;
		V_1 = (int32_t)((~L_15));
		int32_t L_16 = V_1;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_17 = (( int32_t (*) (SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36)->method)((SortedList_2_t3_248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 36));
		if ((((int32_t)L_16) <= ((int32_t)((int32_t)((int32_t)L_17+(int32_t)1)))))
		{
			goto IL_00cf;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_18 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 7));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, _stringLiteral897);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral897;
		ObjectU5BU5D_t1_272* L_19 = (ObjectU5BU5D_t1_272*)L_18;
		int32_t L_20 = ___key;
		int32_t L_21 = L_20;
		Object_t * L_22 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_21);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_22);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 1, sizeof(Object_t *))) = (Object_t *)L_22;
		ObjectU5BU5D_t1_272* L_23 = (ObjectU5BU5D_t1_272*)L_19;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		ArrayElementTypeCheck (L_23, _stringLiteral167);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral167;
		ObjectU5BU5D_t1_272* L_24 = (ObjectU5BU5D_t1_272*)L_23;
		int32_t L_25 = ___value;
		int32_t L_26 = L_25;
		Object_t * L_27 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 3, sizeof(Object_t *))) = (Object_t *)L_27;
		ObjectU5BU5D_t1_272* L_28 = (ObjectU5BU5D_t1_272*)L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 4);
		ArrayElementTypeCheck (L_28, _stringLiteral898);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral898;
		ObjectU5BU5D_t1_272* L_29 = (ObjectU5BU5D_t1_272*)L_28;
		int32_t L_30 = V_1;
		int32_t L_31 = L_30;
		Object_t * L_32 = Box(Int32_t1_3_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 5);
		ArrayElementTypeCheck (L_29, L_32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_29, 5, sizeof(Object_t *))) = (Object_t *)L_32;
		ObjectU5BU5D_t1_272* L_33 = (ObjectU5BU5D_t1_272*)L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 6);
		ArrayElementTypeCheck (L_33, _stringLiteral266);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, 6, sizeof(Object_t *))) = (Object_t *)_stringLiteral266;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m1_562(NULL /*static, unused*/, (ObjectU5BU5D_t1_272*)L_33, /*hidden argument*/NULL);
		Exception_t1_33 * L_35 = (Exception_t1_33 *)il2cpp_codegen_object_new (Exception_t1_33_il2cpp_TypeInfo_var);
		Exception__ctor_m1_1238(L_35, (String_t*)L_34, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_35);
	}

IL_00cf:
	{
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		int32_t L_37 = V_1;
		NullCheck((SortedList_2_t3_248 *)__this);
		(( void (*) (SortedList_2_t3_248 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38)->method)((SortedList_2_t3_248 *)__this, (int32_t)((int32_t)((int32_t)L_36+(int32_t)1)), (int32_t)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 38));
		KeyValuePair_2U5BU5D_t1_2680* L_38 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		V_0 = (KeyValuePair_2U5BU5D_t1_2680*)L_38;
		KeyValuePair_2U5BU5D_t1_2680* L_39 = V_0;
		int32_t L_40 = V_1;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = ___key;
		int32_t L_42 = ___value;
		KeyValuePair_2_t1_2681  L_43 = {0};
		(( void (*) (KeyValuePair_2_t1_2681 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37)->method)(&L_43, (int32_t)L_41, (int32_t)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 37));
		(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_39, L_40, sizeof(KeyValuePair_2_t1_2681 )))) = L_43;
		int32_t L_44 = (int32_t)(__this->___inUse_1);
		__this->___inUse_1 = ((int32_t)((int32_t)L_44+(int32_t)1));
		int32_t L_45 = (int32_t)(__this->___modificationCount_2);
		__this->___modificationCount_2 = ((int32_t)((int32_t)L_45+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Init(System.Collections.Generic.IComparer`1<TKey>,System.Int32,System.Boolean)
extern "C" void SortedList_2_Init_m3_2160_gshared (SortedList_2_t3_248 * __this, Object_t* ___comparer, int32_t ___capacity, bool ___forceSize, const MethodInfo* method)
{
	{
		Object_t* L_0 = ___comparer;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 40));
		Comparer_1_t1_2243 * L_1 = (( Comparer_1_t1_2243 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 39));
		___comparer = (Object_t*)L_1;
	}

IL_000d:
	{
		Object_t* L_2 = ___comparer;
		__this->___comparer_4 = L_2;
		bool L_3 = ___forceSize;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = ___capacity;
		int32_t L_5 = (int32_t)(__this->___defaultCapacity_5);
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_6 = (int32_t)(__this->___defaultCapacity_5);
		___capacity = (int32_t)L_6;
	}

IL_002e:
	{
		int32_t L_7 = ___capacity;
		__this->___table_3 = ((KeyValuePair_2U5BU5D_t1_2680*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15), L_7));
		__this->___inUse_1 = 0;
		__this->___modificationCount_2 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::CopyToArray(System.Array,System.Int32,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t1_193_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral899;
extern Il2CppCodeGenString* _stringLiteral900;
extern "C" void SortedList_2_CopyToArray_m3_2161_gshared (SortedList_2_t3_248 * __this, Array_t * ___arr, int32_t ___i, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		IEnumerator_t1_193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		_stringLiteral899 = il2cpp_codegen_string_literal_from_index(899);
		_stringLiteral900 = il2cpp_codegen_string_literal_from_index(900);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Array_t * L_0 = ___arr;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral899, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___i;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = ___i;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		Array_t * L_5 = ___arr;
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_4))) <= ((int32_t)L_6)))
		{
			goto IL_0036;
		}
	}

IL_002b:
	{
		ArgumentOutOfRangeException_t1_1501 * L_7 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_7, (String_t*)_stringLiteral900, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}

IL_0036:
	{
		int32_t L_8 = ___mode;
		Enumerator_t3_286 * L_9 = (Enumerator_t3_286 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 30));
		(( void (*) (Enumerator_t3_286 *, SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(L_9, (SortedList_2_t3_248 *)__this, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		V_0 = (Object_t *)L_9;
		goto IL_0055;
	}

IL_0043:
	{
		Array_t * L_10 = ___arr;
		Object_t * L_11 = V_0;
		NullCheck((Object_t *)L_11);
		Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_11);
		int32_t L_13 = ___i;
		int32_t L_14 = (int32_t)L_13;
		___i = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		NullCheck((Array_t *)L_10);
		Array_SetValue_m1_1020((Array_t *)L_10, (Object_t *)L_12, (int32_t)L_14, /*hidden argument*/NULL);
	}

IL_0055:
	{
		Object_t * L_15 = V_0;
		NullCheck((Object_t *)L_15);
		bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1_193_il2cpp_TypeInfo_var, (Object_t *)L_15);
		if (L_16)
		{
			goto IL_0043;
		}
	}
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::Find(TKey)
extern "C" int32_t SortedList_2_Find_m3_2162_gshared (SortedList_2_t3_248 * __this, int32_t ___key, const MethodInfo* method)
{
	KeyValuePair_2U5BU5D_t1_2680* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		KeyValuePair_2U5BU5D_t1_2680* L_0 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		V_0 = (KeyValuePair_2U5BU5D_t1_2680*)L_0;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		V_1 = (int32_t)L_1;
		int32_t L_2 = V_1;
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (-1);
	}

IL_0016:
	{
		V_2 = (int32_t)0;
		int32_t L_3 = V_1;
		V_3 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		goto IL_0064;
	}

IL_0021:
	{
		int32_t L_4 = V_2;
		int32_t L_5 = V_3;
		V_4 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4+(int32_t)L_5))>>(int32_t)1));
		Object_t* L_6 = (Object_t*)(__this->___comparer_4);
		KeyValuePair_2U5BU5D_t1_2680* L_7 = V_0;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_7, L_8, sizeof(KeyValuePair_2_t1_2681 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		int32_t L_10 = ___key;
		NullCheck((Object_t*)L_6);
		int32_t L_11 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Int32>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 41), (Object_t*)L_6, (int32_t)L_9, (int32_t)L_10);
		V_5 = (int32_t)L_11;
		int32_t L_12 = V_5;
		if (L_12)
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_13 = V_4;
		return L_13;
	}

IL_004d:
	{
		int32_t L_14 = V_5;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_15 = V_4;
		V_2 = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		goto IL_0064;
	}

IL_005f:
	{
		int32_t L_16 = V_4;
		V_3 = (int32_t)((int32_t)((int32_t)L_16-(int32_t)1));
	}

IL_0064:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_3;
		if ((((int32_t)L_17) <= ((int32_t)L_18)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_19 = V_2;
		return ((~L_19));
	}
}
// TKey System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ToKey(System.Object)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral793;
extern Il2CppCodeGenString* _stringLiteral4132;
extern Il2CppCodeGenString* _stringLiteral4133;
extern Il2CppCodeGenString* _stringLiteral4134;
extern "C" int32_t SortedList_2_ToKey_m3_2163_gshared (SortedList_2_t3_248 * __this, Object_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral793 = il2cpp_codegen_string_literal_from_index(793);
		_stringLiteral4132 = il2cpp_codegen_string_literal_from_index(4132);
		_stringLiteral4133 = il2cpp_codegen_string_literal_from_index(4133);
		_stringLiteral4134 = il2cpp_codegen_string_literal_from_index(4134);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___key;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_1, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t * L_2 = ___key;
		if (((Object_t *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_005b;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_3 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral4132);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4132;
		ObjectU5BU5D_t1_272* L_4 = (ObjectU5BU5D_t1_272*)L_3;
		Object_t * L_5 = ___key;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t1_272* L_6 = (ObjectU5BU5D_t1_272*)L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral4133);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4133;
		ObjectU5BU5D_t1_272* L_7 = (ObjectU5BU5D_t1_272*)L_6;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 42)), /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3, sizeof(Object_t *))) = (Object_t *)L_8;
		ObjectU5BU5D_t1_272* L_9 = (ObjectU5BU5D_t1_272*)L_7;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, _stringLiteral4134);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4134;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1_562(NULL /*static, unused*/, (ObjectU5BU5D_t1_272*)L_9, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_11 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_11, (String_t*)L_10, (String_t*)_stringLiteral793, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_005b:
	{
		Object_t * L_12 = ___key;
		return ((*(int32_t*)((int32_t*)UnBox (L_12, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)))));
	}
}
// TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ToValue(System.Object)
extern TypeInfo* ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4132;
extern Il2CppCodeGenString* _stringLiteral4133;
extern Il2CppCodeGenString* _stringLiteral4134;
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" int32_t SortedList_2_ToValue_m3_2164_gshared (SortedList_2_t3_248 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral4132 = il2cpp_codegen_string_literal_from_index(4132);
		_stringLiteral4133 = il2cpp_codegen_string_literal_from_index(4133);
		_stringLiteral4134 = il2cpp_codegen_string_literal_from_index(4134);
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		if (((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5))))
		{
			goto IL_004a;
		}
	}
	{
		ObjectU5BU5D_t1_272* L_1 = (ObjectU5BU5D_t1_272*)((ObjectU5BU5D_t1_272*)SZArrayNew(ObjectU5BU5D_t1_272_il2cpp_TypeInfo_var, 5));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral4132);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral4132;
		ObjectU5BU5D_t1_272* L_2 = (ObjectU5BU5D_t1_272*)L_1;
		Object_t * L_3 = ___value;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t1_272* L_4 = (ObjectU5BU5D_t1_272*)L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral4133);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral4133;
		ObjectU5BU5D_t1_272* L_5 = (ObjectU5BU5D_t1_272*)L_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 43)), /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t1_272* L_7 = (ObjectU5BU5D_t1_272*)L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 4);
		ArrayElementTypeCheck (L_7, _stringLiteral4134);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral4134;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m1_562(NULL /*static, unused*/, (ObjectU5BU5D_t1_272*)L_7, /*hidden argument*/NULL);
		ArgumentException_t1_1425 * L_9 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13262(L_9, (String_t*)L_8, (String_t*)_stringLiteral61, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_004a:
	{
		Object_t * L_10 = ___value;
		return ((*(int32_t*)((int32_t*)UnBox (L_10, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)))));
	}
}
// TKey System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::KeyAt(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4135;
extern "C" int32_t SortedList_2_KeyAt_m3_2165_gshared (SortedList_2_t3_248 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral4135 = il2cpp_codegen_string_literal_from_index(4135);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_1 = ___index;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0025;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_3 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_4 = ___index;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_3, L_4, sizeof(KeyValuePair_2_t1_2681 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return L_5;
	}

IL_0025:
	{
		ArgumentOutOfRangeException_t1_1501 * L_6 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_6, (String_t*)_stringLiteral4135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}
}
// TValue System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::ValueAt(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4135;
extern "C" int32_t SortedList_2_ValueAt_m3_2166_gshared (SortedList_2_t3_248 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral4135 = il2cpp_codegen_string_literal_from_index(4135);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_1 = ___index;
		NullCheck((SortedList_2_t3_248 *)__this);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)__this);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0025;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_3 = (KeyValuePair_2U5BU5D_t1_2680*)(__this->___table_3);
		int32_t L_4 = ___index;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->method)((KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_3, L_4, sizeof(KeyValuePair_2_t1_2681 ))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		return L_5;
	}

IL_0025:
	{
		ArgumentOutOfRangeException_t1_1501 * L_6 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_6, (String_t*)_stringLiteral4135, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26580_gshared (InternalEnumerator_1_t1_2683 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26581_gshared (InternalEnumerator_1_t1_2683 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26582_gshared (InternalEnumerator_1_t1_2683 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2681  L_0 = (( KeyValuePair_2_t1_2681  (*) (InternalEnumerator_1_t1_2683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1_2681  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26583_gshared (InternalEnumerator_1_t1_2683 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26584_gshared (InternalEnumerator_1_t1_2683 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" KeyValuePair_2_t1_2681  InternalEnumerator_1_get_Current_m1_26585_gshared (InternalEnumerator_1_t1_2683 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		KeyValuePair_2_t1_2681  L_8 = (( KeyValuePair_2_t1_2681  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_26586_gshared (KeyValuePair_2_t1_2681 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key;
		(( void (*) (KeyValuePair_2_t1_2681 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t1_2681 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value;
		(( void (*) (KeyValuePair_2_t1_2681 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1_2681 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m1_26587_gshared (KeyValuePair_2_t1_2681 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___key_0);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_26588_gshared (KeyValuePair_2_t1_2681 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___key_0 = L_0;
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m1_26589_gshared (KeyValuePair_2_t1_2681 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___value_1);
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_26590_gshared (KeyValuePair_2_t1_2681 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___value_1 = L_0;
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::ToString()
extern TypeInfo* StringU5BU5D_t1_238_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral264;
extern Il2CppCodeGenString* _stringLiteral167;
extern Il2CppCodeGenString* _stringLiteral266;
extern "C" String_t* KeyValuePair_2_ToString_m1_26591_gshared (KeyValuePair_2_t1_2681 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t1_238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral264 = il2cpp_codegen_string_literal_from_index(264);
		_stringLiteral167 = il2cpp_codegen_string_literal_from_index(167);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1_238* G_B2_1 = {0};
	StringU5BU5D_t1_238* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1_238* G_B1_1 = {0};
	StringU5BU5D_t1_238* G_B1_2 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1_238* G_B3_2 = {0};
	StringU5BU5D_t1_238* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1_238* G_B5_1 = {0};
	StringU5BU5D_t1_238* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1_238* G_B4_1 = {0};
	StringU5BU5D_t1_238* G_B4_2 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1_238* G_B6_2 = {0};
	StringU5BU5D_t1_238* G_B6_3 = {0};
	{
		StringU5BU5D_t1_238* L_0 = (StringU5BU5D_t1_238*)((StringU5BU5D_t1_238*)SZArrayNew(StringU5BU5D_t1_238_il2cpp_TypeInfo_var, 5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral264);
		*((String_t**)(String_t**)SZArrayLdElema(L_0, 0, sizeof(String_t*))) = (String_t*)_stringLiteral264;
		StringU5BU5D_t1_238* L_1 = (StringU5BU5D_t1_238*)L_0;
		int32_t L_2 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2681 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2681 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		NullCheck((int32_t*)(&V_0));
		String_t* L_4 = Int32_ToString_m1_101((int32_t*)(&V_0), NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(String_t*))) = (String_t*)G_B3_0;
		StringU5BU5D_t1_238* L_6 = (StringU5BU5D_t1_238*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral167);
		*((String_t**)(String_t**)SZArrayLdElema(L_6, 2, sizeof(String_t*))) = (String_t*)_stringLiteral167;
		StringU5BU5D_t1_238* L_7 = (StringU5BU5D_t1_238*)L_6;
		int32_t L_8 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1_2681 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1_2681 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1)));
		String_t* L_10 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((String_t**)(String_t**)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(String_t*))) = (String_t*)G_B6_0;
		StringU5BU5D_t1_238* L_12 = (StringU5BU5D_t1_238*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral266);
		*((String_t**)(String_t**)SZArrayLdElema(L_12, 4, sizeof(String_t*))) = (String_t*)_stringLiteral266;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1_563(NULL /*static, unused*/, (StringU5BU5D_t1_238*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.IFD>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_26592_gshared (InternalEnumerator_1_t1_2684 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_2 = L_0;
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.IFD>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_26593_gshared (InternalEnumerator_1_t1_2684 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_26594_gshared (InternalEnumerator_1_t1_2684 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (InternalEnumerator_1_t1_2684 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1_2684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ExifLibrary.IFD>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_26595_gshared (InternalEnumerator_1_t1_2684 * __this, const MethodInfo* method)
{
	{
		__this->___idx_3 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ExifLibrary.IFD>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_26596_gshared (InternalEnumerator_1_t1_2684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m1_992((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_3 = L_2;
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)(__this->___idx_3);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_3);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_3 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ExifLibrary.IFD>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217;
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" int32_t InternalEnumerator_1_get_Current_m1_26597_gshared (InternalEnumerator_1_t1_2684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_1, (String_t*)_stringLiteral217, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)(__this->___idx_3);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral218, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_002f:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_2);
		Array_t * L_5 = (Array_t *)(__this->___array_2);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m1_992((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_3);
		NullCheck((Array_t *)L_4);
		int32_t L_8 = (( int32_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern "C" void ListKeys__ctor_m3_2167_gshared (ListKeys_t3_279 * __this, SortedList_2_t3_248 * ___host, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		SortedList_2_t3_248 * L_0 = ___host;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13268(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		SortedList_2_t3_248 * L_2 = ___host;
		__this->___host_0 = L_2;
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ListKeys_System_Collections_IEnumerable_GetEnumerator_m3_2168_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	GetEnumeratorU3Ec__Iterator2_t3_280 * V_0 = {0};
	{
		GetEnumeratorU3Ec__Iterator2_t3_280 * L_0 = (GetEnumeratorU3Ec__Iterator2_t3_280 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (GetEnumeratorU3Ec__Iterator2_t3_280 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (GetEnumeratorU3Ec__Iterator2_t3_280 *)L_0;
		GetEnumeratorU3Ec__Iterator2_t3_280 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		GetEnumeratorU3Ec__Iterator2_t3_280 * L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Add(TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListKeys_Add_m3_2169_gshared (ListKeys_t3_279 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Remove(TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" bool ListKeys_Remove_m3_2170_gshared (ListKeys_t3_279 * __this, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListKeys_Clear_m3_2171_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::CopyTo(TKey[],System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral173;
extern Il2CppCodeGenString* _stringLiteral892;
extern Il2CppCodeGenString* _stringLiteral893;
extern "C" void ListKeys_CopyTo_m3_2172_gshared (ListKeys_t3_279 * __this, Int32U5BU5D_t1_275* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		_stringLiteral892 = il2cpp_codegen_string_literal_from_index(892);
		_stringLiteral893 = il2cpp_codegen_string_literal_from_index(893);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Int32U5BU5D_t1_275* L_2 = ___array;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, (String_t*)_stringLiteral173, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___arrayIndex;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_5 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002f:
	{
		int32_t L_6 = ___arrayIndex;
		Int32U5BU5D_t1_275* L_7 = ___array;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))))
		{
			goto IL_0043;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_8 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_8, (String_t*)_stringLiteral892, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0043:
	{
		NullCheck((ListKeys_t3_279 *)__this);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_Count() */, (ListKeys_t3_279 *)__this);
		Int32U5BU5D_t1_275* L_10 = ___array;
		NullCheck(L_10);
		int32_t L_11 = ___arrayIndex;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))-(int32_t)L_11)))))
		{
			goto IL_005e;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_12 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_12, (String_t*)_stringLiteral893, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_005e:
	{
		int32_t L_13 = ___arrayIndex;
		V_0 = (int32_t)L_13;
		V_1 = (int32_t)0;
		goto IL_0082;
	}

IL_0067:
	{
		Int32U5BU5D_t1_275* L_14 = ___array;
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)L_15;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
		SortedList_2_t3_248 * L_17 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_18 = V_1;
		NullCheck((SortedList_2_t3_248 *)L_17);
		int32_t L_19 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((SortedList_2_t3_248 *)L_17, (int32_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))) = (int32_t)L_19;
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_21 = V_1;
		NullCheck((ListKeys_t3_279 *)__this);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_Count() */, (ListKeys_t3_279 *)__this);
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0067;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Contains(TKey)
extern "C" bool ListKeys_Contains_m3_2173_gshared (ListKeys_t3_279 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = ___item;
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_2 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return ((((int32_t)L_2) > ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::IndexOf(TKey)
extern "C" int32_t ListKeys_IndexOf_m3_2174_gshared (ListKeys_t3_279 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = ___item;
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_2 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::Insert(System.Int32,TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListKeys_Insert_m3_2175_gshared (ListKeys_t3_279 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::RemoveAt(System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListKeys_RemoveAt_m3_2176_gshared (ListKeys_t3_279 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// TKey System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_Item(System.Int32)
extern "C" int32_t ListKeys_get_Item_m3_2177_gshared (ListKeys_t3_279 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = ___index;
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_2 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::set_Item(System.Int32,TKey)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral903;
extern "C" void ListKeys_set_Item_m3_2178_gshared (ListKeys_t3_279 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral903 = il2cpp_codegen_string_literal_from_index(903);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral903, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::GetEnumerator()
extern "C" Object_t* ListKeys_GetEnumerator_m3_2179_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		KeyEnumerator_t3_281  L_1 = {0};
		(( void (*) (KeyEnumerator_t3_281 *, SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(&L_1, (SortedList_2_t3_248 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		KeyEnumerator_t3_281  L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_2);
		return (Object_t*)L_3;
	}
}
// System.Int32 System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_Count()
extern "C" int32_t ListKeys_get_Count_m3_2180_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_0);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_IsSynchronized()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" bool ListKeys_get_IsSynchronized_m3_2181_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((Object_t *)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_IsReadOnly()
extern "C" bool ListKeys_get_IsReadOnly_m3_2182_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::get_SyncRoot()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" Object_t * ListKeys_get_SyncRoot_m3_2183_gshared (ListKeys_t3_279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys<System.Int32,ExifLibrary.IFD>::CopyTo(System.Array,System.Int32)
extern "C" void ListKeys_CopyTo_m3_2184_gshared (ListKeys_t3_279 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___arrayIndex;
		NullCheck((SortedList_2_t3_248 *)L_0);
		(( void (*) (SortedList_2_t3_248 *, Array_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SortedList_2_t3_248 *)L_0, (Array_t *)L_1, (int32_t)L_2, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator2__ctor_m3_2185_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2186_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_2);
		return L_0;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3_2187_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator2_MoveNext_m3_2188_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_1);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005a;
		}
	}
	{
		goto IL_008a;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_0068;
	}

IL_002d:
	{
		ListKeys_t3_279 * L_2 = (ListKeys_t3_279 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		SortedList_2_t3_248 * L_3 = (SortedList_2_t3_248 *)(L_2->___host_0);
		int32_t L_4 = (int32_t)(__this->___U3CiU3E__0_0);
		NullCheck((SortedList_2_t3_248 *)L_3);
		int32_t L_5 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((SortedList_2_t3_248 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_6);
		__this->___U24current_2 = L_7;
		__this->___U24PC_1 = 1;
		goto IL_008c;
	}

IL_005a:
	{
		int32_t L_8 = (int32_t)(__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_9 = (int32_t)(__this->___U3CiU3E__0_0);
		ListKeys_t3_279 * L_10 = (ListKeys_t3_279 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		SortedList_2_t3_248 * L_11 = (SortedList_2_t3_248 *)(L_10->___host_0);
		NullCheck((SortedList_2_t3_248 *)L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_11);
		if ((((int32_t)L_9) < ((int32_t)L_12)))
		{
			goto IL_002d;
		}
	}
	{
		__this->___U24PC_1 = (-1);
	}

IL_008a:
	{
		return 0;
	}

IL_008c:
	{
		return 1;
	}
	// Dead block : IL_008e: ldloc.1
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator2_Dispose_m3_2189_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListKeys/<IEnumerable.GetEnumerator>c__Iterator2<System.Int32,ExifLibrary.IFD>::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void GetEnumeratorU3Ec__Iterator2_Reset_m3_2190_gshared (GetEnumeratorU3Ec__Iterator2_t3_280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void KeyEnumerator__ctor_m3_2191_gshared (KeyEnumerator_t3_281 * __this, SortedList_2_t3_248 * ___l, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = ___l;
		__this->___l_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		SortedList_2_t3_248 * L_1 = ___l;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		__this->___ver_2 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.Reset()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4136;
extern "C" void KeyEnumerator_System_Collections_IEnumerator_Reset_m3_2192_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral4136 = il2cpp_codegen_string_literal_from_index(4136);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___ver_2);
		SortedList_2_t3_248 * L_1 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral4136, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0021:
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_2193_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (KeyEnumerator_t3_281 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyEnumerator_t3_281 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void KeyEnumerator_Dispose_m3_2194_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::MoveNext()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4136;
extern "C" bool KeyEnumerator_MoveNext_m3_2195_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral4136 = il2cpp_codegen_string_literal_from_index(4136);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___ver_2);
		SortedList_2_t3_248 * L_1 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral4136, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0021:
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003f;
		}
	}
	{
		SortedList_2_t3_248 * L_5 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck((SortedList_2_t3_248 *)L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_5);
		__this->___idx_1 = L_6;
	}

IL_003f:
	{
		int32_t L_7 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_8 = (int32_t)(__this->___idx_1);
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->___idx_1 = L_9;
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 0;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// TKey System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" int32_t KeyEnumerator_get_Current_m3_2196_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14170(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		SortedList_2_t3_248 * L_2 = (SortedList_2_t3_248 *)(__this->___l_0);
		SortedList_2_t3_248 * L_3 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck((SortedList_2_t3_248 *)L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_3);
		int32_t L_5 = (int32_t)(__this->___idx_1);
		NullCheck((SortedList_2_t3_248 *)L_2);
		int32_t L_6 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((SortedList_2_t3_248 *)L_2, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)1))-(int32_t)L_5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_6;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern "C" void ListValues__ctor_m3_2197_gshared (ListValues_t3_282 * __this, SortedList_2_t3_248 * ___host, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		SortedList_2_t3_248 * L_0 = ___host;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_1 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13268(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		SortedList_2_t3_248 * L_2 = ___host;
		__this->___host_0 = L_2;
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ListValues_System_Collections_IEnumerable_GetEnumerator_m3_2198_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	GetEnumeratorU3Ec__Iterator3_t3_283 * V_0 = {0};
	{
		GetEnumeratorU3Ec__Iterator3_t3_283 * L_0 = (GetEnumeratorU3Ec__Iterator3_t3_283 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (GetEnumeratorU3Ec__Iterator3_t3_283 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (GetEnumeratorU3Ec__Iterator3_t3_283 *)L_0;
		GetEnumeratorU3Ec__Iterator3_t3_283 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		GetEnumeratorU3Ec__Iterator3_t3_283 * L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Add(TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListValues_Add_m3_2199_gshared (ListValues_t3_282 * __this, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Remove(TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" bool ListValues_Remove_m3_2200_gshared (ListValues_t3_282 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Clear()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListValues_Clear_m3_2201_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::CopyTo(TValue[],System.Int32)
extern TypeInfo* ArgumentNullException_t1_1500_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral173;
extern Il2CppCodeGenString* _stringLiteral892;
extern Il2CppCodeGenString* _stringLiteral893;
extern "C" void ListValues_CopyTo_m3_2202_gshared (ListValues_t3_282 * __this, IFDU5BU5D_t8_387* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1_1500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		_stringLiteral892 = il2cpp_codegen_string_literal_from_index(892);
		_stringLiteral893 = il2cpp_codegen_string_literal_from_index(893);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_0);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		IFDU5BU5D_t8_387* L_2 = ___array;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t1_1500 * L_3 = (ArgumentNullException_t1_1500 *)il2cpp_codegen_object_new (ArgumentNullException_t1_1500_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1_13269(L_3, (String_t*)_stringLiteral173, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___arrayIndex;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_5 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13273(L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002f:
	{
		int32_t L_6 = ___arrayIndex;
		IFDU5BU5D_t8_387* L_7 = ___array;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))))
		{
			goto IL_0043;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_8 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_8, (String_t*)_stringLiteral892, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}

IL_0043:
	{
		NullCheck((ListValues_t3_282 *)__this);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_Count() */, (ListValues_t3_282 *)__this);
		IFDU5BU5D_t8_387* L_10 = ___array;
		NullCheck(L_10);
		int32_t L_11 = ___arrayIndex;
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_10)->max_length))))-(int32_t)L_11)))))
		{
			goto IL_005e;
		}
	}
	{
		ArgumentOutOfRangeException_t1_1501 * L_12 = (ArgumentOutOfRangeException_t1_1501 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1_1501_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1_13274(L_12, (String_t*)_stringLiteral893, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_12);
	}

IL_005e:
	{
		int32_t L_13 = ___arrayIndex;
		V_0 = (int32_t)L_13;
		V_1 = (int32_t)0;
		goto IL_0082;
	}

IL_0067:
	{
		IFDU5BU5D_t8_387* L_14 = ___array;
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)L_15;
		V_0 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
		SortedList_2_t3_248 * L_17 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_18 = V_1;
		NullCheck((SortedList_2_t3_248 *)L_17);
		int32_t L_19 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((SortedList_2_t3_248 *)L_17, (int32_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16, sizeof(int32_t))) = (int32_t)L_19;
		int32_t L_20 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_21 = V_1;
		NullCheck((ListValues_t3_282 *)__this);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(33 /* System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_Count() */, (ListValues_t3_282 *)__this);
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0067;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Contains(TValue)
extern "C" bool ListValues_Contains_m3_2203_gshared (ListValues_t3_282 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = ___item;
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_2 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return ((((int32_t)L_2) > ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::IndexOf(TValue)
extern "C" int32_t ListValues_IndexOf_m3_2204_gshared (ListValues_t3_282 * __this, int32_t ___item, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = ___item;
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_2 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::Insert(System.Int32,TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListValues_Insert_m3_2205_gshared (ListValues_t3_282 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::RemoveAt(System.Int32)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void ListValues_RemoveAt_m3_2206_gshared (ListValues_t3_282 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// TValue System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_Item(System.Int32)
extern "C" int32_t ListValues_get_Item_m3_2207_gshared (ListValues_t3_282 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = ___index;
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_2 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::set_Item(System.Int32,TValue)
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral903;
extern "C" void ListValues_set_Item_m3_2208_gshared (ListValues_t3_282 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral903 = il2cpp_codegen_string_literal_from_index(903);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_0, (String_t*)_stringLiteral903, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::GetEnumerator()
extern "C" Object_t* ListValues_GetEnumerator_m3_2209_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		ValueEnumerator_t3_284  L_1 = {0};
		(( void (*) (ValueEnumerator_t3_284 *, SortedList_2_t3_248 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(&L_1, (SortedList_2_t3_248 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		ValueEnumerator_t3_284  L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_2);
		return (Object_t*)L_3;
	}
}
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_Count()
extern "C" int32_t ListValues_get_Count_m3_2210_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((SortedList_2_t3_248 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_0);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_IsSynchronized()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" bool ListValues_get_IsSynchronized_m3_2211_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((Object_t *)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_IsReadOnly()
extern "C" bool ListValues_get_IsReadOnly_m3_2212_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::get_SyncRoot()
extern TypeInfo* ICollection_t1_280_il2cpp_TypeInfo_var;
extern "C" Object_t * ListValues_get_SyncRoot_m3_2213_gshared (ListValues_t3_282 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1_280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1_280_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Int32,ExifLibrary.IFD>::CopyTo(System.Array,System.Int32)
extern "C" void ListValues_CopyTo_m3_2214_gshared (ListValues_t3_282 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___arrayIndex;
		NullCheck((SortedList_2_t3_248 *)L_0);
		(( void (*) (SortedList_2_t3_248 *, Array_t *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((SortedList_2_t3_248 *)L_0, (Array_t *)L_1, (int32_t)L_2, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator3__ctor_m3_2215_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3_2216_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_2);
		return L_0;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3_2217_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator3_MoveNext_m3_2218_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_1);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005a;
		}
	}
	{
		goto IL_008a;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_0068;
	}

IL_002d:
	{
		ListValues_t3_282 * L_2 = (ListValues_t3_282 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		SortedList_2_t3_248 * L_3 = (SortedList_2_t3_248 *)(L_2->___host_0);
		int32_t L_4 = (int32_t)(__this->___U3CiU3E__0_0);
		NullCheck((SortedList_2_t3_248 *)L_3);
		int32_t L_5 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((SortedList_2_t3_248 *)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_6);
		__this->___U24current_2 = L_7;
		__this->___U24PC_1 = 1;
		goto IL_008c;
	}

IL_005a:
	{
		int32_t L_8 = (int32_t)(__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_9 = (int32_t)(__this->___U3CiU3E__0_0);
		ListValues_t3_282 * L_10 = (ListValues_t3_282 *)(__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		SortedList_2_t3_248 * L_11 = (SortedList_2_t3_248 *)(L_10->___host_0);
		NullCheck((SortedList_2_t3_248 *)L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_11);
		if ((((int32_t)L_9) < ((int32_t)L_12)))
		{
			goto IL_002d;
		}
	}
	{
		__this->___U24PC_1 = (-1);
	}

IL_008a:
	{
		return 0;
	}

IL_008c:
	{
		return 1;
	}
	// Dead block : IL_008e: ldloc.1
}
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator3_Dispose_m3_2219_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ListValues/<IEnumerable.GetEnumerator>c__Iterator3<System.Int32,ExifLibrary.IFD>::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void GetEnumeratorU3Ec__Iterator3_Reset_m3_2220_gshared (GetEnumeratorU3Ec__Iterator3_t3_283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void ValueEnumerator__ctor_m3_2221_gshared (ValueEnumerator_t3_284 * __this, SortedList_2_t3_248 * ___l, const MethodInfo* method)
{
	{
		SortedList_2_t3_248 * L_0 = ___l;
		__this->___l_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		SortedList_2_t3_248 * L_1 = ___l;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		__this->___ver_2 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.Reset()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4136;
extern "C" void ValueEnumerator_System_Collections_IEnumerator_Reset_m3_2222_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral4136 = il2cpp_codegen_string_literal_from_index(4136);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___ver_2);
		SortedList_2_t3_248 * L_1 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral4136, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0021:
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * ValueEnumerator_System_Collections_IEnumerator_get_Current_m3_2223_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (( int32_t (*) (ValueEnumerator_t3_284 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ValueEnumerator_t3_284 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void ValueEnumerator_Dispose_m3_2224_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::MoveNext()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4136;
extern "C" bool ValueEnumerator_MoveNext_m3_2225_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		_stringLiteral4136 = il2cpp_codegen_string_literal_from_index(4136);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___ver_2);
		SortedList_2_t3_248 * L_1 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_3 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_3, (String_t*)_stringLiteral4136, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0021:
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003f;
		}
	}
	{
		SortedList_2_t3_248 * L_5 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck((SortedList_2_t3_248 *)L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_5);
		__this->___idx_1 = L_6;
	}

IL_003f:
	{
		int32_t L_7 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_8 = (int32_t)(__this->___idx_1);
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->___idx_1 = L_9;
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 0;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// TValue System.Collections.Generic.SortedList`2/ValueEnumerator<System.Int32,ExifLibrary.IFD>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" int32_t ValueEnumerator_get_Current_m3_2226_gshared (ValueEnumerator_t3_284 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t1_1559 * L_1 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14170(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0012:
	{
		SortedList_2_t3_248 * L_2 = (SortedList_2_t3_248 *)(__this->___l_0);
		SortedList_2_t3_248 * L_3 = (SortedList_2_t3_248 *)(__this->___l_0);
		NullCheck((SortedList_2_t3_248 *)L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_3);
		int32_t L_5 = (int32_t)(__this->___idx_1);
		NullCheck((SortedList_2_t3_248 *)L_2);
		int32_t L_6 = (( int32_t (*) (SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((SortedList_2_t3_248 *)L_2, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)1))-(int32_t)L_5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_6;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::.ctor()
extern "C" void Comparer_1__ctor_m1_26598_gshared (Comparer_1_t1_2685 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::.cctor()
extern const Il2CppType* GenericComparer_1_t1_3063_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t1_31_il2cpp_TypeInfo_var;
extern "C" void Comparer_1__cctor_m1_26599_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericComparer_1_t1_3063_0_0_0_var = il2cpp_codegen_type_from_index(4653);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		TypeU5BU5D_t1_31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(63);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(169 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GenericComparer_1_t1_3063_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1_31* L_4 = (TypeU5BU5D_t1_31*)((TypeU5BU5D_t1_31*)SZArrayNew(TypeU5BU5D_t1_31_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0, sizeof(Type_t *))) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1_31* >::Invoke(210 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1_31*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m1_13034(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1_2685_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((Comparer_1_t1_2685 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t1_2686 * L_8 = (DefaultComparer_t1_2686 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t1_2686 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1_2685_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m1_26600_gshared (Comparer_1_t1_2685 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Object_t * L_0 = ___x;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Object_t * L_1 = ___y;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Object_t * L_2 = ___y;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Object_t * L_3 = ___x;
		if (!((Object_t *)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Object_t * L_4 = ___y;
		if (!((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Object_t * L_5 = ___x;
		Object_t * L_6 = ___y;
		NullCheck((Comparer_1_t1_2685 *)__this);
		int32_t L_7 = (int32_t)VirtFuncInvoker2< int32_t, KeyValuePair_2_t1_2681 , KeyValuePair_2_t1_2681  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::Compare(T,T) */, (Comparer_1_t1_2685 *)__this, (KeyValuePair_2_t1_2681 )((*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)UnBox (L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (KeyValuePair_2_t1_2681 )((*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)UnBox (L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t1_1425 * L_8 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13259(L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::get_Default()
extern "C" Comparer_1_t1_2685 * Comparer_1_get_Default_m1_26601_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1_2685 * L_0 = ((Comparer_1_t1_2685_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::.ctor()
extern "C" void DefaultComparer__ctor_m1_26602_gshared (DefaultComparer_t1_2686 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1_2685 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		(( void (*) (Comparer_1_t1_2685 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Comparer_1_t1_2685 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::Compare(T,T)
extern TypeInfo* IComparable_t1_1745_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t1_1425_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral791;
extern "C" int32_t DefaultComparer_Compare_m1_26603_gshared (DefaultComparer_t1_2686 * __this, KeyValuePair_2_t1_2681  ___x, KeyValuePair_2_t1_2681  ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t1_1745_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		ArgumentException_t1_1425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral791 = il2cpp_codegen_string_literal_from_index(791);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		KeyValuePair_2_t1_2681  L_0 = ___x;
		goto IL_001e;
	}
	{
		KeyValuePair_2_t1_2681  L_1 = ___y;
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		KeyValuePair_2_t1_2681  L_2 = ___y;
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		KeyValuePair_2_t1_2681  L_3 = ___x;
		KeyValuePair_2_t1_2681  L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_4);
		if (!((Object_t*)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		KeyValuePair_2_t1_2681  L_6 = ___x;
		KeyValuePair_2_t1_2681  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_7);
		KeyValuePair_2_t1_2681  L_9 = ___y;
		NullCheck((Object_t*)((Object_t*)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, KeyValuePair_2_t1_2681  >::Invoke(0 /* System.Int32 System.IComparable`1<System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>>::CompareTo(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)((Object_t*)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (KeyValuePair_2_t1_2681 )L_9);
		return L_10;
	}

IL_004d:
	{
		KeyValuePair_2_t1_2681  L_11 = ___x;
		KeyValuePair_2_t1_2681  L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		if (!((Object_t *)IsInst(L_13, IComparable_t1_1745_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		KeyValuePair_2_t1_2681  L_14 = ___x;
		KeyValuePair_2_t1_2681  L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_15);
		KeyValuePair_2_t1_2681  L_17 = ___y;
		KeyValuePair_2_t1_2681  L_18 = L_17;
		Object_t * L_19 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_18);
		NullCheck((Object_t *)((Object_t *)Castclass(L_16, IComparable_t1_1745_il2cpp_TypeInfo_var)));
		int32_t L_20 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1_1745_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_16, IComparable_t1_1745_il2cpp_TypeInfo_var)), (Object_t *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t1_1425 * L_21 = (ArgumentException_t1_1425 *)il2cpp_codegen_object_new (ArgumentException_t1_1425_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1_13260(L_21, (String_t*)_stringLiteral791, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_21);
	}
}
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator0__ctor_m3_2227_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C" KeyValuePair_2_t1_2681  GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2228_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2681  L_0 = (KeyValuePair_2_t1_2681 )(__this->___U24current_3);
		return L_0;
	}
}
// System.Object System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_2229_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2681  L_0 = (KeyValuePair_2_t1_2681 )(__this->___U24current_3);
		KeyValuePair_2_t1_2681  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator0_MoveNext_m3_2230_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_2);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00a6;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_0089;
	}

IL_002d:
	{
		SortedList_2_t3_248 * L_2 = (SortedList_2_t3_248 *)(__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		KeyValuePair_2U5BU5D_t1_2680* L_3 = (KeyValuePair_2U5BU5D_t1_2680*)(L_2->___table_3);
		int32_t L_4 = (int32_t)(__this->___U3CiU3E__0_0);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		__this->___U3CcurrentU3E__1_1 = (*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_3, L_4, sizeof(KeyValuePair_2_t1_2681 ))));
		KeyValuePair_2_t1_2681 * L_5 = (KeyValuePair_2_t1_2681 *)&(__this->___U3CcurrentU3E__1_1);
		int32_t L_6 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1_2681 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t1_2681 * L_7 = (KeyValuePair_2_t1_2681 *)&(__this->___U3CcurrentU3E__1_1);
		int32_t L_8 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2681 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		KeyValuePair_2_t1_2681  L_9 = {0};
		(( void (*) (KeyValuePair_2_t1_2681 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_9, (int32_t)L_6, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->___U24current_3 = L_9;
		__this->___U24PC_2 = 1;
		goto IL_00a8;
	}

IL_007b:
	{
		int32_t L_10 = (int32_t)(__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0089:
	{
		int32_t L_11 = (int32_t)(__this->___U3CiU3E__0_0);
		SortedList_2_t3_248 * L_12 = (SortedList_2_t3_248 *)(__this->___U3CU3Ef__this_4);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)(L_12->___inUse_1);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_002d;
		}
	}
	{
		__this->___U24PC_2 = (-1);
	}

IL_00a6:
	{
		return 0;
	}

IL_00a8:
	{
		return 1;
	}
	// Dead block : IL_00aa: ldloc.1
}
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator0_Dispose_m3_2231_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void GetEnumeratorU3Ec__Iterator0_Reset_m3_2232_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>,System.Collections.Generic.SortedList`2/EnumeratorMode<TKey,TValue>)
extern "C" void Enumerator__ctor_m3_2233_gshared (Enumerator_t3_286 * __this, SortedList_2_t3_248 * ___host, int32_t ___mode, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		SortedList_2_t3_248 * L_0 = ___host;
		__this->___host_0 = L_0;
		SortedList_2_t3_248 * L_1 = ___host;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___modificationCount_2);
		__this->___stamp_1 = L_2;
		SortedList_2_t3_248 * L_3 = ___host;
		NullCheck((SortedList_2_t3_248 *)L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>::get_Count() */, (SortedList_2_t3_248 *)L_3);
		__this->___size_3 = L_4;
		int32_t L_5 = ___mode;
		__this->___mode_4 = L_5;
		NullCheck((Enumerator_t3_286 *)__this);
		(( void (*) (Enumerator_t3_286 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t3_286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::.cctor()
extern Il2CppCodeGenString* _stringLiteral901;
extern "C" void Enumerator__cctor_m3_2234_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral901 = il2cpp_codegen_string_literal_from_index(901);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8 = _stringLiteral901;
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::Reset()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" void Enumerator_Reset_m3_2235_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)(L_0->___modificationCount_2);
		int32_t L_2 = (int32_t)(__this->___stamp_1);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		bool L_3 = (bool)(__this->___invalid_7);
		if (!L_3)
		{
			goto IL_002c;
		}
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		String_t* L_4 = ((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8;
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002c:
	{
		__this->___pos_2 = (-1);
		__this->___currentKey_5 = NULL;
		__this->___currentValue_6 = NULL;
		return;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::MoveNext()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" bool Enumerator_MoveNext_m3_2236_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2U5BU5D_t1_2680* V_0 = {0};
	KeyValuePair_2_t1_2681  V_1 = {0};
	int32_t V_2 = 0;
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)(L_0->___modificationCount_2);
		int32_t L_2 = (int32_t)(__this->___stamp_1);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		bool L_3 = (bool)(__this->___invalid_7);
		if (!L_3)
		{
			goto IL_002c;
		}
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		String_t* L_4 = ((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8;
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_002c:
	{
		SortedList_2_t3_248 * L_6 = (SortedList_2_t3_248 *)(__this->___host_0);
		NullCheck(L_6);
		KeyValuePair_2U5BU5D_t1_2680* L_7 = (KeyValuePair_2U5BU5D_t1_2680*)(L_6->___table_3);
		V_0 = (KeyValuePair_2U5BU5D_t1_2680*)L_7;
		int32_t L_8 = (int32_t)(__this->___pos_2);
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_2 = (int32_t)L_9;
		__this->___pos_2 = L_9;
		int32_t L_10 = V_2;
		int32_t L_11 = (int32_t)(__this->___size_3);
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_008c;
		}
	}
	{
		KeyValuePair_2U5BU5D_t1_2680* L_12 = V_0;
		int32_t L_13 = (int32_t)(__this->___pos_2);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		V_1 = (KeyValuePair_2_t1_2681 )(*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_12, L_13, sizeof(KeyValuePair_2_t1_2681 ))));
		int32_t L_14 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1_2681 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_15);
		__this->___currentKey_5 = L_16;
		int32_t L_17 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1_2681 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_18);
		__this->___currentValue_6 = L_19;
		return 1;
	}

IL_008c:
	{
		__this->___currentKey_5 = NULL;
		__this->___currentValue_6 = NULL;
		return 0;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Entry()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t1_284  Enumerator_get_Entry_m3_2237_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)(__this->___invalid_7);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->___pos_2);
		int32_t L_2 = (int32_t)(__this->___size_3);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = (int32_t)(__this->___pos_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		String_t* L_4 = ((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8;
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		Object_t * L_6 = (Object_t *)(__this->___currentKey_5);
		Object_t * L_7 = (Object_t *)(__this->___currentValue_6);
		DictionaryEntry_t1_284  L_8 = {0};
		DictionaryEntry__ctor_m1_3228(&L_8, (Object_t *)L_6, (Object_t *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Key()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_get_Key_m3_2238_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)(__this->___invalid_7);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->___pos_2);
		int32_t L_2 = (int32_t)(__this->___size_3);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = (int32_t)(__this->___pos_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		String_t* L_4 = ((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8;
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		Object_t * L_6 = (Object_t *)(__this->___currentKey_5);
		return L_6;
	}
}
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Value()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_get_Value_m3_2239_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)(__this->___invalid_7);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->___pos_2);
		int32_t L_2 = (int32_t)(__this->___size_3);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = (int32_t)(__this->___pos_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		String_t* L_4 = ((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8;
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		Object_t * L_6 = (Object_t *)(__this->___currentValue_6);
		return L_6;
	}
}
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::get_Current()
extern TypeInfo* InvalidOperationException_t1_1559_il2cpp_TypeInfo_var;
extern TypeInfo* DictionaryEntry_t1_284_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral902;
extern "C" Object_t * Enumerator_get_Current_m3_2240_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1_1559_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		DictionaryEntry_t1_284_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		_stringLiteral902 = il2cpp_codegen_string_literal_from_index(902);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		bool L_0 = (bool)(__this->___invalid_7);
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_1 = (int32_t)(__this->___pos_2);
		int32_t L_2 = (int32_t)(__this->___size_3);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = (int32_t)(__this->___pos_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0033;
		}
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		String_t* L_4 = ((Enumerator_t3_286_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->___xstr_8;
		InvalidOperationException_t1_1559 * L_5 = (InvalidOperationException_t1_1559 *)il2cpp_codegen_object_new (InvalidOperationException_t1_1559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1_14171(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0033:
	{
		int32_t L_6 = (int32_t)(__this->___mode_4);
		V_0 = (int32_t)L_6;
		int32_t L_7 = V_0;
		if (L_7 == 0)
		{
			goto IL_0051;
		}
		if (L_7 == 1)
		{
			goto IL_0058;
		}
		if (L_7 == 2)
		{
			goto IL_005f;
		}
	}
	{
		goto IL_006b;
	}

IL_0051:
	{
		Object_t * L_8 = (Object_t *)(__this->___currentKey_5);
		return L_8;
	}

IL_0058:
	{
		Object_t * L_9 = (Object_t *)(__this->___currentValue_6);
		return L_9;
	}

IL_005f:
	{
		NullCheck((Enumerator_t3_286 *)__this);
		DictionaryEntry_t1_284  L_10 = (( DictionaryEntry_t1_284  (*) (Enumerator_t3_286 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t3_286 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		DictionaryEntry_t1_284  L_11 = L_10;
		Object_t * L_12 = Box(DictionaryEntry_t1_284_il2cpp_TypeInfo_var, &L_11);
		return L_12;
	}

IL_006b:
	{
		int32_t L_13 = (int32_t)(__this->___mode_4);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1_556(NULL /*static, unused*/, (Object_t *)L_15, (Object_t *)_stringLiteral902, /*hidden argument*/NULL);
		NotSupportedException_t1_1583 * L_17 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14417(L_17, (String_t*)L_16, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_17);
	}
}
// System.Object System.Collections.Generic.SortedList`2/Enumerator<System.Int32,ExifLibrary.IFD>::Clone()
extern "C" Object_t * Enumerator_Clone_m3_2241_gshared (Enumerator_t3_286 * __this, const MethodInfo* method)
{
	Enumerator_t3_286 * V_0 = {0};
	{
		SortedList_2_t3_248 * L_0 = (SortedList_2_t3_248 *)(__this->___host_0);
		int32_t L_1 = (int32_t)(__this->___mode_4);
		Enumerator_t3_286 * L_2 = (Enumerator_t3_286 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		(( void (*) (Enumerator_t3_286 *, SortedList_2_t3_248 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(L_2, (SortedList_2_t3_248 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		V_0 = (Enumerator_t3_286 *)L_2;
		Enumerator_t3_286 * L_3 = V_0;
		int32_t L_4 = (int32_t)(__this->___stamp_1);
		NullCheck(L_3);
		L_3->___stamp_1 = L_4;
		Enumerator_t3_286 * L_5 = V_0;
		int32_t L_6 = (int32_t)(__this->___pos_2);
		NullCheck(L_5);
		L_5->___pos_2 = L_6;
		Enumerator_t3_286 * L_7 = V_0;
		int32_t L_8 = (int32_t)(__this->___size_3);
		NullCheck(L_7);
		L_7->___size_3 = L_8;
		Enumerator_t3_286 * L_9 = V_0;
		Object_t * L_10 = (Object_t *)(__this->___currentKey_5);
		NullCheck(L_9);
		L_9->___currentKey_5 = L_10;
		Enumerator_t3_286 * L_11 = V_0;
		Object_t * L_12 = (Object_t *)(__this->___currentValue_6);
		NullCheck(L_11);
		L_11->___currentValue_6 = L_12;
		Enumerator_t3_286 * L_13 = V_0;
		bool L_14 = (bool)(__this->___invalid_7);
		NullCheck(L_13);
		L_13->___invalid_7 = L_14;
		Enumerator_t3_286 * L_15 = V_0;
		return L_15;
	}
}
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_2242_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m1_0((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C" KeyValuePair_2_t1_2681  U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2243_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2681  L_0 = (KeyValuePair_2_t1_2681 )(__this->___U24current_3);
		return L_0;
	}
}
// System.Object System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_2244_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1_2681  L_0 = (KeyValuePair_2_t1_2681 )(__this->___U24current_3);
		KeyValuePair_2_t1_2681  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_2245_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)(__this->___U24PC_2);
		V_0 = (uint32_t)L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00a6;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_0089;
	}

IL_002d:
	{
		SortedList_2_t3_248 * L_2 = (SortedList_2_t3_248 *)(__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		KeyValuePair_2U5BU5D_t1_2680* L_3 = (KeyValuePair_2U5BU5D_t1_2680*)(L_2->___table_3);
		int32_t L_4 = (int32_t)(__this->___U3CiU3E__0_0);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		__this->___U3CcurrentU3E__1_1 = (*(KeyValuePair_2_t1_2681 *)((KeyValuePair_2_t1_2681 *)(KeyValuePair_2_t1_2681 *)SZArrayLdElema(L_3, L_4, sizeof(KeyValuePair_2_t1_2681 ))));
		KeyValuePair_2_t1_2681 * L_5 = (KeyValuePair_2_t1_2681 *)&(__this->___U3CcurrentU3E__1_1);
		int32_t L_6 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1_2681 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		KeyValuePair_2_t1_2681 * L_7 = (KeyValuePair_2_t1_2681 *)&(__this->___U3CcurrentU3E__1_1);
		int32_t L_8 = (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1_2681 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		KeyValuePair_2_t1_2681  L_9 = {0};
		(( void (*) (KeyValuePair_2_t1_2681 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(&L_9, (int32_t)L_6, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		__this->___U24current_3 = L_9;
		__this->___U24PC_2 = 1;
		goto IL_00a8;
	}

IL_007b:
	{
		int32_t L_10 = (int32_t)(__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0089:
	{
		int32_t L_11 = (int32_t)(__this->___U3CiU3E__0_0);
		SortedList_2_t3_248 * L_12 = (SortedList_2_t3_248 *)(__this->___U3CU3Ef__this_4);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)(L_12->___inUse_1);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_002d;
		}
	}
	{
		__this->___U24PC_2 = (-1);
	}

IL_00a6:
	{
		return 0;
	}

IL_00a8:
	{
		return 1;
	}
	// Dead block : IL_00aa: ldloc.1
}
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_2246_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Int32,ExifLibrary.IFD>::Reset()
extern TypeInfo* NotSupportedException_t1_1583_il2cpp_TypeInfo_var;
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_2247_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_288 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t1_1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1_1583 * L_0 = (NotSupportedException_t1_1583 *)il2cpp_codegen_object_new (NotSupportedException_t1_1583_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m1_14416(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2073_gshared (ExifEnumProperty_1_t8_338 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1974_gshared (ExifEnumProperty_1_t8_338 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_338 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_338 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_338 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2074_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_338 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_338 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_338 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2075_gshared (ExifEnumProperty_1_t8_338 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_338 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_338 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_338 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2076_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2077_gshared (ExifEnumProperty_1_t8_338 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2078_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2079_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2080_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2081_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_338 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_338 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2082_gshared (ExifEnumProperty_1_t8_339 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1975_gshared (ExifEnumProperty_1_t8_339 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_339 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_339 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_339 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2083_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_339 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_339 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_339 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2084_gshared (ExifEnumProperty_1_t8_339 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_339 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_339 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_339 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2085_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2086_gshared (ExifEnumProperty_1_t8_339 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2087_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2088_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2089_gshared (ExifEnumProperty_1_t8_339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.PhotometricInterpretation>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2090_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_339 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_339 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2091_gshared (ExifEnumProperty_1_t8_340 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1976_gshared (ExifEnumProperty_1_t8_340 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_340 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_340 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_340 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2092_gshared (ExifEnumProperty_1_t8_340 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_340 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_340 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_340 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2093_gshared (ExifEnumProperty_1_t8_340 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_340 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_340 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_340 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2094_gshared (ExifEnumProperty_1_t8_340 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2095_gshared (ExifEnumProperty_1_t8_340 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2096_gshared (ExifEnumProperty_1_t8_340 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2097_gshared (ExifEnumProperty_1_t8_340 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2098_gshared (ExifEnumProperty_1_t8_340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Orientation>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2099_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_340 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_340 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2100_gshared (ExifEnumProperty_1_t8_341 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1977_gshared (ExifEnumProperty_1_t8_341 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_341 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_341 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_341 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2101_gshared (ExifEnumProperty_1_t8_341 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_341 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_341 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_341 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2102_gshared (ExifEnumProperty_1_t8_341 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_341 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_341 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_341 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2103_gshared (ExifEnumProperty_1_t8_341 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2104_gshared (ExifEnumProperty_1_t8_341 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2105_gshared (ExifEnumProperty_1_t8_341 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2106_gshared (ExifEnumProperty_1_t8_341 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2107_gshared (ExifEnumProperty_1_t8_341 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.PlanarConfiguration>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2108_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_341 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_341 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2109_gshared (ExifEnumProperty_1_t8_342 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1978_gshared (ExifEnumProperty_1_t8_342 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_342 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_342 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_342 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2110_gshared (ExifEnumProperty_1_t8_342 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_342 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_342 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_342 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2111_gshared (ExifEnumProperty_1_t8_342 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_342 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_342 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_342 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2112_gshared (ExifEnumProperty_1_t8_342 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2113_gshared (ExifEnumProperty_1_t8_342 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2114_gshared (ExifEnumProperty_1_t8_342 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2115_gshared (ExifEnumProperty_1_t8_342 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2116_gshared (ExifEnumProperty_1_t8_342 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.YCbCrPositioning>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2117_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_342 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_342 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1985_gshared (ExifEnumProperty_1_t8_343 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1979_gshared (ExifEnumProperty_1_t8_343 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_343 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_343 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_343 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2118_gshared (ExifEnumProperty_1_t8_343 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_343 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_343 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_343 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2119_gshared (ExifEnumProperty_1_t8_343 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_343 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_343 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_343 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2120_gshared (ExifEnumProperty_1_t8_343 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2121_gshared (ExifEnumProperty_1_t8_343 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2122_gshared (ExifEnumProperty_1_t8_343 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2123_gshared (ExifEnumProperty_1_t8_343 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2124_gshared (ExifEnumProperty_1_t8_343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ResolutionUnit>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2125_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_343 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_343 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2126_gshared (ExifEnumProperty_1_t8_344 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1980_gshared (ExifEnumProperty_1_t8_344 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_344 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_344 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_344 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2127_gshared (ExifEnumProperty_1_t8_344 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_344 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_344 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_344 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2128_gshared (ExifEnumProperty_1_t8_344 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_344 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_344 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_344 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2129_gshared (ExifEnumProperty_1_t8_344 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2130_gshared (ExifEnumProperty_1_t8_344 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2131_gshared (ExifEnumProperty_1_t8_344 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2132_gshared (ExifEnumProperty_1_t8_344 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2133_gshared (ExifEnumProperty_1_t8_344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ColorSpace>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2134_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_344 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_344 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2135_gshared (ExifEnumProperty_1_t8_345 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1981_gshared (ExifEnumProperty_1_t8_345 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_345 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_345 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_345 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2136_gshared (ExifEnumProperty_1_t8_345 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_345 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_345 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_345 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2137_gshared (ExifEnumProperty_1_t8_345 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_345 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_345 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_345 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2138_gshared (ExifEnumProperty_1_t8_345 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2139_gshared (ExifEnumProperty_1_t8_345 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2140_gshared (ExifEnumProperty_1_t8_345 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2141_gshared (ExifEnumProperty_1_t8_345 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2142_gshared (ExifEnumProperty_1_t8_345 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureProgram>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2143_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_345 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_345 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2144_gshared (ExifEnumProperty_1_t8_346 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1982_gshared (ExifEnumProperty_1_t8_346 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_346 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_346 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_346 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2145_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_346 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2146_gshared (ExifEnumProperty_1_t8_346 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_346 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_346 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_346 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2147_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2148_gshared (ExifEnumProperty_1_t8_346 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2149_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2150_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2151_gshared (ExifEnumProperty_1_t8_346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.MeteringMode>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2152_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_346 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_346 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2153_gshared (ExifEnumProperty_1_t8_347 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1983_gshared (ExifEnumProperty_1_t8_347 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_347 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_347 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_347 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2154_gshared (ExifEnumProperty_1_t8_347 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_347 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2155_gshared (ExifEnumProperty_1_t8_347 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_347 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_347 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_347 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2156_gshared (ExifEnumProperty_1_t8_347 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2157_gshared (ExifEnumProperty_1_t8_347 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2158_gshared (ExifEnumProperty_1_t8_347 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2159_gshared (ExifEnumProperty_1_t8_347 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2160_gshared (ExifEnumProperty_1_t8_347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.LightSource>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2161_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_347 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_347 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1984_gshared (ExifEnumProperty_1_t8_348 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2162_gshared (ExifEnumProperty_1_t8_348 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_348 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_348 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_348 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2163_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_348 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_348 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_348 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2164_gshared (ExifEnumProperty_1_t8_348 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_348 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_348 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_348 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2165_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2166_gshared (ExifEnumProperty_1_t8_348 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2167_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2168_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2169_gshared (ExifEnumProperty_1_t8_348 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Flash>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2170_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_348 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_348 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1986_gshared (ExifEnumProperty_1_t8_349 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2171_gshared (ExifEnumProperty_1_t8_349 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_349 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_349 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_349 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2172_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_349 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_349 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_349 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2173_gshared (ExifEnumProperty_1_t8_349 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_349 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_349 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_349 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2174_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2175_gshared (ExifEnumProperty_1_t8_349 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2176_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2177_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2178_gshared (ExifEnumProperty_1_t8_349 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SensingMethod>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2179_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_349 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_349 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1987_gshared (ExifEnumProperty_1_t8_350 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2180_gshared (ExifEnumProperty_1_t8_350 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_350 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_350 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_350 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2181_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_350 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_350 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2182_gshared (ExifEnumProperty_1_t8_350 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_350 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_350 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_350 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2183_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2184_gshared (ExifEnumProperty_1_t8_350 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2185_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2186_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2187_gshared (ExifEnumProperty_1_t8_350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.FileSource>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2188_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_350 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_350 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1988_gshared (ExifEnumProperty_1_t8_351 * __this, int32_t ___tag, uint8_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint8_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2189_gshared (ExifEnumProperty_1_t8_351 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint8_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_351 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_351 *, int32_t, uint8_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_351 *)__this, (int32_t)L_0, (uint8_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2190_gshared (ExifEnumProperty_1_t8_351 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_351 *)__this);
		uint8_t L_0 = (( uint8_t (*) (ExifEnumProperty_1_t8_351 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_351 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2191_gshared (ExifEnumProperty_1_t8_351 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_351 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_351 *, uint8_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_351 *)__this, (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::get_Value()
extern "C" uint8_t ExifEnumProperty_1_get_Value_m8_2192_gshared (ExifEnumProperty_1_t8_351 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = (uint8_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2193_gshared (ExifEnumProperty_1_t8_351 * __this, uint8_t ___value, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2194_gshared (ExifEnumProperty_1_t8_351 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2195_gshared (ExifEnumProperty_1_t8_351 * __this, const MethodInfo* method)
{
	{
		uint8_t* L_0 = (uint8_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2196_gshared (ExifEnumProperty_1_t8_351 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_11 = (uint8_t)(__this->___mValue_3);
		uint8_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint8_t L_31 = (uint8_t)(__this->___mValue_3);
		uint8_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint8_t L_39 = (uint8_t)(__this->___mValue_3);
		uint8_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint8_t L_46 = (uint8_t)(__this->___mValue_3);
		uint8_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.SceneType>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint8_t ExifEnumProperty_1_op_Implicit_m8_2197_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_351 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_351 * L_0 = ___obj;
		NullCheck(L_0);
		uint8_t L_1 = (uint8_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1989_gshared (ExifEnumProperty_1_t8_352 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2198_gshared (ExifEnumProperty_1_t8_352 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_352 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_352 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_352 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2199_gshared (ExifEnumProperty_1_t8_352 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_352 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_352 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_352 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2200_gshared (ExifEnumProperty_1_t8_352 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_352 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_352 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_352 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2201_gshared (ExifEnumProperty_1_t8_352 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2202_gshared (ExifEnumProperty_1_t8_352 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2203_gshared (ExifEnumProperty_1_t8_352 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2204_gshared (ExifEnumProperty_1_t8_352 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2205_gshared (ExifEnumProperty_1_t8_352 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.CustomRendered>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2206_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_352 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_352 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1990_gshared (ExifEnumProperty_1_t8_353 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		NullCheck((ExifProperty_t8_99 *)__this);
		ExifProperty__ctor_m8_502((ExifProperty_t8_99 *)__this, (int32_t)L_0, /*hidden argument*/NULL);
		uint16_t L_1 = ___value;
		__this->___mValue_3 = L_1;
		bool L_2 = ___isbitfield;
		__this->___mIsBitField_4 = L_2;
		return;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2207_gshared (ExifEnumProperty_1_t8_353 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___tag;
		uint16_t L_1 = ___value;
		NullCheck((ExifEnumProperty_1_t8_353 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_353 *, int32_t, uint16_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((ExifEnumProperty_1_t8_353 *)__this, (int32_t)L_0, (uint16_t)L_1, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2208_gshared (ExifEnumProperty_1_t8_353 * __this, const MethodInfo* method)
{
	{
		NullCheck((ExifEnumProperty_1_t8_353 *)__this);
		uint16_t L_0 = (( uint16_t (*) (ExifEnumProperty_1_t8_353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ExifEnumProperty_1_t8_353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return L_2;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2209_gshared (ExifEnumProperty_1_t8_353 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		NullCheck((ExifEnumProperty_1_t8_353 *)__this);
		(( void (*) (ExifEnumProperty_1_t8_353 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ExifEnumProperty_1_t8_353 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2210_gshared (ExifEnumProperty_1_t8_353 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___mValue_3);
		return L_0;
	}
}
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2211_gshared (ExifEnumProperty_1_t8_353 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___mValue_3 = L_0;
		return;
	}
}
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2212_gshared (ExifEnumProperty_1_t8_353 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(__this->___mIsBitField_4);
		return L_0;
	}
}
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2213_gshared (ExifEnumProperty_1_t8_353 * __this, const MethodInfo* method)
{
	{
		uint16_t* L_0 = (uint16_t*)&(__this->___mValue_3);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_0));
		return L_1;
	}
}
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::get_Interoperability()
extern const Il2CppType* FileSource_t8_75_0_0_0_var;
extern const Il2CppType* SceneType_t8_76_0_0_0_var;
extern const Il2CppType* GPSLatitudeRef_t8_86_0_0_0_var;
extern const Il2CppType* GPSLongitudeRef_t8_87_0_0_0_var;
extern const Il2CppType* GPSStatus_t8_89_0_0_0_var;
extern const Il2CppType* GPSMeasureMode_t8_90_0_0_0_var;
extern const Il2CppType* GPSSpeedRef_t8_91_0_0_0_var;
extern const Il2CppType* GPSDirectionRef_t8_92_0_0_0_var;
extern const Il2CppType* GPSDistanceRef_t8_93_0_0_0_var;
extern const Il2CppType* Byte_t1_11_0_0_0_var;
extern const Il2CppType* UInt16_t1_14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t1_24_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t1_109_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1_11_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1_14_il2cpp_TypeInfo_var;
extern TypeInfo* UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var;
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2214_gshared (ExifEnumProperty_1_t8_353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FileSource_t8_75_0_0_0_var = il2cpp_codegen_type_from_index(2003);
		SceneType_t8_76_0_0_0_var = il2cpp_codegen_type_from_index(2004);
		GPSLatitudeRef_t8_86_0_0_0_var = il2cpp_codegen_type_from_index(2014);
		GPSLongitudeRef_t8_87_0_0_0_var = il2cpp_codegen_type_from_index(2015);
		GPSStatus_t8_89_0_0_0_var = il2cpp_codegen_type_from_index(2017);
		GPSMeasureMode_t8_90_0_0_0_var = il2cpp_codegen_type_from_index(2018);
		GPSSpeedRef_t8_91_0_0_0_var = il2cpp_codegen_type_from_index(2019);
		GPSDirectionRef_t8_92_0_0_0_var = il2cpp_codegen_type_from_index(2020);
		GPSDistanceRef_t8_93_0_0_0_var = il2cpp_codegen_type_from_index(2021);
		Byte_t1_11_0_0_0_var = il2cpp_codegen_type_from_index(25);
		UInt16_t1_14_0_0_0_var = il2cpp_codegen_type_from_index(29);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		Enum_t1_24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(67);
		ByteU5BU5D_t1_109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Byte_t1_11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		UInt16_t1_14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4656);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	{
		int32_t L_0 = (int32_t)(((ExifProperty_t8_99 *)__this)->___mTag_0);
		uint16_t L_1 = ExifTagFactory_GetTagID_m8_630(NULL /*static, unused*/, (int32_t)L_0, /*hidden argument*/NULL);
		V_0 = (uint16_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)), /*hidden argument*/NULL);
		V_1 = (Type_t *)L_2;
		Type_t * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t1_24_il2cpp_TypeInfo_var);
		Type_t * L_4 = Enum_GetUnderlyingType_m1_942(NULL /*static, unused*/, (Type_t *)L_3, /*hidden argument*/NULL);
		V_2 = (Type_t *)L_4;
		Type_t * L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(FileSource_t8_75_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_5) == ((Object_t*)(Type_t *)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		Type_t * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(SceneType_t8_76_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0060;
		}
	}

IL_003e:
	{
		uint16_t L_9 = V_0;
		ByteU5BU5D_t1_109* L_10 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_11 = (uint16_t)(__this->___mValue_3);
		uint16_t L_12 = L_11;
		Object_t * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_10, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_13, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_14 = {0};
		ExifInterOperability__ctor_m8_496(&L_14, (uint16_t)L_9, (uint16_t)7, (uint32_t)1, (ByteU5BU5D_t1_109*)L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0060:
	{
		Type_t * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLatitudeRef_t8_86_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_15) == ((Object_t*)(Type_t *)L_16)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSLongitudeRef_t8_87_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_17) == ((Object_t*)(Type_t *)L_18)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSStatus_t8_89_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_19) == ((Object_t*)(Type_t *)L_20)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_21 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSMeasureMode_t8_90_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_21) == ((Object_t*)(Type_t *)L_22)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_23 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_24 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSSpeedRef_t8_91_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_23) == ((Object_t*)(Type_t *)L_24)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_26 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDirectionRef_t8_92_0_0_0_var), /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_25) == ((Object_t*)(Type_t *)L_26)))
		{
			goto IL_00d0;
		}
	}
	{
		Type_t * L_27 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(GPSDistanceRef_t8_93_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_27) == ((Object_t*)(Type_t *)L_28))))
		{
			goto IL_00f2;
		}
	}

IL_00d0:
	{
		uint16_t L_29 = V_0;
		ByteU5BU5D_t1_109* L_30 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 2));
		uint16_t L_31 = (uint16_t)(__this->___mValue_3);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_32);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_30, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_33, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_34 = {0};
		ExifInterOperability__ctor_m8_496(&L_34, (uint16_t)L_29, (uint16_t)2, (uint32_t)2, (ByteU5BU5D_t1_109*)L_30, /*hidden argument*/NULL);
		return L_34;
	}

IL_00f2:
	{
		Type_t * L_35 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_36 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(Byte_t1_11_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_35) == ((Object_t*)(Type_t *)L_36))))
		{
			goto IL_0124;
		}
	}
	{
		uint16_t L_37 = V_0;
		ByteU5BU5D_t1_109* L_38 = (ByteU5BU5D_t1_109*)((ByteU5BU5D_t1_109*)SZArrayNew(ByteU5BU5D_t1_109_il2cpp_TypeInfo_var, 1));
		uint16_t L_39 = (uint16_t)(__this->___mValue_3);
		uint16_t L_40 = L_39;
		Object_t * L_41 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_38, 0, sizeof(uint8_t))) = (uint8_t)((*(uint8_t*)((uint8_t*)UnBox (L_41, Byte_t1_11_il2cpp_TypeInfo_var))));
		ExifInterOperability_t8_113  L_42 = {0};
		ExifInterOperability__ctor_m8_496(&L_42, (uint16_t)L_37, (uint16_t)1, (uint32_t)1, (ByteU5BU5D_t1_109*)L_38, /*hidden argument*/NULL);
		return L_42;
	}

IL_0124:
	{
		Type_t * L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1_1138(NULL /*static, unused*/, (RuntimeTypeHandle_t1_30 )LoadTypeToken(UInt16_t1_14_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_43) == ((Object_t*)(Type_t *)L_44))))
		{
			goto IL_0154;
		}
	}
	{
		uint16_t L_45 = V_0;
		uint16_t L_46 = (uint16_t)(__this->___mValue_3);
		uint16_t L_47 = L_46;
		Object_t * L_48 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_47);
		ByteU5BU5D_t1_109* L_49 = BitConverterEx_GetBytes_m8_354(NULL /*static, unused*/, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_48, UInt16_t1_14_il2cpp_TypeInfo_var)))), (int32_t)0, (int32_t)0, /*hidden argument*/NULL);
		ExifInterOperability_t8_113  L_50 = {0};
		ExifInterOperability__ctor_m8_496(&L_50, (uint16_t)L_45, (uint16_t)3, (uint32_t)1, (ByteU5BU5D_t1_109*)L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_0154:
	{
		UnknownEnumTypeException_t8_96 * L_51 = (UnknownEnumTypeException_t8_96 *)il2cpp_codegen_object_new (UnknownEnumTypeException_t8_96_il2cpp_TypeInfo_var);
		UnknownEnumTypeException__ctor_m8_407(L_51, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_51);
	}
}
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.ExposureMode>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2215_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_353 * ___obj, const MethodInfo* method)
{
	{
		ExifEnumProperty_1_t8_353 * L_0 = ___obj;
		NullCheck(L_0);
		uint16_t L_1 = (uint16_t)(L_0->___mValue_3);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
