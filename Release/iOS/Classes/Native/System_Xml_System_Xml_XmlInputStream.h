﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t1_406;
// System.IO.Stream
struct Stream_t1_405;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Xml.XmlException
struct XmlException_t4_137;

#include "mscorlib_System_IO_Stream.h"

// System.Xml.XmlInputStream
struct  XmlInputStream_t4_140  : public Stream_t1_405
{
	// System.Text.Encoding System.Xml.XmlInputStream::enc
	Encoding_t1_406 * ___enc_2;
	// System.IO.Stream System.Xml.XmlInputStream::stream
	Stream_t1_405 * ___stream_3;
	// System.Byte[] System.Xml.XmlInputStream::buffer
	ByteU5BU5D_t1_109* ___buffer_4;
	// System.Int32 System.Xml.XmlInputStream::bufLength
	int32_t ___bufLength_5;
	// System.Int32 System.Xml.XmlInputStream::bufPos
	int32_t ___bufPos_6;
};
struct XmlInputStream_t4_140_StaticFields{
	// System.Text.Encoding System.Xml.XmlInputStream::StrictUTF8
	Encoding_t1_406 * ___StrictUTF8_1;
	// System.Xml.XmlException System.Xml.XmlInputStream::encodingException
	XmlException_t4_137 * ___encodingException_7;
};
