﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.Security.Policy.HashMembershipCondition
struct  HashMembershipCondition_t1_1351  : public Object_t
{
	// System.Int32 System.Security.Policy.HashMembershipCondition::version
	int32_t ___version_0;
	// System.Security.Cryptography.HashAlgorithm System.Security.Policy.HashMembershipCondition::hash_algorithm
	HashAlgorithm_t1_162 * ___hash_algorithm_1;
	// System.Byte[] System.Security.Policy.HashMembershipCondition::hash_value
	ByteU5BU5D_t1_109* ___hash_value_2;
};
