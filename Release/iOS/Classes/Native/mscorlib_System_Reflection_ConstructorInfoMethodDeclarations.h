﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_MemberTypes.h"

// System.Void System.Reflection.ConstructorInfo::.ctor()
extern "C" void ConstructorInfo__ctor_m1_6766 (ConstructorInfo_t1_478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ConstructorInfo::.cctor()
extern "C" void ConstructorInfo__cctor_m1_6767 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_GetIDsOfNames_m1_6768 (ConstructorInfo_t1_478 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_GetTypeInfo_m1_6769 (ConstructorInfo_t1_478 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.GetTypeInfoCount(System.UInt32&)
extern "C" void ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_GetTypeInfoCount_m1_6770 (ConstructorInfo_t1_478 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_Invoke_m1_6771 (ConstructorInfo_t1_478 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.Invoke_2(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_Invoke_2_m1_6772 (ConstructorInfo_t1_478 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.Invoke_3(System.Object,System.Object[])
extern "C" Object_t * ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_Invoke_3_m1_6773 (ConstructorInfo_t1_478 * __this, Object_t * ___obj, ObjectU5BU5D_t1_272* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.Invoke_4(System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_Invoke_4_m1_6774 (ConstructorInfo_t1_478 * __this, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.Invoke_5(System.Object[])
extern "C" Object_t * ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_Invoke_5_m1_6775 (ConstructorInfo_t1_478 * __this, ObjectU5BU5D_t1_272* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberTypes System.Reflection.ConstructorInfo::get_MemberType()
extern "C" int32_t ConstructorInfo_get_MemberType_m1_6776 (ConstructorInfo_t1_478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern "C" Object_t * ConstructorInfo_Invoke_m1_6777 (ConstructorInfo_t1_478 * __this, ObjectU5BU5D_t1_272* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.ConstructorInfo::System.Runtime.InteropServices._ConstructorInfo.GetType()
extern "C" Type_t * ConstructorInfo_System_Runtime_InteropServices__ConstructorInfo_GetType_m1_6778 (ConstructorInfo_t1_478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
