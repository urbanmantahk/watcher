﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Demo.WebViewDemo
struct WebViewDemo_t8_190;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Demo.WebViewDemo::.ctor()
extern "C" void WebViewDemo__ctor_m8_1114 (WebViewDemo_t8_190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
