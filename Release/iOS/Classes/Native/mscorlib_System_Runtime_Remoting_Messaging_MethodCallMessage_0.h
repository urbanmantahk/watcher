﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.ArgInfo
struct ArgInfo_t1_915;
// System.Runtime.Remoting.Messaging.MethodCallMessageWrapper/DictionaryWrapper
struct DictionaryWrapper_t1_945;

#include "mscorlib_System_Runtime_Remoting_Messaging_InternalMessageWr.h"

// System.Runtime.Remoting.Messaging.MethodCallMessageWrapper
struct  MethodCallMessageWrapper_t1_946  : public InternalMessageWrapper_t1_940
{
	// System.Object[] System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::_args
	ObjectU5BU5D_t1_272* ____args_1;
	// System.Runtime.Remoting.Messaging.ArgInfo System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::_inArgInfo
	ArgInfo_t1_915 * ____inArgInfo_2;
	// System.Runtime.Remoting.Messaging.MethodCallMessageWrapper/DictionaryWrapper System.Runtime.Remoting.Messaging.MethodCallMessageWrapper::_properties
	DictionaryWrapper_t1_945 * ____properties_3;
};
