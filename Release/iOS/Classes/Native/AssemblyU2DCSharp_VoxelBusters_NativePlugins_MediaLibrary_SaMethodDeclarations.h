﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion
struct SaveImageToGalleryCompletion_t8_241;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void SaveImageToGalleryCompletion__ctor_m8_1351 (SaveImageToGalleryCompletion_t8_241 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::Invoke(System.Boolean)
extern "C" void SaveImageToGalleryCompletion_Invoke_m8_1352 (SaveImageToGalleryCompletion_t8_241 * __this, bool ____savedSuccessfully, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_SaveImageToGalleryCompletion_t8_241(Il2CppObject* delegate, bool ____savedSuccessfully);
// System.IAsyncResult VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C" Object_t * SaveImageToGalleryCompletion_BeginInvoke_m8_1353 (SaveImageToGalleryCompletion_t8_241 * __this, bool ____savedSuccessfully, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion::EndInvoke(System.IAsyncResult)
extern "C" void SaveImageToGalleryCompletion_EndInvoke_m8_1354 (SaveImageToGalleryCompletion_t8_241 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
