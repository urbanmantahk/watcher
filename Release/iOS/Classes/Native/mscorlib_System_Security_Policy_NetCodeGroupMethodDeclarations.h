﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.NetCodeGroup
struct NetCodeGroup_t1_1354;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;
// System.Security.Policy.CodeConnectAccess
struct CodeConnectAccess_t1_1338;
// System.Security.Policy.CodeGroup
struct CodeGroup_t1_1339;
// System.Security.Policy.CodeConnectAccess[]
struct CodeConnectAccessU5BU5D_t1_1733;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1_869;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.Policy.Evidence
struct Evidence_t1_398;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.NetCodeGroup::.ctor(System.Security.Policy.IMembershipCondition)
extern "C" void NetCodeGroup__ctor_m1_11556 (NetCodeGroup_t1_1354 * __this, Object_t * ___membershipCondition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.NetCodeGroup::.ctor(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void NetCodeGroup__ctor_m1_11557 (NetCodeGroup_t1_1354 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.NetCodeGroup::.cctor()
extern "C" void NetCodeGroup__cctor_m1_11558 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.NetCodeGroup::get_AttributeString()
extern "C" String_t* NetCodeGroup_get_AttributeString_m1_11559 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.NetCodeGroup::get_MergeLogic()
extern "C" String_t* NetCodeGroup_get_MergeLogic_m1_11560 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.NetCodeGroup::get_PermissionSetName()
extern "C" String_t* NetCodeGroup_get_PermissionSetName_m1_11561 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.NetCodeGroup::AddConnectAccess(System.String,System.Security.Policy.CodeConnectAccess)
extern "C" void NetCodeGroup_AddConnectAccess_m1_11562 (NetCodeGroup_t1_1354 * __this, String_t* ___originScheme, CodeConnectAccess_t1_1338 * ___connectAccess, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.NetCodeGroup::Copy()
extern "C" CodeGroup_t1_1339 * NetCodeGroup_Copy_m1_11563 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.NetCodeGroup::Equals(System.Security.Policy.CodeConnectAccess[],System.Security.Policy.CodeConnectAccess[])
extern "C" bool NetCodeGroup_Equals_m1_11564 (NetCodeGroup_t1_1354 * __this, CodeConnectAccessU5BU5D_t1_1733* ___rules1, CodeConnectAccessU5BU5D_t1_1733* ___rules2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.NetCodeGroup::Equals(System.Object)
extern "C" bool NetCodeGroup_Equals_m1_11565 (NetCodeGroup_t1_1354 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry[] System.Security.Policy.NetCodeGroup::GetConnectAccessRules()
extern "C" DictionaryEntryU5BU5D_t1_869* NetCodeGroup_GetConnectAccessRules_m1_11566 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.NetCodeGroup::GetHashCode()
extern "C" int32_t NetCodeGroup_GetHashCode_m1_11567 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.NetCodeGroup::Resolve(System.Security.Policy.Evidence)
extern "C" PolicyStatement_t1_1334 * NetCodeGroup_Resolve_m1_11568 (NetCodeGroup_t1_1354 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.NetCodeGroup::ResetConnectAccess()
extern "C" void NetCodeGroup_ResetConnectAccess_m1_11569 (NetCodeGroup_t1_1354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.NetCodeGroup::ResolveMatchingCodeGroups(System.Security.Policy.Evidence)
extern "C" CodeGroup_t1_1339 * NetCodeGroup_ResolveMatchingCodeGroups_m1_11570 (NetCodeGroup_t1_1354 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.NetCodeGroup::CreateXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void NetCodeGroup_CreateXml_m1_11571 (NetCodeGroup_t1_1354 * __this, SecurityElement_t1_242 * ___element, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.NetCodeGroup::ParseXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void NetCodeGroup_ParseXml_m1_11572 (NetCodeGroup_t1_1354 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
