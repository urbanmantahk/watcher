﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.Object
struct Object_t6_5;
struct Object_t6_5_marshaled;
// System.Diagnostics.StackTrace
struct StackTrace_t1_336;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_eConsoleLog.h"

// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::.ctor(System.Int32,System.Int32,System.String,VoxelBusters.DebugPRO.Internal.eConsoleLogType,UnityEngine.Object,System.Int32)
extern "C" void ConsoleLog__ctor_m8_1011 (ConsoleLog_t8_170 * __this, int32_t ____logID, int32_t ____tagID, String_t* ____message, int32_t ____type, Object_t6_5 * ____context, int32_t ____skipFrames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::get_ID()
extern "C" int32_t ConsoleLog_get_ID_m8_1012 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_ID(System.Int32)
extern "C" void ConsoleLog_set_ID_m8_1013 (ConsoleLog_t8_170 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.DebugPRO.Internal.ConsoleLog::get_TagID()
extern "C" int32_t ConsoleLog_get_TagID_m8_1014 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_TagID(System.Int32)
extern "C" void ConsoleLog_set_TagID_m8_1015 (ConsoleLog_t8_170 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Message()
extern "C" String_t* ConsoleLog_get_Message_m8_1016 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Message(System.String)
extern "C" void ConsoleLog_set_Message_m8_1017 (ConsoleLog_t8_170 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.DebugPRO.Internal.eConsoleLogType VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Type()
extern "C" int32_t ConsoleLog_get_Type_m8_1018 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Type(VoxelBusters.DebugPRO.Internal.eConsoleLogType)
extern "C" void ConsoleLog_set_Type_m8_1019 (ConsoleLog_t8_170 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Context()
extern "C" Object_t6_5 * ConsoleLog_get_Context_m8_1020 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Context(UnityEngine.Object)
extern "C" void ConsoleLog_set_Context_m8_1021 (ConsoleLog_t8_170 * __this, Object_t6_5 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::get_StackTrace()
extern "C" String_t* ConsoleLog_get_StackTrace_m8_1022 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_StackTrace(System.String)
extern "C" void ConsoleLog_set_StackTrace_m8_1023 (ConsoleLog_t8_170 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::get_Description()
extern "C" String_t* ConsoleLog_get_Description_m8_1024 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::set_Description(System.String)
extern "C" void ConsoleLog_set_Description_m8_1025 (ConsoleLog_t8_170 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::CollectStackTraceInfo(System.Diagnostics.StackTrace)
extern "C" void ConsoleLog_CollectStackTraceInfo_m8_1026 (ConsoleLog_t8_170 * __this, StackTrace_t1_336 * ____stackTrace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.DebugPRO.Internal.ConsoleLog::GetRelativePath(System.String)
extern "C" String_t* ConsoleLog_GetRelativePath_m8_1027 (ConsoleLog_t8_170 * __this, String_t* ____absolutePath, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleLog::IsValid()
extern "C" bool ConsoleLog_IsValid_m8_1028 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleLog::Equals(VoxelBusters.DebugPRO.Internal.ConsoleLog)
extern "C" bool ConsoleLog_Equals_m8_1029 (ConsoleLog_t8_170 * __this, ConsoleLog_t8_170  ____log, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::OnSelect()
extern "C" void ConsoleLog_OnSelect_m8_1030 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.DebugPRO.Internal.ConsoleLog::OnPress()
extern "C" void ConsoleLog_OnPress_m8_1031 (ConsoleLog_t8_170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void ConsoleLog_t8_170_marshal(const ConsoleLog_t8_170& unmarshaled, ConsoleLog_t8_170_marshaled& marshaled);
extern "C" void ConsoleLog_t8_170_marshal_back(const ConsoleLog_t8_170_marshaled& marshaled, ConsoleLog_t8_170& unmarshaled);
extern "C" void ConsoleLog_t8_170_marshal_cleanup(ConsoleLog_t8_170_marshaled& marshaled);
