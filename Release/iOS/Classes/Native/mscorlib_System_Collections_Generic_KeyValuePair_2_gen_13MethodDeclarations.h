﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1_21688(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_2438 *, Event_t6_162 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_21586_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m1_21689(__this, method) (( Event_t6_162 * (*) (KeyValuePair_2_t1_2438 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_21587_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1_21690(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2438 *, Event_t6_162 *, const MethodInfo*))KeyValuePair_2_set_Key_m1_21588_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m1_21691(__this, method) (( int32_t (*) (KeyValuePair_2_t1_2438 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_21589_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1_21692(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2438 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_21590_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m1_21693(__this, method) (( String_t* (*) (KeyValuePair_2_t1_2438 *, const MethodInfo*))KeyValuePair_2_ToString_m1_21591_gshared)(__this, method)
