﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_SystemException.h"

// System.Resources.MissingSatelliteAssemblyException
struct  MissingSatelliteAssemblyException_t1_643  : public SystemException_t1_250
{
	// System.String System.Resources.MissingSatelliteAssemblyException::culture
	String_t* ___culture_12;
};
