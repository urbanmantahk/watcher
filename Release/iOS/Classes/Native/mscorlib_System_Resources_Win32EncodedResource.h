﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Resources_Win32Resource.h"

// System.Resources.Win32EncodedResource
struct  Win32EncodedResource_t1_665  : public Win32Resource_t1_664
{
	// System.Byte[] System.Resources.Win32EncodedResource::data
	ByteU5BU5D_t1_109* ___data_3;
};
