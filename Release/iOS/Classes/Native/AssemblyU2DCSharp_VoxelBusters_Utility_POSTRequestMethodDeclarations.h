﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.POSTRequest
struct POSTRequest_t8_163;
// System.Object
struct Object_t;
// UnityEngine.WWW
struct WWW_t6_78;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.POSTRequest::.ctor(VoxelBusters.Utility.URL,System.Object,System.Boolean)
extern "C" void POSTRequest__ctor_m8_943 (POSTRequest_t8_163 * __this, URL_t8_156  ____URL, Object_t * ____params, bool ____isAsynchronous, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW VoxelBusters.Utility.POSTRequest::CreateWWWObject()
extern "C" WWW_t6_78 * POSTRequest_CreateWWWObject_m8_944 (POSTRequest_t8_163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.POSTRequest VoxelBusters.Utility.POSTRequest::CreateRequest(VoxelBusters.Utility.URL,System.Object)
extern "C" POSTRequest_t8_163 * POSTRequest_CreateRequest_m8_945 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.POSTRequest VoxelBusters.Utility.POSTRequest::CreateAsyncRequest(VoxelBusters.Utility.URL,System.Object)
extern "C" POSTRequest_t8_163 * POSTRequest_CreateAsyncRequest_m8_946 (Object_t * __this /* static, unused */, URL_t8_156  ____URL, Object_t * ____params, const MethodInfo* method) IL2CPP_METHOD_ATTR;
