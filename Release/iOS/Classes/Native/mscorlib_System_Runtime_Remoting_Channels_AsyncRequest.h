﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.AsyncRequest
struct  AsyncRequest_t1_884  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.AsyncRequest::ReplySink
	Object_t * ___ReplySink_0;
	// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.AsyncRequest::MsgRequest
	Object_t * ___MsgRequest_1;
};
