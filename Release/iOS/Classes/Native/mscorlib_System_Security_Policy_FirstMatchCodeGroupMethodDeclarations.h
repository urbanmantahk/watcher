﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.FirstMatchCodeGroup
struct FirstMatchCodeGroup_t1_1347;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Security.Policy.PolicyStatement
struct PolicyStatement_t1_1334;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;
// System.Security.Policy.CodeGroup
struct CodeGroup_t1_1339;
// System.Security.Policy.Evidence
struct Evidence_t1_398;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.FirstMatchCodeGroup::.ctor(System.Security.Policy.IMembershipCondition,System.Security.Policy.PolicyStatement)
extern "C" void FirstMatchCodeGroup__ctor_m1_11490 (FirstMatchCodeGroup_t1_1347 * __this, Object_t * ___membershipCondition, PolicyStatement_t1_1334 * ___policy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.FirstMatchCodeGroup::.ctor(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void FirstMatchCodeGroup__ctor_m1_11491 (FirstMatchCodeGroup_t1_1347 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.FirstMatchCodeGroup::get_MergeLogic()
extern "C" String_t* FirstMatchCodeGroup_get_MergeLogic_m1_11492 (FirstMatchCodeGroup_t1_1347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.FirstMatchCodeGroup::Copy()
extern "C" CodeGroup_t1_1339 * FirstMatchCodeGroup_Copy_m1_11493 (FirstMatchCodeGroup_t1_1347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.PolicyStatement System.Security.Policy.FirstMatchCodeGroup::Resolve(System.Security.Policy.Evidence)
extern "C" PolicyStatement_t1_1334 * FirstMatchCodeGroup_Resolve_m1_11494 (FirstMatchCodeGroup_t1_1347 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.CodeGroup System.Security.Policy.FirstMatchCodeGroup::ResolveMatchingCodeGroups(System.Security.Policy.Evidence)
extern "C" CodeGroup_t1_1339 * FirstMatchCodeGroup_ResolveMatchingCodeGroups_m1_11495 (FirstMatchCodeGroup_t1_1347 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.FirstMatchCodeGroup System.Security.Policy.FirstMatchCodeGroup::CopyNoChildren()
extern "C" FirstMatchCodeGroup_t1_1347 * FirstMatchCodeGroup_CopyNoChildren_m1_11496 (FirstMatchCodeGroup_t1_1347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
