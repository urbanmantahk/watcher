﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.DefaultCharSetAttribute
struct DefaultCharSetAttribute_t1_54;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"

// System.Void System.Runtime.InteropServices.DefaultCharSetAttribute::.ctor(System.Runtime.InteropServices.CharSet)
extern "C" void DefaultCharSetAttribute__ctor_m1_1319 (DefaultCharSetAttribute_t1_54 * __this, int32_t ___charSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.CharSet System.Runtime.InteropServices.DefaultCharSetAttribute::get_CharSet()
extern "C" int32_t DefaultCharSetAttribute_get_CharSet_m1_1320 (DefaultCharSetAttribute_t1_54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
