﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t4_64;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t4_4;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t4_71;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "System_Xml_System_Xml_Schema_XmlSchemaAnnotated.h"
#include "System_Xml_System_Xml_Schema_XmlSchemaDerivationMethod.h"

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_t4_64  : public XmlSchemaAnnotated_t4_53
{
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::BaseXmlSchemaTypeInternal
	XmlSchemaType_t4_64 * ___BaseXmlSchemaTypeInternal_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::DatatypeInternal
	XmlSchemaDatatype_t4_4 * ___DatatypeInternal_5;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::QNameInternal
	XmlQualifiedName_t4_71 * ___QNameInternal_6;
};
struct XmlSchemaType_t4_64_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switch$map2E
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2E_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switch$map2F
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2F_8;
};
