﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t4_40;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdBoolean::.ctor()
extern "C" void XsdBoolean__ctor_m4_58 (XsdBoolean_t4_40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdBoolean::get_TokenizedType()
extern "C" int32_t XsdBoolean_get_TokenizedType_m4_59 (XsdBoolean_t4_40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
