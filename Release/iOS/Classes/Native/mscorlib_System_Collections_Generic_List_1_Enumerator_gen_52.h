﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>
struct List_1_t1_1909;
// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleTag>
struct  Enumerator_t1_2735 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1_1909 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ConsoleTag_t8_171 * ___current_3;
};
