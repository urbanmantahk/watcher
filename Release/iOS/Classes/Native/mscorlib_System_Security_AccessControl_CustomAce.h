﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Security_AccessControl_GenericAce.h"

// System.Security.AccessControl.CustomAce
struct  CustomAce_t1_1144  : public GenericAce_t1_1145
{
	// System.Byte[] System.Security.AccessControl.CustomAce::opaque
	ByteU5BU5D_t1_109* ___opaque_4;
};
struct CustomAce_t1_1144_StaticFields{
	// System.Int32 System.Security.AccessControl.CustomAce::MaxOpaqueLength
	int32_t ___MaxOpaqueLength_5;
};
