﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ContextCallback
struct ContextCallback_t1_1624;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Threading.ContextCallback::.ctor(System.Object,System.IntPtr)
extern "C" void ContextCallback__ctor_m1_14815 (ContextCallback_t1_1624 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ContextCallback::Invoke(System.Object)
extern "C" void ContextCallback_Invoke_m1_14816 (ContextCallback_t1_1624 * __this, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ContextCallback_t1_1624(Il2CppObject* delegate, Object_t * ___state);
// System.IAsyncResult System.Threading.ContextCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * ContextCallback_BeginInvoke_m1_14817 (ContextCallback_t1_1624 * __this, Object_t * ___state, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ContextCallback::EndInvoke(System.IAsyncResult)
extern "C" void ContextCallback_EndInvoke_m1_14818 (ContextCallback_t1_1624 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
