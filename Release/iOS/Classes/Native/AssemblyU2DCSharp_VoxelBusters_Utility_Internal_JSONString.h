﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"

// VoxelBusters.Utility.Internal.JSONString
struct  JSONString_t8_55 
{
	// System.String VoxelBusters.Utility.Internal.JSONString::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_0;
	// System.Boolean VoxelBusters.Utility.Internal.JSONString::<IsNullOrEmpty>k__BackingField
	bool ___U3CIsNullOrEmptyU3Ek__BackingField_1;
	// System.Int32 VoxelBusters.Utility.Internal.JSONString::<Length>k__BackingField
	int32_t ___U3CLengthU3Ek__BackingField_2;
};
// Native definition for marshalling of: VoxelBusters.Utility.Internal.JSONString
struct JSONString_t8_55_marshaled
{
	char* ___U3CValueU3Ek__BackingField_0;
	int32_t ___U3CIsNullOrEmptyU3Ek__BackingField_1;
	int32_t ___U3CLengthU3Ek__BackingField_2;
};
