﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7
struct  U3CGetConsoleTagU3Ec__AnonStorey7_t8_167  : public Object_t
{
	// System.String VoxelBusters.DebugPRO.Console/<GetConsoleTag>c__AnonStorey7::_tagName
	String_t* ____tagName_0;
};
