﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.InsufficientMemoryException
struct InsufficientMemoryException_t1_1556;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"

// System.Void System.InsufficientMemoryException::.ctor()
extern "C" void InsufficientMemoryException__ctor_m1_14162 (InsufficientMemoryException_t1_1556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InsufficientMemoryException::.ctor(System.String)
extern "C" void InsufficientMemoryException__ctor_m1_14163 (InsufficientMemoryException_t1_1556 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InsufficientMemoryException::.ctor(System.String,System.Exception)
extern "C" void InsufficientMemoryException__ctor_m1_14164 (InsufficientMemoryException_t1_1556 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
