﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExifLibrary.JPEGSection[]
struct JPEGSectionU5BU5D_t8_381;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<ExifLibrary.JPEGSection>
struct  List_1_t1_1906  : public Object_t
{
	// T[] System.Collections.Generic.List`1::_items
	JPEGSectionU5BU5D_t8_381* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
};
struct List_1_t1_1906_StaticFields{
	// T[] System.Collections.Generic.List`1::EmptyArray
	JPEGSectionU5BU5D_t8_381* ___EmptyArray_4;
};
