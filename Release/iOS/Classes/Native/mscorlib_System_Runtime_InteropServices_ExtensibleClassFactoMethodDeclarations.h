﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ExtensibleClassFactory
struct ExtensibleClassFactory_t1_790;
// System.Runtime.InteropServices.ObjectCreationDelegate
struct ObjectCreationDelegate_t1_1621;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ExtensibleClassFactory::.ctor()
extern "C" void ExtensibleClassFactory__ctor_m1_7639 (ExtensibleClassFactory_t1_790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ExtensibleClassFactory::.cctor()
extern "C" void ExtensibleClassFactory__cctor_m1_7640 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.ObjectCreationDelegate System.Runtime.InteropServices.ExtensibleClassFactory::GetObjectCreationCallback(System.Type)
extern "C" ObjectCreationDelegate_t1_1621 * ExtensibleClassFactory_GetObjectCreationCallback_m1_7641 (Object_t * __this /* static, unused */, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ExtensibleClassFactory::RegisterObjectCreationCallback(System.Runtime.InteropServices.ObjectCreationDelegate)
extern "C" void ExtensibleClassFactory_RegisterObjectCreationCallback_m1_7642 (Object_t * __this /* static, unused */, ObjectCreationDelegate_t1_1621 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
