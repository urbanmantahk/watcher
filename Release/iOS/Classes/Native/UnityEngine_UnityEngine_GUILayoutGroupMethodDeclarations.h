﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_t6_174;
// UnityEngine.RectOffset
struct RectOffset_t6_178;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t6_290;
// UnityEngine.GUIStyle
struct GUIStyle_t6_176;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t6_177;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.GUILayoutGroup::.ctor()
extern "C" void GUILayoutGroup__ctor_m6_1315 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
extern "C" RectOffset_t6_178 * GUILayoutGroup_get_margin_m6_1316 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
extern "C" void GUILayoutGroup_ApplyOptions_m6_1317 (GUILayoutGroup_t6_174 * __this, GUILayoutOptionU5BU5D_t6_290* ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutGroup_ApplyStyleSettings_m6_1318 (GUILayoutGroup_t6_174 * __this, GUIStyle_t6_176 * ___style, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
extern "C" void GUILayoutGroup_ResetCursor_m6_1319 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::GetNext()
extern "C" GUILayoutEntry_t6_177 * GUILayoutGroup_GetNext_m6_1320 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUILayoutGroup::GetLast()
extern "C" Rect_t6_51  GUILayoutGroup_GetLast_m6_1321 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::Add(UnityEngine.GUILayoutEntry)
extern "C" void GUILayoutGroup_Add_m6_1322 (GUILayoutGroup_t6_174 * __this, GUILayoutEntry_t6_177 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
extern "C" void GUILayoutGroup_CalcWidth_m6_1323 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
extern "C" void GUILayoutGroup_SetHorizontal_m6_1324 (GUILayoutGroup_t6_174 * __this, float ___x, float ___width, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
extern "C" void GUILayoutGroup_CalcHeight_m6_1325 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
extern "C" void GUILayoutGroup_SetVertical_m6_1326 (GUILayoutGroup_t6_174 * __this, float ___y, float ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayoutGroup::ToString()
extern "C" String_t* GUILayoutGroup_ToString_m6_1327 (GUILayoutGroup_t6_174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
