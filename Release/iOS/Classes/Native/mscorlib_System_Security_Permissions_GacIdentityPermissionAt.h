﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"

// System.Security.Permissions.GacIdentityPermissionAttribute
struct  GacIdentityPermissionAttribute_t1_1278  : public CodeAccessSecurityAttribute_t1_1266
{
};
