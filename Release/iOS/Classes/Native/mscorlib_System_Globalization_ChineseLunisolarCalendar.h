﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;

#include "mscorlib_System_Globalization_EastAsianLunisolarCalendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.ChineseLunisolarCalendar
struct  ChineseLunisolarCalendar_t1_357  : public EastAsianLunisolarCalendar_t1_358
{
};
struct ChineseLunisolarCalendar_t1_357_StaticFields{
	// System.Globalization.CCEastAsianLunisolarEraHandler System.Globalization.ChineseLunisolarCalendar::era_handler
	CCEastAsianLunisolarEraHandler_t1_355 * ___era_handler_9;
	// System.DateTime System.Globalization.ChineseLunisolarCalendar::ChineseMin
	DateTime_t1_150  ___ChineseMin_10;
	// System.DateTime System.Globalization.ChineseLunisolarCalendar::ChineseMax
	DateTime_t1_150  ___ChineseMax_11;
};
