﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>
struct ValueCollection_t1_2672;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m1_26482_gshared (ValueCollection_t1_2672 * __this, Dictionary_2_t1_2663 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m1_26482(__this, ___dictionary, method) (( void (*) (ValueCollection_t1_2672 *, Dictionary_2_t1_2663 *, const MethodInfo*))ValueCollection__ctor_m1_26482_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26483_gshared (ValueCollection_t1_2672 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26483(__this, ___item, method) (( void (*) (ValueCollection_t1_2672 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1_26483_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26484_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26484(__this, method) (( void (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1_26484_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26485_gshared (ValueCollection_t1_2672 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26485(__this, ___item, method) (( bool (*) (ValueCollection_t1_2672 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1_26485_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26486_gshared (ValueCollection_t1_2672 * __this, Object_t * ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26486(__this, ___item, method) (( bool (*) (ValueCollection_t1_2672 *, Object_t *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1_26486_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26487_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26487(__this, method) (( Object_t* (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1_26487_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m1_26488_gshared (ValueCollection_t1_2672 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1_26488(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2672 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1_26488_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26489_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26489(__this, method) (( Object_t * (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1_26489_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26490_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26490(__this, method) (( bool (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1_26490_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26491_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26491(__this, method) (( bool (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1_26491_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26492_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26492(__this, method) (( Object_t * (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1_26492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m1_26493_gshared (ValueCollection_t1_2672 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m1_26493(__this, ___array, ___index, method) (( void (*) (ValueCollection_t1_2672 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1_26493_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::GetEnumerator()
extern "C" Enumerator_t1_2673  ValueCollection_GetEnumerator_m1_26494_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1_26494(__this, method) (( Enumerator_t1_2673  (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_GetEnumerator_m1_26494_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ExifLibrary.ExifTag,System.Object>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m1_26495_gshared (ValueCollection_t1_2672 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1_26495(__this, method) (( int32_t (*) (ValueCollection_t1_2672 *, const MethodInfo*))ValueCollection_get_Count_m1_26495_gshared)(__this, method)
