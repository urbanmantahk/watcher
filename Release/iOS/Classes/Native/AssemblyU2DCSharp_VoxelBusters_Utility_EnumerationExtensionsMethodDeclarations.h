﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Enum
struct Enum_t1_24;

#include "codegen/il2cpp-codegen.h"

// System.Int32 VoxelBusters.Utility.EnumerationExtensions::GetValue(System.Enum)
extern "C" int32_t EnumerationExtensions_GetValue_m8_190 (Object_t * __this /* static, unused */, Enum_t1_24 * ____enum, const MethodInfo* method) IL2CPP_METHOD_ATTR;
