﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_Mono_Globalization_Unicode_NormalizationCheck.h"

// Mono.Globalization.Unicode.NormalizationCheck
struct  NormalizationCheck_t1_116 
{
	// System.Int32 Mono.Globalization.Unicode.NormalizationCheck::value__
	int32_t ___value___1;
};
