﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7/SignerInfo
struct SignerInfo_t1_227;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// Mono.Security.ASN1
struct ASN1_t1_149;
// System.String
struct String_t;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t1_151;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1_228;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7/SignerInfo::.ctor()
extern "C" void SignerInfo__ctor_m1_2534 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::.ctor(System.Byte[])
extern "C" void SignerInfo__ctor_m1_2535 (SignerInfo_t1_227 * __this, ByteU5BU5D_t1_109* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::.ctor(Mono.Security.ASN1)
extern "C" void SignerInfo__ctor_m1_2536 (SignerInfo_t1_227 * __this, ASN1_t1_149 * ___asn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.PKCS7/SignerInfo::get_IssuerName()
extern "C" String_t* SignerInfo_get_IssuerName_m1_2537 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/SignerInfo::get_SerialNumber()
extern "C" ByteU5BU5D_t1_109* SignerInfo_get_SerialNumber_m1_2538 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/SignerInfo::get_SubjectKeyIdentifier()
extern "C" ByteU5BU5D_t1_109* SignerInfo_get_SubjectKeyIdentifier_m1_2539 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/SignerInfo::get_ASN1()
extern "C" ASN1_t1_149 * SignerInfo_get_ASN1_m1_2540 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.PKCS7/SignerInfo::get_AuthenticatedAttributes()
extern "C" ArrayList_t1_170 * SignerInfo_get_AuthenticatedAttributes_m1_2541 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Certificate Mono.Security.PKCS7/SignerInfo::get_Certificate()
extern "C" X509Certificate_t1_151 * SignerInfo_get_Certificate_m1_2542 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::set_Certificate(Mono.Security.X509.X509Certificate)
extern "C" void SignerInfo_set_Certificate_m1_2543 (SignerInfo_t1_227 * __this, X509Certificate_t1_151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.PKCS7/SignerInfo::get_HashName()
extern "C" String_t* SignerInfo_get_HashName_m1_2544 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::set_HashName(System.String)
extern "C" void SignerInfo_set_HashName_m1_2545 (SignerInfo_t1_227 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.PKCS7/SignerInfo::get_Key()
extern "C" AsymmetricAlgorithm_t1_228 * SignerInfo_get_Key_m1_2546 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::set_Key(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void SignerInfo_set_Key_m1_2547 (SignerInfo_t1_227 * __this, AsymmetricAlgorithm_t1_228 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/SignerInfo::get_Signature()
extern "C" ByteU5BU5D_t1_109* SignerInfo_get_Signature_m1_2548 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::set_Signature(System.Byte[])
extern "C" void SignerInfo_set_Signature_m1_2549 (SignerInfo_t1_227 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList Mono.Security.PKCS7/SignerInfo::get_UnauthenticatedAttributes()
extern "C" ArrayList_t1_170 * SignerInfo_get_UnauthenticatedAttributes_m1_2550 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mono.Security.PKCS7/SignerInfo::get_Version()
extern "C" uint8_t SignerInfo_get_Version_m1_2551 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.PKCS7/SignerInfo::set_Version(System.Byte)
extern "C" void SignerInfo_set_Version_m1_2552 (SignerInfo_t1_227 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.ASN1 Mono.Security.PKCS7/SignerInfo::GetASN1()
extern "C" ASN1_t1_149 * SignerInfo_GetASN1_m1_2553 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.PKCS7/SignerInfo::GetBytes()
extern "C" ByteU5BU5D_t1_109* SignerInfo_GetBytes_m1_2554 (SignerInfo_t1_227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
