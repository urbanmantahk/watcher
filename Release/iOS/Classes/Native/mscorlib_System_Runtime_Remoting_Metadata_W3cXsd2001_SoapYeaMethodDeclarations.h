﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear
struct SoapYear_t1_995;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::.ctor()
extern "C" void SoapYear__ctor_m1_8911 (SoapYear_t1_995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::.ctor(System.DateTime)
extern "C" void SoapYear__ctor_m1_8912 (SoapYear_t1_995 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::.ctor(System.DateTime,System.Int32)
extern "C" void SoapYear__ctor_m1_8913 (SoapYear_t1_995 * __this, DateTime_t1_150  ___value, int32_t ___sign, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::.cctor()
extern "C" void SoapYear__cctor_m1_8914 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::get_Sign()
extern "C" int32_t SoapYear_get_Sign_m1_8915 (SoapYear_t1_995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::set_Sign(System.Int32)
extern "C" void SoapYear_set_Sign_m1_8916 (SoapYear_t1_995 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::get_Value()
extern "C" DateTime_t1_150  SoapYear_get_Value_m1_8917 (SoapYear_t1_995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::set_Value(System.DateTime)
extern "C" void SoapYear_set_Value_m1_8918 (SoapYear_t1_995 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::get_XsdType()
extern "C" String_t* SoapYear_get_XsdType_m1_8919 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::GetXsdType()
extern "C" String_t* SoapYear_GetXsdType_m1_8920 (SoapYear_t1_995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::Parse(System.String)
extern "C" SoapYear_t1_995 * SoapYear_Parse_m1_8921 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapYear::ToString()
extern "C" String_t* SoapYear_ToString_m1_8922 (SoapYear_t1_995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
