﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Demo
struct Demo_t8_165;
// VoxelBusters.DebugPRO.Console
struct Console_t8_169;

#include "codegen/il2cpp-codegen.h"

// System.Void Demo::.ctor()
extern "C" void Demo__ctor_m8_966 (Demo_t8_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Demo::Start()
extern "C" void Demo_Start_m8_967 (Demo_t8_165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Demo::PrintLogs(VoxelBusters.DebugPRO.Console)
extern "C" void Demo_PrintLogs_m8_968 (Demo_t8_165 * __this, Console_t8_169 * ____p, const MethodInfo* method) IL2CPP_METHOD_ATTR;
