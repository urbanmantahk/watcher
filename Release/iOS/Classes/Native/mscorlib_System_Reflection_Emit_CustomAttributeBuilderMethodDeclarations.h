﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Object
struct Object_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_Emit_CustomAttributeBuilder_Custo.h"

// System.Void System.Reflection.Emit.CustomAttributeBuilder::.ctor(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void CustomAttributeBuilder__ctor_m1_5583 (CustomAttributeBuilder_t1_487 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___cdata, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::.ctor(System.Reflection.ConstructorInfo,System.Object[])
extern "C" void CustomAttributeBuilder__ctor_m1_5584 (CustomAttributeBuilder_t1_487 * __this, ConstructorInfo_t1_478 * ___con, ObjectU5BU5D_t1_272* ___constructorArgs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::.ctor(System.Reflection.ConstructorInfo,System.Object[],System.Reflection.FieldInfo[],System.Object[])
extern "C" void CustomAttributeBuilder__ctor_m1_5585 (CustomAttributeBuilder_t1_487 * __this, ConstructorInfo_t1_478 * ___con, ObjectU5BU5D_t1_272* ___constructorArgs, FieldInfoU5BU5D_t1_1668* ___namedFields, ObjectU5BU5D_t1_272* ___fieldValues, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::.ctor(System.Reflection.ConstructorInfo,System.Object[],System.Reflection.PropertyInfo[],System.Object[])
extern "C" void CustomAttributeBuilder__ctor_m1_5586 (CustomAttributeBuilder_t1_487 * __this, ConstructorInfo_t1_478 * ___con, ObjectU5BU5D_t1_272* ___constructorArgs, PropertyInfoU5BU5D_t1_1670* ___namedProperties, ObjectU5BU5D_t1_272* ___propertyValues, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::.ctor(System.Reflection.ConstructorInfo,System.Object[],System.Reflection.PropertyInfo[],System.Object[],System.Reflection.FieldInfo[],System.Object[])
extern "C" void CustomAttributeBuilder__ctor_m1_5587 (CustomAttributeBuilder_t1_487 * __this, ConstructorInfo_t1_478 * ___con, ObjectU5BU5D_t1_272* ___constructorArgs, PropertyInfoU5BU5D_t1_1670* ___namedProperties, ObjectU5BU5D_t1_272* ___propertyValues, FieldInfoU5BU5D_t1_1668* ___namedFields, ObjectU5BU5D_t1_272* ___fieldValues, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::System.Runtime.InteropServices._CustomAttributeBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void CustomAttributeBuilder_System_Runtime_InteropServices__CustomAttributeBuilder_GetIDsOfNames_m1_5588 (CustomAttributeBuilder_t1_487 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::System.Runtime.InteropServices._CustomAttributeBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void CustomAttributeBuilder_System_Runtime_InteropServices__CustomAttributeBuilder_GetTypeInfo_m1_5589 (CustomAttributeBuilder_t1_487 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::System.Runtime.InteropServices._CustomAttributeBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void CustomAttributeBuilder_System_Runtime_InteropServices__CustomAttributeBuilder_GetTypeInfoCount_m1_5590 (CustomAttributeBuilder_t1_487 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::System.Runtime.InteropServices._CustomAttributeBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void CustomAttributeBuilder_System_Runtime_InteropServices__CustomAttributeBuilder_Invoke_m1_5591 (CustomAttributeBuilder_t1_487 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.CustomAttributeBuilder::get_Ctor()
extern "C" ConstructorInfo_t1_478 * CustomAttributeBuilder_get_Ctor_m1_5592 (CustomAttributeBuilder_t1_487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Emit.CustomAttributeBuilder::get_Data()
extern "C" ByteU5BU5D_t1_109* CustomAttributeBuilder_get_Data_m1_5593 (CustomAttributeBuilder_t1_487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Reflection.Emit.CustomAttributeBuilder::GetBlob(System.Reflection.Assembly,System.Reflection.ConstructorInfo,System.Object[],System.Reflection.PropertyInfo[],System.Object[],System.Reflection.FieldInfo[],System.Object[])
extern "C" ByteU5BU5D_t1_109* CustomAttributeBuilder_GetBlob_m1_5594 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___asmb, ConstructorInfo_t1_478 * ___con, ObjectU5BU5D_t1_272* ___constructorArgs, PropertyInfoU5BU5D_t1_1670* ___namedProperties, ObjectU5BU5D_t1_272* ___propertyValues, FieldInfoU5BU5D_t1_1668* ___namedFields, ObjectU5BU5D_t1_272* ___fieldValues, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.CustomAttributeBuilder::IsValidType(System.Type)
extern "C" bool CustomAttributeBuilder_IsValidType_m1_5595 (CustomAttributeBuilder_t1_487 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.CustomAttributeBuilder::Initialize(System.Reflection.ConstructorInfo,System.Object[],System.Reflection.PropertyInfo[],System.Object[],System.Reflection.FieldInfo[],System.Object[])
extern "C" void CustomAttributeBuilder_Initialize_m1_5596 (CustomAttributeBuilder_t1_487 * __this, ConstructorInfo_t1_478 * ___con, ObjectU5BU5D_t1_272* ___constructorArgs, PropertyInfoU5BU5D_t1_1670* ___namedProperties, ObjectU5BU5D_t1_272* ___propertyValues, FieldInfoU5BU5D_t1_1668* ___namedFields, ObjectU5BU5D_t1_272* ___fieldValues, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.CustomAttributeBuilder::decode_len(System.Byte[],System.Int32,System.Int32&)
extern "C" int32_t CustomAttributeBuilder_decode_len_m1_5597 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___pos, int32_t* ___rpos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.CustomAttributeBuilder::string_from_bytes(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* CustomAttributeBuilder_string_from_bytes_m1_5598 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___data, int32_t ___pos, int32_t ___len, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.CustomAttributeBuilder::string_arg()
extern "C" String_t* CustomAttributeBuilder_string_arg_m1_5599 (CustomAttributeBuilder_t1_487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.CustomAttributeBuilder::get_umarshal(System.Reflection.Emit.CustomAttributeBuilder,System.Boolean)
extern "C" UnmanagedMarshal_t1_505 * CustomAttributeBuilder_get_umarshal_m1_5600 (Object_t * __this /* static, unused */, CustomAttributeBuilder_t1_487 * ___customBuilder, bool ___is_field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.CustomAttributeBuilder::elementTypeToType(System.Int32)
extern "C" Type_t * CustomAttributeBuilder_elementTypeToType_m1_5601 (Object_t * __this /* static, unused */, int32_t ___elementType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.CustomAttributeBuilder::decode_cattr_value(System.Type,System.Byte[],System.Int32,System.Int32&)
extern "C" Object_t * CustomAttributeBuilder_decode_cattr_value_m1_5602 (Object_t * __this /* static, unused */, Type_t * ___t, ByteU5BU5D_t1_109* ___data, int32_t ___pos, int32_t* ___rpos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.CustomAttributeBuilder/CustomAttributeInfo System.Reflection.Emit.CustomAttributeBuilder::decode_cattr(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" CustomAttributeInfo_t1_486  CustomAttributeBuilder_decode_cattr_m1_5603 (Object_t * __this /* static, unused */, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.CustomAttributeBuilder::GetParameters(System.Reflection.ConstructorInfo)
extern "C" ParameterInfoU5BU5D_t1_1685* CustomAttributeBuilder_GetParameters_m1_5604 (Object_t * __this /* static, unused */, ConstructorInfo_t1_478 * ___ctor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
