﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.InternalMessageWrapper
struct  InternalMessageWrapper_t1_940  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.InternalMessageWrapper::WrappedMessage
	Object_t * ___WrappedMessage_0;
};
