﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t1_2372;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t1_1843;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t6_289;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t1_2822;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_20713_gshared (ReadOnlyCollection_1_t1_2372 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_20713(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_20713_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20714_gshared (ReadOnlyCollection_1_t1_2372 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20714(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, UILineInfo_t6_151 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_20714_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20715_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20715(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_20715_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20716_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20716(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_20716_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20717_gshared (ReadOnlyCollection_1_t1_2372 * __this, UILineInfo_t6_151  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20717(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, UILineInfo_t6_151 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_20717_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20718_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20718(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_20718_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UILineInfo_t6_151  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20719_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20719(__this, ___index, method) (( UILineInfo_t6_151  (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_20719_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20720_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, UILineInfo_t6_151  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20720(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, UILineInfo_t6_151 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_20720_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20721_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20721(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_20721_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20722_gshared (ReadOnlyCollection_1_t1_2372 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20722(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_20722_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20723_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20723(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_20723_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_20724_gshared (ReadOnlyCollection_1_t1_2372 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_20724(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2372 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_20724_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20725_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20725(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_20725_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20726_gshared (ReadOnlyCollection_1_t1_2372 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20726(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_20726_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20727_gshared (ReadOnlyCollection_1_t1_2372 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20727(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2372 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_20727_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20728_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20728(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_20728_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20729_gshared (ReadOnlyCollection_1_t1_2372 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20729(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_20729_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20730_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20730(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_20730_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20731_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20731(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_20731_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20732_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20732(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_20732_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20733_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20733(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_20733_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20734_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20734(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_20734_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20735_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20735(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_20735_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20736_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20736(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_20736_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_20737_gshared (ReadOnlyCollection_1_t1_2372 * __this, UILineInfo_t6_151  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_20737(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2372 *, UILineInfo_t6_151 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_20737_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_20738_gshared (ReadOnlyCollection_1_t1_2372 * __this, UILineInfoU5BU5D_t6_289* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_20738(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2372 *, UILineInfoU5BU5D_t6_289*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_20738_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_20739_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_20739(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_20739_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_20740_gshared (ReadOnlyCollection_1_t1_2372 * __this, UILineInfo_t6_151  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_20740(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2372 *, UILineInfo_t6_151 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_20740_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_20741_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_20741(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_20741_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_20742_gshared (ReadOnlyCollection_1_t1_2372 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_20742(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2372 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_20742_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t6_151  ReadOnlyCollection_1_get_Item_m1_20743_gshared (ReadOnlyCollection_1_t1_2372 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_20743(__this, ___index, method) (( UILineInfo_t6_151  (*) (ReadOnlyCollection_1_t1_2372 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_20743_gshared)(__this, ___index, method)
