﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t1_361;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.IFormatProvider
struct IFormatProvider_t1_455;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.NumberFormatInfo::.ctor(System.Int32,System.Boolean)
extern "C" void NumberFormatInfo__ctor_m1_4317 (NumberFormatInfo_t1_361 * __this, int32_t ___lcid, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::.ctor(System.Boolean)
extern "C" void NumberFormatInfo__ctor_m1_4318 (NumberFormatInfo_t1_361 * __this, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::.ctor()
extern "C" void NumberFormatInfo__ctor_m1_4319 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::.cctor()
extern "C" void NumberFormatInfo__cctor_m1_4320 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::InitPatterns()
extern "C" void NumberFormatInfo_InitPatterns_m1_4321 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_CurrencyDecimalDigits()
extern "C" int32_t NumberFormatInfo_get_CurrencyDecimalDigits_m1_4322 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencyDecimalDigits(System.Int32)
extern "C" void NumberFormatInfo_set_CurrencyDecimalDigits_m1_4323 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_CurrencyDecimalSeparator()
extern "C" String_t* NumberFormatInfo_get_CurrencyDecimalSeparator_m1_4324 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencyDecimalSeparator(System.String)
extern "C" void NumberFormatInfo_set_CurrencyDecimalSeparator_m1_4325 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_CurrencyGroupSeparator()
extern "C" String_t* NumberFormatInfo_get_CurrencyGroupSeparator_m1_4326 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencyGroupSeparator(System.String)
extern "C" void NumberFormatInfo_set_CurrencyGroupSeparator_m1_4327 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.NumberFormatInfo::get_CurrencyGroupSizes()
extern "C" Int32U5BU5D_t1_275* NumberFormatInfo_get_CurrencyGroupSizes_m1_4328 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencyGroupSizes(System.Int32[])
extern "C" void NumberFormatInfo_set_CurrencyGroupSizes_m1_4329 (NumberFormatInfo_t1_361 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.NumberFormatInfo::get_RawCurrencyGroupSizes()
extern "C" Int32U5BU5D_t1_275* NumberFormatInfo_get_RawCurrencyGroupSizes_m1_4330 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_RawCurrencyGroupSizes(System.Int32[])
extern "C" void NumberFormatInfo_set_RawCurrencyGroupSizes_m1_4331 (NumberFormatInfo_t1_361 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_CurrencyNegativePattern()
extern "C" int32_t NumberFormatInfo_get_CurrencyNegativePattern_m1_4332 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencyNegativePattern(System.Int32)
extern "C" void NumberFormatInfo_set_CurrencyNegativePattern_m1_4333 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_CurrencyPositivePattern()
extern "C" int32_t NumberFormatInfo_get_CurrencyPositivePattern_m1_4334 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencyPositivePattern(System.Int32)
extern "C" void NumberFormatInfo_set_CurrencyPositivePattern_m1_4335 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_CurrencySymbol()
extern "C" String_t* NumberFormatInfo_get_CurrencySymbol_m1_4336 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_CurrencySymbol(System.String)
extern "C" void NumberFormatInfo_set_CurrencySymbol_m1_4337 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_CurrentInfo()
extern "C" NumberFormatInfo_t1_361 * NumberFormatInfo_get_CurrentInfo_m1_4338 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::get_InvariantInfo()
extern "C" NumberFormatInfo_t1_361 * NumberFormatInfo_get_InvariantInfo_m1_4339 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.NumberFormatInfo::get_IsReadOnly()
extern "C" bool NumberFormatInfo_get_IsReadOnly_m1_4340 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_NaNSymbol()
extern "C" String_t* NumberFormatInfo_get_NaNSymbol_m1_4341 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NaNSymbol(System.String)
extern "C" void NumberFormatInfo_set_NaNSymbol_m1_4342 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_NegativeInfinitySymbol()
extern "C" String_t* NumberFormatInfo_get_NegativeInfinitySymbol_m1_4343 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NegativeInfinitySymbol(System.String)
extern "C" void NumberFormatInfo_set_NegativeInfinitySymbol_m1_4344 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_NegativeSign()
extern "C" String_t* NumberFormatInfo_get_NegativeSign_m1_4345 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NegativeSign(System.String)
extern "C" void NumberFormatInfo_set_NegativeSign_m1_4346 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_NumberDecimalDigits()
extern "C" int32_t NumberFormatInfo_get_NumberDecimalDigits_m1_4347 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NumberDecimalDigits(System.Int32)
extern "C" void NumberFormatInfo_set_NumberDecimalDigits_m1_4348 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_NumberDecimalSeparator()
extern "C" String_t* NumberFormatInfo_get_NumberDecimalSeparator_m1_4349 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NumberDecimalSeparator(System.String)
extern "C" void NumberFormatInfo_set_NumberDecimalSeparator_m1_4350 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_NumberGroupSeparator()
extern "C" String_t* NumberFormatInfo_get_NumberGroupSeparator_m1_4351 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NumberGroupSeparator(System.String)
extern "C" void NumberFormatInfo_set_NumberGroupSeparator_m1_4352 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.NumberFormatInfo::get_NumberGroupSizes()
extern "C" Int32U5BU5D_t1_275* NumberFormatInfo_get_NumberGroupSizes_m1_4353 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NumberGroupSizes(System.Int32[])
extern "C" void NumberFormatInfo_set_NumberGroupSizes_m1_4354 (NumberFormatInfo_t1_361 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.NumberFormatInfo::get_RawNumberGroupSizes()
extern "C" Int32U5BU5D_t1_275* NumberFormatInfo_get_RawNumberGroupSizes_m1_4355 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_RawNumberGroupSizes(System.Int32[])
extern "C" void NumberFormatInfo_set_RawNumberGroupSizes_m1_4356 (NumberFormatInfo_t1_361 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_NumberNegativePattern()
extern "C" int32_t NumberFormatInfo_get_NumberNegativePattern_m1_4357 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_NumberNegativePattern(System.Int32)
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m1_4358 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_PercentDecimalDigits()
extern "C" int32_t NumberFormatInfo_get_PercentDecimalDigits_m1_4359 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentDecimalDigits(System.Int32)
extern "C" void NumberFormatInfo_set_PercentDecimalDigits_m1_4360 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_PercentDecimalSeparator()
extern "C" String_t* NumberFormatInfo_get_PercentDecimalSeparator_m1_4361 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentDecimalSeparator(System.String)
extern "C" void NumberFormatInfo_set_PercentDecimalSeparator_m1_4362 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_PercentGroupSeparator()
extern "C" String_t* NumberFormatInfo_get_PercentGroupSeparator_m1_4363 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentGroupSeparator(System.String)
extern "C" void NumberFormatInfo_set_PercentGroupSeparator_m1_4364 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.NumberFormatInfo::get_PercentGroupSizes()
extern "C" Int32U5BU5D_t1_275* NumberFormatInfo_get_PercentGroupSizes_m1_4365 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentGroupSizes(System.Int32[])
extern "C" void NumberFormatInfo_set_PercentGroupSizes_m1_4366 (NumberFormatInfo_t1_361 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.NumberFormatInfo::get_RawPercentGroupSizes()
extern "C" Int32U5BU5D_t1_275* NumberFormatInfo_get_RawPercentGroupSizes_m1_4367 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_RawPercentGroupSizes(System.Int32[])
extern "C" void NumberFormatInfo_set_RawPercentGroupSizes_m1_4368 (NumberFormatInfo_t1_361 * __this, Int32U5BU5D_t1_275* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_PercentNegativePattern()
extern "C" int32_t NumberFormatInfo_get_PercentNegativePattern_m1_4369 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentNegativePattern(System.Int32)
extern "C" void NumberFormatInfo_set_PercentNegativePattern_m1_4370 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.NumberFormatInfo::get_PercentPositivePattern()
extern "C" int32_t NumberFormatInfo_get_PercentPositivePattern_m1_4371 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentPositivePattern(System.Int32)
extern "C" void NumberFormatInfo_set_PercentPositivePattern_m1_4372 (NumberFormatInfo_t1_361 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_PercentSymbol()
extern "C" String_t* NumberFormatInfo_get_PercentSymbol_m1_4373 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PercentSymbol(System.String)
extern "C" void NumberFormatInfo_set_PercentSymbol_m1_4374 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_PerMilleSymbol()
extern "C" String_t* NumberFormatInfo_get_PerMilleSymbol_m1_4375 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PerMilleSymbol(System.String)
extern "C" void NumberFormatInfo_set_PerMilleSymbol_m1_4376 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_PositiveInfinitySymbol()
extern "C" String_t* NumberFormatInfo_get_PositiveInfinitySymbol_m1_4377 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PositiveInfinitySymbol(System.String)
extern "C" void NumberFormatInfo_set_PositiveInfinitySymbol_m1_4378 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.NumberFormatInfo::get_PositiveSign()
extern "C" String_t* NumberFormatInfo_get_PositiveSign_m1_4379 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.NumberFormatInfo::set_PositiveSign(System.String)
extern "C" void NumberFormatInfo_set_PositiveSign_m1_4380 (NumberFormatInfo_t1_361 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.NumberFormatInfo::GetFormat(System.Type)
extern "C" Object_t * NumberFormatInfo_GetFormat_m1_4381 (NumberFormatInfo_t1_361 * __this, Type_t * ___formatType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.NumberFormatInfo::Clone()
extern "C" Object_t * NumberFormatInfo_Clone_m1_4382 (NumberFormatInfo_t1_361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::ReadOnly(System.Globalization.NumberFormatInfo)
extern "C" NumberFormatInfo_t1_361 * NumberFormatInfo_ReadOnly_m1_4383 (Object_t * __this /* static, unused */, NumberFormatInfo_t1_361 * ___nfi, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.NumberFormatInfo System.Globalization.NumberFormatInfo::GetInstance(System.IFormatProvider)
extern "C" NumberFormatInfo_t1_361 * NumberFormatInfo_GetInstance_m1_4384 (Object_t * __this /* static, unused */, Object_t * ___formatProvider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
