﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.Url
struct Url_t1_1368;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.Policy.Evidence
struct Evidence_t1_398;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.Url::.ctor(System.String)
extern "C" void Url__ctor_m1_11734 (Url_t1_1368 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Url::.ctor(System.String,System.Boolean)
extern "C" void Url__ctor_m1_11735 (Url_t1_1368 * __this, String_t* ___name, bool ___validated, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Url::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t Url_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11736 (Url_t1_1368 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Url::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t Url_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11737 (Url_t1_1368 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Url::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t Url_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11738 (Url_t1_1368 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.Url::Copy()
extern "C" Object_t * Url_Copy_m1_11739 (Url_t1_1368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Policy.Url::CreateIdentityPermission(System.Security.Policy.Evidence)
extern "C" Object_t * Url_CreateIdentityPermission_m1_11740 (Url_t1_1368 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Url::Equals(System.Object)
extern "C" bool Url_Equals_m1_11741 (Url_t1_1368 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Url::GetHashCode()
extern "C" int32_t Url_GetHashCode_m1_11742 (Url_t1_1368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Url::ToString()
extern "C" String_t* Url_ToString_m1_11743 (Url_t1_1368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Url::get_Value()
extern "C" String_t* Url_get_Value_m1_11744 (Url_t1_1368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Url::Prepare(System.String)
extern "C" String_t* Url_Prepare_m1_11745 (Url_t1_1368 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
