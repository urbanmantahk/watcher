﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ConsoleCancelEventArgs
struct ConsoleCancelEventArgs_t1_1513;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ConsoleSpecialKey.h"

// System.Void System.ConsoleCancelEventArgs::.ctor(System.ConsoleSpecialKey)
extern "C" void ConsoleCancelEventArgs__ctor_m1_13412 (ConsoleCancelEventArgs_t1_1513 * __this, int32_t ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ConsoleCancelEventArgs::get_Cancel()
extern "C" bool ConsoleCancelEventArgs_get_Cancel_m1_13413 (ConsoleCancelEventArgs_t1_1513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ConsoleCancelEventArgs::set_Cancel(System.Boolean)
extern "C" void ConsoleCancelEventArgs_set_Cancel_m1_13414 (ConsoleCancelEventArgs_t1_1513 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ConsoleSpecialKey System.ConsoleCancelEventArgs::get_SpecialKey()
extern "C" int32_t ConsoleCancelEventArgs_get_SpecialKey_m1_13415 (ConsoleCancelEventArgs_t1_1513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
