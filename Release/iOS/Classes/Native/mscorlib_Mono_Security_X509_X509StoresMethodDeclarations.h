﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X509Stores
struct X509Stores_t1_198;
// System.String
struct String_t;
// Mono.Security.X509.X509Store
struct X509Store_t1_196;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X509Stores::.ctor(System.String)
extern "C" void X509Stores__ctor_m1_2383 (X509Stores_t1_198 * __this, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_Personal()
extern "C" X509Store_t1_196 * X509Stores_get_Personal_m1_2384 (X509Stores_t1_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_OtherPeople()
extern "C" X509Store_t1_196 * X509Stores_get_OtherPeople_m1_2385 (X509Stores_t1_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_IntermediateCA()
extern "C" X509Store_t1_196 * X509Stores_get_IntermediateCA_m1_2386 (X509Stores_t1_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_TrustedRoot()
extern "C" X509Store_t1_196 * X509Stores_get_TrustedRoot_m1_2387 (X509Stores_t1_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::get_Untrusted()
extern "C" X509Store_t1_196 * X509Stores_get_Untrusted_m1_2388 (X509Stores_t1_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.X509Stores::Clear()
extern "C" void X509Stores_Clear_m1_2389 (X509Stores_t1_198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::Open(System.String,System.Boolean)
extern "C" X509Store_t1_196 * X509Stores_Open_m1_2390 (X509Stores_t1_198 * __this, String_t* ___storeName, bool ___create, const MethodInfo* method) IL2CPP_METHOD_ATTR;
