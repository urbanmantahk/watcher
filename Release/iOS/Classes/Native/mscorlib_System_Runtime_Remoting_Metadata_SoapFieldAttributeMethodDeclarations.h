﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.SoapFieldAttribute
struct SoapFieldAttribute_t1_998;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.SoapFieldAttribute::.ctor()
extern "C" void SoapFieldAttribute__ctor_m1_8943 (SoapFieldAttribute_t1_998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Metadata.SoapFieldAttribute::get_Order()
extern "C" int32_t SoapFieldAttribute_get_Order_m1_8944 (SoapFieldAttribute_t1_998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapFieldAttribute::set_Order(System.Int32)
extern "C" void SoapFieldAttribute_set_Order_m1_8945 (SoapFieldAttribute_t1_998 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.SoapFieldAttribute::get_XmlElementName()
extern "C" String_t* SoapFieldAttribute_get_XmlElementName_m1_8946 (SoapFieldAttribute_t1_998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapFieldAttribute::set_XmlElementName(System.String)
extern "C" void SoapFieldAttribute_set_XmlElementName_m1_8947 (SoapFieldAttribute_t1_998 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Metadata.SoapFieldAttribute::IsInteropXmlElement()
extern "C" bool SoapFieldAttribute_IsInteropXmlElement_m1_8948 (SoapFieldAttribute_t1_998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.SoapFieldAttribute::SetReflectionObject(System.Object)
extern "C" void SoapFieldAttribute_SetReflectionObject_m1_8949 (SoapFieldAttribute_t1_998 * __this, Object_t * ___reflectionObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
