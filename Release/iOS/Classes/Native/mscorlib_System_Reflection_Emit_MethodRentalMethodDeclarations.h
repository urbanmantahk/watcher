﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.MethodRental
struct MethodRental_t1_531;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.Emit.MethodRental::.ctor()
extern "C" void MethodRental__ctor_m1_6141 (MethodRental_t1_531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodRental::System.Runtime.InteropServices._MethodRental.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodRental_System_Runtime_InteropServices__MethodRental_GetIDsOfNames_m1_6142 (MethodRental_t1_531 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodRental::System.Runtime.InteropServices._MethodRental.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodRental_System_Runtime_InteropServices__MethodRental_GetTypeInfo_m1_6143 (MethodRental_t1_531 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodRental::System.Runtime.InteropServices._MethodRental.GetTypeInfoCount(System.UInt32&)
extern "C" void MethodRental_System_Runtime_InteropServices__MethodRental_GetTypeInfoCount_m1_6144 (MethodRental_t1_531 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodRental::System.Runtime.InteropServices._MethodRental.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void MethodRental_System_Runtime_InteropServices__MethodRental_Invoke_m1_6145 (MethodRental_t1_531 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodRental::SwapMethodBody(System.Type,System.Int32,System.IntPtr,System.Int32,System.Int32)
extern "C" void MethodRental_SwapMethodBody_m1_6146 (Object_t * __this /* static, unused */, Type_t * ___cls, int32_t ___methodtoken, IntPtr_t ___rgIL, int32_t ___methodSize, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
