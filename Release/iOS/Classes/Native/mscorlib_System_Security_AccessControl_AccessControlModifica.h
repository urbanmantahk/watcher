﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_AccessControl_AccessControlModifica.h"

// System.Security.AccessControl.AccessControlModification
struct  AccessControlModification_t1_1108 
{
	// System.Int32 System.Security.AccessControl.AccessControlModification::value__
	int32_t ___value___1;
};
