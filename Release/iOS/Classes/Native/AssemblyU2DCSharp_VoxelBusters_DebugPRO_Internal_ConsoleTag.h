﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct  ConsoleTag_t8_171  : public Object_t
{
	// System.String VoxelBusters.DebugPRO.Internal.ConsoleTag::m_name
	String_t* ___m_name_0;
	// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleTag::m_isActive
	bool ___m_isActive_1;
	// System.Boolean VoxelBusters.DebugPRO.Internal.ConsoleTag::m_ignore
	bool ___m_ignore_2;
};
