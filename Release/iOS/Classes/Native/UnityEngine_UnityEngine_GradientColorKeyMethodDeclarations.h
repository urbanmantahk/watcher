﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GradientColorKey.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m6_187 (GradientColorKey_t6_39 * __this, Color_t6_40  ___col, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
