﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.NetworkConnectivitySettings
struct NetworkConnectivitySettings_t8_256;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings
struct EditorSettings_t8_254;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings
struct AndroidSettings_t8_255;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::.ctor()
extern "C" void NetworkConnectivitySettings__ctor_m8_1416 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.NetworkConnectivitySettings::get_IPAddress()
extern "C" String_t* NetworkConnectivitySettings_get_IPAddress_m8_1417 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::set_IPAddress(System.String)
extern "C" void NetworkConnectivitySettings_set_IPAddress_m8_1418 (NetworkConnectivitySettings_t8_256 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings VoxelBusters.NativePlugins.NetworkConnectivitySettings::get_Editor()
extern "C" EditorSettings_t8_254 * NetworkConnectivitySettings_get_Editor_m8_1419 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::set_Editor(VoxelBusters.NativePlugins.NetworkConnectivitySettings/EditorSettings)
extern "C" void NetworkConnectivitySettings_set_Editor_m8_1420 (NetworkConnectivitySettings_t8_256 * __this, EditorSettings_t8_254 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings VoxelBusters.NativePlugins.NetworkConnectivitySettings::get_Android()
extern "C" AndroidSettings_t8_255 * NetworkConnectivitySettings_get_Android_m8_1421 (NetworkConnectivitySettings_t8_256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.NetworkConnectivitySettings::set_Android(VoxelBusters.NativePlugins.NetworkConnectivitySettings/AndroidSettings)
extern "C" void NetworkConnectivitySettings_set_Android_m8_1422 (NetworkConnectivitySettings_t8_256 * __this, AndroidSettings_t8_255 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
