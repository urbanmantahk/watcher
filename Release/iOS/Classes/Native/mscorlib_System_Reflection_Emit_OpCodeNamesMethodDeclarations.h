﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.OpCodeNames
struct OpCodeNames_t1_539;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Emit.OpCodeNames::.ctor()
extern "C" void OpCodeNames__ctor_m1_6269 (OpCodeNames_t1_539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.OpCodeNames::.cctor()
extern "C" void OpCodeNames__cctor_m1_6270 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
