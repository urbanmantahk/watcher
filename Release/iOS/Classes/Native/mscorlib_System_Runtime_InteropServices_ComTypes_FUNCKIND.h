﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_ComTypes_FUNCKIND.h"

// System.Runtime.InteropServices.ComTypes.FUNCKIND
struct  FUNCKIND_t1_735 
{
	// System.Int32 System.Runtime.InteropServices.ComTypes.FUNCKIND::value__
	int32_t ___value___1;
};
