﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger.h"

// Mono.Xml.Schema.XsdUnsignedLong
struct  XsdUnsignedLong_t4_28  : public XsdNonNegativeInteger_t4_27
{
};
