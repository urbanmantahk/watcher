﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;

#include "System_Xml_Mono_Xml_DictionaryBase.h"

// Mono.Xml.DTDCollectionBase
struct  DTDCollectionBase_t4_91  : public DictionaryBase_t4_90
{
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::root
	DTDObjectModel_t4_82 * ___root_5;
};
