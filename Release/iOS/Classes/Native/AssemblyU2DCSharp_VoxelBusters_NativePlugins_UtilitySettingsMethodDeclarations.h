﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UtilitySettings
struct UtilitySettings_t8_309;
// VoxelBusters.NativePlugins.RateMyApp/Settings
struct Settings_t8_310;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.UtilitySettings::.ctor()
extern "C" void UtilitySettings__ctor_m8_1805 (UtilitySettings_t8_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.RateMyApp/Settings VoxelBusters.NativePlugins.UtilitySettings::get_RateMyApp()
extern "C" Settings_t8_310 * UtilitySettings_get_RateMyApp_m8_1806 (UtilitySettings_t8_309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UtilitySettings::set_RateMyApp(VoxelBusters.NativePlugins.RateMyApp/Settings)
extern "C" void UtilitySettings_set_RateMyApp_m8_1807 (UtilitySettings_t8_309 * __this, Settings_t8_310 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
