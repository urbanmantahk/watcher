﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Hosting.ApplicationActivator
struct ApplicationActivator_t1_719;
// System.Runtime.Remoting.ObjectHandle
struct ObjectHandle_t1_1019;
// System.ActivationContext
struct ActivationContext_t1_717;
// System.String[]
struct StringU5BU5D_t1_238;
// System.AppDomainSetup
struct AppDomainSetup_t1_1497;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Hosting.ApplicationActivator::.ctor()
extern "C" void ApplicationActivator__ctor_m1_7571 (ApplicationActivator_t1_719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstance(System.ActivationContext)
extern "C" ObjectHandle_t1_1019 * ApplicationActivator_CreateInstance_m1_7572 (ApplicationActivator_t1_719 * __this, ActivationContext_t1_717 * ___activationContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstance(System.ActivationContext,System.String[])
extern "C" ObjectHandle_t1_1019 * ApplicationActivator_CreateInstance_m1_7573 (ApplicationActivator_t1_719 * __this, ActivationContext_t1_717 * ___activationContext, StringU5BU5D_t1_238* ___activationCustomData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ObjectHandle System.Runtime.Hosting.ApplicationActivator::CreateInstanceHelper(System.AppDomainSetup)
extern "C" ObjectHandle_t1_1019 * ApplicationActivator_CreateInstanceHelper_m1_7574 (Object_t * __this /* static, unused */, AppDomainSetup_t1_1497 * ___adSetup, const MethodInfo* method) IL2CPP_METHOD_ATTR;
