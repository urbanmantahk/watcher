﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "System_Xml_System_Xml_XmlLinkedNode.h"

// System.Xml.XmlProcessingInstruction
struct  XmlProcessingInstruction_t4_159  : public XmlLinkedNode_t4_118
{
	// System.String System.Xml.XmlProcessingInstruction::target
	String_t* ___target_6;
	// System.String System.Xml.XmlProcessingInstruction::data
	String_t* ___data_7;
};
