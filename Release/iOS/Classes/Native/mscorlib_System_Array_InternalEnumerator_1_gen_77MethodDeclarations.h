﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_77.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16305_gshared (InternalEnumerator_1_t1_2069 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16305(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2069 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16305_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16306_gshared (InternalEnumerator_1_t1_2069 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16306(__this, method) (( void (*) (InternalEnumerator_1_t1_2069 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16306_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16307_gshared (InternalEnumerator_1_t1_2069 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16307(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2069 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16307_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16308_gshared (InternalEnumerator_1_t1_2069 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16308(__this, method) (( void (*) (InternalEnumerator_1_t1_2069 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16308_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16309_gshared (InternalEnumerator_1_t1_2069 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16309(__this, method) (( bool (*) (InternalEnumerator_1_t1_2069 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16309_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t1_593  InternalEnumerator_1_get_Current_m1_16310_gshared (InternalEnumerator_1_t1_2069 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16310(__this, method) (( CustomAttributeNamedArgument_t1_593  (*) (InternalEnumerator_1_t1_2069 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16310_gshared)(__this, method)
