﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardPag.h"

// VoxelBusters.NativePlugins.eLeaderboardPageDirection
struct  eLeaderboardPageDirection_t8_228 
{
	// System.Int32 VoxelBusters.NativePlugins.eLeaderboardPageDirection::value__
	int32_t ___value___1;
};
