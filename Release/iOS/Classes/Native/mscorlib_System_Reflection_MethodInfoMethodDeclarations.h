﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1_627;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_MemberTypes.h"

// System.Void System.Reflection.MethodInfo::.ctor()
extern "C" void MethodInfo__ctor_m1_6936 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodInfo::System.Runtime.InteropServices._MethodInfo.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodInfo_System_Runtime_InteropServices__MethodInfo_GetIDsOfNames_m1_6937 (MethodInfo_t * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodInfo::System.Runtime.InteropServices._MethodInfo.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodInfo_System_Runtime_InteropServices__MethodInfo_GetTypeInfo_m1_6938 (MethodInfo_t * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodInfo::System.Runtime.InteropServices._MethodInfo.GetTypeInfoCount(System.UInt32&)
extern "C" void MethodInfo_System_Runtime_InteropServices__MethodInfo_GetTypeInfoCount_m1_6939 (MethodInfo_t * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodInfo::System.Runtime.InteropServices._MethodInfo.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void MethodInfo_System_Runtime_InteropServices__MethodInfo_Invoke_m1_6940 (MethodInfo_t * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberTypes System.Reflection.MethodInfo::get_MemberType()
extern "C" int32_t MethodInfo_get_MemberType_m1_6941 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MethodInfo::get_ReturnType()
extern "C" Type_t * MethodInfo_get_ReturnType_m1_6942 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.MethodInfo::GetGenericMethodDefinition()
extern "C" MethodInfo_t * MethodInfo_GetGenericMethodDefinition_m1_6943 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.MethodInfo::MakeGenericMethod(System.Type[])
extern "C" MethodInfo_t * MethodInfo_MakeGenericMethod_m1_6944 (MethodInfo_t * __this, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.MethodInfo::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* MethodInfo_GetGenericArguments_m1_6945 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethod()
extern "C" bool MethodInfo_get_IsGenericMethod_m1_6946 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodInfo::get_IsGenericMethodDefinition()
extern "C" bool MethodInfo_get_IsGenericMethodDefinition_m1_6947 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodInfo::get_ContainsGenericParameters()
extern "C" bool MethodInfo_get_ContainsGenericParameters_m1_6948 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo System.Reflection.MethodInfo::get_ReturnParameter()
extern "C" ParameterInfo_t1_627 * MethodInfo_get_ReturnParameter_m1_6949 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MethodInfo::System.Runtime.InteropServices._MethodInfo.GetType()
extern "C" Type_t * MethodInfo_System_Runtime_InteropServices__MethodInfo_GetType_m1_6950 (MethodInfo_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
