﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>/$ArrayType$1452
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU241452_t1_1660 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241452_t1_1660__padding[1452];
	};
};
#pragma pack(pop, tp)
