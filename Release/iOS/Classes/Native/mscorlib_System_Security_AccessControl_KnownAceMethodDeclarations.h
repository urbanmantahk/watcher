﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.KnownAce
struct KnownAce_t1_1136;
// System.Security.Principal.SecurityIdentifier
struct SecurityIdentifier_t1_1132;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"

// System.Void System.Security.AccessControl.KnownAce::.ctor(System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags)
extern "C" void KnownAce__ctor_m1_9927 (KnownAce_t1_1136 * __this, int32_t ___inheritanceFlags, int32_t ___propagationFlags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.KnownAce::get_AccessMask()
extern "C" int32_t KnownAce_get_AccessMask_m1_9928 (KnownAce_t1_1136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.KnownAce::set_AccessMask(System.Int32)
extern "C" void KnownAce_set_AccessMask_m1_9929 (KnownAce_t1_1136 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Principal.SecurityIdentifier System.Security.AccessControl.KnownAce::get_SecurityIdentifier()
extern "C" SecurityIdentifier_t1_1132 * KnownAce_get_SecurityIdentifier_m1_9930 (KnownAce_t1_1136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.KnownAce::set_SecurityIdentifier(System.Security.Principal.SecurityIdentifier)
extern "C" void KnownAce_set_SecurityIdentifier_m1_9931 (KnownAce_t1_1136 * __this, SecurityIdentifier_t1_1132 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
