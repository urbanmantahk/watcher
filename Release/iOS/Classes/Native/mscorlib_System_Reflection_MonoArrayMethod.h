﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.String
struct String_t;

#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Reflection.MonoArrayMethod
struct  MonoArrayMethod_t1_537  : public MethodInfo_t
{
	// System.RuntimeMethodHandle System.Reflection.MonoArrayMethod::mhandle
	RuntimeMethodHandle_t1_479  ___mhandle_0;
	// System.Type System.Reflection.MonoArrayMethod::parent
	Type_t * ___parent_1;
	// System.Type System.Reflection.MonoArrayMethod::ret
	Type_t * ___ret_2;
	// System.Type[] System.Reflection.MonoArrayMethod::parameters
	TypeU5BU5D_t1_31* ___parameters_3;
	// System.String System.Reflection.MonoArrayMethod::name
	String_t* ___name_4;
	// System.Int32 System.Reflection.MonoArrayMethod::table_idx
	int32_t ___table_idx_5;
	// System.Reflection.CallingConventions System.Reflection.MonoArrayMethod::call_conv
	int32_t ___call_conv_6;
};
