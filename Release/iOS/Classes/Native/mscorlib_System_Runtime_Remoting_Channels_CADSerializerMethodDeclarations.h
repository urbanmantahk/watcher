﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.CADSerializer
struct CADSerializer_t1_883;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.IO.MemoryStream
struct MemoryStream_t1_433;
// System.Runtime.Remoting.Messaging.IMethodCallMessage
struct IMethodCallMessage_t1_949;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.CADSerializer::.ctor()
extern "C" void CADSerializer__ctor_m1_8094 (CADSerializer_t1_883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Channels.CADSerializer::DeserializeMessage(System.IO.MemoryStream,System.Runtime.Remoting.Messaging.IMethodCallMessage)
extern "C" Object_t * CADSerializer_DeserializeMessage_m1_8095 (Object_t * __this /* static, unused */, MemoryStream_t1_433 * ___mem, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream System.Runtime.Remoting.Channels.CADSerializer::SerializeMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" MemoryStream_t1_433 * CADSerializer_SerializeMessage_m1_8096 (Object_t * __this /* static, unused */, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream System.Runtime.Remoting.Channels.CADSerializer::SerializeObject(System.Object)
extern "C" MemoryStream_t1_433 * CADSerializer_SerializeObject_m1_8097 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.CADSerializer::DeserializeObject(System.IO.MemoryStream)
extern "C" Object_t * CADSerializer_DeserializeObject_m1_8098 (Object_t * __this /* static, unused */, MemoryStream_t1_433 * ___mem, const MethodInfo* method) IL2CPP_METHOD_ATTR;
