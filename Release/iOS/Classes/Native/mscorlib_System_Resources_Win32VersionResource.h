﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;
// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Resources_Win32Resource.h"

// System.Resources.Win32VersionResource
struct  Win32VersionResource_t1_474  : public Win32Resource_t1_664
{
	// System.String[] System.Resources.Win32VersionResource::WellKnownProperties
	StringU5BU5D_t1_238* ___WellKnownProperties_3;
	// System.Int64 System.Resources.Win32VersionResource::signature
	int64_t ___signature_4;
	// System.Int32 System.Resources.Win32VersionResource::struct_version
	int32_t ___struct_version_5;
	// System.Int64 System.Resources.Win32VersionResource::file_version
	int64_t ___file_version_6;
	// System.Int64 System.Resources.Win32VersionResource::product_version
	int64_t ___product_version_7;
	// System.Int32 System.Resources.Win32VersionResource::file_flags_mask
	int32_t ___file_flags_mask_8;
	// System.Int32 System.Resources.Win32VersionResource::file_flags
	int32_t ___file_flags_9;
	// System.Int32 System.Resources.Win32VersionResource::file_os
	int32_t ___file_os_10;
	// System.Int32 System.Resources.Win32VersionResource::file_type
	int32_t ___file_type_11;
	// System.Int32 System.Resources.Win32VersionResource::file_subtype
	int32_t ___file_subtype_12;
	// System.Int64 System.Resources.Win32VersionResource::file_date
	int64_t ___file_date_13;
	// System.Int32 System.Resources.Win32VersionResource::file_lang
	int32_t ___file_lang_14;
	// System.Int32 System.Resources.Win32VersionResource::file_codepage
	int32_t ___file_codepage_15;
	// System.Collections.Hashtable System.Resources.Win32VersionResource::properties
	Hashtable_t1_100 * ___properties_16;
};
