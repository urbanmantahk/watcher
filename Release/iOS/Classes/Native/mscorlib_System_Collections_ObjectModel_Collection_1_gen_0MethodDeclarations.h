﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t1_2070;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t1_589;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1_1748;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1_2778;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void Collection_1__ctor_m1_16341_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_16341(__this, method) (( void (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1__ctor_m1_16341_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_16342_gshared (Collection_1_t1_2070 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_16342(__this, ___list, method) (( void (*) (Collection_1_t1_2070 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_16342_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16343_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16343(__this, method) (( bool (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_16343_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_16344_gshared (Collection_1_t1_2070 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_16344(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2070 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_16344_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_16345_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_16345(__this, method) (( Object_t * (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_16345_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_16346_gshared (Collection_1_t1_2070 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_16346(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2070 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_16346_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_16347_gshared (Collection_1_t1_2070 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_16347(__this, ___value, method) (( bool (*) (Collection_1_t1_2070 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_16347_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_16348_gshared (Collection_1_t1_2070 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_16348(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2070 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_16348_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_16349_gshared (Collection_1_t1_2070 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_16349(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2070 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_16349_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_16350_gshared (Collection_1_t1_2070 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_16350(__this, ___value, method) (( void (*) (Collection_1_t1_2070 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_16350_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_16351_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_16351(__this, method) (( bool (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_16351_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_16352_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_16352(__this, method) (( Object_t * (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_16352_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_16353_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_16353(__this, method) (( bool (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_16353_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_16354_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_16354(__this, method) (( bool (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_16354_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_16355_gshared (Collection_1_t1_2070 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_16355(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2070 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_16355_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_16356_gshared (Collection_1_t1_2070 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_16356(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2070 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_16356_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void Collection_1_Add_m1_16357_gshared (Collection_1_t1_2070 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_16357(__this, ___item, method) (( void (*) (Collection_1_t1_2070 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_Add_m1_16357_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void Collection_1_Clear_m1_16358_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_16358(__this, method) (( void (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_Clear_m1_16358_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_16359_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_16359(__this, method) (( void (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_ClearItems_m1_16359_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m1_16360_gshared (Collection_1_t1_2070 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_16360(__this, ___item, method) (( bool (*) (Collection_1_t1_2070 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_Contains_m1_16360_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_16361_gshared (Collection_1_t1_2070 * __this, CustomAttributeTypedArgumentU5BU5D_t1_1748* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_16361(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2070 *, CustomAttributeTypedArgumentU5BU5D_t1_1748*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_16361_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_16362_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_16362(__this, method) (( Object_t* (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_GetEnumerator_m1_16362_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_16363_gshared (Collection_1_t1_2070 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_16363(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2070 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_IndexOf_m1_16363_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_16364_gshared (Collection_1_t1_2070 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_16364(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2070 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_Insert_m1_16364_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_16365_gshared (Collection_1_t1_2070 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_16365(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2070 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_InsertItem_m1_16365_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_16366_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_16366(__this, method) (( Object_t* (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_get_Items_m1_16366_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m1_16367_gshared (Collection_1_t1_2070 * __this, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_16367(__this, ___item, method) (( bool (*) (Collection_1_t1_2070 *, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_Remove_m1_16367_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_16368_gshared (Collection_1_t1_2070 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_16368(__this, ___index, method) (( void (*) (Collection_1_t1_2070 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_16368_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_16369_gshared (Collection_1_t1_2070 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_16369(__this, ___index, method) (( void (*) (Collection_1_t1_2070 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_16369_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_16370_gshared (Collection_1_t1_2070 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_16370(__this, method) (( int32_t (*) (Collection_1_t1_2070 *, const MethodInfo*))Collection_1_get_Count_m1_16370_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1_594  Collection_1_get_Item_m1_16371_gshared (Collection_1_t1_2070 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_16371(__this, ___index, method) (( CustomAttributeTypedArgument_t1_594  (*) (Collection_1_t1_2070 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_16371_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_16372_gshared (Collection_1_t1_2070 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_16372(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2070 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_set_Item_m1_16372_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_16373_gshared (Collection_1_t1_2070 * __this, int32_t ___index, CustomAttributeTypedArgument_t1_594  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_16373(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2070 *, int32_t, CustomAttributeTypedArgument_t1_594 , const MethodInfo*))Collection_1_SetItem_m1_16373_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_16374_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_16374(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_16374_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeTypedArgument_t1_594  Collection_1_ConvertItem_m1_16375_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_16375(__this /* static, unused */, ___item, method) (( CustomAttributeTypedArgument_t1_594  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_16375_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_16376_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_16376(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_16376_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_16377_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_16377(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_16377_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_16378_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_16378(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_16378_gshared)(__this /* static, unused */, ___list, method)
