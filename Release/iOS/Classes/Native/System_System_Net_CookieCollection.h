﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Net.Cookie>
struct List_1_t1_1811;
// System.Net.CookieCollection/CookieCollectionComparer
struct CookieCollectionComparer_t3_82;

#include "mscorlib_System_Object.h"

// System.Net.CookieCollection
struct  CookieCollection_t3_83  : public Object_t
{
	// System.Collections.Generic.List`1<System.Net.Cookie> System.Net.CookieCollection::list
	List_1_t1_1811 * ___list_0;
};
struct CookieCollection_t3_83_StaticFields{
	// System.Net.CookieCollection/CookieCollectionComparer System.Net.CookieCollection::Comparer
	CookieCollectionComparer_t3_82 * ___Comparer_1;
};
