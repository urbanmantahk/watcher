﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.JSONReader
struct JSONReader_t8_56;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_Internal_JSONString.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_eJSONToken.h"

// System.Void VoxelBusters.Utility.JSONReader::.ctor()
extern "C" void JSONReader__ctor_m8_280 (JSONReader_t8_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONReader::.ctor(System.String)
extern "C" void JSONReader__ctor_m8_281 (JSONReader_t8_56 * __this, String_t* ____inputJSONString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.Internal.JSONString VoxelBusters.Utility.JSONReader::get_JSONString()
extern "C" JSONString_t8_55  JSONReader_get_JSONString_m8_282 (JSONReader_t8_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONReader::set_JSONString(VoxelBusters.Utility.Internal.JSONString)
extern "C" void JSONReader_set_JSONString_m8_283 (JSONReader_t8_56 * __this, JSONString_t8_55  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONReader::Deserialise()
extern "C" Object_t * JSONReader_Deserialise_m8_284 (JSONReader_t8_56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONReader::Deserialise(System.Int32&)
extern "C" Object_t * JSONReader_Deserialise_m8_285 (JSONReader_t8_56 * __this, int32_t* ____errorIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONReader::ReadValue(System.Int32&)
extern "C" Object_t * JSONReader_ReadValue_m8_286 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONReader::ReadObject(System.Int32&)
extern "C" Object_t * JSONReader_ReadObject_m8_287 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.Utility.JSONReader::ReadKeyValuePair(System.Int32&,System.String&,System.Object&)
extern "C" int32_t JSONReader_ReadKeyValuePair_m8_288 (JSONReader_t8_56 * __this, int32_t* ____index, String_t** ____key, Object_t ** ____value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONReader::ReadArray(System.Int32&)
extern "C" Object_t * JSONReader_ReadArray_m8_289 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONReader::ReadArrayElement(System.Int32&,System.Object&)
extern "C" void JSONReader_ReadArrayElement_m8_290 (JSONReader_t8_56 * __this, int32_t* ____index, Object_t ** ____element, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.JSONReader::ReadString(System.Int32&)
extern "C" String_t* JSONReader_ReadString_m8_291 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.JSONReader::ReadNumber(System.Int32&)
extern "C" Object_t * JSONReader_ReadNumber_m8_292 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.eJSONToken VoxelBusters.Utility.JSONReader::LookAhead(System.Int32)
extern "C" int32_t JSONReader_LookAhead_m8_293 (JSONReader_t8_56 * __this, int32_t ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.eJSONToken VoxelBusters.Utility.JSONReader::NextToken(System.Int32&)
extern "C" int32_t JSONReader_NextToken_m8_294 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.JSONReader::RemoveWhiteSpace(System.Int32&)
extern "C" void JSONReader_RemoveWhiteSpace_m8_295 (JSONReader_t8_56 * __this, int32_t* ____index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
