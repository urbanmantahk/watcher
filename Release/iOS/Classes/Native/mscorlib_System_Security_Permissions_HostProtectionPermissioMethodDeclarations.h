﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.HostProtectionPermission
struct HostProtectionPermission_t1_1280;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"

// System.Void System.Security.Permissions.HostProtectionPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void HostProtectionPermission__ctor_m1_10915 (HostProtectionPermission_t1_1280 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionPermission::.ctor(System.Security.Permissions.HostProtectionResource)
extern "C" void HostProtectionPermission__ctor_m1_10916 (HostProtectionPermission_t1_1280 * __this, int32_t ___resources, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.HostProtectionPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t HostProtectionPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_10917 (HostProtectionPermission_t1_1280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionResource System.Security.Permissions.HostProtectionPermission::get_Resources()
extern "C" int32_t HostProtectionPermission_get_Resources_m1_10918 (HostProtectionPermission_t1_1280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionPermission::set_Resources(System.Security.Permissions.HostProtectionResource)
extern "C" void HostProtectionPermission_set_Resources_m1_10919 (HostProtectionPermission_t1_1280 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.HostProtectionPermission::Copy()
extern "C" Object_t * HostProtectionPermission_Copy_m1_10920 (HostProtectionPermission_t1_1280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.HostProtectionPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * HostProtectionPermission_Intersect_m1_10921 (HostProtectionPermission_t1_1280 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.HostProtectionPermission::Union(System.Security.IPermission)
extern "C" Object_t * HostProtectionPermission_Union_m1_10922 (HostProtectionPermission_t1_1280 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool HostProtectionPermission_IsSubsetOf_m1_10923 (HostProtectionPermission_t1_1280 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.HostProtectionPermission::FromXml(System.Security.SecurityElement)
extern "C" void HostProtectionPermission_FromXml_m1_10924 (HostProtectionPermission_t1_1280 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.HostProtectionPermission::ToXml()
extern "C" SecurityElement_t1_242 * HostProtectionPermission_ToXml_m1_10925 (HostProtectionPermission_t1_1280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.HostProtectionPermission::IsUnrestricted()
extern "C" bool HostProtectionPermission_IsUnrestricted_m1_10926 (HostProtectionPermission_t1_1280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.HostProtectionPermission System.Security.Permissions.HostProtectionPermission::Cast(System.Security.IPermission)
extern "C" HostProtectionPermission_t1_1280 * HostProtectionPermission_Cast_m1_10927 (HostProtectionPermission_t1_1280 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
