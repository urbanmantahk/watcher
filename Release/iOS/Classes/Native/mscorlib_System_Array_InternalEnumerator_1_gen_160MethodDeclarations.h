﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_160.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_20229_gshared (InternalEnumerator_1_t1_2347 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_20229(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2347 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_20229_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20230_gshared (InternalEnumerator_1_t1_2347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20230(__this, method) (( void (*) (InternalEnumerator_1_t1_2347 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20230_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20231_gshared (InternalEnumerator_1_t1_2347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20231(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2347 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20231_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_20232_gshared (InternalEnumerator_1_t1_2347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_20232(__this, method) (( void (*) (InternalEnumerator_1_t1_2347 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_20232_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_20233_gshared (InternalEnumerator_1_t1_2347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_20233(__this, method) (( bool (*) (InternalEnumerator_1_t1_2347 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_20233_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::get_Current()
extern "C" CharacterInfo_t6_147  InternalEnumerator_1_get_Current_m1_20234_gshared (InternalEnumerator_1_t1_2347 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_20234(__this, method) (( CharacterInfo_t6_147  (*) (InternalEnumerator_1_t1_2347 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_20234_gshared)(__this, method)
