﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct Collection_1_t1_2724;
// System.Collections.Generic.IList`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IList_1_t1_2723;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// VoxelBusters.DebugPRO.Internal.ConsoleLog[]
struct ConsoleLogU5BU5D_t8_382;
// System.Collections.Generic.IEnumerator`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct IEnumerator_1_t1_2895;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor()
extern "C" void Collection_1__ctor_m1_27236_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1_27236(__this, method) (( void (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1__ctor_m1_27236_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1__ctor_m1_27237_gshared (Collection_1_t1_2724 * __this, Object_t* ___list, const MethodInfo* method);
#define Collection_1__ctor_m1_27237(__this, ___list, method) (( void (*) (Collection_1_t1_2724 *, Object_t*, const MethodInfo*))Collection_1__ctor_m1_27237_gshared)(__this, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27238_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27238(__this, method) (( bool (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_27238_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m1_27239_gshared (Collection_1_t1_2724 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1_27239(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2724 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1_27239_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_27240_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_27240(__this, method) (( Object_t * (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1_27240_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m1_27241_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1_27241(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2724 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1_27241_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m1_27242_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1_27242(__this, ___value, method) (( bool (*) (Collection_1_t1_2724 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1_27242_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m1_27243_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1_27243(__this, ___value, method) (( int32_t (*) (Collection_1_t1_2724 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1_27243_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m1_27244_gshared (Collection_1_t1_2724 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1_27244(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2724 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1_27244_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m1_27245_gshared (Collection_1_t1_2724 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1_27245(__this, ___value, method) (( void (*) (Collection_1_t1_2724 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1_27245_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_27246_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_27246(__this, method) (( bool (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1_27246_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m1_27247_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1_27247(__this, method) (( Object_t * (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1_27247_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m1_27248_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1_27248(__this, method) (( bool (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1_27248_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m1_27249_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1_27249(__this, method) (( bool (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1_27249_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m1_27250_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1_27250(__this, ___index, method) (( Object_t * (*) (Collection_1_t1_2724 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1_27250_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m1_27251_gshared (Collection_1_t1_2724 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1_27251(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2724 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1_27251_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Add(T)
extern "C" void Collection_1_Add_m1_27252_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_Add_m1_27252(__this, ___item, method) (( void (*) (Collection_1_t1_2724 *, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_Add_m1_27252_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Clear()
extern "C" void Collection_1_Clear_m1_27253_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1_27253(__this, method) (( void (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_Clear_m1_27253_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ClearItems()
extern "C" void Collection_1_ClearItems_m1_27254_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1_27254(__this, method) (( void (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_ClearItems_m1_27254_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Contains(T)
extern "C" bool Collection_1_Contains_m1_27255_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_Contains_m1_27255(__this, ___item, method) (( bool (*) (Collection_1_t1_2724 *, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_Contains_m1_27255_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m1_27256_gshared (Collection_1_t1_2724 * __this, ConsoleLogU5BU5D_t8_382* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m1_27256(__this, ___array, ___index, method) (( void (*) (Collection_1_t1_2724 *, ConsoleLogU5BU5D_t8_382*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1_27256_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m1_27257_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1_27257(__this, method) (( Object_t* (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_GetEnumerator_m1_27257_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m1_27258_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m1_27258(__this, ___item, method) (( int32_t (*) (Collection_1_t1_2724 *, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_IndexOf_m1_27258_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m1_27259_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_Insert_m1_27259(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2724 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_Insert_m1_27259_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m1_27260_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m1_27260(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2724 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_InsertItem_m1_27260_gshared)(__this, ___index, ___item, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Items()
extern "C" Object_t* Collection_1_get_Items_m1_27261_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1_27261(__this, method) (( Object_t* (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_get_Items_m1_27261_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::Remove(T)
extern "C" bool Collection_1_Remove_m1_27262_gshared (Collection_1_t1_2724 * __this, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_Remove_m1_27262(__this, ___item, method) (( bool (*) (Collection_1_t1_2724 *, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_Remove_m1_27262_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m1_27263_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m1_27263(__this, ___index, method) (( void (*) (Collection_1_t1_2724 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1_27263_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m1_27264_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m1_27264(__this, ___index, method) (( void (*) (Collection_1_t1_2724 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1_27264_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Count()
extern "C" int32_t Collection_1_get_Count_m1_27265_gshared (Collection_1_t1_2724 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1_27265(__this, method) (( int32_t (*) (Collection_1_t1_2724 *, const MethodInfo*))Collection_1_get_Count_m1_27265_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::get_Item(System.Int32)
extern "C" ConsoleLog_t8_170  Collection_1_get_Item_m1_27266_gshared (Collection_1_t1_2724 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m1_27266(__this, ___index, method) (( ConsoleLog_t8_170  (*) (Collection_1_t1_2724 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1_27266_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m1_27267_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m1_27267(__this, ___index, ___value, method) (( void (*) (Collection_1_t1_2724 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_set_Item_m1_27267_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m1_27268_gshared (Collection_1_t1_2724 * __this, int32_t ___index, ConsoleLog_t8_170  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m1_27268(__this, ___index, ___item, method) (( void (*) (Collection_1_t1_2724 *, int32_t, ConsoleLog_t8_170 , const MethodInfo*))Collection_1_SetItem_m1_27268_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m1_27269_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m1_27269(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m1_27269_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::ConvertItem(System.Object)
extern "C" ConsoleLog_t8_170  Collection_1_ConvertItem_m1_27270_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m1_27270(__this /* static, unused */, ___item, method) (( ConsoleLog_t8_170  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m1_27270_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m1_27271_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m1_27271(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m1_27271_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m1_27272_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1_27272(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m1_27272_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m1_27273_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1_27273(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m1_27273_gshared)(__this /* static, unused */, ___list, method)
