﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_GCCollectionMode.h"

// System.GCCollectionMode
struct  GCCollectionMode_t1_1552 
{
	// System.Int32 System.GCCollectionMode::value__
	int32_t ___value___1;
};
