﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1_466;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Type
struct Type_t;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.Reflection.Module
struct Module_t1_495;
// System.Resources.IResourceWriter
struct IResourceWriter_t1_1684;
// System.Resources.Win32Resource
struct Win32Resource_t1_664;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.IO.FileStream
struct FileStream_t1_146;
// System.IO.FileStream[]
struct FileStreamU5BU5D_t1_1683;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1_471;
// System.Reflection.ManifestResourceInfo
struct ManifestResourceInfo_t1_606;
// System.String[]
struct StringU5BU5D_t1_238;
// System.IO.Stream
struct Stream_t1_405;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderAccess.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_ResourceAttributes.h"
#include "mscorlib_System_Reflection_PortableExecutableKinds.h"
#include "mscorlib_System_Reflection_ImageFileMachine.h"
#include "mscorlib_System_Reflection_Emit_PEFileKinds.h"

// System.Void System.Reflection.Emit.AssemblyBuilder::.ctor(System.Reflection.AssemblyName,System.String,System.Reflection.Emit.AssemblyBuilderAccess,System.Boolean)
extern "C" void AssemblyBuilder__ctor_m1_5453 (AssemblyBuilder_t1_466 * __this, AssemblyName_t1_576 * ___n, String_t* ___directory, int32_t ___access, bool ___corlib_internal, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::System.Runtime.InteropServices._AssemblyBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void AssemblyBuilder_System_Runtime_InteropServices__AssemblyBuilder_GetIDsOfNames_m1_5454 (AssemblyBuilder_t1_466 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::System.Runtime.InteropServices._AssemblyBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void AssemblyBuilder_System_Runtime_InteropServices__AssemblyBuilder_GetTypeInfo_m1_5455 (AssemblyBuilder_t1_466 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::System.Runtime.InteropServices._AssemblyBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void AssemblyBuilder_System_Runtime_InteropServices__AssemblyBuilder_GetTypeInfoCount_m1_5456 (AssemblyBuilder_t1_466 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::System.Runtime.InteropServices._AssemblyBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void AssemblyBuilder_System_Runtime_InteropServices__AssemblyBuilder_Invoke_m1_5457 (AssemblyBuilder_t1_466 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::basic_init(System.Reflection.Emit.AssemblyBuilder)
extern "C" void AssemblyBuilder_basic_init_m1_5458 (Object_t * __this /* static, unused */, AssemblyBuilder_t1_466 * ___ab, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.AssemblyBuilder::get_CodeBase()
extern "C" String_t* AssemblyBuilder_get_CodeBase_m1_5459 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.AssemblyBuilder::get_EntryPoint()
extern "C" MethodInfo_t * AssemblyBuilder_get_EntryPoint_m1_5460 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.AssemblyBuilder::get_Location()
extern "C" String_t* AssemblyBuilder_get_Location_m1_5461 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.AssemblyBuilder::get_ImageRuntimeVersion()
extern "C" String_t* AssemblyBuilder_get_ImageRuntimeVersion_m1_5462 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_ReflectionOnly()
extern "C" bool AssemblyBuilder_get_ReflectionOnly_m1_5463 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::AddResourceFile(System.String,System.String)
extern "C" void AssemblyBuilder_AddResourceFile_m1_5464 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::AddResourceFile(System.String,System.String,System.Reflection.ResourceAttributes)
extern "C" void AssemblyBuilder_AddResourceFile_m1_5465 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, int32_t ___attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::AddResourceFile(System.String,System.String,System.Reflection.ResourceAttributes,System.Boolean)
extern "C" void AssemblyBuilder_AddResourceFile_m1_5466 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, int32_t ___attribute, bool ___fileNeedsToExists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::AddPermissionRequests(System.Security.PermissionSet,System.Security.PermissionSet,System.Security.PermissionSet)
extern "C" void AssemblyBuilder_AddPermissionRequests_m1_5467 (AssemblyBuilder_t1_466 * __this, PermissionSet_t1_563 * ___required, PermissionSet_t1_563 * ___optional, PermissionSet_t1_563 * ___refused, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::EmbedResourceFile(System.String,System.String)
extern "C" void AssemblyBuilder_EmbedResourceFile_m1_5468 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::EmbedResourceFile(System.String,System.String,System.Reflection.ResourceAttributes)
extern "C" void AssemblyBuilder_EmbedResourceFile_m1_5469 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, int32_t ___attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::EmbedResource(System.String,System.Byte[],System.Reflection.ResourceAttributes)
extern "C" void AssemblyBuilder_EmbedResource_m1_5470 (AssemblyBuilder_t1_466 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___blob, int32_t ___attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::AddTypeForwarder(System.Type)
extern "C" void AssemblyBuilder_AddTypeForwarder_m1_5471 (AssemblyBuilder_t1_466 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::DefineDynamicModule(System.String)
extern "C" ModuleBuilder_t1_475 * AssemblyBuilder_DefineDynamicModule_m1_5472 (AssemblyBuilder_t1_466 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::DefineDynamicModule(System.String,System.Boolean)
extern "C" ModuleBuilder_t1_475 * AssemblyBuilder_DefineDynamicModule_m1_5473 (AssemblyBuilder_t1_466 * __this, String_t* ___name, bool ___emitSymbolInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::DefineDynamicModule(System.String,System.String)
extern "C" ModuleBuilder_t1_475 * AssemblyBuilder_DefineDynamicModule_m1_5474 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::DefineDynamicModule(System.String,System.String,System.Boolean)
extern "C" ModuleBuilder_t1_475 * AssemblyBuilder_DefineDynamicModule_m1_5475 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, bool ___emitSymbolInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::DefineDynamicModule(System.String,System.String,System.Boolean,System.Boolean)
extern "C" ModuleBuilder_t1_475 * AssemblyBuilder_DefineDynamicModule_m1_5476 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, bool ___emitSymbolInfo, bool ___transient, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.AssemblyBuilder::InternalAddModule(System.String)
extern "C" Module_t1_495 * AssemblyBuilder_InternalAddModule_m1_5477 (AssemblyBuilder_t1_466 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.AssemblyBuilder::AddModule(System.String)
extern "C" Module_t1_495 * AssemblyBuilder_AddModule_m1_5478 (AssemblyBuilder_t1_466 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.IResourceWriter System.Reflection.Emit.AssemblyBuilder::DefineResource(System.String,System.String,System.String)
extern "C" Object_t * AssemblyBuilder_DefineResource_m1_5479 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___description, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.IResourceWriter System.Reflection.Emit.AssemblyBuilder::DefineResource(System.String,System.String,System.String,System.Reflection.ResourceAttributes)
extern "C" Object_t * AssemblyBuilder_DefineResource_m1_5480 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___description, String_t* ___fileName, int32_t ___attribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::AddUnmanagedResource(System.Resources.Win32Resource)
extern "C" void AssemblyBuilder_AddUnmanagedResource_m1_5481 (AssemblyBuilder_t1_466 * __this, Win32Resource_t1_664 * ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::DefineUnmanagedResource(System.Byte[])
extern "C" void AssemblyBuilder_DefineUnmanagedResource_m1_5482 (AssemblyBuilder_t1_466 * __this, ByteU5BU5D_t1_109* ___resource, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::DefineUnmanagedResource(System.String)
extern "C" void AssemblyBuilder_DefineUnmanagedResource_m1_5483 (AssemblyBuilder_t1_466 * __this, String_t* ___resourceFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::DefineVersionInfoResource()
extern "C" void AssemblyBuilder_DefineVersionInfoResource_m1_5484 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::DefineVersionInfoResource(System.String,System.String,System.String,System.String,System.String)
extern "C" void AssemblyBuilder_DefineVersionInfoResource_m1_5485 (AssemblyBuilder_t1_466 * __this, String_t* ___product, String_t* ___productVersion, String_t* ___company, String_t* ___copyright, String_t* ___trademark, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::DefineIconResource(System.String)
extern "C" void AssemblyBuilder_DefineIconResource_m1_5486 (AssemblyBuilder_t1_466 * __this, String_t* ___iconFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::DefineVersionInfoResourceImpl(System.String)
extern "C" void AssemblyBuilder_DefineVersionInfoResourceImpl_m1_5487 (AssemblyBuilder_t1_466 * __this, String_t* ___fileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder System.Reflection.Emit.AssemblyBuilder::GetDynamicModule(System.String)
extern "C" ModuleBuilder_t1_475 * AssemblyBuilder_GetDynamicModule_m1_5488 (AssemblyBuilder_t1_466 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.AssemblyBuilder::GetExportedTypes()
extern "C" TypeU5BU5D_t1_31* AssemblyBuilder_GetExportedTypes_m1_5489 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.Reflection.Emit.AssemblyBuilder::GetFile(System.String)
extern "C" FileStream_t1_146 * AssemblyBuilder_GetFile_m1_5490 (AssemblyBuilder_t1_466 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream[] System.Reflection.Emit.AssemblyBuilder::GetFiles(System.Boolean)
extern "C" FileStreamU5BU5D_t1_1683* AssemblyBuilder_GetFiles_m1_5491 (AssemblyBuilder_t1_466 * __this, bool ___getResourceModules, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::GetModulesInternal()
extern "C" ModuleU5BU5D_t1_471* AssemblyBuilder_GetModulesInternal_m1_5492 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.AssemblyBuilder::GetTypes(System.Boolean)
extern "C" TypeU5BU5D_t1_31* AssemblyBuilder_GetTypes_m1_5493 (AssemblyBuilder_t1_466 * __this, bool ___exportedOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ManifestResourceInfo System.Reflection.Emit.AssemblyBuilder::GetManifestResourceInfo(System.String)
extern "C" ManifestResourceInfo_t1_606 * AssemblyBuilder_GetManifestResourceInfo_m1_5494 (AssemblyBuilder_t1_466 * __this, String_t* ___resourceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Reflection.Emit.AssemblyBuilder::GetManifestResourceNames()
extern "C" StringU5BU5D_t1_238* AssemblyBuilder_GetManifestResourceNames_m1_5495 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Reflection.Emit.AssemblyBuilder::GetManifestResourceStream(System.String)
extern "C" Stream_t1_405 * AssemblyBuilder_GetManifestResourceStream_m1_5496 (AssemblyBuilder_t1_466 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Reflection.Emit.AssemblyBuilder::GetManifestResourceStream(System.Type,System.String)
extern "C" Stream_t1_405 * AssemblyBuilder_GetManifestResourceStream_m1_5497 (AssemblyBuilder_t1_466 * __this, Type_t * ___type, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsCompilerContext()
extern "C" bool AssemblyBuilder_get_IsCompilerContext_m1_5498 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsSave()
extern "C" bool AssemblyBuilder_get_IsSave_m1_5499 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsRun()
extern "C" bool AssemblyBuilder_get_IsRun_m1_5500 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.AssemblyBuilder::get_AssemblyDir()
extern "C" String_t* AssemblyBuilder_get_AssemblyDir_m1_5501 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsModuleOnly()
extern "C" bool AssemblyBuilder_get_IsModuleOnly_m1_5502 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::set_IsModuleOnly(System.Boolean)
extern "C" void AssemblyBuilder_set_IsModuleOnly_m1_5503 (AssemblyBuilder_t1_466 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.AssemblyBuilder::GetManifestModule()
extern "C" Module_t1_495 * AssemblyBuilder_GetManifestModule_m1_5504 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::Save(System.String,System.Reflection.PortableExecutableKinds,System.Reflection.ImageFileMachine)
extern "C" void AssemblyBuilder_Save_m1_5505 (AssemblyBuilder_t1_466 * __this, String_t* ___assemblyFileName, int32_t ___portableExecutableKind, int32_t ___imageFileMachine, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::Save(System.String)
extern "C" void AssemblyBuilder_Save_m1_5506 (AssemblyBuilder_t1_466 * __this, String_t* ___assemblyFileName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::SetEntryPoint(System.Reflection.MethodInfo)
extern "C" void AssemblyBuilder_SetEntryPoint_m1_5507 (AssemblyBuilder_t1_466 * __this, MethodInfo_t * ___entryMethod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::SetEntryPoint(System.Reflection.MethodInfo,System.Reflection.Emit.PEFileKinds)
extern "C" void AssemblyBuilder_SetEntryPoint_m1_5508 (AssemblyBuilder_t1_466 * __this, MethodInfo_t * ___entryMethod, int32_t ___fileKind, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void AssemblyBuilder_SetCustomAttribute_m1_5509 (AssemblyBuilder_t1_466 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void AssemblyBuilder_SetCustomAttribute_m1_5510 (AssemblyBuilder_t1_466 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::SetCorlibTypeBuilders(System.Type,System.Type,System.Type)
extern "C" void AssemblyBuilder_SetCorlibTypeBuilders_m1_5511 (AssemblyBuilder_t1_466 * __this, Type_t * ___corlib_object_type, Type_t * ___corlib_value_type, Type_t * ___corlib_enum_type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::SetCorlibTypeBuilders(System.Type,System.Type,System.Type,System.Type)
extern "C" void AssemblyBuilder_SetCorlibTypeBuilders_m1_5512 (AssemblyBuilder_t1_466 * __this, Type_t * ___corlib_object_type, Type_t * ___corlib_value_type, Type_t * ___corlib_enum_type, Type_t * ___corlib_void_type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.AssemblyBuilder::not_supported()
extern "C" Exception_t1_33 * AssemblyBuilder_not_supported_m1_5513 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.AssemblyBuilder::check_name_and_filename(System.String,System.String,System.Boolean)
extern "C" void AssemblyBuilder_check_name_and_filename_m1_5514 (AssemblyBuilder_t1_466 * __this, String_t* ___name, String_t* ___fileName, bool ___fileNeedsToExists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.AssemblyBuilder::create_assembly_version(System.String)
extern "C" String_t* AssemblyBuilder_create_assembly_version_m1_5515 (AssemblyBuilder_t1_466 * __this, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.AssemblyBuilder::GetCultureString(System.String)
extern "C" String_t* AssemblyBuilder_GetCultureString_m1_5516 (AssemblyBuilder_t1_466 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Emit.AssemblyBuilder::UnprotectedGetName()
extern "C" AssemblyName_t1_576 * AssemblyBuilder_UnprotectedGetName_m1_5517 (AssemblyBuilder_t1_466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
