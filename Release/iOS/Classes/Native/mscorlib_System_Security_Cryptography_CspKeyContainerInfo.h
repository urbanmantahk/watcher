﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.CspParameters
struct CspParameters_t1_164;

#include "mscorlib_System_Object.h"

// System.Security.Cryptography.CspKeyContainerInfo
struct  CspKeyContainerInfo_t1_1196  : public Object_t
{
	// System.Security.Cryptography.CspParameters System.Security.Cryptography.CspKeyContainerInfo::_params
	CspParameters_t1_164 * ____params_0;
	// System.Boolean System.Security.Cryptography.CspKeyContainerInfo::_random
	bool ____random_1;
};
