﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_ImageFileMachine.h"

// System.Reflection.ImageFileMachine
struct  ImageFileMachine_t1_601 
{
	// System.Int32 System.Reflection.ImageFileMachine::value__
	int32_t ___value___1;
};
