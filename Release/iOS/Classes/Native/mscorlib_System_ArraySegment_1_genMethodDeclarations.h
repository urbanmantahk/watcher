﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ArraySegment_1_gen.h"

// System.Void System.ArraySegment`1<System.Object>::.ctor(T[],System.Int32,System.Int32)
extern "C" void ArraySegment_1__ctor_m1_17557_gshared (ArraySegment_1_t1_2160 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___offset, int32_t ___count, const MethodInfo* method);
#define ArraySegment_1__ctor_m1_17557(__this, ___array, ___offset, ___count, method) (( void (*) (ArraySegment_1_t1_2160 *, ObjectU5BU5D_t1_272*, int32_t, int32_t, const MethodInfo*))ArraySegment_1__ctor_m1_17557_gshared)(__this, ___array, ___offset, ___count, method)
// System.Void System.ArraySegment`1<System.Object>::.ctor(T[])
extern "C" void ArraySegment_1__ctor_m1_17558_gshared (ArraySegment_1_t1_2160 * __this, ObjectU5BU5D_t1_272* ___array, const MethodInfo* method);
#define ArraySegment_1__ctor_m1_17558(__this, ___array, method) (( void (*) (ArraySegment_1_t1_2160 *, ObjectU5BU5D_t1_272*, const MethodInfo*))ArraySegment_1__ctor_m1_17558_gshared)(__this, ___array, method)
// T[] System.ArraySegment`1<System.Object>::get_Array()
extern "C" ObjectU5BU5D_t1_272* ArraySegment_1_get_Array_m1_17559_gshared (ArraySegment_1_t1_2160 * __this, const MethodInfo* method);
#define ArraySegment_1_get_Array_m1_17559(__this, method) (( ObjectU5BU5D_t1_272* (*) (ArraySegment_1_t1_2160 *, const MethodInfo*))ArraySegment_1_get_Array_m1_17559_gshared)(__this, method)
// System.Int32 System.ArraySegment`1<System.Object>::get_Offset()
extern "C" int32_t ArraySegment_1_get_Offset_m1_17560_gshared (ArraySegment_1_t1_2160 * __this, const MethodInfo* method);
#define ArraySegment_1_get_Offset_m1_17560(__this, method) (( int32_t (*) (ArraySegment_1_t1_2160 *, const MethodInfo*))ArraySegment_1_get_Offset_m1_17560_gshared)(__this, method)
// System.Int32 System.ArraySegment`1<System.Object>::get_Count()
extern "C" int32_t ArraySegment_1_get_Count_m1_17561_gshared (ArraySegment_1_t1_2160 * __this, const MethodInfo* method);
#define ArraySegment_1_get_Count_m1_17561(__this, method) (( int32_t (*) (ArraySegment_1_t1_2160 *, const MethodInfo*))ArraySegment_1_get_Count_m1_17561_gshared)(__this, method)
// System.Boolean System.ArraySegment`1<System.Object>::Equals(System.Object)
extern "C" bool ArraySegment_1_Equals_m1_17562_gshared (ArraySegment_1_t1_2160 * __this, Object_t * ___obj, const MethodInfo* method);
#define ArraySegment_1_Equals_m1_17562(__this, ___obj, method) (( bool (*) (ArraySegment_1_t1_2160 *, Object_t *, const MethodInfo*))ArraySegment_1_Equals_m1_17562_gshared)(__this, ___obj, method)
// System.Boolean System.ArraySegment`1<System.Object>::Equals(System.ArraySegment`1<T>)
extern "C" bool ArraySegment_1_Equals_m1_17563_gshared (ArraySegment_1_t1_2160 * __this, ArraySegment_1_t1_2160  ___obj, const MethodInfo* method);
#define ArraySegment_1_Equals_m1_17563(__this, ___obj, method) (( bool (*) (ArraySegment_1_t1_2160 *, ArraySegment_1_t1_2160 , const MethodInfo*))ArraySegment_1_Equals_m1_17563_gshared)(__this, ___obj, method)
// System.Int32 System.ArraySegment`1<System.Object>::GetHashCode()
extern "C" int32_t ArraySegment_1_GetHashCode_m1_17564_gshared (ArraySegment_1_t1_2160 * __this, const MethodInfo* method);
#define ArraySegment_1_GetHashCode_m1_17564(__this, method) (( int32_t (*) (ArraySegment_1_t1_2160 *, const MethodInfo*))ArraySegment_1_GetHashCode_m1_17564_gshared)(__this, method)
// System.Boolean System.ArraySegment`1<System.Object>::op_Equality(System.ArraySegment`1<T>,System.ArraySegment`1<T>)
extern "C" bool ArraySegment_1_op_Equality_m1_17565_gshared (Object_t * __this /* static, unused */, ArraySegment_1_t1_2160  ___a, ArraySegment_1_t1_2160  ___b, const MethodInfo* method);
#define ArraySegment_1_op_Equality_m1_17565(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, ArraySegment_1_t1_2160 , ArraySegment_1_t1_2160 , const MethodInfo*))ArraySegment_1_op_Equality_m1_17565_gshared)(__this /* static, unused */, ___a, ___b, method)
// System.Boolean System.ArraySegment`1<System.Object>::op_Inequality(System.ArraySegment`1<T>,System.ArraySegment`1<T>)
extern "C" bool ArraySegment_1_op_Inequality_m1_17566_gshared (Object_t * __this /* static, unused */, ArraySegment_1_t1_2160  ___a, ArraySegment_1_t1_2160  ___b, const MethodInfo* method);
#define ArraySegment_1_op_Inequality_m1_17566(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, ArraySegment_1_t1_2160 , ArraySegment_1_t1_2160 , const MethodInfo*))ArraySegment_1_op_Inequality_m1_17566_gshared)(__this /* static, unused */, ___a, ___b, method)
