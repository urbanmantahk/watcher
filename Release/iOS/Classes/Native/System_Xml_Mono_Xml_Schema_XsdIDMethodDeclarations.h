﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdID
struct XsdID_t4_15;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdID::.ctor()
extern "C" void XsdID__ctor_m4_23 (XsdID_t4_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdID::get_TokenizedType()
extern "C" int32_t XsdID_get_TokenizedType_m4_24 (XsdID_t4_15 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
