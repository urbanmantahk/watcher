﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_0.h"

// VoxelBusters.NativePlugins.Internal.NPObjectManager/eCollectionType
struct  eCollectionType_t8_325 
{
	// System.Int32 VoxelBusters.NativePlugins.Internal.NPObjectManager/eCollectionType::value__
	int32_t ___value___1;
};
