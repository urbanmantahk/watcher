﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"

// ExifLibrary.ExifURational
struct  ExifURational_t8_120  : public ExifProperty_t8_99
{
	// ExifLibrary.MathEx/UFraction32 ExifLibrary.ExifURational::mValue
	UFraction32_t8_121  ___mValue_3;
};
