﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Encoder
struct Encoder_t1_1428;
// System.Text.EncoderFallback
struct EncoderFallback_t1_1419;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1_1429;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.Encoder::.ctor()
extern "C" void Encoder__ctor_m1_12250 (Encoder_t1_1428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.Encoder::get_Fallback()
extern "C" EncoderFallback_t1_1419 * Encoder_get_Fallback_m1_12251 (Encoder_t1_1428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::set_Fallback(System.Text.EncoderFallback)
extern "C" void Encoder_set_Fallback_m1_12252 (Encoder_t1_1428 * __this, EncoderFallback_t1_1419 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.Encoder::get_FallbackBuffer()
extern "C" EncoderFallbackBuffer_t1_1429 * Encoder_get_FallbackBuffer_m1_12253 (Encoder_t1_1428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoder::GetByteCount(System.Char*,System.Int32,System.Boolean)
extern "C" int32_t Encoder_GetByteCount_m1_12254 (Encoder_t1_1428 * __this, uint16_t* ___chars, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Encoder::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32,System.Boolean)
extern "C" int32_t Encoder_GetBytes_m1_12255 (Encoder_t1_1428 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::Reset()
extern "C" void Encoder_Reset_m1_12256 (Encoder_t1_1428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::Convert(System.Char*,System.Int32,System.Byte*,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&)
extern "C" void Encoder_Convert_m1_12257 (Encoder_t1_1428 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, bool ___flush, int32_t* ___charsUsed, int32_t* ___bytesUsed, bool* ___completed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::Convert(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&)
extern "C" void Encoder_Convert_m1_12258 (Encoder_t1_1428 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, bool ___flush, int32_t* ___charsUsed, int32_t* ___bytesUsed, bool* ___completed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::CheckArguments(System.Char*,System.Int32,System.Byte*,System.Int32)
extern "C" void Encoder_CheckArguments_m1_12259 (Encoder_t1_1428 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
