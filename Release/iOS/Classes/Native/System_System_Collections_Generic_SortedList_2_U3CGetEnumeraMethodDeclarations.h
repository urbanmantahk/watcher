﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>
struct U3CGetEnumeratorU3Ec__Iterator1_t3_265;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_1992_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_265 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_1992(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_265 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1__ctor_m3_1992_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C" KeyValuePair_2_t1_2015  U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_1993_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_265 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_1993(__this, method) (( KeyValuePair_2_t1_2015  (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_265 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_1993_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_1994_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_265 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_1994(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_265 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3_1994_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_1995_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_265 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_1995(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_265 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_MoveNext_m3_1995_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_1996_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_265 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_1996(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_265 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Dispose_m3_1996_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<GetEnumerator>c__Iterator1<System.Object,System.Object>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_1997_gshared (U3CGetEnumeratorU3Ec__Iterator1_t3_265 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_1997(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator1_t3_265 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator1_Reset_m3_1997_gshared)(__this, method)
