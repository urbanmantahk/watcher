﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1_1978;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1_1980;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int32>
struct IDictionary_2_t1_2770;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1_2012;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t1_2771;
// System.Collections.ICollection
struct ICollection_t1_280;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1_2769;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t1_2772;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1_863;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t1_1984;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t1_1988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m1_15561_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15561(__this, method) (( void (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2__ctor_m1_15561_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_15563_gshared (Dictionary_2_t1_1978 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15563(__this, ___comparer, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15563_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m1_15565_gshared (Dictionary_2_t1_1978 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15565(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15565_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m1_15566_gshared (Dictionary_2_t1_1978 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15566(__this, ___capacity, method) (( void (*) (Dictionary_2_t1_1978 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1_15566_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_15568_gshared (Dictionary_2_t1_1978 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15568(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15568_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m1_15570_gshared (Dictionary_2_t1_1978 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15570(__this, ___capacity, ___comparer, method) (( void (*) (Dictionary_2_t1_1978 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2__ctor_m1_15570_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m1_15572_gshared (Dictionary_2_t1_1978 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m1_15572(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_1978 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2__ctor_m1_15572_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_15574_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_15574(__this, method) (( Object_t* (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1_15574_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_15576_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_15576(__this, method) (( Object_t* (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1_15576_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Keys_m1_15578_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1_15578(__this, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1_15578_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Values_m1_15580_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1_15580(__this, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1_15580_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_15582_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_15582(__this, method) (( bool (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1_15582_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_15584_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_15584(__this, method) (( bool (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1_15584_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m1_15586_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1_15586(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1_15586_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m1_15588_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1_15588(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1_15588_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m1_15590_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1_15590(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1_15590_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m1_15592_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1_15592(__this, ___key, method) (( bool (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1_15592_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m1_15594_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1_15594(__this, ___key, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1_15594_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_15596_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_15596(__this, method) (( bool (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1_15596_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_15598_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_15598(__this, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1_15598_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_15600_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_15600(__this, method) (( bool (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1_15600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_15602_gshared (Dictionary_2_t1_1978 * __this, KeyValuePair_2_t1_1981  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_15602(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1_1978 *, KeyValuePair_2_t1_1981 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1_15602_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_15604_gshared (Dictionary_2_t1_1978 * __this, KeyValuePair_2_t1_1981  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_15604(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_1978 *, KeyValuePair_2_t1_1981 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1_15604_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_15606_gshared (Dictionary_2_t1_1978 * __this, KeyValuePair_2U5BU5D_t1_2769* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_15606(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1978 *, KeyValuePair_2U5BU5D_t1_2769*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1_15606_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_15608_gshared (Dictionary_2_t1_1978 * __this, KeyValuePair_2_t1_1981  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_15608(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1_1978 *, KeyValuePair_2_t1_1981 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1_15608_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m1_15610_gshared (Dictionary_2_t1_1978 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1_15610(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1978 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1_15610_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_15612_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_15612(__this, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1_15612_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_15614_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_15614(__this, method) (( Object_t* (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1_15614_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_15616_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_15616(__this, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1_15616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m1_15618_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1_15618(__this, method) (( int32_t (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_get_Count_m1_15618_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m1_15620_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m1_15620(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m1_15620_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m1_15622_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m1_15622(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1_15622_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m1_15624_gshared (Dictionary_2_t1_1978 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m1_15624(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1_1978 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m1_15624_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m1_15626_gshared (Dictionary_2_t1_1978 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m1_15626(__this, ___size, method) (( void (*) (Dictionary_2_t1_1978 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m1_15626_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m1_15628_gshared (Dictionary_2_t1_1978 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1_15628(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1978 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1_15628_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1_1981  Dictionary_2_make_pair_m1_15630_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m1_15630(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1_1981  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m1_15630_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m1_15632_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m1_15632(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1_15632_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m1_15634_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m1_15634(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m1_15634_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m1_15636_gshared (Dictionary_2_t1_1978 * __this, KeyValuePair_2U5BU5D_t1_2769* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1_15636(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1_1978 *, KeyValuePair_2U5BU5D_t1_2769*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1_15636_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m1_15638_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1_15638(__this, method) (( void (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_Resize_m1_15638_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m1_15640_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m1_15640(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m1_15640_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Comparer()
extern "C" Object_t* Dictionary_2_get_Comparer_m1_15642_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_get_Comparer_m1_15642(__this, method) (( Object_t* (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_get_Comparer_m1_15642_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m1_15644_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1_15644(__this, method) (( void (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_Clear_m1_15644_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m1_15646_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1_15646(__this, ___key, method) (( bool (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m1_15646_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m1_15648_gshared (Dictionary_2_t1_1978 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1_15648(__this, ___value, method) (( bool (*) (Dictionary_2_t1_1978 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m1_15648_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m1_15650_gshared (Dictionary_2_t1_1978 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1_15650(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1_1978 *, SerializationInfo_t1_293 *, StreamingContext_t1_1050 , const MethodInfo*))Dictionary_2_GetObjectData_m1_15650_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m1_15652_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1_15652(__this, ___sender, method) (( void (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m1_15652_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m1_15654_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m1_15654(__this, ___key, method) (( bool (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m1_15654_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m1_15656_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1_15656(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1_1978 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m1_15656_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Keys()
extern "C" KeyCollection_t1_1984 * Dictionary_2_get_Keys_m1_15658_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m1_15658(__this, method) (( KeyCollection_t1_1984 * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_get_Keys_m1_15658_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t1_1988 * Dictionary_2_get_Values_m1_15660_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1_15660(__this, method) (( ValueCollection_t1_1988 * (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_get_Values_m1_15660_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m1_15662_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1_15662(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m1_15662_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m1_15664_gshared (Dictionary_2_t1_1978 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1_15664(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1_1978 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m1_15664_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m1_15666_gshared (Dictionary_2_t1_1978 * __this, KeyValuePair_2_t1_1981  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1_15666(__this, ___pair, method) (( bool (*) (Dictionary_2_t1_1978 *, KeyValuePair_2_t1_1981 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1_15666_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t1_1986  Dictionary_2_GetEnumerator_m1_15668_gshared (Dictionary_2_t1_1978 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1_15668(__this, method) (( Enumerator_t1_1986  (*) (Dictionary_2_t1_1978 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1_15668_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1_284  Dictionary_2_U3CCopyToU3Em__0_m1_15670_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1_15670(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1_284  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1_15670_gshared)(__this /* static, unused */, ___key, ___value, method)
