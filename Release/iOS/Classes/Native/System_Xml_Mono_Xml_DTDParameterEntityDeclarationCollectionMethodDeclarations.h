﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t4_85;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t4_82;
// Mono.Xml.DTDParameterEntityDeclaration
struct DTDParameterEntityDeclaration_t4_100;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Xml.DTDParameterEntityDeclarationCollection::.ctor(Mono.Xml.DTDObjectModel)
extern "C" void DTDParameterEntityDeclarationCollection__ctor_m4_250 (DTDParameterEntityDeclarationCollection_t4_85 * __this, DTDObjectModel_t4_82 * ___root, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Xml.DTDParameterEntityDeclaration Mono.Xml.DTDParameterEntityDeclarationCollection::get_Item(System.String)
extern "C" DTDParameterEntityDeclaration_t4_100 * DTDParameterEntityDeclarationCollection_get_Item_m4_251 (DTDParameterEntityDeclarationCollection_t4_85 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Xml.DTDParameterEntityDeclarationCollection::Add(System.String,Mono.Xml.DTDParameterEntityDeclaration)
extern "C" void DTDParameterEntityDeclarationCollection_Add_m4_252 (DTDParameterEntityDeclarationCollection_t4_85 * __this, String_t* ___name, DTDParameterEntityDeclaration_t4_100 * ___decl, const MethodInfo* method) IL2CPP_METHOD_ATTR;
