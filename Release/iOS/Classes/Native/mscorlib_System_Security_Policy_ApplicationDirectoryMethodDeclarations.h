﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ApplicationDirectory
struct ApplicationDirectory_t1_1326;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.ApplicationDirectory::.ctor(System.String)
extern "C" void ApplicationDirectory__ctor_m1_11324 (ApplicationDirectory_t1_1326 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationDirectory::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t ApplicationDirectory_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11325 (ApplicationDirectory_t1_1326 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationDirectory::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t ApplicationDirectory_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11326 (ApplicationDirectory_t1_1326 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationDirectory::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t ApplicationDirectory_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11327 (ApplicationDirectory_t1_1326 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.ApplicationDirectory::get_Directory()
extern "C" String_t* ApplicationDirectory_get_Directory_m1_11328 (ApplicationDirectory_t1_1326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.ApplicationDirectory::Copy()
extern "C" Object_t * ApplicationDirectory_Copy_m1_11329 (ApplicationDirectory_t1_1326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationDirectory::Equals(System.Object)
extern "C" bool ApplicationDirectory_Equals_m1_11330 (ApplicationDirectory_t1_1326 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationDirectory::GetHashCode()
extern "C" int32_t ApplicationDirectory_GetHashCode_m1_11331 (ApplicationDirectory_t1_1326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.ApplicationDirectory::ToString()
extern "C" String_t* ApplicationDirectory_ToString_m1_11332 (ApplicationDirectory_t1_1326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationDirectory::ThrowOnInvalid(System.String)
extern "C" void ApplicationDirectory_ThrowOnInvalid_m1_11333 (ApplicationDirectory_t1_1326 * __this, String_t* ___appdir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
