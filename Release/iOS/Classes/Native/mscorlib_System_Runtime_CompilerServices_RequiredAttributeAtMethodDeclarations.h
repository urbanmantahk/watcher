﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.RequiredAttributeAttribute
struct RequiredAttributeAttribute_t1_47;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.RequiredAttributeAttribute::.ctor(System.Type)
extern "C" void RequiredAttributeAttribute__ctor_m1_1307 (RequiredAttributeAttribute_t1_47 * __this, Type_t * ___requiredContract, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.CompilerServices.RequiredAttributeAttribute::get_RequiredContract()
extern "C" Type_t * RequiredAttributeAttribute_get_RequiredContract_m1_1308 (RequiredAttributeAttribute_t1_47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
