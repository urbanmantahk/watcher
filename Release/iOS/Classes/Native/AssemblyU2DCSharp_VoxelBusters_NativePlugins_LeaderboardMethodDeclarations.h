﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Leaderboard
struct Leaderboard_t8_180;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.Score[]
struct ScoreU5BU5D_t8_230;
// VoxelBusters.NativePlugins.Score
struct Score_t8_231;
// VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion
struct LoadScoreCompletion_t8_229;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardUse.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardTim.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eLeaderboardPag.h"

// System.Void VoxelBusters.NativePlugins.Leaderboard::.ctor()
extern "C" void Leaderboard__ctor_m8_1285 (Leaderboard_t8_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::.ctor(System.String,System.String,System.String,VoxelBusters.NativePlugins.eLeaderboardUserScope,VoxelBusters.NativePlugins.eLeaderboardTimeScope,System.Int32,VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score)
extern "C" void Leaderboard__ctor_m8_1286 (Leaderboard_t8_180 * __this, String_t* ____globalIdentifer, String_t* ____identifier, String_t* ____title, int32_t ____userScope, int32_t ____timeScope, int32_t ____maxResults, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::add_LoadScoreFinishedEvent(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_add_LoadScoreFinishedEvent_m8_1287 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::remove_LoadScoreFinishedEvent(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_remove_LoadScoreFinishedEvent_m8_1288 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Leaderboard::get_GlobalIdentifier()
extern "C" String_t* Leaderboard_get_GlobalIdentifier_m8_1289 (Leaderboard_t8_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::set_GlobalIdentifier(System.String)
extern "C" void Leaderboard_set_GlobalIdentifier_m8_1290 (Leaderboard_t8_180 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelBusters.NativePlugins.Leaderboard::get_MaxResults()
extern "C" int32_t Leaderboard_get_MaxResults_m8_1291 (Leaderboard_t8_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::set_MaxResults(System.Int32)
extern "C" void Leaderboard_set_MaxResults_m8_1292 (Leaderboard_t8_180 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadTopScores(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_LoadTopScores_m8_1293 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadPlayerCenteredScores(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_LoadPlayerCenteredScores_m8_1294 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadMoreScores(VoxelBusters.NativePlugins.eLeaderboardPageDirection,VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_LoadMoreScores_m8_1295 (Leaderboard_t8_180 * __this, int32_t ____pageDirection, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.Leaderboard::ToString()
extern "C" String_t* Leaderboard_ToString_m8_1296 (Leaderboard_t8_180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::SetLoadScoreFinishedEvent(VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion)
extern "C" void Leaderboard_SetLoadScoreFinishedEvent_m8_1297 (Leaderboard_t8_180 * __this, LoadScoreCompletion_t8_229 * ____onCompletion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadScoresFinished(System.Collections.IDictionary)
extern "C" void Leaderboard_LoadScoresFinished_m8_1298 (Leaderboard_t8_180 * __this, Object_t * ____dataDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard::LoadScoresFinished(VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score,System.String)
extern "C" void Leaderboard_LoadScoresFinished_m8_1299 (Leaderboard_t8_180 * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
