﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Boolean[]
struct BooleanU5BU5D_t1_458;

#include "mscorlib_System_Security_CodeAccessPermission.h"

// System.Security.Permissions.SiteIdentityPermission
struct  SiteIdentityPermission_t1_1311  : public CodeAccessPermission_t1_1268
{
	// System.String System.Security.Permissions.SiteIdentityPermission::_site
	String_t* ____site_1;
};
struct SiteIdentityPermission_t1_1311_StaticFields{
	// System.Boolean[] System.Security.Permissions.SiteIdentityPermission::valid
	BooleanU5BU5D_t1_458* ___valid_2;
};
