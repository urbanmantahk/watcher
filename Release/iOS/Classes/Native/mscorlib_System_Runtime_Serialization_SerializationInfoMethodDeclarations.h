﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Type
struct Type_t;
// System.Runtime.Serialization.SerializationEntry[]
struct SerializationEntryU5BU5D_t1_1727;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1_1095;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Runtime.Serialization.SerializationInfoEnumerator
struct SerializationInfoEnumerator_t1_1096;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Serialization.SerializationInfo::.ctor(System.Type)
extern "C" void SerializationInfo__ctor_m1_9616 (SerializationInfo_t1_293 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::.ctor(System.Type,System.Runtime.Serialization.SerializationEntry[])
extern "C" void SerializationInfo__ctor_m1_9617 (SerializationInfo_t1_293 * __this, Type_t * ___type, SerializationEntryU5BU5D_t1_1727* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::.ctor(System.Type,System.Runtime.Serialization.IFormatterConverter)
extern "C" void SerializationInfo__ctor_m1_9618 (SerializationInfo_t1_293 * __this, Type_t * ___type, Object_t * ___converter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.SerializationInfo::get_AssemblyName()
extern "C" String_t* SerializationInfo_get_AssemblyName_m1_9619 (SerializationInfo_t1_293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::set_AssemblyName(System.String)
extern "C" void SerializationInfo_set_AssemblyName_m1_9620 (SerializationInfo_t1_293 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.SerializationInfo::get_FullTypeName()
extern "C" String_t* SerializationInfo_get_FullTypeName_m1_9621 (SerializationInfo_t1_293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::set_FullTypeName(System.String)
extern "C" void SerializationInfo_set_FullTypeName_m1_9622 (SerializationInfo_t1_293 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.SerializationInfo::get_MemberCount()
extern "C" int32_t SerializationInfo_get_MemberCount_m1_9623 (SerializationInfo_t1_293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object,System.Type)
extern "C" void SerializationInfo_AddValue_m1_9624 (SerializationInfo_t1_293 * __this, String_t* ___name, Object_t * ___value, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
extern "C" Object_t * SerializationInfo_GetValue_m1_9625 (SerializationInfo_t1_293 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::SetType(System.Type)
extern "C" void SerializationInfo_SetType_m1_9626 (SerializationInfo_t1_293 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationInfoEnumerator System.Runtime.Serialization.SerializationInfo::GetEnumerator()
extern "C" SerializationInfoEnumerator_t1_1096 * SerializationInfo_GetEnumerator_m1_9627 (SerializationInfo_t1_293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int16)
extern "C" void SerializationInfo_AddValue_m1_9628 (SerializationInfo_t1_293 * __this, String_t* ___name, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt16)
extern "C" void SerializationInfo_AddValue_m1_9629 (SerializationInfo_t1_293 * __this, String_t* ___name, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int32)
extern "C" void SerializationInfo_AddValue_m1_9630 (SerializationInfo_t1_293 * __this, String_t* ___name, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Byte)
extern "C" void SerializationInfo_AddValue_m1_9631 (SerializationInfo_t1_293 * __this, String_t* ___name, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Boolean)
extern "C" void SerializationInfo_AddValue_m1_9632 (SerializationInfo_t1_293 * __this, String_t* ___name, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Char)
extern "C" void SerializationInfo_AddValue_m1_9633 (SerializationInfo_t1_293 * __this, String_t* ___name, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.SByte)
extern "C" void SerializationInfo_AddValue_m1_9634 (SerializationInfo_t1_293 * __this, String_t* ___name, int8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Double)
extern "C" void SerializationInfo_AddValue_m1_9635 (SerializationInfo_t1_293 * __this, String_t* ___name, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Decimal)
extern "C" void SerializationInfo_AddValue_m1_9636 (SerializationInfo_t1_293 * __this, String_t* ___name, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.DateTime)
extern "C" void SerializationInfo_AddValue_m1_9637 (SerializationInfo_t1_293 * __this, String_t* ___name, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Single)
extern "C" void SerializationInfo_AddValue_m1_9638 (SerializationInfo_t1_293 * __this, String_t* ___name, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt32)
extern "C" void SerializationInfo_AddValue_m1_9639 (SerializationInfo_t1_293 * __this, String_t* ___name, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Int64)
extern "C" void SerializationInfo_AddValue_m1_9640 (SerializationInfo_t1_293 * __this, String_t* ___name, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt64)
extern "C" void SerializationInfo_AddValue_m1_9641 (SerializationInfo_t1_293 * __this, String_t* ___name, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object)
extern "C" void SerializationInfo_AddValue_m1_9642 (SerializationInfo_t1_293 * __this, String_t* ___name, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Serialization.SerializationInfo::GetBoolean(System.String)
extern "C" bool SerializationInfo_GetBoolean_m1_9643 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Runtime.Serialization.SerializationInfo::GetByte(System.String)
extern "C" uint8_t SerializationInfo_GetByte_m1_9644 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Runtime.Serialization.SerializationInfo::GetChar(System.String)
extern "C" uint16_t SerializationInfo_GetChar_m1_9645 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Serialization.SerializationInfo::GetDateTime(System.String)
extern "C" DateTime_t1_150  SerializationInfo_GetDateTime_m1_9646 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Serialization.SerializationInfo::GetDecimal(System.String)
extern "C" Decimal_t1_19  SerializationInfo_GetDecimal_m1_9647 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Runtime.Serialization.SerializationInfo::GetDouble(System.String)
extern "C" double SerializationInfo_GetDouble_m1_9648 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.Runtime.Serialization.SerializationInfo::GetInt16(System.String)
extern "C" int16_t SerializationInfo_GetInt16_m1_9649 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.SerializationInfo::GetInt32(System.String)
extern "C" int32_t SerializationInfo_GetInt32_m1_9650 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Runtime.Serialization.SerializationInfo::GetInt64(System.String)
extern "C" int64_t SerializationInfo_GetInt64_m1_9651 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.Runtime.Serialization.SerializationInfo::GetSByte(System.String)
extern "C" int8_t SerializationInfo_GetSByte_m1_9652 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Runtime.Serialization.SerializationInfo::GetSingle(System.String)
extern "C" float SerializationInfo_GetSingle_m1_9653 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Serialization.SerializationInfo::GetString(System.String)
extern "C" String_t* SerializationInfo_GetString_m1_9654 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.Runtime.Serialization.SerializationInfo::GetUInt16(System.String)
extern "C" uint16_t SerializationInfo_GetUInt16_m1_9655 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Runtime.Serialization.SerializationInfo::GetUInt32(System.String)
extern "C" uint32_t SerializationInfo_GetUInt32_m1_9656 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.Runtime.Serialization.SerializationInfo::GetUInt64(System.String)
extern "C" uint64_t SerializationInfo_GetUInt64_m1_9657 (SerializationInfo_t1_293 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationEntry[] System.Runtime.Serialization.SerializationInfo::get_entries()
extern "C" SerializationEntryU5BU5D_t1_1727* SerializationInfo_get_entries_m1_9658 (SerializationInfo_t1_293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
