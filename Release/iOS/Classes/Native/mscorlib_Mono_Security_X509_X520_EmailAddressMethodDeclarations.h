﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.X509.X520/EmailAddress
struct EmailAddress_t1_208;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.X509.X520/EmailAddress::.ctor()
extern "C" void EmailAddress__ctor_m1_2408 (EmailAddress_t1_208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
