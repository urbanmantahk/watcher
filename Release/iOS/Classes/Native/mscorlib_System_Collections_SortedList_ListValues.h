﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.SortedList
struct SortedList_t1_304;

#include "mscorlib_System_Object.h"

// System.Collections.SortedList/ListValues
struct  ListValues_t1_306  : public Object_t
{
	// System.Collections.SortedList System.Collections.SortedList/ListValues::host
	SortedList_t1_304 * ___host_0;
};
