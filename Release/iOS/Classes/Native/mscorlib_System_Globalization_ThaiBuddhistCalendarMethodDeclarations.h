﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.ThaiBuddhistCalendar
struct ThaiBuddhistCalendar_t1_391;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarWeekRule.h"

// System.Void System.Globalization.ThaiBuddhistCalendar::.ctor()
extern "C" void ThaiBuddhistCalendar__ctor_m1_4523 (ThaiBuddhistCalendar_t1_391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.ThaiBuddhistCalendar::.cctor()
extern "C" void ThaiBuddhistCalendar__cctor_m1_4524 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.ThaiBuddhistCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* ThaiBuddhistCalendar_get_Eras_m1_4525 (ThaiBuddhistCalendar_t1_391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::get_TwoDigitYearMax()
extern "C" int32_t ThaiBuddhistCalendar_get_TwoDigitYearMax_m1_4526 (ThaiBuddhistCalendar_t1_391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.ThaiBuddhistCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void ThaiBuddhistCalendar_set_TwoDigitYearMax_m1_4527 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.ThaiBuddhistCalendar::M_CheckEra(System.Int32&)
extern "C" void ThaiBuddhistCalendar_M_CheckEra_m1_4528 (ThaiBuddhistCalendar_t1_391 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::M_CheckYEG(System.Int32,System.Int32&)
extern "C" int32_t ThaiBuddhistCalendar_M_CheckYEG_m1_4529 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.ThaiBuddhistCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void ThaiBuddhistCalendar_M_CheckYE_m1_4530 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::M_CheckYMEG(System.Int32,System.Int32,System.Int32&)
extern "C" int32_t ThaiBuddhistCalendar_M_CheckYMEG_m1_4531 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::M_CheckYMDEG(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" int32_t ThaiBuddhistCalendar_M_CheckYMDEG_m1_4532 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ThaiBuddhistCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  ThaiBuddhistCalendar_AddMonths_m1_4533 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ThaiBuddhistCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  ThaiBuddhistCalendar_AddYears_m1_4534 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t ThaiBuddhistCalendar_GetDayOfMonth_m1_4535 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.ThaiBuddhistCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t ThaiBuddhistCalendar_GetDayOfWeek_m1_4536 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t ThaiBuddhistCalendar_GetDayOfYear_m1_4537 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t ThaiBuddhistCalendar_GetDaysInMonth_m1_4538 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t ThaiBuddhistCalendar_GetDaysInYear_m1_4539 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetEra(System.DateTime)
extern "C" int32_t ThaiBuddhistCalendar_GetEra_m1_4540 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t ThaiBuddhistCalendar_GetLeapMonth_m1_4541 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetMonth(System.DateTime)
extern "C" int32_t ThaiBuddhistCalendar_GetMonth_m1_4542 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t ThaiBuddhistCalendar_GetMonthsInYear_m1_4543 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetWeekOfYear(System.DateTime,System.Globalization.CalendarWeekRule,System.DayOfWeek)
extern "C" int32_t ThaiBuddhistCalendar_GetWeekOfYear_m1_4544 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, int32_t ___rule, int32_t ___firstDayOfWeek, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::GetYear(System.DateTime)
extern "C" int32_t ThaiBuddhistCalendar_GetYear_m1_4545 (ThaiBuddhistCalendar_t1_391 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.ThaiBuddhistCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool ThaiBuddhistCalendar_IsLeapDay_m1_4546 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.ThaiBuddhistCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool ThaiBuddhistCalendar_IsLeapMonth_m1_4547 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.ThaiBuddhistCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool ThaiBuddhistCalendar_IsLeapYear_m1_4548 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ThaiBuddhistCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  ThaiBuddhistCalendar_ToDateTime_m1_4549 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ThaiBuddhistCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t ThaiBuddhistCalendar_ToFourDigitYear_m1_4550 (ThaiBuddhistCalendar_t1_391 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ThaiBuddhistCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  ThaiBuddhistCalendar_get_MinSupportedDateTime_m1_4551 (ThaiBuddhistCalendar_t1_391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ThaiBuddhistCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  ThaiBuddhistCalendar_get_MaxSupportedDateTime_m1_4552 (ThaiBuddhistCalendar_t1_391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
