﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Channels.ChanelSinkStackEntry
struct ChanelSinkStackEntry_t1_876;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.ClientChannelSinkStack
struct  ClientChannelSinkStack_t1_877  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Channels.ClientChannelSinkStack::_replySink
	Object_t * ____replySink_0;
	// System.Runtime.Remoting.Channels.ChanelSinkStackEntry System.Runtime.Remoting.Channels.ClientChannelSinkStack::_sinkStack
	ChanelSinkStackEntry_t1_876 * ____sinkStack_1;
};
