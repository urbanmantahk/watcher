﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_CompilerServices_CustomConstantAttri.h"

// System.Runtime.CompilerServices.IUnknownConstantAttribute
struct  IUnknownConstantAttribute_t1_689  : public CustomConstantAttribute_t1_681
{
};
