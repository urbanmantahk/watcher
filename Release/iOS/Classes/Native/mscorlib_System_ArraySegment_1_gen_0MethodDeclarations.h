﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_ArraySegment_1_gen_0.h"

// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[],System.Int32,System.Int32)
extern "C" void ArraySegment_1__ctor_m1_18086_gshared (ArraySegment_1_t1_2214 * __this, ByteU5BU5D_t1_109* ___array, int32_t ___offset, int32_t ___count, const MethodInfo* method);
#define ArraySegment_1__ctor_m1_18086(__this, ___array, ___offset, ___count, method) (( void (*) (ArraySegment_1_t1_2214 *, ByteU5BU5D_t1_109*, int32_t, int32_t, const MethodInfo*))ArraySegment_1__ctor_m1_18086_gshared)(__this, ___array, ___offset, ___count, method)
// System.Void System.ArraySegment`1<System.Byte>::.ctor(T[])
extern "C" void ArraySegment_1__ctor_m1_18087_gshared (ArraySegment_1_t1_2214 * __this, ByteU5BU5D_t1_109* ___array, const MethodInfo* method);
#define ArraySegment_1__ctor_m1_18087(__this, ___array, method) (( void (*) (ArraySegment_1_t1_2214 *, ByteU5BU5D_t1_109*, const MethodInfo*))ArraySegment_1__ctor_m1_18087_gshared)(__this, ___array, method)
// T[] System.ArraySegment`1<System.Byte>::get_Array()
extern "C" ByteU5BU5D_t1_109* ArraySegment_1_get_Array_m1_18088_gshared (ArraySegment_1_t1_2214 * __this, const MethodInfo* method);
#define ArraySegment_1_get_Array_m1_18088(__this, method) (( ByteU5BU5D_t1_109* (*) (ArraySegment_1_t1_2214 *, const MethodInfo*))ArraySegment_1_get_Array_m1_18088_gshared)(__this, method)
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
extern "C" int32_t ArraySegment_1_get_Offset_m1_18089_gshared (ArraySegment_1_t1_2214 * __this, const MethodInfo* method);
#define ArraySegment_1_get_Offset_m1_18089(__this, method) (( int32_t (*) (ArraySegment_1_t1_2214 *, const MethodInfo*))ArraySegment_1_get_Offset_m1_18089_gshared)(__this, method)
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
extern "C" int32_t ArraySegment_1_get_Count_m1_18090_gshared (ArraySegment_1_t1_2214 * __this, const MethodInfo* method);
#define ArraySegment_1_get_Count_m1_18090(__this, method) (( int32_t (*) (ArraySegment_1_t1_2214 *, const MethodInfo*))ArraySegment_1_get_Count_m1_18090_gshared)(__this, method)
// System.Boolean System.ArraySegment`1<System.Byte>::Equals(System.Object)
extern "C" bool ArraySegment_1_Equals_m1_18091_gshared (ArraySegment_1_t1_2214 * __this, Object_t * ___obj, const MethodInfo* method);
#define ArraySegment_1_Equals_m1_18091(__this, ___obj, method) (( bool (*) (ArraySegment_1_t1_2214 *, Object_t *, const MethodInfo*))ArraySegment_1_Equals_m1_18091_gshared)(__this, ___obj, method)
// System.Boolean System.ArraySegment`1<System.Byte>::Equals(System.ArraySegment`1<T>)
extern "C" bool ArraySegment_1_Equals_m1_18092_gshared (ArraySegment_1_t1_2214 * __this, ArraySegment_1_t1_2214  ___obj, const MethodInfo* method);
#define ArraySegment_1_Equals_m1_18092(__this, ___obj, method) (( bool (*) (ArraySegment_1_t1_2214 *, ArraySegment_1_t1_2214 , const MethodInfo*))ArraySegment_1_Equals_m1_18092_gshared)(__this, ___obj, method)
// System.Int32 System.ArraySegment`1<System.Byte>::GetHashCode()
extern "C" int32_t ArraySegment_1_GetHashCode_m1_18093_gshared (ArraySegment_1_t1_2214 * __this, const MethodInfo* method);
#define ArraySegment_1_GetHashCode_m1_18093(__this, method) (( int32_t (*) (ArraySegment_1_t1_2214 *, const MethodInfo*))ArraySegment_1_GetHashCode_m1_18093_gshared)(__this, method)
// System.Boolean System.ArraySegment`1<System.Byte>::op_Equality(System.ArraySegment`1<T>,System.ArraySegment`1<T>)
extern "C" bool ArraySegment_1_op_Equality_m1_18094_gshared (Object_t * __this /* static, unused */, ArraySegment_1_t1_2214  ___a, ArraySegment_1_t1_2214  ___b, const MethodInfo* method);
#define ArraySegment_1_op_Equality_m1_18094(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, ArraySegment_1_t1_2214 , ArraySegment_1_t1_2214 , const MethodInfo*))ArraySegment_1_op_Equality_m1_18094_gshared)(__this /* static, unused */, ___a, ___b, method)
// System.Boolean System.ArraySegment`1<System.Byte>::op_Inequality(System.ArraySegment`1<T>,System.ArraySegment`1<T>)
extern "C" bool ArraySegment_1_op_Inequality_m1_18095_gshared (Object_t * __this /* static, unused */, ArraySegment_1_t1_2214  ___a, ArraySegment_1_t1_2214  ___b, const MethodInfo* method);
#define ArraySegment_1_op_Inequality_m1_18095(__this /* static, unused */, ___a, ___b, method) (( bool (*) (Object_t * /* static, unused */, ArraySegment_1_t1_2214 , ArraySegment_1_t1_2214 , const MethodInfo*))ArraySegment_1_op_Inequality_m1_18095_gshared)(__this /* static, unused */, ___a, ___b, method)
