﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.ModuleResolveEventHandler
struct ModuleResolveEventHandler_t1_561;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Object
struct Object_t;
// System.IO.FileStream[]
struct FileStreamU5BU5D_t1_1683;
// System.IO.FileStream
struct FileStream_t1_146;
// System.Reflection.Module
struct Module_t1_495;
// System.IO.Stream
struct Stream_t1_405;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Version
struct Version_t1_578;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1_471;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Reflection.AssemblyName[]
struct AssemblyNameU5BU5D_t1_1688;
// System.Reflection.ManifestResourceInfo
struct ManifestResourceInfo_t1_606;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Security.PermissionSet
struct PermissionSet_t1_563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Assembly::.ctor()
extern "C" void Assembly__ctor_m1_6567 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::add_ModuleResolve(System.Reflection.ModuleResolveEventHandler)
extern "C" void Assembly_add_ModuleResolve_m1_6568 (Assembly_t1_467 * __this, ModuleResolveEventHandler_t1_561 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::remove_ModuleResolve(System.Reflection.ModuleResolveEventHandler)
extern "C" void Assembly_remove_ModuleResolve_m1_6569 (Assembly_t1_467 * __this, ModuleResolveEventHandler_t1_561 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_code_base(System.Boolean)
extern "C" String_t* Assembly_get_code_base_m1_6570 (Assembly_t1_467 * __this, bool ___escaped, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_fullname()
extern "C" String_t* Assembly_get_fullname_m1_6571 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_location()
extern "C" String_t* Assembly_get_location_m1_6572 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::InternalImageRuntimeVersion()
extern "C" String_t* Assembly_InternalImageRuntimeVersion_m1_6573 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::GetCodeBase(System.Boolean)
extern "C" String_t* Assembly_GetCodeBase_m1_6574 (Assembly_t1_467 * __this, bool ___escaped, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_CodeBase()
extern "C" String_t* Assembly_get_CodeBase_m1_6575 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_EscapedCodeBase()
extern "C" String_t* Assembly_get_EscapedCodeBase_m1_6576 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_FullName()
extern "C" String_t* Assembly_get_FullName_m1_6577 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Assembly::get_EntryPoint()
extern "C" MethodInfo_t * Assembly_get_EntryPoint_m1_6578 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Reflection.Assembly::get_Evidence()
extern "C" Evidence_t1_398 * Assembly_get_Evidence_m1_6579 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.Evidence System.Reflection.Assembly::UnprotectedGetEvidence()
extern "C" Evidence_t1_398 * Assembly_UnprotectedGetEvidence_m1_6580 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Assembly::get_global_assembly_cache()
extern "C" bool Assembly_get_global_assembly_cache_m1_6581 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Assembly::get_GlobalAssemblyCache()
extern "C" bool Assembly_get_GlobalAssemblyCache_m1_6582 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::set_FromByteArray(System.Boolean)
extern "C" void Assembly_set_FromByteArray_m1_6583 (Assembly_t1_467 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_Location()
extern "C" String_t* Assembly_get_Location_m1_6584 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::get_ImageRuntimeVersion()
extern "C" String_t* Assembly_get_ImageRuntimeVersion_m1_6585 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Assembly_GetObjectData_m1_6586 (Assembly_t1_467 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Assembly::IsDefined(System.Type,System.Boolean)
extern "C" bool Assembly_IsDefined_m1_6587 (Assembly_t1_467 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Assembly::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* Assembly_GetCustomAttributes_m1_6588 (Assembly_t1_467 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Assembly::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* Assembly_GetCustomAttributes_m1_6589 (Assembly_t1_467 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Assembly::GetFilesInternal(System.String,System.Boolean)
extern "C" Object_t * Assembly_GetFilesInternal_m1_6590 (Assembly_t1_467 * __this, String_t* ___name, bool ___getResourceModules, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream[] System.Reflection.Assembly::GetFiles()
extern "C" FileStreamU5BU5D_t1_1683* Assembly_GetFiles_m1_6591 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream[] System.Reflection.Assembly::GetFiles(System.Boolean)
extern "C" FileStreamU5BU5D_t1_1683* Assembly_GetFiles_m1_6592 (Assembly_t1_467 * __this, bool ___getResourceModules, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.FileStream System.Reflection.Assembly::GetFile(System.String)
extern "C" FileStream_t1_146 * Assembly_GetFile_m1_6593 (Assembly_t1_467 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Reflection.Assembly::GetManifestResourceInternal(System.String,System.Int32&,System.Reflection.Module&)
extern "C" IntPtr_t Assembly_GetManifestResourceInternal_m1_6594 (Assembly_t1_467 * __this, String_t* ___name, int32_t* ___size, Module_t1_495 ** ___module, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Reflection.Assembly::GetManifestResourceStream(System.String)
extern "C" Stream_t1_405 * Assembly_GetManifestResourceStream_m1_6595 (Assembly_t1_467 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream System.Reflection.Assembly::GetManifestResourceStream(System.Type,System.String)
extern "C" Stream_t1_405 * Assembly_GetManifestResourceStream_m1_6596 (Assembly_t1_467 * __this, Type_t * ___type, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Assembly::GetTypes(System.Boolean)
extern "C" TypeU5BU5D_t1_31* Assembly_GetTypes_m1_6597 (Assembly_t1_467 * __this, bool ___exportedOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Assembly::GetTypes()
extern "C" TypeU5BU5D_t1_31* Assembly_GetTypes_m1_6598 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Assembly::GetExportedTypes()
extern "C" TypeU5BU5D_t1_31* Assembly_GetExportedTypes_m1_6599 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Assembly::GetType(System.String,System.Boolean)
extern "C" Type_t * Assembly_GetType_m1_6600 (Assembly_t1_467 * __this, String_t* ___name, bool ___throwOnError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Assembly::GetType(System.String)
extern "C" Type_t * Assembly_GetType_m1_6601 (Assembly_t1_467 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Assembly::InternalGetType(System.Reflection.Module,System.String,System.Boolean,System.Boolean)
extern "C" Type_t * Assembly_InternalGetType_m1_6602 (Assembly_t1_467 * __this, Module_t1_495 * ___module, String_t* ___name, bool ___throwOnError, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Assembly::GetType(System.String,System.Boolean,System.Boolean)
extern "C" Type_t * Assembly_GetType_m1_6603 (Assembly_t1_467 * __this, String_t* ___name, bool ___throwOnError, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::InternalGetAssemblyName(System.String,System.Reflection.AssemblyName)
extern "C" void Assembly_InternalGetAssemblyName_m1_6604 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, AssemblyName_t1_576 * ___aname, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::FillName(System.Reflection.Assembly,System.Reflection.AssemblyName)
extern "C" void Assembly_FillName_m1_6605 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___ass, AssemblyName_t1_576 * ___aname, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Assembly::GetName(System.Boolean)
extern "C" AssemblyName_t1_576 * Assembly_GetName_m1_6606 (Assembly_t1_467 * __this, bool ___copiedName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Assembly::GetName()
extern "C" AssemblyName_t1_576 * Assembly_GetName_m1_6607 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Assembly::UnprotectedGetName()
extern "C" AssemblyName_t1_576 * Assembly_UnprotectedGetName_m1_6608 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::ToString()
extern "C" String_t* Assembly_ToString_m1_6609 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Assembly::CreateQualifiedName(System.String,System.String)
extern "C" String_t* Assembly_CreateQualifiedName_m1_6610 (Object_t * __this /* static, unused */, String_t* ___assemblyName, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetAssembly(System.Type)
extern "C" Assembly_t1_467 * Assembly_GetAssembly_m1_6611 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetEntryAssembly()
extern "C" Assembly_t1_467 * Assembly_GetEntryAssembly_m1_6612 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetSatelliteAssembly(System.Globalization.CultureInfo)
extern "C" Assembly_t1_467 * Assembly_GetSatelliteAssembly_m1_6613 (Assembly_t1_467 * __this, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetSatelliteAssembly(System.Globalization.CultureInfo,System.Version)
extern "C" Assembly_t1_467 * Assembly_GetSatelliteAssembly_m1_6614 (Assembly_t1_467 * __this, CultureInfo_t1_277 * ___culture, Version_t1_578 * ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetSatelliteAssemblyNoThrow(System.Globalization.CultureInfo,System.Version)
extern "C" Assembly_t1_467 * Assembly_GetSatelliteAssemblyNoThrow_m1_6615 (Assembly_t1_467 * __this, CultureInfo_t1_277 * ___culture, Version_t1_578 * ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetSatelliteAssembly(System.Globalization.CultureInfo,System.Version,System.Boolean)
extern "C" Assembly_t1_467 * Assembly_GetSatelliteAssembly_m1_6616 (Assembly_t1_467 * __this, CultureInfo_t1_277 * ___culture, Version_t1_578 * ___version, bool ___throwOnError, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadFrom(System.String,System.Boolean)
extern "C" Assembly_t1_467 * Assembly_LoadFrom_m1_6617 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, bool ___refonly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadFrom(System.String)
extern "C" Assembly_t1_467 * Assembly_LoadFrom_m1_6618 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadFrom(System.String,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_LoadFrom_m1_6619 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, Evidence_t1_398 * ___securityEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadFrom(System.String,System.Security.Policy.Evidence,System.Byte[],System.Configuration.Assemblies.AssemblyHashAlgorithm)
extern "C" Assembly_t1_467 * Assembly_LoadFrom_m1_6620 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, Evidence_t1_398 * ___securityEvidence, ByteU5BU5D_t1_109* ___hashValue, int32_t ___hashAlgorithm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadFile(System.String,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_LoadFile_m1_6621 (Object_t * __this /* static, unused */, String_t* ___path, Evidence_t1_398 * ___securityEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadFile(System.String)
extern "C" Assembly_t1_467 * Assembly_LoadFile_m1_6622 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.String)
extern "C" Assembly_t1_467 * Assembly_Load_m1_6623 (Object_t * __this /* static, unused */, String_t* ___assemblyString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.String,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_Load_m1_6624 (Object_t * __this /* static, unused */, String_t* ___assemblyString, Evidence_t1_398 * ___assemblySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.Reflection.AssemblyName)
extern "C" Assembly_t1_467 * Assembly_Load_m1_6625 (Object_t * __this /* static, unused */, AssemblyName_t1_576 * ___assemblyRef, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.Reflection.AssemblyName,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_Load_m1_6626 (Object_t * __this /* static, unused */, AssemblyName_t1_576 * ___assemblyRef, Evidence_t1_398 * ___assemblySecurity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.Byte[])
extern "C" Assembly_t1_467 * Assembly_Load_m1_6627 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___rawAssembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.Byte[],System.Byte[])
extern "C" Assembly_t1_467 * Assembly_Load_m1_6628 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___rawAssembly, ByteU5BU5D_t1_109* ___rawSymbolStore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::Load(System.Byte[],System.Byte[],System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_Load_m1_6629 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___rawAssembly, ByteU5BU5D_t1_109* ___rawSymbolStore, Evidence_t1_398 * ___securityEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::ReflectionOnlyLoad(System.Byte[])
extern "C" Assembly_t1_467 * Assembly_ReflectionOnlyLoad_m1_6630 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___rawAssembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::ReflectionOnlyLoad(System.String)
extern "C" Assembly_t1_467 * Assembly_ReflectionOnlyLoad_m1_6631 (Object_t * __this /* static, unused */, String_t* ___assemblyString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::ReflectionOnlyLoadFrom(System.String)
extern "C" Assembly_t1_467 * Assembly_ReflectionOnlyLoadFrom_m1_6632 (Object_t * __this /* static, unused */, String_t* ___assemblyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadWithPartialName(System.String)
extern "C" Assembly_t1_467 * Assembly_LoadWithPartialName_m1_6633 (Object_t * __this /* static, unused */, String_t* ___partialName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Assembly::LoadModule(System.String,System.Byte[])
extern "C" Module_t1_495 * Assembly_LoadModule_m1_6634 (Assembly_t1_467 * __this, String_t* ___moduleName, ByteU5BU5D_t1_109* ___rawModule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Assembly::LoadModule(System.String,System.Byte[],System.Byte[])
extern "C" Module_t1_495 * Assembly_LoadModule_m1_6635 (Assembly_t1_467 * __this, String_t* ___moduleName, ByteU5BU5D_t1_109* ___rawModule, ByteU5BU5D_t1_109* ___rawSymbolStore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::load_with_partial_name(System.String,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_load_with_partial_name_m1_6636 (Object_t * __this /* static, unused */, String_t* ___name, Evidence_t1_398 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadWithPartialName(System.String,System.Security.Policy.Evidence)
extern "C" Assembly_t1_467 * Assembly_LoadWithPartialName_m1_6637 (Object_t * __this /* static, unused */, String_t* ___partialName, Evidence_t1_398 * ___securityEvidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::LoadWithPartialName(System.String,System.Security.Policy.Evidence,System.Boolean)
extern "C" Assembly_t1_467 * Assembly_LoadWithPartialName_m1_6638 (Object_t * __this /* static, unused */, String_t* ___partialName, Evidence_t1_398 * ___securityEvidence, bool ___oldBehavior, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Assembly::CreateInstance(System.String)
extern "C" Object_t * Assembly_CreateInstance_m1_6639 (Assembly_t1_467 * __this, String_t* ___typeName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Assembly::CreateInstance(System.String,System.Boolean)
extern "C" Object_t * Assembly_CreateInstance_m1_6640 (Assembly_t1_467 * __this, String_t* ___typeName, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Assembly::CreateInstance(System.String,System.Boolean,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo,System.Object[])
extern "C" Object_t * Assembly_CreateInstance_m1_6641 (Assembly_t1_467 * __this, String_t* ___typeName, bool ___ignoreCase, int32_t ___bindingAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___args, CultureInfo_t1_277 * ___culture, ObjectU5BU5D_t1_272* ___activationAttributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Assembly::GetLoadedModules()
extern "C" ModuleU5BU5D_t1_471* Assembly_GetLoadedModules_m1_6642 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Assembly::GetLoadedModules(System.Boolean)
extern "C" ModuleU5BU5D_t1_471* Assembly_GetLoadedModules_m1_6643 (Assembly_t1_467 * __this, bool ___getResourceModules, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Assembly::GetModules()
extern "C" ModuleU5BU5D_t1_471* Assembly_GetModules_m1_6644 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Assembly::GetModule(System.String)
extern "C" Module_t1_495 * Assembly_GetModule_m1_6645 (Assembly_t1_467 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Assembly::GetModulesInternal()
extern "C" ModuleU5BU5D_t1_471* Assembly_GetModulesInternal_m1_6646 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Assembly::GetModules(System.Boolean)
extern "C" ModuleU5BU5D_t1_471* Assembly_GetModules_m1_6647 (Assembly_t1_467 * __this, bool ___getResourceModules, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Reflection.Assembly::GetNamespaces()
extern "C" StringU5BU5D_t1_238* Assembly_GetNamespaces_m1_6648 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Reflection.Assembly::GetManifestResourceNames()
extern "C" StringU5BU5D_t1_238* Assembly_GetManifestResourceNames_m1_6649 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetExecutingAssembly()
extern "C" Assembly_t1_467 * Assembly_GetExecutingAssembly_m1_6650 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Assembly::GetCallingAssembly()
extern "C" Assembly_t1_467 * Assembly_GetCallingAssembly_m1_6651 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName[] System.Reflection.Assembly::GetReferencedAssemblies()
extern "C" AssemblyNameU5BU5D_t1_1688* Assembly_GetReferencedAssemblies_m1_6652 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Assembly::GetManifestResourceInfoInternal(System.String,System.Reflection.ManifestResourceInfo)
extern "C" bool Assembly_GetManifestResourceInfoInternal_m1_6653 (Assembly_t1_467 * __this, String_t* ___name, ManifestResourceInfo_t1_606 * ___info, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ManifestResourceInfo System.Reflection.Assembly::GetManifestResourceInfo(System.String)
extern "C" ManifestResourceInfo_t1_606 * Assembly_GetManifestResourceInfo_m1_6654 (Assembly_t1_467 * __this, String_t* ___resourceName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Assembly::MonoDebugger_GetMethodToken(System.Reflection.MethodBase)
extern "C" int32_t Assembly_MonoDebugger_GetMethodToken_m1_6655 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Reflection.Assembly::get_HostContext()
extern "C" int64_t Assembly_get_HostContext_m1_6656 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Assembly::get_ManifestModule()
extern "C" Module_t1_495 * Assembly_get_ManifestModule_m1_6657 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Assembly::GetManifestModule()
extern "C" Module_t1_495 * Assembly_GetManifestModule_m1_6658 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Assembly::GetManifestModuleInternal()
extern "C" Module_t1_495 * Assembly_GetManifestModuleInternal_m1_6659 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Assembly::get_ReflectionOnly()
extern "C" bool Assembly_get_ReflectionOnly_m1_6660 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::Resolve()
extern "C" void Assembly_Resolve_m1_6661 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Reflection.Assembly::get_GrantedPermissionSet()
extern "C" PermissionSet_t1_563 * Assembly_get_GrantedPermissionSet_m1_6662 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Reflection.Assembly::get_DeniedPermissionSet()
extern "C" PermissionSet_t1_563 * Assembly_get_DeniedPermissionSet_m1_6663 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Assembly::LoadPermissions(System.Reflection.Assembly,System.IntPtr&,System.Int32&,System.IntPtr&,System.Int32&,System.IntPtr&,System.Int32&)
extern "C" bool Assembly_LoadPermissions_m1_6664 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___a, IntPtr_t* ___minimum, int32_t* ___minLength, IntPtr_t* ___optional, int32_t* ___optLength, IntPtr_t* ___refused, int32_t* ___refLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly::LoadAssemblyPermissions()
extern "C" void Assembly_LoadAssemblyPermissions_m1_6665 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Assembly::System.Runtime.InteropServices._Assembly.GetType()
extern "C" Type_t * Assembly_System_Runtime_InteropServices__Assembly_GetType_m1_6666 (Assembly_t1_467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
