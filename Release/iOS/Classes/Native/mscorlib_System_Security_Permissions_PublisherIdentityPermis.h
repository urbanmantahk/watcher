﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;

#include "mscorlib_System_Security_CodeAccessPermission.h"

// System.Security.Permissions.PublisherIdentityPermission
struct  PublisherIdentityPermission_t1_1299  : public CodeAccessPermission_t1_1268
{
	// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Permissions.PublisherIdentityPermission::x509
	X509Certificate_t1_1179 * ___x509_1;
};
