﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void CacheIndex_t6_81_marshal(const CacheIndex_t6_81& unmarshaled, CacheIndex_t6_81_marshaled& marshaled);
extern "C" void CacheIndex_t6_81_marshal_back(const CacheIndex_t6_81_marshaled& marshaled, CacheIndex_t6_81& unmarshaled);
extern "C" void CacheIndex_t6_81_marshal_cleanup(CacheIndex_t6_81_marshaled& marshaled);
