﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>
struct ExifEnumProperty_1_t8_354;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_WhiteBalance.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_1991_gshared (ExifEnumProperty_1_t8_354 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1991(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_354 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1991_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_2216_gshared (ExifEnumProperty_1_t8_354 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2216(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_354 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2216_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2217_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2217(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_354 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2217_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2218_gshared (ExifEnumProperty_1_t8_354 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2218(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_354 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2218_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2219_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2219(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_354 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2219_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2220_gshared (ExifEnumProperty_1_t8_354 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2220(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_354 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2220_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2221_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2221(__this, method) (( bool (*) (ExifEnumProperty_1_t8_354 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2221_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2222_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2222(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_354 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2222_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2223_gshared (ExifEnumProperty_1_t8_354 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2223(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_354 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2223_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.WhiteBalance>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2224_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_354 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2224(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_354 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2224_gshared)(__this /* static, unused */, ___obj, method)
