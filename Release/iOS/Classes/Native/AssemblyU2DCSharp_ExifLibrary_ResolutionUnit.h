﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_ExifLibrary_ResolutionUnit.h"

// ExifLibrary.ResolutionUnit
struct  ResolutionUnit_t8_68 
{
	// System.UInt16 ExifLibrary.ResolutionUnit::value__
	uint16_t ___value___1;
};
