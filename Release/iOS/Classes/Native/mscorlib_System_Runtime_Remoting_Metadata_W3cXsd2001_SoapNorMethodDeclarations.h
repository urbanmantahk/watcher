﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString
struct SoapNormalizedString_t1_989;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::.ctor()
extern "C" void SoapNormalizedString__ctor_m1_8856 (SoapNormalizedString_t1_989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::.ctor(System.String)
extern "C" void SoapNormalizedString__ctor_m1_8857 (SoapNormalizedString_t1_989 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::get_Value()
extern "C" String_t* SoapNormalizedString_get_Value_m1_8858 (SoapNormalizedString_t1_989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::set_Value(System.String)
extern "C" void SoapNormalizedString_set_Value_m1_8859 (SoapNormalizedString_t1_989 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::get_XsdType()
extern "C" String_t* SoapNormalizedString_get_XsdType_m1_8860 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::GetXsdType()
extern "C" String_t* SoapNormalizedString_GetXsdType_m1_8861 (SoapNormalizedString_t1_989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::Parse(System.String)
extern "C" SoapNormalizedString_t1_989 * SoapNormalizedString_Parse_m1_8862 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNormalizedString::ToString()
extern "C" String_t* SoapNormalizedString_ToString_m1_8863 (SoapNormalizedString_t1_989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
