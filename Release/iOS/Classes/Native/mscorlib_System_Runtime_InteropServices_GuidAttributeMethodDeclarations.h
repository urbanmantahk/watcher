﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_t1_48;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
extern "C" void GuidAttribute__ctor_m1_1309 (GuidAttribute_t1_48 * __this, String_t* ___guid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.GuidAttribute::get_Value()
extern "C" String_t* GuidAttribute_get_Value_m1_1310 (GuidAttribute_t1_48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
