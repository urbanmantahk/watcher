﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu
struct GUISubMenu_t8_143;
// UnityEngine.GameObject
struct GameObject_t6_97;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::.ctor()
extern "C" void GUISubMenu__ctor_m8_838 (GUISubMenu_t8_143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::get_CachedGameObject()
extern "C" GameObject_t6_97 * GUISubMenu_get_CachedGameObject_m8_839 (GUISubMenu_t8_143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::get_SubMenuName()
extern "C" String_t* GUISubMenu_get_SubMenuName_m8_840 (GUISubMenu_t8_143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::OnGUI()
extern "C" void GUISubMenu_OnGUI_m8_841 (GUISubMenu_t8_143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::SetActive(System.Boolean)
extern "C" void GUISubMenu_SetActive_m8_842 (GUISubMenu_t8_143 * __this, bool ____newState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::IsActive()
extern "C" bool GUISubMenu_IsActive_m8_843 (GUISubMenu_t8_143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.UnityGUI.MENU.GUISubMenu::OnPressingBackButton()
extern "C" void GUISubMenu_OnPressingBackButton_m8_844 (GUISubMenu_t8_143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
