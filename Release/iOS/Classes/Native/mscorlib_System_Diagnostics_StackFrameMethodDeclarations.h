﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.StackFrame
struct StackFrame_t1_334;
// System.String
struct String_t;
// System.Reflection.MethodBase
struct MethodBase_t1_335;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.StackFrame::.ctor()
extern "C" void StackFrame__ctor_m1_3622 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackFrame::.ctor(System.Boolean)
extern "C" void StackFrame__ctor_m1_3623 (StackFrame_t1_334 * __this, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackFrame::.ctor(System.Int32)
extern "C" void StackFrame__ctor_m1_3624 (StackFrame_t1_334 * __this, int32_t ___skipFrames, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackFrame::.ctor(System.Int32,System.Boolean)
extern "C" void StackFrame__ctor_m1_3625 (StackFrame_t1_334 * __this, int32_t ___skipFrames, bool ___fNeedFileInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackFrame::.ctor(System.String,System.Int32)
extern "C" void StackFrame__ctor_m1_3626 (StackFrame_t1_334 * __this, String_t* ___fileName, int32_t ___lineNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.StackFrame::.ctor(System.String,System.Int32,System.Int32)
extern "C" void StackFrame__ctor_m1_3627 (StackFrame_t1_334 * __this, String_t* ___fileName, int32_t ___lineNumber, int32_t ___colNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.StackFrame::get_frame_info(System.Int32,System.Boolean,System.Reflection.MethodBase&,System.Int32&,System.Int32&,System.String&,System.Int32&,System.Int32&)
extern "C" bool StackFrame_get_frame_info_m1_3628 (Object_t * __this /* static, unused */, int32_t ___skip, bool ___needFileInfo, MethodBase_t1_335 ** ___method, int32_t* ___iloffset, int32_t* ___native_offset, String_t** ___file, int32_t* ___line, int32_t* ___column, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber()
extern "C" int32_t StackFrame_GetFileLineNumber_m1_3629 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.StackFrame::GetFileColumnNumber()
extern "C" int32_t StackFrame_GetFileColumnNumber_m1_3630 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.StackFrame::GetFileName()
extern "C" String_t* StackFrame_GetFileName_m1_3631 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.StackFrame::GetSecureFileName()
extern "C" String_t* StackFrame_GetSecureFileName_m1_3632 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.StackFrame::GetILOffset()
extern "C" int32_t StackFrame_GetILOffset_m1_3633 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod()
extern "C" MethodBase_t1_335 * StackFrame_GetMethod_m1_3634 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Diagnostics.StackFrame::GetNativeOffset()
extern "C" int32_t StackFrame_GetNativeOffset_m1_3635 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.StackFrame::GetInternalMethodName()
extern "C" String_t* StackFrame_GetInternalMethodName_m1_3636 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Diagnostics.StackFrame::ToString()
extern "C" String_t* StackFrame_ToString_m1_3637 (StackFrame_t1_334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
