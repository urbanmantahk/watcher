﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Globalization_Calendar.h"
#include "mscorlib_System_DateTime.h"

// System.Globalization.JulianCalendar
struct  JulianCalendar_t1_380  : public Calendar_t1_338
{
};
struct JulianCalendar_t1_380_StaticFields{
	// System.Int32 System.Globalization.JulianCalendar::JulianEra
	int32_t ___JulianEra_7;
	// System.DateTime System.Globalization.JulianCalendar::JulianMin
	DateTime_t1_150  ___JulianMin_8;
	// System.DateTime System.Globalization.JulianCalendar::JulianMax
	DateTime_t1_150  ___JulianMax_9;
};
