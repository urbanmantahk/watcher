﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct List_1_t1_1908;

#include "mscorlib_System_ValueType.h"
#include "AssemblyU2DCSharp_VoxelBusters_DebugPRO_Internal_ConsoleLog.h"

// System.Collections.Generic.List`1/Enumerator<VoxelBusters.DebugPRO.Internal.ConsoleLog>
struct  Enumerator_t1_2721 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1_1908 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	ConsoleLog_t8_170  ___current_3;
};
