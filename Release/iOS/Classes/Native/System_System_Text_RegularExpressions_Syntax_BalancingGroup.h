﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Syntax.CapturingGroup
struct CapturingGroup_t3_217;

#include "System_System_Text_RegularExpressions_Syntax_CapturingGroup.h"

// System.Text.RegularExpressions.Syntax.BalancingGroup
struct  BalancingGroup_t3_218  : public CapturingGroup_t3_217
{
	// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.BalancingGroup::balance
	CapturingGroup_t3_217 * ___balance_3;
};
