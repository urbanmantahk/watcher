﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.ApplicationSettings/Features
struct  Features_t8_318  : public Object_t
{
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesAddressBook
	bool ___m_usesAddressBook_0;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesBilling
	bool ___m_usesBilling_1;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesGameServices
	bool ___m_usesGameServices_2;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesMediaLibrary
	bool ___m_usesMediaLibrary_3;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesNetworkConnectivity
	bool ___m_usesNetworkConnectivity_4;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesNotificationService
	bool ___m_usesNotificationService_5;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesSharing
	bool ___m_usesSharing_6;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesTwitter
	bool ___m_usesTwitter_7;
	// System.Boolean VoxelBusters.NativePlugins.ApplicationSettings/Features::m_usesWebView
	bool ___m_usesWebView_8;
};
