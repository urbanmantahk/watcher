﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.Prime.Generator.NextPrimeFinder
struct NextPrimeFinder_t1_132;
// Mono.Math.BigInteger
struct BigInteger_t1_139;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Math.Prime.Generator.NextPrimeFinder::.ctor()
extern "C" void NextPrimeFinder__ctor_m1_1744 (NextPrimeFinder_t1_132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.Prime.Generator.NextPrimeFinder::GenerateSearchBase(System.Int32,System.Object)
extern "C" BigInteger_t1_139 * NextPrimeFinder_GenerateSearchBase_m1_1745 (NextPrimeFinder_t1_132 * __this, int32_t ___bits, Object_t * ___Context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
