﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t4_41;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdAnyURI::.ctor()
extern "C" void XsdAnyURI__ctor_m4_60 (XsdAnyURI_t4_41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdAnyURI::get_TokenizedType()
extern "C" int32_t XsdAnyURI_get_TokenizedType_m4_61 (XsdAnyURI_t4_41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
