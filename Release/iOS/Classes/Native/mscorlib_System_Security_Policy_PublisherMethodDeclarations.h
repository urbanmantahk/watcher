﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.Publisher
struct Publisher_t1_1359;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.Object
struct Object_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.Publisher::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" void Publisher__ctor_m1_11634 (Publisher_t1_1359 * __this, X509Certificate_t1_1179 * ___cert, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Publisher::System.Security.Policy.IBuiltInEvidence.GetRequiredSize(System.Boolean)
extern "C" int32_t Publisher_System_Security_Policy_IBuiltInEvidence_GetRequiredSize_m1_11635 (Publisher_t1_1359 * __this, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Publisher::System.Security.Policy.IBuiltInEvidence.InitFromBuffer(System.Char[],System.Int32)
extern "C" int32_t Publisher_System_Security_Policy_IBuiltInEvidence_InitFromBuffer_m1_11636 (Publisher_t1_1359 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Publisher::System.Security.Policy.IBuiltInEvidence.OutputToBuffer(System.Char[],System.Int32,System.Boolean)
extern "C" int32_t Publisher_System_Security_Policy_IBuiltInEvidence_OutputToBuffer_m1_11637 (Publisher_t1_1359 * __this, CharU5BU5D_t1_16* ___buffer, int32_t ___position, bool ___verbose, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Policy.Publisher::get_Certificate()
extern "C" X509Certificate_t1_1179 * Publisher_get_Certificate_m1_11638 (Publisher_t1_1359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.Publisher::Copy()
extern "C" Object_t * Publisher_Copy_m1_11639 (Publisher_t1_1359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Policy.Publisher::CreateIdentityPermission(System.Security.Policy.Evidence)
extern "C" Object_t * Publisher_CreateIdentityPermission_m1_11640 (Publisher_t1_1359 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.Publisher::Equals(System.Object)
extern "C" bool Publisher_Equals_m1_11641 (Publisher_t1_1359 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.Publisher::GetHashCode()
extern "C" int32_t Publisher_GetHashCode_m1_11642 (Publisher_t1_1359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Publisher::ToString()
extern "C" String_t* Publisher_ToString_m1_11643 (Publisher_t1_1359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
