﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.MethodBody
struct MethodBody_t1_610;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Void System.Reflection.MethodBase::.ctor()
extern "C" void MethodBase__ctor_m1_6893 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodBase::System.Runtime.InteropServices._MethodBase.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodBase_System_Runtime_InteropServices__MethodBase_GetIDsOfNames_m1_6894 (MethodBase_t1_335 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodBase::System.Runtime.InteropServices._MethodBase.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void MethodBase_System_Runtime_InteropServices__MethodBase_GetTypeInfo_m1_6895 (MethodBase_t1_335 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodBase::System.Runtime.InteropServices._MethodBase.GetTypeInfoCount(System.UInt32&)
extern "C" void MethodBase_System_Runtime_InteropServices__MethodBase_GetTypeInfoCount_m1_6896 (MethodBase_t1_335 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.MethodBase::System.Runtime.InteropServices._MethodBase.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void MethodBase_System_Runtime_InteropServices__MethodBase_Invoke_m1_6897 (MethodBase_t1_335 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.MethodBase::GetCurrentMethod()
extern "C" MethodBase_t1_335 * MethodBase_GetCurrentMethod_m1_6898 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleNoGenericCheck(System.RuntimeMethodHandle)
extern "C" MethodBase_t1_335 * MethodBase_GetMethodFromHandleNoGenericCheck_m1_6899 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromIntPtr(System.IntPtr,System.IntPtr)
extern "C" MethodBase_t1_335 * MethodBase_GetMethodFromIntPtr_m1_6900 (Object_t * __this /* static, unused */, IntPtr_t ___handle, IntPtr_t ___declaringType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandle(System.RuntimeMethodHandle)
extern "C" MethodBase_t1_335 * MethodBase_GetMethodFromHandle_m1_6901 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandleInternalType(System.IntPtr,System.IntPtr)
extern "C" MethodBase_t1_335 * MethodBase_GetMethodFromHandleInternalType_m1_6902 (Object_t * __this /* static, unused */, IntPtr_t ___method_handle, IntPtr_t ___type_handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.MethodBase::GetMethodFromHandle(System.RuntimeMethodHandle,System.RuntimeTypeHandle)
extern "C" MethodBase_t1_335 * MethodBase_GetMethodFromHandle_m1_6903 (Object_t * __this /* static, unused */, RuntimeMethodHandle_t1_479  ___handle, RuntimeTypeHandle_t1_30  ___declaringType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.MethodBase::GetParameterCount()
extern "C" int32_t MethodBase_GetParameterCount_m1_6904 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[])
extern "C" Object_t * MethodBase_Invoke_m1_6905 (MethodBase_t1_335 * __this, Object_t * ___obj, ObjectU5BU5D_t1_272* ___parameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CallingConventions System.Reflection.MethodBase::get_CallingConvention()
extern "C" int32_t MethodBase_get_CallingConvention_m1_6906 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsPublic()
extern "C" bool MethodBase_get_IsPublic_m1_6907 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsPrivate()
extern "C" bool MethodBase_get_IsPrivate_m1_6908 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsFamily()
extern "C" bool MethodBase_get_IsFamily_m1_6909 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsAssembly()
extern "C" bool MethodBase_get_IsAssembly_m1_6910 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsFamilyAndAssembly()
extern "C" bool MethodBase_get_IsFamilyAndAssembly_m1_6911 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsFamilyOrAssembly()
extern "C" bool MethodBase_get_IsFamilyOrAssembly_m1_6912 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsStatic()
extern "C" bool MethodBase_get_IsStatic_m1_6913 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsFinal()
extern "C" bool MethodBase_get_IsFinal_m1_6914 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsVirtual()
extern "C" bool MethodBase_get_IsVirtual_m1_6915 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsHideBySig()
extern "C" bool MethodBase_get_IsHideBySig_m1_6916 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsAbstract()
extern "C" bool MethodBase_get_IsAbstract_m1_6917 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsSpecialName()
extern "C" bool MethodBase_get_IsSpecialName_m1_6918 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsConstructor()
extern "C" bool MethodBase_get_IsConstructor_m1_6919 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.MethodBase::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C" int32_t MethodBase_get_next_table_index_m1_6920 (MethodBase_t1_335 * __this, Object_t * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.MethodBase::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* MethodBase_GetGenericArguments_m1_6921 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_ContainsGenericParameters()
extern "C" bool MethodBase_get_ContainsGenericParameters_m1_6922 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethodDefinition()
extern "C" bool MethodBase_get_IsGenericMethodDefinition_m1_6923 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.MethodBase::get_IsGenericMethod()
extern "C" bool MethodBase_get_IsGenericMethod_m1_6924 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBody System.Reflection.MethodBase::GetMethodBodyInternal(System.IntPtr)
extern "C" MethodBody_t1_610 * MethodBase_GetMethodBodyInternal_m1_6925 (Object_t * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBody System.Reflection.MethodBase::GetMethodBody(System.IntPtr)
extern "C" MethodBody_t1_610 * MethodBase_GetMethodBody_m1_6926 (Object_t * __this /* static, unused */, IntPtr_t ___handle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBody System.Reflection.MethodBase::GetMethodBody()
extern "C" MethodBody_t1_610 * MethodBase_GetMethodBody_m1_6927 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MethodBase::System.Runtime.InteropServices._MethodBase.GetType()
extern "C" Type_t * MethodBase_System_Runtime_InteropServices__MethodBase_GetType_m1_6928 (MethodBase_t1_335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
