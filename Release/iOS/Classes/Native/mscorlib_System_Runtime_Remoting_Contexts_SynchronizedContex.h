﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
struct SynchronizationAttribute_t1_900;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Contexts.SynchronizedContextReplySink
struct  SynchronizedContextReplySink_t1_904  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::_next
	Object_t * ____next_0;
	// System.Boolean System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::_newLock
	bool ____newLock_1;
	// System.Runtime.Remoting.Contexts.SynchronizationAttribute System.Runtime.Remoting.Contexts.SynchronizedContextReplySink::_att
	SynchronizationAttribute_t1_900 * ____att_2;
};
