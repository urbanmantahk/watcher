﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_TYPEFLAGS.h"

// System.Runtime.InteropServices.TYPEFLAGS
struct  TYPEFLAGS_t1_828 
{
	// System.Int32 System.Runtime.InteropServices.TYPEFLAGS::value__
	int32_t ___value___1;
};
