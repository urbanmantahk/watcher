﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.HashMembershipCondition
struct HashMembershipCondition_t1_1351;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t1_162;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Object
struct Object_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.Policy.HashMembershipCondition::.ctor()
extern "C" void HashMembershipCondition__ctor_m1_11530 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::.ctor(System.Security.Cryptography.HashAlgorithm,System.Byte[])
extern "C" void HashMembershipCondition__ctor_m1_11531 (HashMembershipCondition_t1_1351 * __this, HashAlgorithm_t1_162 * ___hashAlg, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void HashMembershipCondition_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_11532 (HashMembershipCondition_t1_1351 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashMembershipCondition_System_Runtime_Serialization_ISerializable_GetObjectData_m1_11533 (HashMembershipCondition_t1_1351 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.HashAlgorithm System.Security.Policy.HashMembershipCondition::get_HashAlgorithm()
extern "C" HashAlgorithm_t1_162 * HashMembershipCondition_get_HashAlgorithm_m1_11534 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::set_HashAlgorithm(System.Security.Cryptography.HashAlgorithm)
extern "C" void HashMembershipCondition_set_HashAlgorithm_m1_11535 (HashMembershipCondition_t1_1351 * __this, HashAlgorithm_t1_162 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.HashMembershipCondition::get_HashValue()
extern "C" ByteU5BU5D_t1_109* HashMembershipCondition_get_HashValue_m1_11536 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::set_HashValue(System.Byte[])
extern "C" void HashMembershipCondition_set_HashValue_m1_11537 (HashMembershipCondition_t1_1351 * __this, ByteU5BU5D_t1_109* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.HashMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool HashMembershipCondition_Check_m1_11538 (HashMembershipCondition_t1_1351 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.HashMembershipCondition::Copy()
extern "C" Object_t * HashMembershipCondition_Copy_m1_11539 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.HashMembershipCondition::Equals(System.Object)
extern "C" bool HashMembershipCondition_Equals_m1_11540 (HashMembershipCondition_t1_1351 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.HashMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * HashMembershipCondition_ToXml_m1_11541 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.HashMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * HashMembershipCondition_ToXml_m1_11542 (HashMembershipCondition_t1_1351 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void HashMembershipCondition_FromXml_m1_11543 (HashMembershipCondition_t1_1351 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.HashMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void HashMembershipCondition_FromXml_m1_11544 (HashMembershipCondition_t1_1351 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.HashMembershipCondition::GetHashCode()
extern "C" int32_t HashMembershipCondition_GetHashCode_m1_11545 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.HashMembershipCondition::ToString()
extern "C" String_t* HashMembershipCondition_ToString_m1_11546 (HashMembershipCondition_t1_1351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.HashMembershipCondition::Compare(System.Byte[],System.Byte[])
extern "C" bool HashMembershipCondition_Compare_m1_11547 (HashMembershipCondition_t1_1351 * __this, ByteU5BU5D_t1_109* ___expected, ByteU5BU5D_t1_109* ___actual, const MethodInfo* method) IL2CPP_METHOD_ATTR;
