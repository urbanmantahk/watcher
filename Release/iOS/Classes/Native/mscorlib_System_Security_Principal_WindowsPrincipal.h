﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Principal.WindowsIdentity
struct WindowsIdentity_t1_1384;
// System.String[]
struct StringU5BU5D_t1_238;

#include "mscorlib_System_Object.h"

// System.Security.Principal.WindowsPrincipal
struct  WindowsPrincipal_t1_1386  : public Object_t
{
	// System.Security.Principal.WindowsIdentity System.Security.Principal.WindowsPrincipal::_identity
	WindowsIdentity_t1_1384 * ____identity_0;
	// System.String[] System.Security.Principal.WindowsPrincipal::m_roles
	StringU5BU5D_t1_238* ___m_roles_1;
};
