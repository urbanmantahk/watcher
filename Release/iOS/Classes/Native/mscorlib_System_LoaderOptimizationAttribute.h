﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_LoaderOptimization.h"

// System.LoaderOptimizationAttribute
struct  LoaderOptimizationAttribute_t1_1563  : public Attribute_t1_2
{
	// System.LoaderOptimization System.LoaderOptimizationAttribute::lo
	int32_t ___lo_0;
};
