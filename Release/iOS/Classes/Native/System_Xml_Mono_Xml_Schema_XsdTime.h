﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;

#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType.h"

// Mono.Xml.Schema.XsdTime
struct  XsdTime_t4_47  : public XsdAnySimpleType_t4_3
{
};
struct XsdTime_t4_47_StaticFields{
	// System.String[] Mono.Xml.Schema.XsdTime::timeFormats
	StringU5BU5D_t1_238* ___timeFormats_61;
};
