﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.CustomConstantAttribute
struct CustomConstantAttribute_t1_681;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.CustomConstantAttribute::.ctor()
extern "C" void CustomConstantAttribute__ctor_m1_7534 (CustomConstantAttribute_t1_681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
