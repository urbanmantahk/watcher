﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities
struct SoapEntities_t1_971;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::.ctor()
extern "C" void SoapEntities__ctor_m1_8712 (SoapEntities_t1_971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::.ctor(System.String)
extern "C" void SoapEntities__ctor_m1_8713 (SoapEntities_t1_971 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::get_Value()
extern "C" String_t* SoapEntities_get_Value_m1_8714 (SoapEntities_t1_971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::set_Value(System.String)
extern "C" void SoapEntities_set_Value_m1_8715 (SoapEntities_t1_971 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::get_XsdType()
extern "C" String_t* SoapEntities_get_XsdType_m1_8716 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::GetXsdType()
extern "C" String_t* SoapEntities_GetXsdType_m1_8717 (SoapEntities_t1_971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::Parse(System.String)
extern "C" SoapEntities_t1_971 * SoapEntities_Parse_m1_8718 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapEntities::ToString()
extern "C" String_t* SoapEntities_ToString_m1_8719 (SoapEntities_t1_971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
