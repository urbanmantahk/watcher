﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ComCompatibleVersionAttribute
struct ComCompatibleVersionAttribute_t1_768;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.ComCompatibleVersionAttribute::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void ComCompatibleVersionAttribute__ctor_m1_7601 (ComCompatibleVersionAttribute_t1_768 * __this, int32_t ___major, int32_t ___minor, int32_t ___build, int32_t ___revision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_MajorVersion()
extern "C" int32_t ComCompatibleVersionAttribute_get_MajorVersion_m1_7602 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_MinorVersion()
extern "C" int32_t ComCompatibleVersionAttribute_get_MinorVersion_m1_7603 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_BuildNumber()
extern "C" int32_t ComCompatibleVersionAttribute_get_BuildNumber_m1_7604 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.ComCompatibleVersionAttribute::get_RevisionNumber()
extern "C" int32_t ComCompatibleVersionAttribute_get_RevisionNumber_m1_7605 (ComCompatibleVersionAttribute_t1_768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
