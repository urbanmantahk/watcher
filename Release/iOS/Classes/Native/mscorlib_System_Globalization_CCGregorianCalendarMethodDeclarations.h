﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCGregorianCalendar
struct CCGregorianCalendar_t1_344;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.CCGregorianCalendar::.ctor()
extern "C" void CCGregorianCalendar__ctor_m1_3718 (CCGregorianCalendar_t1_344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCGregorianCalendar::is_leap_year(System.Int32)
extern "C" bool CCGregorianCalendar_is_leap_year_m1_3719 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::fixed_from_dmy(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCGregorianCalendar_fixed_from_dmy_m1_3720 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::year_from_fixed(System.Int32)
extern "C" int32_t CCGregorianCalendar_year_from_fixed_m1_3721 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCGregorianCalendar::my_from_fixed(System.Int32&,System.Int32&,System.Int32)
extern "C" void CCGregorianCalendar_my_from_fixed_m1_3722 (Object_t * __this /* static, unused */, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.CCGregorianCalendar::dmy_from_fixed(System.Int32&,System.Int32&,System.Int32&,System.Int32)
extern "C" void CCGregorianCalendar_dmy_from_fixed_m1_3723 (Object_t * __this /* static, unused */, int32_t* ___day, int32_t* ___month, int32_t* ___year, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::month_from_fixed(System.Int32)
extern "C" int32_t CCGregorianCalendar_month_from_fixed_m1_3724 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::day_from_fixed(System.Int32)
extern "C" int32_t CCGregorianCalendar_day_from_fixed_m1_3725 (Object_t * __this /* static, unused */, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::date_difference(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCGregorianCalendar_date_difference_m1_3726 (Object_t * __this /* static, unused */, int32_t ___dayA, int32_t ___monthA, int32_t ___yearA, int32_t ___dayB, int32_t ___monthB, int32_t ___yearB, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::day_number(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCGregorianCalendar_day_number_m1_3727 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::days_remaining(System.Int32,System.Int32,System.Int32)
extern "C" int32_t CCGregorianCalendar_days_remaining_m1_3728 (Object_t * __this /* static, unused */, int32_t ___day, int32_t ___month, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCGregorianCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  CCGregorianCalendar_AddMonths_m1_3729 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCGregorianCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  CCGregorianCalendar_AddYears_m1_3730 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t CCGregorianCalendar_GetDayOfMonth_m1_3731 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t CCGregorianCalendar_GetDayOfYear_m1_3732 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::GetDaysInMonth(System.Int32,System.Int32)
extern "C" int32_t CCGregorianCalendar_GetDaysInMonth_m1_3733 (Object_t * __this /* static, unused */, int32_t ___year, int32_t ___month, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::GetDaysInYear(System.Int32)
extern "C" int32_t CCGregorianCalendar_GetDaysInYear_m1_3734 (Object_t * __this /* static, unused */, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::GetMonth(System.DateTime)
extern "C" int32_t CCGregorianCalendar_GetMonth_m1_3735 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCGregorianCalendar::GetYear(System.DateTime)
extern "C" int32_t CCGregorianCalendar_GetYear_m1_3736 (Object_t * __this /* static, unused */, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCGregorianCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32)
extern "C" bool CCGregorianCalendar_IsLeapDay_m1_3737 (Object_t * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.CCGregorianCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  CCGregorianCalendar_ToDateTime_m1_3738 (Object_t * __this /* static, unused */, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___milliseconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
