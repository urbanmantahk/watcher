﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1_92;

#include "mscorlib_System_Object.h"

// System.Security.Policy.DefaultPolicies/ReservedNames
struct  ReservedNames_t1_1341  : public Object_t
{
};
struct ReservedNames_t1_1341_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Security.Policy.DefaultPolicies/ReservedNames::<>f__switch$map2C
	Dictionary_2_t1_92 * ___U3CU3Ef__switchU24map2C_7;
};
