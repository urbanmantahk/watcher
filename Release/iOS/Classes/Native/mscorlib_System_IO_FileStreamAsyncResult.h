﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Exception
struct Exception_t1_33;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t1_431;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// System.IO.FileStreamAsyncResult
struct  FileStreamAsyncResult_t1_430  : public Object_t
{
	// System.Object System.IO.FileStreamAsyncResult::state
	Object_t * ___state_0;
	// System.Boolean System.IO.FileStreamAsyncResult::completed
	bool ___completed_1;
	// System.Boolean System.IO.FileStreamAsyncResult::done
	bool ___done_2;
	// System.Exception System.IO.FileStreamAsyncResult::exc
	Exception_t1_33 * ___exc_3;
	// System.Threading.ManualResetEvent System.IO.FileStreamAsyncResult::wh
	ManualResetEvent_t1_431 * ___wh_4;
	// System.AsyncCallback System.IO.FileStreamAsyncResult::cb
	AsyncCallback_t1_28 * ___cb_5;
	// System.Boolean System.IO.FileStreamAsyncResult::completedSynch
	bool ___completedSynch_6;
	// System.Byte[] System.IO.FileStreamAsyncResult::Buffer
	ByteU5BU5D_t1_109* ___Buffer_7;
	// System.Int32 System.IO.FileStreamAsyncResult::Offset
	int32_t ___Offset_8;
	// System.Int32 System.IO.FileStreamAsyncResult::Count
	int32_t ___Count_9;
	// System.Int32 System.IO.FileStreamAsyncResult::OriginalCount
	int32_t ___OriginalCount_10;
	// System.Int32 System.IO.FileStreamAsyncResult::BytesRead
	int32_t ___BytesRead_11;
	// System.AsyncCallback System.IO.FileStreamAsyncResult::realcb
	AsyncCallback_t1_28 * ___realcb_12;
};
