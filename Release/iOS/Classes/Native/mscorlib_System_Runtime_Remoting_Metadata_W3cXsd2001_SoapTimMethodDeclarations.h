﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime
struct SoapTime_t1_993;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::.ctor()
extern "C" void SoapTime__ctor_m1_8894 (SoapTime_t1_993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::.ctor(System.DateTime)
extern "C" void SoapTime__ctor_m1_8895 (SoapTime_t1_993 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::.cctor()
extern "C" void SoapTime__cctor_m1_8896 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::get_Value()
extern "C" DateTime_t1_150  SoapTime_get_Value_m1_8897 (SoapTime_t1_993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::set_Value(System.DateTime)
extern "C" void SoapTime_set_Value_m1_8898 (SoapTime_t1_993 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::get_XsdType()
extern "C" String_t* SoapTime_get_XsdType_m1_8899 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::GetXsdType()
extern "C" String_t* SoapTime_GetXsdType_m1_8900 (SoapTime_t1_993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::Parse(System.String)
extern "C" SoapTime_t1_993 * SoapTime_Parse_m1_8901 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapTime::ToString()
extern "C" String_t* SoapTime_ToString_m1_8902 (SoapTime_t1_993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
