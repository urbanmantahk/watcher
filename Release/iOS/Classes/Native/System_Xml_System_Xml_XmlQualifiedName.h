﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t4_71;
// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t4_71  : public Object_t
{
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;
};
struct XmlQualifiedName_t4_71_StaticFields{
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t4_71 * ___Empty_0;
};
