﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings
struct iOSSettings_t8_319;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::.ctor()
extern "C" void iOSSettings__ctor_m8_1876 (iOSSettings_t8_319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::get_StoreIdentifier()
extern "C" String_t* iOSSettings_get_StoreIdentifier_m8_1877 (iOSSettings_t8_319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.ApplicationSettings/iOSSettings::set_StoreIdentifier(System.String)
extern "C" void iOSSettings_set_StoreIdentifier_m8_1878 (iOSSettings_t8_319 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
