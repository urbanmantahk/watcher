﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t1_533;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1_466;
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1_502;
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t1_534;
// System.Reflection.Emit.MonoResource[]
struct MonoResourceU5BU5D_t1_470;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Type
struct Type_t;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Int32[]
struct Int32U5BU5D_t1_275;
// System.Reflection.Emit.ModuleBuilderTokenGenerator
struct ModuleBuilderTokenGenerator_t1_535;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t1_536;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_UIntPtr.h"

// System.Reflection.Emit.ModuleBuilder
struct  ModuleBuilder_t1_475  : public Module_t1_495
{
	// System.UIntPtr System.Reflection.Emit.ModuleBuilder::dynamic_image
	UIntPtr_t  ___dynamic_image_10;
	// System.Int32 System.Reflection.Emit.ModuleBuilder::num_types
	int32_t ___num_types_11;
	// System.Reflection.Emit.TypeBuilder[] System.Reflection.Emit.ModuleBuilder::types
	TypeBuilderU5BU5D_t1_533* ___types_12;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.ModuleBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_13;
	// System.Byte[] System.Reflection.Emit.ModuleBuilder::guid
	ByteU5BU5D_t1_109* ___guid_14;
	// System.Int32 System.Reflection.Emit.ModuleBuilder::table_idx
	int32_t ___table_idx_15;
	// System.Reflection.Emit.AssemblyBuilder System.Reflection.Emit.ModuleBuilder::assemblyb
	AssemblyBuilder_t1_466 * ___assemblyb_16;
	// System.Reflection.Emit.MethodBuilder[] System.Reflection.Emit.ModuleBuilder::global_methods
	MethodBuilderU5BU5D_t1_502* ___global_methods_17;
	// System.Reflection.Emit.FieldBuilder[] System.Reflection.Emit.ModuleBuilder::global_fields
	FieldBuilderU5BU5D_t1_534* ___global_fields_18;
	// System.Boolean System.Reflection.Emit.ModuleBuilder::is_main
	bool ___is_main_19;
	// System.Reflection.Emit.MonoResource[] System.Reflection.Emit.ModuleBuilder::resources
	MonoResourceU5BU5D_t1_470* ___resources_20;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.ModuleBuilder::global_type
	TypeBuilder_t1_481 * ___global_type_21;
	// System.Type System.Reflection.Emit.ModuleBuilder::global_type_created
	Type_t * ___global_type_created_22;
	// System.Collections.Hashtable System.Reflection.Emit.ModuleBuilder::name_cache
	Hashtable_t1_100 * ___name_cache_23;
	// System.Collections.Hashtable System.Reflection.Emit.ModuleBuilder::us_string_cache
	Hashtable_t1_100 * ___us_string_cache_24;
	// System.Int32[] System.Reflection.Emit.ModuleBuilder::table_indexes
	Int32U5BU5D_t1_275* ___table_indexes_25;
	// System.Boolean System.Reflection.Emit.ModuleBuilder::transient
	bool ___transient_26;
	// System.Reflection.Emit.ModuleBuilderTokenGenerator System.Reflection.Emit.ModuleBuilder::token_gen
	ModuleBuilderTokenGenerator_t1_535 * ___token_gen_27;
	// System.Collections.Hashtable System.Reflection.Emit.ModuleBuilder::resource_writers
	Hashtable_t1_100 * ___resource_writers_28;
	// System.Diagnostics.SymbolStore.ISymbolWriter System.Reflection.Emit.ModuleBuilder::symbolWriter
	Object_t * ___symbolWriter_29;
};
struct ModuleBuilder_t1_475_StaticFields{
	// System.Char[] System.Reflection.Emit.ModuleBuilder::type_modifiers
	CharU5BU5D_t1_16* ___type_modifiers_30;
};
