﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t1_2704;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26870_gshared (Enumerator_t1_2710 * __this, Dictionary_2_t1_2704 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_26870(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2710 *, Dictionary_2_t1_2704 *, const MethodInfo*))Enumerator__ctor_m1_26870_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26872_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_26872(__this, method) (( Object_t * (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_26872_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26874_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_26874(__this, method) (( void (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_26874_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26876_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26876(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26876_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26878_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26878(__this, method) (( Object_t * (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26878_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26880_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26880(__this, method) (( Object_t * (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26882_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_26882(__this, method) (( bool (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_MoveNext_m1_26882_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_Current()
extern "C" KeyValuePair_2_t1_2706  Enumerator_get_Current_m1_26884_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_26884(__this, method) (( KeyValuePair_2_t1_2706  (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_get_Current_m1_26884_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m1_26886_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_26886(__this, method) (( Object_t * (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_26886_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::get_CurrentValue()
extern "C" float Enumerator_get_CurrentValue_m1_26888_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_26888(__this, method) (( float (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_26888_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Reset()
extern "C" void Enumerator_Reset_m1_26890_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_26890(__this, method) (( void (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_Reset_m1_26890_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_26892_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_26892(__this, method) (( void (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_VerifyState_m1_26892_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_26894_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_26894(__this, method) (( void (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_26894_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m1_26896_gshared (Enumerator_t1_2710 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_26896(__this, method) (( void (*) (Enumerator_t1_2710 *, const MethodInfo*))Enumerator_Dispose_m1_26896_gshared)(__this, method)
