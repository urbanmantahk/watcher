﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ModuleBuilderTokenGenerator
struct ModuleBuilderTokenGenerator_t1_535;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.String
struct String_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.Emit.SignatureHelper
struct SignatureHelper_t1_551;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Emit.ModuleBuilderTokenGenerator::.ctor(System.Reflection.Emit.ModuleBuilder)
extern "C" void ModuleBuilderTokenGenerator__ctor_m1_6233 (ModuleBuilderTokenGenerator_t1_535 * __this, ModuleBuilder_t1_475 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilderTokenGenerator::GetToken(System.String)
extern "C" int32_t ModuleBuilderTokenGenerator_GetToken_m1_6234 (ModuleBuilderTokenGenerator_t1_535 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilderTokenGenerator::GetToken(System.Reflection.MemberInfo)
extern "C" int32_t ModuleBuilderTokenGenerator_GetToken_m1_6235 (ModuleBuilderTokenGenerator_t1_535 * __this, MemberInfo_t * ___member, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilderTokenGenerator::GetToken(System.Reflection.MethodInfo,System.Type[])
extern "C" int32_t ModuleBuilderTokenGenerator_GetToken_m1_6236 (ModuleBuilderTokenGenerator_t1_535 * __this, MethodInfo_t * ___method, TypeU5BU5D_t1_31* ___opt_param_types, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.ModuleBuilderTokenGenerator::GetToken(System.Reflection.Emit.SignatureHelper)
extern "C" int32_t ModuleBuilderTokenGenerator_GetToken_m1_6237 (ModuleBuilderTokenGenerator_t1_535 * __this, SignatureHelper_t1_551 * ___helper, const MethodInfo* method) IL2CPP_METHOD_ATTR;
