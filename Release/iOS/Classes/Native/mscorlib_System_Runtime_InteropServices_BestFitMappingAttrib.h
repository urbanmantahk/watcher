﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Runtime.InteropServices.BestFitMappingAttribute
struct  BestFitMappingAttribute_t1_758  : public Attribute_t1_2
{
	// System.Boolean System.Runtime.InteropServices.BestFitMappingAttribute::bfm
	bool ___bfm_0;
	// System.Boolean System.Runtime.InteropServices.BestFitMappingAttribute::ThrowOnUnmappableChar
	bool ___ThrowOnUnmappableChar_1;
};
