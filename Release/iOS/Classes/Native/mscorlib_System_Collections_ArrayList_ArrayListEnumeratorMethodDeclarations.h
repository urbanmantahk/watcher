﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/ArrayListEnumerator
struct ArrayListEnumerator_t1_258;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/ArrayListEnumerator::.ctor(System.Collections.ArrayList)
extern "C" void ArrayListEnumerator__ctor_m1_2785 (ArrayListEnumerator_t1_258 * __this, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListEnumerator::.ctor(System.Collections.ArrayList,System.Int32,System.Int32)
extern "C" void ArrayListEnumerator__ctor_m1_2786 (ArrayListEnumerator_t1_258 * __this, ArrayList_t1_170 * ___list, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListEnumerator::Clone()
extern "C" Object_t * ArrayListEnumerator_Clone_m1_2787 (ArrayListEnumerator_t1_258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/ArrayListEnumerator::get_Current()
extern "C" Object_t * ArrayListEnumerator_get_Current_m1_2788 (ArrayListEnumerator_t1_258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/ArrayListEnumerator::MoveNext()
extern "C" bool ArrayListEnumerator_MoveNext_m1_2789 (ArrayListEnumerator_t1_258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/ArrayListEnumerator::Reset()
extern "C" void ArrayListEnumerator_Reset_m1_2790 (ArrayListEnumerator_t1_258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
