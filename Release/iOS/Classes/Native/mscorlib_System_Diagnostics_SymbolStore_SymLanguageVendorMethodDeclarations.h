﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.SymbolStore.SymLanguageVendor
struct SymLanguageVendor_t1_321;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.SymbolStore.SymLanguageVendor::.ctor()
extern "C" void SymLanguageVendor__ctor_m1_3565 (SymLanguageVendor_t1_321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
