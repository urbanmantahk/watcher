﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"
#include "mscorlib_System_Runtime_Versioning_ResourceScope.h"

// System.Runtime.Versioning.ResourceConsumptionAttribute
struct  ResourceConsumptionAttribute_t1_1100  : public Attribute_t1_2
{
	// System.Runtime.Versioning.ResourceScope System.Runtime.Versioning.ResourceConsumptionAttribute::resource
	int32_t ___resource_0;
	// System.Runtime.Versioning.ResourceScope System.Runtime.Versioning.ResourceConsumptionAttribute::consumption
	int32_t ___consumption_1;
};
