﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorageContainm.h"

// System.Security.Permissions.IsolatedStoragePermissionAttribute
struct  IsolatedStoragePermissionAttribute_t1_1287  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.Permissions.IsolatedStorageContainment System.Security.Permissions.IsolatedStoragePermissionAttribute::usage_allowed
	int32_t ___usage_allowed_2;
	// System.Int64 System.Security.Permissions.IsolatedStoragePermissionAttribute::user_quota
	int64_t ___user_quota_3;
};
