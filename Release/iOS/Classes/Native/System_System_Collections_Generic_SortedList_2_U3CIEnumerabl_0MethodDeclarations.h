﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>
struct GetEnumeratorU3Ec__Iterator0_t3_285;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::.ctor()
extern "C" void GetEnumeratorU3Ec__Iterator0__ctor_m3_2227_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0__ctor_m3_2227(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0__ctor_m3_2227_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_Current()
extern "C" KeyValuePair_2_t1_2681  GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2228_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2228(__this, method) (( KeyValuePair_2_t1_2681  (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_Current_m3_2228_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_2229_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_2229(__this, method) (( Object_t * (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3_2229_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool GetEnumeratorU3Ec__Iterator0_MoveNext_m3_2230_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_MoveNext_m3_2230(__this, method) (( bool (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_MoveNext_m3_2230_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void GetEnumeratorU3Ec__Iterator0_Dispose_m3_2231_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Dispose_m3_2231(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Dispose_m3_2231_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/<IEnumerable.GetEnumerator>c__Iterator0<System.Int32,ExifLibrary.IFD>::Reset()
extern "C" void GetEnumeratorU3Ec__Iterator0_Reset_m3_2232_gshared (GetEnumeratorU3Ec__Iterator0_t3_285 * __this, const MethodInfo* method);
#define GetEnumeratorU3Ec__Iterator0_Reset_m3_2232(__this, method) (( void (*) (GetEnumeratorU3Ec__Iterator0_t3_285 *, const MethodInfo*))GetEnumeratorU3Ec__Iterator0_Reset_m3_2232_gshared)(__this, method)
