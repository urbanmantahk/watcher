﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.InteropServices.CriticalHandle
struct  CriticalHandle_t1_83  : public CriticalFinalizerObject_t1_713
{
	// System.IntPtr System.Runtime.InteropServices.CriticalHandle::handle
	IntPtr_t ___handle_0;
	// System.Boolean System.Runtime.InteropServices.CriticalHandle::_disposed
	bool ____disposed_1;
};
