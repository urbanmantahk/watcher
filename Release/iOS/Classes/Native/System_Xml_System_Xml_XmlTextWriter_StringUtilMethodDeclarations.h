﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Xml.XmlTextWriter/StringUtil::.cctor()
extern "C" void StringUtil__cctor_m4_958 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Xml.XmlTextWriter/StringUtil::IndexOf(System.String,System.String)
extern "C" int32_t StringUtil_IndexOf_m4_959 (Object_t * __this /* static, unused */, String_t* ___src, String_t* ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Xml.XmlTextWriter/StringUtil::Format(System.String,System.Object[])
extern "C" String_t* StringUtil_Format_m4_960 (Object_t * __this /* static, unused */, String_t* ___format, ObjectU5BU5D_t1_272* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
