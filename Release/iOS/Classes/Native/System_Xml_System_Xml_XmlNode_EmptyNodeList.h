﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "System_Xml_System_Xml_XmlNodeList.h"

// System.Xml.XmlNode/EmptyNodeList
struct  EmptyNodeList_t4_146  : public XmlNodeList_t4_147
{
};
struct EmptyNodeList_t4_146_StaticFields{
	// System.Collections.IEnumerator System.Xml.XmlNode/EmptyNodeList::emptyEnumerator
	Object_t * ___emptyEnumerator_0;
};
