﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.NativeCppClassAttribute
struct NativeCppClassAttribute_t1_705;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.CompilerServices.NativeCppClassAttribute::.ctor()
extern "C" void NativeCppClassAttribute__ctor_m1_7550 (NativeCppClassAttribute_t1_705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
