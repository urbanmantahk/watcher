﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.IFD0IsEmptyException
struct IFD0IsEmptyException_t8_97;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ExifLibrary.IFD0IsEmptyException::.ctor()
extern "C" void IFD0IsEmptyException__ctor_m8_409 (IFD0IsEmptyException_t8_97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.IFD0IsEmptyException::.ctor(System.String)
extern "C" void IFD0IsEmptyException__ctor_m8_410 (IFD0IsEmptyException_t8_97 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
