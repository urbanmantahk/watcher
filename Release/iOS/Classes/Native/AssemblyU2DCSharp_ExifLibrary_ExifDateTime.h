﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "mscorlib_System_DateTime.h"

// ExifLibrary.ExifDateTime
struct  ExifDateTime_t8_100  : public ExifProperty_t8_99
{
	// System.DateTime ExifLibrary.ExifDateTime::mValue
	DateTime_t1_150  ___mValue_3;
};
