﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.SynchronizationContext
struct SynchronizationContext_t1_1475;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t1_1627;
// System.Object
struct Object_t;
// System.IntPtr[]
struct IntPtrU5BU5D_t1_34;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.SynchronizationContext::.ctor()
extern "C" void SynchronizationContext__ctor_m1_12772 (SynchronizationContext_t1_1475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::.ctor(System.Threading.SynchronizationContext)
extern "C" void SynchronizationContext__ctor_m1_12773 (SynchronizationContext_t1_1475 * __this, SynchronizationContext_t1_1475 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::get_Current()
extern "C" SynchronizationContext_t1_1475 * SynchronizationContext_get_Current_m1_12774 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::CreateCopy()
extern "C" SynchronizationContext_t1_1475 * SynchronizationContext_CreateCopy_m1_12775 (SynchronizationContext_t1_1475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.SynchronizationContext::IsWaitNotificationRequired()
extern "C" bool SynchronizationContext_IsWaitNotificationRequired_m1_12776 (SynchronizationContext_t1_1475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::OperationCompleted()
extern "C" void SynchronizationContext_OperationCompleted_m1_12777 (SynchronizationContext_t1_1475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::OperationStarted()
extern "C" void SynchronizationContext_OperationStarted_m1_12778 (SynchronizationContext_t1_1475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern "C" void SynchronizationContext_Post_m1_12779 (SynchronizationContext_t1_1475 * __this, SendOrPostCallback_t1_1627 * ___d, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern "C" void SynchronizationContext_Send_m1_12780 (SynchronizationContext_t1_1475 * __this, SendOrPostCallback_t1_1627 * ___d, Object_t * ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetSynchronizationContext(System.Threading.SynchronizationContext)
extern "C" void SynchronizationContext_SetSynchronizationContext_m1_12781 (Object_t * __this /* static, unused */, SynchronizationContext_t1_1475 * ___syncContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetThreadStaticContext(System.Threading.SynchronizationContext)
extern "C" void SynchronizationContext_SetThreadStaticContext_m1_12782 (Object_t * __this /* static, unused */, SynchronizationContext_t1_1475 * ___syncContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetWaitNotificationRequired()
extern "C" void SynchronizationContext_SetWaitNotificationRequired_m1_12783 (SynchronizationContext_t1_1475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.SynchronizationContext::Wait(System.IntPtr[],System.Boolean,System.Int32)
extern "C" int32_t SynchronizationContext_Wait_m1_12784 (SynchronizationContext_t1_1475 * __this, IntPtrU5BU5D_t1_34* ___waitHandles, bool ___waitAll, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.SynchronizationContext::WaitHelper(System.IntPtr[],System.Boolean,System.Int32)
extern "C" int32_t SynchronizationContext_WaitHelper_m1_12785 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1_34* ___waitHandles, bool ___waitAll, int32_t ___millisecondsTimeout, const MethodInfo* method) IL2CPP_METHOD_ATTR;
