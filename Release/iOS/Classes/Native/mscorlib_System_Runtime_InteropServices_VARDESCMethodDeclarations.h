﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void VARDESC_t1_846_marshal(const VARDESC_t1_846& unmarshaled, VARDESC_t1_846_marshaled& marshaled);
extern "C" void VARDESC_t1_846_marshal_back(const VARDESC_t1_846_marshaled& marshaled, VARDESC_t1_846& unmarshaled);
extern "C" void VARDESC_t1_846_marshal_cleanup(VARDESC_t1_846_marshaled& marshaled);
