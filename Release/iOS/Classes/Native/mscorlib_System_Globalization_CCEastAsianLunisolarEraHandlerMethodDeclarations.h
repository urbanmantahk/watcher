﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Globalization_CCEastAsianLunisolarEraHandler.h"

// System.Void System.Globalization.CCEastAsianLunisolarEraHandler/Era::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void Era__ctor_m1_3808 (Era_t1_354 * __this, int32_t ___nr, int32_t ___start, int32_t ___end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarEraHandler/Era::get_Nr()
extern "C" int32_t Era_get_Nr_m1_3809 (Era_t1_354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarEraHandler/Era::GregorianYear(System.Int32)
extern "C" int32_t Era_GregorianYear_m1_3810 (Era_t1_354 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.CCEastAsianLunisolarEraHandler/Era::Covers(System.Int32)
extern "C" bool Era_Covers_m1_3811 (Era_t1_354 * __this, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCEastAsianLunisolarEraHandler/Era::EraYear(System.Int32&,System.Int32)
extern "C" int32_t Era_EraYear_m1_3812 (Era_t1_354 * __this, int32_t* ___era, int32_t ___date, const MethodInfo* method) IL2CPP_METHOD_ATTR;
