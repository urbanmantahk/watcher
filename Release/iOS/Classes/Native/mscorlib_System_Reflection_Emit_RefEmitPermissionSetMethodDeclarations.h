﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionSet.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Reflection.Emit.RefEmitPermissionSet::.ctor(System.Security.Permissions.SecurityAction,System.String)
extern "C" void RefEmitPermissionSet__ctor_m1_5451 (RefEmitPermissionSet_t1_463 * __this, int32_t ___action, String_t* ___pset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void RefEmitPermissionSet_t1_463_marshal(const RefEmitPermissionSet_t1_463& unmarshaled, RefEmitPermissionSet_t1_463_marshaled& marshaled);
extern "C" void RefEmitPermissionSet_t1_463_marshal_back(const RefEmitPermissionSet_t1_463_marshaled& marshaled, RefEmitPermissionSet_t1_463& unmarshaled);
extern "C" void RefEmitPermissionSet_t1_463_marshal_cleanup(RefEmitPermissionSet_t1_463_marshaled& marshaled);
