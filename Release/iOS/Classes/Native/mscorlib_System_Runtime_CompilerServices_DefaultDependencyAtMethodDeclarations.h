﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.CompilerServices.DefaultDependencyAttribute
struct DefaultDependencyAttribute_t1_683;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"

// System.Void System.Runtime.CompilerServices.DefaultDependencyAttribute::.ctor(System.Runtime.CompilerServices.LoadHint)
extern "C" void DefaultDependencyAttribute__ctor_m1_7538 (DefaultDependencyAttribute_t1_683 * __this, int32_t ___loadHintArgument, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.CompilerServices.LoadHint System.Runtime.CompilerServices.DefaultDependencyAttribute::get_LoadHint()
extern "C" int32_t DefaultDependencyAttribute_get_LoadHint_m1_7539 (DefaultDependencyAttribute_t1_683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
