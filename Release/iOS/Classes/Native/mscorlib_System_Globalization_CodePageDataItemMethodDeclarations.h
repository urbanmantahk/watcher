﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CodePageDataItem
struct CodePageDataItem_t1_359;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.CodePageDataItem::.ctor()
extern "C" void CodePageDataItem__ctor_m1_3837 (CodePageDataItem_t1_359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
