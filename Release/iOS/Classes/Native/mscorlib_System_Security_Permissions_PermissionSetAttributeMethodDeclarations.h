﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.PermissionSetAttribute
struct PermissionSetAttribute_t1_1294;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.PermissionSet
struct PermissionSet_t1_563;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"

// System.Void System.Security.Permissions.PermissionSetAttribute::.ctor(System.Security.Permissions.SecurityAction)
extern "C" void PermissionSetAttribute__ctor_m1_11018 (PermissionSetAttribute_t1_1294 * __this, int32_t ___action, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PermissionSetAttribute::get_File()
extern "C" String_t* PermissionSetAttribute_get_File_m1_11019 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PermissionSetAttribute::set_File(System.String)
extern "C" void PermissionSetAttribute_set_File_m1_11020 (PermissionSetAttribute_t1_1294 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PermissionSetAttribute::get_Hex()
extern "C" String_t* PermissionSetAttribute_get_Hex_m1_11021 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PermissionSetAttribute::set_Hex(System.String)
extern "C" void PermissionSetAttribute_set_Hex_m1_11022 (PermissionSetAttribute_t1_1294 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PermissionSetAttribute::get_Name()
extern "C" String_t* PermissionSetAttribute_get_Name_m1_11023 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PermissionSetAttribute::set_Name(System.String)
extern "C" void PermissionSetAttribute_set_Name_m1_11024 (PermissionSetAttribute_t1_1294 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.PermissionSetAttribute::get_UnicodeEncoded()
extern "C" bool PermissionSetAttribute_get_UnicodeEncoded_m1_11025 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PermissionSetAttribute::set_UnicodeEncoded(System.Boolean)
extern "C" void PermissionSetAttribute_set_UnicodeEncoded_m1_11026 (PermissionSetAttribute_t1_1294 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.PermissionSetAttribute::get_XML()
extern "C" String_t* PermissionSetAttribute_get_XML_m1_11027 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.PermissionSetAttribute::set_XML(System.String)
extern "C" void PermissionSetAttribute_set_XML_m1_11028 (PermissionSetAttribute_t1_1294 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.PermissionSetAttribute::CreatePermission()
extern "C" Object_t * PermissionSetAttribute_CreatePermission_m1_11029 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Permissions.PermissionSetAttribute::CreateFromXml(System.String)
extern "C" PermissionSet_t1_563 * PermissionSetAttribute_CreateFromXml_m1_11030 (PermissionSetAttribute_t1_1294 * __this, String_t* ___xml, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.PermissionSet System.Security.Permissions.PermissionSetAttribute::CreatePermissionSet()
extern "C" PermissionSet_t1_563 * PermissionSetAttribute_CreatePermissionSet_m1_11031 (PermissionSetAttribute_t1_1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
