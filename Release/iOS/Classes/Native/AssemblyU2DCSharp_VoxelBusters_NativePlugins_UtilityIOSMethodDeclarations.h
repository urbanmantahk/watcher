﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.UtilityIOS
struct UtilityIOS_t8_308;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.UtilityIOS::.ctor()
extern "C" void UtilityIOS__ctor_m8_1801 (UtilityIOS_t8_308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UtilityIOS::setApplicationIconBadgeNumber(System.Int32)
extern "C" void UtilityIOS_setApplicationIconBadgeNumber_m8_1802 (Object_t * __this /* static, unused */, int32_t ____badgeNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UtilityIOS::OpenStoreLink(System.String)
extern "C" void UtilityIOS_OpenStoreLink_m8_1803 (UtilityIOS_t8_308 * __this, String_t* ____applicationID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.UtilityIOS::SetApplicationIconBadgeNumber(System.Int32)
extern "C" void UtilityIOS_SetApplicationIconBadgeNumber_m8_1804 (UtilityIOS_t8_308 * __this, int32_t ____badgeNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
