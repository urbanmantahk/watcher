﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "System_Xml_Mono_Xml_DTDNode.h"

// Mono.Xml.DTDNotationDeclaration
struct  DTDNotationDeclaration_t4_99  : public DTDNode_t4_89
{
	// System.String Mono.Xml.DTDNotationDeclaration::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDNotationDeclaration::localName
	String_t* ___localName_6;
	// System.String Mono.Xml.DTDNotationDeclaration::prefix
	String_t* ___prefix_7;
	// System.String Mono.Xml.DTDNotationDeclaration::publicId
	String_t* ___publicId_8;
	// System.String Mono.Xml.DTDNotationDeclaration::systemId
	String_t* ___systemId_9;
};
