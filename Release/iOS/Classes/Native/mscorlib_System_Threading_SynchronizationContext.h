﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.SynchronizationContext
struct SynchronizationContext_t1_1475;

#include "mscorlib_System_Object.h"

// System.Threading.SynchronizationContext
struct  SynchronizationContext_t1_1475  : public Object_t
{
	// System.Boolean System.Threading.SynchronizationContext::notification_required
	bool ___notification_required_0;
};
struct SynchronizationContext_t1_1475_ThreadStaticFields{
	// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::currentContext
	SynchronizationContext_t1_1475 * ___currentContext_1;
};
