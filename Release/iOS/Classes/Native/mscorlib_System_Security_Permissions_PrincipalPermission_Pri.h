﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"

// System.Security.Permissions.PrincipalPermission/PrincipalInfo
struct  PrincipalInfo_t1_1296  : public Object_t
{
	// System.String System.Security.Permissions.PrincipalPermission/PrincipalInfo::_name
	String_t* ____name_0;
	// System.String System.Security.Permissions.PrincipalPermission/PrincipalInfo::_role
	String_t* ____role_1;
	// System.Boolean System.Security.Permissions.PrincipalPermission/PrincipalInfo::_isAuthenticated
	bool ____isAuthenticated_2;
};
