﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::.ctor()
#define Stack_1__ctor_m3_2025(__this, method) (( void (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1__ctor_m3_1816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_2026(__this, method) (( bool (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3_2027(__this, method) (( Object_t * (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m3_2028(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3_269 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_2029(__this, method) (( Object_t* (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_2030(__this, method) (( Object_t * (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::Contains(T)
#define Stack_1_Contains_m3_2031(__this, ___t, method) (( bool (*) (Stack_1_t3_269 *, List_1_t1_1894 *, const MethodInfo*))Stack_1_Contains_m3_1827_gshared)(__this, ___t, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::Peek()
#define Stack_1_Peek_m3_2032(__this, method) (( List_1_t1_1894 * (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_Peek_m3_1829_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::Pop()
#define Stack_1_Pop_m3_2033(__this, method) (( List_1_t1_1894 * (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_Pop_m3_1830_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::Push(T)
#define Stack_1_Push_m3_2034(__this, ___t, method) (( void (*) (Stack_1_t3_269 *, List_1_t1_1894 *, const MethodInfo*))Stack_1_Push_m3_1831_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::get_Count()
#define Stack_1_get_Count_m3_2035(__this, method) (( int32_t (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_get_Count_m3_1833_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>::GetEnumerator()
#define Stack_1_GetEnumerator_m3_2036(__this, method) (( Enumerator_t3_291  (*) (Stack_1_t3_269 *, const MethodInfo*))Stack_1_GetEnumerator_m3_1835_gshared)(__this, method)
