﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_InteropServices_VarEnum.h"

// System.Runtime.InteropServices.VarEnum
struct  VarEnum_t1_848 
{
	// System.Int32 System.Runtime.InteropServices.VarEnum::value__
	int32_t ___value___1;
};
