﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_Permissions_CodeAccessSecurityAttri.h"
#include "mscorlib_System_Security_Permissions_HostProtectionResource.h"

// System.Security.Permissions.HostProtectionAttribute
struct  HostProtectionAttribute_t1_1279  : public CodeAccessSecurityAttribute_t1_1266
{
	// System.Security.Permissions.HostProtectionResource System.Security.Permissions.HostProtectionAttribute::_resources
	int32_t ____resources_2;
};
