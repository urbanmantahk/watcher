﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.PublisherMembershipCondition
struct PublisherMembershipCondition_t1_1360;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1_1179;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.PublisherMembershipCondition::.ctor()
extern "C" void PublisherMembershipCondition__ctor_m1_11644 (PublisherMembershipCondition_t1_1360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PublisherMembershipCondition::.ctor(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" void PublisherMembershipCondition__ctor_m1_11645 (PublisherMembershipCondition_t1_1360 * __this, X509Certificate_t1_1179 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate System.Security.Policy.PublisherMembershipCondition::get_Certificate()
extern "C" X509Certificate_t1_1179 * PublisherMembershipCondition_get_Certificate_m1_11646 (PublisherMembershipCondition_t1_1360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PublisherMembershipCondition::set_Certificate(System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C" void PublisherMembershipCondition_set_Certificate_m1_11647 (PublisherMembershipCondition_t1_1360 * __this, X509Certificate_t1_1179 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.PublisherMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool PublisherMembershipCondition_Check_m1_11648 (PublisherMembershipCondition_t1_1360 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.PublisherMembershipCondition::Copy()
extern "C" Object_t * PublisherMembershipCondition_Copy_m1_11649 (PublisherMembershipCondition_t1_1360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.PublisherMembershipCondition::Equals(System.Object)
extern "C" bool PublisherMembershipCondition_Equals_m1_11650 (PublisherMembershipCondition_t1_1360 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PublisherMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void PublisherMembershipCondition_FromXml_m1_11651 (PublisherMembershipCondition_t1_1360 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.PublisherMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void PublisherMembershipCondition_FromXml_m1_11652 (PublisherMembershipCondition_t1_1360 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.PublisherMembershipCondition::GetHashCode()
extern "C" int32_t PublisherMembershipCondition_GetHashCode_m1_11653 (PublisherMembershipCondition_t1_1360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.PublisherMembershipCondition::ToString()
extern "C" String_t* PublisherMembershipCondition_ToString_m1_11654 (PublisherMembershipCondition_t1_1360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.PublisherMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * PublisherMembershipCondition_ToXml_m1_11655 (PublisherMembershipCondition_t1_1360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.PublisherMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * PublisherMembershipCondition_ToXml_m1_11656 (PublisherMembershipCondition_t1_1360 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
