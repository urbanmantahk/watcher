﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_MarshalByRefObject.h"

// System.Runtime.Remoting.ObjectHandle
struct  ObjectHandle_t1_1019  : public MarshalByRefObject_t1_69
{
	// System.Object System.Runtime.Remoting.ObjectHandle::_wrapped
	Object_t * ____wrapped_1;
};
