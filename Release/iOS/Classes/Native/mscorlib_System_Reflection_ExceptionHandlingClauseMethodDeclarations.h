﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ExceptionHandlingClause
struct ExceptionHandlingClause_t1_597;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_ExceptionHandlingClauseOptions.h"

// System.Void System.Reflection.ExceptionHandlingClause::.ctor()
extern "C" void ExceptionHandlingClause__ctor_m1_6832 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.ExceptionHandlingClause::get_CatchType()
extern "C" Type_t * ExceptionHandlingClause_get_CatchType_m1_6833 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ExceptionHandlingClause::get_FilterOffset()
extern "C" int32_t ExceptionHandlingClause_get_FilterOffset_m1_6834 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ExceptionHandlingClauseOptions System.Reflection.ExceptionHandlingClause::get_Flags()
extern "C" int32_t ExceptionHandlingClause_get_Flags_m1_6835 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ExceptionHandlingClause::get_HandlerLength()
extern "C" int32_t ExceptionHandlingClause_get_HandlerLength_m1_6836 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ExceptionHandlingClause::get_HandlerOffset()
extern "C" int32_t ExceptionHandlingClause_get_HandlerOffset_m1_6837 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ExceptionHandlingClause::get_TryLength()
extern "C" int32_t ExceptionHandlingClause_get_TryLength_m1_6838 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.ExceptionHandlingClause::get_TryOffset()
extern "C" int32_t ExceptionHandlingClause_get_TryOffset_m1_6839 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.ExceptionHandlingClause::ToString()
extern "C" String_t* ExceptionHandlingClause_ToString_m1_6840 (ExceptionHandlingClause_t1_597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
