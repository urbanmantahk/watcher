﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Versioning_ResourceScope.h"

// System.Int32 System.Runtime.Versioning.VersioningHelper::GetDomainId()
extern "C" int32_t VersioningHelper_GetDomainId_m1_9692 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Versioning.VersioningHelper::GetProcessId()
extern "C" int32_t VersioningHelper_GetProcessId_m1_9693 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Versioning.VersioningHelper::SafeName(System.String,System.Boolean,System.Boolean)
extern "C" String_t* VersioningHelper_SafeName_m1_9694 (Object_t * __this /* static, unused */, String_t* ___name, bool ___process, bool ___appdomain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Versioning.VersioningHelper::ConvertFromMachine(System.String,System.Runtime.Versioning.ResourceScope,System.Type)
extern "C" String_t* VersioningHelper_ConvertFromMachine_m1_9695 (Object_t * __this /* static, unused */, String_t* ___name, int32_t ___to, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Versioning.VersioningHelper::ConvertFromProcess(System.String,System.Runtime.Versioning.ResourceScope,System.Type)
extern "C" String_t* VersioningHelper_ConvertFromProcess_m1_9696 (Object_t * __this /* static, unused */, String_t* ___name, int32_t ___to, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Versioning.VersioningHelper::ConvertFromAppDomain(System.String,System.Runtime.Versioning.ResourceScope,System.Type)
extern "C" String_t* VersioningHelper_ConvertFromAppDomain_m1_9697 (Object_t * __this /* static, unused */, String_t* ___name, int32_t ___to, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Versioning.VersioningHelper::MakeVersionSafeName(System.String,System.Runtime.Versioning.ResourceScope,System.Runtime.Versioning.ResourceScope)
extern "C" String_t* VersioningHelper_MakeVersionSafeName_m1_9698 (Object_t * __this /* static, unused */, String_t* ___name, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Versioning.VersioningHelper::MakeVersionSafeName(System.String,System.Runtime.Versioning.ResourceScope,System.Runtime.Versioning.ResourceScope,System.Type)
extern "C" String_t* VersioningHelper_MakeVersionSafeName_m1_9699 (Object_t * __this /* static, unused */, String_t* ___name, int32_t ___from, int32_t ___to, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
