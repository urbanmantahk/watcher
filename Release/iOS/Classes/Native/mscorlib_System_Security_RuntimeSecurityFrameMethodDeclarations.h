﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.RuntimeSecurityFrame
struct RuntimeSecurityFrame_t1_1402;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.RuntimeSecurityFrame::.ctor()
extern "C" void RuntimeSecurityFrame__ctor_m1_12099 (RuntimeSecurityFrame_t1_1402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
