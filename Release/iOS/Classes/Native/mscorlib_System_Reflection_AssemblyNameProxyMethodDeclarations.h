﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.AssemblyNameProxy
struct AssemblyNameProxy_t1_580;
// System.Reflection.AssemblyName
struct AssemblyName_t1_576;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.AssemblyNameProxy::.ctor()
extern "C" void AssemblyNameProxy__ctor_m1_6736 (AssemblyNameProxy_t1_580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.AssemblyNameProxy::GetAssemblyName(System.String)
extern "C" AssemblyName_t1_576 * AssemblyNameProxy_GetAssemblyName_m1_6737 (AssemblyNameProxy_t1_580 * __this, String_t* ___assemblyFile, const MethodInfo* method) IL2CPP_METHOD_ATTR;
