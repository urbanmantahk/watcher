﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// System.Object
struct Object_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.Utility.DownloadTexture/Completion::.ctor(System.Object,System.IntPtr)
extern "C" void Completion__ctor_m8_925 (Completion_t8_161 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture/Completion::Invoke(UnityEngine.Texture2D,System.String)
extern "C" void Completion_Invoke_m8_926 (Completion_t8_161 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_Completion_t8_161(Il2CppObject* delegate, Texture2D_t6_33 * ____texture, String_t* ____error);
// System.IAsyncResult VoxelBusters.Utility.DownloadTexture/Completion::BeginInvoke(UnityEngine.Texture2D,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * Completion_BeginInvoke_m8_927 (Completion_t8_161 * __this, Texture2D_t6_33 * ____texture, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTexture/Completion::EndInvoke(System.IAsyncResult)
extern "C" void Completion_EndInvoke_m8_928 (Completion_t8_161 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
