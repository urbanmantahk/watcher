﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.MembershipConditionHelper
struct MembershipConditionHelper_t1_1352;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.String
struct String_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.MembershipConditionHelper::.ctor()
extern "C" void MembershipConditionHelper__ctor_m1_11548 (MembershipConditionHelper_t1_1352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.MembershipConditionHelper::.cctor()
extern "C" void MembershipConditionHelper__cctor_m1_11549 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.MembershipConditionHelper::CheckSecurityElement(System.Security.SecurityElement,System.String,System.Int32,System.Int32)
extern "C" int32_t MembershipConditionHelper_CheckSecurityElement_m1_11550 (Object_t * __this /* static, unused */, SecurityElement_t1_242 * ___se, String_t* ___parameterName, int32_t ___minimumVersion, int32_t ___maximumVersion, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.MembershipConditionHelper::Element(System.Type,System.Int32)
extern "C" SecurityElement_t1_242 * MembershipConditionHelper_Element_m1_11551 (Object_t * __this /* static, unused */, Type_t * ___type, int32_t ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
