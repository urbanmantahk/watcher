﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Reflection_Emit_FlowControl.h"

// System.Reflection.Emit.FlowControl
struct  FlowControl_t1_508 
{
	// System.Int32 System.Reflection.Emit.FlowControl::value__
	int32_t ___value___1;
};
