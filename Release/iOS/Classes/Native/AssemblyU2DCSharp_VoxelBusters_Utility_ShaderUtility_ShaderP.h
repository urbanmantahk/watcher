﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_ShaderUtility_eShader.h"

// VoxelBusters.Utility.ShaderUtility/ShaderProperty
struct  ShaderProperty_t8_27  : public Object_t
{
	// System.String VoxelBusters.Utility.ShaderUtility/ShaderProperty::m_name
	String_t* ___m_name_0;
	// VoxelBusters.Utility.ShaderUtility/eShaderPropertyType VoxelBusters.Utility.ShaderUtility/ShaderProperty::m_type
	int32_t ___m_type_1;
};
