﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.CannotUnloadAppDomainException
struct CannotUnloadAppDomainException_t1_1510;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.CannotUnloadAppDomainException::.ctor()
extern "C" void CannotUnloadAppDomainException__ctor_m1_13343 (CannotUnloadAppDomainException_t1_1510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CannotUnloadAppDomainException::.ctor(System.String)
extern "C" void CannotUnloadAppDomainException__ctor_m1_13344 (CannotUnloadAppDomainException_t1_1510 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CannotUnloadAppDomainException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void CannotUnloadAppDomainException__ctor_m1_13345 (CannotUnloadAppDomainException_t1_1510 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.CannotUnloadAppDomainException::.ctor(System.String,System.Exception)
extern "C" void CannotUnloadAppDomainException__ctor_m1_13346 (CannotUnloadAppDomainException_t1_1510 * __this, String_t* ___message, Exception_t1_33 * ___innerException, const MethodInfo* method) IL2CPP_METHOD_ATTR;
