﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"

// System.Security.SecureString
struct  SecureString_t1_1197  : public CriticalFinalizerObject_t1_713
{
	// System.Int32 System.Security.SecureString::length
	int32_t ___length_2;
	// System.Boolean System.Security.SecureString::disposed
	bool ___disposed_3;
	// System.Boolean System.Security.SecureString::read_only
	bool ___read_only_4;
	// System.Byte[] System.Security.SecureString::data
	ByteU5BU5D_t1_109* ___data_5;
};
