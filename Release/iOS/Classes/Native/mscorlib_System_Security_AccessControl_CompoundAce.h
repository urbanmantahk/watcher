﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_AccessControl_KnownAce.h"
#include "mscorlib_System_Security_AccessControl_CompoundAceType.h"

// System.Security.AccessControl.CompoundAce
struct  CompoundAce_t1_1135  : public KnownAce_t1_1136
{
	// System.Security.AccessControl.CompoundAceType System.Security.AccessControl.CompoundAce::compound_ace_type
	int32_t ___compound_ace_type_6;
};
