﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_1830;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_9.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_20950_gshared (Enumerator_t1_2388 * __this, Dictionary_2_t1_1830 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_20950(__this, ___host, method) (( void (*) (Enumerator_t1_2388 *, Dictionary_2_t1_1830 *, const MethodInfo*))Enumerator__ctor_m1_20950_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_20951_gshared (Enumerator_t1_2388 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_20951(__this, method) (( Object_t * (*) (Enumerator_t1_2388 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_20951_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_20952_gshared (Enumerator_t1_2388 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_20952(__this, method) (( void (*) (Enumerator_t1_2388 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_20952_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_20953_gshared (Enumerator_t1_2388 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_20953(__this, method) (( void (*) (Enumerator_t1_2388 *, const MethodInfo*))Enumerator_Dispose_m1_20953_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_20954_gshared (Enumerator_t1_2388 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_20954(__this, method) (( bool (*) (Enumerator_t1_2388 *, const MethodInfo*))Enumerator_MoveNext_m1_20954_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m1_20955_gshared (Enumerator_t1_2388 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_20955(__this, method) (( int32_t (*) (Enumerator_t1_2388 *, const MethodInfo*))Enumerator_get_Current_m1_20955_gshared)(__this, method)
