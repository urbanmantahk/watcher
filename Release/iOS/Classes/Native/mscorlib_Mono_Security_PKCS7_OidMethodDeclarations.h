﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.PKCS7/Oid
struct Oid_t1_221;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.PKCS7/Oid::.ctor()
extern "C" void Oid__ctor_m1_2473 (Oid_t1_221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
