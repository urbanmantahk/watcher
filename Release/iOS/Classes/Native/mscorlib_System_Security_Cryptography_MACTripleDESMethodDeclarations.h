﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.MACTripleDES
struct MACTripleDES_t1_1220;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"

// System.Void System.Security.Cryptography.MACTripleDES::.ctor()
extern "C" void MACTripleDES__ctor_m1_10407 (MACTripleDES_t1_1220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::.ctor(System.Byte[])
extern "C" void MACTripleDES__ctor_m1_10408 (MACTripleDES_t1_1220 * __this, ByteU5BU5D_t1_109* ___rgbKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::.ctor(System.String,System.Byte[])
extern "C" void MACTripleDES__ctor_m1_10409 (MACTripleDES_t1_1220 * __this, String_t* ___strTripleDES, ByteU5BU5D_t1_109* ___rgbKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::Setup(System.String,System.Byte[])
extern "C" void MACTripleDES_Setup_m1_10410 (MACTripleDES_t1_1220 * __this, String_t* ___strTripleDES, ByteU5BU5D_t1_109* ___rgbKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::Finalize()
extern "C" void MACTripleDES_Finalize_m1_10411 (MACTripleDES_t1_1220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.PaddingMode System.Security.Cryptography.MACTripleDES::get_Padding()
extern "C" int32_t MACTripleDES_get_Padding_m1_10412 (MACTripleDES_t1_1220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::set_Padding(System.Security.Cryptography.PaddingMode)
extern "C" void MACTripleDES_set_Padding_m1_10413 (MACTripleDES_t1_1220 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::Dispose(System.Boolean)
extern "C" void MACTripleDES_Dispose_m1_10414 (MACTripleDES_t1_1220 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::Initialize()
extern "C" void MACTripleDES_Initialize_m1_10415 (MACTripleDES_t1_1220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.MACTripleDES::HashCore(System.Byte[],System.Int32,System.Int32)
extern "C" void MACTripleDES_HashCore_m1_10416 (MACTripleDES_t1_1220 * __this, ByteU5BU5D_t1_109* ___rgbData, int32_t ___ibStart, int32_t ___cbSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.MACTripleDES::HashFinal()
extern "C" ByteU5BU5D_t1_109* MACTripleDES_HashFinal_m1_10417 (MACTripleDES_t1_1220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
