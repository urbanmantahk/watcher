﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t4_12;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdNMTokens::.ctor()
extern "C" void XsdNMTokens__ctor_m4_17 (XsdNMTokens_t4_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdNMTokens::get_TokenizedType()
extern "C" int32_t XsdNMTokens_get_TokenizedType_m4_18 (XsdNMTokens_t4_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
