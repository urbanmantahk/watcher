﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CADMessageBase
struct CADMessageBase_t1_924;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.Collections.ArrayList
struct ArrayList_t1_170;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Runtime.Remoting.Messaging.IMethodMessage
struct IMethodMessage_t1_948;
// System.Runtime.Remoting.Messaging.LogicalCallContext
struct LogicalCallContext_t1_941;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CADMessageBase::.ctor()
extern "C" void CADMessageBase__ctor_m1_8297 (CADMessageBase_t1_924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Messaging.CADMessageBase::MarshalProperties(System.Collections.IDictionary,System.Collections.ArrayList&)
extern "C" int32_t CADMessageBase_MarshalProperties_m1_8298 (Object_t * __this /* static, unused */, Object_t * ___dict, ArrayList_t1_170 ** ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CADMessageBase::UnmarshalProperties(System.Collections.IDictionary,System.Int32,System.Collections.ArrayList)
extern "C" void CADMessageBase_UnmarshalProperties_m1_8299 (Object_t * __this /* static, unused */, Object_t * ___dict, int32_t ___count, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.Messaging.CADMessageBase::IsPossibleToIgnoreMarshal(System.Object)
extern "C" bool CADMessageBase_IsPossibleToIgnoreMarshal_m1_8300 (Object_t * __this /* static, unused */, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CADMessageBase::MarshalArgument(System.Object,System.Collections.ArrayList&)
extern "C" Object_t * CADMessageBase_MarshalArgument_m1_8301 (CADMessageBase_t1_924 * __this, Object_t * ___arg, ArrayList_t1_170 ** ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CADMessageBase::UnmarshalArgument(System.Object,System.Collections.ArrayList)
extern "C" Object_t * CADMessageBase_UnmarshalArgument_m1_8302 (CADMessageBase_t1_924 * __this, Object_t * ___arg, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.CADMessageBase::MarshalArguments(System.Object[],System.Collections.ArrayList&)
extern "C" ObjectU5BU5D_t1_272* CADMessageBase_MarshalArguments_m1_8303 (CADMessageBase_t1_924 * __this, ObjectU5BU5D_t1_272* ___arguments, ArrayList_t1_170 ** ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.Messaging.CADMessageBase::UnmarshalArguments(System.Object[],System.Collections.ArrayList)
extern "C" ObjectU5BU5D_t1_272* CADMessageBase_UnmarshalArguments_m1_8304 (CADMessageBase_t1_924 * __this, ObjectU5BU5D_t1_272* ___arguments, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CADMessageBase::SaveLogicalCallContext(System.Runtime.Remoting.Messaging.IMethodMessage,System.Collections.ArrayList&)
extern "C" void CADMessageBase_SaveLogicalCallContext_m1_8305 (CADMessageBase_t1_924 * __this, Object_t * ___msg, ArrayList_t1_170 ** ___serializeList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.LogicalCallContext System.Runtime.Remoting.Messaging.CADMessageBase::GetLogicalCallContext(System.Collections.ArrayList)
extern "C" LogicalCallContext_t1_941 * CADMessageBase_GetLogicalCallContext_m1_8306 (CADMessageBase_t1_924 * __this, ArrayList_t1_170 * ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
