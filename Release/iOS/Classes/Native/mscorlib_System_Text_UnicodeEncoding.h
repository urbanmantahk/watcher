﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Text_Encoding.h"

// System.Text.UnicodeEncoding
struct  UnicodeEncoding_t1_1455  : public Encoding_t1_406
{
	// System.Boolean System.Text.UnicodeEncoding::bigEndian
	bool ___bigEndian_32;
	// System.Boolean System.Text.UnicodeEncoding::byteOrderMark
	bool ___byteOrderMark_33;
};
