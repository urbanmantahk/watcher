﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m6_2093(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t6_359 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m6_1942_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::Invoke(T0)
#define UnityAction_1_Invoke_m6_2094(__this, ___arg0, method) (( void (*) (UnityAction_1_t6_359 *, List_1_t1_1834 *, const MethodInfo*))UnityAction_1_Invoke_m6_1943_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m6_2095(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t6_359 *, List_1_t1_1834 *, AsyncCallback_t1_28 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m6_1944_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m6_2096(__this, ___result, method) (( void (*) (UnityAction_1_t6_359 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m6_1945_gshared)(__this, ___result, method)
