﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Serialization.OptionalFieldAttribute
struct OptionalFieldAttribute_t1_1090;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Serialization.OptionalFieldAttribute::.ctor()
extern "C" void OptionalFieldAttribute__ctor_m1_9587 (OptionalFieldAttribute_t1_1090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Serialization.OptionalFieldAttribute::get_VersionAdded()
extern "C" int32_t OptionalFieldAttribute_get_VersionAdded_m1_9588 (OptionalFieldAttribute_t1_1090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.OptionalFieldAttribute::set_VersionAdded(System.Int32)
extern "C" void OptionalFieldAttribute_set_VersionAdded_m1_9589 (OptionalFieldAttribute_t1_1090 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
