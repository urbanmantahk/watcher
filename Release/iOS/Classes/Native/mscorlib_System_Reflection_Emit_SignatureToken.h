﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_SignatureToken.h"

// System.Reflection.Emit.SignatureToken
struct  SignatureToken_t1_552 
{
	// System.Int32 System.Reflection.Emit.SignatureToken::tokValue
	int32_t ___tokValue_0;
};
struct SignatureToken_t1_552_StaticFields{
	// System.Reflection.Emit.SignatureToken System.Reflection.Emit.SignatureToken::Empty
	SignatureToken_t1_552  ___Empty_1;
};
