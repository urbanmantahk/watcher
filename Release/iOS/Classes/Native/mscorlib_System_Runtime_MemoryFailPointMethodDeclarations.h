﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.MemoryFailPoint
struct MemoryFailPoint_t1_1106;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.MemoryFailPoint::.ctor(System.Int32)
extern "C" void MemoryFailPoint__ctor_m1_9703 (MemoryFailPoint_t1_1106 * __this, int32_t ___sizeInMegabytes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.MemoryFailPoint::Finalize()
extern "C" void MemoryFailPoint_Finalize_m1_9704 (MemoryFailPoint_t1_1106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.MemoryFailPoint::Dispose()
extern "C" void MemoryFailPoint_Dispose_m1_9705 (MemoryFailPoint_t1_1106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
