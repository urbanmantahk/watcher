﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boomlagoon.JSON.JSONArray
struct JSONArray_t8_6;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// Boomlagoon.JSON.JSONValue
struct JSONValue_t8_4;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<Boomlagoon.JSON.JSONValue>
struct IEnumerator_1_t1_1914;

#include "codegen/il2cpp-codegen.h"

// System.Void Boomlagoon.JSON.JSONArray::.ctor()
extern "C" void JSONArray__ctor_m8_29 (JSONArray_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONArray::.ctor(Boomlagoon.JSON.JSONArray)
extern "C" void JSONArray__ctor_m8_30 (JSONArray_t8_6 * __this, JSONArray_t8_6 * ___array, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Boomlagoon.JSON.JSONArray::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * JSONArray_System_Collections_IEnumerable_GetEnumerator_m8_31 (JSONArray_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONArray::Add(Boomlagoon.JSON.JSONValue)
extern "C" void JSONArray_Add_m8_32 (JSONArray_t8_6 * __this, JSONValue_t8_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONValue Boomlagoon.JSON.JSONArray::get_Item(System.Int32)
extern "C" JSONValue_t8_4 * JSONArray_get_Item_m8_33 (JSONArray_t8_6 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONArray::set_Item(System.Int32,Boomlagoon.JSON.JSONValue)
extern "C" void JSONArray_set_Item_m8_34 (JSONArray_t8_6 * __this, int32_t ___index, JSONValue_t8_4 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Boomlagoon.JSON.JSONArray::get_Length()
extern "C" int32_t JSONArray_get_Length_m8_35 (JSONArray_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Boomlagoon.JSON.JSONArray::ToString()
extern "C" String_t* JSONArray_ToString_m8_36 (JSONArray_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Boomlagoon.JSON.JSONValue> Boomlagoon.JSON.JSONArray::GetEnumerator()
extern "C" Object_t* JSONArray_GetEnumerator_m8_37 (JSONArray_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONArray::Parse(System.String)
extern "C" JSONArray_t8_6 * JSONArray_Parse_m8_38 (Object_t * __this /* static, unused */, String_t* ___jsonString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONArray::Clear()
extern "C" void JSONArray_Clear_m8_39 (JSONArray_t8_6 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boomlagoon.JSON.JSONArray::Remove(System.Int32)
extern "C" void JSONArray_Remove_m8_40 (JSONArray_t8_6 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Boomlagoon.JSON.JSONArray Boomlagoon.JSON.JSONArray::op_Addition(Boomlagoon.JSON.JSONArray,Boomlagoon.JSON.JSONArray)
extern "C" JSONArray_t8_6 * JSONArray_op_Addition_m8_41 (Object_t * __this /* static, unused */, JSONArray_t8_6 * ___lhs, JSONArray_t8_6 * ___rhs, const MethodInfo* method) IL2CPP_METHOD_ATTR;
