﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ArrayList_ArrayListWrapper.h"

// System.Collections.ArrayList/RangedArrayList
struct  RangedArrayList_t1_267  : public ArrayListWrapper_t1_263
{
	// System.Int32 System.Collections.ArrayList/RangedArrayList::m_InnerIndex
	int32_t ___m_InnerIndex_6;
	// System.Int32 System.Collections.ArrayList/RangedArrayList::m_InnerCount
	int32_t ___m_InnerCount_7;
	// System.Int32 System.Collections.ArrayList/RangedArrayList::m_InnerStateChanges
	int32_t ___m_InnerStateChanges_8;
};
