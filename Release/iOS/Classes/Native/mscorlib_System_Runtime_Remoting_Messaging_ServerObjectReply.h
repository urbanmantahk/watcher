﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1_70;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Messaging.ServerObjectReplySink
struct  ServerObjectReplySink_t1_963  : public Object_t
{
	// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.ServerObjectReplySink::_replySink
	Object_t * ____replySink_0;
	// System.Runtime.Remoting.ServerIdentity System.Runtime.Remoting.Messaging.ServerObjectReplySink::_identity
	ServerIdentity_t1_70 * ____identity_1;
};
