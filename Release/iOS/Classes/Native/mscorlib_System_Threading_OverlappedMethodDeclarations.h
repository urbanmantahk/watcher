﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Overlapped
struct Overlapped_t1_1472;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.Threading.IOCompletionCallback
struct IOCompletionCallback_t1_1625;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Threading_NativeOverlapped.h"

// System.Void System.Threading.Overlapped::.ctor()
extern "C" void Overlapped__ctor_m1_12725 (Overlapped_t1_1472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::.ctor(System.Int32,System.Int32,System.Int32,System.IAsyncResult)
extern "C" void Overlapped__ctor_m1_12726 (Overlapped_t1_1472 * __this, int32_t ___offsetLo, int32_t ___offsetHi, int32_t ___hEvent, Object_t * ___ar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::.ctor(System.Int32,System.Int32,System.IntPtr,System.IAsyncResult)
extern "C" void Overlapped__ctor_m1_12727 (Overlapped_t1_1472 * __this, int32_t ___offsetLo, int32_t ___offsetHi, IntPtr_t ___hEvent, Object_t * ___ar, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::Free(System.Threading.NativeOverlapped*)
extern "C" void Overlapped_Free_m1_12728 (Object_t * __this /* static, unused */, NativeOverlapped_t1_1471 * ___nativeOverlappedPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Overlapped System.Threading.Overlapped::Unpack(System.Threading.NativeOverlapped*)
extern "C" Overlapped_t1_1472 * Overlapped_Unpack_m1_12729 (Object_t * __this /* static, unused */, NativeOverlapped_t1_1471 * ___nativeOverlappedPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.NativeOverlapped* System.Threading.Overlapped::Pack(System.Threading.IOCompletionCallback)
extern "C" NativeOverlapped_t1_1471 * Overlapped_Pack_m1_12730 (Overlapped_t1_1472 * __this, IOCompletionCallback_t1_1625 * ___iocb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.NativeOverlapped* System.Threading.Overlapped::Pack(System.Threading.IOCompletionCallback,System.Object)
extern "C" NativeOverlapped_t1_1471 * Overlapped_Pack_m1_12731 (Overlapped_t1_1472 * __this, IOCompletionCallback_t1_1625 * ___iocb, Object_t * ___userData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.NativeOverlapped* System.Threading.Overlapped::UnsafePack(System.Threading.IOCompletionCallback)
extern "C" NativeOverlapped_t1_1471 * Overlapped_UnsafePack_m1_12732 (Overlapped_t1_1472 * __this, IOCompletionCallback_t1_1625 * ___iocb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.NativeOverlapped* System.Threading.Overlapped::UnsafePack(System.Threading.IOCompletionCallback,System.Object)
extern "C" NativeOverlapped_t1_1471 * Overlapped_UnsafePack_m1_12733 (Overlapped_t1_1472 * __this, IOCompletionCallback_t1_1625 * ___iocb, Object_t * ___userData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Threading.Overlapped::get_AsyncResult()
extern "C" Object_t * Overlapped_get_AsyncResult_m1_12734 (Overlapped_t1_1472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::set_AsyncResult(System.IAsyncResult)
extern "C" void Overlapped_set_AsyncResult_m1_12735 (Overlapped_t1_1472 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Overlapped::get_EventHandle()
extern "C" int32_t Overlapped_get_EventHandle_m1_12736 (Overlapped_t1_1472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::set_EventHandle(System.Int32)
extern "C" void Overlapped_set_EventHandle_m1_12737 (Overlapped_t1_1472 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Threading.Overlapped::get_EventHandleIntPtr()
extern "C" IntPtr_t Overlapped_get_EventHandleIntPtr_m1_12738 (Overlapped_t1_1472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::set_EventHandleIntPtr(System.IntPtr)
extern "C" void Overlapped_set_EventHandleIntPtr_m1_12739 (Overlapped_t1_1472 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Overlapped::get_OffsetHigh()
extern "C" int32_t Overlapped_get_OffsetHigh_m1_12740 (Overlapped_t1_1472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::set_OffsetHigh(System.Int32)
extern "C" void Overlapped_set_OffsetHigh_m1_12741 (Overlapped_t1_1472 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Overlapped::get_OffsetLow()
extern "C" int32_t Overlapped_get_OffsetLow_m1_12742 (Overlapped_t1_1472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Overlapped::set_OffsetLow(System.Int32)
extern "C" void Overlapped_set_OffsetLow_m1_12743 (Overlapped_t1_1472 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
