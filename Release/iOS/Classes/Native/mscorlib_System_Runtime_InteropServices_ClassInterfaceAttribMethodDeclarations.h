﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.ClassInterfaceAttribute
struct ClassInterfaceAttribute_t1_765;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"

// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Int16)
extern "C" void ClassInterfaceAttribute__ctor_m1_7596 (ClassInterfaceAttribute_t1_765 * __this, int16_t ___classInterfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.ClassInterfaceAttribute::.ctor(System.Runtime.InteropServices.ClassInterfaceType)
extern "C" void ClassInterfaceAttribute__ctor_m1_7597 (ClassInterfaceAttribute_t1_765 * __this, int32_t ___classInterfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.ClassInterfaceType System.Runtime.InteropServices.ClassInterfaceAttribute::get_Value()
extern "C" int32_t ClassInterfaceAttribute_get_Value_m1_7598 (ClassInterfaceAttribute_t1_765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
