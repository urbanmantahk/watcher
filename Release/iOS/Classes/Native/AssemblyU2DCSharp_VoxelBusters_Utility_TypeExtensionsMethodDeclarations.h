﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TypeCode.h"

// System.Void VoxelBusters.Utility.TypeExtensions::.cctor()
extern "C" void TypeExtensions__cctor_m8_245 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object VoxelBusters.Utility.TypeExtensions::DefaultValue(System.Type)
extern "C" Object_t * TypeExtensions_DefaultValue_m8_246 (Object_t * __this /* static, unused */, Type_t * ____type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type VoxelBusters.Utility.TypeExtensions::GetTypeFromTypeCode(System.TypeCode)
extern "C" Type_t * TypeExtensions_GetTypeFromTypeCode_m8_247 (Object_t * __this /* static, unused */, int32_t ____typeCode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
