﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Decoder
struct Decoder_t1_407;
// System.Text.DecoderFallback
struct DecoderFallback_t1_1420;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1_1421;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Char[]
struct CharU5BU5D_t1_16;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.Decoder::.ctor()
extern "C" void Decoder__ctor_m1_12200 (Decoder_t1_407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.Decoder::get_Fallback()
extern "C" DecoderFallback_t1_1420 * Decoder_get_Fallback_m1_12201 (Decoder_t1_407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::set_Fallback(System.Text.DecoderFallback)
extern "C" void Decoder_set_Fallback_m1_12202 (Decoder_t1_407 * __this, DecoderFallback_t1_1420 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.Decoder::get_FallbackBuffer()
extern "C" DecoderFallbackBuffer_t1_1421 * Decoder_get_FallbackBuffer_m1_12203 (Decoder_t1_407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetCharCount(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C" int32_t Decoder_GetCharCount_m1_12204 (Decoder_t1_407 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___index, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetCharCount(System.Byte*,System.Int32,System.Boolean)
extern "C" int32_t Decoder_GetCharCount_m1_12205 (Decoder_t1_407 * __this, uint8_t* ___bytes, int32_t ___count, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Boolean)
extern "C" int32_t Decoder_GetChars_m1_12206 (Decoder_t1_407 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetChars(System.Byte*,System.Int32,System.Char*,System.Int32,System.Boolean)
extern "C" int32_t Decoder_GetChars_m1_12207 (Decoder_t1_407 * __this, uint8_t* ___bytes, int32_t ___byteCount, uint16_t* ___chars, int32_t ___charCount, bool ___flush, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::Reset()
extern "C" void Decoder_Reset_m1_12208 (Decoder_t1_407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::Convert(System.Byte*,System.Int32,System.Char*,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&)
extern "C" void Decoder_Convert_m1_12209 (Decoder_t1_407 * __this, uint8_t* ___bytes, int32_t ___byteCount, uint16_t* ___chars, int32_t ___charCount, bool ___flush, int32_t* ___bytesUsed, int32_t* ___charsUsed, bool* ___completed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::Convert(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&)
extern "C" void Decoder_Convert_m1_12210 (Decoder_t1_407 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, int32_t ___charCount, bool ___flush, int32_t* ___bytesUsed, int32_t* ___charsUsed, bool* ___completed, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::CheckArguments(System.Char[],System.Int32)
extern "C" void Decoder_CheckArguments_m1_12211 (Decoder_t1_407 * __this, CharU5BU5D_t1_16* ___chars, int32_t ___charIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::CheckArguments(System.Byte[],System.Int32,System.Int32)
extern "C" void Decoder_CheckArguments_m1_12212 (Decoder_t1_407 * __this, ByteU5BU5D_t1_109* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::CheckArguments(System.Char*,System.Int32,System.Byte*,System.Int32)
extern "C" void Decoder_CheckArguments_m1_12213 (Decoder_t1_407 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
