﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.EventBuilder
struct EventBuilder_t1_500;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_Emit_EventToken.h"

// System.Void System.Reflection.Emit.EventBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.String,System.Reflection.EventAttributes,System.Type)
extern "C" void EventBuilder__ctor_m1_5797 (EventBuilder_t1_500 * __this, TypeBuilder_t1_481 * ___tb, String_t* ___eventName, int32_t ___eventAttrs, Type_t * ___eventType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::System.Runtime.InteropServices._EventBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void EventBuilder_System_Runtime_InteropServices__EventBuilder_GetIDsOfNames_m1_5798 (EventBuilder_t1_500 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::System.Runtime.InteropServices._EventBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void EventBuilder_System_Runtime_InteropServices__EventBuilder_GetTypeInfo_m1_5799 (EventBuilder_t1_500 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::System.Runtime.InteropServices._EventBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void EventBuilder_System_Runtime_InteropServices__EventBuilder_GetTypeInfoCount_m1_5800 (EventBuilder_t1_500 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::System.Runtime.InteropServices._EventBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void EventBuilder_System_Runtime_InteropServices__EventBuilder_Invoke_m1_5801 (EventBuilder_t1_500 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.EventBuilder::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C" int32_t EventBuilder_get_next_table_index_m1_5802 (EventBuilder_t1_500 * __this, Object_t * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::AddOtherMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void EventBuilder_AddOtherMethod_m1_5803 (EventBuilder_t1_500 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.EventToken System.Reflection.Emit.EventBuilder::GetEventToken()
extern "C" EventToken_t1_504  EventBuilder_GetEventToken_m1_5804 (EventBuilder_t1_500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::SetAddOnMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void EventBuilder_SetAddOnMethod_m1_5805 (EventBuilder_t1_500 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::SetRaiseMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void EventBuilder_SetRaiseMethod_m1_5806 (EventBuilder_t1_500 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::SetRemoveOnMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void EventBuilder_SetRemoveOnMethod_m1_5807 (EventBuilder_t1_500 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void EventBuilder_SetCustomAttribute_m1_5808 (EventBuilder_t1_500 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void EventBuilder_SetCustomAttribute_m1_5809 (EventBuilder_t1_500 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.EventBuilder::RejectIfCreated()
extern "C" void EventBuilder_RejectIfCreated_m1_5810 (EventBuilder_t1_500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
