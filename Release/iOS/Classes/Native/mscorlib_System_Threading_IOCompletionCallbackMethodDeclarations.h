﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.IOCompletionCallback
struct IOCompletionCallback_t1_1625;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Threading_NativeOverlapped.h"

// System.Void System.Threading.IOCompletionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void IOCompletionCallback__ctor_m1_14819 (IOCompletionCallback_t1_1625 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.IOCompletionCallback::Invoke(System.UInt32,System.UInt32,System.Threading.NativeOverlapped*)
extern "C" void IOCompletionCallback_Invoke_m1_14820 (IOCompletionCallback_t1_1625 * __this, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_IOCompletionCallback_t1_1625(Il2CppObject* delegate, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP);
// System.IAsyncResult System.Threading.IOCompletionCallback::BeginInvoke(System.UInt32,System.UInt32,System.Threading.NativeOverlapped*,System.AsyncCallback,System.Object)
extern "C" Object_t * IOCompletionCallback_BeginInvoke_m1_14821 (IOCompletionCallback_t1_1625 * __this, uint32_t ___errorCode, uint32_t ___numBytes, NativeOverlapped_t1_1471 * ___pOVERLAP, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.IOCompletionCallback::EndInvoke(System.IAsyncResult)
extern "C" void IOCompletionCallback_EndInvoke_m1_14822 (IOCompletionCallback_t1_1625 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
