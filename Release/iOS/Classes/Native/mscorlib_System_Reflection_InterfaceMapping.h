﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Type
struct Type_t;

#include "mscorlib_System_ValueType.h"

// System.Reflection.InterfaceMapping
struct  InterfaceMapping_t1_602 
{
	// System.Reflection.MethodInfo[] System.Reflection.InterfaceMapping::InterfaceMethods
	MethodInfoU5BU5D_t1_603* ___InterfaceMethods_0;
	// System.Type System.Reflection.InterfaceMapping::InterfaceType
	Type_t * ___InterfaceType_1;
	// System.Reflection.MethodInfo[] System.Reflection.InterfaceMapping::TargetMethods
	MethodInfoU5BU5D_t1_603* ___TargetMethods_2;
	// System.Type System.Reflection.InterfaceMapping::TargetType
	Type_t * ___TargetType_3;
};
