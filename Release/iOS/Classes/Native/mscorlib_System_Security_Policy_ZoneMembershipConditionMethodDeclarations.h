﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ZoneMembershipCondition
struct ZoneMembershipCondition_t1_1371;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Void System.Security.Policy.ZoneMembershipCondition::.ctor()
extern "C" void ZoneMembershipCondition__ctor_m1_11771 (ZoneMembershipCondition_t1_1371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ZoneMembershipCondition::.ctor(System.Security.SecurityZone)
extern "C" void ZoneMembershipCondition__ctor_m1_11772 (ZoneMembershipCondition_t1_1371 * __this, int32_t ___zone, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityZone System.Security.Policy.ZoneMembershipCondition::get_SecurityZone()
extern "C" int32_t ZoneMembershipCondition_get_SecurityZone_m1_11773 (ZoneMembershipCondition_t1_1371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ZoneMembershipCondition::set_SecurityZone(System.Security.SecurityZone)
extern "C" void ZoneMembershipCondition_set_SecurityZone_m1_11774 (ZoneMembershipCondition_t1_1371 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ZoneMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool ZoneMembershipCondition_Check_m1_11775 (ZoneMembershipCondition_t1_1371 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.ZoneMembershipCondition::Copy()
extern "C" Object_t * ZoneMembershipCondition_Copy_m1_11776 (ZoneMembershipCondition_t1_1371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ZoneMembershipCondition::Equals(System.Object)
extern "C" bool ZoneMembershipCondition_Equals_m1_11777 (ZoneMembershipCondition_t1_1371 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ZoneMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void ZoneMembershipCondition_FromXml_m1_11778 (ZoneMembershipCondition_t1_1371 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ZoneMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void ZoneMembershipCondition_FromXml_m1_11779 (ZoneMembershipCondition_t1_1371 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ZoneMembershipCondition::GetHashCode()
extern "C" int32_t ZoneMembershipCondition_GetHashCode_m1_11780 (ZoneMembershipCondition_t1_1371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.ZoneMembershipCondition::ToString()
extern "C" String_t* ZoneMembershipCondition_ToString_m1_11781 (ZoneMembershipCondition_t1_1371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.ZoneMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * ZoneMembershipCondition_ToXml_m1_11782 (ZoneMembershipCondition_t1_1371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.ZoneMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * ZoneMembershipCondition_ToXml_m1_11783 (ZoneMembershipCondition_t1_1371 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
