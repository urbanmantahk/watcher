﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.GenericAcl
struct GenericAcl_t1_1114;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Security.AccessControl.GenericAce[]
struct GenericAceU5BU5D_t1_1729;
// System.Security.AccessControl.AceEnumerator
struct AceEnumerator_t1_1113;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.AccessControl.GenericAcl::.ctor()
extern "C" void GenericAcl__ctor_m1_9913 (GenericAcl_t1_1114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.GenericAcl::.cctor()
extern "C" void GenericAcl__cctor_m1_9914 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.GenericAcl::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void GenericAcl_System_Collections_ICollection_CopyTo_m1_9915 (GenericAcl_t1_1114 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.AccessControl.GenericAcl::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * GenericAcl_System_Collections_IEnumerable_GetEnumerator_m1_9916 (GenericAcl_t1_1114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.GenericAcl::get_IsSynchronized()
extern "C" bool GenericAcl_get_IsSynchronized_m1_9917 (GenericAcl_t1_1114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.AccessControl.GenericAcl::get_SyncRoot()
extern "C" Object_t * GenericAcl_get_SyncRoot_m1_9918 (GenericAcl_t1_1114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.GenericAcl::CopyTo(System.Security.AccessControl.GenericAce[],System.Int32)
extern "C" void GenericAcl_CopyTo_m1_9919 (GenericAcl_t1_1114 * __this, GenericAceU5BU5D_t1_1729* ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AceEnumerator System.Security.AccessControl.GenericAcl::GetEnumerator()
extern "C" AceEnumerator_t1_1113 * GenericAcl_GetEnumerator_m1_9920 (GenericAcl_t1_1114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
