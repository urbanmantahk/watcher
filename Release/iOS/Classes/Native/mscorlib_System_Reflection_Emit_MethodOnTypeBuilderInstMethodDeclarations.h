﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.MethodOnTypeBuilderInst
struct MethodOnTypeBuilderInst_t1_530;
// System.Reflection.MonoGenericClass
struct MonoGenericClass_t1_485;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1_1686;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Void System.Reflection.Emit.MethodOnTypeBuilderInst::.ctor(System.Reflection.MonoGenericClass,System.Reflection.Emit.MethodBuilder)
extern "C" void MethodOnTypeBuilderInst__ctor_m1_6115 (MethodOnTypeBuilderInst_t1_530 * __this, MonoGenericClass_t1_485 * ___instantiation, MethodBuilder_t1_501 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.MethodOnTypeBuilderInst::.ctor(System.Reflection.Emit.MethodOnTypeBuilderInst,System.Type[])
extern "C" void MethodOnTypeBuilderInst__ctor_m1_6116 (MethodOnTypeBuilderInst_t1_530 * __this, MethodOnTypeBuilderInst_t1_530 * ___gmd, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.MethodOnTypeBuilderInst::get_DeclaringType()
extern "C" Type_t * MethodOnTypeBuilderInst_get_DeclaringType_m1_6117 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.MethodOnTypeBuilderInst::get_Name()
extern "C" String_t* MethodOnTypeBuilderInst_get_Name_m1_6118 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.MethodOnTypeBuilderInst::get_ReflectedType()
extern "C" Type_t * MethodOnTypeBuilderInst_get_ReflectedType_m1_6119 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.MethodOnTypeBuilderInst::get_ReturnType()
extern "C" Type_t * MethodOnTypeBuilderInst_get_ReturnType_m1_6120 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodOnTypeBuilderInst::IsDefined(System.Type,System.Boolean)
extern "C" bool MethodOnTypeBuilderInst_IsDefined_m1_6121 (MethodOnTypeBuilderInst_t1_530 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.MethodOnTypeBuilderInst::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MethodOnTypeBuilderInst_GetCustomAttributes_m1_6122 (MethodOnTypeBuilderInst_t1_530 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.MethodOnTypeBuilderInst::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* MethodOnTypeBuilderInst_GetCustomAttributes_m1_6123 (MethodOnTypeBuilderInst_t1_530 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.MethodOnTypeBuilderInst::ToString()
extern "C" String_t* MethodOnTypeBuilderInst_ToString_m1_6124 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodImplAttributes System.Reflection.Emit.MethodOnTypeBuilderInst::GetMethodImplementationFlags()
extern "C" int32_t MethodOnTypeBuilderInst_GetMethodImplementationFlags_m1_6125 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.MethodOnTypeBuilderInst::GetParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* MethodOnTypeBuilderInst_GetParameters_m1_6126 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodOnTypeBuilderInst::get_MetadataToken()
extern "C" int32_t MethodOnTypeBuilderInst_get_MetadataToken_m1_6127 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.MethodOnTypeBuilderInst::GetParameterCount()
extern "C" int32_t MethodOnTypeBuilderInst_GetParameterCount_m1_6128 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.MethodOnTypeBuilderInst::Invoke(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * MethodOnTypeBuilderInst_Invoke_m1_6129 (MethodOnTypeBuilderInst_t1_530 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___parameters, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeMethodHandle System.Reflection.Emit.MethodOnTypeBuilderInst::get_MethodHandle()
extern "C" RuntimeMethodHandle_t1_479  MethodOnTypeBuilderInst_get_MethodHandle_m1_6130 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodAttributes System.Reflection.Emit.MethodOnTypeBuilderInst::get_Attributes()
extern "C" int32_t MethodOnTypeBuilderInst_get_Attributes_m1_6131 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.CallingConventions System.Reflection.Emit.MethodOnTypeBuilderInst::get_CallingConvention()
extern "C" int32_t MethodOnTypeBuilderInst_get_CallingConvention_m1_6132 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.MethodOnTypeBuilderInst::MakeGenericMethod(System.Type[])
extern "C" MethodInfo_t * MethodOnTypeBuilderInst_MakeGenericMethod_m1_6133 (MethodOnTypeBuilderInst_t1_530 * __this, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.MethodOnTypeBuilderInst::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* MethodOnTypeBuilderInst_GetGenericArguments_m1_6134 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.MethodOnTypeBuilderInst::GetGenericMethodDefinition()
extern "C" MethodInfo_t * MethodOnTypeBuilderInst_GetGenericMethodDefinition_m1_6135 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodOnTypeBuilderInst::get_ContainsGenericParameters()
extern "C" bool MethodOnTypeBuilderInst_get_ContainsGenericParameters_m1_6136 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodOnTypeBuilderInst::get_IsGenericMethodDefinition()
extern "C" bool MethodOnTypeBuilderInst_get_IsGenericMethodDefinition_m1_6137 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.MethodOnTypeBuilderInst::get_IsGenericMethod()
extern "C" bool MethodOnTypeBuilderInst_get_IsGenericMethod_m1_6138 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.MethodOnTypeBuilderInst::GetBaseDefinition()
extern "C" MethodInfo_t * MethodOnTypeBuilderInst_GetBaseDefinition_m1_6139 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ICustomAttributeProvider System.Reflection.Emit.MethodOnTypeBuilderInst::get_ReturnTypeCustomAttributes()
extern "C" Object_t * MethodOnTypeBuilderInst_get_ReturnTypeCustomAttributes_m1_6140 (MethodOnTypeBuilderInst_t1_530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
