﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Permissions.EnvironmentPermission
struct EnvironmentPermission_t1_1267;
// System.String
struct String_t;
// System.Security.IPermission
struct IPermission_t1_1400;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Collections.ArrayList
struct ArrayList_t1_170;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Permissions_PermissionState.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPermissionAc.h"

// System.Void System.Security.Permissions.EnvironmentPermission::.ctor(System.Security.Permissions.PermissionState)
extern "C" void EnvironmentPermission__ctor_m1_10778 (EnvironmentPermission_t1_1267 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermission::.ctor(System.Security.Permissions.EnvironmentPermissionAccess,System.String)
extern "C" void EnvironmentPermission__ctor_m1_10779 (EnvironmentPermission_t1_1267 * __this, int32_t ___flag, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Permissions.EnvironmentPermission::System.Security.Permissions.IBuiltInPermission.GetTokenIndex()
extern "C" int32_t EnvironmentPermission_System_Security_Permissions_IBuiltInPermission_GetTokenIndex_m1_10780 (EnvironmentPermission_t1_1267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermission::AddPathList(System.Security.Permissions.EnvironmentPermissionAccess,System.String)
extern "C" void EnvironmentPermission_AddPathList_m1_10781 (EnvironmentPermission_t1_1267 * __this, int32_t ___flag, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.EnvironmentPermission::Copy()
extern "C" Object_t * EnvironmentPermission_Copy_m1_10782 (EnvironmentPermission_t1_1267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermission::FromXml(System.Security.SecurityElement)
extern "C" void EnvironmentPermission_FromXml_m1_10783 (EnvironmentPermission_t1_1267 * __this, SecurityElement_t1_242 * ___esd, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.EnvironmentPermission::GetPathList(System.Security.Permissions.EnvironmentPermissionAccess)
extern "C" String_t* EnvironmentPermission_GetPathList_m1_10784 (EnvironmentPermission_t1_1267 * __this, int32_t ___flag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.EnvironmentPermission::Intersect(System.Security.IPermission)
extern "C" Object_t * EnvironmentPermission_Intersect_m1_10785 (EnvironmentPermission_t1_1267 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.EnvironmentPermission::IsSubsetOf(System.Security.IPermission)
extern "C" bool EnvironmentPermission_IsSubsetOf_m1_10786 (EnvironmentPermission_t1_1267 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.EnvironmentPermission::IsUnrestricted()
extern "C" bool EnvironmentPermission_IsUnrestricted_m1_10787 (EnvironmentPermission_t1_1267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermission::SetPathList(System.Security.Permissions.EnvironmentPermissionAccess,System.String)
extern "C" void EnvironmentPermission_SetPathList_m1_10788 (EnvironmentPermission_t1_1267 * __this, int32_t ___flag, String_t* ___pathList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Permissions.EnvironmentPermission::ToXml()
extern "C" SecurityElement_t1_242 * EnvironmentPermission_ToXml_m1_10789 (EnvironmentPermission_t1_1267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.IPermission System.Security.Permissions.EnvironmentPermission::Union(System.Security.IPermission)
extern "C" Object_t * EnvironmentPermission_Union_m1_10790 (EnvironmentPermission_t1_1267 * __this, Object_t * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Permissions.EnvironmentPermission::IsEmpty()
extern "C" bool EnvironmentPermission_IsEmpty_m1_10791 (EnvironmentPermission_t1_1267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.EnvironmentPermission System.Security.Permissions.EnvironmentPermission::Cast(System.Security.IPermission)
extern "C" EnvironmentPermission_t1_1267 * EnvironmentPermission_Cast_m1_10792 (EnvironmentPermission_t1_1267 * __this, Object_t * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Permissions.EnvironmentPermission::ThrowInvalidFlag(System.Security.Permissions.EnvironmentPermissionAccess,System.Boolean)
extern "C" void EnvironmentPermission_ThrowInvalidFlag_m1_10793 (EnvironmentPermission_t1_1267 * __this, int32_t ___flag, bool ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Permissions.EnvironmentPermission::GetPathList(System.Collections.ArrayList)
extern "C" String_t* EnvironmentPermission_GetPathList_m1_10794 (EnvironmentPermission_t1_1267 * __this, ArrayList_t1_170 * ___list, const MethodInfo* method) IL2CPP_METHOD_ATTR;
