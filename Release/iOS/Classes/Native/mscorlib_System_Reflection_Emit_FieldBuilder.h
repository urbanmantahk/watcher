﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Emit.CustomAttributeBuilder[]
struct CustomAttributeBuilderU5BU5D_t1_469;
// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1_505;
// System.Type[]
struct TypeU5BU5D_t1_31;

#include "mscorlib_System_Reflection_FieldInfo.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_RuntimeFieldHandle.h"

// System.Reflection.Emit.FieldBuilder
struct  FieldBuilder_t1_499  : public FieldInfo_t
{
	// System.Reflection.FieldAttributes System.Reflection.Emit.FieldBuilder::attrs
	int32_t ___attrs_0;
	// System.Type System.Reflection.Emit.FieldBuilder::type
	Type_t * ___type_1;
	// System.String System.Reflection.Emit.FieldBuilder::name
	String_t* ___name_2;
	// System.Object System.Reflection.Emit.FieldBuilder::def_value
	Object_t * ___def_value_3;
	// System.Int32 System.Reflection.Emit.FieldBuilder::offset
	int32_t ___offset_4;
	// System.Int32 System.Reflection.Emit.FieldBuilder::table_idx
	int32_t ___table_idx_5;
	// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.FieldBuilder::typeb
	TypeBuilder_t1_481 * ___typeb_6;
	// System.Byte[] System.Reflection.Emit.FieldBuilder::rva_data
	ByteU5BU5D_t1_109* ___rva_data_7;
	// System.Reflection.Emit.CustomAttributeBuilder[] System.Reflection.Emit.FieldBuilder::cattrs
	CustomAttributeBuilderU5BU5D_t1_469* ___cattrs_8;
	// System.Reflection.Emit.UnmanagedMarshal System.Reflection.Emit.FieldBuilder::marshal_info
	UnmanagedMarshal_t1_505 * ___marshal_info_9;
	// System.RuntimeFieldHandle System.Reflection.Emit.FieldBuilder::handle
	RuntimeFieldHandle_t1_36  ___handle_10;
	// System.Type[] System.Reflection.Emit.FieldBuilder::modReq
	TypeU5BU5D_t1_31* ___modReq_11;
	// System.Type[] System.Reflection.Emit.FieldBuilder::modOpt
	TypeU5BU5D_t1_31* ___modOpt_12;
};
