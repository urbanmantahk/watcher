﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#include "mscorlib_System_Reflection_Emit_ILExceptionBlock.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_16209_gshared (InternalEnumerator_1_t1_2053 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_16209(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2053 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_16209_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16210_gshared (InternalEnumerator_1_t1_2053 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16210(__this, method) (( void (*) (InternalEnumerator_1_t1_2053 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_16210_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16211_gshared (InternalEnumerator_1_t1_2053 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16211(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2053 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_16211_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_16212_gshared (InternalEnumerator_1_t1_2053 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_16212(__this, method) (( void (*) (InternalEnumerator_1_t1_2053 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_16212_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_16213_gshared (InternalEnumerator_1_t1_2053 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_16213(__this, method) (( bool (*) (InternalEnumerator_1_t1_2053 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_16213_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILExceptionBlock>::get_Current()
extern "C" ILExceptionBlock_t1_510  InternalEnumerator_1_get_Current_m1_16214_gshared (InternalEnumerator_1_t1_2053 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_16214(__this, method) (( ILExceptionBlock_t1_510  (*) (InternalEnumerator_1_t1_2053 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_16214_gshared)(__this, method)
