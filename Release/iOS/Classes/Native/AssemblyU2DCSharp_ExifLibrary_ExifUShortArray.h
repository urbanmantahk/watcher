﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt16[]
struct UInt16U5BU5D_t1_1231;

#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"

// ExifLibrary.ExifUShortArray
struct  ExifUShortArray_t8_103  : public ExifProperty_t8_99
{
	// System.UInt16[] ExifLibrary.ExifUShortArray::mValue
	UInt16U5BU5D_t1_1231* ___mValue_3;
};
