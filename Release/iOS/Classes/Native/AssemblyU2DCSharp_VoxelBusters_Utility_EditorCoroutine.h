﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t1_193;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.EditorCoroutine
struct  EditorCoroutine_t8_144  : public Object_t
{
	// System.Boolean VoxelBusters.Utility.EditorCoroutine::m_finishedCoroutine
	bool ___m_finishedCoroutine_0;
	// System.Collections.IEnumerator VoxelBusters.Utility.EditorCoroutine::<CoroutineMethod>k__BackingField
	Object_t * ___U3CCoroutineMethodU3Ek__BackingField_1;
};
