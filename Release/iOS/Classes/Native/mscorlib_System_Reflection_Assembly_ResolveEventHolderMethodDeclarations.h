﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t1_560;
// System.Reflection.ModuleResolveEventHandler
struct ModuleResolveEventHandler_t1_561;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.Assembly/ResolveEventHolder::.ctor()
extern "C" void ResolveEventHolder__ctor_m1_6562 (ResolveEventHolder_t1_560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly/ResolveEventHolder::add_ModuleResolve(System.Reflection.ModuleResolveEventHandler)
extern "C" void ResolveEventHolder_add_ModuleResolve_m1_6563 (ResolveEventHolder_t1_560 * __this, ModuleResolveEventHandler_t1_561 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Assembly/ResolveEventHolder::remove_ModuleResolve(System.Reflection.ModuleResolveEventHandler)
extern "C" void ResolveEventHolder_remove_ModuleResolve_m1_6564 (ResolveEventHolder_t1_560 * __this, ModuleResolveEventHandler_t1_561 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
