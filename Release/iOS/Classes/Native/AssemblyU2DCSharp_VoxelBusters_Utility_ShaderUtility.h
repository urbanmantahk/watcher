﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo>
struct List_1_t1_1901;
// System.String[]
struct StringU5BU5D_t1_238;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_AdvancedScriptableObj.h"

// VoxelBusters.Utility.ShaderUtility
struct  ShaderUtility_t8_29  : public AdvancedScriptableObject_1_t8_30
{
	// System.Collections.Generic.List`1<VoxelBusters.Utility.ShaderUtility/ShaderInfo> VoxelBusters.Utility.ShaderUtility::m_shaderInfoList
	List_1_t1_1901 * ___m_shaderInfoList_8;
	// System.String[] VoxelBusters.Utility.ShaderUtility::m_builtInAssetPathList
	StringU5BU5D_t1_238* ___m_builtInAssetPathList_9;
};
