﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_Mutabl.h"

// VoxelBusters.NativePlugins.Internal.AndroidBillingProduct
struct  AndroidBillingProduct_t8_205  : public MutableBillingProduct_t8_206
{
};
