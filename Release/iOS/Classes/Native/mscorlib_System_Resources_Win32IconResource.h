﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Resources.ICONDIRENTRY
struct ICONDIRENTRY_t1_667;

#include "mscorlib_System_Resources_Win32Resource.h"

// System.Resources.Win32IconResource
struct  Win32IconResource_t1_666  : public Win32Resource_t1_664
{
	// System.Resources.ICONDIRENTRY System.Resources.Win32IconResource::icon
	ICONDIRENTRY_t1_667 * ___icon_3;
};
