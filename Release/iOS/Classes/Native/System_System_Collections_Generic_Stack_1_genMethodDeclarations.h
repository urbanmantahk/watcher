﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.String>::.ctor()
#define Stack_1__ctor_m3_28(__this, method) (( void (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1__ctor_m3_1816_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.String>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1817(__this, method) (( bool (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m3_1818_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.String>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1819(__this, method) (( Object_t * (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3_1820_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m3_1821(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3_17 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3_1822_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1823(__this, method) (( Object_t* (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3_1824_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1825(__this, method) (( Object_t * (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3_1826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.String>::Contains(T)
#define Stack_1_Contains_m3_29(__this, ___t, method) (( bool (*) (Stack_1_t3_17 *, String_t*, const MethodInfo*))Stack_1_Contains_m3_1827_gshared)(__this, ___t, method)
// T System.Collections.Generic.Stack`1<System.String>::Peek()
#define Stack_1_Peek_m3_1828(__this, method) (( String_t* (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_Peek_m3_1829_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.String>::Pop()
#define Stack_1_Pop_m3_31(__this, method) (( String_t* (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_Pop_m3_1830_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.String>::Push(T)
#define Stack_1_Push_m3_30(__this, ___t, method) (( void (*) (Stack_1_t3_17 *, String_t*, const MethodInfo*))Stack_1_Push_m3_1831_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.String>::get_Count()
#define Stack_1_get_Count_m3_1832(__this, method) (( int32_t (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_get_Count_m3_1833_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.String>::GetEnumerator()
#define Stack_1_GetEnumerator_m3_1834(__this, method) (( Enumerator_t3_252  (*) (Stack_1_t3_17 *, const MethodInfo*))Stack_1_GetEnumerator_m3_1835_gshared)(__this, method)
