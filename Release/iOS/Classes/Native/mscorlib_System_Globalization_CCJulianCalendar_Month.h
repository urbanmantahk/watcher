﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Globalization_CCJulianCalendar_Month.h"

// System.Globalization.CCJulianCalendar/Month
struct  Month_t1_345 
{
	// System.Int32 System.Globalization.CCJulianCalendar/Month::value__
	int32_t ___value___1;
};
