﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Variant.h"

// System.Void System.Variant::SetValue(System.Object)
extern "C" void Variant_SetValue_m1_14719 (Variant_t1_1617 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Variant::GetValue()
extern "C" Object_t * Variant_GetValue_m1_14720 (Variant_t1_1617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Variant::Clear()
extern "C" void Variant_Clear_m1_14721 (Variant_t1_1617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
