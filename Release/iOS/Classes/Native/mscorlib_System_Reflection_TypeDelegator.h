﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Type.h"

// System.Reflection.TypeDelegator
struct  TypeDelegator_t1_641  : public Type_t
{
	// System.Type System.Reflection.TypeDelegator::typeImpl
	Type_t * ___typeImpl_8;
};
