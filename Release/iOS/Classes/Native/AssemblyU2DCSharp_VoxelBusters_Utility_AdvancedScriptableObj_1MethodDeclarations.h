﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>
struct AdvancedScriptableObject_1_t8_375;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::.ctor()
extern "C" void AdvancedScriptableObject_1__ctor_m8_2047_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method);
#define AdvancedScriptableObject_1__ctor_m8_2047(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_375 *, const MethodInfo*))AdvancedScriptableObject_1__ctor_m8_2047_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::.cctor()
extern "C" void AdvancedScriptableObject_1__cctor_m8_2049_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define AdvancedScriptableObject_1__cctor_m8_2049(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))AdvancedScriptableObject_1__cctor_m8_2049_gshared)(__this /* static, unused */, method)
// T VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::get_Instance()
extern "C" Object_t * AdvancedScriptableObject_1_get_Instance_m8_2051_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define AdvancedScriptableObject_1_get_Instance_m8_2051(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))AdvancedScriptableObject_1_get_Instance_m8_2051_gshared)(__this /* static, unused */, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::Reset()
extern "C" void AdvancedScriptableObject_1_Reset_m8_2053_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method);
#define AdvancedScriptableObject_1_Reset_m8_2053(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_375 *, const MethodInfo*))AdvancedScriptableObject_1_Reset_m8_2053_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::OnEnable()
extern "C" void AdvancedScriptableObject_1_OnEnable_m8_2054_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method);
#define AdvancedScriptableObject_1_OnEnable_m8_2054(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_375 *, const MethodInfo*))AdvancedScriptableObject_1_OnEnable_m8_2054_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::OnDisable()
extern "C" void AdvancedScriptableObject_1_OnDisable_m8_2056_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method);
#define AdvancedScriptableObject_1_OnDisable_m8_2056(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_375 *, const MethodInfo*))AdvancedScriptableObject_1_OnDisable_m8_2056_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::OnDestroy()
extern "C" void AdvancedScriptableObject_1_OnDestroy_m8_2058_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method);
#define AdvancedScriptableObject_1_OnDestroy_m8_2058(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_375 *, const MethodInfo*))AdvancedScriptableObject_1_OnDestroy_m8_2058_gshared)(__this, method)
// System.Void VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::Save()
extern "C" void AdvancedScriptableObject_1_Save_m8_2060_gshared (AdvancedScriptableObject_1_t8_375 * __this, const MethodInfo* method);
#define AdvancedScriptableObject_1_Save_m8_2060(__this, method) (( void (*) (AdvancedScriptableObject_1_t8_375 *, const MethodInfo*))AdvancedScriptableObject_1_Save_m8_2060_gshared)(__this, method)
// T VoxelBusters.Utility.AdvancedScriptableObject`1<System.Object>::GetAsset(System.String)
extern "C" Object_t * AdvancedScriptableObject_1_GetAsset_m8_2062_gshared (Object_t * __this /* static, unused */, String_t* ____assetName, const MethodInfo* method);
#define AdvancedScriptableObject_1_GetAsset_m8_2062(__this /* static, unused */, ____assetName, method) (( Object_t * (*) (Object_t * /* static, unused */, String_t*, const MethodInfo*))AdvancedScriptableObject_1_GetAsset_m8_2062_gshared)(__this /* static, unused */, ____assetName, method)
