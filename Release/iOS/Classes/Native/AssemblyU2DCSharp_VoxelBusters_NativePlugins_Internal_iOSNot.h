﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_CrossPlatformNo_1.h"

// VoxelBusters.NativePlugins.Internal.iOSNotificationPayload
struct  iOSNotificationPayload_t8_267  : public CrossPlatformNotification_t8_259
{
};
