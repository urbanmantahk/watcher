﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityAction
struct UnityAction_t6_259;

#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"

// UnityEngine.Events.InvokableCall
struct  InvokableCall_t6_258  : public BaseInvokableCall_t6_257
{
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t6_259 * ___Delegate_0;
};
