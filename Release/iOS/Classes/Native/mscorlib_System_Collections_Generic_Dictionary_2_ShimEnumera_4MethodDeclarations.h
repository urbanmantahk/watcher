﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>
struct ShimEnumerator_t1_2676;
// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m1_26514_gshared (ShimEnumerator_t1_2676 * __this, Dictionary_2_t1_2663 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m1_26514(__this, ___host, method) (( void (*) (ShimEnumerator_t1_2676 *, Dictionary_2_t1_2663 *, const MethodInfo*))ShimEnumerator__ctor_m1_26514_gshared)(__this, ___host, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void ShimEnumerator_Dispose_m1_26515_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_Dispose_m1_26515(__this, method) (( void (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_Dispose_m1_26515_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m1_26516_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1_26516(__this, method) (( bool (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_MoveNext_m1_26516_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1_284  ShimEnumerator_get_Entry_m1_26517_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1_26517(__this, method) (( DictionaryEntry_t1_284  (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_get_Entry_m1_26517_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m1_26518_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1_26518(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_get_Key_m1_26518_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m1_26519_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1_26519(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_get_Value_m1_26519_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m1_26520_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1_26520(__this, method) (( Object_t * (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_get_Current_m1_26520_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ExifLibrary.ExifTag,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m1_26521_gshared (ShimEnumerator_t1_2676 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1_26521(__this, method) (( void (*) (ShimEnumerator_t1_2676 *, const MethodInfo*))ShimEnumerator_Reset_m1_26521_gshared)(__this, method)
