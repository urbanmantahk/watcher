﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IO.SearchPattern
struct SearchPattern_t1_442;
// System.String
struct String_t;
// System.IO.SearchPattern/Op
struct Op_t1_440;

#include "codegen/il2cpp-codegen.h"

// System.Void System.IO.SearchPattern::.ctor(System.String)
extern "C" void SearchPattern__ctor_m1_5152 (SearchPattern_t1_442 * __this, String_t* ___pattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SearchPattern::.ctor(System.String,System.Boolean)
extern "C" void SearchPattern__ctor_m1_5153 (SearchPattern_t1_442 * __this, String_t* ___pattern, bool ___ignore, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SearchPattern::.cctor()
extern "C" void SearchPattern__cctor_m1_5154 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.SearchPattern::IsMatch(System.String)
extern "C" bool SearchPattern_IsMatch_m1_5155 (SearchPattern_t1_442 * __this, String_t* ___text, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.SearchPattern::Compile(System.String)
extern "C" void SearchPattern_Compile_m1_5156 (SearchPattern_t1_442 * __this, String_t* ___pattern, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IO.SearchPattern::Match(System.IO.SearchPattern/Op,System.String,System.Int32)
extern "C" bool SearchPattern_Match_m1_5157 (SearchPattern_t1_442 * __this, Op_t1_440 * ___op, String_t* ___text, int32_t ___ptr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
