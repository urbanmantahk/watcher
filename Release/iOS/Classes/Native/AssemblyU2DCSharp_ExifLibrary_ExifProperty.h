﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// ExifLibrary.ExifProperty
struct  ExifProperty_t8_99  : public Object_t
{
	// ExifLibrary.ExifTag ExifLibrary.ExifProperty::mTag
	int32_t ___mTag_0;
	// ExifLibrary.IFD ExifLibrary.ExifProperty::mIFD
	int32_t ___mIFD_1;
	// System.String ExifLibrary.ExifProperty::mName
	String_t* ___mName_2;
};
