﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.TextInfo
struct TextInfo_t1_124;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Void.h"

// System.Void System.Globalization.TextInfo::.ctor(System.Globalization.CultureInfo,System.Int32,System.Void*,System.Boolean)
extern "C" void TextInfo__ctor_m1_4499 (TextInfo_t1_124 * __this, CultureInfo_t1_277 * ___ci, int32_t ___lcid, void* ___data, bool ___read_only, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TextInfo::.ctor(System.Globalization.TextInfo)
extern "C" void TextInfo__ctor_m1_4500 (TextInfo_t1_124 * __this, TextInfo_t1_124 * ___textInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TextInfo::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m1_4501 (TextInfo_t1_124 * __this, Object_t * ___sender, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextInfo::get_ANSICodePage()
extern "C" int32_t TextInfo_get_ANSICodePage_m1_4502 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextInfo::get_EBCDICCodePage()
extern "C" int32_t TextInfo_get_EBCDICCodePage_m1_4503 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextInfo::get_LCID()
extern "C" int32_t TextInfo_get_LCID_m1_4504 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextInfo::get_ListSeparator()
extern "C" String_t* TextInfo_get_ListSeparator_m1_4505 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.TextInfo::set_ListSeparator(System.String)
extern "C" void TextInfo_set_ListSeparator_m1_4506 (TextInfo_t1_124 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextInfo::get_MacCodePage()
extern "C" int32_t TextInfo_get_MacCodePage_m1_4507 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextInfo::get_OEMCodePage()
extern "C" int32_t TextInfo_get_OEMCodePage_m1_4508 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextInfo::get_CultureName()
extern "C" String_t* TextInfo_get_CultureName_m1_4509 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TextInfo::get_IsReadOnly()
extern "C" bool TextInfo_get_IsReadOnly_m1_4510 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TextInfo::get_IsRightToLeft()
extern "C" bool TextInfo_get_IsRightToLeft_m1_4511 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.TextInfo::Equals(System.Object)
extern "C" bool TextInfo_Equals_m1_4512 (TextInfo_t1_124 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.TextInfo::GetHashCode()
extern "C" int32_t TextInfo_GetHashCode_m1_4513 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextInfo::ToString()
extern "C" String_t* TextInfo_ToString_m1_4514 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextInfo::ToTitleCase(System.String)
extern "C" String_t* TextInfo_ToTitleCase_m1_4515 (TextInfo_t1_124 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Globalization.TextInfo::ToLower(System.Char)
extern "C" uint16_t TextInfo_ToLower_m1_4516 (TextInfo_t1_124 * __this, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Globalization.TextInfo::ToUpper(System.Char)
extern "C" uint16_t TextInfo_ToUpper_m1_4517 (TextInfo_t1_124 * __this, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.Globalization.TextInfo::ToTitleCase(System.Char)
extern "C" uint16_t TextInfo_ToTitleCase_m1_4518 (TextInfo_t1_124 * __this, uint16_t ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextInfo::ToLower(System.String)
extern "C" String_t* TextInfo_ToLower_m1_4519 (TextInfo_t1_124 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.TextInfo::ToUpper(System.String)
extern "C" String_t* TextInfo_ToUpper_m1_4520 (TextInfo_t1_124 * __this, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.TextInfo System.Globalization.TextInfo::ReadOnly(System.Globalization.TextInfo)
extern "C" TextInfo_t1_124 * TextInfo_ReadOnly_m1_4521 (Object_t * __this /* static, unused */, TextInfo_t1_124 * ___textInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.TextInfo::Clone()
extern "C" Object_t * TextInfo_Clone_m1_4522 (TextInfo_t1_124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
