﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.ChineseLunisolarCalendar
struct ChineseLunisolarCalendar_t1_357;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Globalization.ChineseLunisolarCalendar::.ctor()
extern "C" void ChineseLunisolarCalendar__ctor_m1_3831 (ChineseLunisolarCalendar_t1_357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.ChineseLunisolarCalendar::.cctor()
extern "C" void ChineseLunisolarCalendar__cctor_m1_3832 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.ChineseLunisolarCalendar::get_Eras()
extern "C" Int32U5BU5D_t1_275* ChineseLunisolarCalendar_get_Eras_m1_3833 (ChineseLunisolarCalendar_t1_357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.ChineseLunisolarCalendar::GetEra(System.DateTime)
extern "C" int32_t ChineseLunisolarCalendar_GetEra_m1_3834 (ChineseLunisolarCalendar_t1_357 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ChineseLunisolarCalendar::get_MinSupportedDateTime()
extern "C" DateTime_t1_150  ChineseLunisolarCalendar_get_MinSupportedDateTime_m1_3835 (ChineseLunisolarCalendar_t1_357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.ChineseLunisolarCalendar::get_MaxSupportedDateTime()
extern "C" DateTime_t1_150  ChineseLunisolarCalendar_get_MaxSupportedDateTime_m1_3836 (ChineseLunisolarCalendar_t1_357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
