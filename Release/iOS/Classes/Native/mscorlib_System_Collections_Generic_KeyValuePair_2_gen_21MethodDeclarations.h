﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
#include "AssemblyU2DCSharp_ExifLibrary_IFD.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m1_26586_gshared (KeyValuePair_2_t1_2681 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1_26586(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1_2681 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1_26586_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m1_26587_gshared (KeyValuePair_2_t1_2681 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1_26587(__this, method) (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))KeyValuePair_2_get_Key_m1_26587_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m1_26588_gshared (KeyValuePair_2_t1_2681 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1_26588(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2681 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1_26588_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m1_26589_gshared (KeyValuePair_2_t1_2681 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1_26589(__this, method) (( int32_t (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))KeyValuePair_2_get_Value_m1_26589_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m1_26590_gshared (KeyValuePair_2_t1_2681 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1_26590(__this, ___value, method) (( void (*) (KeyValuePair_2_t1_2681 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1_26590_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,ExifLibrary.IFD>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m1_26591_gshared (KeyValuePair_2_t1_2681 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1_26591(__this, method) (( String_t* (*) (KeyValuePair_2_t1_2681 *, const MethodInfo*))KeyValuePair_2_ToString_m1_26591_gshared)(__this, method)
