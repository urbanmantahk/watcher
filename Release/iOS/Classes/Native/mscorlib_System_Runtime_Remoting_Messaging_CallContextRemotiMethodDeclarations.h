﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t1_942;
// System.String
struct String_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern "C" void CallContextRemotingData__ctor_m1_8409 (CallContextRemotingData_t1_942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Messaging.CallContextRemotingData::get_LogicalCallID()
extern "C" String_t* CallContextRemotingData_get_LogicalCallID_m1_8410 (CallContextRemotingData_t1_942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::set_LogicalCallID(System.String)
extern "C" void CallContextRemotingData_set_LogicalCallID_m1_8411 (CallContextRemotingData_t1_942 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.CallContextRemotingData::Clone()
extern "C" Object_t * CallContextRemotingData_Clone_m1_8412 (CallContextRemotingData_t1_942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
