﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1_238;
// VoxelBusters.NativePlugins.eShareOptions[]
struct eShareOptionsU5BU5D_t8_186;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"

// VoxelBusters.NativePlugins.Demo.SharingDemo
struct  SharingDemo_t8_185  : public NPDisabledFeatureDemo_t8_175
{
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_smsBody
	String_t* ___m_smsBody_10;
	// System.String[] VoxelBusters.NativePlugins.Demo.SharingDemo::m_smsRecipients
	StringU5BU5D_t1_238* ___m_smsRecipients_11;
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_mailSubject
	String_t* ___m_mailSubject_12;
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_plainMailBody
	String_t* ___m_plainMailBody_13;
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_htmlMailBody
	String_t* ___m_htmlMailBody_14;
	// System.String[] VoxelBusters.NativePlugins.Demo.SharingDemo::m_mailToRecipients
	StringU5BU5D_t1_238* ___m_mailToRecipients_15;
	// System.String[] VoxelBusters.NativePlugins.Demo.SharingDemo::m_mailCCRecipients
	StringU5BU5D_t1_238* ___m_mailCCRecipients_16;
	// System.String[] VoxelBusters.NativePlugins.Demo.SharingDemo::m_mailBCCRecipients
	StringU5BU5D_t1_238* ___m_mailBCCRecipients_17;
	// VoxelBusters.NativePlugins.eShareOptions[] VoxelBusters.NativePlugins.Demo.SharingDemo::m_excludedOptions
	eShareOptionsU5BU5D_t8_186* ___m_excludedOptions_18;
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_shareMessage
	String_t* ___m_shareMessage_19;
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_shareURL
	String_t* ___m_shareURL_20;
	// System.String VoxelBusters.NativePlugins.Demo.SharingDemo::m_shareImageRelativePath
	String_t* ___m_shareImageRelativePath_21;
};
