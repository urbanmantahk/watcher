﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.SHA384
struct SHA384_t1_1254;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.SHA384::.ctor()
extern "C" void SHA384__ctor_m1_10672 (SHA384_t1_1254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SHA384 System.Security.Cryptography.SHA384::Create()
extern "C" SHA384_t1_1254 * SHA384_Create_m1_10673 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.SHA384 System.Security.Cryptography.SHA384::Create(System.String)
extern "C" SHA384_t1_1254 * SHA384_Create_m1_10674 (Object_t * __this /* static, unused */, String_t* ___hashName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
