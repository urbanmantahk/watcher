﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties
struct  BaseChannelObjectWithProperties_t1_864  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties::table
	Hashtable_t1_100 * ___table_0;
};
