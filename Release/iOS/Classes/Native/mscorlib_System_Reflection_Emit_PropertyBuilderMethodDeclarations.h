﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t1_547;
// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1_1685;
// System.Object
struct Object_t;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Reflection.Module
struct Module_t1_495;
// System.Exception
struct Exception_t1_33;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Reflection_Emit_PropertyToken.h"
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Emit.PropertyBuilder::.ctor(System.Reflection.Emit.TypeBuilder,System.String,System.Reflection.PropertyAttributes,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" void PropertyBuilder__ctor_m1_6298 (PropertyBuilder_t1_547 * __this, TypeBuilder_t1_481 * ___tb, String_t* ___name, int32_t ___attributes, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnModReq, TypeU5BU5D_t1_31* ___returnModOpt, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___paramModReq, TypeU5BU5DU5BU5D_t1_483* ___paramModOpt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::System.Runtime.InteropServices._PropertyBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void PropertyBuilder_System_Runtime_InteropServices__PropertyBuilder_GetIDsOfNames_m1_6299 (PropertyBuilder_t1_547 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::System.Runtime.InteropServices._PropertyBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void PropertyBuilder_System_Runtime_InteropServices__PropertyBuilder_GetTypeInfo_m1_6300 (PropertyBuilder_t1_547 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::System.Runtime.InteropServices._PropertyBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void PropertyBuilder_System_Runtime_InteropServices__PropertyBuilder_GetTypeInfoCount_m1_6301 (PropertyBuilder_t1_547 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::System.Runtime.InteropServices._PropertyBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void PropertyBuilder_System_Runtime_InteropServices__PropertyBuilder_Invoke_m1_6302 (PropertyBuilder_t1_547 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyAttributes System.Reflection.Emit.PropertyBuilder::get_Attributes()
extern "C" int32_t PropertyBuilder_get_Attributes_m1_6303 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyBuilder::get_CanRead()
extern "C" bool PropertyBuilder_get_CanRead_m1_6304 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyBuilder::get_CanWrite()
extern "C" bool PropertyBuilder_get_CanWrite_m1_6305 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PropertyBuilder::get_DeclaringType()
extern "C" Type_t * PropertyBuilder_get_DeclaringType_m1_6306 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.PropertyBuilder::get_Name()
extern "C" String_t* PropertyBuilder_get_Name_m1_6307 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.PropertyToken System.Reflection.Emit.PropertyBuilder::get_PropertyToken()
extern "C" PropertyToken_t1_549  PropertyBuilder_get_PropertyToken_m1_6308 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PropertyBuilder::get_PropertyType()
extern "C" Type_t * PropertyBuilder_get_PropertyType_m1_6309 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.PropertyBuilder::get_ReflectedType()
extern "C" Type_t * PropertyBuilder_get_ReflectedType_m1_6310 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::AddOtherMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void PropertyBuilder_AddOtherMethod_m1_6311 (PropertyBuilder_t1_547 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.PropertyBuilder::GetAccessors(System.Boolean)
extern "C" MethodInfoU5BU5D_t1_603* PropertyBuilder_GetAccessors_m1_6312 (PropertyBuilder_t1_547 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.PropertyBuilder::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* PropertyBuilder_GetCustomAttributes_m1_6313 (PropertyBuilder_t1_547 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.PropertyBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* PropertyBuilder_GetCustomAttributes_m1_6314 (PropertyBuilder_t1_547 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.PropertyBuilder::GetGetMethod(System.Boolean)
extern "C" MethodInfo_t * PropertyBuilder_GetGetMethod_m1_6315 (PropertyBuilder_t1_547 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo[] System.Reflection.Emit.PropertyBuilder::GetIndexParameters()
extern "C" ParameterInfoU5BU5D_t1_1685* PropertyBuilder_GetIndexParameters_m1_6316 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.PropertyBuilder::GetSetMethod(System.Boolean)
extern "C" MethodInfo_t * PropertyBuilder_GetSetMethod_m1_6317 (PropertyBuilder_t1_547 * __this, bool ___nonPublic, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.PropertyBuilder::GetValue(System.Object,System.Object[])
extern "C" Object_t * PropertyBuilder_GetValue_m1_6318 (PropertyBuilder_t1_547 * __this, Object_t * ___obj, ObjectU5BU5D_t1_272* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.PropertyBuilder::GetValue(System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" Object_t * PropertyBuilder_GetValue_m1_6319 (PropertyBuilder_t1_547 * __this, Object_t * ___obj, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___index, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.PropertyBuilder::IsDefined(System.Type,System.Boolean)
extern "C" bool PropertyBuilder_IsDefined_m1_6320 (PropertyBuilder_t1_547 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetConstant(System.Object)
extern "C" void PropertyBuilder_SetConstant_m1_6321 (PropertyBuilder_t1_547 * __this, Object_t * ___defaultValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void PropertyBuilder_SetCustomAttribute_m1_6322 (PropertyBuilder_t1_547 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void PropertyBuilder_SetCustomAttribute_m1_6323 (PropertyBuilder_t1_547 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetGetMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void PropertyBuilder_SetGetMethod_m1_6324 (PropertyBuilder_t1_547 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetSetMethod(System.Reflection.Emit.MethodBuilder)
extern "C" void PropertyBuilder_SetSetMethod_m1_6325 (PropertyBuilder_t1_547 * __this, MethodBuilder_t1_501 * ___mdBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetValue(System.Object,System.Object,System.Object[])
extern "C" void PropertyBuilder_SetValue_m1_6326 (PropertyBuilder_t1_547 * __this, Object_t * ___obj, Object_t * ___value, ObjectU5BU5D_t1_272* ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.PropertyBuilder::SetValue(System.Object,System.Object,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object[],System.Globalization.CultureInfo)
extern "C" void PropertyBuilder_SetValue_m1_6327 (PropertyBuilder_t1_547 * __this, Object_t * ___obj, Object_t * ___value, int32_t ___invokeAttr, Binder_t1_585 * ___binder, ObjectU5BU5D_t1_272* ___index, CultureInfo_t1_277 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.PropertyBuilder::get_Module()
extern "C" Module_t1_495 * PropertyBuilder_get_Module_m1_6328 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.PropertyBuilder::not_supported()
extern "C" Exception_t1_33 * PropertyBuilder_not_supported_m1_6329 (PropertyBuilder_t1_547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
