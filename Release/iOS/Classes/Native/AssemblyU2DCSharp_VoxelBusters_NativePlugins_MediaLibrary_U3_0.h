﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion
struct SaveImageToGalleryCompletion_t8_241;
// VoxelBusters.NativePlugins.MediaLibrary
struct MediaLibrary_t8_245;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC
struct  U3CSaveScreenshotToGalleryU3Ec__AnonStoreyC_t8_246  : public Object_t
{
	// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC::_onCompletion
	SaveImageToGalleryCompletion_t8_241 * ____onCompletion_0;
	// VoxelBusters.NativePlugins.MediaLibrary VoxelBusters.NativePlugins.MediaLibrary/<SaveScreenshotToGallery>c__AnonStoreyC::<>f__this
	MediaLibrary_t8_245 * ___U3CU3Ef__this_1;
};
