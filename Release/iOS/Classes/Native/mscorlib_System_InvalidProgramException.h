﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_SystemException.h"

// System.InvalidProgramException
struct  InvalidProgramException_t1_1560  : public SystemException_t1_250
{
};
