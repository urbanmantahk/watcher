﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// VoxelBusters.Utility.WebRequest/JSONResponse
struct JSONResponse_t8_157;

#include "AssemblyU2DCSharp_VoxelBusters_Utility_Request.h"

// VoxelBusters.Utility.WebRequest
struct  WebRequest_t8_158  : public Request_t8_155
{
	// System.Object VoxelBusters.Utility.WebRequest::<Parameters>k__BackingField
	Object_t * ___U3CParametersU3Ek__BackingField_4;
	// VoxelBusters.Utility.WebRequest/JSONResponse VoxelBusters.Utility.WebRequest::<OnSuccess>k__BackingField
	JSONResponse_t8_157 * ___U3COnSuccessU3Ek__BackingField_5;
	// VoxelBusters.Utility.WebRequest/JSONResponse VoxelBusters.Utility.WebRequest::<OnFailure>k__BackingField
	JSONResponse_t8_157 * ___U3COnFailureU3Ek__BackingField_6;
};
