﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t1_100;

#include "mscorlib_System_Object.h"

// System.Net.DigestClient
struct  DigestClient_t3_90  : public Object_t
{
};
struct DigestClient_t3_90_StaticFields{
	// System.Collections.Hashtable System.Net.DigestClient::cache
	Hashtable_t1_100 * ___cache_0;
};
