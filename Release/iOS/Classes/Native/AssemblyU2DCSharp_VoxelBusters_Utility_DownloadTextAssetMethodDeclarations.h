﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.Utility.DownloadTextAsset
struct DownloadTextAsset_t8_160;
// VoxelBusters.Utility.DownloadTextAsset/Completion
struct Completion_t8_159;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// System.Void VoxelBusters.Utility.DownloadTextAsset::.ctor(VoxelBusters.Utility.URL,System.Boolean,System.Boolean)
extern "C" void DownloadTextAsset__ctor_m8_920 (DownloadTextAsset_t8_160 * __this, URL_t8_156  ____URL, bool ____isAsynchronous, bool ____autoFixOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.Utility.DownloadTextAsset/Completion VoxelBusters.Utility.DownloadTextAsset::get_OnCompletion()
extern "C" Completion_t8_159 * DownloadTextAsset_get_OnCompletion_m8_921 (DownloadTextAsset_t8_160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextAsset::set_OnCompletion(VoxelBusters.Utility.DownloadTextAsset/Completion)
extern "C" void DownloadTextAsset_set_OnCompletion_m8_922 (DownloadTextAsset_t8_160 * __this, Completion_t8_159 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextAsset::DidFailStartRequestWithError(System.String)
extern "C" void DownloadTextAsset_DidFailStartRequestWithError_m8_923 (DownloadTextAsset_t8_160 * __this, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.Utility.DownloadTextAsset::OnFetchingResponse()
extern "C" void DownloadTextAsset_OnFetchingResponse_m8_924 (DownloadTextAsset_t8_160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
