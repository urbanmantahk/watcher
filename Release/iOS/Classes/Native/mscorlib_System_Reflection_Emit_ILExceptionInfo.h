﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Emit.ILExceptionBlock[]
struct ILExceptionBlockU5BU5D_t1_512;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_Reflection_Emit_Label.h"

// System.Reflection.Emit.ILExceptionInfo
struct  ILExceptionInfo_t1_511 
{
	// System.Reflection.Emit.ILExceptionBlock[] System.Reflection.Emit.ILExceptionInfo::handlers
	ILExceptionBlockU5BU5D_t1_512* ___handlers_0;
	// System.Int32 System.Reflection.Emit.ILExceptionInfo::start
	int32_t ___start_1;
	// System.Int32 System.Reflection.Emit.ILExceptionInfo::len
	int32_t ___len_2;
	// System.Reflection.Emit.Label System.Reflection.Emit.ILExceptionInfo::end
	Label_t1_513  ___end_3;
};
