﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t6_65;
// UnityEngine.GameObject
struct GameObject_t6_97;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// VoxelBusters.DesignPatterns.SingletonPattern`1<System.Object>
struct  SingletonPattern_1_t8_372  : public MonoBehaviour_t6_91
{
	// UnityEngine.Transform VoxelBusters.DesignPatterns.SingletonPattern`1::m_transform
	Transform_t6_65 * ___m_transform_5;
	// UnityEngine.GameObject VoxelBusters.DesignPatterns.SingletonPattern`1::m_gameObject
	GameObject_t6_97 * ___m_gameObject_6;
	// System.Boolean VoxelBusters.DesignPatterns.SingletonPattern`1::m_isInitialized
	bool ___m_isInitialized_7;
	// System.Boolean VoxelBusters.DesignPatterns.SingletonPattern`1::m_isForcefullyDestroyed
	bool ___m_isForcefullyDestroyed_8;
};
struct SingletonPattern_1_t8_372_StaticFields{
	// T VoxelBusters.DesignPatterns.SingletonPattern`1::instance
	Object_t * ___instance_2;
	// System.Object VoxelBusters.DesignPatterns.SingletonPattern`1::instanceLock
	Object_t * ___instanceLock_3;
	// System.Boolean VoxelBusters.DesignPatterns.SingletonPattern`1::destroyedOnApplicationQuit
	bool ___destroyedOnApplicationQuit_4;
};
