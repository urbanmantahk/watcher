﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Decimal.h"

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger
struct  SoapNonNegativeInteger_t1_987  : public Object_t
{
	// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::_value
	Decimal_t1_19  ____value_0;
};
