﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_PropertyAttribute.h"

// VoxelBusters.Utility.RegexAttribute
struct  RegexAttribute_t8_149  : public PropertyAttribute_t6_236
{
	// System.String VoxelBusters.Utility.RegexAttribute::pattern
	String_t* ___pattern_0;
	// System.String VoxelBusters.Utility.RegexAttribute::helpMessage
	String_t* ___helpMessage_1;
};
