﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifByte
struct ExifByte_t8_114;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifByte::.ctor(ExifLibrary.ExifTag,System.Byte)
extern "C" void ExifByte__ctor_m8_509 (ExifByte_t8_114 * __this, int32_t ___tag, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifByte::get__Value()
extern "C" Object_t * ExifByte_get__Value_m8_510 (ExifByte_t8_114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifByte::set__Value(System.Object)
extern "C" void ExifByte_set__Value_m8_511 (ExifByte_t8_114 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ExifLibrary.ExifByte::get_Value()
extern "C" uint8_t ExifByte_get_Value_m8_512 (ExifByte_t8_114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifByte::set_Value(System.Byte)
extern "C" void ExifByte_set_Value_m8_513 (ExifByte_t8_114 * __this, uint8_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifByte::ToString()
extern "C" String_t* ExifByte_ToString_m8_514 (ExifByte_t8_114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifByte::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifByte_get_Interoperability_m8_515 (ExifByte_t8_114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ExifLibrary.ExifByte::op_Implicit(ExifLibrary.ExifByte)
extern "C" uint8_t ExifByte_op_Implicit_m8_516 (Object_t * __this /* static, unused */, ExifByte_t8_114 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
