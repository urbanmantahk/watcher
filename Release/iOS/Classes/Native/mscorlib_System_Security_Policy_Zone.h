﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_Security_SecurityZone.h"

// System.Security.Policy.Zone
struct  Zone_t1_1370  : public Object_t
{
	// System.Security.SecurityZone System.Security.Policy.Zone::zone
	int32_t ___zone_0;
};
