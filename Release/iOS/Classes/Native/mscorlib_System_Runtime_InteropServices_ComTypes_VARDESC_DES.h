﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.InteropServices.ComTypes.VARDESC/DESCUNION
#pragma pack(push, tp, 1)
struct  DESCUNION_t1_747 
{
	union
	{
		// System.IntPtr System.Runtime.InteropServices.ComTypes.VARDESC/DESCUNION::lpvarValue
		struct
		{
			IntPtr_t ___lpvarValue_0;
		};
		// System.Int32 System.Runtime.InteropServices.ComTypes.VARDESC/DESCUNION::oInst
		struct
		{
			int32_t ___oInst_1;
		};
	};
};
#pragma pack(pop, tp)
