﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8
struct U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168;
// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8::.ctor()
extern "C" void U3CGetIndexOfConsoleTagU3Ec__AnonStorey8__ctor_m8_974 (U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.DebugPRO.Console/<GetIndexOfConsoleTag>c__AnonStorey8::<>m__6(VoxelBusters.DebugPRO.Internal.ConsoleTag)
extern "C" bool U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_U3CU3Em__6_m8_975 (U3CGetIndexOfConsoleTagU3Ec__AnonStorey8_t8_168 * __this, ConsoleTag_t8_171 * ____consoleTag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
