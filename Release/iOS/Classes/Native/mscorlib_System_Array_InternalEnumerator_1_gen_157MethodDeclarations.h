﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_157.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_20211_gshared (InternalEnumerator_1_t1_2344 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_20211(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2344 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_20211_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20212_gshared (InternalEnumerator_1_t1_2344 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20212(__this, method) (( void (*) (InternalEnumerator_1_t1_2344 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_20212_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20213_gshared (InternalEnumerator_1_t1_2344 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20213(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2344 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_20213_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_20214_gshared (InternalEnumerator_1_t1_2344 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_20214(__this, method) (( void (*) (InternalEnumerator_1_t1_2344 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_20214_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_20215_gshared (InternalEnumerator_1_t1_2344 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_20215(__this, method) (( bool (*) (InternalEnumerator_1_t1_2344 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_20215_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C" RaycastHit2D_t6_118  InternalEnumerator_1_get_Current_m1_20216_gshared (InternalEnumerator_1_t1_2344 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_20216(__this, method) (( RaycastHit2D_t6_118  (*) (InternalEnumerator_1_t1_2344 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_20216_gshared)(__this, method)
