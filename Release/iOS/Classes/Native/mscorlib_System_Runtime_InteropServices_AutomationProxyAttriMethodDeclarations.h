﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.AutomationProxyAttribute
struct AutomationProxyAttribute_t1_754;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.AutomationProxyAttribute::.ctor(System.Boolean)
extern "C" void AutomationProxyAttribute__ctor_m1_7584 (AutomationProxyAttribute_t1_754 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.InteropServices.AutomationProxyAttribute::get_Value()
extern "C" bool AutomationProxyAttribute_get_Value_m1_7585 (AutomationProxyAttribute_t1_754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
