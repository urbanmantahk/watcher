﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t1_870;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern "C" void ChannelInfo__ctor_m1_8033 (ChannelInfo_t1_870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ChannelInfo::.ctor(System.Object)
extern "C" void ChannelInfo__ctor_m1_8034 (ChannelInfo_t1_870 * __this, Object_t * ___remoteChannelData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern "C" ObjectU5BU5D_t1_272* ChannelInfo_get_ChannelData_m1_8035 (ChannelInfo_t1_870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ChannelInfo::set_ChannelData(System.Object[])
extern "C" void ChannelInfo_set_ChannelData_m1_8036 (ChannelInfo_t1_870 * __this, ObjectU5BU5D_t1_272* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
