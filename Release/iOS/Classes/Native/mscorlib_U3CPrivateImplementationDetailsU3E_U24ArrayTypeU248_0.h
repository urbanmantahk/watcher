﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// <PrivateImplementationDetails>/$ArrayType$84
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU2484_t1_1646 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2484_t1_1646__padding[84];
	};
};
#pragma pack(pop, tp)
