﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.MonoBehaviourExtensions
struct  MonoBehaviourExtensions_t8_38  : public Object_t
{
};
struct MonoBehaviourExtensions_t8_38_StaticFields{
	// System.Boolean VoxelBusters.Utility.MonoBehaviourExtensions::isPaused
	bool ___isPaused_0;
	// System.Single VoxelBusters.Utility.MonoBehaviourExtensions::timeScale
	float ___timeScale_1;
};
