﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>
struct ListValues_t3_259;
// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t3_255;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1_2210;
// System.Array
struct Array_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void ListValues__ctor_m3_1947_gshared (ListValues_t3_259 * __this, SortedList_2_t3_255 * ___host, const MethodInfo* method);
#define ListValues__ctor_m3_1947(__this, ___host, method) (( void (*) (ListValues_t3_259 *, SortedList_2_t3_255 *, const MethodInfo*))ListValues__ctor_m3_1947_gshared)(__this, ___host, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ListValues_System_Collections_IEnumerable_GetEnumerator_m3_1948_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_System_Collections_IEnumerable_GetEnumerator_m3_1948(__this, method) (( Object_t * (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_System_Collections_IEnumerable_GetEnumerator_m3_1948_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Add(TValue)
extern "C" void ListValues_Add_m3_1949_gshared (ListValues_t3_259 * __this, Object_t * ___item, const MethodInfo* method);
#define ListValues_Add_m3_1949(__this, ___item, method) (( void (*) (ListValues_t3_259 *, Object_t *, const MethodInfo*))ListValues_Add_m3_1949_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Remove(TValue)
extern "C" bool ListValues_Remove_m3_1950_gshared (ListValues_t3_259 * __this, Object_t * ___value, const MethodInfo* method);
#define ListValues_Remove_m3_1950(__this, ___value, method) (( bool (*) (ListValues_t3_259 *, Object_t *, const MethodInfo*))ListValues_Remove_m3_1950_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Clear()
extern "C" void ListValues_Clear_m3_1951_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_Clear_m3_1951(__this, method) (( void (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_Clear_m3_1951_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::CopyTo(TValue[],System.Int32)
extern "C" void ListValues_CopyTo_m3_1952_gshared (ListValues_t3_259 * __this, ObjectU5BU5D_t1_272* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ListValues_CopyTo_m3_1952(__this, ___array, ___arrayIndex, method) (( void (*) (ListValues_t3_259 *, ObjectU5BU5D_t1_272*, int32_t, const MethodInfo*))ListValues_CopyTo_m3_1952_gshared)(__this, ___array, ___arrayIndex, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Contains(TValue)
extern "C" bool ListValues_Contains_m3_1953_gshared (ListValues_t3_259 * __this, Object_t * ___item, const MethodInfo* method);
#define ListValues_Contains_m3_1953(__this, ___item, method) (( bool (*) (ListValues_t3_259 *, Object_t *, const MethodInfo*))ListValues_Contains_m3_1953_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::IndexOf(TValue)
extern "C" int32_t ListValues_IndexOf_m3_1954_gshared (ListValues_t3_259 * __this, Object_t * ___item, const MethodInfo* method);
#define ListValues_IndexOf_m3_1954(__this, ___item, method) (( int32_t (*) (ListValues_t3_259 *, Object_t *, const MethodInfo*))ListValues_IndexOf_m3_1954_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::Insert(System.Int32,TValue)
extern "C" void ListValues_Insert_m3_1955_gshared (ListValues_t3_259 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ListValues_Insert_m3_1955(__this, ___index, ___item, method) (( void (*) (ListValues_t3_259 *, int32_t, Object_t *, const MethodInfo*))ListValues_Insert_m3_1955_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::RemoveAt(System.Int32)
extern "C" void ListValues_RemoveAt_m3_1956_gshared (ListValues_t3_259 * __this, int32_t ___index, const MethodInfo* method);
#define ListValues_RemoveAt_m3_1956(__this, ___index, method) (( void (*) (ListValues_t3_259 *, int32_t, const MethodInfo*))ListValues_RemoveAt_m3_1956_gshared)(__this, ___index, method)
// TValue System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_Item(System.Int32)
extern "C" Object_t * ListValues_get_Item_m3_1957_gshared (ListValues_t3_259 * __this, int32_t ___index, const MethodInfo* method);
#define ListValues_get_Item_m3_1957(__this, ___index, method) (( Object_t * (*) (ListValues_t3_259 *, int32_t, const MethodInfo*))ListValues_get_Item_m3_1957_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::set_Item(System.Int32,TValue)
extern "C" void ListValues_set_Item_m3_1958_gshared (ListValues_t3_259 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ListValues_set_Item_m3_1958(__this, ___index, ___value, method) (( void (*) (ListValues_t3_259 *, int32_t, Object_t *, const MethodInfo*))ListValues_set_Item_m3_1958_gshared)(__this, ___index, ___value, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ListValues_GetEnumerator_m3_1959_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_GetEnumerator_m3_1959(__this, method) (( Object_t* (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_GetEnumerator_m3_1959_gshared)(__this, method)
// System.Int32 System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_Count()
extern "C" int32_t ListValues_get_Count_m3_1960_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_get_Count_m3_1960(__this, method) (( int32_t (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_get_Count_m3_1960_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_IsSynchronized()
extern "C" bool ListValues_get_IsSynchronized_m3_1961_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_get_IsSynchronized_m3_1961(__this, method) (( bool (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_get_IsSynchronized_m3_1961_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ListValues_get_IsReadOnly_m3_1962_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_get_IsReadOnly_m3_1962(__this, method) (( bool (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_get_IsReadOnly_m3_1962_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::get_SyncRoot()
extern "C" Object_t * ListValues_get_SyncRoot_m3_1963_gshared (ListValues_t3_259 * __this, const MethodInfo* method);
#define ListValues_get_SyncRoot_m3_1963(__this, method) (( Object_t * (*) (ListValues_t3_259 *, const MethodInfo*))ListValues_get_SyncRoot_m3_1963_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/ListValues<System.Object,System.Object>::CopyTo(System.Array,System.Int32)
extern "C" void ListValues_CopyTo_m3_1964_gshared (ListValues_t3_259 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ListValues_CopyTo_m3_1964(__this, ___array, ___arrayIndex, method) (( void (*) (ListValues_t3_259 *, Array_t *, int32_t, const MethodInfo*))ListValues_CopyTo_m3_1964_gshared)(__this, ___array, ___arrayIndex, method)
