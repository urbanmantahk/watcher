﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ApplicationDirectoryMembershipCondition
struct ApplicationDirectoryMembershipCondition_t1_1327;
// System.Security.Policy.Evidence
struct Evidence_t1_398;
// System.Security.Policy.IMembershipCondition
struct IMembershipCondition_t1_1340;
// System.Object
struct Object_t;
// System.Security.SecurityElement
struct SecurityElement_t1_242;
// System.Security.Policy.PolicyLevel
struct PolicyLevel_t1_1357;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Policy.ApplicationDirectoryMembershipCondition::.ctor()
extern "C" void ApplicationDirectoryMembershipCondition__ctor_m1_11334 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationDirectoryMembershipCondition::Check(System.Security.Policy.Evidence)
extern "C" bool ApplicationDirectoryMembershipCondition_Check_m1_11335 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, Evidence_t1_398 * ___evidence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.IMembershipCondition System.Security.Policy.ApplicationDirectoryMembershipCondition::Copy()
extern "C" Object_t * ApplicationDirectoryMembershipCondition_Copy_m1_11336 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationDirectoryMembershipCondition::Equals(System.Object)
extern "C" bool ApplicationDirectoryMembershipCondition_Equals_m1_11337 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, Object_t * ___o, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationDirectoryMembershipCondition::FromXml(System.Security.SecurityElement)
extern "C" void ApplicationDirectoryMembershipCondition_FromXml_m1_11338 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, SecurityElement_t1_242 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationDirectoryMembershipCondition::FromXml(System.Security.SecurityElement,System.Security.Policy.PolicyLevel)
extern "C" void ApplicationDirectoryMembershipCondition_FromXml_m1_11339 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, SecurityElement_t1_242 * ___e, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationDirectoryMembershipCondition::GetHashCode()
extern "C" int32_t ApplicationDirectoryMembershipCondition_GetHashCode_m1_11340 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.ApplicationDirectoryMembershipCondition::ToString()
extern "C" String_t* ApplicationDirectoryMembershipCondition_ToString_m1_11341 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.ApplicationDirectoryMembershipCondition::ToXml()
extern "C" SecurityElement_t1_242 * ApplicationDirectoryMembershipCondition_ToXml_m1_11342 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityElement System.Security.Policy.ApplicationDirectoryMembershipCondition::ToXml(System.Security.Policy.PolicyLevel)
extern "C" SecurityElement_t1_242 * ApplicationDirectoryMembershipCondition_ToXml_m1_11343 (ApplicationDirectoryMembershipCondition_t1_1327 * __this, PolicyLevel_t1_1357 * ___level, const MethodInfo* method) IL2CPP_METHOD_ATTR;
