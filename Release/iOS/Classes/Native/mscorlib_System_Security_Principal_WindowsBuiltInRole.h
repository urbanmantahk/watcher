﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_Principal_WindowsBuiltInRole.h"

// System.Security.Principal.WindowsBuiltInRole
struct  WindowsBuiltInRole_t1_1383 
{
	// System.Int32 System.Security.Principal.WindowsBuiltInRole::value__
	int32_t ___value___1;
};
