﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Reflection_Emit_DerivedType.h"

// System.Reflection.Emit.PointerType
struct  PointerType_t1_492  : public DerivedType_t1_489
{
};
