﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.TypeBuilder
struct TypeBuilder_t1_481;
// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t1_475;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1_31;
// System.Reflection.EventInfo
struct EventInfo_t;
// System.Reflection.Emit.EventBuilder
struct EventBuilder_t1_500;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.Module
struct Module_t1_495;
// System.Security.PermissionSet
struct PermissionSet_t1_563;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t1_478;
// System.Reflection.Binder
struct Binder_t1_585;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1_1669;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.Emit.ConstructorBuilder
struct ConstructorBuilder_t1_477;
// System.Type[][]
struct TypeU5BU5DU5BU5D_t1_483;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1_501;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1_499;
// System.Reflection.Emit.PropertyBuilder
struct PropertyBuilder_t1_547;
// System.Diagnostics.SymbolStore.ISymbolWriter
struct ISymbolWriter_t1_536;
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1_1671;
// System.Reflection.EventInfo[]
struct EventInfoU5BU5D_t1_1667;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1_1668;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1_1054;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t1_603;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1_1670;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Object
struct Object_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1_277;
// System.String[]
struct StringU5BU5D_t1_238;
// System.Reflection.Emit.CustomAttributeBuilder
struct CustomAttributeBuilder_t1_487;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Exception
struct Exception_t1_33;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1_529;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_Emit_PackingSize.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_Security_Permissions_SecurityAction.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_Emit_TypeToken.h"
#include "mscorlib_System_Reflection_InterfaceMapping.h"
#include "mscorlib_System_Reflection_GenericParameterAttributes.h"

// System.Void System.Reflection.Emit.TypeBuilder::.ctor(System.Reflection.Emit.ModuleBuilder,System.Reflection.TypeAttributes,System.Int32)
extern "C" void TypeBuilder__ctor_m1_6405 (TypeBuilder_t1_481 * __this, ModuleBuilder_t1_475 * ___mb, int32_t ___attr, int32_t ___table_idx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::.ctor(System.Reflection.Emit.ModuleBuilder,System.String,System.Reflection.TypeAttributes,System.Type,System.Type[],System.Reflection.Emit.PackingSize,System.Int32,System.Type)
extern "C" void TypeBuilder__ctor_m1_6406 (TypeBuilder_t1_481 * __this, ModuleBuilder_t1_475 * ___mb, String_t* ___name, int32_t ___attr, Type_t * ___parent, TypeU5BU5D_t1_31* ___interfaces, int32_t ___packing_size, int32_t ___type_size, Type_t * ___nesting_type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::System.Runtime.InteropServices._TypeBuilder.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void TypeBuilder_System_Runtime_InteropServices__TypeBuilder_GetIDsOfNames_m1_6407 (TypeBuilder_t1_481 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::System.Runtime.InteropServices._TypeBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void TypeBuilder_System_Runtime_InteropServices__TypeBuilder_GetTypeInfo_m1_6408 (TypeBuilder_t1_481 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::System.Runtime.InteropServices._TypeBuilder.GetTypeInfoCount(System.UInt32&)
extern "C" void TypeBuilder_System_Runtime_InteropServices__TypeBuilder_GetTypeInfoCount_m1_6409 (TypeBuilder_t1_481 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::System.Runtime.InteropServices._TypeBuilder.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void TypeBuilder_System_Runtime_InteropServices__TypeBuilder_Invoke_m1_6410 (TypeBuilder_t1_481 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.TypeAttributes System.Reflection.Emit.TypeBuilder::GetAttributeFlagsImpl()
extern "C" int32_t TypeBuilder_GetAttributeFlagsImpl_m1_6411 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::setup_internal_class(System.Reflection.Emit.TypeBuilder)
extern "C" void TypeBuilder_setup_internal_class_m1_6412 (TypeBuilder_t1_481 * __this, TypeBuilder_t1_481 * ___tb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::create_internal_class(System.Reflection.Emit.TypeBuilder)
extern "C" void TypeBuilder_create_internal_class_m1_6413 (TypeBuilder_t1_481 * __this, TypeBuilder_t1_481 * ___tb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::setup_generic_class()
extern "C" void TypeBuilder_setup_generic_class_m1_6414 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::create_generic_class()
extern "C" void TypeBuilder_create_generic_class_m1_6415 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.Emit.TypeBuilder::get_event_info(System.Reflection.Emit.EventBuilder)
extern "C" EventInfo_t * TypeBuilder_get_event_info_m1_6416 (TypeBuilder_t1_481 * __this, EventBuilder_t1_500 * ___eb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.Reflection.Emit.TypeBuilder::get_Assembly()
extern "C" Assembly_t1_467 * TypeBuilder_get_Assembly_m1_6417 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.TypeBuilder::get_AssemblyQualifiedName()
extern "C" String_t* TypeBuilder_get_AssemblyQualifiedName_m1_6418 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::get_BaseType()
extern "C" Type_t * TypeBuilder_get_BaseType_m1_6419 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::get_DeclaringType()
extern "C" Type_t * TypeBuilder_get_DeclaringType_m1_6420 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::get_UnderlyingSystemType()
extern "C" Type_t * TypeBuilder_get_UnderlyingSystemType_m1_6421 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.TypeBuilder::GetFullName()
extern "C" String_t* TypeBuilder_GetFullName_m1_6422 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.TypeBuilder::get_FullName()
extern "C" String_t* TypeBuilder_get_FullName_m1_6423 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid System.Reflection.Emit.TypeBuilder::get_GUID()
extern "C" Guid_t1_319  TypeBuilder_get_GUID_m1_6424 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module System.Reflection.Emit.TypeBuilder::get_Module()
extern "C" Module_t1_495 * TypeBuilder_get_Module_m1_6425 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.TypeBuilder::get_Name()
extern "C" String_t* TypeBuilder_get_Name_m1_6426 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.TypeBuilder::get_Namespace()
extern "C" String_t* TypeBuilder_get_Namespace_m1_6427 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.PackingSize System.Reflection.Emit.TypeBuilder::get_PackingSize()
extern "C" int32_t TypeBuilder_get_PackingSize_m1_6428 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.TypeBuilder::get_Size()
extern "C" int32_t TypeBuilder_get_Size_m1_6429 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::get_ReflectedType()
extern "C" Type_t * TypeBuilder_get_ReflectedType_m1_6430 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::AddDeclarativeSecurity(System.Security.Permissions.SecurityAction,System.Security.PermissionSet)
extern "C" void TypeBuilder_AddDeclarativeSecurity_m1_6431 (TypeBuilder_t1_481 * __this, int32_t ___action, PermissionSet_t1_563 * ___pset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::AddInterfaceImplementation(System.Type)
extern "C" void TypeBuilder_AddInterfaceImplementation_m1_6432 (TypeBuilder_t1_481 * __this, Type_t * ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.TypeBuilder::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" ConstructorInfo_t1_478 * TypeBuilder_GetConstructorImpl_m1_6433 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsDefined(System.Type,System.Boolean)
extern "C" bool TypeBuilder_IsDefined_m1_6434 (TypeBuilder_t1_481 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.TypeBuilder::GetCustomAttributes(System.Boolean)
extern "C" ObjectU5BU5D_t1_272* TypeBuilder_GetCustomAttributes_m1_6435 (TypeBuilder_t1_481 * __this, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Reflection.Emit.TypeBuilder::GetCustomAttributes(System.Type,System.Boolean)
extern "C" ObjectU5BU5D_t1_272* TypeBuilder_GetCustomAttributes_m1_6436 (TypeBuilder_t1_481 * __this, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String)
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6437 (TypeBuilder_t1_481 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String,System.Reflection.TypeAttributes)
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6438 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String,System.Reflection.TypeAttributes,System.Type)
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6439 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String,System.Reflection.TypeAttributes,System.Type,System.Type[],System.Reflection.Emit.PackingSize,System.Int32)
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6440 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, TypeU5BU5D_t1_31* ___interfaces, int32_t ___packSize, int32_t ___typeSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String,System.Reflection.TypeAttributes,System.Type,System.Type[])
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6441 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, TypeU5BU5D_t1_31* ___interfaces, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String,System.Reflection.TypeAttributes,System.Type,System.Int32)
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6442 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, int32_t ___typeSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeBuilder System.Reflection.Emit.TypeBuilder::DefineNestedType(System.String,System.Reflection.TypeAttributes,System.Type,System.Reflection.Emit.PackingSize)
extern "C" TypeBuilder_t1_481 * TypeBuilder_DefineNestedType_m1_6443 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attr, Type_t * ___parent, int32_t ___packSize, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineConstructor(System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type[])
extern "C" ConstructorBuilder_t1_477 * TypeBuilder_DefineConstructor_m1_6444 (TypeBuilder_t1_481 * __this, int32_t ___attributes, int32_t ___callingConvention, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineConstructor(System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type[],System.Type[][],System.Type[][])
extern "C" ConstructorBuilder_t1_477 * TypeBuilder_DefineConstructor_m1_6445 (TypeBuilder_t1_481 * __this, int32_t ___attributes, int32_t ___callingConvention, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___requiredCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___optionalCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineDefaultConstructor(System.Reflection.MethodAttributes)
extern "C" ConstructorBuilder_t1_477 * TypeBuilder_DefineDefaultConstructor_m1_6446 (TypeBuilder_t1_481 * __this, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::append_method(System.Reflection.Emit.MethodBuilder)
extern "C" void TypeBuilder_append_method_m1_6447 (TypeBuilder_t1_481 * __this, MethodBuilder_t1_501 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Type,System.Type[])
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefineMethod_m1_6448 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[])
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefineMethod_m1_6449 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefineMethod_m1_6450 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnTypeRequiredCustomModifiers, TypeU5BU5D_t1_31* ___returnTypeOptionalCustomModifiers, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeRequiredCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeOptionalCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefinePInvokeMethod(System.String,System.String,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Runtime.InteropServices.CallingConvention,System.Runtime.InteropServices.CharSet)
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefinePInvokeMethod_m1_6451 (TypeBuilder_t1_481 * __this, String_t* ___name, String_t* ___dllName, String_t* ___entryName, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, int32_t ___nativeCallConv, int32_t ___nativeCharSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefinePInvokeMethod(System.String,System.String,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][],System.Runtime.InteropServices.CallingConvention,System.Runtime.InteropServices.CharSet)
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefinePInvokeMethod_m1_6452 (TypeBuilder_t1_481 * __this, String_t* ___name, String_t* ___dllName, String_t* ___entryName, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnTypeRequiredCustomModifiers, TypeU5BU5D_t1_31* ___returnTypeOptionalCustomModifiers, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeRequiredCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeOptionalCustomModifiers, int32_t ___nativeCallConv, int32_t ___nativeCharSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefinePInvokeMethod(System.String,System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions,System.Type,System.Type[],System.Runtime.InteropServices.CallingConvention,System.Runtime.InteropServices.CharSet)
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefinePInvokeMethod_m1_6453 (TypeBuilder_t1_481 * __this, String_t* ___name, String_t* ___dllName, int32_t ___attributes, int32_t ___callingConvention, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, int32_t ___nativeCallConv, int32_t ___nativeCharSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes)
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefineMethod_m1_6454 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.MethodBuilder System.Reflection.Emit.TypeBuilder::DefineMethod(System.String,System.Reflection.MethodAttributes,System.Reflection.CallingConventions)
extern "C" MethodBuilder_t1_501 * TypeBuilder_DefineMethod_m1_6455 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, int32_t ___callingConvention, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::DefineMethodOverride(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
extern "C" void TypeBuilder_DefineMethodOverride_m1_6456 (TypeBuilder_t1_481 * __this, MethodInfo_t * ___methodInfoBody, MethodInfo_t * ___methodInfoDeclaration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.TypeBuilder::DefineField(System.String,System.Type,System.Reflection.FieldAttributes)
extern "C" FieldBuilder_t1_499 * TypeBuilder_DefineField_m1_6457 (TypeBuilder_t1_481 * __this, String_t* ___fieldName, Type_t * ___type, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.TypeBuilder::DefineField(System.String,System.Type,System.Type[],System.Type[],System.Reflection.FieldAttributes)
extern "C" FieldBuilder_t1_499 * TypeBuilder_DefineField_m1_6458 (TypeBuilder_t1_481 * __this, String_t* ___fieldName, Type_t * ___type, TypeU5BU5D_t1_31* ___requiredCustomModifiers, TypeU5BU5D_t1_31* ___optionalCustomModifiers, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.PropertyBuilder System.Reflection.Emit.TypeBuilder::DefineProperty(System.String,System.Reflection.PropertyAttributes,System.Type,System.Type[])
extern "C" PropertyBuilder_t1_547 * TypeBuilder_DefineProperty_m1_6459 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, Type_t * ___returnType, TypeU5BU5D_t1_31* ___parameterTypes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.PropertyBuilder System.Reflection.Emit.TypeBuilder::DefineProperty(System.String,System.Reflection.PropertyAttributes,System.Type,System.Type[],System.Type[],System.Type[],System.Type[][],System.Type[][])
extern "C" PropertyBuilder_t1_547 * TypeBuilder_DefineProperty_m1_6460 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, Type_t * ___returnType, TypeU5BU5D_t1_31* ___returnTypeRequiredCustomModifiers, TypeU5BU5D_t1_31* ___returnTypeOptionalCustomModifiers, TypeU5BU5D_t1_31* ___parameterTypes, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeRequiredCustomModifiers, TypeU5BU5DU5BU5D_t1_483* ___parameterTypeOptionalCustomModifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ConstructorBuilder System.Reflection.Emit.TypeBuilder::DefineTypeInitializer()
extern "C" ConstructorBuilder_t1_477 * TypeBuilder_DefineTypeInitializer_m1_6461 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::create_runtime_class(System.Reflection.Emit.TypeBuilder)
extern "C" Type_t * TypeBuilder_create_runtime_class_m1_6462 (TypeBuilder_t1_481 * __this, TypeBuilder_t1_481 * ___tb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::is_nested_in(System.Type)
extern "C" bool TypeBuilder_is_nested_in_m1_6463 (TypeBuilder_t1_481 * __this, Type_t * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::has_ctor_method()
extern "C" bool TypeBuilder_has_ctor_method_m1_6464 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::CreateType()
extern "C" Type_t * TypeBuilder_CreateType_m1_6465 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::GenerateDebugInfo(System.Diagnostics.SymbolStore.ISymbolWriter)
extern "C" void TypeBuilder_GenerateDebugInfo_m1_6466 (TypeBuilder_t1_481 * __this, Object_t * ___symbolWriter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.TypeBuilder::GetConstructors(System.Reflection.BindingFlags)
extern "C" ConstructorInfoU5BU5D_t1_1671* TypeBuilder_GetConstructors_m1_6467 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo[] System.Reflection.Emit.TypeBuilder::GetConstructorsInternal(System.Reflection.BindingFlags)
extern "C" ConstructorInfoU5BU5D_t1_1671* TypeBuilder_GetConstructorsInternal_m1_6468 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::GetElementType()
extern "C" Type_t * TypeBuilder_GetElementType_m1_6469 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo System.Reflection.Emit.TypeBuilder::GetEvent(System.String,System.Reflection.BindingFlags)
extern "C" EventInfo_t * TypeBuilder_GetEvent_m1_6470 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.Emit.TypeBuilder::GetEvents()
extern "C" EventInfoU5BU5D_t1_1667* TypeBuilder_GetEvents_m1_6471 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.Emit.TypeBuilder::GetEvents(System.Reflection.BindingFlags)
extern "C" EventInfoU5BU5D_t1_1667* TypeBuilder_GetEvents_m1_6472 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.EventInfo[] System.Reflection.Emit.TypeBuilder::GetEvents_internal(System.Reflection.BindingFlags)
extern "C" EventInfoU5BU5D_t1_1667* TypeBuilder_GetEvents_internal_m1_6473 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Emit.TypeBuilder::GetField(System.String,System.Reflection.BindingFlags)
extern "C" FieldInfo_t * TypeBuilder_GetField_m1_6474 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] System.Reflection.Emit.TypeBuilder::GetFields(System.Reflection.BindingFlags)
extern "C" FieldInfoU5BU5D_t1_1668* TypeBuilder_GetFields_m1_6475 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::GetInterface(System.String,System.Boolean)
extern "C" Type_t * TypeBuilder_GetInterface_m1_6476 (TypeBuilder_t1_481 * __this, String_t* ___name, bool ___ignoreCase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.TypeBuilder::GetInterfaces()
extern "C" TypeU5BU5D_t1_31* TypeBuilder_GetInterfaces_m1_6477 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.Emit.TypeBuilder::GetMember(System.String,System.Reflection.MemberTypes,System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* TypeBuilder_GetMember_m1_6478 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___type, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] System.Reflection.Emit.TypeBuilder::GetMembers(System.Reflection.BindingFlags)
extern "C" MemberInfoU5BU5D_t1_1054* TypeBuilder_GetMembers_m1_6479 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.TypeBuilder::GetMethodsByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern "C" MethodInfoU5BU5D_t1_603* TypeBuilder_GetMethodsByName_m1_6480 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___bindingAttr, bool ___ignoreCase, Type_t * ___reflected_type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo[] System.Reflection.Emit.TypeBuilder::GetMethods(System.Reflection.BindingFlags)
extern "C" MethodInfoU5BU5D_t1_603* TypeBuilder_GetMethods_m1_6481 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.TypeBuilder::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern "C" MethodInfo_t * TypeBuilder_GetMethodImpl_m1_6482 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, int32_t ___callConvention, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::GetNestedType(System.String,System.Reflection.BindingFlags)
extern "C" Type_t * TypeBuilder_GetNestedType_m1_6483 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.TypeBuilder::GetNestedTypes(System.Reflection.BindingFlags)
extern "C" TypeU5BU5D_t1_31* TypeBuilder_GetNestedTypes_m1_6484 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] System.Reflection.Emit.TypeBuilder::GetProperties(System.Reflection.BindingFlags)
extern "C" PropertyInfoU5BU5D_t1_1670* TypeBuilder_GetProperties_m1_6485 (TypeBuilder_t1_481 * __this, int32_t ___bindingAttr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System.Reflection.Emit.TypeBuilder::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern "C" PropertyInfo_t * TypeBuilder_GetPropertyImpl_m1_6486 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___bindingAttr, Binder_t1_585 * ___binder, Type_t * ___returnType, TypeU5BU5D_t1_31* ___types, ParameterModifierU5BU5D_t1_1669* ___modifiers, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::HasElementTypeImpl()
extern "C" bool TypeBuilder_HasElementTypeImpl_m1_6487 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.Emit.TypeBuilder::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern "C" Object_t * TypeBuilder_InvokeMember_m1_6488 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___invokeAttr, Binder_t1_585 * ___binder, Object_t * ___target, ObjectU5BU5D_t1_272* ___args, ParameterModifierU5BU5D_t1_1669* ___modifiers, CultureInfo_t1_277 * ___culture, StringU5BU5D_t1_238* ___namedParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsArrayImpl()
extern "C" bool TypeBuilder_IsArrayImpl_m1_6489 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsByRefImpl()
extern "C" bool TypeBuilder_IsByRefImpl_m1_6490 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsCOMObjectImpl()
extern "C" bool TypeBuilder_IsCOMObjectImpl_m1_6491 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsPointerImpl()
extern "C" bool TypeBuilder_IsPointerImpl_m1_6492 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsPrimitiveImpl()
extern "C" bool TypeBuilder_IsPrimitiveImpl_m1_6493 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsValueTypeImpl()
extern "C" bool TypeBuilder_IsValueTypeImpl_m1_6494 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::MakeArrayType()
extern "C" Type_t * TypeBuilder_MakeArrayType_m1_6495 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::MakeArrayType(System.Int32)
extern "C" Type_t * TypeBuilder_MakeArrayType_m1_6496 (TypeBuilder_t1_481 * __this, int32_t ___rank, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::MakeByRefType()
extern "C" Type_t * TypeBuilder_MakeByRefType_m1_6497 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::MakeGenericType(System.Type[])
extern "C" Type_t * TypeBuilder_MakeGenericType_m1_6498 (TypeBuilder_t1_481 * __this, TypeU5BU5D_t1_31* ___typeArguments, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::MakePointerType()
extern "C" Type_t * TypeBuilder_MakePointerType_m1_6499 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.RuntimeTypeHandle System.Reflection.Emit.TypeBuilder::get_TypeHandle()
extern "C" RuntimeTypeHandle_t1_30  TypeBuilder_get_TypeHandle_m1_6500 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::SetCharSet(System.Reflection.TypeAttributes)
extern "C" void TypeBuilder_SetCharSet_m1_6501 (TypeBuilder_t1_481 * __this, int32_t ___ta, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::SetCustomAttribute(System.Reflection.Emit.CustomAttributeBuilder)
extern "C" void TypeBuilder_SetCustomAttribute_m1_6502 (TypeBuilder_t1_481 * __this, CustomAttributeBuilder_t1_487 * ___customBuilder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::SetCustomAttribute(System.Reflection.ConstructorInfo,System.Byte[])
extern "C" void TypeBuilder_SetCustomAttribute_m1_6503 (TypeBuilder_t1_481 * __this, ConstructorInfo_t1_478 * ___con, ByteU5BU5D_t1_109* ___binaryAttribute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.EventBuilder System.Reflection.Emit.TypeBuilder::DefineEvent(System.String,System.Reflection.EventAttributes,System.Type)
extern "C" EventBuilder_t1_500 * TypeBuilder_DefineEvent_m1_6504 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___attributes, Type_t * ___eventtype, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.TypeBuilder::DefineInitializedData(System.String,System.Byte[],System.Reflection.FieldAttributes)
extern "C" FieldBuilder_t1_499 * TypeBuilder_DefineInitializedData_m1_6505 (TypeBuilder_t1_481 * __this, String_t* ___name, ByteU5BU5D_t1_109* ___data, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.FieldBuilder System.Reflection.Emit.TypeBuilder::DefineUninitializedData(System.String,System.Int32,System.Reflection.FieldAttributes)
extern "C" FieldBuilder_t1_499 * TypeBuilder_DefineUninitializedData_m1_6506 (TypeBuilder_t1_481 * __this, String_t* ___name, int32_t ___size, int32_t ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.TypeToken System.Reflection.Emit.TypeBuilder::get_TypeToken()
extern "C" TypeToken_t1_558  TypeBuilder_get_TypeToken_m1_6507 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::SetParent(System.Type)
extern "C" void TypeBuilder_SetParent_m1_6508 (TypeBuilder_t1_481 * __this, Type_t * ___parent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.TypeBuilder::get_next_table_index(System.Object,System.Int32,System.Boolean)
extern "C" int32_t TypeBuilder_get_next_table_index_m1_6509 (TypeBuilder_t1_481 * __this, Object_t * ___obj, int32_t ___table, bool ___inc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.InterfaceMapping System.Reflection.Emit.TypeBuilder::GetInterfaceMap(System.Type)
extern "C" InterfaceMapping_t1_602  TypeBuilder_GetInterfaceMap_m1_6510 (TypeBuilder_t1_481 * __this, Type_t * ___interfaceType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsCompilerContext()
extern "C" bool TypeBuilder_get_IsCompilerContext_m1_6511 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::get_is_created()
extern "C" bool TypeBuilder_get_is_created_m1_6512 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.TypeBuilder::not_supported()
extern "C" Exception_t1_33 * TypeBuilder_not_supported_m1_6513 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::check_not_created()
extern "C" void TypeBuilder_check_not_created_m1_6514 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::check_created()
extern "C" void TypeBuilder_check_created_m1_6515 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Emit.TypeBuilder::check_name(System.String,System.String)
extern "C" void TypeBuilder_check_name_m1_6516 (TypeBuilder_t1_481 * __this, String_t* ___argName, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Reflection.Emit.TypeBuilder::ToString()
extern "C" String_t* TypeBuilder_ToString_m1_6517 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsAssignableFrom(System.Type)
extern "C" bool TypeBuilder_IsAssignableFrom_m1_6518 (TypeBuilder_t1_481 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsSubclassOf(System.Type)
extern "C" bool TypeBuilder_IsSubclassOf_m1_6519 (TypeBuilder_t1_481 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsAssignableTo(System.Type)
extern "C" bool TypeBuilder_IsAssignableTo_m1_6520 (TypeBuilder_t1_481 * __this, Type_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsCreated()
extern "C" bool TypeBuilder_IsCreated_m1_6521 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.TypeBuilder::GetGenericArguments()
extern "C" TypeU5BU5D_t1_31* TypeBuilder_GetGenericArguments_m1_6522 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.Emit.TypeBuilder::GetGenericTypeDefinition()
extern "C" Type_t * TypeBuilder_GetGenericTypeDefinition_m1_6523 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::get_ContainsGenericParameters()
extern "C" bool TypeBuilder_get_ContainsGenericParameters_m1_6524 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsGenericParameter()
extern "C" bool TypeBuilder_get_IsGenericParameter_m1_6525 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.GenericParameterAttributes System.Reflection.Emit.TypeBuilder::get_GenericParameterAttributes()
extern "C" int32_t TypeBuilder_get_GenericParameterAttributes_m1_6526 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsGenericTypeDefinition()
extern "C" bool TypeBuilder_get_IsGenericTypeDefinition_m1_6527 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::get_IsGenericType()
extern "C" bool TypeBuilder_get_IsGenericType_m1_6528 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Emit.TypeBuilder::get_GenericParameterPosition()
extern "C" int32_t TypeBuilder_get_GenericParameterPosition_m1_6529 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Emit.TypeBuilder::get_DeclaringMethod()
extern "C" MethodBase_t1_335 * TypeBuilder_get_DeclaringMethod_m1_6530 (TypeBuilder_t1_481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.GenericTypeParameterBuilder[] System.Reflection.Emit.TypeBuilder::DefineGenericParameters(System.String[])
extern "C" GenericTypeParameterBuilderU5BU5D_t1_529* TypeBuilder_DefineGenericParameters_m1_6531 (TypeBuilder_t1_481 * __this, StringU5BU5D_t1_238* ___names, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Reflection.Emit.TypeBuilder::GetConstructor(System.Type,System.Reflection.ConstructorInfo)
extern "C" ConstructorInfo_t1_478 * TypeBuilder_GetConstructor_m1_6532 (Object_t * __this /* static, unused */, Type_t * ___type, ConstructorInfo_t1_478 * ___constructor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.TypeBuilder::IsValidGetMethodType(System.Type)
extern "C" bool TypeBuilder_IsValidGetMethodType_m1_6533 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Reflection.Emit.TypeBuilder::GetMethod(System.Type,System.Reflection.MethodInfo)
extern "C" MethodInfo_t * TypeBuilder_GetMethod_m1_6534 (Object_t * __this /* static, unused */, Type_t * ___type, MethodInfo_t * ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo System.Reflection.Emit.TypeBuilder::GetField(System.Type,System.Reflection.FieldInfo)
extern "C" FieldInfo_t * TypeBuilder_GetField_m1_6535 (Object_t * __this /* static, unused */, Type_t * ___type, FieldInfo_t * ___field, const MethodInfo* method) IL2CPP_METHOD_ATTR;
