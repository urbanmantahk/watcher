﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifURational
struct ExifURational_t8_120;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t1_142;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_MathEx_UFraction32.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifURational::.ctor(ExifLibrary.ExifTag,System.UInt32,System.UInt32)
extern "C" void ExifURational__ctor_m8_565 (ExifURational_t8_120 * __this, int32_t ___tag, uint32_t ___numerator, uint32_t ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifURational::.ctor(ExifLibrary.ExifTag,ExifLibrary.MathEx/UFraction32)
extern "C" void ExifURational__ctor_m8_566 (ExifURational_t8_120 * __this, int32_t ___tag, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ExifLibrary.ExifURational::get__Value()
extern "C" Object_t * ExifURational_get__Value_m8_567 (ExifURational_t8_120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifURational::set__Value(System.Object)
extern "C" void ExifURational_set__Value_m8_568 (ExifURational_t8_120 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.MathEx/UFraction32 ExifLibrary.ExifURational::get_Value()
extern "C" UFraction32_t8_121  ExifURational_get_Value_m8_569 (ExifURational_t8_120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExifLibrary.ExifURational::set_Value(ExifLibrary.MathEx/UFraction32)
extern "C" void ExifURational_set_Value_m8_570 (ExifURational_t8_120 * __this, UFraction32_t8_121  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ExifLibrary.ExifURational::ToString()
extern "C" String_t* ExifURational_ToString_m8_571 (ExifURational_t8_120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.ExifURational::ToFloat()
extern "C" float ExifURational_ToFloat_m8_572 (ExifURational_t8_120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] ExifLibrary.ExifURational::ToArray()
extern "C" UInt32U5BU5D_t1_142* ExifURational_ToArray_m8_573 (ExifURational_t8_120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.ExifInterOperability ExifLibrary.ExifURational::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifURational_get_Interoperability_m8_574 (ExifURational_t8_120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.ExifURational::op_Explicit(ExifLibrary.ExifURational)
extern "C" float ExifURational_op_Explicit_m8_575 (Object_t * __this /* static, unused */, ExifURational_t8_120 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
