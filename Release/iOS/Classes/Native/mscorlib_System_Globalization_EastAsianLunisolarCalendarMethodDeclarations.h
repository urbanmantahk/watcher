﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.EastAsianLunisolarCalendar
struct EastAsianLunisolarCalendar_t1_358;
// System.Globalization.CCEastAsianLunisolarEraHandler
struct CCEastAsianLunisolarEraHandler_t1_355;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_Globalization_CalendarAlgorithmType.h"

// System.Void System.Globalization.EastAsianLunisolarCalendar::.ctor(System.Globalization.CCEastAsianLunisolarEraHandler)
extern "C" void EastAsianLunisolarCalendar__ctor_m1_4057 (EastAsianLunisolarCalendar_t1_358 * __this, CCEastAsianLunisolarEraHandler_t1_355 * ___eraHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::get_TwoDigitYearMax()
extern "C" int32_t EastAsianLunisolarCalendar_get_TwoDigitYearMax_m1_4058 (EastAsianLunisolarCalendar_t1_358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.EastAsianLunisolarCalendar::set_TwoDigitYearMax(System.Int32)
extern "C" void EastAsianLunisolarCalendar_set_TwoDigitYearMax_m1_4059 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.EastAsianLunisolarCalendar::M_CheckDateTime(System.DateTime)
extern "C" void EastAsianLunisolarCalendar_M_CheckDateTime_m1_4060 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::get_ActualCurrentEra()
extern "C" int32_t EastAsianLunisolarCalendar_get_ActualCurrentEra_m1_4061 (EastAsianLunisolarCalendar_t1_358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.EastAsianLunisolarCalendar::M_CheckEra(System.Int32&)
extern "C" void EastAsianLunisolarCalendar_M_CheckEra_m1_4062 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::M_CheckYEG(System.Int32,System.Int32&)
extern "C" int32_t EastAsianLunisolarCalendar_M_CheckYEG_m1_4063 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.EastAsianLunisolarCalendar::M_CheckYE(System.Int32,System.Int32&)
extern "C" void EastAsianLunisolarCalendar_M_CheckYE_m1_4064 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::M_CheckYMEG(System.Int32,System.Int32,System.Int32&)
extern "C" int32_t EastAsianLunisolarCalendar_M_CheckYMEG_m1_4065 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___month, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::M_CheckYMDEG(System.Int32,System.Int32,System.Int32,System.Int32&)
extern "C" int32_t EastAsianLunisolarCalendar_M_CheckYMDEG_m1_4066 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t* ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.EastAsianLunisolarCalendar::AddMonths(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  EastAsianLunisolarCalendar_AddMonths_m1_4067 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, int32_t ___months, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.EastAsianLunisolarCalendar::AddYears(System.DateTime,System.Int32)
extern "C" DateTime_t1_150  EastAsianLunisolarCalendar_AddYears_m1_4068 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, int32_t ___years, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetDayOfMonth(System.DateTime)
extern "C" int32_t EastAsianLunisolarCalendar_GetDayOfMonth_m1_4069 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.EastAsianLunisolarCalendar::GetDayOfWeek(System.DateTime)
extern "C" int32_t EastAsianLunisolarCalendar_GetDayOfWeek_m1_4070 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetDayOfYear(System.DateTime)
extern "C" int32_t EastAsianLunisolarCalendar_GetDayOfYear_m1_4071 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetDaysInMonth(System.Int32,System.Int32,System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_GetDaysInMonth_m1_4072 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetDaysInYear(System.Int32,System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_GetDaysInYear_m1_4073 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetLeapMonth(System.Int32,System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_GetLeapMonth_m1_4074 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetMonth(System.DateTime)
extern "C" int32_t EastAsianLunisolarCalendar_GetMonth_m1_4075 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetMonthsInYear(System.Int32,System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_GetMonthsInYear_m1_4076 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetYear(System.DateTime)
extern "C" int32_t EastAsianLunisolarCalendar_GetYear_m1_4077 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.EastAsianLunisolarCalendar::IsLeapDay(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool EastAsianLunisolarCalendar_IsLeapDay_m1_4078 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.EastAsianLunisolarCalendar::IsLeapMonth(System.Int32,System.Int32,System.Int32)
extern "C" bool EastAsianLunisolarCalendar_IsLeapMonth_m1_4079 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___month, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.EastAsianLunisolarCalendar::IsLeapYear(System.Int32,System.Int32)
extern "C" bool EastAsianLunisolarCalendar_IsLeapYear_m1_4080 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.Globalization.EastAsianLunisolarCalendar::ToDateTime(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" DateTime_t1_150  EastAsianLunisolarCalendar_ToDateTime_m1_4081 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, int32_t ___month, int32_t ___day, int32_t ___hour, int32_t ___minute, int32_t ___second, int32_t ___millisecond, int32_t ___era, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::ToFourDigitYear(System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_ToFourDigitYear_m1_4082 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___year, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CalendarAlgorithmType System.Globalization.EastAsianLunisolarCalendar::get_AlgorithmType()
extern "C" int32_t EastAsianLunisolarCalendar_get_AlgorithmType_m1_4083 (EastAsianLunisolarCalendar_t1_358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetCelestialStem(System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_GetCelestialStem_m1_4084 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___sexagenaryYear, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetSexagenaryYear(System.DateTime)
extern "C" int32_t EastAsianLunisolarCalendar_GetSexagenaryYear_m1_4085 (EastAsianLunisolarCalendar_t1_358 * __this, DateTime_t1_150  ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.EastAsianLunisolarCalendar::GetTerrestrialBranch(System.Int32)
extern "C" int32_t EastAsianLunisolarCalendar_GetTerrestrialBranch_m1_4086 (EastAsianLunisolarCalendar_t1_358 * __this, int32_t ___sexagenaryYear, const MethodInfo* method) IL2CPP_METHOD_ATTR;
