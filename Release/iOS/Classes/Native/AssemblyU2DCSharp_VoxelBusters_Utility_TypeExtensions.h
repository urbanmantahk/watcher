﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type[]
struct TypeU5BU5D_t1_31;

#include "mscorlib_System_Object.h"

// VoxelBusters.Utility.TypeExtensions
struct  TypeExtensions_t8_46  : public Object_t
{
};
struct TypeExtensions_t8_46_StaticFields{
	// System.Type[] VoxelBusters.Utility.TypeExtensions::typeCodesToType
	TypeU5BU5D_t1_31* ___typeCodesToType_0;
};
