﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.Utility.DownloadTexture/Completion
struct Completion_t8_161;
// VoxelBusters.NativePlugins.AddressBookContact
struct AddressBookContact_t8_198;

#include "mscorlib_System_Object.h"

// VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA
struct  U3CGetImageAsyncU3Ec__AnonStoreyA_t8_197  : public Object_t
{
	// VoxelBusters.Utility.DownloadTexture/Completion VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA::_onCompletion
	Completion_t8_161 * ____onCompletion_0;
	// VoxelBusters.NativePlugins.AddressBookContact VoxelBusters.NativePlugins.AddressBookContact/<GetImageAsync>c__AnonStoreyA::<>f__this
	AddressBookContact_t8_198 * ___U3CU3Ef__this_1;
};
