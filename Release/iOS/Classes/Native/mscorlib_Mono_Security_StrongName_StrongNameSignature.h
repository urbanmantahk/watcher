﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "mscorlib_System_Object.h"

// Mono.Security.StrongName/StrongNameSignature
struct  StrongNameSignature_t1_230  : public Object_t
{
	// System.Byte[] Mono.Security.StrongName/StrongNameSignature::hash
	ByteU5BU5D_t1_109* ___hash_0;
	// System.Byte[] Mono.Security.StrongName/StrongNameSignature::signature
	ByteU5BU5D_t1_109* ___signature_1;
	// System.UInt32 Mono.Security.StrongName/StrongNameSignature::signaturePosition
	uint32_t ___signaturePosition_2;
	// System.UInt32 Mono.Security.StrongName/StrongNameSignature::signatureLength
	uint32_t ___signatureLength_3;
	// System.UInt32 Mono.Security.StrongName/StrongNameSignature::metadataPosition
	uint32_t ___metadataPosition_4;
	// System.UInt32 Mono.Security.StrongName/StrongNameSignature::metadataLength
	uint32_t ___metadataLength_5;
	// System.Byte Mono.Security.StrongName/StrongNameSignature::cliFlag
	uint8_t ___cliFlag_6;
	// System.UInt32 Mono.Security.StrongName/StrongNameSignature::cliFlagPosition
	uint32_t ___cliFlagPosition_7;
};
