﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>
struct ReadOnlyCollection_1_t1_2291;
// System.Collections.Generic.IList`1<UnityEngine.Vector2>
struct IList_1_t1_2292;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t6_279;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t1_2803;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m1_19224_gshared (ReadOnlyCollection_1_t1_2291 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1_19224(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_19224_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19225_gshared (ReadOnlyCollection_1_t1_2291 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19225(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, Vector2_t6_47 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_19225_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19226_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19226(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_19226_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19227_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, Vector2_t6_47  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19227(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, Vector2_t6_47 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_19227_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19228_gshared (ReadOnlyCollection_1_t1_2291 * __this, Vector2_t6_47  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19228(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, Vector2_t6_47 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_19228_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19229_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19229(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_19229_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Vector2_t6_47  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19230_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19230(__this, ___index, method) (( Vector2_t6_47  (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_19230_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19231_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, Vector2_t6_47  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19231(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, Vector2_t6_47 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_19231_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19232_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19232(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_19232_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19233_gshared (ReadOnlyCollection_1_t1_2291 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19233(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_19233_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19234_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19234(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_19234_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1_19235_gshared (ReadOnlyCollection_1_t1_2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_19235(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_19235_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19236_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19236(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_19236_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19237_gshared (ReadOnlyCollection_1_t1_2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19237(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_19237_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19238_gshared (ReadOnlyCollection_1_t1_2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19238(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_19238_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19239_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19239(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_19239_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19240_gshared (ReadOnlyCollection_1_t1_2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19240(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_19240_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19241_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19241(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_19241_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19242_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19242(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_19242_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19243_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19243(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_19243_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19244_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19244(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_19244_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19245_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19245(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_19245_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19246_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19246(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_19246_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19247_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19247(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_19247_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m1_19248_gshared (ReadOnlyCollection_1_t1_2291 * __this, Vector2_t6_47  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1_19248(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2291 *, Vector2_t6_47 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1_19248_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m1_19249_gshared (ReadOnlyCollection_1_t1_2291 * __this, Vector2U5BU5D_t6_279* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1_19249(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2291 *, Vector2U5BU5D_t6_279*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_19249_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m1_19250_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1_19250(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_19250_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m1_19251_gshared (ReadOnlyCollection_1_t1_2291 * __this, Vector2_t6_47  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1_19251(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2291 *, Vector2_t6_47 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_19251_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m1_19252_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1_19252(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_19252_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::get_Items()
extern "C" Object_t* ReadOnlyCollection_1_get_Items_m1_19253_gshared (ReadOnlyCollection_1_t1_2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Items_m1_19253(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2291 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_19253_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C" Vector2_t6_47  ReadOnlyCollection_1_get_Item_m1_19254_gshared (ReadOnlyCollection_1_t1_2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1_19254(__this, ___index, method) (( Vector2_t6_47  (*) (ReadOnlyCollection_1_t1_2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_19254_gshared)(__this, ___index, method)
