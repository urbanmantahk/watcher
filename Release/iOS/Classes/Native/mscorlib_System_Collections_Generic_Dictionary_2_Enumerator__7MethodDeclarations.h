﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1_1830;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_20956_gshared (Enumerator_t1_2389 * __this, Dictionary_2_t1_1830 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m1_20956(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2389 *, Dictionary_2_t1_1830 *, const MethodInfo*))Enumerator__ctor_m1_20956_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_20957_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_20957(__this, method) (( Object_t * (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_20957_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_20958_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_20958(__this, method) (( void (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_20958_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1_284  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_20959_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_20959(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_20959_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_20960_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_20960(__this, method) (( Object_t * (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_20960_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_20961_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_20961(__this, method) (( Object_t * (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_20961_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_20962_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_20962(__this, method) (( bool (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_MoveNext_m1_20962_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1_2385  Enumerator_get_Current_m1_20963_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_20963(__this, method) (( KeyValuePair_2_t1_2385  (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_get_Current_m1_20963_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m1_20964_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1_20964(__this, method) (( int32_t (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_20964_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m1_20965_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1_20965(__this, method) (( Object_t * (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_20965_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C" void Enumerator_Reset_m1_20966_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1_20966(__this, method) (( void (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_Reset_m1_20966_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_20967_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_20967(__this, method) (( void (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_VerifyState_m1_20967_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m1_20968_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1_20968(__this, method) (( void (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_20968_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_20969_gshared (Enumerator_t1_2389 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_20969(__this, method) (( void (*) (Enumerator_t1_2389 *, const MethodInfo*))Enumerator_Dispose_m1_20969_gshared)(__this, method)
