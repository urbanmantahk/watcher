﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimerSetting/<UpdateTimerToServer>c__Iterator2
struct U3CUpdateTimerToServerU3Ec__Iterator2_t8_12;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void TimerSetting/<UpdateTimerToServer>c__Iterator2::.ctor()
extern "C" void U3CUpdateTimerToServerU3Ec__Iterator2__ctor_m8_97 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TimerSetting/<UpdateTimerToServer>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CUpdateTimerToServerU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m8_98 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TimerSetting/<UpdateTimerToServer>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CUpdateTimerToServerU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m8_99 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TimerSetting/<UpdateTimerToServer>c__Iterator2::MoveNext()
extern "C" bool U3CUpdateTimerToServerU3Ec__Iterator2_MoveNext_m8_100 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting/<UpdateTimerToServer>c__Iterator2::Dispose()
extern "C" void U3CUpdateTimerToServerU3Ec__Iterator2_Dispose_m8_101 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerSetting/<UpdateTimerToServer>c__Iterator2::Reset()
extern "C" void U3CUpdateTimerToServerU3Ec__Iterator2_Reset_m8_102 (U3CUpdateTimerToServerU3Ec__Iterator2_t8_12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
