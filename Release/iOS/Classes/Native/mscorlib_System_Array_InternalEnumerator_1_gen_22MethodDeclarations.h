﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_22.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_15548_gshared (InternalEnumerator_1_t1_1973 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_15548(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_1973 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_15548_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15549_gshared (InternalEnumerator_1_t1_1973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15549(__this, method) (( void (*) (InternalEnumerator_1_t1_1973 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_15549_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15550_gshared (InternalEnumerator_1_t1_1973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15550(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_1973 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_15550_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_15551_gshared (InternalEnumerator_1_t1_1973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_15551(__this, method) (( void (*) (InternalEnumerator_1_t1_1973 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_15551_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_15552_gshared (InternalEnumerator_1_t1_1973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_15552(__this, method) (( bool (*) (InternalEnumerator_1_t1_1973 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_15552_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m1_15553_gshared (InternalEnumerator_1_t1_1973 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_15553(__this, method) (( IntPtr_t (*) (InternalEnumerator_1_t1_1973 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_15553_gshared)(__this, method)
