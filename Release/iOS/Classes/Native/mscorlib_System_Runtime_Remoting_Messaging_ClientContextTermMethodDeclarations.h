﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.ClientContextTerminatorSink
struct ClientContextTerminatorSink_t1_928;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1_891;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageCtrl
struct IMessageCtrl_t1_920;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::.ctor(System.Runtime.Remoting.Contexts.Context)
extern "C" void ClientContextTerminatorSink__ctor_m1_8337 (ClientContextTerminatorSink_t1_928 * __this, Context_t1_891 * ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessage System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::SyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage)
extern "C" Object_t * ClientContextTerminatorSink_SyncProcessMessage_m1_8338 (ClientContextTerminatorSink_t1_928 * __this, Object_t * ___msg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageCtrl System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::AsyncProcessMessage(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" Object_t * ClientContextTerminatorSink_AsyncProcessMessage_m1_8339 (ClientContextTerminatorSink_t1_928 * __this, Object_t * ___msg, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Messaging.IMessageSink System.Runtime.Remoting.Messaging.ClientContextTerminatorSink::get_NextSink()
extern "C" Object_t * ClientContextTerminatorSink_get_NextSink_m1_8340 (ClientContextTerminatorSink_t1_928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
