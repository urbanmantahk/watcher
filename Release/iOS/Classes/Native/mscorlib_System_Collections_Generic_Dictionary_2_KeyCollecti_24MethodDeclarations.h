﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ExifLibrary.ExifTag,System.Object>
struct Dictionary_2_t1_2663;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_26458_gshared (Enumerator_t1_2669 * __this, Dictionary_2_t1_2663 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_26458(__this, ___host, method) (( void (*) (Enumerator_t1_2669 *, Dictionary_2_t1_2663 *, const MethodInfo*))Enumerator__ctor_m1_26458_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_26459_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_26459(__this, method) (( Object_t * (*) (Enumerator_t1_2669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_26459_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_26460_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_26460(__this, method) (( void (*) (Enumerator_t1_2669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_26460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m1_26461_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_26461(__this, method) (( void (*) (Enumerator_t1_2669 *, const MethodInfo*))Enumerator_Dispose_m1_26461_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_26462_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_26462(__this, method) (( bool (*) (Enumerator_t1_2669 *, const MethodInfo*))Enumerator_MoveNext_m1_26462_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<ExifLibrary.ExifTag,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m1_26463_gshared (Enumerator_t1_2669 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_26463(__this, method) (( int32_t (*) (Enumerator_t1_2669 *, const MethodInfo*))Enumerator_get_Current_m1_26463_gshared)(__this, method)
