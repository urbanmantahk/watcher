﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Attribute.h"

// System.Reflection.AssemblyAlgorithmIdAttribute
struct  AssemblyAlgorithmIdAttribute_t1_564  : public Attribute_t1_2
{
	// System.UInt32 System.Reflection.AssemblyAlgorithmIdAttribute::id
	uint32_t ___id_0;
};
