﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.DebugPRO.Internal.ConsoleTag
struct ConsoleTag_t8_171;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<VoxelBusters.DebugPRO.Internal.ConsoleTag>
struct  Predicate_1_t1_1930  : public MulticastDelegate_t1_21
{
};
