﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.CookieCollection/CookieCollectionComparer
struct CookieCollectionComparer_t3_82;
// System.Net.Cookie
struct Cookie_t3_81;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Net.CookieCollection/CookieCollectionComparer::.ctor()
extern "C" void CookieCollectionComparer__ctor_m3_463 (CookieCollectionComparer_t3_82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CookieCollection/CookieCollectionComparer::Compare(System.Net.Cookie,System.Net.Cookie)
extern "C" int32_t CookieCollectionComparer_Compare_m3_464 (CookieCollectionComparer_t3_82 * __this, Cookie_t3_81 * ___x, Cookie_t3_81 * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
