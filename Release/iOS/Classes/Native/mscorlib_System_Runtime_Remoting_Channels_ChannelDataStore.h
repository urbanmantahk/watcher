﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1_238;
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1_869;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.ChannelDataStore
struct  ChannelDataStore_t1_868  : public Object_t
{
	// System.String[] System.Runtime.Remoting.Channels.ChannelDataStore::_channelURIs
	StringU5BU5D_t1_238* ____channelURIs_0;
	// System.Collections.DictionaryEntry[] System.Runtime.Remoting.Channels.ChannelDataStore::_extraData
	DictionaryEntryU5BU5D_t1_869* ____extraData_1;
};
