﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.NPObjectManager
struct NPObjectManager_t8_326;
// VoxelBusters.NativePlugins.Internal.NPObject
struct NPObject_t8_222;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_NPObje_0.h"

// System.Void VoxelBusters.NativePlugins.Internal.NPObjectManager::.ctor()
extern "C" void NPObjectManager__ctor_m8_1905 (NPObjectManager_t8_326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.NPObjectManager::.cctor()
extern "C" void NPObjectManager__cctor_m8_1906 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Internal.NPObjectManager::AddNewObjectToCollection(VoxelBusters.NativePlugins.Internal.NPObject,VoxelBusters.NativePlugins.Internal.NPObjectManager/eCollectionType)
extern "C" void NPObjectManager_AddNewObjectToCollection_m8_1907 (Object_t * __this /* static, unused */, NPObject_t8_222 * ____newObject, int32_t ____collectionType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
