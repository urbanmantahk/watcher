﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t1_1859;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m1_22354_gshared (Enumerator_t1_2468 * __this, List_1_t1_1859 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m1_22354(__this, ___l, method) (( void (*) (Enumerator_t1_2468 *, List_1_t1_1859 *, const MethodInfo*))Enumerator__ctor_m1_22354_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_22355_gshared (Enumerator_t1_2468 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_22355(__this, method) (( void (*) (Enumerator_t1_2468 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_22355_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_22356_gshared (Enumerator_t1_2468 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_22356(__this, method) (( Object_t * (*) (Enumerator_t1_2468 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_22356_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m1_22357_gshared (Enumerator_t1_2468 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_22357(__this, method) (( void (*) (Enumerator_t1_2468 *, const MethodInfo*))Enumerator_Dispose_m1_22357_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m1_22358_gshared (Enumerator_t1_2468 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1_22358(__this, method) (( void (*) (Enumerator_t1_2468 *, const MethodInfo*))Enumerator_VerifyState_m1_22358_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_22359_gshared (Enumerator_t1_2468 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_22359(__this, method) (( bool (*) (Enumerator_t1_2468 *, const MethodInfo*))Enumerator_MoveNext_m1_22359_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t7_31  Enumerator_get_Current_m1_22360_gshared (Enumerator_t1_2468 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_22360(__this, method) (( RaycastResult_t7_31  (*) (Enumerator_t1_2468 *, const MethodInfo*))Enumerator_get_Current_m1_22360_gshared)(__this, method)
