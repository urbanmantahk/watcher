﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.CCMath
struct CCMath_t1_341;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.CCMath::.ctor()
extern "C" void CCMath__ctor_m1_3698 (CCMath_t1_341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Globalization.CCMath::round(System.Double)
extern "C" double CCMath_round_m1_3699 (Object_t * __this /* static, unused */, double ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Globalization.CCMath::mod(System.Double,System.Double)
extern "C" double CCMath_mod_m1_3700 (Object_t * __this /* static, unused */, double ___x, double ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::div(System.Int32,System.Int32)
extern "C" int32_t CCMath_div_m1_3701 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::mod(System.Int32,System.Int32)
extern "C" int32_t CCMath_mod_m1_3702 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::div_mod(System.Int32&,System.Int32,System.Int32)
extern "C" int32_t CCMath_div_mod_m1_3703 (Object_t * __this /* static, unused */, int32_t* ___remainder, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::signum(System.Double)
extern "C" int32_t CCMath_signum_m1_3704 (Object_t * __this /* static, unused */, double ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::signum(System.Int32)
extern "C" int32_t CCMath_signum_m1_3705 (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Globalization.CCMath::amod(System.Double,System.Double)
extern "C" double CCMath_amod_m1_3706 (Object_t * __this /* static, unused */, double ___x, double ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::amod(System.Int32,System.Int32)
extern "C" int32_t CCMath_amod_m1_3707 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
