﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.CustomAce
struct CustomAce_t1_1144;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AceType.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"

// System.Void System.Security.AccessControl.CustomAce::.ctor(System.Security.AccessControl.AceType,System.Security.AccessControl.AceFlags,System.Byte[])
extern "C" void CustomAce__ctor_m1_9808 (CustomAce_t1_1144 * __this, int32_t ___type, uint8_t ___flags, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CustomAce::get_BinaryLength()
extern "C" int32_t CustomAce_get_BinaryLength_m1_9809 (CustomAce_t1_1144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.AccessControl.CustomAce::get_OpaqueLength()
extern "C" int32_t CustomAce_get_OpaqueLength_m1_9810 (CustomAce_t1_1144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CustomAce::GetBinaryForm(System.Byte[],System.Int32)
extern "C" void CustomAce_GetBinaryForm_m1_9811 (CustomAce_t1_1144 * __this, ByteU5BU5D_t1_109* ___binaryForm, int32_t ___offset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.AccessControl.CustomAce::GetOpaque()
extern "C" ByteU5BU5D_t1_109* CustomAce_GetOpaque_m1_9812 (CustomAce_t1_1144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.CustomAce::SetOpaque(System.Byte[])
extern "C" void CustomAce_SetOpaque_m1_9813 (CustomAce_t1_1144 * __this, ByteU5BU5D_t1_109* ___opaque, const MethodInfo* method) IL2CPP_METHOD_ATTR;
