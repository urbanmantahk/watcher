﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t1_843;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"

// System.Void System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::.ctor(System.Runtime.InteropServices.CallingConvention)
extern "C" void UnmanagedFunctionPointerAttribute__ctor_m1_7938 (UnmanagedFunctionPointerAttribute_t1_843 * __this, int32_t ___callingConvention, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.InteropServices.CallingConvention System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::get_CallingConvention()
extern "C" int32_t UnmanagedFunctionPointerAttribute_get_CallingConvention_m1_7939 (UnmanagedFunctionPointerAttribute_t1_843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
