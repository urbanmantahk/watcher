﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t1_406;
// System.IO.Stream
struct Stream_t1_405;
// System.Byte[]
struct ByteU5BU5D_t1_109;
// System.Char[]
struct CharU5BU5D_t1_16;
// System.IO.StreamWriter
struct StreamWriter_t1_448;

#include "mscorlib_System_IO_TextWriter.h"

// System.IO.StreamWriter
struct  StreamWriter_t1_448  : public TextWriter_t1_449
{
	// System.Text.Encoding System.IO.StreamWriter::internalEncoding
	Encoding_t1_406 * ___internalEncoding_6;
	// System.IO.Stream System.IO.StreamWriter::internalStream
	Stream_t1_405 * ___internalStream_7;
	// System.Boolean System.IO.StreamWriter::iflush
	bool ___iflush_8;
	// System.Byte[] System.IO.StreamWriter::byte_buf
	ByteU5BU5D_t1_109* ___byte_buf_9;
	// System.Int32 System.IO.StreamWriter::byte_pos
	int32_t ___byte_pos_10;
	// System.Char[] System.IO.StreamWriter::decode_buf
	CharU5BU5D_t1_16* ___decode_buf_11;
	// System.Int32 System.IO.StreamWriter::decode_pos
	int32_t ___decode_pos_12;
	// System.Boolean System.IO.StreamWriter::DisposedAlready
	bool ___DisposedAlready_13;
	// System.Boolean System.IO.StreamWriter::preamble_done
	bool ___preamble_done_14;
};
struct StreamWriter_t1_448_StaticFields{
	// System.IO.StreamWriter System.IO.StreamWriter::Null
	StreamWriter_t1_448 * ___Null_15;
};
