﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.VariantWrapper
struct VariantWrapper_t1_849;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.InteropServices.VariantWrapper::.ctor(System.Object)
extern "C" void VariantWrapper__ctor_m1_7940 (VariantWrapper_t1_849 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.VariantWrapper::get_WrappedObject()
extern "C" Object_t * VariantWrapper_get_WrappedObject_m1_7941 (VariantWrapper_t1_849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
