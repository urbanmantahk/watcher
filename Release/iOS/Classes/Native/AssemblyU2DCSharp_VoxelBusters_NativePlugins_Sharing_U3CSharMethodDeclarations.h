﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF
struct U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "codegen/il2cpp-codegen.h"

// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF::.ctor()
extern "C" void U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF__ctor_m8_1580 (U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Sharing/<ShareScreenshotOnWhatsApp>c__AnonStoreyF::<>m__10(UnityEngine.Texture2D)
extern "C" void U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_U3CU3Em__10_m8_1581 (U3CShareScreenshotOnWhatsAppU3Ec__AnonStoreyF_t8_276 * __this, Texture2D_t6_33 * ____texture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
