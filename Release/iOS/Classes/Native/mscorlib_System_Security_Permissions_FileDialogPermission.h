﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Security_CodeAccessPermission.h"
#include "mscorlib_System_Security_Permissions_FileDialogPermissionAcc.h"

// System.Security.Permissions.FileDialogPermission
struct  FileDialogPermission_t1_1271  : public CodeAccessPermission_t1_1268
{
	// System.Security.Permissions.FileDialogPermissionAccess System.Security.Permissions.FileDialogPermission::_access
	int32_t ____access_1;
};
