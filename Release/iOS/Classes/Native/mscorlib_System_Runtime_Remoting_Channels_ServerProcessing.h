﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ServerProcessing.h"

// System.Runtime.Remoting.Channels.ServerProcessing
struct  ServerProcessing_t1_888 
{
	// System.Int32 System.Runtime.Remoting.Channels.ServerProcessing::value__
	int32_t ___value___1;
};
