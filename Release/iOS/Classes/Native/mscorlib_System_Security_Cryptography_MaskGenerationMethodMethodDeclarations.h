﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.MaskGenerationMethod
struct MaskGenerationMethod_t1_1224;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.MaskGenerationMethod::.ctor()
extern "C" void MaskGenerationMethod__ctor_m1_10431 (MaskGenerationMethod_t1_1224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
