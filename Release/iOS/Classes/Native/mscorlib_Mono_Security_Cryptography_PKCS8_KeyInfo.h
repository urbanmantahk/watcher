﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_Mono_Security_Cryptography_PKCS8_KeyInfo.h"

// Mono.Security.Cryptography.PKCS8/KeyInfo
struct  KeyInfo_t1_168 
{
	// System.Int32 Mono.Security.Cryptography.PKCS8/KeyInfo::value__
	int32_t ___value___1;
};
