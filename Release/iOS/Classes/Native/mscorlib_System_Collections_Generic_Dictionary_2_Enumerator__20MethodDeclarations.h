﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__22MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1_26869(__this, ___dictionary, method) (( void (*) (Enumerator_t1_2699 *, Dictionary_2_t1_1929 *, const MethodInfo*))Enumerator__ctor_m1_26870_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_26871(__this, method) (( Object_t * (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_26872_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1_26873(__this, method) (( void (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_26874_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26875(__this, method) (( DictionaryEntry_t1_284  (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1_26876_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26877(__this, method) (( Object_t * (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1_26878_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26879(__this, method) (( Object_t * (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1_26880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::MoveNext()
#define Enumerator_MoveNext_m1_26881(__this, method) (( bool (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_MoveNext_m1_26882_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_Current()
#define Enumerator_get_Current_m1_26883(__this, method) (( KeyValuePair_2_t1_2696  (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_get_Current_m1_26884_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1_26885(__this, method) (( String_t* (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_get_CurrentKey_m1_26886_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1_26887(__this, method) (( float (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_get_CurrentValue_m1_26888_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Reset()
#define Enumerator_Reset_m1_26889(__this, method) (( void (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_Reset_m1_26890_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyState()
#define Enumerator_VerifyState_m1_26891(__this, method) (( void (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_VerifyState_m1_26892_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1_26893(__this, method) (( void (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_VerifyCurrent_m1_26894_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Single>::Dispose()
#define Enumerator_Dispose_m1_26895(__this, method) (( void (*) (Enumerator_t1_2699 *, const MethodInfo*))Enumerator_Dispose_m1_26896_gshared)(__this, method)
