﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttribute.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapOption.h"
#include "mscorlib_System_Runtime_Remoting_Metadata_XmlFieldOrderOptio.h"

// System.Runtime.Remoting.Metadata.SoapTypeAttribute
struct  SoapTypeAttribute_t1_1002  : public SoapAttribute_t1_997
{
	// System.Runtime.Remoting.Metadata.SoapOption System.Runtime.Remoting.Metadata.SoapTypeAttribute::_soapOption
	int32_t ____soapOption_4;
	// System.Boolean System.Runtime.Remoting.Metadata.SoapTypeAttribute::_useAttribute
	bool ____useAttribute_5;
	// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::_xmlElementName
	String_t* ____xmlElementName_6;
	// System.Runtime.Remoting.Metadata.XmlFieldOrderOption System.Runtime.Remoting.Metadata.SoapTypeAttribute::_xmlFieldOrder
	int32_t ____xmlFieldOrder_7;
	// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::_xmlNamespace
	String_t* ____xmlNamespace_8;
	// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::_xmlTypeName
	String_t* ____xmlTypeName_9;
	// System.String System.Runtime.Remoting.Metadata.SoapTypeAttribute::_xmlTypeNamespace
	String_t* ____xmlTypeNamespace_10;
	// System.Boolean System.Runtime.Remoting.Metadata.SoapTypeAttribute::_isType
	bool ____isType_11;
	// System.Boolean System.Runtime.Remoting.Metadata.SoapTypeAttribute::_isElement
	bool ____isElement_12;
};
