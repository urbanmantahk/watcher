﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ExifLibrary_ExifProperty.h"
#include "AssemblyU2DCSharp_ExifLibrary_GPSLatitudeRef.h"

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.GPSLatitudeRef>
struct  ExifEnumProperty_1_t8_361  : public ExifProperty_t8_99
{
	// T ExifLibrary.ExifEnumProperty`1::mValue
	uint8_t ___mValue_3;
	// System.Boolean ExifLibrary.ExifEnumProperty`1::mIsBitField
	bool ___mIsBitField_4;
};
