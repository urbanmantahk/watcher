﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion
struct ReadContactsCompletion_t8_194;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.AddressBookContact[]
struct AddressBookContactU5BU5D_t8_177;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eABAuthorizatio.h"

// System.Void VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void ReadContactsCompletion__ctor_m8_1122 (ReadContactsCompletion_t8_194 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::Invoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBookContact[])
extern "C" void ReadContactsCompletion_Invoke_m8_1123 (ReadContactsCompletion_t8_194 * __this, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_ReadContactsCompletion_t8_194(Il2CppObject* delegate, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList);
// System.IAsyncResult VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::BeginInvoke(VoxelBusters.NativePlugins.eABAuthorizationStatus,VoxelBusters.NativePlugins.AddressBookContact[],System.AsyncCallback,System.Object)
extern "C" Object_t * ReadContactsCompletion_BeginInvoke_m8_1124 (ReadContactsCompletion_t8_194 * __this, int32_t ____authorizationStatus, AddressBookContactU5BU5D_t8_177* ____contactList, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.AddressBook/ReadContactsCompletion::EndInvoke(System.IAsyncResult)
extern "C" void ReadContactsCompletion_EndInvoke_m8_1125 (ReadContactsCompletion_t8_194 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
