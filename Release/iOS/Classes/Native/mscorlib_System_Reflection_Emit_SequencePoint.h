﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType.h"

// System.Reflection.Emit.SequencePoint
struct  SequencePoint_t1_526 
{
	// System.Int32 System.Reflection.Emit.SequencePoint::Offset
	int32_t ___Offset_0;
	// System.Int32 System.Reflection.Emit.SequencePoint::Line
	int32_t ___Line_1;
	// System.Int32 System.Reflection.Emit.SequencePoint::Col
	int32_t ___Col_2;
	// System.Int32 System.Reflection.Emit.SequencePoint::EndLine
	int32_t ___EndLine_3;
	// System.Int32 System.Reflection.Emit.SequencePoint::EndCol
	int32_t ___EndCol_4;
};
