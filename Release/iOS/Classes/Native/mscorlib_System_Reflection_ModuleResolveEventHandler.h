﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Module
struct Module_t1_495;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t1_1595;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "mscorlib_System_MulticastDelegate.h"

// System.Reflection.ModuleResolveEventHandler
struct  ModuleResolveEventHandler_t1_561  : public MulticastDelegate_t1_21
{
};
