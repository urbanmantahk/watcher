﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Attribute
struct Attribute_t1_2;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t1_272;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1_627;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.Assembly
struct Assembly_t1_467;
// System.Reflection.Module
struct Module_t1_495;
// System.Attribute[]
struct AttributeU5BU5D_t1_1662;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_IntPtr.h"

// System.Void System.Attribute::.ctor()
extern "C" void Attribute__ctor_m1_20 (Attribute_t1_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::System.Runtime.InteropServices._Attribute.GetIDsOfNames(System.Guid&,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Attribute_System_Runtime_InteropServices__Attribute_GetIDsOfNames_m1_21 (Attribute_t1_2 * __this, Guid_t1_319 * ___riid, IntPtr_t ___rgszNames, uint32_t ___cNames, uint32_t ___lcid, IntPtr_t ___rgDispId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::System.Runtime.InteropServices._Attribute.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)
extern "C" void Attribute_System_Runtime_InteropServices__Attribute_GetTypeInfo_m1_22 (Attribute_t1_2 * __this, uint32_t ___iTInfo, uint32_t ___lcid, IntPtr_t ___ppTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::System.Runtime.InteropServices._Attribute.GetTypeInfoCount(System.UInt32&)
extern "C" void Attribute_System_Runtime_InteropServices__Attribute_GetTypeInfoCount_m1_23 (Attribute_t1_2 * __this, uint32_t* ___pcTInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::System.Runtime.InteropServices._Attribute.Invoke(System.UInt32,System.Guid&,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern "C" void Attribute_System_Runtime_InteropServices__Attribute_Invoke_m1_24 (Attribute_t1_2 * __this, uint32_t ___dispIdMember, Guid_t1_319 * ___riid, uint32_t ___lcid, int16_t ___wFlags, IntPtr_t ___pDispParams, IntPtr_t ___pVarResult, IntPtr_t ___pExcepInfo, IntPtr_t ___puArgErr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Attribute::get_TypeId()
extern "C" Object_t * Attribute_get_TypeId_m1_25 (Attribute_t1_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::CheckParameters(System.Object,System.Type)
extern "C" void Attribute_CheckParameters_m1_26 (Object_t * __this /* static, unused */, Object_t * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::FindAttribute(System.Object[])
extern "C" Attribute_t1_2 * Attribute_FindAttribute_m1_27 (Object_t * __this /* static, unused */, ObjectU5BU5D_t1_272* ___attributes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.ParameterInfo,System.Type)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_28 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.MemberInfo,System.Type)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_29 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.Assembly,System.Type)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_30 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.Module,System.Type)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_31 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.Module,System.Type,System.Boolean)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_32 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.Assembly,System.Type,System.Boolean)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_33 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.ParameterInfo,System.Type,System.Boolean)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_34 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute System.Attribute::GetCustomAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern "C" Attribute_t1_2 * Attribute_GetCustomAttribute_m1_35 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Assembly)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_36 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.ParameterInfo)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_37 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.MemberInfo)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_38 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Module)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_39 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Assembly,System.Type)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_40 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Module,System.Type)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_41 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.ParameterInfo,System.Type)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_42 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.MemberInfo,System.Type)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_43 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Assembly,System.Type,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_44 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.ParameterInfo,System.Type,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_45 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Module,System.Type,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_46 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_47 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, Type_t * ___type, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Module,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_48 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.Assembly,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_49 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.MemberInfo,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_50 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Attribute[] System.Attribute::GetCustomAttributes(System.Reflection.ParameterInfo,System.Boolean)
extern "C" AttributeU5BU5D_t1_1662* Attribute_GetCustomAttributes_m1_51 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Attribute::GetHashCode()
extern "C" int32_t Attribute_GetHashCode_m1_52 (Attribute_t1_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefaultAttribute()
extern "C" bool Attribute_IsDefaultAttribute_m1_53 (Attribute_t1_2 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.Module,System.Type)
extern "C" bool Attribute_IsDefined_m1_54 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.ParameterInfo,System.Type)
extern "C" bool Attribute_IsDefined_m1_55 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.MemberInfo,System.Type)
extern "C" bool Attribute_IsDefined_m1_56 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.Assembly,System.Type)
extern "C" bool Attribute_IsDefined_m1_57 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, Type_t * ___attributeType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern "C" bool Attribute_IsDefined_m1_58 (Object_t * __this /* static, unused */, MemberInfo_t * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.Assembly,System.Type,System.Boolean)
extern "C" bool Attribute_IsDefined_m1_59 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.Module,System.Type,System.Boolean)
extern "C" bool Attribute_IsDefined_m1_60 (Object_t * __this /* static, unused */, Module_t1_495 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::IsDefined(System.Reflection.ParameterInfo,System.Type,System.Boolean)
extern "C" bool Attribute_IsDefined_m1_61 (Object_t * __this /* static, unused */, ParameterInfo_t1_627 * ___element, Type_t * ___attributeType, bool ___inherit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::Match(System.Object)
extern "C" bool Attribute_Match_m1_62 (Attribute_t1_2 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Attribute::Equals(System.Object)
extern "C" bool Attribute_Equals_m1_63 (Attribute_t1_2 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
