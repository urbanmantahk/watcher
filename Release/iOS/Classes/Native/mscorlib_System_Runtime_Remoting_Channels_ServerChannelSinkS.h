﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.Channels.ChanelSinkStackEntry
struct ChanelSinkStackEntry_t1_876;

#include "mscorlib_System_Object.h"

// System.Runtime.Remoting.Channels.ServerChannelSinkStack
struct  ServerChannelSinkStack_t1_885  : public Object_t
{
	// System.Runtime.Remoting.Channels.ChanelSinkStackEntry System.Runtime.Remoting.Channels.ServerChannelSinkStack::_sinkStack
	ChanelSinkStackEntry_t1_876 * ____sinkStack_0;
};
