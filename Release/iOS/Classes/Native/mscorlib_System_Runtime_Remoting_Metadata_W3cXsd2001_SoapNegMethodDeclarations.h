﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger
struct SoapNegativeInteger_t1_984;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::.ctor()
extern "C" void SoapNegativeInteger__ctor_m1_8816 (SoapNegativeInteger_t1_984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::.ctor(System.Decimal)
extern "C" void SoapNegativeInteger__ctor_m1_8817 (SoapNegativeInteger_t1_984 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::get_Value()
extern "C" Decimal_t1_19  SoapNegativeInteger_get_Value_m1_8818 (SoapNegativeInteger_t1_984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::set_Value(System.Decimal)
extern "C" void SoapNegativeInteger_set_Value_m1_8819 (SoapNegativeInteger_t1_984 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::get_XsdType()
extern "C" String_t* SoapNegativeInteger_get_XsdType_m1_8820 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::GetXsdType()
extern "C" String_t* SoapNegativeInteger_GetXsdType_m1_8821 (SoapNegativeInteger_t1_984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::Parse(System.String)
extern "C" SoapNegativeInteger_t1_984 * SoapNegativeInteger_Parse_m1_8822 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNegativeInteger::ToString()
extern "C" String_t* SoapNegativeInteger_ToString_m1_8823 (SoapNegativeInteger_t1_984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
