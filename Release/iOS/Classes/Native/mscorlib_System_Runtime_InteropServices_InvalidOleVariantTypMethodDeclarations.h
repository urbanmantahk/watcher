﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.InteropServices.InvalidOleVariantTypeException
struct InvalidOleVariantTypeException_t1_807;
// System.String
struct String_t;
// System.Exception
struct Exception_t1_33;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor()
extern "C" void InvalidOleVariantTypeException__ctor_m1_7689 (InvalidOleVariantTypeException_t1_807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor(System.String)
extern "C" void InvalidOleVariantTypeException__ctor_m1_7690 (InvalidOleVariantTypeException_t1_807 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor(System.String,System.Exception)
extern "C" void InvalidOleVariantTypeException__ctor_m1_7691 (InvalidOleVariantTypeException_t1_807 * __this, String_t* ___message, Exception_t1_33 * ___inner, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.InvalidOleVariantTypeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void InvalidOleVariantTypeException__ctor_m1_7692 (InvalidOleVariantTypeException_t1_807 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
