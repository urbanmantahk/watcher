﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t1_35;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Inte_0.h"

// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::.ctor(System.Boolean)
extern "C" void ProductUpdateInfo__ctor_m8_138 (ProductUpdateInfo_t8_21 * __this, bool ____newUpdateAvailable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::.ctor(System.String,System.Collections.IDictionary)
extern "C" void ProductUpdateInfo__ctor_m8_139 (ProductUpdateInfo_t8_21 * __this, String_t* ____currentVersion, Object_t * ____dataDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_NewUpdateAvailable()
extern "C" bool ProductUpdateInfo_get_NewUpdateAvailable_m8_140 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_NewUpdateAvailable(System.Boolean)
extern "C" void ProductUpdateInfo_set_NewUpdateAvailable_m8_141 (ProductUpdateInfo_t8_21 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_VersionNumber()
extern "C" String_t* ProductUpdateInfo_get_VersionNumber_m8_142 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_VersionNumber(System.String)
extern "C" void ProductUpdateInfo_set_VersionNumber_m8_143 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_DownloadLink()
extern "C" String_t* ProductUpdateInfo_get_DownloadLink_m8_144 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_DownloadLink(System.String)
extern "C" void ProductUpdateInfo_set_DownloadLink_m8_145 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_AssetStoreLink()
extern "C" String_t* ProductUpdateInfo_get_AssetStoreLink_m8_146 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_AssetStoreLink(System.String)
extern "C" void ProductUpdateInfo_set_AssetStoreLink_m8_147 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::get_ReleaseNote()
extern "C" String_t* ProductUpdateInfo_get_ReleaseNote_m8_148 (ProductUpdateInfo_t8_21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.AssetStoreProductUtility.Internal.ProductUpdateInfo::set_ReleaseNote(System.String)
extern "C" void ProductUpdateInfo_set_ReleaseNote_m8_149 (ProductUpdateInfo_t8_21 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void ProductUpdateInfo_t8_21_marshal(const ProductUpdateInfo_t8_21& unmarshaled, ProductUpdateInfo_t8_21_marshaled& marshaled);
extern "C" void ProductUpdateInfo_t8_21_marshal_back(const ProductUpdateInfo_t8_21_marshaled& marshaled, ProductUpdateInfo_t8_21& unmarshaled);
extern "C" void ProductUpdateInfo_t8_21_marshal_cleanup(ProductUpdateInfo_t8_21_marshaled& marshaled);
