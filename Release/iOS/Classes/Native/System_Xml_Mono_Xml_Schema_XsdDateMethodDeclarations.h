﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Xml.Schema.XsdDate
struct XsdDate_t4_46;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlTokenizedType.h"

// System.Void Mono.Xml.Schema.XsdDate::.ctor()
extern "C" void XsdDate__ctor_m4_68 (XsdDate_t4_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlTokenizedType Mono.Xml.Schema.XsdDate::get_TokenizedType()
extern "C" int32_t XsdDate_get_TokenizedType_m4_69 (XsdDate_t4_46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
