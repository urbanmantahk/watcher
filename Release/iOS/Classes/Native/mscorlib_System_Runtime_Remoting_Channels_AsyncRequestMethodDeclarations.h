﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Channels.AsyncRequest
struct AsyncRequest_t1_884;
// System.Runtime.Remoting.Messaging.IMessage
struct IMessage_t1_875;
// System.Runtime.Remoting.Messaging.IMessageSink
struct IMessageSink_t1_874;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Channels.AsyncRequest::.ctor(System.Runtime.Remoting.Messaging.IMessage,System.Runtime.Remoting.Messaging.IMessageSink)
extern "C" void AsyncRequest__ctor_m1_8099 (AsyncRequest_t1_884 * __this, Object_t * ___msgRequest, Object_t * ___replySink, const MethodInfo* method) IL2CPP_METHOD_ATTR;
