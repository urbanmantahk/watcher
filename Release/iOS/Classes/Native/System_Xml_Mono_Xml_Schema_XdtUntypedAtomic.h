﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType.h"

// Mono.Xml.Schema.XdtUntypedAtomic
struct  XdtUntypedAtomic_t4_6  : public XdtAnyAtomicType_t4_5
{
};
