﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.MutexAccessRule
struct MutexAccessRule_t1_1158;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_MutexRights.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"

// System.Void System.Security.AccessControl.MutexAccessRule::.ctor(System.Security.Principal.IdentityReference,System.Security.AccessControl.MutexRights,System.Security.AccessControl.AccessControlType)
extern "C" void MutexAccessRule__ctor_m1_9932 (MutexAccessRule_t1_1158 * __this, IdentityReference_t1_1120 * ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.MutexAccessRule::.ctor(System.String,System.Security.AccessControl.MutexRights,System.Security.AccessControl.AccessControlType)
extern "C" void MutexAccessRule__ctor_m1_9933 (MutexAccessRule_t1_1158 * __this, String_t* ___identity, int32_t ___eventRights, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.MutexRights System.Security.AccessControl.MutexAccessRule::get_MutexRights()
extern "C" int32_t MutexAccessRule_get_MutexRights_m1_9934 (MutexAccessRule_t1_1158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
