﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Object,System.Object>
struct SortedList_2_t3_255;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_KeyEnumerator.h"

// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void KeyEnumerator__ctor_m3_1941_gshared (KeyEnumerator_t3_258 * __this, SortedList_2_t3_255 * ___l, const MethodInfo* method);
#define KeyEnumerator__ctor_m3_1941(__this, ___l, method) (( void (*) (KeyEnumerator_t3_258 *, SortedList_2_t3_255 *, const MethodInfo*))KeyEnumerator__ctor_m3_1941_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void KeyEnumerator_System_Collections_IEnumerator_Reset_m3_1942_gshared (KeyEnumerator_t3_258 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_Reset_m3_1942(__this, method) (( void (*) (KeyEnumerator_t3_258 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_Reset_m3_1942_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_1943_gshared (KeyEnumerator_t3_258 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_1943(__this, method) (( Object_t * (*) (KeyEnumerator_t3_258 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_1943_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::Dispose()
extern "C" void KeyEnumerator_Dispose_m3_1944_gshared (KeyEnumerator_t3_258 * __this, const MethodInfo* method);
#define KeyEnumerator_Dispose_m3_1944(__this, method) (( void (*) (KeyEnumerator_t3_258 *, const MethodInfo*))KeyEnumerator_Dispose_m3_1944_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool KeyEnumerator_MoveNext_m3_1945_gshared (KeyEnumerator_t3_258 * __this, const MethodInfo* method);
#define KeyEnumerator_MoveNext_m3_1945(__this, method) (( bool (*) (KeyEnumerator_t3_258 *, const MethodInfo*))KeyEnumerator_MoveNext_m3_1945_gshared)(__this, method)
// TKey System.Collections.Generic.SortedList`2/KeyEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * KeyEnumerator_get_Current_m3_1946_gshared (KeyEnumerator_t3_258 * __this, const MethodInfo* method);
#define KeyEnumerator_get_Current_m3_1946(__this, method) (( Object_t * (*) (KeyEnumerator_t3_258 *, const MethodInfo*))KeyEnumerator_get_Current_m3_1946_gshared)(__this, method)
