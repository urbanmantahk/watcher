﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.SoapServices
struct SoapServices_t1_1037;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Collections.Hashtable
struct Hashtable_t1_100;
// System.Reflection.MethodBase
struct MethodBase_t1_335;
// System.Reflection.Assembly
struct Assembly_t1_467;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.SoapServices::.ctor()
extern "C" void SoapServices__ctor_m1_9275 (SoapServices_t1_1037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::.cctor()
extern "C" void SoapServices__cctor_m1_9276 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::get_XmlNsForClrType()
extern "C" String_t* SoapServices_get_XmlNsForClrType_m1_9277 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::get_XmlNsForClrTypeWithAssembly()
extern "C" String_t* SoapServices_get_XmlNsForClrTypeWithAssembly_m1_9278 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::get_XmlNsForClrTypeWithNs()
extern "C" String_t* SoapServices_get_XmlNsForClrTypeWithNs_m1_9279 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::get_XmlNsForClrTypeWithNsAndAssembly()
extern "C" String_t* SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m1_9280 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::CodeXmlNamespaceForClrTypeNamespace(System.String,System.String)
extern "C" String_t* SoapServices_CodeXmlNamespaceForClrTypeNamespace_m1_9281 (Object_t * __this /* static, unused */, String_t* ___typeNamespace, String_t* ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.SoapServices::DecodeXmlNamespaceForClrTypeNamespace(System.String,System.String&,System.String&)
extern "C" bool SoapServices_DecodeXmlNamespaceForClrTypeNamespace_m1_9282 (Object_t * __this /* static, unused */, String_t* ___inNamespace, String_t** ___typeNamespace, String_t** ___assemblyName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::GetInteropFieldTypeAndNameFromXmlAttribute(System.Type,System.String,System.String,System.Type&,System.String&)
extern "C" void SoapServices_GetInteropFieldTypeAndNameFromXmlAttribute_m1_9283 (Object_t * __this /* static, unused */, Type_t * ___containingType, String_t* ___xmlAttribute, String_t* ___xmlNamespace, Type_t ** ___type, String_t** ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::GetInteropFieldTypeAndNameFromXmlElement(System.Type,System.String,System.String,System.Type&,System.String&)
extern "C" void SoapServices_GetInteropFieldTypeAndNameFromXmlElement_m1_9284 (Object_t * __this /* static, unused */, Type_t * ___containingType, String_t* ___xmlElement, String_t* ___xmlNamespace, Type_t ** ___type, String_t** ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::GetInteropFieldInfo(System.Collections.Hashtable,System.String,System.String,System.Type&,System.String&)
extern "C" void SoapServices_GetInteropFieldInfo_m1_9285 (Object_t * __this /* static, unused */, Hashtable_t1_100 * ___fields, String_t* ___xmlName, String_t* ___xmlNamespace, Type_t ** ___type, String_t** ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::GetNameKey(System.String,System.String)
extern "C" String_t* SoapServices_GetNameKey_m1_9286 (Object_t * __this /* static, unused */, String_t* ___name, String_t* ___namspace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.SoapServices::GetInteropTypeFromXmlElement(System.String,System.String)
extern "C" Type_t * SoapServices_GetInteropTypeFromXmlElement_m1_9287 (Object_t * __this /* static, unused */, String_t* ___xmlElement, String_t* ___xmlNamespace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Runtime.Remoting.SoapServices::GetInteropTypeFromXmlType(System.String,System.String)
extern "C" Type_t * SoapServices_GetInteropTypeFromXmlType_m1_9288 (Object_t * __this /* static, unused */, String_t* ___xmlType, String_t* ___xmlTypeNamespace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::GetAssemblyName(System.Reflection.MethodBase)
extern "C" String_t* SoapServices_GetAssemblyName_m1_9289 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::GetSoapActionFromMethodBase(System.Reflection.MethodBase)
extern "C" String_t* SoapServices_GetSoapActionFromMethodBase_m1_9290 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.SoapServices::GetTypeAndMethodNameFromSoapAction(System.String,System.String&,System.String&)
extern "C" bool SoapServices_GetTypeAndMethodNameFromSoapAction_m1_9291 (Object_t * __this /* static, unused */, String_t* ___soapAction, String_t** ___typeName, String_t** ___methodName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.SoapServices::GetXmlElementForInteropType(System.Type,System.String&,System.String&)
extern "C" bool SoapServices_GetXmlElementForInteropType_m1_9292 (Object_t * __this /* static, unused */, Type_t * ___type, String_t** ___xmlElement, String_t** ___xmlNamespace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::GetXmlNamespaceForMethodCall(System.Reflection.MethodBase)
extern "C" String_t* SoapServices_GetXmlNamespaceForMethodCall_m1_9293 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::GetXmlNamespaceForMethodResponse(System.Reflection.MethodBase)
extern "C" String_t* SoapServices_GetXmlNamespaceForMethodResponse_m1_9294 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.SoapServices::GetXmlTypeForInteropType(System.Type,System.String&,System.String&)
extern "C" bool SoapServices_GetXmlTypeForInteropType_m1_9295 (Object_t * __this /* static, unused */, Type_t * ___type, String_t** ___xmlType, String_t** ___xmlTypeNamespace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.SoapServices::IsClrTypeNamespace(System.String)
extern "C" bool SoapServices_IsClrTypeNamespace_m1_9296 (Object_t * __this /* static, unused */, String_t* ___namespaceString, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Runtime.Remoting.SoapServices::IsSoapActionValidForMethodBase(System.String,System.Reflection.MethodBase)
extern "C" bool SoapServices_IsSoapActionValidForMethodBase_m1_9297 (Object_t * __this /* static, unused */, String_t* ___soapAction, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::PreLoad(System.Reflection.Assembly)
extern "C" void SoapServices_PreLoad_m1_9298 (Object_t * __this /* static, unused */, Assembly_t1_467 * ___assembly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::PreLoad(System.Type)
extern "C" void SoapServices_PreLoad_m1_9299 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::RegisterInteropXmlElement(System.String,System.String,System.Type)
extern "C" void SoapServices_RegisterInteropXmlElement_m1_9300 (Object_t * __this /* static, unused */, String_t* ___xmlElement, String_t* ___xmlNamespace, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::RegisterInteropXmlType(System.String,System.String,System.Type)
extern "C" void SoapServices_RegisterInteropXmlType_m1_9301 (Object_t * __this /* static, unused */, String_t* ___xmlType, String_t* ___xmlTypeNamespace, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::RegisterSoapActionForMethodBase(System.Reflection.MethodBase)
extern "C" void SoapServices_RegisterSoapActionForMethodBase_m1_9302 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::InternalGetSoapAction(System.Reflection.MethodBase)
extern "C" String_t* SoapServices_InternalGetSoapAction_m1_9303 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.SoapServices::RegisterSoapActionForMethodBase(System.Reflection.MethodBase,System.String)
extern "C" void SoapServices_RegisterSoapActionForMethodBase_m1_9304 (Object_t * __this /* static, unused */, MethodBase_t1_335 * ___mb, String_t* ___soapAction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::EncodeNs(System.String)
extern "C" String_t* SoapServices_EncodeNs_m1_9305 (Object_t * __this /* static, unused */, String_t* ___ns, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.SoapServices::DecodeNs(System.String)
extern "C" String_t* SoapServices_DecodeNs_m1_9306 (Object_t * __this /* static, unused */, String_t* ___ns, const MethodInfo* method) IL2CPP_METHOD_ATTR;
