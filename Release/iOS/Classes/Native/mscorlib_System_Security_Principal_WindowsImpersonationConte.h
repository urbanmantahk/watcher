﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object.h"
#include "mscorlib_System_IntPtr.h"

// System.Security.Principal.WindowsImpersonationContext
struct  WindowsImpersonationContext_t1_1385  : public Object_t
{
	// System.IntPtr System.Security.Principal.WindowsImpersonationContext::_token
	IntPtr_t ____token_0;
	// System.Boolean System.Security.Principal.WindowsImpersonationContext::undo
	bool ___undo_1;
};
