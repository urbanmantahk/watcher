﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedList`2<System.Int32,ExifLibrary.IFD>
struct SortedList_2_t3_248;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedList_2_KeyEnumerator_0.h"

// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::.ctor(System.Collections.Generic.SortedList`2<TKey,TValue>)
extern "C" void KeyEnumerator__ctor_m3_2191_gshared (KeyEnumerator_t3_281 * __this, SortedList_2_t3_248 * ___l, const MethodInfo* method);
#define KeyEnumerator__ctor_m3_2191(__this, ___l, method) (( void (*) (KeyEnumerator_t3_281 *, SortedList_2_t3_248 *, const MethodInfo*))KeyEnumerator__ctor_m3_2191_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.Reset()
extern "C" void KeyEnumerator_System_Collections_IEnumerator_Reset_m3_2192_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_Reset_m3_2192(__this, method) (( void (*) (KeyEnumerator_t3_281 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_Reset_m3_2192_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_2193_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method);
#define KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_2193(__this, method) (( Object_t * (*) (KeyEnumerator_t3_281 *, const MethodInfo*))KeyEnumerator_System_Collections_IEnumerator_get_Current_m3_2193_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::Dispose()
extern "C" void KeyEnumerator_Dispose_m3_2194_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method);
#define KeyEnumerator_Dispose_m3_2194(__this, method) (( void (*) (KeyEnumerator_t3_281 *, const MethodInfo*))KeyEnumerator_Dispose_m3_2194_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::MoveNext()
extern "C" bool KeyEnumerator_MoveNext_m3_2195_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method);
#define KeyEnumerator_MoveNext_m3_2195(__this, method) (( bool (*) (KeyEnumerator_t3_281 *, const MethodInfo*))KeyEnumerator_MoveNext_m3_2195_gshared)(__this, method)
// TKey System.Collections.Generic.SortedList`2/KeyEnumerator<System.Int32,ExifLibrary.IFD>::get_Current()
extern "C" int32_t KeyEnumerator_get_Current_m3_2196_gshared (KeyEnumerator_t3_281 * __this, const MethodInfo* method);
#define KeyEnumerator_get_Current_m3_2196(__this, method) (( int32_t (*) (KeyEnumerator_t3_281 *, const MethodInfo*))KeyEnumerator_get_Current_m3_2196_gshared)(__this, method)
