﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_ValueType.h"

// System.Runtime.InteropServices.ComTypes.CONNECTDATA
struct  CONNECTDATA_t1_723 
{
	// System.Object System.Runtime.InteropServices.ComTypes.CONNECTDATA::pUnk
	Object_t * ___pUnk_0;
	// System.Int32 System.Runtime.InteropServices.ComTypes.CONNECTDATA::dwCookie
	int32_t ___dwCookie_1;
};
