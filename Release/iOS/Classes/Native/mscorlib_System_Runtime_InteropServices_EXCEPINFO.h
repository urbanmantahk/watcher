﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType.h"
#include "mscorlib_System_IntPtr.h"

// System.Runtime.InteropServices.EXCEPINFO
struct  EXCEPINFO_t1_787 
{
	// System.Int16 System.Runtime.InteropServices.EXCEPINFO::wCode
	int16_t ___wCode_0;
	// System.Int16 System.Runtime.InteropServices.EXCEPINFO::wReserved
	int16_t ___wReserved_1;
	// System.String System.Runtime.InteropServices.EXCEPINFO::bstrSource
	String_t* ___bstrSource_2;
	// System.String System.Runtime.InteropServices.EXCEPINFO::bstrDescription
	String_t* ___bstrDescription_3;
	// System.String System.Runtime.InteropServices.EXCEPINFO::bstrHelpFile
	String_t* ___bstrHelpFile_4;
	// System.Int32 System.Runtime.InteropServices.EXCEPINFO::dwHelpContext
	int32_t ___dwHelpContext_5;
	// System.IntPtr System.Runtime.InteropServices.EXCEPINFO::pvReserved
	IntPtr_t ___pvReserved_6;
	// System.IntPtr System.Runtime.InteropServices.EXCEPINFO::pfnDeferredFillIn
	IntPtr_t ___pfnDeferredFillIn_7;
};
// Native definition for marshalling of: System.Runtime.InteropServices.EXCEPINFO
struct EXCEPINFO_t1_787_marshaled
{
	int16_t ___wCode_0;
	int16_t ___wReserved_1;
	char* ___bstrSource_2;
	char* ___bstrDescription_3;
	char* ___bstrHelpFile_4;
	int32_t ___dwHelpContext_5;
	intptr_t ___pvReserved_6;
	intptr_t ___pfnDeferredFillIn_7;
};
