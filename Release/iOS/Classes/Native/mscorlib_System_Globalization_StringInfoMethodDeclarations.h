﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.StringInfo
struct StringInfo_t1_386;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Globalization.TextElementEnumerator
struct TextElementEnumerator_t1_389;
// System.Int32[]
struct Int32U5BU5D_t1_275;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.StringInfo::.ctor()
extern "C" void StringInfo__ctor_m1_4441 (StringInfo_t1_386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.StringInfo::.ctor(System.String)
extern "C" void StringInfo__ctor_m1_4442 (StringInfo_t1_386 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Globalization.StringInfo::Equals(System.Object)
extern "C" bool StringInfo_Equals_m1_4443 (StringInfo_t1_386 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.StringInfo::GetHashCode()
extern "C" int32_t StringInfo_GetHashCode_m1_4444 (StringInfo_t1_386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.StringInfo::get_LengthInTextElements()
extern "C" int32_t StringInfo_get_LengthInTextElements_m1_4445 (StringInfo_t1_386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.StringInfo::get_String()
extern "C" String_t* StringInfo_get_String_m1_4446 (StringInfo_t1_386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.StringInfo::set_String(System.String)
extern "C" void StringInfo_set_String_m1_4447 (StringInfo_t1_386 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.StringInfo::SubstringByTextElements(System.Int32)
extern "C" String_t* StringInfo_SubstringByTextElements_m1_4448 (StringInfo_t1_386 * __this, int32_t ___startingTextElement, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.StringInfo::SubstringByTextElements(System.Int32,System.Int32)
extern "C" String_t* StringInfo_SubstringByTextElements_m1_4449 (StringInfo_t1_386 * __this, int32_t ___startingTextElement, int32_t ___lengthInTextElements, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.StringInfo::GetNextTextElement(System.String)
extern "C" String_t* StringInfo_GetNextTextElement_m1_4450 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Globalization.StringInfo::GetNextTextElement(System.String,System.Int32)
extern "C" String_t* StringInfo_GetNextTextElement_m1_4451 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.StringInfo::GetNextTextElementLength(System.String,System.Int32)
extern "C" int32_t StringInfo_GetNextTextElementLength_m1_4452 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.TextElementEnumerator System.Globalization.StringInfo::GetTextElementEnumerator(System.String)
extern "C" TextElementEnumerator_t1_389 * StringInfo_GetTextElementEnumerator_m1_4453 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Globalization.TextElementEnumerator System.Globalization.StringInfo::GetTextElementEnumerator(System.String,System.Int32)
extern "C" TextElementEnumerator_t1_389 * StringInfo_GetTextElementEnumerator_m1_4454 (Object_t * __this /* static, unused */, String_t* ___str, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.StringInfo::ParseCombiningCharacters(System.String)
extern "C" Int32U5BU5D_t1_275* StringInfo_ParseCombiningCharacters_m1_4455 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
