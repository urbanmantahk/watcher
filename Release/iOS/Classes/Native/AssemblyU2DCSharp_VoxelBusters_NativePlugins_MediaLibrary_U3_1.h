﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion
struct SaveImageToGalleryCompletion_t8_241;
// VoxelBusters.NativePlugins.MediaLibrary
struct MediaLibrary_t8_245;

#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_VoxelBusters_Utility_URL.h"

// VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD
struct  U3CSaveImageToGalleryU3Ec__AnonStoreyD_t8_247  : public Object_t
{
	// VoxelBusters.Utility.URL VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::_URL
	URL_t8_156  ____URL_0;
	// VoxelBusters.NativePlugins.MediaLibrary/SaveImageToGalleryCompletion VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::_onCompletion
	SaveImageToGalleryCompletion_t8_241 * ____onCompletion_1;
	// VoxelBusters.NativePlugins.MediaLibrary VoxelBusters.NativePlugins.MediaLibrary/<SaveImageToGallery>c__AnonStoreyD::<>f__this
	MediaLibrary_t8_245 * ___U3CU3Ef__this_2;
};
