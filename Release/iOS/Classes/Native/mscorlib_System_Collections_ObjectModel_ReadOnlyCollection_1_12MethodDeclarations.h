﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_4MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m1_18183(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1_15265_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_18184(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, Cookie_t3_81 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1_15266_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_18185(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1_15267_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_18186(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, Cookie_t3_81 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1_15268_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_18187(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, Cookie_t3_81 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1_15269_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_18188(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1_15270_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_18189(__this, ___index, method) (( Cookie_t3_81 * (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1_15271_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_18190(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, Cookie_t3_81 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1_15272_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_18191(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1_15273_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_18192(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1_15274_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_18193(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1_15275_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1_18194(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2216 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1_15276_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1_18195(__this, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1_15277_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1_18196(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1_15278_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_18197(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2216 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1_15279_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1_18198(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1_15280_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1_18199(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1_15281_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_18200(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1_15282_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_18201(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1_15283_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_18202(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1_15284_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_18203(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1_15285_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_18204(__this, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1_15286_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_18205(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1_15287_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_18206(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1_15288_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::Contains(T)
#define ReadOnlyCollection_1_Contains_m1_18207(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1_2216 *, Cookie_t3_81 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m1_15289_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m1_18208(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1_2216 *, CookieU5BU5D_t3_266*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1_15290_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m1_18209(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1_15291_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1_18210(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2216 *, Cookie_t3_81 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1_15292_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1_18211(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1_15293_gshared)(__this, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::get_Items()
#define ReadOnlyCollection_1_get_Items_m1_18212(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1_2216 *, const MethodInfo*))ReadOnlyCollection_1_get_Items_m1_15294_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Net.Cookie>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m1_18213(__this, ___index, method) (( Cookie_t3_81 * (*) (ReadOnlyCollection_1_t1_2216 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1_15295_gshared)(__this, ___index, method)
