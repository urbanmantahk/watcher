﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_DTDEntityBase.h"

// Mono.Xml.DTDParameterEntityDeclaration
struct  DTDParameterEntityDeclaration_t4_100  : public DTDEntityBase_t4_97
{
};
