﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MTAThreadAttribute
struct MTAThreadAttribute_t1_1565;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MTAThreadAttribute::.ctor()
extern "C" void MTAThreadAttribute__ctor_m1_14187 (MTAThreadAttribute_t1_1565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
