﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Internal.iOSBillingTransaction
struct iOSBillingTransaction_t8_214;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// VoxelBusters.NativePlugins.BillingTransaction
struct BillingTransaction_t8_211;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eBillingTransac.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Internal_iOSBil_0.h"

// System.Void VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::.ctor(System.Collections.IDictionary)
extern "C" void iOSBillingTransaction__ctor_m8_1223 (iOSBillingTransaction_t8_214 * __this, Object_t * ____transactionJsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::CreateJSONObject(VoxelBusters.NativePlugins.BillingTransaction)
extern "C" Object_t * iOSBillingTransaction_CreateJSONObject_m8_1224 (Object_t * __this /* static, unused */, BillingTransaction_t8_211 * ____transaction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eBillingTransactionState VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::ConvertToBillingTransactionState(VoxelBusters.NativePlugins.Internal.iOSBillingTransaction/SKPaymentTransactionState)
extern "C" int32_t iOSBillingTransaction_ConvertToBillingTransactionState_m8_1225 (Object_t * __this /* static, unused */, int32_t ____skTransactionState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.Internal.iOSBillingTransaction/SKPaymentTransactionState VoxelBusters.NativePlugins.Internal.iOSBillingTransaction::ConvertToSKTransactionState(VoxelBusters.NativePlugins.eBillingTransactionState)
extern "C" int32_t iOSBillingTransaction_ConvertToSKTransactionState_m8_1226 (Object_t * __this /* static, unused */, int32_t ____billingTransactionState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
