﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.MarshalByRefObject
struct MarshalByRefObject_t1_69;

#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct  ClientActivatedIdentity_t1_1031  : public ServerIdentity_t1_70
{
	// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::_targetThis
	MarshalByRefObject_t1_69 * ____targetThis_12;
};
