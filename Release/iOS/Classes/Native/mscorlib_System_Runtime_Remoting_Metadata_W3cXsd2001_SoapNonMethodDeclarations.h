﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger
struct SoapNonNegativeInteger_t1_987;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal.h"

// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::.ctor()
extern "C" void SoapNonNegativeInteger__ctor_m1_8840 (SoapNonNegativeInteger_t1_987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::.ctor(System.Decimal)
extern "C" void SoapNonNegativeInteger__ctor_m1_8841 (SoapNonNegativeInteger_t1_987 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::get_Value()
extern "C" Decimal_t1_19  SoapNonNegativeInteger_get_Value_m1_8842 (SoapNonNegativeInteger_t1_987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::set_Value(System.Decimal)
extern "C" void SoapNonNegativeInteger_set_Value_m1_8843 (SoapNonNegativeInteger_t1_987 * __this, Decimal_t1_19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::get_XsdType()
extern "C" String_t* SoapNonNegativeInteger_get_XsdType_m1_8844 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::GetXsdType()
extern "C" String_t* SoapNonNegativeInteger_GetXsdType_m1_8845 (SoapNonNegativeInteger_t1_987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::Parse(System.String)
extern "C" SoapNonNegativeInteger_t1_987 * SoapNonNegativeInteger_Parse_m1_8846 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNonNegativeInteger::ToString()
extern "C" String_t* SoapNonNegativeInteger_ToString_m1_8847 (SoapNonNegativeInteger_t1_987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
