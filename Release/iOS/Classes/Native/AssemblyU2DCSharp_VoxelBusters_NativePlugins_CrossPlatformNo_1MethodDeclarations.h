﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.CrossPlatformNotification
struct CrossPlatformNotification_t8_259;
// System.Collections.IDictionary
struct IDictionary_t1_35;
// System.String
struct String_t;
// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties
struct iOSSpecificProperties_t8_266;
// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties
struct AndroidSpecificProperties_t8_265;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_eNotificationRe.h"

// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::.ctor()
extern "C" void CrossPlatformNotification__ctor_m8_1514 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::.ctor(System.Collections.IDictionary)
extern "C" void CrossPlatformNotification__ctor_m8_1515 (CrossPlatformNotification_t8_259 * __this, Object_t * ____jsonDict, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::get_AlertBody()
extern "C" String_t* CrossPlatformNotification_get_AlertBody_m8_1516 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_AlertBody(System.String)
extern "C" void CrossPlatformNotification_set_AlertBody_m8_1517 (CrossPlatformNotification_t8_259 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime VoxelBusters.NativePlugins.CrossPlatformNotification::get_FireDate()
extern "C" DateTime_t1_150  CrossPlatformNotification_get_FireDate_m8_1518 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_FireDate(System.DateTime)
extern "C" void CrossPlatformNotification_set_FireDate_m8_1519 (CrossPlatformNotification_t8_259 * __this, DateTime_t1_150  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.eNotificationRepeatInterval VoxelBusters.NativePlugins.CrossPlatformNotification::get_RepeatInterval()
extern "C" int32_t CrossPlatformNotification_get_RepeatInterval_m8_1520 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_RepeatInterval(VoxelBusters.NativePlugins.eNotificationRepeatInterval)
extern "C" void CrossPlatformNotification_set_RepeatInterval_m8_1521 (CrossPlatformNotification_t8_259 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification::get_UserInfo()
extern "C" Object_t * CrossPlatformNotification_get_UserInfo_m8_1522 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_UserInfo(System.Collections.IDictionary)
extern "C" void CrossPlatformNotification_set_UserInfo_m8_1523 (CrossPlatformNotification_t8_259 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::get_SoundName()
extern "C" String_t* CrossPlatformNotification_get_SoundName_m8_1524 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_SoundName(System.String)
extern "C" void CrossPlatformNotification_set_SoundName_m8_1525 (CrossPlatformNotification_t8_259 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties VoxelBusters.NativePlugins.CrossPlatformNotification::get_iOSProperties()
extern "C" iOSSpecificProperties_t8_266 * CrossPlatformNotification_get_iOSProperties_m8_1526 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_iOSProperties(VoxelBusters.NativePlugins.CrossPlatformNotification/iOSSpecificProperties)
extern "C" void CrossPlatformNotification_set_iOSProperties_m8_1527 (CrossPlatformNotification_t8_259 * __this, iOSSpecificProperties_t8_266 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties VoxelBusters.NativePlugins.CrossPlatformNotification::get_AndroidProperties()
extern "C" AndroidSpecificProperties_t8_265 * CrossPlatformNotification_get_AndroidProperties_m8_1528 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.CrossPlatformNotification::set_AndroidProperties(VoxelBusters.NativePlugins.CrossPlatformNotification/AndroidSpecificProperties)
extern "C" void CrossPlatformNotification_set_AndroidProperties_m8_1529 (CrossPlatformNotification_t8_259 * __this, AndroidSpecificProperties_t8_265 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::GetNotificationID()
extern "C" String_t* CrossPlatformNotification_GetNotificationID_m8_1530 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::GenerateNotificationID()
extern "C" String_t* CrossPlatformNotification_GenerateNotificationID_m8_1531 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary VoxelBusters.NativePlugins.CrossPlatformNotification::JSONObject()
extern "C" Object_t * CrossPlatformNotification_JSONObject_m8_1532 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VoxelBusters.NativePlugins.CrossPlatformNotification VoxelBusters.NativePlugins.CrossPlatformNotification::CreateNotificationFromPayload(System.String)
extern "C" CrossPlatformNotification_t8_259 * CrossPlatformNotification_CreateNotificationFromPayload_m8_1533 (Object_t * __this /* static, unused */, String_t* ____notificationPayload, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String VoxelBusters.NativePlugins.CrossPlatformNotification::ToString()
extern "C" String_t* CrossPlatformNotification_ToString_m8_1534 (CrossPlatformNotification_t8_259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
