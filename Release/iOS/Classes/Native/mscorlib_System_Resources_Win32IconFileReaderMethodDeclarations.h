﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.Win32IconFileReader
struct Win32IconFileReader_t1_671;
// System.IO.Stream
struct Stream_t1_405;
// System.Resources.ICONDIRENTRY[]
struct ICONDIRENTRYU5BU5D_t1_1693;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.Win32IconFileReader::.ctor(System.IO.Stream)
extern "C" void Win32IconFileReader__ctor_m1_7522 (Win32IconFileReader_t1_671 * __this, Stream_t1_405 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Resources.ICONDIRENTRY[] System.Resources.Win32IconFileReader::ReadIcons()
extern "C" ICONDIRENTRYU5BU5D_t1_1693* Win32IconFileReader_ReadIcons_m1_7523 (Win32IconFileReader_t1_671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
