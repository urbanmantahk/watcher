﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion
struct LoadScoreCompletion_t8_229;
// System.Object
struct Object_t;
// VoxelBusters.NativePlugins.Score[]
struct ScoreU5BU5D_t8_230;
// VoxelBusters.NativePlugins.Score
struct Score_t8_231;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1_27;
// System.AsyncCallback
struct AsyncCallback_t1_28;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_String.h"

// System.Void VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::.ctor(System.Object,System.IntPtr)
extern "C" void LoadScoreCompletion__ctor_m8_1281 (LoadScoreCompletion_t8_229 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::Invoke(VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score,System.String)
extern "C" void LoadScoreCompletion_Invoke_m8_1282 (LoadScoreCompletion_t8_229 * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_LoadScoreCompletion_t8_229(Il2CppObject* delegate, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error);
// System.IAsyncResult VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::BeginInvoke(VoxelBusters.NativePlugins.Score[],VoxelBusters.NativePlugins.Score,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * LoadScoreCompletion_BeginInvoke_m8_1283 (LoadScoreCompletion_t8_229 * __this, ScoreU5BU5D_t8_230* ____scores, Score_t8_231 * ____localUserScore, String_t* ____error, AsyncCallback_t1_28 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelBusters.NativePlugins.Leaderboard/LoadScoreCompletion::EndInvoke(System.IAsyncResult)
extern "C" void LoadScoreCompletion_EndInvoke_m8_1284 (LoadScoreCompletion_t8_229 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
