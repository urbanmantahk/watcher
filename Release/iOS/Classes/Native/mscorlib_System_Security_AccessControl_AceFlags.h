﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Security_AccessControl_AceFlags.h"

// System.Security.AccessControl.AceFlags
struct  AceFlags_t1_1115 
{
	// System.Byte System.Security.AccessControl.AceFlags::value__
	uint8_t ___value___1;
};
