﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// System.Runtime.Serialization.Formatters.SoapFault
struct  SoapFault_t1_1071  : public Object_t
{
	// System.String System.Runtime.Serialization.Formatters.SoapFault::code
	String_t* ___code_0;
	// System.String System.Runtime.Serialization.Formatters.SoapFault::actor
	String_t* ___actor_1;
	// System.String System.Runtime.Serialization.Formatters.SoapFault::faultString
	String_t* ___faultString_2;
	// System.Object System.Runtime.Serialization.Formatters.SoapFault::detail
	Object_t * ___detail_3;
};
