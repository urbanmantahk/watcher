﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_PlatformID_ePla.h"

// VoxelBusters.NativePlugins.PlatformID/ePlatform
struct  ePlatform_t8_330 
{
	// System.Int32 VoxelBusters.NativePlugins.PlatformID/ePlatform::value__
	int32_t ___value___1;
};
