﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Policy.ApplicationTrustCollection
struct ApplicationTrustCollection_t1_1331;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t1_193;
// System.Object
struct Object_t;
// System.Security.Policy.ApplicationTrust
struct ApplicationTrust_t1_1333;
// System.String
struct String_t;
// System.Security.Policy.ApplicationTrust[]
struct ApplicationTrustU5BU5D_t1_1732;
// System.ApplicationIdentity
struct ApplicationIdentity_t1_718;
// System.Security.Policy.ApplicationTrustEnumerator
struct ApplicationTrustEnumerator_t1_1336;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Policy_ApplicationVersionMatch.h"

// System.Void System.Security.Policy.ApplicationTrustCollection::.ctor()
extern "C" void ApplicationTrustCollection__ctor_m1_11372 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ApplicationTrustCollection_System_Collections_ICollection_CopyTo_m1_11373 (ApplicationTrustCollection_t1_1331 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Policy.ApplicationTrustCollection::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ApplicationTrustCollection_System_Collections_IEnumerable_GetEnumerator_m1_11374 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationTrustCollection::get_Count()
extern "C" int32_t ApplicationTrustCollection_get_Count_m1_11375 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.ApplicationTrustCollection::get_IsSynchronized()
extern "C" bool ApplicationTrustCollection_get_IsSynchronized_m1_11376 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Policy.ApplicationTrustCollection::get_SyncRoot()
extern "C" Object_t * ApplicationTrustCollection_get_SyncRoot_m1_11377 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.Policy.ApplicationTrustCollection::get_Item(System.Int32)
extern "C" ApplicationTrust_t1_1333 * ApplicationTrustCollection_get_Item_m1_11378 (ApplicationTrustCollection_t1_1331 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrust System.Security.Policy.ApplicationTrustCollection::get_Item(System.String)
extern "C" ApplicationTrust_t1_1333 * ApplicationTrustCollection_get_Item_m1_11379 (ApplicationTrustCollection_t1_1331 * __this, String_t* ___appFullName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.ApplicationTrustCollection::Add(System.Security.Policy.ApplicationTrust)
extern "C" int32_t ApplicationTrustCollection_Add_m1_11380 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrust_t1_1333 * ___trust, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::AddRange(System.Security.Policy.ApplicationTrust[])
extern "C" void ApplicationTrustCollection_AddRange_m1_11381 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrustU5BU5D_t1_1732* ___trusts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::AddRange(System.Security.Policy.ApplicationTrustCollection)
extern "C" void ApplicationTrustCollection_AddRange_m1_11382 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrustCollection_t1_1331 * ___trusts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::Clear()
extern "C" void ApplicationTrustCollection_Clear_m1_11383 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::CopyTo(System.Security.Policy.ApplicationTrust[],System.Int32)
extern "C" void ApplicationTrustCollection_CopyTo_m1_11384 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrustU5BU5D_t1_1732* ___array, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrustCollection System.Security.Policy.ApplicationTrustCollection::Find(System.ApplicationIdentity,System.Security.Policy.ApplicationVersionMatch)
extern "C" ApplicationTrustCollection_t1_1331 * ApplicationTrustCollection_Find_m1_11385 (ApplicationTrustCollection_t1_1331 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, int32_t ___versionMatch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Policy.ApplicationTrustEnumerator System.Security.Policy.ApplicationTrustCollection::GetEnumerator()
extern "C" ApplicationTrustEnumerator_t1_1336 * ApplicationTrustCollection_GetEnumerator_m1_11386 (ApplicationTrustCollection_t1_1331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::Remove(System.Security.Policy.ApplicationTrust)
extern "C" void ApplicationTrustCollection_Remove_m1_11387 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrust_t1_1333 * ___trust, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::Remove(System.ApplicationIdentity,System.Security.Policy.ApplicationVersionMatch)
extern "C" void ApplicationTrustCollection_Remove_m1_11388 (ApplicationTrustCollection_t1_1331 * __this, ApplicationIdentity_t1_718 * ___applicationIdentity, int32_t ___versionMatch, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::RemoveRange(System.Security.Policy.ApplicationTrust[])
extern "C" void ApplicationTrustCollection_RemoveRange_m1_11389 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrustU5BU5D_t1_1732* ___trusts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::RemoveRange(System.Security.Policy.ApplicationTrustCollection)
extern "C" void ApplicationTrustCollection_RemoveRange_m1_11390 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrustCollection_t1_1331 * ___trusts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.ApplicationTrustCollection::RemoveAllInstances(System.Security.Policy.ApplicationTrust)
extern "C" void ApplicationTrustCollection_RemoveAllInstances_m1_11391 (ApplicationTrustCollection_t1_1331 * __this, ApplicationTrust_t1_1333 * ___trust, const MethodInfo* method) IL2CPP_METHOD_ATTR;
