﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.BitConverterEx
struct BitConverterEx_t8_61;
// System.Byte[]
struct ByteU5BU5D_t1_109;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_BitConverterEx_ByteOrder.h"

// System.Void ExifLibrary.BitConverterEx::.ctor(ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" void BitConverterEx__ctor_m8_340 (BitConverterEx_t8_61 * __this, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.BitConverterEx::get_SystemByteOrder()
extern "C" int32_t BitConverterEx_get_SystemByteOrder_m8_341 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.BitConverterEx ExifLibrary.BitConverterEx::get_LittleEndian()
extern "C" BitConverterEx_t8_61 * BitConverterEx_get_LittleEndian_m8_342 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.BitConverterEx ExifLibrary.BitConverterEx::get_BigEndian()
extern "C" BitConverterEx_t8_61 * BitConverterEx_get_BigEndian_m8_343 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.BitConverterEx ExifLibrary.BitConverterEx::get_SystemEndian()
extern "C" BitConverterEx_t8_61 * BitConverterEx_get_SystemEndian_m8_344 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ExifLibrary.BitConverterEx::ToChar(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" uint16_t BitConverterEx_ToChar_m8_345 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.BitConverterEx::ToUInt16(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" uint16_t BitConverterEx_ToUInt16_m8_346 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.BitConverterEx::ToUInt32(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" uint32_t BitConverterEx_ToUInt32_m8_347 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ExifLibrary.BitConverterEx::ToUInt64(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" uint64_t BitConverterEx_ToUInt64_m8_348 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ExifLibrary.BitConverterEx::ToInt16(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" int16_t BitConverterEx_ToInt16_m8_349 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.BitConverterEx::ToInt32(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" int32_t BitConverterEx_ToInt32_m8_350 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ExifLibrary.BitConverterEx::ToInt64(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" int64_t BitConverterEx_ToInt64_m8_351 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.BitConverterEx::ToSingle(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" float BitConverterEx_ToSingle_m8_352 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLibrary.BitConverterEx::ToDouble(System.Byte[],System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" double BitConverterEx_ToDouble_m8_353 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt16,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_354 (Object_t * __this /* static, unused */, uint16_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_355 (Object_t * __this /* static, unused */, uint32_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt64,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_356 (Object_t * __this /* static, unused */, uint64_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int16,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_357 (Object_t * __this /* static, unused */, int16_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_358 (Object_t * __this /* static, unused */, int32_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int64,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_359 (Object_t * __this /* static, unused */, int64_t ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Single,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_360 (Object_t * __this /* static, unused */, float ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Double,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_361 (Object_t * __this /* static, unused */, double ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char ExifLibrary.BitConverterEx::ToChar(System.Byte[],System.Int32)
extern "C" uint16_t BitConverterEx_ToChar_m8_362 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ExifLibrary.BitConverterEx::ToUInt16(System.Byte[],System.Int32)
extern "C" uint16_t BitConverterEx_ToUInt16_m8_363 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ExifLibrary.BitConverterEx::ToUInt32(System.Byte[],System.Int32)
extern "C" uint32_t BitConverterEx_ToUInt32_m8_364 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ExifLibrary.BitConverterEx::ToUInt64(System.Byte[],System.Int32)
extern "C" uint64_t BitConverterEx_ToUInt64_m8_365 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ExifLibrary.BitConverterEx::ToInt16(System.Byte[],System.Int32)
extern "C" int16_t BitConverterEx_ToInt16_m8_366 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ExifLibrary.BitConverterEx::ToInt32(System.Byte[],System.Int32)
extern "C" int32_t BitConverterEx_ToInt32_m8_367 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ExifLibrary.BitConverterEx::ToInt64(System.Byte[],System.Int32)
extern "C" int64_t BitConverterEx_ToInt64_m8_368 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ExifLibrary.BitConverterEx::ToSingle(System.Byte[],System.Int32)
extern "C" float BitConverterEx_ToSingle_m8_369 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ExifLibrary.BitConverterEx::ToDouble(System.Byte[],System.Int32)
extern "C" double BitConverterEx_ToDouble_m8_370 (BitConverterEx_t8_61 * __this, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt16)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_371 (BitConverterEx_t8_61 * __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt32)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_372 (BitConverterEx_t8_61 * __this, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.UInt64)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_373 (BitConverterEx_t8_61 * __this, uint64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int16)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_374 (BitConverterEx_t8_61 * __this, int16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int32)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_375 (BitConverterEx_t8_61 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Int64)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_376 (BitConverterEx_t8_61 * __this, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Single)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_377 (BitConverterEx_t8_61 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::GetBytes(System.Double)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_GetBytes_m8_378 (BitConverterEx_t8_61 * __this, double ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::CheckData(System.Byte[],System.Int32,System.Int32,ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_CheckData_m8_379 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___startIndex, int32_t ___length, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ExifLibrary.BitConverterEx::CheckData(System.Byte[],ExifLibrary.BitConverterEx/ByteOrder,ExifLibrary.BitConverterEx/ByteOrder)
extern "C" ByteU5BU5D_t1_109* BitConverterEx_CheckData_m8_380 (Object_t * __this /* static, unused */, ByteU5BU5D_t1_109* ___value, int32_t ___from, int32_t ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ExifLibrary.BitConverterEx/ByteOrder ExifLibrary.BitConverterEx::CheckByteOrder(ExifLibrary.BitConverterEx/ByteOrder)
extern "C" int32_t BitConverterEx_CheckByteOrder_m8_381 (Object_t * __this /* static, unused */, int32_t ___order, const MethodInfo* method) IL2CPP_METHOD_ATTR;
