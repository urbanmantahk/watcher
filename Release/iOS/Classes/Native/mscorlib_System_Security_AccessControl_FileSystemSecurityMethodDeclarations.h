﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.AccessControl.FileSystemSecurity
struct FileSystemSecurity_t1_1148;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Security.AccessControl.AccessRule
struct AccessRule_t1_1111;
// System.Security.Principal.IdentityReference
struct IdentityReference_t1_1120;
// System.Security.AccessControl.FileSystemAccessRule
struct FileSystemAccessRule_t1_1154;
// System.Security.AccessControl.AuditRule
struct AuditRule_t1_1119;
// System.Security.AccessControl.FileSystemAuditRule
struct FileSystemAuditRule_t1_1155;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_AccessControl_AccessControlSections.h"
#include "mscorlib_System_Security_AccessControl_InheritanceFlags.h"
#include "mscorlib_System_Security_AccessControl_PropagationFlags.h"
#include "mscorlib_System_Security_AccessControl_AccessControlType.h"
#include "mscorlib_System_Security_AccessControl_AuditFlags.h"

// System.Void System.Security.AccessControl.FileSystemSecurity::.ctor(System.Boolean)
extern "C" void FileSystemSecurity__ctor_m1_9880 (FileSystemSecurity_t1_1148 * __this, bool ___isContainer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::.ctor(System.Boolean,System.String,System.Security.AccessControl.AccessControlSections)
extern "C" void FileSystemSecurity__ctor_m1_9881 (FileSystemSecurity_t1_1148 * __this, bool ___isContainer, String_t* ___name, int32_t ___includeSections, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.FileSystemSecurity::get_AccessRightType()
extern "C" Type_t * FileSystemSecurity_get_AccessRightType_m1_9882 (FileSystemSecurity_t1_1148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.FileSystemSecurity::get_AccessRuleType()
extern "C" Type_t * FileSystemSecurity_get_AccessRuleType_m1_9883 (FileSystemSecurity_t1_1148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Security.AccessControl.FileSystemSecurity::get_AuditRuleType()
extern "C" Type_t * FileSystemSecurity_get_AuditRuleType_m1_9884 (FileSystemSecurity_t1_1148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AccessRule System.Security.AccessControl.FileSystemSecurity::AccessRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AccessControlType)
extern "C" AccessRule_t1_1111 * FileSystemSecurity_AccessRuleFactory_m1_9885 (FileSystemSecurity_t1_1148 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::AddAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern "C" void FileSystemSecurity_AddAccessRule_m1_9886 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.FileSystemSecurity::RemoveAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern "C" bool FileSystemSecurity_RemoveAccessRule_m1_9887 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAccessRuleAll(System.Security.AccessControl.FileSystemAccessRule)
extern "C" void FileSystemSecurity_RemoveAccessRuleAll_m1_9888 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAccessRuleSpecific(System.Security.AccessControl.FileSystemAccessRule)
extern "C" void FileSystemSecurity_RemoveAccessRuleSpecific_m1_9889 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::ResetAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern "C" void FileSystemSecurity_ResetAccessRule_m1_9890 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::SetAccessRule(System.Security.AccessControl.FileSystemAccessRule)
extern "C" void FileSystemSecurity_SetAccessRule_m1_9891 (FileSystemSecurity_t1_1148 * __this, FileSystemAccessRule_t1_1154 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.AccessControl.AuditRule System.Security.AccessControl.FileSystemSecurity::AuditRuleFactory(System.Security.Principal.IdentityReference,System.Int32,System.Boolean,System.Security.AccessControl.InheritanceFlags,System.Security.AccessControl.PropagationFlags,System.Security.AccessControl.AuditFlags)
extern "C" AuditRule_t1_1119 * FileSystemSecurity_AuditRuleFactory_m1_9892 (FileSystemSecurity_t1_1148 * __this, IdentityReference_t1_1120 * ___identityReference, int32_t ___accessMask, bool ___isInherited, int32_t ___inheritanceFlags, int32_t ___propagationFlags, int32_t ___flags, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::AddAuditRule(System.Security.AccessControl.FileSystemAuditRule)
extern "C" void FileSystemSecurity_AddAuditRule_m1_9893 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.AccessControl.FileSystemSecurity::RemoveAuditRule(System.Security.AccessControl.FileSystemAuditRule)
extern "C" bool FileSystemSecurity_RemoveAuditRule_m1_9894 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAuditRuleAll(System.Security.AccessControl.FileSystemAuditRule)
extern "C" void FileSystemSecurity_RemoveAuditRuleAll_m1_9895 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::RemoveAuditRuleSpecific(System.Security.AccessControl.FileSystemAuditRule)
extern "C" void FileSystemSecurity_RemoveAuditRuleSpecific_m1_9896 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.AccessControl.FileSystemSecurity::SetAuditRule(System.Security.AccessControl.FileSystemAuditRule)
extern "C" void FileSystemSecurity_SetAuditRule_m1_9897 (FileSystemSecurity_t1_1148 * __this, FileSystemAuditRule_t1_1155 * ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
