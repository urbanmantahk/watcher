﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.MLangCodePageEncoding
struct MLangCodePageEncoding_t1_1442;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1_293;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Text.MLangCodePageEncoding::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MLangCodePageEncoding__ctor_m1_12409 (MLangCodePageEncoding_t1_1442 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.MLangCodePageEncoding::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MLangCodePageEncoding_GetObjectData_m1_12410 (MLangCodePageEncoding_t1_1442 * __this, SerializationInfo_t1_293 * ___info, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.MLangCodePageEncoding::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern "C" Object_t * MLangCodePageEncoding_GetRealObject_m1_12411 (MLangCodePageEncoding_t1_1442 * __this, StreamingContext_t1_1050  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
