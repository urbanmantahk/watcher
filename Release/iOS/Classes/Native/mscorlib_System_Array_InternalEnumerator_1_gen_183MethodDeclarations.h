﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_183.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m1_27060_gshared (InternalEnumerator_1_t1_2707 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1_27060(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1_2707 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m1_27060_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27061_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27061(__this, method) (( void (*) (InternalEnumerator_1_t1_2707 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1_27061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27062_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27062(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1_2707 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1_27062_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m1_27063_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1_27063(__this, method) (( void (*) (InternalEnumerator_1_t1_2707 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1_27063_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m1_27064_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1_27064(__this, method) (( bool (*) (InternalEnumerator_1_t1_2707 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1_27064_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern "C" KeyValuePair_2_t1_2706  InternalEnumerator_1_get_Current_m1_27065_gshared (InternalEnumerator_1_t1_2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1_27065(__this, method) (( KeyValuePair_2_t1_2706  (*) (InternalEnumerator_1_t1_2707 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1_27065_gshared)(__this, method)
