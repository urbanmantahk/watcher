﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort.h"

// Mono.Xml.Schema.XsdUnsignedByte
struct  XsdUnsignedByte_t4_31  : public XsdUnsignedShort_t4_30
{
};
