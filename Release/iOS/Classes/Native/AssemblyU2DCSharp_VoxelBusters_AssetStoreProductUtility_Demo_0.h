﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu[]
struct DemoSubMenuU5BU5D_t8_17;
// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu
struct DemoSubMenu_t8_18;

#include "AssemblyU2DCSharp_VoxelBusters_AssetStoreProductUtility_Demo.h"

// VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu
struct  DemoMainMenu_t8_16  : public DemoGUIWindow_t8_14
{
	// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu[] VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::m_subMenuList
	DemoSubMenuU5BU5D_t8_17* ___m_subMenuList_7;
	// VoxelBusters.AssetStoreProductUtility.Demo.DemoSubMenu VoxelBusters.AssetStoreProductUtility.Demo.DemoMainMenu::m_currentSubMenu
	DemoSubMenu_t8_18 * ___m_currentSubMenu_8;
};
