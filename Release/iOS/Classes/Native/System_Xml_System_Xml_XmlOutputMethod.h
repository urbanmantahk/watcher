﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "System_Xml_System_Xml_XmlOutputMethod.h"

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_t4_155 
{
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___1;
};
