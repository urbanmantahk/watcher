﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Punycode
struct Punycode_t1_376;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.Punycode::.ctor()
extern "C" void Punycode__ctor_m1_4213 (Punycode_t1_376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
