﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Exception.h"

// System.Runtime.CompilerServices.RuntimeWrappedException
struct  RuntimeWrappedException_t1_706  : public Exception_t1_33
{
	// System.Object System.Runtime.CompilerServices.RuntimeWrappedException::wrapped_exception
	Object_t * ___wrapped_exception_11;
};
