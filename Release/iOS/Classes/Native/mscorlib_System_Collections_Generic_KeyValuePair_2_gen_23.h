﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t5_11;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1_1929;

#include "mscorlib_System_ValueType.h"

// System.Collections.Generic.KeyValuePair`2<System.Action,System.Collections.Generic.Dictionary`2<System.String,System.Single>>
struct  KeyValuePair_2_t1_2700 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	Action_t5_11 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Dictionary_2_t1_1929 * ___value_1;
};
