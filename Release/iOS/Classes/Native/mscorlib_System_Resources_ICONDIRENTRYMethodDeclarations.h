﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Resources.ICONDIRENTRY
struct ICONDIRENTRY_t1_667;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Resources.ICONDIRENTRY::.ctor()
extern "C" void ICONDIRENTRY__ctor_m1_7520 (ICONDIRENTRY_t1_667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Resources.ICONDIRENTRY::ToString()
extern "C" String_t* ICONDIRENTRY_ToString_m1_7521 (ICONDIRENTRY_t1_667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
