﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>
struct ExifEnumProperty_1_t8_338;
// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifTag.h"
#include "AssemblyU2DCSharp_ExifLibrary_Compression.h"
#include "AssemblyU2DCSharp_ExifLibrary_ExifInterOperability.h"

// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::.ctor(ExifLibrary.ExifTag,T,System.Boolean)
extern "C" void ExifEnumProperty_1__ctor_m8_2073_gshared (ExifEnumProperty_1_t8_338 * __this, int32_t ___tag, uint16_t ___value, bool ___isbitfield, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_2073(__this, ___tag, ___value, ___isbitfield, method) (( void (*) (ExifEnumProperty_1_t8_338 *, int32_t, uint16_t, bool, const MethodInfo*))ExifEnumProperty_1__ctor_m8_2073_gshared)(__this, ___tag, ___value, ___isbitfield, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::.ctor(ExifLibrary.ExifTag,T)
extern "C" void ExifEnumProperty_1__ctor_m8_1974_gshared (ExifEnumProperty_1_t8_338 * __this, int32_t ___tag, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1__ctor_m8_1974(__this, ___tag, ___value, method) (( void (*) (ExifEnumProperty_1_t8_338 *, int32_t, uint16_t, const MethodInfo*))ExifEnumProperty_1__ctor_m8_1974_gshared)(__this, ___tag, ___value, method)
// System.Object ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get__Value()
extern "C" Object_t * ExifEnumProperty_1_get__Value_m8_2074_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get__Value_m8_2074(__this, method) (( Object_t * (*) (ExifEnumProperty_1_t8_338 *, const MethodInfo*))ExifEnumProperty_1_get__Value_m8_2074_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::set__Value(System.Object)
extern "C" void ExifEnumProperty_1_set__Value_m8_2075_gshared (ExifEnumProperty_1_t8_338 * __this, Object_t * ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set__Value_m8_2075(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_338 *, Object_t *, const MethodInfo*))ExifEnumProperty_1_set__Value_m8_2075_gshared)(__this, ___value, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get_Value()
extern "C" uint16_t ExifEnumProperty_1_get_Value_m8_2076_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Value_m8_2076(__this, method) (( uint16_t (*) (ExifEnumProperty_1_t8_338 *, const MethodInfo*))ExifEnumProperty_1_get_Value_m8_2076_gshared)(__this, method)
// System.Void ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::set_Value(T)
extern "C" void ExifEnumProperty_1_set_Value_m8_2077_gshared (ExifEnumProperty_1_t8_338 * __this, uint16_t ___value, const MethodInfo* method);
#define ExifEnumProperty_1_set_Value_m8_2077(__this, ___value, method) (( void (*) (ExifEnumProperty_1_t8_338 *, uint16_t, const MethodInfo*))ExifEnumProperty_1_set_Value_m8_2077_gshared)(__this, ___value, method)
// System.Boolean ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get_IsBitField()
extern "C" bool ExifEnumProperty_1_get_IsBitField_m8_2078_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_IsBitField_m8_2078(__this, method) (( bool (*) (ExifEnumProperty_1_t8_338 *, const MethodInfo*))ExifEnumProperty_1_get_IsBitField_m8_2078_gshared)(__this, method)
// System.String ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::ToString()
extern "C" String_t* ExifEnumProperty_1_ToString_m8_2079_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_ToString_m8_2079(__this, method) (( String_t* (*) (ExifEnumProperty_1_t8_338 *, const MethodInfo*))ExifEnumProperty_1_ToString_m8_2079_gshared)(__this, method)
// ExifLibrary.ExifInterOperability ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::get_Interoperability()
extern "C" ExifInterOperability_t8_113  ExifEnumProperty_1_get_Interoperability_m8_2080_gshared (ExifEnumProperty_1_t8_338 * __this, const MethodInfo* method);
#define ExifEnumProperty_1_get_Interoperability_m8_2080(__this, method) (( ExifInterOperability_t8_113  (*) (ExifEnumProperty_1_t8_338 *, const MethodInfo*))ExifEnumProperty_1_get_Interoperability_m8_2080_gshared)(__this, method)
// T ExifLibrary.ExifEnumProperty`1<ExifLibrary.Compression>::op_Implicit(ExifLibrary.ExifEnumProperty`1<T>)
extern "C" uint16_t ExifEnumProperty_1_op_Implicit_m8_2081_gshared (Object_t * __this /* static, unused */, ExifEnumProperty_1_t8_338 * ___obj, const MethodInfo* method);
#define ExifEnumProperty_1_op_Implicit_m8_2081(__this /* static, unused */, ___obj, method) (( uint16_t (*) (Object_t * /* static, unused */, ExifEnumProperty_1_t8_338 *, const MethodInfo*))ExifEnumProperty_1_op_Implicit_m8_2081_gshared)(__this /* static, unused */, ___obj, method)
