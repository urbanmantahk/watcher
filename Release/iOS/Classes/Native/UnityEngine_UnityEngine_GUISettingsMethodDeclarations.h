﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUISettings
struct GUISettings_t6_183;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m6_1337 (GUISettings_t6_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUISettings::get_doubleClickSelectsWord()
extern "C" bool GUISettings_get_doubleClickSelectsWord_m6_1338 (GUISettings_t6_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUISettings::get_tripleClickSelectsLine()
extern "C" bool GUISettings_get_tripleClickSelectsLine_m6_1339 (GUISettings_t6_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUISettings::get_cursorColor()
extern "C" Color_t6_40  GUISettings_get_cursorColor_m6_1340 (GUISettings_t6_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUISettings::get_cursorFlashSpeed()
extern "C" float GUISettings_get_cursorFlashSpeed_m6_1341 (GUISettings_t6_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUISettings::get_selectionColor()
extern "C" Color_t6_40  GUISettings_get_selectionColor_m6_1342 (GUISettings_t6_183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
extern "C" float GUISettings_Internal_GetCursorFlashSpeed_m6_1343 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
