﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_t1_324;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingMod.h"

// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Boolean,System.Boolean)
extern "C" void DebuggableAttribute__ctor_m1_3573 (DebuggableAttribute_t1_324 * __this, bool ___isJITTrackingEnabled, bool ___isJITOptimizerDisabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
extern "C" void DebuggableAttribute__ctor_m1_3574 (DebuggableAttribute_t1_324 * __this, int32_t ___modes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::get_DebuggingFlags()
extern "C" int32_t DebuggableAttribute_get_DebuggingFlags_m1_3575 (DebuggableAttribute_t1_324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.DebuggableAttribute::get_IsJITTrackingEnabled()
extern "C" bool DebuggableAttribute_get_IsJITTrackingEnabled_m1_3576 (DebuggableAttribute_t1_324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.DebuggableAttribute::get_IsJITOptimizerDisabled()
extern "C" bool DebuggableAttribute_get_IsJITOptimizerDisabled_m1_3577 (DebuggableAttribute_t1_324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
