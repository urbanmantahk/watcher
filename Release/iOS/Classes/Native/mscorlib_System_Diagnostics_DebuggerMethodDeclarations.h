﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.Debugger
struct Debugger_t1_325;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Diagnostics.Debugger::.ctor()
extern "C" void Debugger__ctor_m1_3578 (Debugger_t1_325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Debugger::.cctor()
extern "C" void Debugger__cctor_m1_3579 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.Debugger::get_IsAttached()
extern "C" bool Debugger_get_IsAttached_m1_3580 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.Debugger::IsAttached_internal()
extern "C" bool Debugger_IsAttached_internal_m1_3581 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Debugger::Break()
extern "C" void Debugger_Break_m1_3582 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.Debugger::IsLogging()
extern "C" bool Debugger_IsLogging_m1_3583 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Diagnostics.Debugger::Launch()
extern "C" bool Debugger_Launch_m1_3584 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Debugger::Log(System.Int32,System.String,System.String)
extern "C" void Debugger_Log_m1_3585 (Object_t * __this /* static, unused */, int32_t ___level, String_t* ___category, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
