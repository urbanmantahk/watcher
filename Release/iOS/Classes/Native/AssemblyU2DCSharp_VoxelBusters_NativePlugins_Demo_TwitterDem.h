﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t6_33;

#include "AssemblyU2DCSharp_VoxelBusters_NativePlugins_Demo_NPDisabled.h"

// VoxelBusters.NativePlugins.Demo.TwitterDemo
struct  TwitterDemo_t8_187  : public NPDisabledFeatureDemo_t8_175
{
	// System.String VoxelBusters.NativePlugins.Demo.TwitterDemo::m_shareMessage
	String_t* ___m_shareMessage_10;
	// System.String VoxelBusters.NativePlugins.Demo.TwitterDemo::m_shareURL
	String_t* ___m_shareURL_11;
	// UnityEngine.Texture2D VoxelBusters.NativePlugins.Demo.TwitterDemo::m_shareImage
	Texture2D_t6_33 * ___m_shareImage_12;
};
