﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.HostExecutionContextManager
struct HostExecutionContextManager_t1_1464;
// System.Threading.HostExecutionContext
struct HostExecutionContext_t1_1463;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.HostExecutionContextManager::.ctor()
extern "C" void HostExecutionContextManager__ctor_m1_12660 (HostExecutionContextManager_t1_1464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.HostExecutionContext System.Threading.HostExecutionContextManager::Capture()
extern "C" HostExecutionContext_t1_1463 * HostExecutionContextManager_Capture_m1_12661 (HostExecutionContextManager_t1_1464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.HostExecutionContextManager::Revert(System.Object)
extern "C" void HostExecutionContextManager_Revert_m1_12662 (HostExecutionContextManager_t1_1464 * __this, Object_t * ___previousState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Threading.HostExecutionContextManager::SetHostExecutionContext(System.Threading.HostExecutionContext)
extern "C" Object_t * HostExecutionContextManager_SetHostExecutionContext_m1_12663 (HostExecutionContextManager_t1_1464 * __this, HostExecutionContext_t1_1463 * ___hostExecutionContext, const MethodInfo* method) IL2CPP_METHOD_ATTR;
