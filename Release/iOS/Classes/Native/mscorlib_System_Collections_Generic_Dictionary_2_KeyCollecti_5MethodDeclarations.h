﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1_2221;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m1_18370_gshared (Enumerator_t1_2226 * __this, Dictionary_2_t1_2221 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m1_18370(__this, ___host, method) (( void (*) (Enumerator_t1_2226 *, Dictionary_2_t1_2221 *, const MethodInfo*))Enumerator__ctor_m1_18370_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m1_18371_gshared (Enumerator_t1_2226 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1_18371(__this, method) (( Object_t * (*) (Enumerator_t1_2226 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1_18371_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1_18372_gshared (Enumerator_t1_2226 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1_18372(__this, method) (( void (*) (Enumerator_t1_2226 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1_18372_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m1_18373_gshared (Enumerator_t1_2226 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1_18373(__this, method) (( void (*) (Enumerator_t1_2226 *, const MethodInfo*))Enumerator_Dispose_m1_18373_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m1_18374_gshared (Enumerator_t1_2226 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1_18374(__this, method) (( bool (*) (Enumerator_t1_2226 *, const MethodInfo*))Enumerator_MoveNext_m1_18374_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m1_18375_gshared (Enumerator_t1_2226 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1_18375(__this, method) (( Object_t * (*) (Enumerator_t1_2226 *, const MethodInfo*))Enumerator_get_Current_m1_18375_gshared)(__this, method)
